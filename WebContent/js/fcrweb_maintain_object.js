var	mtn_gps_kv_sep 	= "=";
var mtn_gps_e_sep 	= "&";

function create_mtn_grps(name, type)
{
	this.initialize = function(name, type)
	{
		this.name		= name;
		this.type 		= type;	
		this.elems 		= new Object();
		this.store 		= new Object();
		this.grps;
		this.Grp_idx 	= -1;
		this.elems['g-type'] = "mtn-" + type;
		return this;
	};
	
	this.getNbGrps 	= function()
	{
		if (isEmpty(this.elems['g-nb-group']))
			this.setNbGrps(0);
		return this.elems['g-nb-group'];
	};
	this.setNbGrps 	= function(nbGrp){this.elems['g-nb-group'] = nbGrp;};
	
	this.getGrpsIdx = function()
	{
		if (isEmpty(this.elems['g-idx']))this.setGrpsIdx(this.getNbGrps() + 1);
		return this.elems['g-idx'];
	};
	this.setGrpsIdx = function(idx){this.elems['g-idx'] = idx;};

	this.addGrpsInfo = function(key, value)
	{
		this.elems["g-" + this.getGrpsIdx() + "-" + key] = value;
	};
	
	this.getGrpsInfo = function(key)
	{
		var tmp = this.elems["g-" + this.getGrpsIdx() + "-" + key];
		return !isEmpty(tmp) ? tmp : "";
	};
	
	this.removeGrpsInfos = function(idx)
	{
		for (var key in this.elems)
		{
			if (hasX(key, "g-" + idx))
				delete this.elems[key];
		}
	};
	
	this.replaceGroup = function(old_idx, new_idx)
	{
		/** clean up new index **/
		this.removeGrpsInfos(new_idx);		
		for (var key in this.elems)
		{
			if (hasX(key, "g-" + old_idx))
				this.elems[key.replace("g-" + old_idx, "g-" + new_idx)] = this.elems[key];				
		}
		this.removeGrpsInfos(old_idx);
	};

	this.create = function()
	{
		if (!check_app_srv_pre(true))
			return -1;
		
		changeApplicationService_mtn();
		//show(this.type + "-dialog");mask_on();
		switch_mtn_dialog_view(this.type + "-dialog");
		setHtml(this.type + "-dialog-header", "Add");				
		this.setGrpsIdx(this.getNbGrps() + 1);			
		this.clear(0);
		
		setReadonlyViewControls();
		jq_hide("edit-button");
		jq_show("add-button");	
	};
	
	this.load = function(idx, action)
	{
		if (!check_app_srv_pre(false))
		{
			notify("Error: undetermined application, service context")
			return -1;
		}
		
		//show(this.type + "-dialog");mask_on();
		switch_mtn_dialog_view(this.type + "-dialog");
		setHtml(this.type + "-dialog-header", capFirst(action));
		this.setGrpsIdx(idx);
		this.clear(0);
		
		if (this.type=="goo")
		{
			//changeApplicationService_mtn();	//only services stuff
			var next = this.type + "-org";
			setSelectE(next, this.getGrpsInfo(next));
			this.changeOrg(set_org_elems_end_load_mtn);
			/** @see this.end_load_goo **/
		}
		else
		{
			changeApplicationService_mtn(set_app_srv_end_load_gsrv);
			/** @see this.end_load_gsrv **/
		}
		
		jq_hide("add-button");
		jq_show("edit-button");
		
	};
	
	this.end_load_goo = function()
	{
		next = this.type + "-name";
		getE(next).value = this.getGrpsInfo(next);
		this.changeGOO();
		next = this.type + "-env";
		setSelectE(next, getEnvProposition(this.getGrpsInfo(next)));
		next = this.type + "-action";
		var action = this.getGrpsInfo(next);
		checkRadio(next + "-" + this.getGrpsInfo(next));
		this.change_goo_action();			
		next = this.type + "-new-name";
		if (isX(action, "rename"))
			getE(next).value = this.getGrpsInfo(next);
		if (isX(action, "create") | isX(action, "update"))
		{
			next = this.type + "-hosts";
			setHtml(next, this.groupList2options(next));
			next = this.type + "-subnets";
			setHtml(next, this.groupList2options(next));
			next = this.type + "-goos";
			setHtml(next, this.groupList2options(next));
			
			this.add_cat_element_group_member_in_use();
		}
		$( ".goo-group-element-wrapper" ).scrollTop(0);
		this.end_load_common();
	}
	
	this.end_load_gsrv = function()
	{
		var next = this.type + "-name";
		getE(next).value = this.getGrpsInfo(next);
		this.changeGroupOfServices();
		next = this.type + "-action";
		var action = this.getGrpsInfo(next);
		checkRadio(next + "-" + this.getGrpsInfo(next));
		this.change_gsrv_action();			
		next = this.type + "-new-name";
		if (isX(action, "rename"))
			getE(next).value = this.getGrpsInfo(next);
		if (isX(action, "create") | isX(action, "update"))
		{
			next = this.type + "-srvs";
			setHtml(next, this.groupList2options(next));
			this.add_cat_element_group_member_in_use();
		}
		
		this.end_load_common();
	}
	
	this.end_load_common = function()
	{
		this.setPreapprovedView();
		setReadonlyViewControls();
	};
	
	this.setPreapprovedView = function()
	{
		if (isPreapprovedGroups())
		{
			jq_hide("preapproved-unauthorized");
			jq_disabled("preapproved-fields");

			if (!isX(getRadio(this.type + "-action").value, "update"))
			{
				checkRadio("goo-action-update");
				this.change_goo_action();
			}
		}
	};
	
	this.remove = function(remove_idx)
	{
		if (isX(getE('debug-area').value,'true') || confirm(" confirm delete the group " + getHtml("del-g-" + remove_idx + "-name")))
		{
			/** in order to avoid gap in control indexes, reset last index control to current index. **/
			if (remove_idx < this.getNbGrps())
				this.replaceGroup(this.getNbGrps(), remove_idx);
			else
				this.removeGrpsInfos(remove_idx);			
			
			this.setNbGrps(this.getNbGrps() - 1);
			this.setGrpsIdx(this.getNbGrps());		// set index to last ctrl
			this.leastOneGroup();
			this.refreshGroupsView();
		}
	};
	
	this.groupList2options = function(elem)
	{
		var options 	= "";
		var elements 	= this.getGrpsInfo(elem).split(";;;");
		for (var i in elements)
		{
			if (!isEmpty(elements[i]))
			{
				var map 			= in_params2map(elements[i]);
				var disabled_opt 	= ""//"disabled";
				options 			+= "<option value='"+ elements[i] + "' " + disabled_opt + " class='control-option-disabled' >";
				options				+= map['output'] + " - " + getActionName(map["uc"]);
				options				+= "</option>";
			}
		}
		return options;
	};

	this.add_cat_element_group_member_in_use = function ()
	{	
		if (isX(this.type, "goo"))
		{
			var obj_type 	= "goo-host"; 
			var obj_sel_id 	= "goo-hosts"
			var member_list	= householder["list-members-" + obj_type + "-fw"];
			for (var mbr in member_list)
			{
				if (!optionExist(obj_sel_id, mbr + ext_remove) & !optionExist(obj_sel_id, mbr + ext_use))
					addOptionL(obj_sel_id,  mbr + ext_use , member_list[mbr]);				
			}
			
			obj_type 	= "goo-subnet"; 
			obj_sel_id 	= "goo-subnets"
			member_list	= householder["list-members-" + obj_type + "-fw"];
			for (var mbr in member_list)
			{
				if (!optionExist(obj_sel_id, mbr + ext_remove) & !optionExist(obj_sel_id, mbr + ext_use))
					addOptionL(obj_sel_id,  mbr + ext_use , member_list[mbr]);				
			}
			
			obj_type 	= "goo-selector"; 
			obj_sel_id 	= "goo-goos"
			member_list	= householder["list-members-" + obj_type + "-fw"];
			for (var mbr in member_list)
			{
				if (!optionExist(obj_sel_id, mbr + ext_remove) & !optionExist(obj_sel_id, mbr + ext_use))
					addOptionL(obj_sel_id,  mbr + ext_use , member_list[mbr]);				
			}
		}
		else
		{
			var obj_type 	= "gsrv-srv"; 
			var obj_sel_id 	= "gsrv-srvs"
			var member_list	= householder["list-members-" + obj_type + "-fw"];
			for (var mbr in member_list)
			{
				if (!optionExist(obj_sel_id, mbr + ext_remove) & !optionExist(obj_sel_id, mbr + ext_use))
					addOptionL(obj_sel_id,  mbr + ext_use , member_list[mbr]);				
			}
		}
	};

	this.sel_add = function(elem, do_delete)
	{
		if (this.valid_subgroup(elem))
		{
			var map 	= new Object();	
			var meta 	= new Object();
			var	id 		= ""; 
			var sel 	= "";
			if (isX(elem, "host"))
			{
				sel				= "hosts";				
				map["cat"]		= "host";
				map["host"] 	= getE(this.type + "-host").value;
				map["env"] 		= getSelectE(this.type + "-host-env").value;
				map["ip"] 		= getE(this.type + "-ip").value;
				map["ip-tsl"] 	= getE(this.type + "-ip-tsl").value; 
				map["uc"]		= this.getElementAction("host", map["host"]);

				meta["area"]	= getE(this.type + "-host-area").value;				
				
				id				= getOrgShort(getSelectE(this.type + "-org").value);
				id				+= "-host-" + normalize(map["host"]) + "-" + map["env"] + "-" + map["ip"];				
			}
			else if (isX(elem, "subnet"))
			{
				sel				= "subnets";
				map["cat"]		= "subnet";
				map["subnet"] 	= getE(this.type + "-subnet").value;
				map["area"] 	= getE(this.type + "-subnet-area").value;
				map["uc"]		= this.getElementAction("subnet", map["subnet"]);	
				
				meta["area"]	= getE(this.type + "-subnet-area").value;
				
				id				= getOrgShort(getSelectE(this.type + "-org").value);
				id				+= "-net-" + replaceAllIn(map["subnet"], "/", "-");				
			}
			else if (isX(elem, "goo-selector"))
			{
				sel				= "goos";
				map["cat"]		= "goo";
				map["goo"] 		= getE(this.type + "-selector").value;
				map["env"] 		= getSelectE(this.type + "-sel-env").value;
				map["uc"]		= this.getElementAction("selector", map["goo"]);
				id				= getOrgShort(getSelectE(this.type + "-org").value);
				id				+= "-" + normalize(map["goo"]) + "-" + map["env"] + "-grp";
			}
			else if (isX(elem, "srv"))
			{
				sel				= "srvs";
				map["srv-name"]	= getE(this.type + "-srv-name").value;
				map["srv-proto"]= getE(this.type + "-srv-proto").value;
				map["srv-type"]	= getE(this.type + "-srv-type").value;
				map["srv-port"]	= getE(this.type + "-srv-port").value;				
				map["uc"]		= this.getElementAction("srv", map["srv-name"]);		
				id				= map["srv-proto"] + "-" + normalize(map["srv-name"]) + (isX(map["srv-proto"], "icmp") ? "" : ("-" + map["srv-port"]));				
			}
			map["output"] 	= id;
			map["meta"] 	= meta_map2params(meta);

			var  uc = getActionName(map["uc"])
			if (isX(uc, "remove"))
			{
				notify("this " + (isX(this.type, "goo") ? "object" : "service") + " is already in this group");
			}
			else if (!optionExist(this.type + "-" + sel, id + " - " + uc))
			{
				var options = new Object();
				/*
				options["disabled"] 	= "true";
				options["font-color"] 	= "black";
				options["class"] 		= "control-option-disabled";
				*/
				addOptionL(this.type + "-" + sel, id + " - " +  uc, in_map2params(map), options);
				jq_hide("warning-error-tr-" + this.type + "-" + sel);
			}
			else
				notify("the element " + id + " is already in selection");
		}
	};
	
	this.sel_rem = function(elem)
	{
		var sel_id = this.getSelectorId(elem);

		if (!hasSelection(sel_id) /*| !this.is_all_not_use(elem) */)
			return ;

		var selected 	= getSelectedIndex(sel_id);
		var options		= getE(sel_id).options;
		for (var i=0 ; i<selected.length ; i++)
		{
			if (selected[i])
			{
				if (isPreapprovedGroups() && hasX(options[i].text, ext_use))
				{
					/** clone this object **/
					addOptionL(sel_id,options[i].text,options[i].value);
				}
				else //toggle member <-> remove
				{
					if (hasX(options[i].text, ext_use))
					{
						addOptionL
						(
							sel_id, 
							replaceAllIn(options[i].text, ext_use, ext_remove), 
							replaceAllIn(options[i].value,"uc---member","uc---remove")
						);
					}
					else if (hasX(options[i].text, ext_remove))
					{
						addOptionL
						(
							sel_id, 
							replaceAllIn(options[i].text, ext_remove, ext_use), 
							replaceAllIn(options[i].value,"uc---remove","uc---member")
						);
					}
				}				
			}
		}		
		
		removeSelectedOptions(this.getSelectorId(elem));
		jq_hide("delete-member-button");
		jq_hide("drop-rule-button");
	};
	
	this.display_selection_action_button = function(elem)
	{
		if (hasSelection(this.getSelectorId(elem)))
			show(this.type + "-" + elem + "-drop-rule-button");
		else
			hide(this.type + "-" + elem + "-drop-rule-button");
	};
		
	this.getSelectorId = function(elem)
	{
		var sel = "";
		if (isX(elem, "host"))
			sel = this.type + "-hosts";
		else if (isX(elem, "subnet"))
			sel = this.type + "-subnets";
		else if (hasX(elem, "selector"))
			sel = this.type + "-goos";
		else if (isX(elem, "srv"))
			sel = this.type + "-srvs";
		return sel;
	};
	
	this.getElementAction = function(elem, value)
	{
		if (isInMemberList(this.type + "-" + elem, value))
			return "remove";
		else if (isInSuggestionList((this.type!="gsrv") ? this.type + "-" + elem : "gsrv-srv-name", value))
			return "use";
		else 
			return (elem!="goo") ? "create" : "undefined";
	};
	
	this.valid_subgroup = function (elem)
	{
		var valid = true;
		if (this.type == "goo")
		{
			if (isX(elem, "host"))
			{
				/** TODO check public host ip **/
				var host 	= getE(this.type + "-host").value;
				var ip 		= getE(this.type + "-ip").value;
				var h_sugg	= isInSuggestionList(this.type + "-host", host);
				var ip_sugg	= isInSuggestionList(this.type + "-ip", ip);
				if (isEmpty(host))
				{
					setHtml("warning-error-td-" + this.type + "-host", "Missing value.");
					showError(this.type + "-host");
					valid = false;
				}
				else if (isEmpty(ip) | !isIp(ip))
				{
					setHtml("warning-error-td-" + this.type + "-ip", "Missing value or incorrect ip address.");
					showError(this.type + "-ip");
					valid = false;
				}
				// host or ip is an autocomplete value but not both
				else if ((h_sugg & !ip_sugg) | (!h_sugg & ip_sugg))
				{
					setHtml("warning-error-td-" + this.type + "-host", "this host name has already been created.");
					setHtml("warning-error-td-" + this.type + "-ip", "this ip address has already been created.");
					if((!h_sugg & ip_sugg)) 
						showError(this.type + "-ip");
					else 
						showError(this.type + "-host");
					valid = false;
				}
				//always check ip-tsl for ip validity
				if (!isEmpty(getE(this.type + "-ip-tsl").value) && !isIp(getE(this.type + "-ip-tsl").value))
				{
					showError(this.type + "-ip-tsl");
					valid = false;
				}
				if (isX(getSelectE(this.type + "-org").value, "MOBISTAR") && !h_sugg)
				{
					setError(this.type + "-host-opt", "You are not allowed to create a new host");
					hideError(this.type + "-host");
					showError(this.type + "-host-opt");
					valid = false;
				}
				else if (!this.check_pending_validation("ip"))	/** external validations **/
					valid = false;
			}
			else if (isX(elem, "subnet"))
			{
				var subnet 	= getE(this.type + "-subnet").value;
				if (isEmpty(subnet) || !isSubnetWithMask(subnet))
				{
					showError(this.type + "-subnet");
					valid = false;
				}
				/*
				var area = this.type + "-subnet-area";
				
				if ( isEmpty(getE(area).value) || isX(getSelectE(this.type + "-org").value, "MOBISTAR") && !isInSuggestionList(area, getE(area).value))
				{
					showError(area);
					valid = false;
				}
				*/
				if (isX(getSelectE(this.type + "-org").value, "MOBISTAR") && !isInSuggestionList(this.type + "-subnet", subnet))
				{
					setError(this.type + "-subnet-opt", "You are not allowed to create a new subnet");
					hideError(this.type + "-subnet");
					showError(this.type + "-subnet-opt");
					valid = false;
				}				
				else if (!this.check_pending_validation("subnet"))	/** external validations **/
					valid = false;
			}			
			else if (isX(elem, "goo-selector"))
			{
				var next = this.type + "-selector";
				/** goo must be an existant goo but cannot be in relation with itself **/
				if (isX(getE(this.type + "-name").value, getE(next).value) | !isInSuggestionList(next, getE(next).value))
				{
					showError(next);
					valid = false;
				}
			}
			else if (isX(elem, "goo"))
			{
				var name 	= this.type + "-name";
				var action	= this.type + "-action";
				var rename	= this.type + "-new-name";
				
				if (isEmpty(getSelectE(this.type + "-org").value) | isX(getSelectE(this.type + "-org").value, default_choice))
				{
					showError(this.type + "-org");
					valid = false;
				}
				else if (isEmpty(getE(name).value))
				{
					showError(name);
					valid = false;
				}
				else if (getRadio(action)==null || isEmpty(getRadio(action).value) || isX(getRadio(action).value, "modify"))
				{
					showError(action);
					valid = false;
				}
				/** renaming : new name could not be in name suggestion list (name conlict) **/
				else if 
				(
					isX(getRadio(action).value, "rename") && 
					(isEmpty(getE(rename).value) | isInSuggestionList(name, getE(rename).value))
				)
				{
					showError(rename);
					valid = false;
				}
				else if (isX(getRadio(action).value, "create") | isX(getRadio(action).value, "update"))
				{
					var total_objects = getE(this.type + "-hosts").options.length;
					total_objects += getE(this.type + "-subnets").options.length;
					total_objects += getE(this.type + "-goos").options.length;
					if (total_objects <= 0)
					{
						jq_show("warning-error-tr-" + this.type + "-selections");
						valid = false;						
					}
				}
			}
		}
		else
		{
			if (isX(elem, "srv"))
			{
				var name = this.type + "-srv-name";
				if (isEmpty(getE(name).value))
				{
					showError(name);
					valid = false;
				}				
				else if (isInSuggestionList(name, getE(name).value))
				{
					//skip;
				}
				else
				{
					var port = getE(this.type + "-srv-port").value;
					if (isEmptyOrDefault(getSelectE(this.type + "-srv-proto").value))
					{
						showError(this.type + "-srv-proto");
						valid = false;
					}
					
					if (isEmpty(port) || (!isPort(port) && !isPortRange(port)))
					{
						showError(this.type + "-srv-port");
						valid = false;
					}					
				}
			}
			else if (isX(elem, "gsrv"))
			{	
				var name 	= this.type + "-name";
				var action	= this.type + "-action";
				var rename	= this.type + "-new-name";
				if (isEmpty(getE(name).value))
				{
					showError(name);
					valid = false;
				}
				else if (getRadio(action)==null || isEmpty(getRadio(action).value) || isX(getRadio(action).value, "modify"))
				{
					showError(action);
					valid = false;
				}
				/** renaming : new name could not be in name suggestion list (name conlict) **/
				else if 
				(
					isX(getRadio(action).value, "rename") && 
					(isEmpty(getE(rename).value) | isInSuggestionList(name, getE(rename).value))
				)
				{
					showError(rename);
					valid = false;
				}
				else if (isX(getRadio(action).value, "create") | isX(getRadio(action).value, "update"))
				{
					if (getE(this.type + "-srvs").options.length <= 0)
					{
						jq_show("warning-error-tr-" + this.type + "-srvs");
						valid = false;						
					}
				}
			}
		}
		return valid;
	};
	
	this.check_pending_validation = function(elem)
	{
		var valid = true;
		if (isX(getE(this.type + "-" + elem + "-code").value, "0"))
		{
			notify("Please, wait a while until some validations terminates then retry.");
			valid = false;
		}
		else if(isX(getE(this.type + "-" + elem + "-code").value, "-1"))
		{
			valid = false;
		}
		return valid;
	};
	
	this.valid = function()
	{
		var valid = true;
		
		if (this.type=="goo")
		{
			if (isEmpty(getSelectE(this.type + "-org").value))
			{
				showError(this.type + "-org");
				valid = false;
			}
			else
				valid = this.valid_subgroup("goo");
		}
		else
			valid = this.valid_subgroup("gsrv");
			
		return valid;
	}
	
	
	this.insert_replace = function (do_replace)
	{
		if (!this.valid())
			return ;

		if (!do_replace)
			this.setNbGrps(this.getNbGrps() + 1);
		else
			this.removeGrpsInfos(this.getGrpsIdx());
	
		var meta = new Object();
		
		if (this.type=="goo")
		{
			this.addGrpsInfo(this.type + "-org", getSelectE(this.type + "-org").value);			
			this.addGrpsInfo(this.type + "-env", getSelectE(this.type + "-env").value);
			var name = getE(this.type + "-name").value;
			this.addGrpsInfo(this.type + "-name", name);
			
			var output = getOrgShort(getSelectE(this.type + "-org").value);
				output += "-" + normalize(getE(this.type + "-name").value);
				output += "-" + getSelectE(this.type + "-env").value + "-grp";
			this.addGrpsInfo(this.type + "-output", output);
			
			var action = getRadio(this.type + "-action").value
			this.addGrpsInfo(this.type + "-action", action);			
			if (isX(action, "create") | isX(action, "update"))
			{
				var list_suffix = "-hosts";
				this.addGrpsInfo(this.type + list_suffix, select_list2values(getE(this.type + list_suffix).options, ";;;"));
				list_suffix = "-subnets";
				this.addGrpsInfo(this.type + list_suffix, select_list2values(getE(this.type + list_suffix).options, ";;;"));
				list_suffix = "-goos";
				this.addGrpsInfo(this.type + list_suffix, select_list2values(getE(this.type + list_suffix).options, ";;;"));
			}
			if (isX(action, "rename"))
			{
				var rename = getE(this.type + "-new-name").value;
				this.addGrpsInfo(this.type + "-new-name", rename);
				this.addGrpsInfo(this.type + "-new-name-fw", replaceAllIn(output, normalize(name), normalize(rename)));
			}
		}
		else
		{
			var name = getE(this.type + "-name").value;
			this.addGrpsInfo(this.type + "-name", name);
			var output = normalize(getE(this.type + "-name").value) + "-grp";
			this.addGrpsInfo(this.type + "-output", output);
			var action = getRadio(this.type + "-action").value
			this.addGrpsInfo(this.type + "-action", action);			
			if (isX(action, "create") | isX(action, "update"))
				this.addGrpsInfo(this.type + "-srvs", select_list2values(getE(this.type + "-srvs").options, ";;;"));
			if (isX(action, "rename"))
			{
				var rename = getE(this.type + "-new-name").value;
				this.addGrpsInfo(this.type + "-new-name", rename);
				this.addGrpsInfo(this.type + "-new-name-fw", replaceAllIn(output, normalize(name), normalize(rename)));
			}
		}
		
		this.addGrpsInfo(this.type + "-meta", meta_map2params(meta));
		
		
		this.setGrpsIdx(this.getNbGrps() + 1);
		this.clear(0);
		mask_off();
		hide(this.type + "-dialog");
		switch_mtn_dialog_view("main");
		this.leastOneGroup();
		this.refreshGroupsView();
	};
	
	this.add 	= function(){this.insert_replace(false);};
	this.edit 	= function(){this.insert_replace(true);};
	
	this.toHtmlParams = function()
	{
		var vars = "";
		for (var key in this.elems)
		vars += (vars!="") 	? mtn_gps_e_sep + key + mtn_gps_kv_sep + this.elems[key] 
							: key + mtn_gps_kv_sep + this.elems[key]
		return vars;
	};
	
	this.leastOneGroup = function()
	{
		var hasLeast = isPos(this.getNbGrps());
		
		if (hasLeast)
		{
			jq_disabled("application-service");
			show("edit-application-service-button-mtn");
		} else {
			jq_enabled("application-service");
			hide("edit-application-service-button-mtn");
		} 
		return hasLeast;
	};
	
	this.refreshGroupsView = function()
	{
		setHtml("list-maintain-groups", "");
		getE("conflicts").value = false;
		for (var i=1 ; i<=this.getNbGrps() ; i++)
		{
			this.setGrpsIdx(i);			
			setHtml("list-maintain-groups", getHtml("list-maintain-groups") + getHtml("flow-template").replace(/template/g, "g-" + i).replace(/idx/g, i));
			
			var action 		= this.getGrpsInfo(this.type + "-action");
			//var act_color	= isFWO() ? "style='color:" + getActionColor(action) + ";'" : "";
			var act_color	= "style='color:" + getActionColor(action) + ";'"; // always display with colors
			setHtml("g-" + i + "-action", "<span " + act_color + ">" + this.getOutputAction(action) + "</span>");
			setHtml("g-" + i + "-name", "<span id='del-g-" + i + "-name'" + act_color + ">" + this.getGrpsInfo(this.type + "-output") + "</span>");
			setHtml("g-" + i + "-list", isX(action,"rename") ? "<span " + act_color + ">" + this.getGrpsInfo(this.type + "-new-name-fw") + "</span>" : this.getGroupList());
			if (isX(action,"modify"))
				getE("conflicts").value = true;
		}
	
		$('.maintain-groups-row:odd').css('background-color','#CCCCCC');
	};
	
	this.getOutputAction = function(action)
	{
		if (isX(action,"create")) return "Create group";
		else if (isX(action,"rename")) return "Update";
		else if (isX(action,"update")) return "Add / Delete " + (isX(this.type,"goo") ?  "object" : "service") + "(s)";
		else if (isX(action,"delete")) return "Delete group";
		else return action;
	};
	
	/** grpsIdx dependent **/
	this.getGroupList = function()
	{	
		var names 		= "";
		var elems 		= this.type=="goo" ? ["-hosts","-subnets","-goos"] : ["-srvs"];

		for (var i in elems)
		{
			var elements = this.getGrpsInfo(this.type + elems[i]).split(";;;");
			for (var i in elements)
			{
				if (!isEmpty(elements[i]))
				{
					var element = in_params2map(elements[i]);
					var id 		= element['output'];
					
					//if (isFWO())
						id = "<span style='color:" + getActionColor(element['uc']) + ";'>" + id + "</span>";
					
					names += isEmpty(names) ? id : small_br + id;					
				}
			}
		}
		return names;
	};
	
	
	this.clear = function(lvl)
	{
		jq_hide("warning-error");
		jq_hide("control-create");
		
		if (this.type=="goo")
		{
			/** TODO GOO flowControlFocusOut **/
			if (lvl<=0)
			{
				setSelectE(this.type + "-org", default_choice);	
			}
			if (lvl<=1)
			{
				getE("goo-name").value = "";	
				setSelectE("goo-env", default_choice);
				jq_hide("goo-rows");
				clearMembers("goo-host");
				clearMembers("goo-subnet");
				clearMembers("goo-goo");
			}
			if (lvl<=2)
			{
				uncheckAllRadio("goo-action");
				hide("row-goo-action");
				jq_hide("row-goo-action");
			}
			if (lvl<=3)
			{
				getE("goo-new-name").value = "";
				var attrs = ["host","ip","ip-tsl","host-area","subnet","subnet-area","selector"];
				for (var k=0 ; k<attrs.length ; k++)
					getE(this.type  + "-" + attrs[k]).value = "";
					
				setSelectE("goo-host-env", default_choice);
				setSelectE("goo-sel-env", default_choice);
				
				removeOptions(this.type + "-hosts");
				removeOptions(this.type + "-subnets");
				removeOptions(this.type + "-goos");
				jq_hide("delete-member-button");
				jq_hide("drop-rule-button");
				jq_hide("row-goo-action-field");
				/*
				hide("tr-" + this.type + "-host-area");
				hide("tr-" + this.type + "-subnet-area");
				*/
				var cons_checks = ["ip","subnet"];
				for (var k=0 ; k<cons_checks.length ; k++)
				{
					hide(this.type + "-" + cons_checks[k] + "-check");
					hide(this.type + "-" + cons_checks[k] + "-error");
					getE(this.type + "-" + cons_checks[k] + "-code").value = "";
					hideError(this.type + "-" + cons_checks[k] + "-opt");
				}
			}
			jq_hide("goo-warning");
		}
		else
		{
			if (lvl<=0)
			{
				getE("gsrv-name").value = "";
				clearMembers("gsrv-srv");
			}
			if (lvl<=1)
			{
				uncheckAllRadio("gsrv-action");
				jq_hide("row-gsrv-action");
			}
			if (lvl<=2)
			{					
				jq_hide("row-gsrv-action-field");
				getE("gsrv-new-name").value = "";
				getE("gsrv-srv-name").value = "";
				
				removeOptions(this.type + "-srvs");
				jq_hide("delete-member-button");
				jq_hide("drop-rule-button");
			}
			if (lvl<=3)
			{	
				setSelectE("gsrv-srv-proto", default_choice);
				//this.changeProto();
				getE("gsrv-srv-type").value 	= "";
				getE("gsrv-srv-port").value 	= "";
			}			
			jq_hide("gsrv-warning");
		}
	};
	
	this.cancel = function()
	{
		hide(this.type + "-dialog");
		switch_mtn_dialog_view("main");
		mask_off();
	};
	
	
	/**
		GOO functions
	**/
	this.changeOrg = function(opt_fun)
	{
		this.clear(1);
		if (!isEmptyOrDefault(getSelectE(this.type + "-org").value))
		{
			modal_on();
			send_request
			(
				{
					"method":'GET',
					"url":default_url,
					"callback": opt_fun==null ? set_org_list_elements_mtn : opt_fun,
					"data":
					{
						"action":access('list-organization-elements'),
						"application-service":getE("application-service").value,
						"organization":getSelectE(this.type + "-org").value,
						"type":this.type,
						"filter-global":true
					}
				}
			);
			jq_show("goo-rows");
		}		
	};
	
	this.changeGOO = function()
	{
		var id 		= "goo-name";
		var type 	= this.type;
		this.clear(2);		
		
		if (isEmpty(getE(id).value))
			jq_hide("row-goo-action");
		else
		{
			var isInList = doAutocompleteOnValueChange
			(
				id, 
				getE(id).value,
				{
					"type":type, 
					"element":"goo", 
					"name":getE(id).value,
					"org":getSelectE(type + '-org').value
				}, 
				function(request)
				{
					setSelectEC(type + "-env", getEnvProposition(request['json'].environment));					
					setSelectEC(type + "-env", (isEmpty(getE(id).value) ? default_choice : getEnvProposition(request['json'].environment)));
				}
			)
			
			if (isInList)
			{
				getMembers("goo", "goo");
				show("row-goo-action");
				show("row-goo-rename");
				show("row-goo-update");
				show("row-goo-delete");
			}
			else
			{
				clearMembers("goo-host");
				show("row-goo-action");
				show("row-goo-create");
				checkRadio(this.type + "-action-create");
				$('.group-element-scrollable').scrollTop(0);
				this.change_goo_action();				
			}			
		}		
	};
	
	this.change_goo_action = function()
	{
		this.clear(3);
		jq_show("row-" + this.type + "-" + getRadio(this.type + "-action").value);
		
		if (!isEmpty(getRadio(this.type + "-action").value) & !isX(getRadio(this.type + "-action").value, "create"))
		{
			show("goo-show-list-member");			
			this.add_cat_element_group_member_in_use();
		}
		else
			hide("goo-show-list-member");
			
		this.resize_group_wrapper();
	};
	
	this.resize_group_wrapper = function()
	{		
		if (isIE8())
		{	
			var action = $("input:radio[name='" + this.type + "-action" + "']:checked").val();
			var resize = isX(action, "create") || isX(action, "update");

			if (resize)
				jq_css(this.type + "-group-element-wrapper", "height", "400px");
			else
				jq_css(this.type + "-group-element-wrapper", "height", "");
		}
	};

	this.changeHost = function()
	{
		if (isX(getSelectE(this.type + "-org").value,"EXTERNAL"))
			getE(this.type + "-ip").value = "";
		this.changeHostIp("host");
	};
	this.changeIp = function(){this.changeHostIp("ip")};	
	this.changeHostIp = function(elem)
	{
		
		var type 	= this.type;
		var tmp_k 	= type + '-' + elem;
		var tmp_v 	= getE(type + '-' + elem).value;
		//if (isX(householder[tmp_k + "-tmp"], tmp_v)){return -1;}
		this.clearInfo(1);
		getE(tmp_k).value = tmp_v;
		/** caution host-ip recursive interaction **/
		var isInList = doAutocompleteOnValueChange
		(
			type + "-" + elem, 
			getE(type + "-" + elem).value,
			{
				"type":type, 
				"element":elem, 
				"name":getE(type + '-' + elem).value, 
				"ip":getE(type + '-' + elem).value,
				"org":getSelectE(type + '-org').value
			}, 
			function(request)
			{
				getE(type + "-host").value 		= request['json'].name;
				getE(type + "-ip").value 		= request['json'].ip;
				getE(type + "-ip-tsl").value 	= request['json'].tsl_ip;
				setSelectEC(type + "-host-env", (isEmpty(getE(type + "-host").value) ? default_choice : getEnvProposition(request['json'].environment)));
				
				if (isX(getSelectE(type + '-org').value, "MOBISTAR"))
				{
					getE(type + "-host-area").value = request['json'].area;
					show("tr-" + type + "-host-area");
				}
				else
					hide("tr-" + type + "-host-area");
				
				if (!isEmpty(request['json'].ips))
				{
					setError(type + "-ip-opt", "Some other ip(s) matche(s) to this host: " + request['json'].ips);
					showError(type + "-ip-opt");
					show(type + "-ip-warning");
				}
				else
				{
					hideError(type + "-ip-opt");
					hide(type + "-ip-warning");
				}
			}
		)
		if (isInList)
		{	
			disable_autocomplete_fields(this.type + "-host-autocomplete-rows");
			jq_hide("control-create-host");
		}
		else
		{
			enable_autocomplete_fields(this.type + "-host-autocomplete-rows");
			
			/** if External user attempt to create new host check if the ip is not in private range **/
			if (elem=="ip" && isIp(getE(type + '-ip').value) && getSelectE(this.type + "-org").value=="EXTERNAL")
			{
				getE(this.type + "-ip-code").value = "0";
				send_request
				(
					{
						"method":'GET', 
						"url":default_url, 
						"callback": checkExternalIpRanges, 
						"data":
						{
							"action":access('check-private-range-constraint'),
							"type":this.type,
							"element":'ip',
							"ip":getE(type + '-ip').value
						}
					}
				);
				modal_on();				
			}
			if (!isX(getSelectE(this.type + "-org").value, "MOBISTAR"))
				jq_show("control-create-host");
		}
	};

	this.changeEnv = function(attr)
	{
	};
	
	this.changeSubnet = function()
	{
		var type = this.type;
		var id = type + "-subnet";		
		this.clearInfo(2);

		var isInList = doAutocompleteOnValueChange
		(
			type + "-subnet", 
			getE(type + "-subnet").value,
			{
				"type":type, 
				"element":"subnet", 
				"subnet":getE(type + '-subnet').value,
				"org":getSelectE(type + '-org').value
			}, 
			function(request)
			{
				getE(type + "-subnet-area").value = request['json'].area;
			}
		)
		
		if (isInList)
		{
			disable_autocomplete_fields(this.type + "-subnet-autocomplete-rows");
			jq_hide("control-create-subnet");
		}
		else
		{
			enable_autocomplete_fields(this.type + "-subnet-autocomplete-rows");
			
			if (isSubnetWithMask(getE(type + '-subnet').value) && getSelectE(this.type + "-org").value=="EXTERNAL")
			{
				getE(this.type + "-subnet-code").value = "0";
				send_request
				(
					{
						"method":'GET', 
						"url":default_url, 
						"callback": checkExternalIpRanges, 
						"data":
						{
							"action":access('check-private-range-constraint'),
							"type":this.type,
							"element":'subnet',
							"subnet": getE(type + '-subnet').value
						}
					}
				);
				modal_on();				
			}
			if (!isX(getSelectE(this.type + "-org").value, "MOBISTAR"))
				jq_show("control-create-subnet");
		}
	};
	
	this.changeSubnetArea = function()
	{

	};
	
	this.changeGOO_sel = function()
	{
		var type = this.type;
		var isInList = doAutocompleteOnValueChange
		(
			type + "-selector", 
			getE(type + "-selector").value,
			{
				"type":type, 
				"element":"goo", 
				"name":getE(type + '-selector').value,
				"ts_name":getE('application-service').value,
				"org":getSelectE(type + '-org').value
			}, 
			function(request)
			{
				setSelectEC(type + "-sel-env", getEnvProposition(request['json'].environment));
			}
		);
	};
	
	this.clearInfo = function(option_delete)
	{
		if (this.type != "srv")
		{
			/** do clear iff doAutocompleteOnValueChange is called **/
			if (option_delete == 1) //delete host values
			{
				var fields = [/*'-host','-ip'*/,'-ip-tsl'];
				for (var f_idx in fields)
					getE(this.type + fields[f_idx]).value = "";
					
				jq_hide(this.type + "-ip-check");
				jq_hide(this.type + "-ip-error");
				jq_hide(this.type + "-ip-warning");
				hide("warning-error-tr-" + this.type + "-ip-opt");
				getE(this.type + "-ip-code").value = "";					
			}
			else if(option_delete == 2) //delete subnet values
			{
				getE(this.type + "-subnet-area").value = "";
			}
			else if(option_delete == 3) //delete goo values
			{
				setSelectE(this.type + "-goo-env", default_choice);
			}
		}
		else
		{
			if (option_delete == 1) //delete srv-one values
			{
				getE(this.type + "-srv-port").value = "";				
				setSelectE(this.type + "-srv-proto", default_choice);
				this.changeProto();
			}
			else if(option_delete == 2) //delete srv-gsrv values
			{
			}
		}
	};
	
	/**
		GSRV functions
	**/
	this.changeGroupOfServices = function()
	{
		var id = "gsrv-name";
		this.clear(1);		
		
		if (isEmpty(getE(id).value))
			jq_hide("row-gsrv-action");
		else if (isInSuggestionList(id, getE(id).value))
		{
			getMembers("gsrv", "gsrv-srv");
			/** use id to display action row **/
			show("row-gsrv-action");
			show("row-gsrv-rename");
			show("row-gsrv-update");
			show("row-gsrv-delete");
		}
		else
		{
			clearMembers("gsrv-srv");
			show("row-gsrv-action");
			show("row-gsrv-create");
			checkRadio(this.type + "-action-create");
			this.change_gsrv_action();
		}
	};
	
	this.change_gsrv_action = function()
	{
		this.clear(2);
		jq_show("row-" + this.type + "-" + getRadio(this.type + "-action").value);
		
		if (!isEmpty(getRadio(this.type + "-action").value) & !isX(getRadio(this.type + "-action").value, "create"))
		{
			show("gsrv-show-list-member");
			this.add_cat_element_group_member_in_use();
		}
		else
			hide("gsrv-show-list-member");
	}
	
	this.changeService = function()
	{
		var type 	= this.type;
		var name 	= type + '-srv-name';
		this.clear(3);
		var isInList = doAutocompleteOnValueChange
		(
			name, 
			getE(name).value,
			{"element":"service", "name":getE(name).value, "application-service":getE('application-service').value}, 
			function(request)
			{
				getE(name).value 				= request['json'].name;
				getE(type + '-srv-proto').value = request['json'].protocol;
				getE(type + '-srv-type').value 	= request['json'].type;
				getE(type + '-srv-port').value 	= request['json'].port;
			}
		)

		if (isInList)
			jq_disabled("srv-autocomplete-rows");
		else
			jq_enabled("srv-autocomplete-rows");
	};
	
	this.changeProto = function()
	{
		if (isX(getSelectE(this.type + "-srv-proto").value, "icmp"))
		{
			hide("row-gsrv-srv-port");
			getE("gsrv-srv-port").value = "-";			
		}
		else
		{
			show("row-gsrv-srv-port");
			getE("gsrv-srv-port").value = "";
		}
	};
	
	return this.initialize(name, type);
}