function toggle_detail(id) 
{
	tr = document.getElementById("detail_" + id);
	if (! tr) return;
 
	img_more = document.getElementById("img_more_" + id);
	img_less = document.getElementById("img_less_" + id);
 
	if (tr.style.display == 'none') 
	{
		tr.style.display       = '';
		img_more.style.display = 'none';
		img_less.style.display = '';
	}
	else 
	{
		tr.style.display       = 'none';
		img_more.style.display = '';
		img_less.style.display = 'none';
	}
}

function toggle_search_detail(id) 
{
	tr = document.getElementById("search_detail_" + id);
	if (! tr) return;
 
	img_more = document.getElementById("img_more_search_" + id);
	img_less = document.getElementById("img_less_search_" + id);
 
	if (tr.style.display == 'none') 
	{
		tr.style.display       = '';
		img_more.style.display = 'none';
		img_less.style.display = '';
	}
	else
	{
		tr.style.display       = 'none';
		img_more.style.display = '';
		img_less.style.display = 'none';
	}
}

function displayJsonRequest(jsonObject, toDisplayifEmpty)
{
	var nbResults = 0;
	for (var requestSummary in jsonObject.requestSummary) 
	{
		nbResults = nbResults + 1;
	}
	if (nbResults > 0)
	{
		var html = "<table>";		 		
		html += "	<tr>";
		html += "		<th></th>";
		html += "		<th>ID</th>";
		html += "		<th>Detail</th>";
		html += "		<th>Requestor</th>";
		html += "		<th>Date</th>";
		html += "		<th>Task</th>";
		html += "		<th></th>";
		html += "	</tr>";
		
		
		var i = 0;
		for (var requestSummary in jsonObject.requestSummary) 
		{
			var id = jsonObject.requestSummary[i].id;
			var detail = jsonObject.requestSummary[i].detail;
			var requestor = jsonObject.requestSummary[i].requestor;
			var date = jsonObject.requestSummary[i].date;
			var status = jsonObject.requestSummary[i].status;
			var label_status = jsonObject.requestSummary[i].label_status;
			var form_mode = jsonObject.requestSummary[i].form_mode;
			
			if (i%2 == 0)
			{
				html += "	<tr class=\"row_a\">";
			}
			else
			{
				html += "	<tr class=\"row_b\">";
			}
			
			var form_type = "fcr-form";
			if (form_mode.indexOf("maintain") != -1)
			{
				form_type = "fcr-maintain-form";
			}
			
			html += "		<td  rowspan=\"1\" onClick=\"toggle_detail('"+ id +"')\"><img src=\"ressources/more.gif\" id=\"img_more_"+ id +"\" border=\"0\"><img src=\"ressources/less.gif\" id=\"img_less_"+id+"\" border=\"0\" style=\"display: none;\"></td>";
			html += "		<td>"+ id +"</td>";
			html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('/fcrweb/jsp/"+form_type+".jsp?fid="+id+"', '_blank', 'scrollbars=1,resizable=yes, width=1050, height=700')\" >" +detail+ "</a></td>";
			html += "		<td>"+requestor+"</td>";
			html += "		<td>"+date+"</td>";
			html += "		<td class=\""+ status.toLowerCase().replace(' ', '_') +"\">" + label_status +"</td>";
			if (status == "Closure")
			{
				html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('/fcrweb/forms.htm?pr_form_mode=dup&pr_dup_form_id="+id+"', '_blank', 'scrollbars=1,resizable=yes, width=1050, height=700')\"><img src=\"ressources/element_add.png\" alt=\"Duplicate FCR\" title=\"Duplicate FCR\" width=\"25\"></a></td>";
			}
			html += "	</tr>";	 			
			html += "	<tr style=\"display: none;\" id=\"detail_"+id+"\" class=\"row_even\">";
			html += "		<td colspan=\"5\" class=\"history\">";
			html += "			<table class=\"history\">";
			html += "				<tr>";
			html += "					<td class=\"header h_task\">Task</td>";
			html += "					<td class=\"header h_user\">User</td>";
			html += "					<td class=\"header h_date\">Date</td>";
			html += "					<td class=\"header h_com\">Comment</td>";
			html += "				</tr>";
			
			var j = 0;
			for (var t in jsonObject.requestSummary[i].tasks) 
			{
				var task = jsonObject.requestSummary[i].tasks[j].task;
				var label_task = jsonObject.requestSummary[i].tasks[j].label_task;
				var user = jsonObject.requestSummary[i].tasks[j].user;
				var date = jsonObject.requestSummary[i].tasks[j].date;
				var comment = jsonObject.requestSummary[i].tasks[j].comment;
				
				html += "				<tr>";
				html += "					<td class=\""+ task.toLowerCase().replace(' ', '_') +"\">"+ label_task +"</td>";
				html += "					<td class=\"history\">"+ user +"</td>";
				html += "					<td class=\"history\">"+ date +"</td>";
				html += "					<td class=\"history comment\">"+ comment +"</td>";
				html += "				</tr>";	 			
				
				j = j+1;
			}
			html += "			</table>";
			html += "		</td>";
			html += "	</tr>";
			
			i = i+1;	 			
		}
		html += "</table>";
	}
	else
	{
		html = toDisplayifEmpty;
	}
	
	return html;
}

function displayJsonRule(jsonObject, toDisplayifEmpty)
{
	var nbResults = 0;
	for (var ruleSummary in jsonObject.ruleSummary) 
	{
		nbResults = nbResults + 1;
	}
	if (nbResults > 0)
	{
		var html = "<input id=\"button0\" style=\"display: none;\" class=\"button\" value=\"Edit Rule(s)\" type=\"button\" onClick=\"editSelectedRulesInSearch()\" />";
		html += "<table style=\"width:98%;word-wrap:break-word;table-layout:fixed;\">";		
		html += "	<tr>";
		html += "		<th style=\"width:4%;\" class=\"first\"></th>";
		html += "		<th style=\"width:10%;\">Name</th>";
		html += "		<th style=\"width:32%;\">Source</th>";
		html += "		<th style=\"width:27%;\">Destination</th>";
		html += "		<th style=\"width:18%;\">Service</th>";
		html += "		<th style=\"width:9%;\">Action</th>";
		html += "	</tr>";
		
		
		var i = 0;
		for (var ruleSummary in jsonObject.ruleSummary) 
		{
			var name = jsonObject.ruleSummary[i].name;
			var id = jsonObject.ruleSummary[i].id;
			var action = jsonObject.ruleSummary[i].action;
			var active = jsonObject.ruleSummary[i].active;
			var technicalService = jsonObject.ruleSummary[i].technicalService;
			
			if (i%2 == 0)
			{
				html += "	<tr class=\"row-a\">";
			}
			else
			{
				html += "	<tr class=\"row-b\">";
			}
			html += "		<td class=\"first\"><input type=\"checkbox\" id=\"check"+id+"\" onclick=\"ruleCheckedInSearch("+id+", 0, '"+technicalService+"');\"/></td>";
			if (active == "true")
			{
				html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?id="+id+"', '_blank', 'resizable=yes, scrollbars=1, width=700, height=500')\">"+ name +"</a></td>";
			}
			else
			{
				html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?id="+id+"', '_blank', 'resizable=yes, scrollbars=1, width=700, height=500')\"><strike>"+ name +"</strike></a></td>";
			}
			
			var source = "";
			var j=0;
			for (var srcRuleObjectSummary in jsonObject.ruleSummary[i].source)
			{
				var src_id = jsonObject.ruleSummary[i].source[j].id;
				var src_name = jsonObject.ruleSummary[i].source[j].name;
				var src_type = jsonObject.ruleSummary[i].source[j].type;
				var src_profile = jsonObject.ruleSummary[i].source[j].profile;
				var src_predefined = jsonObject.ruleSummary[i].source[j].predefined;
			
				if (source != "")
				{
					source = source + "<br>";
				}
				var temp_result = "<img src='ressources/mob_icons/"+src_type+".png'>";
				if (src_predefined != "unknown")
				{
					var glocal = "local";
					if (src_predefined == "1")
					{
						glocal = "global";
					}
					temp_result += "<img src='ressources/mob_icons/"+glocal+".png'>";
				}
				if (src_profile == "")
				{
					temp_result += "&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + src_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + src_name + "</a>";
				}
				else
				{
					temp_result += "&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?profile_name=" + src_profile + "', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">" + src_profile + "</a>@<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + src_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + src_name + "</a>";
				}
				
				source += temp_result;
				
				j=j+1;
			}
			
			var destination = "";
			j=0;
			for (var dstRuleObjectSummary in jsonObject.ruleSummary[i].destination)
			{
				var dst_id = jsonObject.ruleSummary[i].destination[j].id;
				var dst_name = jsonObject.ruleSummary[i].destination[j].name;
				var dst_type = jsonObject.ruleSummary[i].destination[j].type;				
				var dst_predefined = jsonObject.ruleSummary[i].destination[j].predefined;

				if (destination != "")
				{
					destination = destination + "<br>";
				}
				var temp_result = "<img src='ressources/mob_icons/"+dst_type+".png'>";
				if (dst_predefined != "unknown")
				{
					var glocal = "local";
					if (dst_predefined == "1")
					{
						glocal = "global";
					}
					temp_result += "<img src='ressources/mob_icons/"+glocal+".png'>";
				}

				temp_result += "&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + dst_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + dst_name + "</a>";


				destination += temp_result;

				j=j+1;
			}

			var service = "";
			j=0;
			for (var svcRuleObjectSummary in jsonObject.ruleSummary[i].service)
			{
				var svc_id = jsonObject.ruleSummary[i].service[j].id;
				var svc_name = jsonObject.ruleSummary[i].service[j].name;
				var svc_type = jsonObject.ruleSummary[i].service[j].type;				
				var svc_predefined = jsonObject.ruleSummary[i].service[j].predefined;
								
				if (service != "")
				{
					service = service + "<br>";
				}
				var temp_result = "<img src='ressources/mob_icons/"+svc_type+".png'>";
				if (svc_predefined != "unknown")
				{
					var glocal = "local";
					if (svc_predefined == "1")
					{
						glocal = "global";
					}
					temp_result += "<img src='ressources/mob_icons/"+glocal+".png'>";
				}
				
				temp_result += "&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + svc_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + svc_name + "</a>";
								
				service += temp_result;
				
				j=j+1;
			}
			
			html += "		<td>"+ source +"</td>";
			html += "		<td>"+ destination +"</td>";
			html += "		<td>"+ service +"</td>";
			html += "		<td><img src='ressources/mob_icons/"+action.replace(' ', '').toLowerCase() +".png'>&nbsp;"+ action +"</td>";
			html += "	</tr>";
			i=i+1;
		}
		html+= "</table>";
	}
	else
	{
		html = toDisplayifEmpty;
	}	
	return html;
}

function displayJsonObject(jsonObject, toDisplayifEmpty)
{
	var nbResults = 0;
	for (var objectSummary in jsonObject.objectSummary) 
	{
		nbResults = nbResults + 1;
	}
	if (nbResults > 0)
	{
		var html = "<table >";
		html += "	<tr>";
		html += "		<th >Name</th>";
		html += "		<th >Description</th>";
		html += "	</tr>";
		
		var i = 0;
		for (var objectSummary in jsonObject.objectSummary) 
		{
			var name = jsonObject.objectSummary[i].name;
			var id = jsonObject.objectSummary[i].id;
			var description = jsonObject.objectSummary[i].description;
			
			if (i%2 == 0)
			{
				html += "	<tr class=\"row-a\">";
			}
			else
			{
				html += "	<tr class=\"row-b\">";
			}
			html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('objects.htm?id="+id+"&type=group', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">"+ name +"</a></td>";
			html += "		<td>"+ description +"</td>";
			html += "	</tr>";
			i=i+1;
		}
		html+= "</table>";
	}
	else
	{
		html = toDisplayifEmpty;
	}
	
	return html;
}


function displayJsonService(jsonObject, toDisplayifEmpty)
{
	var nbResults = 0;
	for (var serviceSummary in jsonObject.serviceSummary) 
	{
		nbResults = nbResults + 1;
	}
	if (nbResults > 0)
	{
		var html = "<table >";
		html += "	<tr>";
		html += "		<th >Name</th>";
		html += "		<th >Protocol</th>";
		html += "		<th >Port</th>";
		html += "	</tr>";
		
		var i = 0;
		for (var serviceSummary in jsonObject.serviceSummary) 
		{
			var name = jsonObject.serviceSummary[i].name;
			var protocol = jsonObject.serviceSummary[i].protocol;
			var port = jsonObject.serviceSummary[i].port;
			
			if (i%2 == 0)
			{
				html += "	<tr class=\"row-a\">";
			}
			else
			{
				html += "	<tr class=\"row-b\">";
			}
			html += "		<td><a href=\"javascript:void(0)\">"+ name +"</a></td>";
			html += "		<td>"+ protocol +"</td>";
			html += "		<td>"+ port +"</td>";
			html += "	</tr>";
			i=i+1;
		}
		html+= "</table>";
	}
	else
	{
		html = toDisplayifEmpty;
	}
	
	return html;
}

function displayJsonServiceGroup(jsonObject, toDisplayifEmpty)
{
	var nbResultsGroups = 0;
	var html = "";
	for (var serviceGroupSummary in jsonObject.serviceGroupSummary) 
	{
		nbResultsGroups = nbResultsGroups + 1;
	}	
	if (nbResultsGroups > 0)
	{
		html = "<table >";
		html += "	<tr>";
		html += "		<th >Name</th>";
		html += "		<th >Description</th>";
		html += "	</tr>";
		
		var i = 0;
		for (var serviceGroupSummary in jsonObject.serviceGroupSummary) 
		{
			var name = jsonObject.serviceGroupSummary[i].name;
			var description = jsonObject.serviceGroupSummary[i].description;
			var id = jsonObject.serviceGroupSummary[i].id;
			
			if (i%2 == 0)
			{
				html += "	<tr class=\"row-a\">";
			}
			else
			{
				html += "	<tr class=\"row-b\">";
			}
			html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('services.htm?id="+id+"&type=group', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">"+ name +"</a></td>";
			html += "		<td>"+ description +"</td>";
			html += "	</tr>";
			i=i+1;
		}
		html+= "</table>";
	}
	
	
	var nbResultsServices = 0;
	for (var serviceSummary in jsonObject.serviceSummary) 
	{
		nbResultsServices = nbResultsServices + 1;
	}	
	if (nbResultsServices > 0)
	{
		html += "<table >";
		html += "	<tr>";
		html += "		<th >Name</th>";
		html += "		<th >Protocol</th>";
		html += "		<th >Port</th>";
		html += "	</tr>";
		
		var i = 0;
		for (var serviceSummary in jsonObject.serviceSummary) 
		{
			var name = jsonObject.serviceSummary[i].name;
			var protocol = jsonObject.serviceSummary[i].protocol;
			var id = jsonObject.serviceSummary[i].id;
			var port = jsonObject.serviceSummary[i].port;
			
			if (i%2 == 0)
			{
				html += "	<tr class=\"row-a\">";
			}
			else
			{
				html += "	<tr class=\"row-b\">";
			}
			html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('services.htm?id="+id+"&type=other', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">"+ name +"</a></td>";
			html += "		<td>"+ protocol +"</td>";
			html += "		<td>"+ port +"</td>";
			html += "	</tr>";
			i=i+1;
		}
		html+= "</table>";
	}
	
	if (nbResultsServices == 0 && nbResultsGroups == 0)
	{
		html = toDisplayifEmpty;
	}
	
	return html;
}

function displayJsonProfile(jsonObject, toDisplayifEmpty)
{
	var nbResults = 0;
	for (var profileSummary in jsonObject.profileSummary) 
	{
		nbResults = nbResults + 1;
	}
	if (nbResults > 0)
	{
		var html = "<table >";		
		html += "	<tr>";
		html += "		<th >Name</th>";
		html += "		<th >Description</th>";
		html += "	</tr>";
		
		var i = 0;
		for (var profileSummary in jsonObject.profileSummary) 
		{
			var id = jsonObject.profileSummary[i].id;
			var name = jsonObject.profileSummary[i].name;
			var description = jsonObject.profileSummary[i].description;
			
			if (i%2 == 0)
			{
				html += "	<tr class=\"row-a\">";
			}
			else
			{
				html += "	<tr class=\"row-b\">";
			}
			html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('profiles.htm?id="+id+"', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">"+ name +"</a></td>";
			html += "		<td>"+ description +"</td>";
			html += "	</tr>";
			i=i+1;
		}
		html+= "</table>";
	}
	else
	{
		html = toDisplayifEmpty;
	}
	
	return html;
}

function displayJsonRuleConflictObjects(jsonObject)
{		
	var html = "<table>";		
	html += "	<tr>";
	html += "		<th>Rule</th>";
	html += "		<th>Object</th>";
	html += "		<th>Type</th>";
	html += "		<th>Problem</th>";
	html += "	</tr>";
	
	var i = 0;
	for (var ruleConflictObjects in jsonObject.ruleConflictObjects) 
	{
		var rule_name = jsonObject.ruleConflictObjects[i].ruleName;
		var j = 0;
		for (var object in jsonObject.ruleConflictObjects[i].conflictObjects) 
		{
			var id = jsonObject.ruleConflictObjects[i].conflictObjects[j].id;
			var object_name = jsonObject.ruleConflictObjects[i].conflictObjects[j].name;
			var object_type = jsonObject.ruleConflictObjects[i].conflictObjects[j].type;
			var technical_service = jsonObject.ruleConflictObjects[i].conflictObjects[j].technicalService;
		
			html += "	<tr >";
			html += "		<td>"+rule_name+"</td>";
			if (object_type == "Object Group")
			{
				html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('objects.htm?id="+id+"&type=group', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">"+object_name+"</a></td>";
			}
			else if (object_type == "Service Group")
			{
				html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('services.htm?id="+id+"&type=group', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">"+object_name+"</a></td>";
			}
			else if (object_type == "Service")
			{
				html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('services.htm?id="+id+"&type=service', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">"+object_name+"</a></td>";
			}
			else if (object_type == "Rule")
			{
				html += "		<td><a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?id="+id+"', '_blank', 'resizable=yes, scrollbars=1, width=700, height=500')\">"+object_name+"</a></td>";
			}
			html += "		<td>"+object_type+"</td>";
			html += "		<td>assigned to: "+technical_service+"</td>";
			html += "	</tr>";
			j=j+1;
		}
		i=i+1;
	}
	html+= "</table>";

	return html;
}

function displayJsonChangeLog(jsonObject, toDisplayifEmpty, expanded)
{
	var nbResults = 0;
	for (var changeLogSummary in jsonObject.changeLogSummary) 
	{
		nbResults = nbResults + 1;
	}
	if (nbResults > 0)
	{
		var html = "<table >";		
		html += "	<tr>";
		html += "		<th >Date</th>";
		html += "		<th >Change</th>";
		html += "		<th >Source</th>";
		html += "		<th >Destination</th>";
		html += "		<th >Service</th>";
		html += "		<th >Action</th>";
		html += "	</tr>";
		
		
		var i = 0;
		for (var changeLogSummary in jsonObject.changeLogSummary) 
		{
			var date = jsonObject.changeLogSummary[i].date;
			var change = jsonObject.changeLogSummary[i].change;
			var src_existing = "";
			var src_new = "";
			var src_removed = "";
			var dst_existing = "";
			var dst_new = "";
			var dst_removed = "";
			var svc_existing = "";
			var svc_new = "";
			var svc_removed = "";
			
			if (expanded == "false")
			{
				src_existing = jsonObject.changeLogSummary[i].src_existing;
				src_new = jsonObject.changeLogSummary[i].src_new;
				src_removed = jsonObject.changeLogSummary[i].src_removed;
				dst_existing = jsonObject.changeLogSummary[i].dst_existing;
				dst_new = jsonObject.changeLogSummary[i].dst_new;
				dst_removed = jsonObject.changeLogSummary[i].dst_removed;
				svc_existing = jsonObject.changeLogSummary[i].svc_existing;
				svc_new = jsonObject.changeLogSummary[i].svc_new;
				svc_removed = jsonObject.changeLogSummary[i].svc_removed;
			}
			else
			{
				src_existing = jsonObject.changeLogSummary[i].expanded_src_existing;
				src_new = jsonObject.changeLogSummary[i].expanded_src_new;
				src_removed = jsonObject.changeLogSummary[i].expanded_src_removed;
				dst_existing = jsonObject.changeLogSummary[i].expanded_dst_existing;
				dst_new = jsonObject.changeLogSummary[i].expanded_dst_new;
				dst_removed = jsonObject.changeLogSummary[i].expanded_dst_removed;
				svc_existing = jsonObject.changeLogSummary[i].expanded_svc_existing;
				svc_new = jsonObject.changeLogSummary[i].expanded_svc_new;
				svc_removed = jsonObject.changeLogSummary[i].expanded_svc_removed;
			}
			var action = jsonObject.changeLogSummary[i].action;
			
			if (i%2 == 0)
			{
				html += "	<tr class=\"row-a\">";
			}
			else
			{
				html += "	<tr class=\"row-b\">";
			}
			html += "		<td>"+ date +"</td>";
			html += "		<td>"+ change +"</td>";
			html += "		<td>"+ src_new.replace(/doublequote/g , "\"") + src_existing.replace(/doublequote/g , "\"") + src_removed.replace(/doublequote/g , "\"") +"</td>";
			html += "		<td>"+ dst_new.replace(/doublequote/g , "\"") + dst_existing.replace(/doublequote/g , "\"") + dst_removed.replace(/doublequote/g , "\"") +"</td>";
			html += "		<td>"+ svc_new.replace(/doublequote/g , "\"") + svc_existing.replace(/doublequote/g , "\"") + svc_removed.replace(/doublequote/g , "\"") +"</td>";
			html += "		<td>"+ action +"</td>";
			html += "	</tr>";
			i=i+1;
		}
		html+= "</table>";
	}
	else
	{
		html = toDisplayifEmpty;
	}
	return html;
}

var rules = [];
var ts = [];
function ruleCheckedInSearch(rule_id, list_id, technical_service)
{	
	document.getElementById("button"+list_id).style.display = "block";
	
	if (!Array.prototype.indexOf)
	{
	  Array.prototype.indexOf = function(elt /*, from*/)
	  {
	    var len = this.length >>> 0;
	    
	    var from = Number(arguments[1]) || 0;
	    from = (from < 0)
	         ? Math.ceil(from)
	         : Math.floor(from);
	    if (from < 0)
	      from += len;

	    for (; from < len; from++)
	    {
	      if (from in this &&
	          this[from] === elt)
	        return from;
	    }
	    return -1;
	  };
	}	
	
	var exist = rules.indexOf(rule_id);
		
	if (exist == -1)
	{
		var conflict = false;
		
		for (var i = 0; i < ts.length; i++)
		{
			if (ts[i] != technical_service)
			{
				conflict = true;
			}
		}
		if (!conflict)
		{
			rules.push(rule_id);
			ts.push(technical_service);
		}
		else
		{
			alert("You cannot select rules from different technical services");
			document.getElementById("check"+rule_id).checked = false;
		}
	}
	else
	{
		rules.splice(exist, 1);
		ts.splice(exist, 1);
		if (rules.length == 0)
		{
			document.getElementById("button"+list_id).style.display = "none";
		}
	}
}

function editSelectedRulesInSearch()
{
	var rules_final = "";
	var and = "";
	for (var i = 0; i < rules.length; i++)
	{
		rules_final += and + rules[i];
		and = ",";
	}
	var technical_service = ts[0];
	window.open('forms.htm?pr_form_mode=rule&pr_maintain_ids='+rules_final +'&pr_ts_name='+filter(technical_service), '_blank', 'resizable=yes,scrollbars=1, width=1050, height=700');
}
