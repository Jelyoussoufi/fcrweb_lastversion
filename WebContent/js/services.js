var xhr;
var xhr2;

function getSearchResult()
{	
	var searchValue = document.getElementById("search_value").value;
	if (searchValue.length > 1)
	{
		var html = "<h2>My Search Result</h2><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search in progress ... <img src=\"ressources/progress.gif\"/><br><br>";
		document.getElementById("SearchResult").innerHTML = html;
		document.getElementById("search_value").value = "";
		xhr = new XMLHttpRequest();
		xhr.open('GET', 'services.htm?action=getSearchResult&searchValue=' + searchValue + '&ms=' + new Date().getTime() , true);
		xhr.onreadystatechange = function() { displaySearchResult(); } ;
		xhr.send(null);
	}
	else
	{
		alert("Search Value must be at least 3 characters");
	}
}

function displaySearchResult()
{
	if (xhr.readyState == 4) //call is completed true
	 {
	 	if(xhr.status == 200) //http request is successfull
		{
	 		var requests = xhr.responseText;
	 		var jsonObject = JSON.parse(requests);
	 		
	 		var html = displayJsonServiceGroup(jsonObject, "<p>No results are matching your search criteria</p>");	 		
	 		document.getElementById("SearchResult").innerHTML = "<h2>My Search Result <a href=\"javascript:void(0)\" onClick=\"hideSearchResult()\">(hide)</a></h2>" + html;
		}
		else
		{
	 		alert("Error ---> \n"+ xhr.status + " : " + xhr.statusText);
	 	}
	 }
}

function hideSearchResult()
{
	document.getElementById("SearchResult").innerHTML = "";
}

function setServiceAsPredefined(service_id, type)
{
	if (confirm("Are you sure you want to define this service as predefined?"))
	{
		xhr2 = new XMLHttpRequest();
		xhr2.open('GET', 'services.htm?action=setPredefined&id=' + service_id + '&type='+type+'&ms=' + new Date().getTime() , true);
		xhr2.onreadystatechange = function() { serviceCorrectlyPredefined(); } ;
		xhr2.send(null);
	}	
}

function serviceCorrectlyPredefined()
{
	if (xhr2.readyState == 4) //call is completed true
	{
	 	if(xhr2.status == 200) //http request is successfull
		{
	 		var response = xhr2.responseText;
	 		if (response =="ok")
 			{
	 			document.getElementById("predefined").checked = true;
 			}
	 		else
	 		{
	 			alert("Service cannot be set as predefined since it has some members that are not predefined");
	 		}
		}
		else
		{
	 		alert("Error ---> \n"+ xhr2.status + " : " + xhr2.statusText);
	 	}
	}	
}

function serviceChecked(service_id, list_id)
{
	if (!Array.prototype.indexOf)
	{
	  Array.prototype.indexOf = function(elt /*, from*/)
	  {
	    var len = this.length >>> 0;
	    
	    var from = Number(arguments[1]) || 0;
	    from = (from < 0)
	         ? Math.ceil(from)
	         : Math.floor(from);
	    if (from < 0)
	      from += len;

	    for (; from < len; from++)
	    {
	      if (from in this &&
	          this[from] === elt)
	        return from;
	    }
	    return -1;
	  };
	}	
	var exist = listTab[list_id].indexOf(service_id);
	document.getElementById("button"+list_id).style.display = "block";
	
	if (exist == -1)
	{		
		listTab[list_id].push(service_id);
	}
	else
	{
		listTab[list_id].splice(exist, 1);
		if (listTab[list_id].length == 0)
		{
			document.getElementById("button"+list_id).style.display = "none";
		}
	}
}

function editSelectedServices(list_id, ts_name)
{
	if (listTab[list_id].length == 0)
	{
		alert("At least one service must be selected");
	}
	else
	{
		var serviceList = "";
		var first = true;
		for (var i = 0; i < listTab[list_id].length; i++)
		{
			if (first)
			{
				first = false;
				serviceList = listTab[list_id][i];
			}
			else
			{
				serviceList = serviceList + "," + listTab[list_id][i];
			}			
		}		
		
		window.open('forms.htm?pr_form_mode=maintain-group-services&pr_maintain_ids='+serviceList +'&pr_ts_name='+filter(ts_name), '_blank', 'resizable=yes,scrollbars=1, width=1050, height=700');
	}
	
}
