var xhr_rule;
var xhr_ts;
var xhr_ts_update;

function assignTechnicalService(force_assign)
{
	var technical_service = document.getElementById("technical_service_name").value;
	var rule = document.getElementById("rule_name").value;
	if (technical_service.length == 0 || rule.length == 0)
	{
		alert("Rule Name and Technical Service must be filled");
	}
	else
	{
		document.getElementById("object_conflicts").innerHTML = "";
		document.getElementById("load_img").style.visibility = "visible";
		xhr_rule = new XMLHttpRequest();
		var params = 'admin.htm?id=1&service='+technical_service+'&rule='+rule+'&force='+force_assign+'&ms=' + new Date().getTime();
		params = params.replace(" ", "%20");
		xhr_rule.open('GET', params , true);
		xhr_rule.onreadystatechange = function() { assignmentDone(); } ;
		xhr_rule.send(null);		
	}
}

function assignmentDone()
{
	if (xhr_rule.readyState == 4) //call is completed true
	 {
	 	if(xhr_rule) //http request is successfull
		{
	 		document.getElementById("load_img").style.visibility = "hidden";
	 		var response = xhr_rule.responseText;	 		
	 		if (response == "ok")
 			{
	 			document.getElementById("object_conflicts").innerHTML = "<p style=\"font-size:18px; color:green;\">Assignment done WITHOUT conflicts</p>";
 			}
	 		else
 			{
	 			if (response != "Not Existing")
	 			{
		 			var jsonObject = JSON.parse(response);
		 			var divHtml = "<p style=\"font-size:18px; color:red;\">Conflicts:</p><br>";
		 			divHtml = divHtml + displayJsonRuleConflictObjects(jsonObject);
		 			divHtml = divHtml + "<center><input id=\"force_assign\" class=\"button\" value=\"Force assign\" type=\"button\" onClick=\"assignTechnicalService('true')\" /></center>";
		 			document.getElementById("object_conflicts").innerHTML = divHtml;
	 			}
	 			else
	 			{
	 				alert("Entered rule or Technical Service does not exist");
	 			}
 			}
		}
		else
		{
	 		alert("Error ---> \n"+ xhr_rule.status + " : " + xhr_rule.statusText);
	 	}
	 }
}

function addRuleToTS()
{	
	var selectfrom = document.getElementById("selectfrom");
	var selectto = document.getElementById("selectto");
	for (var i = selectfrom.length-1; i >= 0; i--) 
	{ 
		if (selectfrom.options[i].selected)
		{
			var newOption=document.createElement("option");
			newOption.text=selectfrom.options[i].text;
			selectto.add(newOption, null);
			selectfrom.remove(i);
		}
    }
}

function removeRuleFromTS()
{
	var selectfrom = document.getElementById("selectfrom");
	var selectto = document.getElementById("selectto");
	for (var i = selectto.length-1; i >= 0; i--) 
	{ 
		if (selectto.options[i].selected)
		{
			var newOption=document.createElement("option");
			newOption.text=selectto.options[i].text;
			selectfrom.add(newOption, null);
			selectto.remove(i);
		}
    }
}

function manageTechnicalService()
{	
	var technical_service = document.getElementById("technical_service").value;
	if (technical_service.length == 0)
	{
		alert("Technical Service must be filled");
	}
	else
	{
		document.getElementById("load_img_second").style.visibility = "visible";
		document.getElementById("object_conflicts").innerHTML = "";
		document.getElementById("technical_service_name_hidden").value = technical_service;
		xhr_ts = new XMLHttpRequest();
		var params = 'admin.htm?id=1&service='+technical_service+'&ms=' + new Date().getTime();
		params = params.replace(" ", "%20");
		xhr_ts.open('GET', params , true);
		xhr_ts.onreadystatechange = function() { showManagementPanel(); } ;
		xhr_ts.send(null);		
	}	
}

function showManagementPanel()
{
	if (xhr_ts.readyState == 4) //call is completed true
	 {
	 	if(xhr_ts.status == 200) //http request is successfull
		{				 
	 		var requests = xhr_ts.responseText;	 		
	 		var jsonObject = JSON.parse(requests);
	 		
	 		var selectfrom = document.getElementById("selectfrom");
			var selectto = document.getElementById("selectto");
			
			while (selectfrom.options.length > 0) {
				selectfrom.remove(0);
			}
			while (selectto.options.length > 0) {
				selectto.remove(0);
			}
			
	 		var assignedRules = jsonObject.assignedRules.split(",");
	 		for (var i = 0; i < assignedRules.length; i++)
 			{
	 			if (assignedRules[i] != "")
	 			{
		 			var newOption=document.createElement("option");
					newOption.text=assignedRules[i];
					selectto.add(newOption, null);
	 			}
 			}
	 		
	 		var unassignedRules = jsonObject.unassignedRules.split(",");
	 		for (var i = 0; i < unassignedRules.length; i++)
 			{
	 			if (unassignedRules[i] != "")
	 			{
		 			var newOption=document.createElement("option");
					newOption.text=unassignedRules[i];
					selectfrom.add(newOption, null);
	 			}
 			}
	 		document.getElementById("load_img_second").style.visibility = "hidden";
	 		document.getElementById("managementPanel").style.display = "block";
		}
		else
		{
	 		alert("Error ---> \n"+ xhr_ts.status + " : " + xhr_ts.statusText);
	 	}
	 }			
}

function saveConfiguration(force_assign)
{	
	var selectto = document.getElementById("selectto");
	var updatedRules = "";
	var first = "true";
	for (var i = 0; i < selectto.length; i++) 
	{
		if (first == "true")
		{
			first = "false";
			updatedRules = selectto.options[i].text;
		}
		else
		{
			updatedRules += "," + selectto.options[i].text;
		}
    }
	var technical_service = document.getElementById("technical_service_name_hidden").value;
	var params = "admin.htm?id=1&service="+technical_service+"&list="+updatedRules+"&force="+force_assign+"&ms=" + new Date().getTime();
	params = params.replace(" ", "%20");
	xhr_ts_update = new XMLHttpRequest();
	xhr_ts_update.open('GET', params , true);
	xhr_ts_update.onreadystatechange = function() { updateDone(); } ;
	xhr_ts_update.send(null);
	
}

function updateDone()
{
	if (xhr_ts_update.readyState == 4) //call is completed true
	 {
	 	if(xhr_ts_update) //http request is successfull
		{
	 		var response = xhr_ts_update.responseText;	 		
	 		if (response == "ok")
 			{
	 			document.getElementById("object_conflicts").innerHTML = "<p style=\"font-size:18px; color:green;\">Update done WITHOUT conflicts</p>";
 			}
	 		else
 			{
	 			var jsonObject = JSON.parse(response);
	 			var divHtml = "<p style=\"font-size:18px; color:red;\">Conflicts:</p><br>";
	 			divHtml = divHtml + displayJsonRuleConflictObjects(jsonObject);
	 			divHtml = divHtml + "<center><input id=\"force_assign\" class=\"button\" value=\"Force assign\" type=\"button\" onClick=\"saveConfiguration('true')\" /></center>";
	 			document.getElementById("object_conflicts").innerHTML = divHtml;
 			}
		}
		else
		{
	 		alert("Error ---> \n"+ xhr_ts_update.status + " : " + xhr_ts_update.statusText);
	 	}
	 }
}

function checkRule()
{
	var ruleField = document.getElementById("rule_name");
	var rulename = getUrlParam("rule");
	ruleField.value = rulename.replace(/%20/g, " ");
}
		
function getUrlParam(key, default_) 
{
    if (default_==null) default_="";
    key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if(qs == null) return default_; else return qs[1];
}