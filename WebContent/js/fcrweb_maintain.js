var mtn_groups;
var  ext_remove = " - remove";
var  ext_use 	= " - use";
		
function mtn(uri){return "maintain/" + uri;}

function onload_maintain()
{
	hide("tr-on-behalf-of");
	checkDebugTool();
	setFormHeight("mtn");
	atCelsius = getE("atCelsius").value == "true";	
	buildHeaderAttributeMap();
	getLdapUsers("on-behalf-of");
	get_list_application_service(true);
	get_list_context_detail();
	setupMaintainControls();	
	loadMtnForm();
}

function loadMtnForm()
{
	var data = getFormMetaValues();
	data["action"] = mtn('load-form');
	send_request({"method":'GET', "url":default_url, "callback":setMaintainFormValues, "data":data});
}

function setupMaintainControls()
{
	var hide_error_controls = 
	[
		"on-behalf-of","context-detail","application-service",
		"group-description",
		"debug-area"
	]
	for (var idx=0 ; idx<hide_error_controls.length ; idx++)
		doHideErrorOnValueChange(hide_error_controls[idx]);

	var hide_error_select_controls = 
	[
		"request-context",	
		"debug-list-selection"
	];
	
	for (var idx=0 ; idx<hide_error_select_controls.length ; idx++)
		doHideErrorOnSelectChange(hide_error_select_controls[idx]);
	
	jQuery(".ldap-user").keyup(function() {getLdapUsers(jQuery( this ).attr('id'));});
	
	
	/** onfocusout only else data load while cause long latency **/
	jQuery( "#application-service").focusout(function() {changeApplicationService();});
	jQuery(".flow-control-action").focusin(function() {houseHoldOnFocusIn(jQuery( this ).attr('id'));});
	jQuery(".flow-control-action").keyup(function() {flowControlFocusOut_mtn(jQuery( this ).attr('id'));});
	
	jQuery(".fixed-autosuggest").focusin(fixeAutoSuggest());
	jQuery(".fixed-autosuggest").keydown(fixeAutoSuggest());
	
	jq_disabled("control-readonly");
	
	jq_disabled("control-readonly");
	
	disableSubmitOnEnterPress();
}

function setMaintainFormValues(request)
{
	modal_off();
	setAdminFormValues(request);	
	
	var formValues 	= request["json"];
	var next 		= "";	
	
	getE("form-type").value = jsonNode(formValues,"administrative-information/fcr-mode");
	
	try{
		next = "fwo";getE(next).value = jsonNode(formValues,"hidden/" + next);
	}catch(e){}
	
	initialize_mtn_groups();
	
	for (var key in formValues.maintain)
	{
		mtn_groups.elems[key] = (hasX(key, "nb") || hasX(key, "idx")) ? getInt(formValues.maintain[key]) : formValues.maintain[key];
	}

	mtn_groups.leastOneGroup();
	mtn_groups.refreshGroupsView();
	
	if (isPreapprovedGroups())
	{
		jq_hide("preapproved-unauthorized");
		jq_disabled("preapproved-fields");
	}
	
	set_select_ro_inputs();
}

function initialize_mtn_groups()
{
	mtn_groups = new create_mtn_grps("group of " + (is_mtn_goo() ? "objects" : "services"), is_mtn_goo() ? "goo" : "gsrv");
}

function is_mtn(){return hasX(getE("form-type").value, "maintain");}
function is_mtn_goo(){return hasX(getE("form-type").value, "maintain-group-objects");}
function is_mtn_gsrv(){return hasX(getE("form-type").value, "maintain-group-services");}

function setChangeApplicationService_mtn()
{
	if (isX(getE('debug-area').value,'true') || confirm("Warning: changing the application service context implies all defined flows to be cleared, do you want to continue ?"))
	{
		getE("application-service").value = "";
		jq_enabled("application-service");
		/** TODO
		clearGroupInfo();
		mtn_groups.leastOneGroup();			
		mtn_groups.refreshGroupView();
		**/
	}
}

function changeApplicationService_mtn(func)
{
	var id = "application-service";

	if (isInSuggestionList(id, getE(id).value))
	{
		modal_on();
		send_request
		(
			{
				'method':"GET",
				'url':default_url,
				'callback':(func!=null) ? func : set_app_srv_list_elements_mtn,
				'data':
				{
					'action':access("list-application-service-elements"),
					'application-service':getE(id).value
				}
			}
		);
	}
}

function set_app_srv_list_elements_mtn(request)
{
	modal_off();
	addNewAutoSuggest("gsrv-srv-name", request['json'].srv, changeService_mtn);
	addNewAutoSuggest("gsrv-name", request['json'].gsrv, changeGroupOfServices_mtn);
}

function set_app_srv_end_load_gsrv(request)
{
	set_app_srv_list_elements_mtn(request)
	mtn_groups.end_load_gsrv();
}

function flowControlFocusOut_mtn(id)
{
	if (hasX(id, "goo"))
	{
		if (isX(id, "goo-name"))changeGOO_mtn();
		else if (isX(id, "goo-host"))changeHost_mtn();
		else if (isX(id, "goo-ip"))changeIp_mtn();
		else if (isX(id, "goo-subnet"))changeSubnet_mtn();
		//else if (isX(id, "goo-subnet-area"))changeSubnetArea_mtn();
		else if (isX(id, "goo-selector"))changeGOO_sel_mtn();
	}
	else if (hasX(id, "gsrv"))
	{
		if (isX(id,"gsrv-name"))changeGroupOfServices_mtn();
		else if (isX(id,"gsrv-srv-name"))changeService_mtn();
	}
}

function changeGOO_mtn(){mtn_groups.changeGOO();}
function changeHost_mtn(){mtn_groups.changeHost();}
function changeIp_mtn(){mtn_groups.changeIp();}
function changeSubnet_mtn(){mtn_groups.changeSubnet();}
function changeSubnetArea_mtn(){mtn_groups.changeSubnetArea();}
function changeGOO_sel_mtn(){mtn_groups.changeGOO_sel();}
function changeService_mtn(){mtn_groups.changeService();}
function changeGroupOfServices_mtn(){mtn_groups.changeGroupOfServices();}

function getMembers(type, type_elems)
{
	modal_on();
	send_request
	(
		{
			'method':"GET",
			'url':default_url,
			'callback':setGroupMembers,
			'data':
			{
				'type': type,
				'type_elems': type_elems,
				'action':access("list-members"),
				'group':getE(type + "-name").value,
				'ts_name':getE("application-service").value,
				'org':(type=="goo") ? getSelectE(type + "-org").value : ""
			}
		}
	);
}

function json2map(json_object, id, val)
{
	var map = new Object();

	if (json_object!=null)
	{
		for (var k in json_object)
			map[json_object[k][id]] = json_object[k][val];
	}
	return map;
}

function setGroupMembers(request)
{
	modal_off();
	if (isX(request['data']['type'], "goo"))
	{
		setMembers("goo-host", request['json'].hosts, json2map(request['json'].hosts_fw,"id","value"));
		setMembers("goo-subnet", request['json'].subnets, json2map(request['json'].subnets_fw,"id","value"));
		setMembers("goo-selector", request['json'].goos, json2map(request['json'].goos_fw,"id","value"));
	}
	else
	{
		setMembers("gsrv-srv", request['json'].srvs, json2map(request['json'].srvs_fw,"id","value"));
	}
	mtn_groups.add_cat_element_group_member_in_use();
}

function setMembers(type_elems, items, items_fw)
{
	var map 	= new Object();
	if (items.length>0)
	{
		for (var k in items)
			map[items[k]] = true;
	}
	householder["list-members-" + type_elems] = map;
	householder["list-members-" + type_elems + "-fw"] = items_fw;
}

function isInMemberList(type_elems, val)
{
	var members = householder["list-members-" + type_elems];
	return members!=null && members[val]==true;
}

function clearMembers(type_elems)
{
	householder["list-members-" + type_elems] = null;
	householder["list-members-" + type_elems + "-fw"] = null;
}

function showGroupMembers(group_id)
{
	var members = "";
	
	if (is_mtn_goo())
	{
		var hosts 	= ""; 
		var subnets = "";
		var goos 	= "";
		
		var map = householder["list-members-goo-host-fw"];	
		for (var mbr in map)
			hosts += "<tr><td>" + mbr + "</td></tr>";

		map = householder["list-members-goo-subnet-fw"];		
		for (var mbr in map)
			subnets += "<tr><td>" + mbr + "</td></tr>";

		map = householder["list-members-goo-selector-fw"];		
		for (var mbr in map)
			goos += "<tr><td>" + mbr + "</td></tr>";

		members += isEmpty(hosts) ? "" : "<tr><th>Host(s)</th></tr>" + hosts;
		members += isEmpty(subnets) ? "" : "<tr><th>Subnet(s)</th></tr>" + subnets;
		members += isEmpty(goos) ? "" : "<tr><th>Group(s) of objects</th></tr>" + goos;
	}
	else
	{
		var map = householder["list-members-gsrv-srv-fw"];

		var srvs = ""; 
		for (var mbr in map)
			srvs += "<tr><td>" + mbr + "</td></tr>";
			
		members += isEmpty(srvs) ? "" : "<tr><th>Service(s)</th></tr>" + srvs;
	}
	var fw_name = this.mtn_groups.getGrpsInfo("output"); 
	setHtml("group-members-header", !isEmpty(fw_name) ? fw_name : getE(group_id).value);
	setHtml("group-members-list", !isEmpty(members) ? members : "<tr><th>Member(s)</th></tr><tr><td>This group is currently empty</td></tr>");
	//hide(this.mtn_groups.type + "-dialog");
	show("group-members");
}

function hideGroupMembers()
{
	hide("group-members");
	show(this.mtn_groups.type + "-dialog");
}

function set_org_list_elements_mtn(request)
{
	var type 		= request['data']['type'];
	var goo 		= request['json'].goo;
	addNewAutoSuggest(type + "-host", request['json'].host_name, changeHost_mtn);
	addNewAutoSuggest(type + "-ip", request['json'].host_ip, changeIp_mtn);
	addNewAutoSuggest(type + "-subnet", request['json'].subnet_mask, changeSubnet_mtn);
	//addNewAutoSuggest(type + "-subnet-area", request['json'].area, changeSubnetArea_mtn);
	addNewAutoSuggest(type + "-name", goo, changeGOO_mtn);
	addNewAutoSuggest(type + "-selector", goo, changeGOO_sel_mtn);
	
	householder["list-" + type + "-host-autosuggest"].do_force_reposition 			= true;
	householder["list-" + type + "-ip-autosuggest"].do_force_reposition 			= true;
	householder["list-" + type + "-subnet-autosuggest"].do_force_reposition 		= true;
	householder["list-" + type + "-selector-autosuggest"].do_force_reposition 	= true;
	
	modal_off();
}

function set_org_elems_end_load_mtn(request)
{
	set_org_list_elements_mtn(request);
	mtn_groups.end_load_goo();
}


function setChangeApplicationService_mtn()
{
	if (isX(getE('debug-area').value,'true') || confirm("Warning: changing the application service context implies all defined groups to be cleared, do you want to continue ?"))
	{
		getE("application-service").value = "";
		jq_enabled("application-service");
		initialize_mtn_groups();
		mtn_groups.leastOneGroup();		
		mtn_groups.refreshGroupsView();
	}
}

function getFormValues_mtn(addValues)
{
	var formValues 			= addValues!=null ? addValues : new Object();
	for (var key in mtn_groups.elems)
		formValues[key] = mtn_groups.elems[key];
		
	return formValues;
}

function button_saveForm_mtn()
{
	{
		modal_on();
		var formValues = getFormValues_mtn(getFormAdminValues(getFormMetaValues()));
		formValues["action"] = form_action('save-form');
		send_request({"method":'GET',"url":default_url,"data":formValues,"callback":submitFlowDocumentation});	
	}
}

function button_submitForm_mtn()
{
	if (isX(getE("conflicts").value, "true"))
	{
		notify("Some group(s) are not defined correctly, correct it (them) first.");
		return ;
	}
	if (!isSubmittingFileCorrect())
		return ;
	var formValues = getFormValues_mtn(getFormAdminValues(getFormMetaValues()));
	if (formValidated(formValues, true))
	{
		modal_on();
		formValues["action"] = form_action('submit-form');
		send_request({"method":'GET',"url":default_url,"data":formValues,"callback":onMainActionReturn})
	}
	else 
	{
		notify("Some error(s) remains in the form, please correct it (them) first.");
	}
}

function button_dropForm_mtn()
{
	button_dropForm();
}

function getActionName(action)
{
	return replaceAllIn(replaceAllIn(action, "use", "add"), "member", "use");
}

function isPreapprovedGroups()
{
	//return false;
	return hasX(getE("form-type").value, "preapproved");
}

function setReadonlyViewControls()
{
	if (isX(getE("readonly").value, "false"))
	{
		jq_hide("control-display-readonly");
		jq_show("control-display-editable");
	}
	else
	{
		jq_hide("control-display-editable");
		jq_show("control-display-readonly");	
	}
	set_select_ro_inputs();	
	mtn_groups.setPreapprovedView();
}

/**
	DEBUG TOOL
**/

function updateCatalogMtn()
{
	send_request
	(
		{
			"method":'GET',
			"url":default_url,
			"data":
			{
				"action":debug_action('update-catalog'),
				"form-id":getE("form-id").value,
				"type":'mtn'
			},
			"callback":doNothing
		}
	)
}

function showGroups()
{
	var values = "";
	values += "\/*** Group(s) ***\/\n";
	for(var k in mtn_groups.elems)values += "addGroupEntry('" + k + "','" + mtn_groups.elems[k] + "');\n";
	values += "*** Group(s) in html params ***\n";
	values += mtn_groups.toHtmlParams() + "\n";
	/**/
	getE("debug-area").value = values;
}


function addMtnSamples(num)
{
	initialize_mtn_groups();
	doAddMtnSamples(num);
	mtn_groups.refreshGroupsView();
}

function addGroupEntry(key,value)
{
	mtn_groups.elems[key] = value;
}

function doAddMtnSamples(num)
{
	if (num==1)
	{
addGroupEntry('g-type','mtn-goo');
addGroupEntry('g-nb-group','3');
addGroupEntry('g-idx','3');
addGroupEntry('g-2-goo-org','MOBISTAR');
addGroupEntry('g-2-goo-name','Any');
addGroupEntry('g-2-goo-env','nonProd');
addGroupEntry('g-2-goo-output','mob-Any-nonProd-grp');
addGroupEntry('g-2-goo-action','update');
addGroupEntry('g-2-goo-hosts','cat---host:::host---ampere:::env---Prod:::ip---10.0.0.11:::ip-tsl---:::uc---create:::output---mob-host-ampere-Prod-10.0.0.11;;;cat---host:::host---khali:::env---nonProd:::ip---10.0.0.0:::ip-tsl---:::uc---create:::output---mob-host-khali-nonProd-10.0.0.0;;;cat---host:::host---maxwell:::env---nonProd:::ip---10.0.0.3:::ip-tsl---:::uc---create:::output---mob-host-maxwell-nonProd-10.0.0.3');
addGroupEntry('g-2-goo-subnets','cat---subnet:::subnet---10.0.128.0/22:::uc---use:::output---10.0.128.0/22;;;cat---subnet:::subnet---10.0.0.0/18:::area---FIKI:areaTelcoInfostar:::uc---use:::output---mob-net-10.0.0.0-18');
addGroupEntry('g-2-goo-goos','cat---goo:::goo---appMapiGw:::env---nonProd:::uc---create:::output---mob-appMapiGw-nonProd-grp');
addGroupEntry('g-1-goo-org','MOBISTAR');
addGroupEntry('g-1-goo-name','virtPartnerPortal');
addGroupEntry('g-1-goo-env','nonProd');
addGroupEntry('g-1-goo-output','mob-virtPartnerPortal-nonProd-grp');
addGroupEntry('g-1-goo-action','rename');
addGroupEntry('g-1-goo-new-name','virtualPartner');
addGroupEntry('g-3-goo-org','MOBISTAR');
addGroupEntry('g-3-goo-name','appMapiGw');
addGroupEntry('g-3-goo-env','nonProd');
addGroupEntry('g-3-goo-output','mob-appMapiGw-nonProd-grp');
addGroupEntry('g-3-goo-action','delete');
	}
	else
	{
		/*** Group(s) ***/
		addGroupEntry('g-type','mtn-gsrv');
		addGroupEntry('g-nb-group','3');
		addGroupEntry('g-idx','3');
		addGroupEntry('g-1-gsrv-name','Kerberos authentication protocol');
		addGroupEntry('g-1-gsrv-action','rename');
		addGroupEntry('g-1-gsrv-new-name','kerobero app');
		addGroupEntry('g-2-gsrv-name','HTTP/HTTPS');
		addGroupEntry('g-2-gsrv-action','update');
		addGroupEntry('g-2-gsrv-srvs','srv-name---HTTPS:::srv-proto---tcp:::srv-type----:::srv-port---443:::uc---remove:::output---HTTPS;;;srv-name---HTTP:::srv-proto---tcp:::srv-type----:::srv-port---80:::uc---remove:::output---HTTP;;;srv-name---SSH & SFTP:::srv-proto---tcp:::srv-type----:::srv-port---22:::uc---use:::output---SSH & SFTP');
		addGroupEntry('g-3-gsrv-name','Radius flow');
		addGroupEntry('g-3-gsrv-action','delete');
	}
}

/**
jvoisin
maintain-group-objects
1000,1002,2001
TS: Citrix new Infra - VDI
*/
