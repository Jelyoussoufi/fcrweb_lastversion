var xhr;
		

function getSearchResult()
{	
	var html = "<h2>My Search Result</h2><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search in progress ... <img src=\"ressources/progress.gif\"/><br><br>";
	document.getElementById("SearchResult").innerHTML = html;	
	var searchValue = document.getElementById("search_value").value;
	document.getElementById("search_value").value = "";
	xhr = new XMLHttpRequest();
	xhr.open('GET', 'profiles.htm?action=getSearchResult&searchValue=' + searchValue + '&ms=' + new Date().getTime() , true);
	xhr.onreadystatechange = function() { displaySearchResult(); } ;
	xhr.send(null);
}

function displaySearchResult()
{
	if (xhr.readyState == 4) //call is completed true
	 {
	 	if(xhr.status == 200) //http request is successfull
		{
	 		var requests = xhr.responseText;	 		
	 		var jsonObject = JSON.parse(requests);
	 		
	 		var html = displayJsonObject(jsonObject, "<p>No results are matching your search criteria</p>");	 		
	 		document.getElementById("SearchResult").innerHTML = "<h2>My Search Result <a href=\"javascript:void(0)\" onClick=\"hideSearchResult()\">(hide)</a></h2>" + html;
		}
		else
		{
	 		alert("Error ---> \n"+ xhr.status + " : " + xhr.statusText);
	 	}
	 }
}

function hideSearchResult()
{
	document.getElementById("SearchResult").innerHTML = "";
}