var xhr;
var xhr2;	
var xhr3;

function getSearchResult()
{	
	var html = "<h2>My Search Result</h2><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search in progress ... <img src=\"ressources/progress.gif\"/><br><br>";
	document.getElementById("SearchResult").innerHTML = html;	
	var searchValue = document.getElementById("search_value").value;
	document.getElementById("search_value").value = "";
	xhr = new XMLHttpRequest();
	xhr.open('GET', 'profiles.htm?action=getSearchResult&searchValue=' + searchValue + '&ms=' + new Date().getTime() , true);
	xhr.onreadystatechange = function() { displaySearchResult(); } ;
	xhr.send(null);
}

function displaySearchResult()
{
	if (xhr.readyState == 4) //call is completed true
	 {
	 	if(xhr.status == 200) //http request is successfull
		{
	 		var requests = xhr.responseText;	 		
	 		var jsonObject = JSON.parse(requests);
	 		
	 		var html = displayJsonObject(jsonObject, "<p>No results are matching your search criteria</p>");	 		
	 		document.getElementById("SearchResult").innerHTML = "<h2>My Search Result <a href=\"javascript:void(0)\" onClick=\"hideSearchResult()\">(hide)</a></h2>" + html;
		}
		else
		{
	 		alert("Error ---> \n"+ xhr.status + " : " + xhr.statusText);
	 	}
	 }
}

function hideSearchResult()
{
	document.getElementById("SearchResult").innerHTML = "";
}

function updateParameters()
{
	var totalNumber = document.getElementById("param_number").value;
	xhr2 = new XMLHttpRequest();
	var urlVars = 'admin.htm?id=2&paramnumber=' + totalNumber;
	for (var i = 0; i < totalNumber; i++)
	{
		var paramName = document.getElementById("param"+i+"name").value;
		var paramValue = document.getElementById("param"+i+"value").value;
		urlVars += '&param'+i+'name=' + paramName + '&param'+i+'value=' + paramValue;
	}
	urlVars += '&ms=' + new Date().getTime();
		
	xhr2.open('GET', urlVars.replace(/ /g, "%20") , true);
	xhr2.onreadystatechange = function() { parametersUpdated(); } ;
	xhr2.send(null);
}

function parametersUpdated()
{
	if (xhr2.readyState == 4) //call is completed true
	 {
	 	if(xhr2.status == 200) //http request is successfull
		{
	 		alert("Parameters updated");
		}
		else
		{
	 		alert("Error ---> \n"+ xhr2.status + " : " + xhr2.statusText);
	 	}
	 }
}

function displayNotCompliant(element_id)
{
	var element = document.getElementById(element_id+"_div");
	var text = document.getElementById(element_id);
	if(element.style.display == "block") {
		element.style.display = "none";
		text.innerHTML = "(show)";
  	}
	else {
		element.style.display = "block";
		text.innerHTML = "(hide)";
	}
}

function removeRange(range_id)
{
	var paramNumbers = document.getElementById("param_numbers").value;
	document.getElementById("param_numbers").value = paramNumbers.replace("-"+range_id+"-", "");
	var row = document.getElementById("row"+range_id);
    row.parentNode.removeChild(row);
}

function addRange()
{
	var last_id = document.getElementById("last_id").value;
	var param_numbers = document.getElementById("param_numbers");
	param_numbers.value = param_numbers.value+"-"+ last_id +"-";
	var tab = document.getElementById("rangeTable");
	var row = tab.insertRow(-1);
	row.id="row"+last_id;
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	cell1.innerHTML = "From <input type=\"text\" value=\"\" id=\"range"+last_id+"start\"/>";
	cell2.innerHTML = "To <input type=\"text\" value=\"\" id=\"range"+last_id+"end\"/>";
	cell3.innerHTML = "<a href=\"javascript:void(0)\" onclick=\"removeRange("+last_id+")\"><img src=\"ressources/navigate_minus.png\" alt=\"Remove this range\" title=\"Remove this range\"></a>";

	document.getElementById("last_id").value = parseInt(last_id)+1;
}

function updateRanges()
{
	var numberList = document.getElementById("param_numbers").value;
	var last_id = document.getElementById("last_id").value;
	var urlParam = "";
	var isValid = true;
	for (var i = 0; i < last_id && isValid; i++)
	{
		if (numberList.indexOf("-"+i+"-") != -1)
		{
			var rangeStart = document.getElementById("range"+i+"start").value;
			var rangeEnd = document.getElementById("range"+i+"end").value;
			if (isValidIpAddress(rangeStart) && isValidIpAddress(rangeEnd))
			{
				urlParam = urlParam + ";"+rangeStart+","+rangeEnd;
			}
			else
			{
				alert("Invalid values");
				isValid = false;				
			}
		}
	}
	if (isValid)
	{
		xhr3 = new XMLHttpRequest();
		var urlVars = 'admin.htm?id=3&ranges=' + urlParam.substring(1);		
		urlVars += '&ms=' + new Date().getTime();		
		xhr3.open('GET', urlVars, true);
		xhr3.onreadystatechange = function() { rangesUpdated(); } ;
		xhr3.send(null);
	}
}

function isValidIpAddress(ip)
{
	if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function rangesUpdated()
{
	if (xhr3.readyState == 4) //call is completed true
	 {
	 	if(xhr3.status == 200) //http request is successfull
		{
	 		alert("Ranges updated");
		}
		else
		{
	 		alert("Error ---> \n"+ xhr3.status + " : " + xhr3.statusText);
	 	}
	 }
}