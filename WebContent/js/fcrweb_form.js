/**
https://sectools-acc.nonprod.mobistar.be/fcrweb
http://stallion.acc.mobistar.be:8096/fcrweb

http://bonita.acc.mobistar.be
https://opmweb.acc.mobistar.be/phpmyadmin
*/
var householder = new Object();
var atCelsius 	= false;

var default_choice 		= "[Select ...]";
var default_choice_sh 	= "[...]";
var file_size_limit 	= 10 * 1024 * 1024; //10 MB
//var file_size_limit = 10 * 1024 ; //10 kb


var form_meta 	= {"form-id":"fid","local":"local"};
var admin_form 	= ["requestor", "on-behalf-of", "application-service", "context-detail", "detailed-reason-request"];

var source;
var destination;
var service;
var flow;
var use_strong_constraint = false;
var default_url = "fcr-action.htm";
var form_type = "";

var meta_e_sep		= "���";
var meta_e_kv_sep	= "###";


function notifyCloseBox()
{
	//logout("notifyCloseBox");
	notify("Close dialog box first.");
}



function meta_params2map(params)
{
	set_params2map_seps(meta_e_sep, meta_e_kv_sep);
	return params2map(params);
}

function meta_map2params(map)
{
	set_params2map_seps(meta_e_sep, meta_e_kv_sep);
	return map2params(map, false);
}

function load(ft)
{
	
	form_type = ft;
	modal_on();
	ZWDrag.makeDraggable( getE('display-group-members'), getE('display-group-members') );
	ZWDrag.makeDraggable( getE('debug-tool'), getE('debug-tool-handle') );
	$("#debug-tool").css("left","66%");
	if (isX(form_type, "request"))
	{
		onload_request();
		/*
		ZWDrag.makeDraggable( getE('source-dialog'), getE('source-dialog-handle') );
		ZWDrag.makeDraggable( getE('destination-dialog'), getE('destination-dialog-handle') );
		ZWDrag.makeDraggable( getE('service-dialog'), getE('service-dialog-handle') );
		*/
	}
	else 
	{
		onload_maintain();
		/*
		ZWDrag.makeDraggable( getE('group-members'), getE('group-members-handle') );
		ZWDrag.makeDraggable( getE('goo-dialog'), getE('goo-dialog-handle') );
		ZWDrag.makeDraggable( getE('gsrv-dialog'), getE('gsrv-dialog-handle') );
		*/
	}
	if (isIE())
		adjust2IE();
	else if (isFox())
		adjust2Fox();
	else if (isChrome())
		adjust2Chrome();
	
}

function adjust2IE()
{
	//$('#wrap img.bgcontent').css('height','98%');
}

function adjust2Fox()
{
}

function adjust2Chrome()
{
	$('.multiple-select-linked-button').css('top','-20px');
}

function onload_request()
{
	checkDebugTool();
	setFormHeight("simple");
	atCelsius = getE("atCelsius").value == "true";	
	buildHeaderAttributeMap();
	getLdapUsers("");
	get_list_application_service(false);
	get_list_context_detail();	
	setup_controls();
	initialize_flows_src_dest_srv();
	loadForm();	
	
}

function loadForm()
{
	var data = getFormMetaValues();
	data["action"] = form_action('load-form');
	send_request({"method":'GET', "url":default_url, "callback":setFormValues, "data":data});
}
function setFormHeight(type)
{
	var form_height 	= {"advanced":1010,"simple":650,"flow":760,"mtn":1010,"ctrl":800};
	jQuery('#wrap').css('height',  form_height[type] + 'px');
	
	if (isIE())
		$('#main-section').height($('#wrap').height()-225);
	
	//jQuery('.maskass-ie8').css('height',  (form_height[type]) + 'px');
}

function switch_flow_dialog_view(type)
{
	$(".control-dialog").hide();
	$('#flow-general').hide();
	if (hasX(type, "dialog"))
	{
		setFormHeight("ctrl");
		$('#' + type).show();
	}
	else
	{
		setFormHeight("flow");
		$('#flow-general').show();		
	}
}

function switch_mtn_dialog_view(type)
{
	$(".control-dialog").hide();
	$('.main-form-element-mtn').hide();
	if (hasX(type, "dialog"))
	{
		setFormHeight("ctrl");
		$('#' + type).show();
	}
	else
	{
		setFormHeight("mtn");
		$('.main-form-element-mtn').show();		
	}
}

function buildHeaderAttributeMap()
{
	
	var url = getUrl();
		url = url.indexOf('#') != -1 ? url.split('#')[0] : url;
	var headers = hasX(url, "?") ? html_params2map(url.split("?")[1]) : new Object();

	for(var prop in form_meta)
		getE(prop).value = getParam(headers, form_meta[prop]);
}



function setup_controls()
{
	var hide_error_controls = 
	[
		"on-behalf-of","context-detail","application-service",
		"flow-details",
		"flow-description","end-date",
		"profile","profile-owner","profile-description",
		"src-host","src-ip","scr-ip-tsl","src-subnet","src-goo","src-goo-selector",
		"dest-host","dest-ip","scr-ip-tsl","dest-subnet","dest-goo","dest-goo-selector",
		"srv-one-name","srv-gsrv-name","srv-one-type","srv-one-port",
		"debug-area"
	]
	for (var idx=0 ; idx<hide_error_controls.length ; idx++)
		doHideErrorOnValueChange(hide_error_controls[idx]);

	var hide_error_select_controls = 
	[
		"request-context",
		"flow-action",
		"src-org","dest-org",
		"src-cat","dest-cat","srv-cat",	
		"srv-one-proto",
		"srv-gsrv-srv",
		"src-action","dest-action","srv-action",		
		"debug-list-selection"
	];
	
	for (var idx=0 ; idx<hide_error_select_controls.length ; idx++)
		doHideErrorOnSelectChange(hide_error_select_controls[idx]);
	
	/*
	var hide_error_select_group_controls = 
	[
		"src-hosts","src-subnets","src-goos",
		"dest-hosts","dest-subnets","dest-goos"
	]	
	
	for (hescg_idx in hide_error_select_group_controls)
		doHideErrorOnSelectChangeGroup(hide_error_select_group_controls[hescg_idx]);
	*/
	
	var  hide_create_controls = 
	[
		"profile",
		"src-host","src-subnet","src-goo",
		"dest-host","dest-subnet","dest-goo",
	]
	
	//jQuery( "#application-service").focusin(function() {houseHoldOnFocusIn(jQuery( this ).attr('id'));});
	
	jQuery(".ldap-user").keyup(function() {getLdapUsers(jQuery( this ).attr('id'));});
	
	
	/** onfocusout only else data load while cause long latency **/
	jQuery( "#application-service").focusout(function() {changeApplicationService();});
	//jQuery( "#profile").focusin(function() {houseHoldOnFocusIn(jQuery( this ).attr('id'));});
	jQuery("#profile").keyup(function() {changeProfile();});
	jQuery(".flow-control-action").focusin(function() {houseHoldOnFocusIn(jQuery( this ).attr('id'));});
	jQuery(".flow-control-action").keyup(function() {flowControlFocusOut(jQuery( this ).attr('id'));});
	
	jQuery(".fixed-autosuggest").focusin(fixeAutoSuggest());
	jQuery(".fixed-autosuggest").keydown(fixeAutoSuggest());
	
	jq_disabled("control-readonly");
	
	initEndDate()	
	disableSubmitOnEnterPress();
}

function initEndDate()
{
	if (isIE())
	{

	}
	else
	{
		new JsDatePick({
			useMode:2,
			target:"end-date",
			dateFormat:"%d/%m/%Y"
			//,imgPath:"/ressources/datePicker_img";
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
	}
}


/** prevent user to submit flow documentation on enter key press. **/
function disableSubmitOnEnterPress()
{
	jQuery(document).ready(function() {
	  jQuery(window).keydown(function(event){
		if(event.keyCode == 13) {
		  event.preventDefault();
		  return false;
		}
	  });
	});
}

function initialize_flows_src_dest_srv()
{
	source		= new fcrweb_control_object("source", "src", true);
	destination = new fcrweb_control_object("destination", "dest", true);
	service		= new fcrweb_control_object("service", "srv", true);
	flow		= new fcrweb_flow_object(source, destination, service);
	
	source.flow 		= flow;
	destination.flow 	= flow;
	service.flow 		= flow;
}

function debug_action(uri){return fcr("local/" + uri);}
function test_action(uri){return fcr("test/" + uri);}
function access(uri){return fcr("access/" + uri);}
function form_action(uri){return fcr("action/" + uri);}
function fwo(uri){return fcr("fwo/" + uri);}

//function fcr(uri){return "FCR/" + uri;}
function fcr(uri){return uri;}

function doNothing(index, control){};

function get_list_application_service(filter_owner)
{
	send_request({"method":"GET","url":default_url,"data":{'action':access("list-application-service"),'owner':filter_owner}, "callback":setList_application_service});
}

function get_list_context_detail()
{
	getE("context-detail").value = "";
	send_request({"method":"GET","url":default_url,"data":{"action":access("list-context-detail"),"request-context":getE("request-context").value},"callback":setList_context_detail});
}

function get_list_help_msg()
{
	send_request({"method":"GET","url":default_url,"data":{"action":access("list-help-msg")},"callback":set_list_help_msg});
}

function set_list_help_msg(request)
{
	jq_show_i("control-help");
	var helps = request['json'].configs;
	
	if (helps==null)return ;
	
	var single_map = 
	{
		"on-behalf-of":true, "application-service":true, "request-context":true, "context-detail":true, "detailed-reason-request":true,  "flow-documentation":true, "flow-details":true,
		"flow-description":true, "dataflow-type":true, "flow-action":true, "end-date":true, 
		"profile":true, "profile-owner":true, "profile-description":true,
		"src-nao-auth":true, "src-nao-enc":true,
		"srv-cat":true, "srv-one-name":true, "srv-one-proto":true, "srv-one-port":true, "srv-gsrv-name":true,
		"output-source":true, "output-destination":true, "output-service":true
	};
	for (var i=0 ; i<helps.length ; i++)
	{
		try
		{
			if (single_map[helps[i].name])
			{
				if (getE("help-" + helps[i].name)!=null)
					getE("help-" + helps[i].name).title = helps[i].text;
			}
			else if (isX(helps[i].form_type,"advanced"))
			{
				if (getE("help-src-" + helps[i].name)!=null)
					getE("help-src-" + helps[i].name).title = helps[i].text;
				if (getE("help-dest-" + helps[i].name)!=null)
					getE("help-dest-" + helps[i].name).title = helps[i].text;
			}
		}
		catch (e)
		{
			logout("error on help:" + helps[i].name);
		}
	}
}
function getLdapUsers(id)
{
	send_request({"method":"GET","url":default_url,'data':{'action':access("list-user"),'template':!isEmpty(id)?getE(id).value:"",'field':id},"callback":setList_user});
}

function setList_user(request)
{
	var list = request.json.items;
	var field = request.data.field;
	if (isEmpty(field))
	{
		addNewAutoSuggest("on-behalf-of", list, doNothing);
		addNewAutoSuggest("profile-owner", list, doNothing);
	}
	else if (field=="on-behalf-of")
		addNewAutoSuggest("on-behalf-of", list, doNothing);
	else if (field=="profile-owner")
		addNewAutoSuggest("profile-owner", list, doNothing);
}

function setList_application_service(request)
{
	addNewAutoSuggest("application-service", getJson(request["responseText"]).items, changeApplicationService);
	householder["list-application-service-autosuggest"].restrict_typing = true;
}

function setList_context_detail(request)
{
	addNewAutoSuggest("context-detail", getJson(request["responseText"]).items, doNothing);	
}

function addNewHouseHoldList(id, list)
{
	var suggestion 	= new Array();
	householder["list-" + id + "-elements"] = new Object();
	for (var i in list)
	{
		var elem = isEmpty(list[i]) ? "" : trim(list[i]);
		if (!isEmpty(elem))
		{
			if (householder["list-" + id + "-elements"][elem] != true)	//already in suggest list;
			{
				householder["list-" + id + "-elements"][elem] = true;
				suggestion.push(elem);
			}
		}
	}
}

function addNewAutoSuggest(id, list, func)
{
	var suggestion 	= new Array();
	var size 		= 0;
	householder["list-" + id + "-elements"] = new Object();
	for (var i in list)
	{
		var elem = isEmpty(list[i]) ? "" : trim(list[i]);
		if (!isEmpty(elem))
		{
			if (householder["list-" + id + "-elements"][elem] != true)	//already in suggest list;
			{
				householder["list-" + id + "-elements"][elem] = true;
				suggestion.push(elem);
				size++;
			}
		}
	}
	if (isEmpty(householder["list-" + id + "-set"]))
	{
		householder["list-" + id + "-set"] = true;
		householder["list-" + id + "-autosuggest"] = new autosuggest(id, suggestion, null, func);			
	}
	else if (size>0)
		householder["list-" + id + "-autosuggest"].bindArray(suggestion);
	else
		householder["list-" + id + "-autosuggest"].bindArray2(suggestion);
}

function removeAutoSuggest(id)
{
	if (!isEmpty(householder["list-" + id + "-set"]))
	{
		householder["list-" + id + "-elements"] = new Object();//[];
		householder["list-" + id + "-autosuggest"].bindArray2([]);	
	}
}

function isInSuggestionList(id, value)
{	
//logout("isInSuggestionList(" + id + "," + value + "):" + (householder["list-" + id + "-elements"][value]==true));
if (householder["list-" + id + "-elements"]==null)
	logout("Error suggestion list " + id + " does not exist.");
	return householder["list-" + id + "-elements"][value]==true;
}

function getHouseHoldList(id)
{
	return householder["list-" + id + "-elements"];
}

function check_app_srv_pre(notifyError)
{
	var app_ser = getE("application-service").value;
	var code = 0;

	if (isEmpty(app_ser))
		code = -1;			
	else if (!isInSuggestionList("application-service", app_ser))
		code = -2;

	if (code!=0 && notifyError)
	{
		if (code==-1)
			notify("Please, define the applicaction, service context first.");
		else if (code==-2)
			notify("Please, select an applicaction, service context from suggestion list first.");
			
		showError("application-service");		
	}
		
	return code==0;
}

function setFormValues(request)
{
	modal_off();
	setAdminFormValues(request);
		
	var formValues 	= request["json"];
	var next 		= "";	
	next 			= "user-type"; 	getE(next).value = jsonNode(formValues,"hidden/" + next);/** simple|advanced */
	next 			= "form-sel"; 	getE(next).value = jsonNode(formValues,"hidden/" + next);/** empty|simple|advanced */
	next 			= "req";		getE(next).value = jsonNode(formValues,"hidden/" + next);
	
	var details = jsonNode(formValues,"simple/flow-details");
	var reason 	= jsonNode(formValues,"administrative-information/detailed-reason-request");
	
	/** 
		first requestor task and user is requestor (#FRT)
	**/
	if 
	(
		isX(jsonNode(formValues, "administrative-information/fcr-status"), "fcr edition")
		& isX(getE("req").value, "true")
	)
	{	
		var ut = getE("user-type").value;
		var ft = getE("form-sel").value;
	
		if (isX(ut,"simple"))
		{
			hide("advanced-edition-link");
			
			/** for simple users, there is simple form only**/
			switchFormEdition("simple", true);
		}
		else
		{
			if (!isX(getE("readonly").value, "true"))
				show("advanced-edition-link");
				
			if (isX(ft, "simple"))
				switchFormEdition("simple", true);
			else 
				/** if form is undefined or advanced then display advanced form **/
				switchFormEdition("advanced", true);
		}
	}
	else	/** arbitrary task */
	{
		hide("link-advanced-form");
		/** advanced form only */
		switchFormEdition("advanced", true);
	}	
	
	var form_type = getE("form-selector").value;

	if (isX(form_type,"advanced"))
	{
		for (var key in formValues.advanced)
		{
			var value = formValues.advanced[key];
			if (hasX(key, "nb") || hasX(key, "idx"))
				value = getInt(formValues.advanced[key]);
			if (hasX(key, "src"))
				source.elems[key] = value;
			else if (hasX(key, "dest"))
				destination.elems[key] = value;
			else if (hasX(key, "srv"))
				service.elems[key] = value;
			else
				flow.elems[key] = value;
		}
		flow.leastOneFlow();			
		flow.refreshFlowsView();
	}
	sync_simple_advanced_values
	(
		{'type':form_type,'flow-details':details,'detailed-reason-request':reason}
	);
	
	
	/** 
		show/hide on-behalf-of
		enable fw operations, technical-service rules revalidation color, extra columns and features 
	**/
	next = "fwe";			getE(next).value = jsonNode(formValues,"hidden/" + next);
	next = "fwo";			getE(next).value = jsonNode(formValues,"hidden/" + next);
	next = "change-rule";	getE(next).value = jsonNode(formValues,"hidden/" + next);
	next = "tsrr";			getE(next).value = jsonNode(formValues,"hidden/" + next);
	setUserGroupView();
	set_select_ro_inputs();
}

function setAdminFormValues(request)
{
	var formValues 	= request["json"];
	var next 		= "";
	
	next = "form-id"; 
	var formId = getE(next).value = jsonNode(formValues,"hidden/" + next); 
	
	setHtml("title-form-id", formId);
	
	
	next = "readonly"; 
	var ro = getE(next).value = jsonNode(formValues,"hidden/" + next);
	
	setHtml("form-title", isX(ro, "false") ? "- Fill in the form" : "- View form");
	setReadonlyView(ro);
	

	var i = 0;
	while(i<admin_form.length)
	{
		next = admin_form[i]; 
		if(getE(next)!=null)
			getE(next).value = jsonNode(formValues,"administrative-information/" + next);
		i++;
	}

	setSelectE("request-context", jsonNode(formValues,"administrative-information/request-context"));
	setFlowDocumentationDisplay(jsonNode(formValues,"administrative-information/filename"));
	
	if (usedebug)
	{
		var adm		= jsonNode(formValues,"hidden/adm");
		var type 	= jsonNode(formValues,"hidden/user-type");
		var group 	= jsonNode(formValues,"hidden/user-group");
		var ftype 	= jsonNode(formValues,"hidden/form-sel");
		logout(jsonNode(formValues,"hidden/user") + " - adm:" + adm + " - user-group:" + group + " - user-type:" + type + " - form-type:" + ftype);
		$("#adm-switch").css("color", adm ? "white" : "red");
		$("#adv-switch").css("color", isX(type, "advanced") ? "white" : "red");
		$("#fwe-switch").css("color", hasX(group, "fwe") ? "white" : "red");
		$("#so-switch").css("color", hasX(group, "so") ? "white" : "red");
		$("#fwo-switch").css("color", hasX(group, "fwo") ? "white" : "red");
	}
}

function setReadonlyView(readonly)
{
	if (isX("false", readonly + ""))
	{
		//jq_enabled("control-editable");
		$(".control-editable").removeAttr('disabled');
		jQuery(".mask_arrow").removeClass("mask_arrow");
		jq_hide("control-display-readonly");
		jq_show("control-display-editable");
		get_list_help_msg();
	}
	else
	{
		$('.control-editable').attr('disabled','disabled');
		//jq_disabled("control-editable");
		jQuery("select").find(".control-editable").addClass("mask_arrow");
		jq_hide("control-display-editable");
		jq_show("control-display-readonly");
		hide("link-advanced-form");
		hide("advanced-edition-link");
	}
	set_select_ro_inputs();
}

function switchFormEdition(new_type, ignore)
{
	jq_hide("warning-error");
	if (isX(new_type, "simple"))
	{
		if (ignore==true)
		{}	//skip message
		else if (isX(getE('debug-area').value,'true') || confirm("Warning: switching to simple form implies all flow(s) definition(s) to be dropped, would you like to switch to simple form?"))
		{
			//clearAdvancedForm();
			clearFlowDefinition();			
		}
		else 
			return ;
			
		$('.advanced-form-fields').hide();
		$('.simple-form-fields').show();		
	}
	else
	{	
		$('.simple-form-fields').hide();
		$('.advanced-form-fields').show();
	}	

	setFormHeight(new_type);
	getE("form-selector").value = new_type;
	
	sync_simple_advanced_values({'type':new_type,'flow-details':getE("flow-details").value,'detailed-reason-request':getE("detailed-reason-request").value});
}

function sync_simple_advanced_values(values)
{
	var v1 = isX(values["type"], "simple") ? "flow-details": "detailed-reason-request";
	var v2 = isX(values["type"], "simple") ? "detailed-reason-request": "flow-details";
	
	if (!isEmpty(values[v2]))
		getE(v1).value = values[v2];
	else if (!isX(values[v1], getE(v1).value))
		getE(v1).value = values[v1];
}


/**
	update form view according user group: req,fwe,tso,so,fwo
**/
function setUserGroupView()
{
	jq_hide("control-display-change-rule");
	
	if (isX(getE("fwe").value, "true") && isX(getE("readonly").value,'false'))
	{
		jq_disabled("on-behalf-of");
		hide("link-advanced-form");
		if (isEmpty(getE("on-behalf-of").value))
			hide("tr-on-behalf-of");
		else
			show("tr-on-behalf-of");
	}
	
	if (isChangeRule() | isFWO())
		jq_show("control-display-change-rule");
	
	jq_hide("control-display-fwo");
	if (isFWO())
		jq_show("control-display-fwo");
		
	if (isTSRR())
		setReadonlyView(true);
}


function formValidated(formValues, displayErrors)
{
	var has_errors = false;
	var next = "requestor"; var tmp = "";
	if (isEmpty(getE(next).value))
	{
		showError(next);
		has_errors = true;
	}

	next = "application-service"; tmp = getE(next).value;
	if (isEmpty(tmp) || !isInSuggestionList("application-service", tmp))
	{
		showError(next);
		has_errors = true;
	}
		
	next = "request-context"; 
	var rc = tmp = getE(next).value;
	if (isEmpty(tmp))
	{
		showError(next);
		has_errors = true;
	}
	
	next = "context-detail"; tmp = getE(next).value;
	
	if (isEmpty(tmp))
	{
		showError(next);
		has_errors = true;
	}
	else if ((isX(rc, "Project") | isX(rc, "Remote Access")) & !isInSuggestionList("context-detail", tmp))
	{
		showError(next);
		has_errors = true;
	}	
	
	next = "form-selector"; 
	var fs = tmp = getE(next).value;
	
	if (isX(fs, "simple"))
	{
		next = "flow-details"; 
		tmp = getE(next).value;
		if (isEmpty(tmp))
		{
			showError(next);
			has_errors = true;
		}
	}
	else if (isX(fs, "advanced"))
	{
		next = "on-behalf-of"; tmp = getE(next).value;
	
		if (!isEmpty(tmp))
		{
			if (!isInSuggestionList("on-behalf-of", tmp))
			{	
				showError(next);
				has_errors = true;
			}
		}	
		
		if (this.flow.getNbFlow()<=0)
		{
			showError("aefs");
			has_errors = true;
		}
		else if (isX(getE("conflicts").value, "true"))
		{
			has_errors = true;
		}
		
	}
	return !has_errors;
}

function setError(id, msg){setHtml("warning-error-td-" + id, msg);}
function showError(id){show("warning-error-tr-" + id);}
function hideError(id){hide("warning-error-tr-" + id);}

function doHideErrorOnValueChange(id)
{
	jQuery( "#" + id).focusout(
		function() 
		{
			hideError(id); 
			jq_hide("warning-error-tr-" + id);
		}
	);
}
function doHideErrorOnSelectChange(className){jQuery( "." + className).change(function() {hideError(className);});}
function doHideErrorOnSelectChangeGroup(className){jQuery( "." + className).change(function() {jq_hide("warning-error-tr-" + className);});}
function getFormMetaValues()
{
	var formValues 	= new Object();
	for(var prop in form_meta)
		formValues[prop] = getE(prop).value;
	return formValues;
}

function getFormAdminValues(addValues)
{
	var formValues 			= addValues!=null ? addValues : new Object();	
	var idx = 0;
	while(idx!=admin_form.length)
	{
		next = admin_form[idx];
		formValues[next] = getE(next).value;
		idx++;
	}
	
	formValues["request-context"] = getSelectE("request-context").value;
	
	return formValues;
}

function getFormValues(addValues)
{
	var formValues 			= addValues!=null ? addValues : new Object();		
	var next = "form-selector";
	var form_selection = formValues[next] = getE(next).value;
	if (isX(form_selection, "simple"))
	{
		next = "flow-details";
		formValues[next] = getE(next).value;
	}
	else if (isX(form_selection, "advanced"))
	{
		for (var key in flow.elems)
			formValues[key] = flow.elems[key];
		for (var key in source.elems)
			formValues[key] = source.elems[key];
		for (var key in destination.elems)
			formValues[key] = destination.elems[key];
		for (var key in service.elems)
			formValues[key] = service.elems[key];
	}
	
	return formValues;
}

function button_update_rule_refs()
{	
	if (isFWO())
	{	
		var hasErrors = false;
		var map = getFormMetaValues();
		
		map["f-nb-flow"] = flow.getNbFlow();
		for (var i=1 ; i<=flow.getNbFlow() ; i++)
		{	
			var fw_rdd = new Object();
			flow.setIdx(i);
			map["f-" + i + "-flow-id"] 	= flow.getFlowInfo("flow-id");			
			map["f-" + i + "-meta"] 	= flow.getFlowInfo("meta");
			
			var nb_rules = flow.getFlowInfo("nb-rule");
			map["f-" + i + "-nb-rule"] = nb_rules;
			for (var j=1 ; j<=nb_rules ; j++)
			{
				try{
					var fw = flow.getFlowInfo("rule-" + j + "-fw");
					if (isEmptyOrDefault(fw))
					{
						hasErrors = true;
						break;
					}
					map["f-" + i + "-rule-" + j + "-fw"] = fw;
					
					/** check if a flow implements two rules on same firewall **/
					if (fw_rdd[fw])
					{	
						hasErrors = true;
						break;
					}					
						fw_rdd[fw] = true;
				}catch(e){
					hasErrors = true;
					break;
				}

				try{
					var rule_id = flow.getFlowInfo("rule-" + j + "-rule-id");
					if (isEmpty(rule_id) || getInt(rule_id)<=0)
					{
						hasErrors = true;
						break;
					}
					map["f-" + i + "-rule-" + j + "-rule-id"] = rule_id;	
				}catch(e){
					hasErrors = true;
					break;
				}
			}
			if (hasErrors)
				break;
		}

		if (hasErrors)
			notify("Some rule(s) is (are) not defined correctly, please correct it (them) first.");
		else
		{
			modal_on();
			map["action"] = fwo('update-rule-refs');
			send_request({"method":'GET',"url":default_url,"data":map,"callback":notifyUserAction})	
		}
	}
}

function notifyUserAction(request)
{
	modal_off();
	notify(request['json'].statusCode + " - " + request['json'].statusDescription);
}


function button_saveForm()
{
	if (!isSubmittingFileCorrect())
		return ;
		
	if (isX(getE("form-selector").value, "advanced") && isX(getE("conflicts").value, "true"))
	{
		notify("Some error(s) remains in flow(s) definition which prevent data to be saved.")
	}
	else
	{
		modal_on();
		var formValues = getFormValues(getFormAdminValues(getFormMetaValues()));
		formValues["action"] = form_action('save-form');
		send_request({"method":'GET',"url":default_url,"data":formValues,"callback":onMainActionReturn})		
	}
}

function button_submitForm()
{
	if (!isSubmittingFileCorrect())
		return ;
	var formValues = getFormValues(getFormAdminValues(getFormMetaValues()));
	if (formValidated(formValues, true))
	{
		modal_on();
		formValues["action"] = form_action('submit-form');
		send_request({"method":'GET',"url":default_url,"data":formValues,"callback":onMainActionReturn})
	}
	else 
	{
		notify("Some error(s) remains in the form, please correct it (them) first.");
	}
}

function button_dropForm()
{
	var formValues = getFormMetaValues();
		formValues["action"] = form_action('drop-form');
		send_request({"method":'GET',"url":default_url,"data":formValues,"callback":onMainActionReturn})		
	modal_on();
}

function button_closeForm()
{
	
}

function onMainActionReturn(request)
{
	/** @pre isSubmittingFileCorrect() must be done before saving flow documentation **/
	if (hasX(request['data']['action'],"submit-form") | hasX(request['data']['action'],"save-form"))
		submitFlowDocumentation();
	else
		modal_off();
		
	try{
		if (hasX(request['data']['action'],"submit-form") | hasX(request['data']['action'],"drop-form"))
		{	
			hide("edition-control-buttons");	
			getE("readonly").value = true;
			setReadonlyView(getE("readonly").value);
		}
	}catch(e){}
	try
	{
		notify(request['json'].statusDescription);
	}catch(e){}
	
	
}

function isSubmittingFileCorrect()
{
	var doc_id = "myFile";
	

	var isValid = false;
	if (getE(doc_id).files==null || getE(doc_id).files[0]==null)
		isValid = true;
	else if (getE(doc_id).files[0].size <= file_size_limit)
		isValid = true;	
	else 
		isValid = false;
		
	if (isValid)
		hideError("flow-documentation");
	else
		showError("flow-documentation");
	return isValid;
}

function submitFlowDocumentation()
{
	var map = {"action":'document-submit',"form-id":getE("form-id").value,"form-url":document.location.href};
	send_request({"method":'GET',"url":'document-submit.htm',"data":map,"callback":submitDocument});
}

function submitDocument(request)
{
	if (hasUploadFile)
	{
		getE('submit-flow-documentation').click();
	}
	modal_off();	
}

var hasUploadFile = false;

function uploadFile()
{
	hasUploadFile=true;
}

function sendFlowDocumentation()
{
/*
	send_request
	(
		{
			"method":'POST',
			"url":default_url,
			"submit-file":true,
			"file":getE("flow-documentation").files[0],
			"data":
			{		
				"action":form_action("submit-flow-documentation"),
				"form-id":getE("form-id").value,
				"file-name":getE("flow-documentation").files[0].name,
				"file-size":getE("flow-documentation").files[0].size,
				"file-type":getE("flow-documentation").files[0].type
			},
			"callback":doNothing
		}
	)
*/
}

function setFlowDocumentationDisplay(filename)
{
	setHtml("flow-documentation-name", filename);
	
	if (isX(getE("readonly").value, "false"))
	{	
		if (!isEmpty(filename))
		{
			hide("flow-documentation-upload");
			show("flow-documentation-download");
			show("delete-document-button");
		}
		else
		{
			hide("flow-documentation-download");
			show("flow-documentation-upload");
			hide("delete-document-button");
		}
	}
	else
	{
		hide("flow-documentation-upload");
		show("flow-documentation-download");
		hide("delete-document-button");
	}
}

function getFlowDocumentation()
{
	var url 	= getE('contextPath').value + "/document-submit.htm?form-id="+getE("form-id").value+"&action=get-flow-documentation";
	var w 		= window.open(url, "_blank", "status, resizable=1, width=100, height=100, x=50, y=50");
}

function deleteFlowDocumentation()
{
	setHtml("flow-documentation-name", "");
	hide("flow-documentation-download");
	hide("delete-document-button");
	show("flow-documentation-upload");	
}

function houseHoldOnFocusIn(id)
{
	householder[id + "-tmp"] = getE(id).value;
}

function mask_on(){if (isIE8()) jq_show_oth("maskass-ie8","");else jq_show_oth("maskass","");}
function mask_off(){if (isIE8()) jq_hide("maskass-ie8");else jq_hide("maskass");}


function modal_on()
{
	if (isIE8())
	{ 
		$(".process-icon").show();
		$('.mask-ie').show();
	}
	else jq_show_oth("processing","");
}
function modal_off()
{
	if (isIE8())
	{ 
		$(".process-icon").hide();		
		$('.mask-ie').hide();	
	}
	else jq_hide("processing");
}

function isIE8(){return getE("ieold")!=null;}

function fixeAutoSuggest(){jQuery("autosuggest-body").addClass("fixe-element");}
function unfixeAutoSuggest(){jQuery("autosuggest-body").removeClass("fixe-element");}

/**
	Flow information methods
*/
function setChangeDataflowType()
{
	if (isX(getE('debug-area').value,'true') || confirm("Warning: Changing dataflow type implies defined source(s) to be dropped.  \nContinue?"))
		resetSource();
}

function changeDataflowType()
{
	var flowType = getRadio("dataflow-type").value;
	
	/** update flow action choices */
	
	var options = "<option class='' value=''>[Select ...]</option>";
	if (isX(flowType, "User 2 Host"))
	{		
		options += "<option id='flow-action-auth' class='user2host' value='User authentication'>User authentication</option>";
		options += "<option id='flow-action-vpn'class='user2host' value='Encryption (VPN)'>Encryption (VPN)</option>";		
	}
	else
	{
		options += "<option id='flow-action-accept' class='host2host' value='Accept'>Accept</option>";
		options += "<option id='flow-action-reject' class='host2host' value='Deny'>Deny</option>";
	}
	setHtml("flow-action", options);
	setSelectE("flow-action", default_choice);
	
	/** reinitialize profile sub-form */
	clearProfile(1);
	if (isX(flowType, "User 2 Host"))
		jq_show_i("profile-section");
	else
		jq_hide("profile-section");
}


function setChangeFlowAction()
{
	if (isX(getE('debug-area').value,'true') || confirm("Warning: Changing flow action implies defined source(s) to be dropped.  \nContinue?"))
		resetSource();
}

function setChangeProfile()
{
	if (isX(getE('debug-area').value,'true') || confirm("Warning: Changing user group implies defined source(s) to be dropped.  \nContinue?"))
		resetSource();
}

function resetSource()
{
	source.removeAllCtrlValues();
	setHtml("output-source", map2select_options(source.getCtrlListOutput(), source.getCtrlListOutputColor()));
	source.updateNaoView();
}

/** TODO handle flow type new names **/
function isU2H(flow_type){return isX(flow_type, "User 2 Host") | isX(flow_type, "U2H") | isX(flow_type, "User authenticated dataflow");}
function isEncryption(action){return hasX(action, "encryption");}
function isUserAuthentication(action){return hasX(action, "authentication");}

/** caution :  take only flow_action into account **/
function isNao()
{
	var flow_action = getSelectE("flow-action").value;
	return isEncryption(flow_action) || isUserAuthentication(flow_action);
}

function getNaoId()
{
	if (!isNao())
		return "";
		
	var id = "";
	var flow_action = getSelectE("flow-action").value;
	if (isEncryption(flow_action))
		return "src-nao-enc";
	else if (isUserAuthentication(flow_action))
		return "src-nao-auth";
}

function clearProfile(grade)
{
	if (grade<=1)
		getE("profile").value = "";
	if (grade<=2)
	{
		getE("profile-owner").value 		= "";
		getE("profile-description").value 	= "";
		
		jq_hide("profile-error");
	}
}

/**
	@see fcrweb_control_object.changeOrganization()
**/
function set_org_list_elements(request)
{
	var type 		= request['data']['type'];
	var host_name 	= request['json'].host_name;
	var subnet_mask = request['json'].subnet_mask;
	var goo 		= request['json'].goo;

	if (type=="src")
	{
		addNewAutoSuggest(type + "-host", host_name, changeSrcHost);
		addNewAutoSuggest(type + "-ip", request['json'].host_ip, changeSrcIp);
		addNewAutoSuggest(type + "-subnet", subnet_mask, changeSrcSubnet);
		addNewAutoSuggest(type + "-goo", goo, changeSrcGOO);	
		addNewAutoSuggest(type + "-goo-selector", goo, changeSrcGOO_sel);
		//addNewAutoSuggest(type + "-subnet-area", request['json'].area, changeSrcSubnetArea);
	}
	else
	{
		addNewAutoSuggest(type + "-host", host_name, changeDestHost);
		addNewAutoSuggest(type + "-ip", request['json'].host_ip, changeDestIp);
		addNewAutoSuggest(type + "-subnet", subnet_mask, changeDestSubnet);
		addNewAutoSuggest(type + "-goo", goo, changeDestGOO);
		addNewAutoSuggest(type + "-goo-selector", goo, changeDestGOO_sel);
		//addNewAutoSuggest(type + "-subnet-area", request['json'].area, changeDestSubnetArea);	
	}
	
	householder["list-" + type + "-host-autosuggest"].do_force_reposition 			= true;
	householder["list-" + type + "-ip-autosuggest"].do_force_reposition 			= true;
	householder["list-" + type + "-subnet-autosuggest"].do_force_reposition 		= true;
	householder["list-" + type + "-goo-selector-autosuggest"].do_force_reposition 	= true;

	addNewHouseHoldList(type + "-global-goo", request['json'].global_goo);

	modal_off();
}

/** deprecated **/
function set_org_list_elements_and_update_selections(request)
{
	set_org_list_elements(request);
	
	var type = request['data']['type'];
	if (isX(type, "src"))
		source.end_open_edit();
	else
		destination.end_open_edit();
}

function setChangeApplicationService()
{
	if (isX(getE('debug-area').value,'true') || confirm("Warning: changing the application service context implies all defined flows to be cleared, do you want to continue ?"))
	{
		getE("application-service").value = "";
		jq_enabled("application-service");
		clearFlowDefinition();
	}
}
function clearFlowDefinition()
{
	clearFlowInfo();
	flow.leastOneFlow();			
	flow.refreshFlowsView();
}

function clearAdvancedForm()
{
	var next = "";
	getE("on-behalf-of").value 				= "";
	getE("application-service").value 		= "";
	setSelectE("request-context", default_choice);
	get_list_context_detail();
	getE("context-detail").value 			= "";
	getE("detailed-reason-request").value 	= "";
}

function changeApplicationService()
{
	var id = "application-service";

	if (isInSuggestionList(id, getE(id).value))
	{
		if (isX(getE("form-selector").value, "advanced"))
		{
			modal_on();
			send_request
			(
				{
					'method':"GET",
					'url':default_url,
					'callback':set_app_srv_list_elements,
					'data':
					{
						'action':access("list-application-service-elements"),
						'application-service':getE(id).value
					}
				}
			);
		}
	}
}

function set_app_srv_list_elements(request)
{
	modal_off();
	addNewAutoSuggest("profile", request['json'].profile, changeProfile);
	addNewAutoSuggest("srv-one-name", request['json'].srv, changeService);
	addNewAutoSuggest("srv-one-port", request['json'].srv_port, changeSrvPort);
	addNewAutoSuggest("srv-gsrv-name", request['json'].gsrv, changeGroupOfServices);
	addNewHouseHoldList("srv-global-gsrv", request['json'].global_gsrv);

	setHtml("src-nao-auth", list2options(request['json'].goo_authentication, true));
	setHtml("src-nao-enc", list2options(request['json'].goo_encryption, true));
	householder["src-nao-auth-map"] = request['json'].goo_authentication_map;
	householder["src-nao-enc-map"] 	= request['json'].goo_encryption_map;
	
}

function hide_src_nao_Errors()
{
	jq_hide("src-nao-errors");
}

function list2options(list, add_default)
{
	var options = add_default ? "<option value=''>" + default_choice + "</option>" : "";
	for (var i=0 ; i<list.length ; i++)
		options += "<option value='" + list[i] + "'>" + list[i] + "</option>"
	return options;
}

function changeProfile()
{
	var id = "profile";
	clearProfile(2);
	var isInList = doAutocompleteOnValueChange
	(
		"profile", 
		getE('profile').value,
		{"element":'profile',"name":getE('profile').value,"application-service":getE('application-service').value}, 
		function(request)
		{
			getE('profile-owner').value = request['json'].owner;
			getE('profile-description').value = request['json'].description;
			getLdapUsers("profile-owner");
		}
	)
	
	if (isInList)
	{	
		jq_disabled("control-profile-autocomplete");
	}
	else
	{
		jq_enabled("control-profile-autocomplete");
	}
}

function flowControlFocusOut(id)
{
	if (hasX(id, "src"))
	{
		if (hasX(id, "host"))changeSrcHost();
		else if (hasX(id, "ip"))changeSrcIp();
		//else if (hasX(id, "subnet-area"))changeSrcSubnetArea();
		else if (hasX(id, "subnet"))changeSrcSubnet();		
		else if (hasX(id, "goo-selector"))changeSrcGOO_sel();
		else if (hasX(id, "goo"))changeSrcGOO();
	}
	else if (hasX(id, "dest"))
	{
		if (hasX(id, "host"))changeDestHost();
		else if (hasX(id, "ip"))changeDestIp();
		//else if (hasX(id, "subnet-area"))changeDestSubnetArea();
		else if (hasX(id, "subnet"))changeDestSubnet();		
		else if (hasX(id, "goo-selector"))changeDestGOO_sel();
		else if (hasX(id, "goo"))changeDestGOO();
	}
	else if (hasX(id, "srv"))
	{
		if (hasX(id, "one"))changeService();
		else if (hasX(id, "gsrv"))changeGroupOfServices();
	}
}

function clearFlowInfo()
{
	removeAutoSuggest("profile");
	removeAutoSuggest("srv-one-name");
	removeAutoSuggest("srv-gsrv-name");
	removeAutoSuggest("src-host");
	removeAutoSuggest("src-subnet");
	//removeAutoSuggest("src-subnet-area");
	removeAutoSuggest("src-goo");
	removeAutoSuggest("dest-host");
	removeAutoSuggest("dest-subnet");
	//removeAutoSuggest("dest-subnet-area");
	removeAutoSuggest("dest-goo");
	initialize_flows_src_dest_srv();
}

function changeSrcHost(){source.changeHost();}
function changeSrcIp(){source.changeIp();}
function changeSrcSubnet(){source.changeSubnet();}
function changeSrcSubnetArea(){source.changeSubnetArea();}
function changeSrcGOO(){source.changeGOO();}
function changeSrcGOO_sel(){source.changeGOO_sel();}
function changeDestHost(){destination.changeHost();}
function changeDestIp(){destination.changeIp();}
function changeDestSubnet(){destination.changeSubnet();}
function changeDestSubnetArea(){destination.changeSubnetArea();}
function changeDestGOO(){destination.changeGOO();}
function changeDestGOO_sel(){destination.changeGOO_sel();}
function changeService(){service.changeService();}
function changeSrvProto(){service.changeProto();}
function changeSrvPort(){service.changePort();}
function changeGroupOfServices(){service.changeGroupOfServices();}


function checkExternalIpRanges(request)
{
	modal_off();
	var type 	= request['data']['type'];
	var elem 	= request['data']['element'];
	var code	= request['json']['statusCode'];
	getE(type + "-" + elem + "-code").value = code;

	if (isX(code, "1"))
	{
		hide(type + "-" + elem + "-error");
		show(type + "-" + elem + "-check", "inline");
		hideError(type + "-" + elem);		
		hideError(type + "-" + elem + "-opt");
	}
	else if (isX(code, "-1"))
	{
		hide(type + "-" + elem + "-check");
		show(type + "-" + elem + "-error", "inline");
		getE(type + "-" + elem + "-code").value = request['json']['statusCode'];
		hideError(type + "-" + elem);
		setError(type + "-" + elem + "-opt", "the defined " + elem + " address belongs to a private range of addresses.");
		showError(type + "-" + elem + "-opt");
		
		jq_hide("control-create");
	}
}


function checkPortConflicts(id, port_range_chk)
{
	var list = getHouseHoldList(id);
	if (!isPortOrPortRange(port_range_chk))
		return false;
	else if (list==null)
		return false;
	
	var isRange_chk	= isPortRange(port_range_chk, default_port_range_separator);
	var p_chk_low	= 0;
	var p_chk_up	= 0;
	
	if (isRange_chk)
	{
		var ports_chk 	= replaceAllIn(port_range_chk, default_port_range_separator, "_").split("_");
		p_chk_low 		= getInt(ports_chk[0]);
		p_chk_up 		= getInt(ports_chk[1]);
	}
	else
	{
		p_chk_low = p_chk_up = getInt(port_range_chk);
	}
	
	for (var port_range in list)
	{
		if (isX(port_range_chk, port_range))
		{
			/** interrupt **/
			return false;
		}
		else if (isPortOrPortRange(port_range))
		{
			var isRange = isPortRange(port_range, default_port_range_separator);			
			var p_low	= 0;
			var p_up 	= 0;
			
			if (isRange)
			{
				var ports 	= replaceAllIn(port_range, default_port_range_separator, "_").split("_");
				p_low 		= getInt(ports[0]);
				p_up 		= getInt(ports[1]);
			}
			else
			{
				p_low = p_up = getInt(port_range);
			}
			
			if ((p_low<=p_chk_low & p_chk_low<=p_up) | (p_low<=p_chk_up & p_chk_up<=p_up))
			{
				return true;
			}
		}
	}
	return false;
}

function openGrp(fw_name)
{
	//mask_on();
	jq_hide("grp-group-members");
	show("members-" + fw_name);
	show("display-group-members");	
}


function getActionColor(action)
{
	var color_map = 
	{
		"use":'blue',
		"add":'blue',
		"update":'blue',
		"rename":'orange',
		
		"create":'green',
		
		"remove":'red',
		"delete":'red',
		
		"modify":'purple'
	};
	
	if (!isEmpty(color_map[action]))
		return color_map[action];
	/** abnormal case yellow **/
	return "yellow";
}

var cache = new Object();

function doAutocompleteOnValueChange(id, value, value_map,compl_func)
{
	if (isInSuggestionList(id, value))
	{
		value_map['action'] = access('autocomplete');
		var cache_id = "ac---" + id + "---" + value;
		if (cache[cache_id]==null)
		{			
			logout(cache_id);
			cache[cache_id] = true;
			modal_on();
			send_request
			(
				{
					"method":'GET',
					"url":default_url,
					"data":value_map,
					"callback":compl_func,
					"pre-cbk":function(){logout("pre-cbk");modal_off();cache[cache_id] = false;}
				}
			);
		}
		return true;
	}
	else 
		return false;
}

/**
	@pre map and color_map has exact same keys
**/
function map2select_options(map, color_map)
{
	var options = "";
	
	for (var key in map)
	{
		var style_color = isEmpty(color_map[key]) ? "" : "style='color:" + color_map[key] + "'";
		options += "<option value='" + key + "' " + style_color + " >" + map[key] + "</option>";
	}
	return options;
}


var small_br = "<div style='height:1px;font-size:1px;width:120px;'>&nbsp;</div>";
function map2chain(map, opt_fun)
{
	var values = "";
	for (var key in map)
	{
		var value 	= (opt_fun!=null) ? opt_fun(map[key]) : map[key];
		values += !isEmpty(values) ? small_br + value : value;
	}
	return values;
}

var default_limit_string = 15;
function limitString(val, limit)
{
	if (limit==null)limit = default_limit_string;
	return (!isEmpty(val) && val.length>limit+3) ? val.substring(0,limit) + "..." : val;
}

function map2chain_limit(map){return map2chain(map, limitString);}



function isEmptyOrDefault(value){return isEmpty(value) || isX(value, default_choice) || isX(value, default_choice_sh);}

function getOrgShort(org)
{
	if (isX(org, "mobistar"))return "mob";
	else if (isX(org, "mes"))return "mes";
	else if (isX(org, "olu"))return "olu";
	else if (isX(org, "external"))return "ext";
	return "?";
}

function getEnvProposition(env)
{
	if (isEmpty(env))
		return "Prod";
	else if (hasX(env, "non") & hasX(env, "prod"))
		return "nonProd";
	else
		return "Prod";
}

function normalize(value)
{
	return replaceAllIn(value, " ", "_");
}

function get_fw_rule_id(flow_id, rule_idx)
{
	var fw = getSelectE("f-" + flow_id + "-rule-" + rule_idx + "-fw").value;
	if (isEmptyOrDefault(fw))
		getE("f-" + flow_id + "-rule-" + rule_idx + "-rule-id").value = "";
	else
	{
		modal_on();
		send_request
		(
			{
				"method":'GET',
				"url":default_url,
				"data":
				{
					"action":access('fw-next-rule-id'),
					"fw":fw,
					"flow-id":flow_id,
					"rule-idx":rule_idx
				},
				"callback":setFwRuleId
			}
		)
	}	
	flow.setIdx(flow_id);
	flow.addFlowInfo("rule-" + rule_idx + "-fw" , fw);
	
}

function setFwRuleId(request)
{
	modal_off();
	var next_rule_id 	= request['json'].next_rule_id;
	var flow_id 		= request['data']['flow-id'];
	var rule_idx 		= request['data']['rule-idx'];
	getE("f-" + flow_id + "-rule-" + rule_idx + "-rule-id").value = next_rule_id;
	updateFlowRuleId(flow_id, rule_idx);
}

function flag(idx)
{
	var id = "f-" + idx + "-flag";
	flow.setIdx(idx);
	var meta = meta_params2map(flow.getFlowInfo("meta"));	
	meta['flag'] = getE(id).checked ? "1" : "0";
	flow.addFlowInfo("meta", meta_map2params(meta));
}


function updateFlowRuleId(flow_id, rule_idx)
{
	flow.setIdx(flow_id);
	flow.addFlowInfo("rule-" + rule_idx + "-rule-id" , getE("f-" + flow_id + "-rule-" + rule_idx + "-rule-id").value);
}

function isFWO(){return getE("fwo").value == "true";}
function isChangeRule(){return getE("change-rule").value == "true";}
function isTSRR(){return getE("tsrr").value == "true";}

function openWindow()
{
var myWindow = window.open("", "_blank");
myWindow.document.write("<p>I replaced the current window.</p>");
}

/**
	DEBUG TOOL
*/
function showList(id)
{
	if (!isEmpty(id))
		setHtml("debug-section", getHtml("debug-section") + "<br/>CACHE:" + householder["list-" + id + "-elements"] + "<br/>AUTO:" + householder["list-" + id + "-autosuggest"].keywords)
	else
		showError("debug-list-selection");
}


function checkDebugTool()
{
	if (hasX(document.location.href,"debug-mode-on"))
	{
		show("debug-tool");
		usedebug = true;
	}
}

function toggleLdapGroup(group)
{
	send_request({"method":'GET',"url":default_url,"data":{"action":debug_action('toggleLdapGroup'),"group":group},"callback":doNothing})
}

function setCelsiusUser()
{
	send_request({"method":'GET',"url":default_url,"data":{"action":debug_action('setCelsiusUser'),"celsiusUser":getE("debug-area").value},"callback":doNothing})
}

function updateCatalog()
{
	send_request
	(
		{
			"method":'GET',
			"url":default_url,
			"data":
			{
				"action":debug_action('update-catalog'),
				"form-id":getE("form-id").value,
				"type":'fcr'
			},
			"callback":doNothing
		}
	)
}

function clearDT(){setHtml("debug-section", "");}

function ant()
{
	try
	{
		setTimeout(getE("debug-area").value, 0);
	}
	catch(e){notify(e);}
}

function showAefs()
{
	var values = "";
	values += "\/*** FLOW(S) ***\/\n";
	for(var k in flow.elems)values += "addAefEntry('" + k + "','" + flow.elems[k] + "');\n";
	values += "\/*** SOURCE(S) ***\/\n";
	for(var k in source.elems)values += "addAefEntry('" + k + "','" + source.elems[k] + "');\n";
	values += "\/*** DESTINATION(S) ***\/\n";
	for(var k in destination.elems)values += "addAefEntry('" + k + "','" + destination.elems[k] + "');\n";
	values += "\/*** SERVICE(S) ***\/\n";
	for(var k in service.elems)values += "addAefEntry('" + k + "','" + service.elems[k] + "');\n";
	values += "/*";
	values += "*** FLOW(S) ***\n";
	values += flow.toHtmlParams() + "\n";
	values += "*** SOURCE(S) ***\n";
	values += source.toHtmlParams() + "\n";
	values += "*** DESTINATION(S) ***\n";
	values += destination.toHtmlParams() + "\n";
	values += "*** SERVICE(S) ***\n";
	values += service.toHtmlParams() + "\n";
	values += "*/";
	/**/
	getE("debug-area").value = values;
}


function set_select_ro_inputs()
{
	if (isX(getE("readonly").value, "false"))
		hide_select_ro_inputs("");
	else
		show_select_ro_inputs("");
}

function show_select_ro_inputs(classNames)
{
	classNames = ".control-select.control-editable" + (!isEmpty(classNames) ? classNames : "")
	jQuery(classNames).each(function( index ) 
	{
		var elem_id = jQuery(this).attr("id");
		try
		{	
			var selection = getSelectE(elem_id).value;//$('#' + elem_id).val()
			$('#' + elem_id + "-ro").val(selection);
			$('#' + elem_id).hide();
			$('#' + elem_id + "-ro").css('display','inline');
		}
		catch (e)
		{
		}
	});
}

function hide_select_ro_inputs(classNames)
{
	classNames = ".control-select.control-editable" + (!isEmpty(classNames) ? classNames : "")

	jQuery(classNames).each(function( index ) 
	{
		try
		{
			var elem_id = jQuery(this).attr("id");
			$('#' + elem_id + "-ro").hide();
			$('#' + elem_id).css('display','inline');
		}
		catch (e){}
	});
}

function isFox()
{
	return hasX($('#userAgent').val(),'firefox');
}

function isChrome()
{
	return hasX($('#userAgent').val(),'chrome');
}

function isIE(){return isX($("#ie").val(),"true");}


function disable_autocomplete_fields(className)
{
	jq_disabled(className);
	show_select_ro_inputs('.' + className);	
}

function enable_autocomplete_fields(className)
{
	jq_enabled(className);
	hide_select_ro_inputs('.' + className);
}

/** type:source|destination|service*/
function dragMe()
{
var type = "source"
	ZWDrag.makeDraggable( getE(type + '-dialog'), getE(type + '-dialog-header') );
}

function log(x)
{
	getE("debug-area").value += x + "\n";
}

function logout(x)
{
	setHtml("debug-section", getHtml("debug-section") + "<br/>" + x);
}

function check_HH_keys()
{
	for (var k in householder)
		logout(k);
}

function DF()
{
	var elems = getE("debug-area").value.split(",");

	if ((elems.length==2) && (!isEmpty(elems[0]) & !isEmpty(elems[1])))
	{
		if (isX(getE('debug-area').value,'true') || confirm("df from " + elems[0] + " to " + elems[1]))
		{
			send_request({"method":'GET',"url":default_url,"data":{"action":debug_action('duplicate-form'),"form-id":elems[1],"dup-form-id":elems[0]},"callback":doNothing})
		}
	}
	
}

function initFWN()
{
	send_request
	(
		{
			"method":'GET',
			"url":default_url,
			"data":
			{
				"action":debug_action('init-firewall-name'),
				"pwd":'debug-mode-on-init-firewall-name'
			},
			"callback":doNothing
		}
	)
}

function desa(element,attribut) 
{
	var test = document.createElement(element);
	if (attribut in test) {
	logout("Element " + element + " support attribute " + attribut);
	} else {
	logout("Element " + element + " DO NOT support attribute " + attribut);
	}
}


function ldapMe()
{
	send_request
	(
		{
			"method":'GET',
			"url":default_url,
			"data":
			{
				"action":test_action('ldap-action'),
				"values":getE("debug-area").value
			},
			"callback":doNothing
		}
	)
}

function test_WS(test_type)
{
	send_request
	(
		{
			"method":'GET',
			"url":default_url,
			"data":
			{
				"action":test_action('ws-action'),
				"ws":test_type
			},
			"callback":doNothing
		}
	)
}

function addSampleFlows()
{
	initialize_flows_src_dest_srv();
	doAddSamples();
	switchFormEdition('advanced', true);
	flow.refreshFlowsView();
}

function addAefEntry(key, value)
{
	if (hasX(key, "nb") || hasX(key, "idx"))
		value = getInt(value);
	if (hasX(key, "src"))
		source.elems[key] = value;
	else if (hasX(key, "dest"))
		destination.elems[key] = value;
	else if (hasX(key, "srv"))
		service.elems[key] = value;
	else
		flow.elems[key] = value;
}

function doAddSamples()
{
/*** FLOW(S) ***
addAefEntry('f-nb-flow','2');
addAefEntry('f-idx','2');
addAefEntry('f-1-dataflow-type','User 2 Host');
addAefEntry('f-1-profile-description','good to know');
addAefEntry('f-1-flow-description','popucho');
addAefEntry('f-2-profile-owner','gmanche');
addAefEntry('f-1-profile','gtk');
addAefEntry('f-2-dataflow-type','User 2 Host');
addAefEntry('f-2-flow-action','Encryption (VPN)');
addAefEntry('f-1-fw','');
addAefEntry('f-1-profile-owner','jhuilian');
addAefEntry('f-2-profile','ht');
addAefEntry('f-1-flow-id','10839');
addAefEntry('f-2-flow-description','pokito');
addAefEntry('f-2-rule-id','');
addAefEntry('f-2-flow-id','10840');
addAefEntry('f-2-fw','');
addAefEntry('f-1-rule-id','');
addAefEntry('f-2-profile-description','how to');
addAefEntry('f-1-flow-action','User authentication');
addAefEntry('f-1-end-date','');
addAefEntry('f-2-profile-uc','create');
addAefEntry('f-1-profile-uc','create');
addAefEntry('f-2-end-date','');
/*** SOURCE(S) ***
addAefEntry('f-1.1-src-uc','use');
addAefEntry('f-1.1-src-cat','nao');
addAefEntry('f-2-nb-src','0');
addAefEntry('f-1.1-src-output-opt','gtk@mob-appVirtElehnaMettal-nonProd-grp');
addAefEntry('f-1.1-src-output','mob-appVirtElehnaMettal-nonProd-grp');
addAefEntry('f-1-src-idx','1');
addAefEntry('f-2-src-idx','-1');
addAefEntry('f-1.1-src-name','appVirtElehnaMettal');
addAefEntry('f-1-nb-src','1');
/*** DESTINATION(S) ***
addAefEntry('f-1.1-dest-output','olu-net-20.0.10.0-32');
addAefEntry('f-1.1-dest-uc','use');
addAefEntry('f-2.1-dest-cat','Host');
addAefEntry('f-2.1-dest-ip-tsl','');
addAefEntry('f-2.1-dest-host','ampere');
addAefEntry('f-2.1-dest-org','MOBISTAR');
addAefEntry('f-2-nb-dest','1');
addAefEntry('f-1.1-dest-cat','Subnet');
addAefEntry('f-1.1-dest-subnet','20.0.10.0/32');
addAefEntry('f-1-nb-dest','1');
addAefEntry('f-1-dest-idx','1');
addAefEntry('f-2-dest-idx','1');
addAefEntry('f-2.1-dest-output','mob-host-ampere-Prod-10.0.0.11');
addAefEntry('f-2.1-dest-host-env','Prod');
addAefEntry('f-1.1-dest-org','OLU');
addAefEntry('f-2.1-dest-ip','10.0.0.11');
addAefEntry('f-2.1-dest-uc','use');
addAefEntry('f-1.1-dest-subnet-area','olu massachusetts');
/*** SERVICE(S) ***
addAefEntry('f-1-nb-srv','1');
addAefEntry('f-2.1-srv-output','tcp-SSH_&_SFTP-22');
addAefEntry('f-2.1-srv-one-port','22');
addAefEntry('f-2.1-srv-cat','Single service');
addAefEntry('f-1-srv-idx','1');
addAefEntry('f-2-srv-idx','1');
addAefEntry('f-2-nb-srv','1');
addAefEntry('f-2.1-srv-uc','use');
addAefEntry('f-1.1-srv-output','tcp-HTTPS-443');
addAefEntry('f-1.1-srv-one-port','443');
addAefEntry('f-1.1-srv-uc','use');
addAefEntry('f-1.1-srv-one-proto','tcp');
addAefEntry('f-1.1-srv-cat','Single service');
addAefEntry('f-2.1-srv-one-type','-');
addAefEntry('f-1.1-srv-one-type','-');
addAefEntry('f-1.1-srv-one-name','HTTPS');
addAefEntry('f-2.1-srv-one-proto','tcp');
addAefEntry('f-2.1-srv-one-name','SSH & SFTP');
*/

/*** FLOW(S) ***/
addAefEntry('f-nb-flow','0');
addAefEntry('f-idx','1');
/*** SOURCE(S) ***/
addAefEntry('f-1-src-idx','2');
addAefEntry('f-1-nb-src','2');
addAefEntry('f-1.1-src-org','OLU');
addAefEntry('f-1.1-src-cat','Host');
addAefEntry('f-1.1-src-host','tolstoi');
addAefEntry('f-1.1-src-ip','100.0.0.0');
addAefEntry('f-1.1-src-ip-tsl','');
addAefEntry('f-1.1-src-host-env','');
addAefEntry('f-1.1-src-uc','create');
addAefEntry('f-1.1-src-output','olu-host-tolstoi--100.0.0.0');
addAefEntry('f-1.2-src-org','OLU');
addAefEntry('f-1.2-src-cat','Group of objects');
addAefEntry('f-1.2-src-goo','guns');
addAefEntry('f-1.2-src-goo-env','nonProd');
addAefEntry('f-1.2-src-hosts','cat---host:::host---sigsauer34:::ip---100.0.1.1:::ip-tsl---:::env---nonProd:::uc---create:::id---olu-host-sigsauer34-nonProd-100.0.1.1');
addAefEntry('f-1.2-src-subnets','cat---subnet:::subnet---100.0.1.0/1:::area---olu massachusetts:::uc---create:::id---olu-net-100.0.1.0-1');
addAefEntry('f-1.2-src-goos','cat---goo:::goo---olu bin:::env---Prod:::uc---use:::id---olu-olu_bin-Prod-grp');
addAefEntry('f-1.2-src-uc','create');
addAefEntry('f-1.2-src-output','olu-guns-nonProd-grp');
/*** DESTINATION(S) ***/
addAefEntry('f-1-dest-idx','1');
addAefEntry('f-1-nb-dest','1');
addAefEntry('f-1.1-dest-org','OLU');
addAefEntry('f-1.1-dest-cat','Subnet');
addAefEntry('f-1.1-dest-subnet','100.0.0.0/1');
addAefEntry('f-1.1-dest-subnet-area','olu massachusetts');
addAefEntry('f-1.1-dest-uc','create');
addAefEntry('f-1.1-dest-output','olu-net-100.0.0.0-1');
/*** SERVICE(S) ***/
addAefEntry('f-1-srv-idx','1');
addAefEntry('f-1-nb-srv','1');
addAefEntry('f-1.1-srv-cat','Single service');
addAefEntry('f-1.1-srv-one-name','HTTP');
addAefEntry('f-1.1-srv-one-type','-');
addAefEntry('f-1.1-srv-one-port','80');
addAefEntry('f-1.1-srv-one-proto','tcp');
addAefEntry('f-1.1-srv-uc','use');
addAefEntry('f-1.1-srv-output','tcp-HTTP-80');
}



