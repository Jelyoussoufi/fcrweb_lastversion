/**
 * TABLE ELEMENT
 * IMG ELEMENT
 * HTML RESPONSE
 * SELECT ELEMENT
 * AJAX FUNCTIONS 
 **/
 
var usedebug 	= false;
var xmlHttps 	= new Object();
/** request template
 send_request({"method":'GET',"url":fcr('actionName'),"data":map,"callback":func})
*/

function setUsedebug(val)
{
	usedebug = val;
}
function debug(s){if(usedebug)alert(s);}
function debugError(elems, message)
{
	if ((elems==null)  && (atCelsius | usedebug))
	{	
		document.getElementById("debug-section").innerHTML = message + " is null";
	}
}

function notify(s){alert(s);}
function notifyEmpty(val){notify(val + " could not be empty !");}

function getE(id)
{
	var elem = document.getElementById(id);
	debugError(elem, "getE(" + id + ")");
	return elem;
}

function getEN(groupName)
{
	var elems = document.getElementsByName(groupName);
	debugError(elems, "getEN(" + groupName + ")");
	return elems;
}

function getET(tagName)
{
	var elems = document.getElementsByTagName(tagName);
	debugError(elems, "getET(" + tagName + ")");
	return elems;
}




function focus(id)
{
	try{getE(id).focus();}catch(e){}
}

/*
	onKeyUp="javascript:this.value=trim(this.value); doEnterKeyPress(event,performDiagnosis);"
*/
function doEnterKeyPress(evt, dofunc)
{
	evt = (evt) ? evt : ((window.event) ? window.event : "");

	if(evt) 
	{
		var keyCode = (evt.keyCode) ? evt.keyCode : evt.which;
		if(keyCode == 13) dofunc();
	}
	
}

function isEvent(evt, touch)
{
	evt = (evt) ? evt : ((window.event) ? window.event : "");

	if(evt) 
	{
		var keyCode = (evt.keyCode) ? evt.keyCode : evt.which;
		if(keyCode == touch)
			return true;
	}
	return false;
}

function isEnterEvent(evt){return isEvent(evt, 13);}




function doOnPaste(func)
{
	setTimeout(func,0);
}

/**
JQUERY FUNCTIONS
need to import jquery or jquery.min
*/

function jq_hide(className){jq_show_oth(className, "none")}
function jq_show(className){jq_show_oth(className, "")}
function jq_show_b(className){jq_show_oth(className, "block")}
function jq_show_i(className){jq_show_oth(className, "inline")}
function jq_show_oth(className, newStyle){jq_css(className, 'display', newStyle)}
function jq_css(className, styleAttr, newStyle){jQuery("." + className).css(styleAttr, newStyle);}

function jq_disabled(className){jQuery("." + className).attr("disabled", true);jQuery("#" + className).attr("disabled", true);}
function jq_enabled(className){jQuery("." + className).attr("disabled", false);jQuery("#" + className).attr("disabled", false);}


function show(id, newStyle){try{getE(id).style.display = isEmpty(newStyle) ? "" : newStyle;}catch(e){logout("cannot hide element " + id)}}
function hide(id)
{
	try{
	getE(id).style.display = "none";
	}
	catch(e){logout("cannot hide element " + id);}
}
function setHtml(id, text)
{
	try{
		//getE(id).innerHTML = text;
		jQuery("#" + id).html(text);
	}
	catch(e)
	{
		logout("elem " + id + " is null");
	}
}
function getHtml(id){return getE(id).innerHTML;}

function setReadonly(id, val){getE(id).readOnly = val;}
function setSrc(id, val){getE(id).src = val;}
function setHref(id, val){getE(id).href = val;}

function getUri(){return document.location.href;}
function setUri(uri){document.location.href = uri}

/*******************
 *	TABLE ELEMENT  *
 *******************/
function cn(className){return " class='" + className + "' ";}
function addE(tag, header, body){return "<" + tag + " " + header + ">" + body + "</" + tag + ">";}
function div(header,body){return addE("div", header, body);}
function span(header,body){return addE("span", header, body);}
function tb(header,body){return addE("table", header, body);}
function thead(header,body){return addE("thead", header, body);}
function th(header,body){return addE("th", header, body);}
function tbody(header,body){return addE("tbody", header, body);}
function tr(header,body){return addE("tr", header, body);}
function td(header,body){return addE("td", header, body);}
function pg(header,body){return addE("p", header, body);}
function ahref(href, header, body)
{
	if (href.indexOf(".jsp")==-1)	// not .jsp extension means there is no redirection and then the href must be a javascript function
		href = "javascript:" + href;
	return addE("a", "href='" + href + "'" + header, body);
}
function hidden(id,value){return addE("input", "type='hidden' id='" + id + "' value='" + value + "'", "");}
function chkbx(id, value, group){return addE("input", "id='" + id + "' value='" + value + "' ")}

/**
	deprecated
*/
function handClick(href, clickAction, elem)
{
	return handClickOpts("href='" + href + "'", "onclick='" + clickAction + "'", elem);
}

/**
	deprecated
*/
function handClickOpts(aOptions, buttonOptions, elem)
{
	return addE("a", aOptions, span(buttonOptions, elem));
}

/*******************
 *	IMAGE ELEMENT  *
 *******************/
var imgNotEq 		= getSrcImage("noteq.png");
var imgProg 		= getSrcImage("progress3.gif");
var imgInnerProg 	= getSrcImage("progress4.gif");
var imgAdd			= getSrcImage("addIcon.png");
var imgCancel		= getSrcImage("cancelIcon.png");


function img(style, src, ttl, fct)
{
	return "<img " + style + " src='" + src + "' title='" + ttl + "' onclick=" + fct + " ></img>";
}

function imgLoad(id)
{
	getE(id).src = imgProg;
}

function imgIdle(id)
{
	hide(id);
}

function getSrcImage(imgName)
{
	try{ return getE('contextPath').value + "/ressources/" + imgName;}
	catch(e){return "";}
}

/*******************
 *	HTML RESPONSE  *
 *******************/

/**
 *	methods to access a xml structure.
 */
function getNodeNull(node,attr)	// null value admissible
{
	try
	{
		var node = node.getElementsByTagName(tag);
		return node
	}
	catch (err)
	{
		return null;
	}
}

function getNode(node,attr)	//null value not expected.
{
	try
	{
		var elem = node.getElementsByTagName(tag);
		return elem;
	}
	catch (err)
	{
		debug("getNode ERROR: "+attr+" doesn't exists");
	}
}

function nodeValueNF(node, tag, attr) //null value not expected.
{
	try
	{
		var element = node.getElementsByTagName(tag);
		return element[0].getAttribute(attr);
	}
	catch (err)
	{
		debug("nodeValue ERROR: attribute "+attr+" doesn't exists for "+tag);
		return "";
	}
}

function nodeValue(node, tag, attr) //null value not expected.
{
	try
	{
		var element = node.getElementsByTagName(tag);
		return undoFilter(element[0].getAttribute(attr));
	}
	catch (err)
	{
		debug("nodeValue ERROR: attribute "+attr+" doesn't exists for "+tag);
		return "";
	}
}

/**
 *	first method to call before accessing a json structure.
 */
function getJson(json)
{
	try
	{
		return JSON.parse(undoFilter(json));
	}
	catch(e){}
	return null;
}
var json_path_sep = "/";
function setJsonPathSep(path_sep){json_path_sep = path_sep;}
function jsonNode(root, path)
{
	var expr = "";
	try
	{
		var node 	= root;
		var exprs 	= path.split(json_path_sep);
		var idx 	= 0;
		while (idx!=exprs.length)
		{
			expr = exprs[idx];
			node = node[expr];				
			idx++;
		}
	}
	catch(e){}
	//debugError(node, "Error on jsonNode:" + path + " at node:" + expr);
	return node;
}

/*******************
 *	INPUT ELEMENT  *
 *******************/

function setDisabled(id, disabled)
{
	var bc = "white";
	var fc = ""
	if (disabled)
	{
		bc = "#666666";
		fc = "#CCCCCC";
	}
	getE(id).style.color 			= fc;
	getE(id).style.backgroundColor 	= bc;
	getE(id).disabled 				= disabled;
}

/****************************
 *	AUTO SUGGEST FUNCTIONS  *
 ****************************/
function setAutoSuggest(id, array, onchangeFunc)
{
	//need lib autosuggest.js and autosuggest.css
	//id the identifier of input text which must be turned into autosuggest
	//array the autosuggest values.
	//onchangeFunc onchange function(index, control){}
	//control.keywords[index] selected key
	//control.values[index] selected value
	var url = ""; // url where values can be caught.
	new autosuggest(id, array, url, onchangeFunc);
}

/********************** 
 *	CHECKBOX ELEMENT  * 
 **********************/

function setCheck(id, val)
{
	getE(id).checked = val;
}

function getCheck(id)
{
	return getE(id).checked;
}

function getCheckedRadioId(groupName) 
{
    var elements = document.getElementsByName(groupName);
    for (var i=0; i<elements.length; ++i)
        if (elements[i].checked) return elements[i];
}
/********************
 *	SELECT ELEMENT  *
 ********************/


function getSelectE(id)
{
	var opt = getE(id).options[getE(id).selectedIndex];
	return opt!=null ? opt : getDefaultObject();
}

function setSelectE(id, value)
{
$('#' + id).find('option').removeAttr('selected');
	if (isEmpty(value)) return;
	var list = getE(id);
	for (var intI = 0; intI < list.length; intI++) 
	{
		if 
		(
			isX(list.options[intI].value,value)||
			isX(list.options[intI].text,value)||
			isX(list.options[intI].id,value)
		)
		{
			list.options[intI].selected = true;
		}
	}
//$('#' + id).val(value);
}

function setSelectEC(id,value)
{
	setSelectE(id, value);
	try{
		if (getE(id).onchange!=null)
			getE(id).onchange();
	}catch(e){}
}

function setSelectIndex(id, idx)
{
	var list = getE(id);
	list.options[idx].selected = true;
}

function optionExist(selectorId, value)
{
	var options = getE(selectorId).options;
	for (var i=0; i<options.length; i++)
		if (isX(options[i].value,value)||isX(options[i].text,value)||isX(options[i].id,value))return true;
	return false;
}

function clearOptions(selectorId)
{
	getE(selectorId).options.length = 0;
}


function addOptionF(id, text, val, options){addOption(id, text, val, 0, options);}
function addOptionL(id, text, val, options){addOption(id, text, val, getE(id).options.length, options);}

function addOption(id, text, val, idx, options)
{
	var option = new Option(text, val);
	
	if (options!=null)
	{
		if (isX(options["disabled"],"true"))
		{
			option.disabled = true;
			option.style += "color:" + (!isEmpty(options["font-color"]) ? options["font-color"] : "grey") + ";";			
		}
		if (!isEmpty(options["class"]))
			option.className += options["class"];
	}
	getE(id).options[idx] = option;
}

function removeOption(id, index)
{
	x.remove(index)
}

function removeOptions(id)
{
	var options = getE(id).options;
	for (var idx=0 ; idx<options.length ; idx++ )
		getE(id).remove(0);
}

function removeSelectedOptions(id)
{
	var x = getE(id);
	while (x.selectedIndex!=-1)
		x.remove(x.selectedIndex);
}

function getSelectValues(id) {
  var result = [];
  var options = getE(id).options;
  var opt;

  for (var i=0, iLen=options.length; i<iLen; i++) 
  {
    opt = options[i];
    if (opt.selected)
      result.push(opt);
  }
  return result;
}

function getSelectedIndex(id)
{
	var selected = new Array();
	for (var i=0 ; i<getE(id).options.length ; i++)
	{
		selected.push(getE(id).options[i].selected);
	}
	return selected;
}

function hasSelection(id)
{
	for (var i=0 ; i<getE(id).options.length ; i++)
	{
		if (getE(id).options[i].selected)
			return true;
	}
	return false;
}

function setMSelect(id, array)
{
	for(var a_idx=0 ; a_idx<array.length ; a_idx++)
		setSelectE(id,array[a_idx]);
}

function clearMSelect(id)
{
	var options = getE(id).options;
	for (var i=0, iLen=options.length; i<iLen; i++) 
		options[i].selected = false;
}

function select_list2values(array_of_select, sep)
{
	var values = "";
	for (var idx=0; idx<array_of_select.length ; idx++) 
	{	
		var val = array_of_select[idx].value;

		if (!isEmpty(val) && !hasX(val, "uc---member"))
		{	
			values += isEmpty(values) ? val : sep + val;
		}
	}
	return values;
}

function getDefaultObject()
{
	var null_object 	= new Object();
	null_object.id 		= "null_object";
	null_object.value 	= "empty";
	return null_object;
}

/********************* 
 *	RADIO FUNCTIONS  * 
 *********************/
function getRadio(radioGroupName) 
{
	var rads = document.getElementsByName(radioGroupName);
	for(var i=0 ; i<rads.length ; i++) 
	{
		if(rads[i].checked)
		return rads[i];
	}
  return getDefaultObject();
}

function uncheckAllRadio(radioGroupName){
	var rads = document.getElementsByName(radioGroupName);
	for(var i=0 ; i<rads.length ; i++)rads[i].checked = false;
}

function setCheckRadio(radioId, value){getE(radioId).checked = value;}
function checkRadio(radioId){setCheckRadio(radioId,true);}
function uncheckRadio(radioId){setCheckRadio(radioId,false);}

/******************** 
 *	AJAX FUNCTIONS  * 
 ********************/

/**
	method: get or post (submission type)
	url: the endpoint
	[callback]: the function to call when request succeeds. 
				- request map is filled with response content and returned as argument of callback function.
	[data]: get method request map {param-1: value-1, param-1:value-2}
	data: post method request map {param-1: value-1, param-1:value-2}
*/

function send_request(request)
{
	try
	{
		var actionURL 	= request['url'];
		var method		= request['method'];
		var callback 	= request['callback'];
		var submit_file = request['submit-file'];
		
		if (isEmpty(actionURL)){notify("missing action url.");return -1;}
		else if (isEmpty(method)){notify("missing request method. (get or post)");return -1;}

		/*
		if (atCelsius)
			actionURL = getE("contextPath").value + "/" + actionURL;
		else
			actionURL = "/" + actionURL;
			*/
		actionURL = getE("contextPath").value + "/" + actionURL;

		if (!isEmpty(request['full-url']))
			actionURL = request['full-url'];

		var xmlHttp = getXMLHttpRequest();
		
		if(callback!=null){
		xmlHttp.onreadystatechange = 
			function () 
			{	
				if(xmlHttp.readyState == 4) 
				{
					if(xmlHttp.status == 200 || xmlHttp.status == 0){
						request["responseXML"] 			= xmlHttp.responseXML;
						request["responseText"] 		= xmlHttp.responseText;
						request["responseStatus"] 		= xmlHttp.status;
						request["responseReadyState"] 	= xmlHttp.readyState;
						
						//check mapping request first using noCache value
						var json = getJson(xmlHttp.responseText);
						request["json"] = json;
						
						if (request["pre-cbk"]!=null)
							request["pre-cbk"]();
						
						callback(request);
						/*
						var json = getJson(xmlHttp.responseText);
						request["json"] = json;
						if (json!=null && !isEmpty(json.noCache))
						{	
							var request_return = xmlHttps[json.noCache];
							xmlHttps[json.noCache] = null;
							
							request_return['callback'](request_return);
						}
						*/
					}
					else if(xmlHttp.status == 404) {
						notify('HTTP error 404: action ' + actionURL + ' not found.');
					}
					else {
						notify('HTTP error ' + xmlHttp.status +  ' while submitting the command.');
					}
				}
			};
		}

		if ((isX(method, "get") | submit_file) && !isEmpty(request['data']))
			actionURL += "?" + html_map2params(request['data']);
			
		var noCache = getNoCacheValue();
		xmlHttps[noCache] = request;
		request['noCache'] = noCache;
		xmlHttp.open(method, setNoCache(actionURL, noCache), true);
		xmlHttp.setRequestHeader("Cache-Control","no-cache");	

		if (submit_file)
		{
			xmlHttp.setRequestHeader("Content-type", "multipart/form-data");
			if (request['file']!=null)
			{
				xmlHttp.setRequestHeader("Content-type", "multipart/form-data");
				var formData = new FormData();
				formData.append("data", request['file']);				
				//xmlHttp.send(formData);	
				xmlHttp.send(request['file']);	
			}
			else
			{
				notify("Missing file !!!");
				return ;
			}
		}
		else if (isX(method, "get"))
		{
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.send();
		}
		else if (isX(method, "post"))
		{
			xmlHttp.setRequestHeader("Content-type", "multipart/form-data");
			
			if (request['data'] == null)
			{
				notify("Missing (post) data !!!");
				return ;
			}
			else
			{
				var params = html_map2params(request['data']);

				xmlHttp.send(params);			
			}
			
		}
		//keep track of connection
		//request['xmlHttp'] = xmlHttp;
		return request;
	}
	catch (e){}
}

function getXMLHttpRequest(){
    if (window.XMLHttpRequest){
		 // Mozilla / Safari / IE7
        return new XMLHttpRequest();
	}
    else{
    	// IE < 7
        var aVersions = [ "MSXML2.XMLHttp.6.0", "MSXML2.XMLHttp.5.0", "MSXML2.XMLHttp.4.0", "MSXML2.XMLHttp.3.0", "MSXML2.XMLHttp", "Microsoft.XMLHttp" ];
		for (var i=0; i<aVersions.length; i++){
			try{
				return new ActiveXObject(aVersions[i]);
            }
            catch (oError){}
		}
		notify("Error: your browser does not support AJAX!");
		throw new Error("Cannot create XMLHttpRequest object.");
	}
}

function noCache(uri){
	return setNoCache(uri, getNoCacheValue());
}
function setNoCache(uri, noCache){
	return uri.concat(/\?/.test(uri)?"&":"?", "noCache=", noCache);
}
function getNoCacheValue()
{
	return (new Date).getTime() + "." + (Math.random()*1234567);
}

