

/**
<soapenv:Envelope
	xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
	xmlns:xsd='http://www.w3.org/2001/XMLSchema'
	xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>
	<soapenv:Header/>
	<soapenv:Body>
		<BonitaOnFCRWeb>
			<Request>
				<action>TS_revalidation</action>					
			</Request>
		</BonitaOnFCRWeb>
	</soapenv:Body>
</soapenv:Envelope>
*/

function rules_revalidation(uri){return "rules-revalidation/" + uri;}

function load()
{
	jQuery('#wrap').css('height', '600px');
	checkDebugTool();
	getUserLoginAndTSName();
}

function buildHeaderAttributeMap()
{
	
	var url = getUrl();
		url = url.indexOf('#') != -1 ? url.split('#')[0] : url;
	var headers = hasX(url, "?") ? html_params2map(url.split("?")[1]) : new Object();
	var form_meta 	= {"ts_id":"ts_id","local":"local"};
	for(var prop in form_meta)
		getE(prop).value = getParam(headers, form_meta[prop]);
}


function getUserLoginAndTSName()
{
	buildHeaderAttributeMap();
	
	send_request
	(
		{
			"method":'GET',
			"url":'fcr-action.htm',
			"data":
			{
				"action":rules_revalidation('check-data'),
				"ts_id":getE("ts_id").value,
				"local":getE("local").value,
			},
			"callback":getRevalidationRules
		}
	)
}

function getRevalidationRules(request)
{
	if (!isX(request['json']['statusCode'], "1"))
	{
		notify(request['json']['statusDescription']);
		return ;
	}
	getE("uid").value 		= request['json']['sm_user'];
	getE("ts_name").value 	= request['json']['ts_name'];
	getE("edition").value 	= request['json']['edition'];
	getE("fwo").value 		= request['json']['fwo'];
	
	if (!isEmpty(getE("uid").value))
	send_request
	(
		{
			"method":'GET',
			"url":hasX(document.location.href, "remote=true") ? 'fcr-action.htm' : 'rules.htm',
			"data":
			{
				"action":hasX(document.location.href, "remote=true") ? rules_revalidation('getRevalidationRules') : 'getRevalidationRules',
				"login":getE("uid").value,
				"ts":getE("ts_name").value
			},
			"callback":setRevalidationRules
		}
	)
}

var rule_map		= new Object();	/** rule-Mota-300:Mota 300**/
var rule_id_map		= new Object();	/** Mota 300:1122046354	rule.id **/
var rules_decision 	= new Object();	/** Mota 300:Keep|Drop **/

function setRevalidationRules(request)
{
	var rules 		= request['json']['ruleSummary'];
	var ts_id_map 	= new Object();
	var id_count	= 0;
	
	for (var i=0 ; i<rules.length ; i++)
	{
		/** id,name,technicalService,source,destination,service,action,active,nr,last_used **/
		/** source,destination,service : id,name,type,profile,predefined **/
		var nl						= "<div style='height:1px;font-size:1px;width:120px;'></div>";
		var technicalService 		= rules[i].technicalService;
		
		if (isX(technicalService, getE("ts_name").value))
		{
			var ts_id 					= -1;
			if (ts_id_map[technicalService]>=0)
				ts_id = "ts-" + ts_id_map[technicalService];
			else
			{
				ts_id = "ts-" + id_count;
				ts_id_map[technicalService] = id_count;
				id_count++;
			}

			if (getE(ts_id + "-title")==null)
			{
				var ts_rules 	= "<h3 id='" + ts_id + "-title'>" + technicalService + "</h3>"
								+ "<table class='group-component'>"
								+ "<thead>" 
								+ "<tr id='" + ts_id + "-tr-header'>"
								+ "		<th class='fwo-view'  style='display:none;width:5%;'>Flag</th>"
								+ "		<th style='width:8%;'>Decision</th>"
								+ "		<th style='width:10%;'>Name</th>"
								+ "		<th style='width:20%;'>Source</th>"
								+ "		<th style='width:20%;'>Destination</th>"
								+ "		<th style='width:20%;'>Service</th>"
								+ "		<th style='width:10%;'>Action</th>"
								+ "		<th style='width:7%;'>Last Used</th>"
								+ "	</tr>"
								+ "</thead>"
								+ "<tbody id='" + ts_id + "-body'></tbody>"							
								+ "</table>"
								;
				setHtml("rules", getHtml("rules") + ts_rules);
			}
			var rule 	= rules[i].name; 
			var rule_id = "rule-" + replaceAllIn(rule, " ", "-");
			
			var code 	= "<tr id='template-tr-body'>"
						+ "	<td class='fwo-view' style='display:none;'><input id='flag-" + rule_id + "' type='checkbox' /></td>"
						+ "	<td id='td-" + rule_id + "-revalidation'>"
						+ " 	<select id='decision-" + rule_id + "' class='control-select control-editable' disabled onchange=changeValidationDecision('" + rule_id + "') style='width:75px;'>"
						//+ " 		<option class='default-decision' selected value=''>[Select]</option>"
						+ " 		<option value='Keep'>Keep</option>"
						+ " 		<option value='Drop'>Drop</option>"
						+"		</select>"
						+ " </td>"
						+ "	<td><label id='td-" + rule_id + "-name' style='font-size:80%;'>" + rule + "</label></td>"
						+ "	<td><label id='td-" + rule_id + "-source' style='font-size:80%;'></label></td>"
						+ "	<td><label id='td-" + rule_id + "-destination' style='font-size:80%;'></label></td>"
						+ "	<td><label id='td-" + rule_id + "-service' style='font-size:80%;'></label></td>"
						+ "	<td><label id='td-" + rule_id + "-action' style='font-size:80%;'>" + rules[i].action + "</label></td>"
						+ "	<td><label id='td-" + rule_id + "-last-used' style='font-size:80%;'>" + rules[i].last_used + "</label></td>"
						+ "</tr>"
						;
						
			rule_map[rule_id] 	= rule;
			rule_id_map[rule]	= rules[i].id;

			setHtml(ts_id + "-body", getHtml(ts_id + "-body") + code);

			var ctrls = ["source","destination","service"];
			for (var i_ctrl=0 ; i_ctrl<ctrls.length ; i_ctrl++)
			{
				var controls 	= "";
				var control_id 	= ctrls[i_ctrl];
				for (var i_control=0 ; i_control<rules[i][control_id].length ; i_control++)
				{
					var control = rules[i][control_id][i_control];			
					controls += isEmpty(controls) ? control.name : nl + control.name;
				}
				setHtml("td-" + rule_id + "-" + control_id, controls);
			}
			
			var sources = "";
			for (var i_src=0 ; i_src<rules[i].source.length ; i_src++)
			{
				var src = rules[i].source[i_src];			
				sources += isEmpty(sources) ? src.name : nl + src.name;
			}
			setHtml("td-" + rule_id + "-source", sources);	

			rules_decision[rule] = $('#decision-' + rule_id).val();
		}
	}

	if (isX(getE("edition").value, "true"))
	{
		jq_enabled('control-editable');
		jq_show('owner-view');
	}
	else
	{
		jq_disabled('control-editable');
		jq_hide('owner-view');
	}
	/*
	if (isX(getE("fwo").value, "true"))	
		jq_show('fwo-view');
	else
		jq_hide('fwo-view');
	*/
	send_request
	(
		{
			"method":'GET',
			"url":'fcr-action.htm',
			"data":
			{
				"action":rules_revalidation('check-saved-rules-decision-flag'),
				"ts_id":getE("ts_id").value
			},
			"callback":function (request)
			{
				var rules = request['json'].rules
				for (var i=0 ; i<rules.length ; i++)
				{
					var rule 		= rules[i];
					var rule_sfx 	= "-rule-" + replaceAllIn(rule['rule'], " ", "-");
					$("#decision" + rule_sfx).val(rule['decision']);
					getE("flag" + rule_sfx).checked = isX(rule['flag'], 'true');
				}
			}			
		}
	);
}


function changeValidationDecision(rule_id)
{
	rules_decision[rule_map[rule_id]] = $('#decision-' + rule_id).val();
}

function send()
{
	var no_sel 			= "";
	var no_sel_count 	= 0;
	
	for (var rule in rules_decision)
	{
		//logout(rule + " - " + rules_decision[rule]);
		if (isEmpty(rules_decision[rule]))
		{	
			no_sel += (isEmpty(no_sel) ? "" : ", ");			
			if (no_sel_count>=3)
			{
				no_sel_count = 0;
				no_sel += "\n";
			}	
			no_sel += rule;
			no_sel_count++;
		}		
	}
	
	if (!isEmpty(no_sel))
	{
		notify("Please, make a decision about rule(s):\n " + no_sel);
		return ;
	}
	
	modal_on();
	send_request
	(
		{
			"method":'GET',
			"url":'fcr-action.htm',
			"data":
			{
				"action":rules_revalidation('update-decision'),
				"decision":in_map2params(rules_decision),
				"rule_id_map":in_map2params(rule_id_map),
				"ts_id":getE("ts_id").value
			},
			"callback":notifyUpdateErrors
		}
	);
}

function send_flags()
{
	var flags = new Object();
	for (var rule in rules_decision)
	{
		var flag_id = "flag-rule-" + replaceAllIn(rule, " ", "-");
		flags[rule] = getE(flag_id).checked ? "true" : "false";
	}
	modal_on();
	send_request
	(
		{
			"method":'GET',
			"url":'fcr-action.htm',
			"data":
			{
				"action":rules_revalidation('update-flags'),
				"flags":in_map2params(flags),
				"ts_id":getE("ts_id").value
			},
			"callback":notifyUpdateErrors
		}
	)
}

function notifyUpdateErrors(request)
{
	modal_off();
	try{
		notify(request.json.statusDescription);
		if(isX(request.json.statusCode, "1"))
		{
			jq_disabled('control-editable');
			jq_hide('owner-view');
		}
	}catch(e){}
}


function mask_on(){if (isIE8()) jq_show_oth("maskass-ie8","");else jq_show_oth("maskass","");}
function mask_off(){if (isIE8()) jq_hide("maskass-ie8");else jq_hide("maskass");}
function modal_on(){if (isIE8()) jq_show_oth("processing-ie8","");else jq_show_oth("processing","");}
function modal_off(){if (isIE8()) jq_hide("processing-ie8");else jq_hide("processing");}

function isIE8(){return getE("ieold")!=null;}

/**
	DEBUG SECTION
*/

function init_ts_reval()
{
	send_request
	(
		{
			"method":'GET',
			"url":'fcr-action.htm',
			"data":
			{
				"action":'test/init-ts-revalidation'
			},
			"callback":doNothing
		}
	)
}


function checkDebugTool()
{
	if (hasX(document.location.href,"debug-mode-on"))
	{
		show("debug-tool");
		hide('footer');
	}
}


function doNothing(request){}

function logout(x)
{
	setHtml("debug-section", getHtml("debug-section") + "<br/>" + x);
}

function ant()
{
	try
	{
		setTimeout(getE("debug-area").value, 0);
	}
	catch(e){notify(e);}
}