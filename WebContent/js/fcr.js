var xhr;
var xhr2;
		
function getPreviousRequests() 
{	
	xhr = new XMLHttpRequest();
	xhr.open('GET', 'fcr.htm?action=getPreviousRequests&ms=' + new Date().getTime() , true);
	xhr.onreadystatechange = function() { displayPreviousRequests(); } ;
	xhr.send(null);
}

function displayPreviousRequests()
{
	if (xhr.readyState == 4) //call is completed true
	 {
	 	if(xhr.status == 200) //http request is successfull
		{				 
	 		var requests = xhr.responseText;
	 		var jsonObject = JSON.parse(requests);
	 		
	 		var html = displayJsonRequest(jsonObject, "<p>You have no previous requests</p>");
	 		document.getElementById("PreviousRequests").innerHTML = html;
		}
		else
		{
	 		//alert("Error ---> \n"+ xhr.status + " : " + xhr.statusText);
	 	}
	 }
}

function getSearchResult()
{	
	var searchValue = document.getElementById("search_value").value;
	if (searchValue.length > 2)
	{
		var html = "<h2>My Search Result</h2><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search in progress ... <img src=\"ressources/progress.gif\"/><br><br>";
		document.getElementById("SearchResult").innerHTML = html;
		document.getElementById("search_value").value = "";
		xhr2 = new XMLHttpRequest();
		xhr2.open('GET', 'fcr.htm?action=getSearchResult&searchValue=' + searchValue + '&ms=' + new Date().getTime() , true);
		xhr2.onreadystatechange = function() { displaySearchResult(); } ;
		xhr2.send(null);
	}
	else
	{
		alert("Search Value must be at least 3 characters");
	}
}

function displaySearchResult()
{
	if (xhr2.readyState == 4) //call is completed true
	 {
	 	if(xhr2.status == 200) //http request is successfull
		{
	 		var requests = xhr2.responseText;	 		
	 		var jsonObject = JSON.parse(requests);
	 		
	 		var html = displayJsonRequest(jsonObject, "<p>No results are matching your search criteria</p>");
	 		html = html.replace("toggle_detail", "toggle_search_detail").replace("detail_", "search_detail_").replace("img_more_","img_more_search_").replace("img_less_","img_less_search_");
	 		document.getElementById("SearchResult").innerHTML = "<h2>My Search Result <a href=\"javascript:void(0)\" onClick=\"hideSearchResult()\">(hide)</a></h2>" + html;
		}
		else
		{
	 		alert("Error ---> \n"+ xhr2.status + " : " + xhr2.statusText);
	 	}
	 }
}

function hideSearchResult()
{
	document.getElementById("SearchResult").innerHTML = "";
}