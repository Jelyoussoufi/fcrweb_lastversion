var atCelsius 			= false;
var ie					= false;
var useHandClick		= true;
var usedebug 			= false;
var default_max_length 	= 30;
var displayMaxLength 	= default_max_length;
var td_default_symb 	= "&nbsp;";
var td_min_length 		= 100;
var busyMsg				= "a query is still running, please wait a while then try again.";
var safe_conflic_error 	= "-11";
var isBusy 				= false;
var isSubBusy 			= false;
var CR 					= "\n";
var BR 					= "<br>";
var userInfo;
var xhttp;
var SENTENCE_selectResource = "Please, select a resource first";
var SENTENCE_selectSafe 	= "Please, select a safe first";
var undoneTasks 			= "";
var global	 				= new Object();

function isIE()
{
	return ie;
}

function isLocaltesting()
{
	if (!isEmpty(userInfo) && isTrue(userInfo.staff.hasStaffInfo.value))
	{
		if (isX(userInfo.staff.login.value, "dcarels") | isX(userInfo.staff.phone.value, "localtesting_CT"))
			return true;
	}
	return false;
}

function load()
{
	try{var val = getE("ie").value; ie = isTrue(val);}catch(e){ie = false;}
	atCelsius = isTrue(getE("atCelsius").value);
	sendRequest("GET", "request.do", "action=getUserInfo", initUserInfo, "text");
}

function getUserInfoAndLogs()
{
	sendRequest("GET", "request.do", "action=getUserInfo", initUserInfo, "text");	// reload the page
	//sendRequest("GET", "request.do", "action=getUserInfo", setUserInfoAndLogs, "text"); // don't reload page
}

function initUserInfo(textResponse)
{
	setUserInfoAndLogs(textResponse);
}


function setUserInfoAndLogs(textResponse)
{
	userInfo = getJson(textResponse);
	showActor();

	if (isTrue(userInfo.staff.hasStaffInfo.value))
	{
		hide("progress");
		initUserLogs(userInfo);
		setTodoTasks(userInfo);
		try{setListElements(userInfo.staff.environments.environments, ["environment", "name"]);}catch(e){}

		if (!hasX(document.location.href, "query") & !hasX(document.location.href, "index"))
		getSearchRequest(); //update global search request value
			
		endLoad();
	}
	else 
		document.location.href = "exception.jsp";
}		

function initUserLogs(userInfo)
{
	setHtml("userLogs", "");
	
	if (isTrue(userInfo.staff.userLogs.userLogs.hasUserLogs.value))
	{
		var logs 		= userInfo.staff.userLogs.userLogs.logs.logs;
		var logCode 	= "<th></th><th>Log</th>";
		var doClearLog 	= true;
		
		for (var i=0 ; i<logs.length ; i++)
		{
			var logElems 	= logs[i].log.message.value.split(";");
			var imgName 	= "";	//deprecated
			var template	= "";
			var content 	= logElems[1];
			var ans			= "";
			
			if (isX(logElems[0], "1"))
			{
				template 	= "success";
			}
			else if (isX(logElems[0], "2"))
			{
				template 	= "error";
				try
				{
					if (document.location.href.indexOf("debug.jsp")!=-1)
						content += "<br>" + logs[i].log.error.value;
				}
				catch (e){}
			}
			else if (isX(logElems[0], "3"))
			{
				template 		= "warning";
				var taskInfo 	= logElems[2].split("_");
				var taskId		= taskInfo[0];
				var todoTaskId	= taskInfo[1];
				var taskName	= taskInfo[2];
				ans				= BR;

				if (isTrue(logs[i].log.expirable.value) && isTrue(logs[i].log.isExpired.value))
					ans += "NOANSWER";
				else if (logs[i].log.message.value.indexOf("DONE")==-1)
				{
					/*if (useHandClick){
						ans += handClick("#doReply?" + taskName + "=YES", "doReply(" + i + "," + todoTaskId  + "," + taskId + ",\"" + taskName + "\",\"YES\")", "YES");
						ans += "&nbsp;&nbsp;/&nbsp;&nbsp;";
						ans	+= handClick("#doReply?" + taskName + "=NO", "doReply(" + i + "," + todoTaskId  + "," + taskId + ",\"" + taskName + "\",\"NO\")", "NO");
					}else{
					*/
						ans += ahref("doReply(" + i + "," + todoTaskId  + "," + taskId + ",\"" + taskName + "\",\"YES\")", "", "YES");
						ans += "&nbsp;&nbsp;/&nbsp;&nbsp;";
						ans += ahref("doReply(" + i + "," + todoTaskId  + "," + taskId + ",\"" + taskName + "\",\"NO\")", "", "NO");
					//}
					ans	= span("id='replyAction'", ans);
					doClearLog = false;
				}
				else
					ans += "REPLIED";
			}
			else if (isX(logElems[0], "4"))
			{
				template = "info2";
				//doClearLog = false;
			}
			
			setHtml("requestProgression", div("class='" + template + "'", content + ans));
			
			try{
				if (!isEmpty(logs[i].log.action))
				{
					var args = "";
					if (!isEmpty(logs[i].log.args))
						args = logs[i].log.args.value;
					
					setTimeout(logs[i].log.action.value + "('" + args + "')", 0);
				}
			}catch(e){}
		}

		
		if (doClearLog)
			sendRequest("GET", "request.do", "action=clearLogs", null, "");
	}
}

// display the number of tasks that must be performed by user next to Tasks Tag if any
function setTodoTasks(userInfo)
{
	try
	{
		if (getInt(userInfo.staff.todotasks.value)>0)
		{
			undoneTasks = "(" + userInfo.staff.todotasks.value + ")";
		}
	}
	catch(e){}
}

var mapList = new Object();
function getMapListElements(id){return mapList[id];}

function setListElements(basket, array)
{
	try
	{
		var list = new Array();
		
		for (var i=0 ; i<basket.length ; i++)
			list[i] = basket[i].value;

		mapList[array[0] + "_" + array[1]] = list;

		var func = array[2];
	}
	catch (e){}
}

function displayProgress()
{
	//getE("imgProgress").src = getSrcImage("progress6.gif");
	//show("progress");
	setHtml("requestProgression", div("class='info'", "Processing your request...") );
}

function notifyLimitedSearch(type)
{
	var text = "Results are currently limited to 500<br/>Refine search attributes for further results";
	setHtml(type + "Info", span("class='info'", text));
}

function getSearchRequest()
{
	sendRequest("GET", "homeAction.do", "action=hasSearchRequest", updateSearchRequest, "text");
}

function updateSearchRequest(textResponse)
{
	var json = getJson(textResponse);
				
	if (!isEmpty(json.searchRequestValue))
	{
		getE("searchRequestValue").value = json.searchRequestValue;
		//show("query");
	}
}

function doReply(logId, todoTaskId, taskId, taskName, ans)
{
	if (!isEmpty(userInfo) && isTrue(userInfo.staff.hasStaffInfo.value))
	{
		var vars = "&staffId=" + userInfo.staff.id.value;
			vars += "&staffName=" + userInfo.staff.login.value;
			vars += "&teamId=" + userInfo.staff.teamId.value;
			vars += "&teamName=" + userInfo.staff.teamName.value;
			vars += "&taskId=" + taskId;
			vars += "&todoTaskId=" + todoTaskId;
			vars += "&reply=" + ans;
			vars += "&action=reply";
			
		if (confirm("confirm your answer:" + ans))
		{
			hide("replyAction");
			updateReplyStatus(logId);
			sendRequest("GET", "tasksAction.do", vars, null, "");
		}
	}
}

function updateReplyStatus(logId)
{
	sendRequest("GET", "request.do", "action=updateLog&logId=" + logId, getUserInfoAndLogs, "");
}

function clearLogs()
{
	setHtml("userLogs", "");
	sendRequest("GET", "request.do", "action=clearLogs", getUserInfoAndLogs, "")
}

function cancel()
{
	xhttp.abort();
	sendRequest("GET",action,"action=abort",clear,"text");
}

function isSRV(val)
{
	return hasX(val,"server") || hasX(val,"srv");
}

function isDB(val)
{
	return hasX(val,"database") || hasX(val,"db");
}

function doGlobalSearch()
{
	var action 	= "homeAction.do";
	var vars	= "action=search&searchValue=" + getE("searchRequestValue").value;
	
	//sendRequest("POST", action, vars, redirectQueryPage(), "none");
}

/**
	DEPRECATED
*/
function globalSearch()
{
	var searchRequestValue = getE("searchRequestValue").value;
	if (isEmpty(searchRequestValue) || isX(searchRequestValue,"Search..."))
	{
		notifyEmpty("search");
		return ;
	}
	sendRequest("GET", "homeAction.do", "action=search&searchValue=" + searchRequestValue, redirectQueryPage, null);
}

function showActor()
{	
	if (getE("userSelector")!=null && (atCelsius | hasX(getUri(),"debug.jsp")))
	{
		show("userSelector");
		setHtml("user", addE("h3", "", "Current user: " + userInfo.staff.login.value));
	}
}

function changeActor()
{
	if(advancedMode)
		sendRequest("GET", "debugAction.do", "action=getStaffInfo&login=" + getSelectE("actorSelector").text, fillActorForm, "text");
}

function setNewActor(synchronize)
{
	
	if (!isLocaltesting()) 
		return ;
		
	if (synchronize)
	{
		var vars = "action=setNewActor";
			vars += "&login=" + getSelectE("actorSelector").text
			vars += "&fullname=" + getE("fullname").value;
			vars += "&mobilephone=" + getE("mobilephone").value;
			vars += "&mail=" + getE("mail").value;
			vars += "&team=" + getE("team").value;
			vars += "&admin=" + getE("adm").value;
			vars += "&synchronize=" + synchronize;
			vars += "&usertype=" + getSelectE("usertype").text;
			
		sendRequest("GET", "debugAction.do", vars, getUserInfoAndLogs, "");
	}
	else
		sendRequest("GET", "debugAction.do", "action=setNewActor&login=" + getSelectE("actorSelector").text, getUserInfoAndLogs, "");		
}

var advancedMode = false;
function showAdvancedOptions()
{
	advancedMode = true;
	show("advancedOptions");
	changeActor();
}

function fillActorForm(text)
{
	userInfo = getJson(text);
	
	if (!isEmpty(userInfo) && isTrue(userInfo.staff.hasStaffInfo.value))
	{
		getE("fullname").value 		= userInfo.staff.fullName.value;
		getE("mobilephone").value 	= userInfo.staff.mobile.value;
		getE("mail").value 			= userInfo.staff.mail.value;
		getE("team").value 			= userInfo.staff.teamName.value;
		getE("adm").value 			= userInfo.staff.adm.value;
	}
	else
	{
		getE("fullname").value  	= "";
		getE("mobilephone").value 	= "";
		getE("mail").value			= "";
		getE("team").value			= "";
		getE("adm").value 			= "";
	}
}

function redirect(pageName)
{
	var uri = pageName;
	
	if (!atCelsius & !isIE())
		uri = "jsp/auth/" + pageName;
	else if (hasX(document.location.href, ".do"))
		uri = "jsp/auth/" + pageName;
		
	document.location.href = uri;
}

function redirectHomePage()
{
	redirect("home.jsp");
}

function redirectQueryPage()
{
	redirect("query.jsp");
}

function openApplicationLink(id, name)
{
	return openLink(id, name, "application", "openApplication");
}

function openSafeLink(id, name)
{
	return openLink(id, name, "safe", "openSafe");
}

function openResourceLink(id, name)
{
	return openLink(id, name, "resource", "openResource");
}

function openLink(id, name, page, action)
{
	return ahref(getInfix() + page + ".jsp#" + action + "?id=" + id + "&name=" + name, "", name);
}

function checkRequestInUrl()
{
	if (document.location.href.indexOf("#open")!=-1)
	{
		var params 	= document.location.href.split("?")[1].split("&");
		var id 		= "";
		var name	= "";
		
		for (var i=0 ; i<params.length ; i++)
		{
			var key_val = params[i].split("=");
			
			if (isX(key_val[0],"id"))
				id = key_val[1];
			else if (isX(key_val[0],"name"))
				name = key_val[1];
		}
		openElementRequest(id,name);
	}
}

function openRequest(id, name, action, redirect, callFunction)
{
	var nextAction = doNothing;
	
	if (isTrue(redirect))
		nextAction = redirectToResourcePage;
	else if (!isNull(callFunction))
		nextAction = callFunction;
		
	sendRequest("GET", action, "action=openRequest&id=" + id + "&name=" + name, nextAction, null);
}

function openApplication(id, name, redirect, callFunction)
{
	openRequest(id, name, "applicationAction.do", redirect, callFunction);
}

function openSafe(id, name, redirect, callFunction)
{
	openRequest(id, name, "safeAction.do", redirect, callFunction);
}

function openResource(id, name, redirect, callFunction)
{
	openRequest(id, name, "resourceAction.do", redirect, callFunction);
}



function redirectToApplicationPage(){redirect("application.jsp");}
function redirectToSafePage(){redirect("safe.jsp");}
function redirectToResourcePage(){redirect("resource.jsp");}
function redirectTasksPage(){redirect("tasks.jsp");}

function redirectToDebugPage()
{
	/*
	var login = userInfo.staff.login.value;

	if (!isEmpty(login) && (isX(login, "dcarels") | isX(login, "jhuilian")))
		redirect("debug.jsp");
	*/
}

function doNothing(){}

function hasForbiddenChars(value)
{
	var forbiddenChars = {}; //{";",":",".",",","'","^","-","+","*","/","%","=","&","|","(",")","<",">","{","}","[","]"};
	//";:,.-+*/%=&|()<>{}[]";
	for (var i=0 ; i<forbiddenChars.length ; i++)
	{
		if (value.indexOf(forbiddenChars[i])!=-1)
			return forbiddenChars[i]; 
		
		return "";
	}
}

/**
	@deprecated
*/
function getErrorMsg(name, value)
{
	
	if (isEmpty(value))
		return name + " could not be empty";
	/*
	else
	{
		var forbidChar = hasForbiddenChars(value);
		if (!isEmpty(forbidChar))
			return name  + " contains the following forbidden character '" + forbidChar + "'"; 
	}
	*/
	return "";
}

/**
	id: unique identifier, in case of many content form
	type: 	- 1 : filter
			- 1X : page
	
*/
var contentMap = new Object();
function initContentForm(id, type)
{
	var contentFilterTag 	= "contentFormFilter_" + id;
	var contentPageTag 		= "contentFormPage_" + id; 
	var option;
	
	if (isOptionEnabled(type, 1))
	{
		var array = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
		
		var pagelist = getE(contentFilterTag);
			while (pagelist.length>1)
				pagelist.options[pagelist.length-1] = null;
		
		if (atCelsius)
		{
			option = new Option("ALL", "*");
			getE(contentFilterTag).options[getE(contentFilterTag).length] = option;
		}
		
		option = new Option("NULL", "#");
		getE(contentFilterTag).options[getE(contentFilterTag).length] = option;
		
		for (var i=0 ; i<array.length ; i++)
		{
			option = new Option(array[i], array[i]);
			getE(contentFilterTag).options[getE(contentFilterTag).length] = option;
		}
		
		setSelectE(contentFilterTag, "A");
		contentMap[contentFilterTag] = getSelectE(contentFilterTag).value;
		contentMap[contentFilterTag + "_exist"] = true;
	}
	else
		contentMap[contentFilterTag + "_exist"] = false;
		
	if (isOptionEnabled(type, 2))
	{
		var pagelist = getE(contentPageTag);
			while (pagelist.length>1)
				pagelist.options[pagelist.length-1] = null;
				
		option = new Option("1", "1");
		getE(contentPageTag).options[getE(contentPageTag).length] = option;
		setSelectE(contentPageTag, "1");
		contentMap[contentPageTag] = getSelectE(contentPageTag).value;
		contentMap[contentPageTag + "_exist"] = true;
	}
	else
		contentMap[contentPageTag + "_exist"] = false;
}

function setContentForm(id, json)
{
	try
	{
		var form = json.container.form.form;
		var contentFilterTag 	= "contentFormFilter_" + id;
		var contentPageTag 		= "contentFormPage_" + id; 
		var option;
		
		if (contentMap[contentFilterTag + "_exist"]==true)
		{
			setSelectE(contentFilterTag, form.filter.value);
			contentMap[contentFilterTag] = getSelectE(contentFilterTag).value;
		}
		if (contentMap[contentPageTag + "_exist"]==true)
		{
			var pagelist = getE(contentPageTag);
			
			while (pagelist.length>1)
				pagelist.options[pagelist.length-1] = null;
			
			for (var i = 0 ; i<form.nbPage.value ; i++)
			{
				option = new Option(i + 1, i + 1);
				getE(contentPageTag).options[getE(contentPageTag).length] = option;
			}
			
			setSelectE(contentPageTag, form.page.value);
			contentMap[contentPageTag] = getSelectE(contentPageTag).value;
		}
	}
	catch (e){}
}

function updateApply(src, id, fct)
{
	var contentFilterTag 	= "contentFormFilter_" + id;
	var contentPageTag 		= "contentFormPage_" + id; 
	
	if (src==1 && contentMap[contentPageTag + "_exist"]==true)
	{
		var pagelist = getE(contentPageTag);
			while (pagelist.length>1)
				pagelist.options[pagelist.length-1] = null;
		option = new Option("1", "1");
		getE(contentPageTag).options[getE(contentPageTag).length] = option;
	}
	
	if (contentMap[contentFilterTag + "_exist"]==true)
		contentMap[contentFilterTag] = getSelectE(contentFilterTag).value;
	if (contentMap[contentPageTag + "_exist"]==true)
		contentMap[contentPageTag] = getSelectE(contentPageTag).value;
	
	fct();
}

/**
	options chain of bits (10101...01011)
	return true if the bit at position 'index' starting from end is equal to 1.
	else return false
	
	ADVANTAGE: many options could be set 
	e.g: 00101 (first and third options enabled)
*/
function isOptionEnabled(options, index)
{
	return options.length>=index && options.charAt(options.length-index)=="1";
}

function toShort(value, maxLength)
{
	var max = maxLength;
	
	if (isEmpty(max))
		max = default_max_length;
	if (value.length>max)
		return value.substr(0, max) + "...";
	else 
		return value;
}

function loading(elem)
{
	if (isEmpty(elem))
		elem = "loading";
	setHtml(elem, "<img class='simple' alt='loading' title='loading' src='" + getSrcImage("progress3.gif") + "'></img>")
}
function loaded(elem)
{
	if (isEmpty(elem))
		elem = "loading";
	setHtml(elem, "");
}

//deprecated
function showOff()
{
	loaded();
	/*
	hide("searching");
	hide("cancel");
	isBusy = false;
	*/
}

//deprecated
function showOn()
{
	loading();
	/*
	show("searching");
	show("cancel");
	isBusy = true;
	*/
}

function chkBusy()
{
	return isBusy || isSubBusy;
}

function setPopupMaxSize(id, array)
{
	var total 	= 0;
	var max 	= 475;
	total = max;
	/*
	for (var i=0 ; i<array.length ; i++)
	{
		if (array[i]>0)
			total += 150 + array[i] * 33;
		else 
			total += 130;
	}

	if (total>max)
	{
		getE(id).style.overflow = "auto";
		total = max;
	}
	else
		getE(id).style.overflow = "visible";
	*/	
	getE(id).style.height = total + "px";
	getE(id).style.overflow = "auto";	
}

function getLockTimeImg()
{
	return "<img class='simple' src='resources/lock_time.png' title='resource is temporarily locked for time to complete a request' />";
}
