var xhr;
var xhr2;
var xhr3;

function displayGenericRules()
{
	var element = document.getElementById("generic_rules");
	var text = document.getElementById("generic_rules_link");
	if(element.style.display == "block") {
		element.style.display = "none";
		text.innerHTML = "(show)";
  	}
	else {
		element.style.display = "block";
		text.innerHTML = "(hide)";
	}
}

function getSearchResult()
{	
	var searchValue = document.getElementById("search_value").value;
	if (searchValue.length > 2)
	{
		var html = "<h2>My Search Result</h2><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search in progress ... <img src=\"ressources/progress.gif\"/><br><br>";
		document.getElementById("SearchResult").innerHTML = html;	
		document.getElementById("search_value").value = "";
		xhr = new XMLHttpRequest();
		xhr.open('GET', 'rules.htm?action=getSearchResult&searchValue=' + searchValue + '&ms=' + new Date().getTime() , true);
		xhr.onreadystatechange = function() { displaySearchResult(); } ;
		xhr.send(null);
	}
	else
	{
		alert("Search Value must be at least 3 characters");
	}
}

function displaySearchResult()
{
	if (xhr.readyState == 4) //call is completed true
	 {
	 	if(xhr.status == 200) //http request is successfull
		{
	 		var requests = xhr.responseText;
	 		var jsonObject = JSON.parse(requests);
	 		var html = displayJsonRule(jsonObject, "<p>No results are matching your search criteria</p>");
	 		document.getElementById("SearchResult").innerHTML = "<h2>My Search Result <a href=\"javascript:void(0)\" onClick=\"hideSearchResult()\">(hide)</a></h2><div style=\"font-size:10px\">" + html + "</div>";
		}
		else
		{
	 		alert("Error ---> \n"+ xhr.status + " : " + xhr.statusText);
	 	}
	 }
}

function hideSearchResult()
{
	document.getElementById("SearchResult").innerHTML = "";
}

function switchSimpleExpanded()
{		
	if (document.getElementById("simple_rule").style.display == "none")
	{
		document.getElementById("expanded_rule").style.display = "none";
		document.getElementById("simple_rule").style.display = "block";
		document.getElementById("expLink").innerHTML = "Display expanded rule";
	}
	else
	{		
		document.getElementById("simple_rule").style.display = "none";
		document.getElementById("expanded_rule").style.display = "block";
		document.getElementById("expLink").innerHTML = "Display unexpanded rule";
	}
}

function updateRule(id, technical_service)
{
	window.open('forms.htm?pr_form_mode=rule&pr_maintain_ids='+id +'&pr_ts_name='+technical_service, '_blank', 'resizable=yes, width=1050, height=700');
}

function getChangeLog(ruleName)
{	
	var html = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"ressources/progress.gif\"/>";
	document.getElementById("change_log").innerHTML = html;
	xhr2 = new XMLHttpRequest();
	xhr2.open('GET', 'rules.htm?action=getChangeLog&ruleName=' + ruleName.replace(" ", "-S-") + '&ms=' + new Date().getTime() , true);
	xhr2.onreadystatechange = function() { displayChangeLog(); } ;
	xhr2.send(null);
}

function displayChangeLog()
{
	if (xhr2.readyState == 4) //call is completed true
	 {
	 	if(xhr2.status == 200) //http request is successfull
		{
	 		var changeLog = xhr2.responseText;
	 		var jsonObject = JSON.parse(changeLog);
	 		var html = displayJsonChangeLog(jsonObject, "<p>No Change Log for this rule</p>", "false");
	 		var html2 = displayJsonChangeLog(jsonObject, "<p>No Change Log for this rule</p>", "true");
	 		var innerh = "<h3>Change Log</h3>";
	 		innerh += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id=\"clexpLink\" href=\"javascript:void(0)\" onClick=\"switchChangeLogSimpleExpanded()\">Display expanded Change Log</a>";
	 		innerh += "<div id=\"unexpanded_change_log\" style=\"font-size:10px\">" + html + "</div>";
	 		innerh += "<div id=\"expanded_change_log\" style=\"font-size:10px;display:none;\">" + html2 + "</div>";
	 		document.getElementById("change_log").innerHTML = innerh;
		}
		else
		{
	 		alert("Error ---> \n"+ xhr2.status + " : " + xhr2.statusText);
	 	}
	 }
}

function switchChangeLogSimpleExpanded()
{
	if (document.getElementById("unexpanded_change_log").style.display == "none")
	{
		document.getElementById("expanded_change_log").style.display = "none";
		document.getElementById("unexpanded_change_log").style.display = "block";
		document.getElementById("clexpLink").innerHTML = "Display expanded Change Log";
	}
	else
	{
		document.getElementById("unexpanded_change_log").style.display = "none";
		document.getElementById("expanded_change_log").style.display = "block";
		document.getElementById("clexpLink").innerHTML = "Display unexpanded Change Log";
	}
}

function ruleChecked(rule_id, list_id)
{
	if (!Array.prototype.indexOf)
	{
	  Array.prototype.indexOf = function(elt /*, from*/)
	  {
	    var len = this.length >>> 0;
	    
	    var from = Number(arguments[1]) || 0;
	    from = (from < 0)
	         ? Math.ceil(from)
	         : Math.floor(from);
	    if (from < 0)
	      from += len;

	    for (; from < len; from++)
	    {
	      if (from in this &&
	          this[from] === elt)
	        return from;
	    }
	    return -1;
	  };
	}	
	
	var exist = listTab[list_id].indexOf(rule_id);
	document.getElementById("button"+list_id).style.display = "block";
	
	if (exist == -1)
	{		
		listTab[list_id].push(rule_id);
	}
	else
	{
		listTab[list_id].splice(exist, 1);
		if (listTab[list_id].length == 0)
		{
			document.getElementById("button"+list_id).style.display = "none";
		}
	}
}

function editSelectedRules(list_id, ts_name)
{
	if (listTab[list_id].length == 0)
	{
		alert("At least one rule must be selected");
	}
	else
	{
		var ruleList = "";
		var first = true;
		for (var i = 0; i < listTab[list_id].length; i++)
		{
			if (first)
			{
				first = false;
				ruleList = listTab[list_id][i];
			}
			else
			{
				ruleList = ruleList + "," + listTab[list_id][i];
			}			
		}		
		
		window.open('forms.htm?pr_form_mode=rule&pr_maintain_ids='+ruleList +'&pr_ts_name='+filter(ts_name), '_blank', 'resizable=yes, width=1050, height=700');
	}
	
}
