var xhr;
var xhr2;

function getSearchResult()
{	
	var searchValue = document.getElementById("search_value").value;
	if (searchValue.length > 2)
	{
		var html = "<h2>My Search Result</h2><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search in progress ... <img src=\"ressources/progress.gif\"/><br><br>";
		document.getElementById("SearchResult").innerHTML = html;
		document.getElementById("search_value").value = "";
		xhr = new XMLHttpRequest();
		xhr.open('GET', 'profiles.htm?action=getSearchResult&searchValue=' + searchValue + '&ms=' + new Date().getTime() , true);
		xhr.onreadystatechange = function() { displaySearchResult(); } ;
		xhr.send(null);
	}
	else
	{
		alert("Search Value must be at least 3 characters");
	}
}

function displaySearchResult()
{
	if (xhr.readyState == 4) //call is completed true
	 {
	 	if(xhr.status == 200) //http request is successfull
		{
	 		var requests = xhr.responseText;
	 		var jsonObject = JSON.parse(requests);
	 		
	 		var html = displayJsonProfile(jsonObject, "<p>No results are matching your search criteria</p>");	 		
	 		document.getElementById("SearchResult").innerHTML = "<h2>My Search Result <a href=\"javascript:void(0)\" onClick=\"hideSearchResult()\">(hide)</a></h2>" + html;
		}
		else
		{
	 		alert("Error ---> \n"+ xhr.status + " : " + xhr.statusText);
	 	}
	 }
}

function hideSearchResult()
{
	document.getElementById("SearchResult").innerHTML = "";
}

function setProfileAsPredefined(profile_id)
{
	if (confirm("Are you sure you want to define this profile as predefined?"))
	{
		xhr2 = new XMLHttpRequest();
		xhr2.open('GET', 'profiles.htm?action=setPredefined&id=' + profile_id + '&ms=' + new Date().getTime() , true);
		xhr2.onreadystatechange = function() { profileCorrectlyPredefined(); } ;
		xhr2.send(null);
	}	
}

function profileCorrectlyPredefined()
{
	if (xhr2.readyState == 4) //call is completed true
	{
	 	if(xhr2.status == 200) //http request is successfull
		{
	 		document.getElementById("predefined").checked = true;
		}
		else
		{
	 		alert("Error ---> \n"+ xhr2.status + " : " + xhr2.statusText);
	 	}
	}	
}