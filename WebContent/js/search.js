var xhr;

function searchFCR()
{
	var vars = "";
	var number = document.getElementById("fcr_number").value;
	var hide = "no";
	if (number != "")
	{		
		if (!isNaN(number))
		{
			vars += "&fcrnumber=" + number;		
		}
		else
		{
			alert("FCR Number must be a number");
			hide = "yes";
		}
	}
	var requestor = document.getElementById("requestor").value;
	if (requestor != "")
	{
		vars += "&requestor=" + requestor;		
	}	
	/*var technical = document.getElementById("technical_service").value;
	if (technical != "")
	{
		vars += "&technical=" + technical;
	}*/
	var context = document.getElementById("context").value;
	if (context != "all")
	{
		vars += "&context=" + context;
	}
	var from = document.getElementById("from").value;
	if (from != "")
	{
		vars += "&from=" + from;
	}
	var to = document.getElementById("to").value;
	if (to != "")
	{
		vars += "&to=" + to;
	}
	var task = document.getElementById("task").value;
	if (task != "all")
	{
		vars += "&task=" + task;
	}
	var source = document.getElementById("source").value;
	if (source != "")
	{
		vars += "&source=" + source;
	}
	var destination = document.getElementById("destination").value;
	if (destination != "")
	{
		vars += "&destination=" + destination;
	}
	if (vars == "")
	{
		if (hide == "no")
		{
			alert("At least one search criteria must be filled");
		}
	}
	else
	{
		var html = "<h2>My Search Result</h2><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search in progress ... <img src=\"ressources/progress.gif\"/><br><br>";
		document.getElementById("SearchResult").innerHTML = html;

		xhr = new XMLHttpRequest();
		xhr.open('GET', 'search.htm?action=getFCRResult&ms=' + new Date().getTime()+vars , true);
		xhr.onreadystatechange = function() { displayFCRResult(); } ;
		xhr.send(null);
	}
}

function displayFCRResult()
{
	if (xhr.readyState == 4) //call is completed true
	 {
	 	if(xhr.status == 200) //http request is successfull
		{
	 		var requests = xhr.responseText;	 		
	 		var jsonObject = JSON.parse(requests);
	 		
	 		var html = displayJsonRequest(jsonObject, "<p>No results are matching your search criteria</p>");	 		
	 		document.getElementById("SearchResult").innerHTML = "<h2>My Search Result</h2>" + html;
		}
		else
		{
	 		alert("Error ---> \n"+ xhr.status + " : " + xhr.statusText);
	 	}
	 }
}

function searchRule()
{
	var vars = "";
	
	var name = document.getElementById("name").value;
	if (name != "")
	{
		vars += "&name=" + name;		
	}
	var from = document.getElementById("from").value;
	if (from != "")
	{
		vars += "&from=" + from;		
	}	
	var to = document.getElementById("to").value;
	if (to != "")
	{
		vars += "&to=" + to;		
	}	
	var source = document.getElementById("source").value;
	if (source != "")
	{
		vars += "&source=" + source;		
	}	
	var destination = document.getElementById("destination").value;
	if (destination != "")
	{
		vars += "&destination=" + destination;		
	}	
	var service = document.getElementById("service").value;
	if (service != "")
	{
		vars += "&service=" + service;		
	}	
	
	if (vars == "")
	{		
		alert("At least one search criteria must be filled");		
	}
	else
	{
		var html = "<h2>My Search Result</h2><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search in progress ... <img src=\"ressources/progress.gif\"/><br><br>";
		document.getElementById("SearchResult").innerHTML = html;	
		
		xhr = new XMLHttpRequest();
		xhr.open('GET', 'search.htm?action=getRuleResult&ms=' + new Date().getTime()+vars , true);
		xhr.onreadystatechange = function() { displayRuleResult(); } ;
		xhr.send(null);
	}
}

function displayRuleResult()
{
	if (xhr.readyState == 4) //call is completed true
	 {
	 	if(xhr.status == 200) //http request is successfull
		{
	 		var rules = xhr.responseText;	 		
	 		var jsonObject = JSON.parse(rules);
	 		
	 		var html = displayJsonRule(jsonObject, "<p>No results are matching your search criteria</p>");	 		
	 		document.getElementById("SearchResult").innerHTML = "<h2>My Search Result</h2>" + html;
		}
		else
		{
	 		alert("Error ---> \n"+ xhr.status + " : " + xhr.statusText);
	 	}
	 }
}

function clearFCR()
{	
	document.getElementById("fcr_number").value ="";	
	document.getElementById("requestor").value ="";
	//document.getElementById("technical_service").value ="";
	document.getElementById("context").value = "all";
	document.getElementById("from").value ="";
	document.getElementById("to").value ="";
	document.getElementById("task").value = "all";	
	document.getElementById("source").value ="";
	document.getElementById("destination").value ="";
}

function clearRule()
{
	document.getElementById("name").value ="";
	document.getElementById("source").value ="";
	document.getElementById("destination").value ="";
	document.getElementById("service").value ="";
	document.getElementById("from").value ="";
	document.getElementById("to").value ="";
}

function initDatePicker()
{
	new JsDatePick({
		useMode:2,
		target:"from",
		dateFormat:"%d-%m-%Y"
		/*selectedDate:{				This is an example of what the full configuration offers.
			day:5,						For full documentation about these settings please see the full version of the code.
			month:9,
			year:2006
		},
		yearsRange:[1978,2020],
		limitToToday:false,
		cellColorScheme:"beige",
		dateFormat:"%m-%d-%Y",
		imgPath:"img/",
		weekStartDay:1*/
	});
	new JsDatePick({
		useMode:2,
		target:"to",
		dateFormat:"%d-%m-%Y"
		/*selectedDate:{				This is an example of what the full configuration offers.
			day:5,						For full documentation about these settings please see the full version of the code.
			month:9,
			year:2006
		},
		yearsRange:[1978,2020],
		limitToToday:false,
		cellColorScheme:"beige",
		dateFormat:"%m-%d-%Y",
		imgPath:"ressources/datePicker_img/",		
		weekStartDay:1*/
	});
}