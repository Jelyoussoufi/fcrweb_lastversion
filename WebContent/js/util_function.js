
/**
 * INTEGER FUNCTIONS
 * STRING FUNCTIONS
 * DATE FUNCTIONS
 * REGEX FUNCTIONS
 **/

/********************\
|*	UTIL FUNCTIONS  *|
\********************/

 /*
	!!! isX function is not adequat to implement this function.
*/
function isTrue(bool){return bool == true || bool == "true" || bool=="TRUE" || bool == "True" ;}

// compare val to null chain
function isEmpty(val)
{
	// || isNaN(val)  result in bug
	return (val==undefined || val=="" || val=="null" || val=="undefined" || val=="%null");	// null == undefined
}

// compare elem to null ref
function isNull(elem)
{
	return elem==undefined || elem==null;
}

function getUrl()
{
	//return isEmpty(getE("queryString").value) ? getE("reqURI").value :  getE("reqURI").value + "?" + getE("queryString").value
	return document.location.href;
	//return document.URL;
	//return window.location
}


/******************************\
|*	DATASTRUCTURES FUNCTIONS  *|
\******************************/

var html_e_sep 		= "&";
var html_kv_sep 	= "=";
var in_e_sep		= ":::";
var in_e_kv_sep		= "---";


var params2map_kv_sep 	= in_e_sep;
var params2map_e_sep 	= in_e_kv_sep;

function html_params2map(params)
{
	set_params2map_seps(html_e_sep, html_kv_sep);
	return params2map(params);
}
function in_params2map(params)
{
	set_params2map_seps(in_e_sep, in_e_kv_sep);
	return params2map(params);
}

// also works for map2params
function set_params2map_seps(e_sep, kv_sep)
{
	params2map_e_sep 	= e_sep;
	params2map_kv_sep 	= kv_sep;
}

function params2map(params)
{
	var map = new Object();
	try
	{
		var elements 	= params.split(params2map_e_sep);
		
		for (var i=0 ; i<elements.length ; i++)
		{
			var id_val = elements[i].split(params2map_kv_sep);
			map[id_val[0]] = id_val[1];
		}
	}
	catch (e){map = new Object();}

	return map;
}

function getParam(params, val)
{
	try
	{
		return !isEmpty(params[val]) ? params[val] : "";
	}
	catch (e)
	{
		return "";
	}
}

function html_map2params(map)
{
	set_params2map_seps(html_e_sep, html_kv_sep);
	return map2params(map, true);
}
function in_map2params(map)
{
	set_params2map_seps(in_e_sep, in_e_kv_sep);
	return map2params(map, false);
}

function map2params(map, useFilter)
{
	var params = "";
	if (map==null)
		return params;
		
	for(var prop in map)
	{
		var value = useFilter==true ? filter(map[prop]) : map[prop]
		var k_v = !isEmpty(value) ? (prop + params2map_kv_sep + value) : (prop + params2map_kv_sep);
		params += !isEmpty(params) ? (params2map_e_sep + k_v) : k_v;
	}
	return params;
}

function isInList(list, val)
{
	if (list==null)
		return false;
	else if (isEmpty(val))
		return false;
		
	for (var i=0 ; i<list.length ; i++)
		if (list[i]==val)return true;
		
	return false;
}

/***********************
 *	INTEGER FUNCTIONS  *
 ***********************/

function isInt(value)
{
	var intVal = getInt(value);
	if(isNaN(intVal))return false;
	else if (isEmpty(intVal)) return false;
	return true;
}

function getInt(val)
{
	try
	{
		var intVal = parseInt(val);
		return intVal;
	}
	catch(oError)
	{
		return "null";
	}
}

function isPos(nb)
{
	return isEmpty(nb) ? false : getInt(nb) > 0;
}

/**********************
 *	STRING FUNCTIONS  *
 **********************/
 
function capFirst(val)
{
	return isEmpty(val) ? "" : (val.substring(0,1).toUpperCase() + ((val.length>1) ? val.substring(1,val.length).toLowerCase() : ""));
}

function fill(val, size, symbol)
{
	while (val.length < size)
		val = val + "" + symbol;
	return val;
}
function fillDef(val)
{
	return fill(val, td_min_length, td_default_symb);
}

function trim(myString)
{
	if (!isEmpty(myString))
		return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
	return myString;
} 

function isX(value, X)
{
	return (isEmpty(X) ? "" : trim(X).toLowerCase()) == (isEmpty(value) ? "" : trim(value).toLowerCase());
}

function hasX(value, X)
{
	var x = trim(X).toLowerCase(); 
	var val = trim(value).toLowerCase();
	return val.indexOf(x) != -1;
}

function replaceAllIn(chain, from, to)
{
	if(!isEmpty(chain))
		return chain.replace(new RegExp(from, "g"), to);
	return chain;
}

function removeAll(chain, toRemove)
{
	return chain.replace(new RegExp(toRemove, "g"), "");
}

function getSpecChar(data)
{
	var iChars = "<>";		//"!@#$%^&*()+=-[]\\\';,./{}|\":<>?~_"
	var eChars = '< >';

	return getSpecCharArray(data, iChars, eChars);
}

function getSpecCharArray(data, iChars, eChars)
{	
	for (var i = 0; i < data.length; i++) 
	{
		if (iChars.indexOf(data.charAt(i)) != -1) 
		{
			return eChars;
		}
	}
	return "";
}

function undoFilter(value)
{
	value = value + "";
	var reg;

	reg = new RegExp("_XCode_SLA_", "g"); value = value.replace(reg, "/");
	reg = new RegExp("_XCode_AND_", "g"); value = value.replace(reg, "&");
	reg = new RegExp("_XCode_QUO_", "g"); value = value.replace(reg, "\"");
	reg = new RegExp("_XCode_ACC_", "g"); value = value.replace(reg, "'");
	reg = new RegExp("_XCode_CR_", "g"); value = value.replace(reg, "\\n");
	reg = new RegExp("_XCode_SPA_", "g"); value = value.replace(reg, " ");
	reg = new RegExp("_XCode_ESC_", "g"); value = value.replace(reg, "\\");
	reg = new RegExp("_XCode_OR_", "g"); value = value.replace(reg, "|");
	reg = new RegExp("_XCode_NEG_", "g"); value = value.replace(reg, "~");
	reg = new RegExp("_XCode_EQ_", "g"); value = value.replace(reg, "=");
	reg = new RegExp("_XCode_ADD_", "g"); value = value.replace(reg, "+");
	reg = new RegExp("_XCode_SUB_", "g"); value = value.replace(reg, "-");
	reg = new RegExp("_XCode_OBX_", "g"); value = value.replace(reg, "[");
	reg = new RegExp("_XCode_CBX_", "g"); value = value.replace(reg, "]");
	reg = new RegExp("_XCode_OBR_", "g"); value = value.replace(reg, "{");
	reg = new RegExp("_XCode_CBR_", "g"); value = value.replace(reg, "}");
	reg = new RegExp("_XCode_LT_", "g"); value = value.replace(reg, "<");
	reg = new RegExp("_XCode_GT_", "g"); value = value.replace(reg, ">");
	reg = new RegExp("_XCode_OP_", "g"); value = value.replace(reg, "(");
	reg = new RegExp("_XCode_CP_", "g"); value = value.replace(reg, ")");
	reg = new RegExp("_XCode_TIM_", "g"); value = value.replace(reg, "*");
	reg = new RegExp("_XCode_DD_", "g"); value = value.replace(reg, ":");
	reg = new RegExp("_XCode_DOT_", "g"); value = value.replace(reg, ".");
	reg = new RegExp("_XCode_COL_", "g"); value = value.replace(reg, ",");
	reg = new RegExp("_XCode_SEC_", "g"); value = value.replace(reg, ";");
	reg = new RegExp("_XCode_INT_", "g"); value = value.replace(reg, "?");
	reg = new RegExp("_XCode_EXC_", "g"); value = value.replace(reg, "!");
	reg = new RegExp("_XCode_ARO_", "g"); value = value.replace(reg, "@");
	reg = new RegExp("_XCode_LAC_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_RAC_", "g"); value = value.replace(reg, "`");
	reg = new RegExp("_XCode_CIR_", "g"); value = value.replace(reg, "^");
	reg = new RegExp("_XCode_EYE_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_DEG_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_PD_", "g"); value = value.replace(reg, "�");
	//reg = new RegExp("_XCode_DOL_", "g"); value = value.replace(reg, "$");
	reg = new RegExp("_XCode_SQ_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_CUB_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_EUR_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_EA_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_EG_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_DS_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_CED_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_AG_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_UG_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_MU_", "g"); value = value.replace(reg, "�");
	reg = new RegExp("_XCode_SHA_", "g"); value = value.replace(reg, "#");
	reg = new RegExp("_XCode_TD_", "g"); value = value.replace(reg, "�"); 
	reg = new RegExp("_XCode_UND_", "g"); value = value.replace(reg, "_");
	reg = new RegExp("_XCode_PER%", "g"); value = value.replace(reg, "_");	// last change !!!
	return value;
}

function filter(value)
{
	try{
	value = value.replace(/\%/g,"_XCode_PER_"); // first change !!!
	value = value.replace(/\_/g,"_XCode_UND_");
	value = value.replace(/\//g,"_XCode_SLA_");
	value = value.replace(/\&/g,"_XCode_AND_");
	value = value.replace(/\"/g,"_XCode_QUO_");
	value = value.replace(/\'/g,"_XCode_ACC_");
	value = value.replace(/\n/g,"_XCode_CR_");
	value = value.replace(/ /g,"_XCode_SPA_");
	value = value.replace(/\\/g,"_XCode_ESC_");
	value = value.replace(/\|/g,"_XCode_OR_");
	value = value.replace(/\~/g,"_XCode_NEG_");
	value = value.replace(/\=/g,"_XCode_EQ_");
	value = value.replace(/\+/g,"_XCode_ADD_");
	value = value.replace(/\-/g,"_XCode_SUB_");
	value = value.replace(/\[/g,"_XCode_OBX_");
	value = value.replace(/\]/g,"_XCode_CBX_");
	value = value.replace(/\{/g,"_XCode_OBR_");
	value = value.replace(/\}/g,"_XCode_CBR_");
	value = value.replace(/\</g,"_XCode_LT_");
	value = value.replace(/\>/g,"_XCode_GT_");
	value = value.replace(/\(/g,"_XCode_OP_");
	value = value.replace(/\)/g,"_XCode_CP_");
	value = value.replace(/\*/g,"_XCode_TIM_");
	value = value.replace(/\:/g,"_XCode_DD_");
	value = value.replace(/\./g,"_XCode_DOT_");
	value = value.replace(/\,/g,"_XCode_COL_");
	value = value.replace(/\;/g,"_XCode_SEC_");
	value = value.replace(/\?/g,"_XCode_INT_");
	value = value.replace(/\!/g,"_XCode_EXC_");
	value = value.replace(/\@/g,"_XCode_ARO_");
	value = value.replace(/\�/g,"_XCode_LAC_");
	value = value.replace(/\`/g,"_XCode_RAC_");
	value = value.replace(/\^/g,"_XCode_CIR_");
	value = value.replace(/\�/g,"_XCode_EYE_");
	value = value.replace(/\�/g,"_XCode_DEG_");
	value = value.replace(/\�/g,"_XCode_PD_");
	//value = value.replace(/\$/g,"_XCode_DOL_");
	value = value.replace(/\�/g,"_XCode_SQ_");
	value = value.replace(/\�/g,"_XCode_CUB_");
	value = value.replace(/\�/g,"_XCode_EUR_");
	value = value.replace(/\�/g,"_XCode_EA_");
	value = value.replace(/\�/g,"_XCode_EG_");
	value = value.replace(/\�/g,"_XCode_DS_");
	value = value.replace(/\�/g,"_XCode_CED_");
	value = value.replace(/\�/g,"_XCode_AG_");
	value = value.replace(/\�/g,"_XCode_UG_");
	value = value.replace(/\�/g,"_XCode_MU_");
	value = value.replace(/\#/g,"_XCode_SHA_");
	value = value.replace(/\�/g,"_XCode_TD_");
	}catch(e)
	{
		
	}
	return value;
}

/********************
 *	DATE FUNCTIONS  *
 ********************/

function isPastDay(date)
{
	if(!isDate(date)) return false;
	if(date == getDate()) return false; // date equals today
	var elem = date.split('/');
	var aDate = new Date(elem[2],(parseInt(elem[1])-1),elem[0]);
	var today = new Date();
	if(aDate < today)return true;
	else return false;
}
function isWorkingDay(date)
{
	if(!isDate(date)) return false;
	var elem = date.split('/');
	var aDate = new Date(elem[2],(parseInt(elem[1])-1),elem[0]);
	var day = aDate.getDay();
	if(day=="0"||day=="6") return false;
	return true;
}

function getWorkingDate()
{
	var date = getDate();
	return date;
	
	if(isWorkingDay(date))return date;
	date = getDatePlus(1);
	if(isWorkingDay(date))return date;
	return getDatePlus(2);
	
}
function getDate()
{
	var now = new Date();
	return now.getDate() + "/" + (now.getMonth()+1) + "/" + now.getFullYear();
}

function getDatePlus(nbDays)
{
	var now = new Date();
	now.setDate(now.getDate() + nbDays);
	return now.getDate() + "/" + (now.getMonth()+1) + "/" + now.getFullYear();
}

function getDatePlusMonth(nbMonth)
{
	var now = new Date();
	now.setMonth(now.getMonth() + nbMonth);
	return now.getDate() + "/" + (now.getMonth()+1) + "/" + now.getFullYear();
}

function isDate(dateStr) 
{
	var datePat = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
	var matchArray = dateStr.match(datePat); // is the format ok?

	if (matchArray == null) 
	{
		//notify("Please enter date as either dd/mm/yyyy.");
		return false;
	}

	day 	= matchArray[1]; // p@rse date into variables
	month 	= matchArray[3];
	year 	= matchArray[5];

	if (month < 1 || month > 12) 
	{ // check month range
		//notify("Month must be between 1 and 12.");
		return false;
	}
	if (day < 1 || day > 31) 
	{
		//notify("Day must be between 1 and 31.");
		return false;
	}
	if ((month==4 || month==6 || month==9 || month==11) && day==31) 
	{
		//notify("Month "+month+" doesn`t have 31 days!")
		return false;
	}
	if (month == 2) 
	{ 	// check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day > 29 || (day==29 && !isleap)) 
		{
			//notify("February " + year + " doesn`t have " + day + " days!");
			return false;
		}
	}
	return true; // date is valid
}

function sec2day(val)
{
	if (!isInt(val)|isNaN(val))
		return "";
	var min 	= 60;
	var hour 	= min * 60;
	var day 	= hour * 24;
	var nbd 	= Math.floor(val / day);
	val 		= val - (nbd*day);
	var nbh		= Math.floor(val / hour);
	val 		= val - (nbh*hour);
	var nbm		= Math.floor(val / min);
	val 		= val - (nbm*min);
	var time 	= "";
	
	time += nbd + " day";
	if (nbd>1)time+="s";
	time += " " + nbh + " hour";	
	if (nbh>1)time+="s";
	time += " " + nbm + " minute";
	if (nbm>1)time+="s";
	time += " " + val + " seconde";
	if (val>1)time+="s";
	
	return time;
}

/*********************
 *	REGEX FUNCTIONS  *
 *********************/

/*
TEST EMAIL ADDRESS
var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
if(reg.test(mailteste))
*/

function isIp(input){return isIpV4(input) /*|| isIpV6(input);*/}
function isIpV4(input)
{
	//var pattern  = /^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$/;
	var pattern  = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
	return input.match(pattern)!=null;
}
function isIpV6(input){return isIPv6StdAddress(input) || isIPv6HexCompressedAddress(input);}
function isIPv6StdAddress(input)
{
	var pattern  = /^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$/;
	return input.match(pattern)!=null;
}
function isIPv6HexCompressedAddress(input)
{
	var pattern  = /^((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)::((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)$/;
	return input.match(pattern)!=null;
}

function isPort(port)
{
	try
	{
		if (isX(port,"0"))
			return true;
		else if (!isInt(getInt(port)))
			return false;
		else if (!isX(getInt(port) + "", port + ""))
			return false;
		
		var val = getInt(port);
		if (0<=val & val <= 65535)
			return true;
	}
	catch (e){}
	return false;
}

var default_port_range_separator = ":";

function isPortRange(range, sep)
{
	var separator = isEmpty(sep) ? default_port_range_separator : sep;
	try
	{
		if (isEmpty(range))
			return false;
			
		var ports = replaceAllIn(range, separator, "_").split("_");
		if (ports.length!=2)
		{
			return false;
		}
		else
		{	
			return isPort(ports[0]) && isPort(ports[1]) && (getInt(ports[0]) < getInt(ports[1]));
		}
	}
	catch(e)
	{
	}
	return false;
}

function isPortOrPortRange(port, sep)
{
	return isPort(port) | isPortRange(port, sep);
}


function isMask(mask)
{
	try
	{
		if (!isInt(getInt(mask)))
			return false;
		
		var val = getInt(mask);
		if (0<=val & val <= 32)return true;
	}
	catch (e){}
	return false;
}

function isSubnetWithMask(subnet)
{
	var  subnet_mask = subnet.split("/");
	if (subnet_mask.length != 2)
		return false;
	return isIpV4(subnet_mask[0]) & isMask(subnet_mask[1]);
}
