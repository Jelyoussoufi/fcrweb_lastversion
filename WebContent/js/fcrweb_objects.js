var	fcr_object_kv_sep 	= "=";
var fcr_object_e_sep 	= "&";

/************************
	CONTROL OBJECT
	type: src,dest srv 
*************************/
function fcrweb_control_object(name, type, multiple)
{	
	this.initialize = function(name, type, multiple)
	{
		this.name		= name;			// identifier of control_object (source,destination,service)
		this.type 		= type;			// type of control_object (src,dest,srv)
		this.multiple	= multiple;		// true or for multi elements. (type:src,dest)
		this.elems 		= new Object();	// control_values
		this.flow;
		this.ctrl_idx 	= -1;
		this.store 		= new Object();
		return this;
	};
	
	this.put = function(key,value)
	{
		this.elems[key] = value;
		return this;
	};
	
	this.toHtmlParams = function()
	{
		var vars = "";
		for (var key in this.elems)
		vars += (vars!="") 	? fcr_object_e_sep + key + fcr_object_kv_sep + this.elems[key] 
							:  key + fcr_object_kv_sep + this.elems[key];
		return vars;
	};
	
	this.getFlowIdx = function(){return this.flow.getIdx();};
	/** this.setFlowIdx is forbidden **/
	this.getCtrlIdx = function()
	{
		var tmp = this.elems["f-" + this.getFlowIdx() + "-" + this.type + "-idx"];
		return isEmpty(tmp) ? 1 : tmp; 
	};
	this.setCtrlIdx = function(idx){this.elems["f-" + this.getFlowIdx() + "-" + this.type + "-idx"] = idx;};
	this.getNbCtrl = function()
	{
		var tmp = this.elems["f-" + this.getFlowIdx() + "-nb-" + this.type];
		return isEmpty(tmp) ? 0 : tmp;
	};
	this.setNbCtrl = function(nbCtrl){this.elems["f-" + this.getFlowIdx() + "-nb-" + this.type] = nbCtrl;};
	
	this.addCtrlValue = function(key, value)
	{
		this.elems["f-" + this.getFlowIdx() + "." + this.getCtrlIdx() + "-" + key] = value;
	};
	
	this.getCtrlValue = function (key)
	{
		return this.elems["f-" + this.getFlowIdx() + "." + this.getCtrlIdx() + "-" + key];
	}
	
	this.deleteCtrlValue = function(key, value)
	{
		delete this.elems["f-" + this.getFlowIdx() + "." + this.getCtrlIdx() + "-" + key];
	};
	
	/** 
		remove all ctrl of the current flow
		useful for source only when nao dataflow type change or flow action change.
		@note flow idx dependent
	**/
	this.removeAllCtrlValues = function()
	{
		this.removeFlowValues(this.getFlowIdx());
		this.setNbCtrl(0);
		this.setCtrlIdx(1);
	};
	
	/** remove all control values at index flow_idx **/
	this.removeFlowValues = function(flow_idx)
	{
		for (var key in this.elems)
		{
			if (hasX(key, "f-" + flow_idx))
				delete this.elems[key];
		}
	};
	
	/** swap all control values on flow_old_idx flow index to new index flow_new_idx **/
	this.replaceFlowIndex = function(flow_old_idx, flow_new_idx)
	{
		/** clean up flow_new_idx values **/
		this.removeFlowValues(flow_new_idx);
		/** do swap from old to new **/
		for (var key in this.elems)
		{
			if (hasX(key, "f-" + flow_old_idx))
			{
				this.elems[key.replace("f-" + flow_old_idx, "f-" + flow_new_idx)] = this.elems[key];				
				delete this.elems[key];	// delete old entry
			}
		}
	};
	
	
	/** remove values of idx-th control at current flow index **/
	this.removeValues = function(idx)
	{
		for (var key in this.elems)
		{
			if (hasX(key, "f-" + this.getFlowIdx() + "." + idx))
				delete this.elems[key];
		}
	};
	
	/** swap values of old_idx-th control to new_idx-th index at current flow index **/
	this.replaceIndex = function(old_idx, new_idx)
	{
		/** clean up new index **/
		this.removeValues(new_idx);	
		
		for (var key in this.elems)
		{
			if (hasX(key, "f-" + this.getFlowIdx() + "." + old_idx))
			{
				this.elems[key.replace("." + old_idx, "." + new_idx)] = this.elems[key];				
				delete this.elems[key];	// delete old entry
			}
		}
	};
	
	this.saveInStore = function()
	{
		this.store = new Object();
		for (var key in this.elems)
		{
			if (hasX(key, "f-" + this.getFlowIdx()))
				this.store[key] = this.elems[key];
		}
	};
	
	this.restoreFromStore = function()
	{
		this.removeFlowValues(this.getFlowIdx());
		for (var key in this.store)
		{
			this.elems[key] = this.store[key];
		}
	};
	
	this.sel_add = function(elem)
	{

		if (this.valid_subgroup(elem))
		{

			var map 	= new Object();	
			var meta 	= new Object();
			var	id 		= ""; 
			var sel 	= "";
			
			if (isX(elem, "host"))
			{
				map["cat"]		= "host";
				map["host"] 	= getE(this.type + "-host").value;
				map["ip"] 		= getE(this.type + "-ip").value;
				map["ip-tsl"] 	= getE(this.type + "-ip-tsl").value;
				map["env"] 		= getSelectE(this.type + "-host-env").value;				 
				map["uc"]		= (isInSuggestionList(this.type + "-host", getE(this.type + "-host").value)) ? "use" : "create";
				
				meta["area"]	= getE(this.type + "-host-area").value;
				
				id				= getOrgShort(getSelectE(this.type + "-org").value);
				id				+= "-host-" + normalize(map["host"]) + "-" + map["env"] + "-" + map["ip"];				
				sel				= "hosts";
			}
			else if (isX(elem, "subnet"))
			{
				map["cat"]		= "subnet";
				map["subnet"] 	= getE(this.type + "-subnet").value;
				map["area"] 	= getE(this.type + "-subnet-area").value;
				map["uc"]		= (isInSuggestionList(this.type + "-subnet", getE(this.type + "-subnet").value)) ? "use" : "create";
				
				meta["area"]	= getE(this.type + "-subnet-area").value;
				
				id				= getOrgShort(getSelectE(this.type + "-org").value);
				id				+= "-net-" + replaceAllIn(map["subnet"], "/", "-");
				sel				= "subnets";
			}
			else if (isX(elem, "goo-selector"))
			{
				map["cat"]		= "goo";
				map["goo"] 		= getE(this.type + "-goo-selector").value;
				map["env"] 		= getSelectE(this.type + "-goo-sel-env").value;
				map["uc"]		= "use";	//use only
				//id				= getE(this.type + "-goo-selector").value;
				id				= getOrgShort(getSelectE(this.type + "-org").value);
				id				+= "-" + normalize(map["goo"]) + "-" + map["env"];
				//id				+= "-" + (isInSuggestionList(this.type + "-global-goo", map["goo"]) ? "grp" : "lgrp");				
				id				+= "-grp";
				sel				= "goos";
			}
			else if (isX(elem, "srv"))
			{
				map["cat"]		= "srv";
				map["name"] 	= getE(this.type + "-one-name").value;
				map["proto"] 	= getSelectE(this.type + "-one-proto").value;
				map["type"] 	= getE(this.type + "-one-type").value;
				map["port"] 	= getE(this.type + "-one-port").value;
				map["uc"]		= (isInSuggestionList(this.type + "-one-name", getE(this.type + "-one-name").value)) ? "use" : "create";
				//id				= getE(this.type + "-one-name").value;
				id				= map["proto"] + "-" + normalize(map["name"]) + (isX(map["proto"], "icmp") ? "" : ("-" + map["port"]));
				sel				= "gsrv-srvs";
			}
			map["id"] 	= id;
			map["meta"] = meta_map2params(meta);

			if (!optionExist(this.type + "-" + sel, id))
			{
				jq_hide("warning-error-tr-" + this.type + "-" + sel);
				addOptionL(this.type + "-" + sel, id + " - " +  map["uc"], in_map2params(map), new Object());
			}
			else
				notify("the element " + id + " already exist");
		}
	};
	
	this.sel_rem = function(elem)
	{
		var sel = "";
		if (isX(elem, "host"))
			removeSelectedOptions(this.type + "-hosts");
		else if (isX(elem, "subnet"))
			removeSelectedOptions(this.type + "-subnets");
		else if (isX(elem, "goo-selector"))
			removeSelectedOptions(this.type + "-goos");
		else if (isX(elem, "srv"))
			removeSelectedOptions(this.type + "-gsrv-srvs");
	};
	
	/** 
		DIALOG OPEN CONTROLS 
	**/
	
	this.open_add = function()
	{
		this.clear(0);		
		this.setCtrlIdx(this.getNbCtrl() + 1);
		setHtml(this.name + "-dialog-header", "Add a " + this.name);
		jq_hide(this.name + "-edit-button");
		jq_show(this.name + "-add-button");
		hideError("aefs");
		
		var open_nao = this.open_source_nao();
		if (open_nao==1)
		{
			/** nao form displayed **/
			/** TODO set nao form values **/
		}
		else if (open_nao==-1)
		{
			/** nao form not initialized correctly **/
			return ;
		}
		set_select_ro_inputs();
		//mask_on();		
		//show(this.name + "-dialog");
		switch_flow_dialog_view(this.name + "-dialog");
	};
	
	this.open_edit = function(action)
	{			
		if (getE("output-" + this.name).options.length<=0)
		{
			notify("No " + + this.name + "(s)");
			return -1;
		}			
		var selections = getSelectValues("output-" + this.name);
		if (selections.length<=0)
		{
			notify("Please select a " + this.name + " first.");
			return -1;
		}
		else if (selections.length>1)
		{
			notify("Please, " + action + " one " + this.name + " at a time.");
			return -1;
		}
	
		this.clear(0);		
		this.setCtrlIdx(getSelectE("output-" + this.name).value);
		
		/** cannot edit object on firewall, just delete operation allowed **/
		var meta = meta_params2map(this.getCtrlValue(this.type + "-meta"));
		if (isX(meta['from-rule'], "true"))
		{
			notify(isX(action,"edit") ? "You can not edit the object on firewall." : "information not available");
			return ;
		}
		
		var open_nao = this.open_source_nao();
		if (open_nao==1)
		{
			/** nao form displayed **/
			setSelectE(getNaoId(), this.getCtrlValue(this.type + "-name"));
		}
		else if (open_nao==-1)
		{
			/** nao form not initialized correctly **/
			return ;
		}
		else
		{
			if (type!="srv")
			{
				setSelectE(this.type + "-org", this.getCtrlValue(this.type + "-org"));
				this.changeOrganization();		
				$("." + this.type + "-group-element-wrapper" ).scrollTop(0);
			}
			
			var cat 	= this.getCtrlValue(this.type + "-cat");
			var attrs 	= this.val_attrs[cat];
			setSelectE(this.type + "-cat", cat);
			this.changeCategory();
			
			for(var idx=0 ; idx<attrs.length ; idx++)
				getE(this.type + "-" + attrs[idx]).value = this.getCtrlValue(this.type + "-" + attrs[idx]);
			
			if (isX(cat, "Host"))
			{
				getE(this.type + "-host-area").value = meta["area"];
				if (isX(getSelectE(this.type + "-org").value, "MOBISTAR"))
					show("tr-" + this.type + "-host-area");
				else 
					hide("tr-" + this.type + "-host-area");
					
				setSelectEC(this.type + "-host-env", this.getCtrlValue(this.type + "-host-env"));
			}
			else if (isX(cat, "Subnet"))			
			{
				getE(this.type + "-subnet-area").value = meta["area"];
				if (isX(getSelectE(this.type + "-org").value, "MOBISTAR"))
					show("tr-" + this.type + "-subnet-area");
				else 
					hide("tr-" + this.type + "-subnet-area");
			}
			else if (isX(cat, "Group of objects"))
				setSelectEC(this.type + "-goo-env", this.getCtrlValue(this.type + "-goo-env"));
			else if (isX(cat, "Single service"))
			{
				setSelectEC(this.type + "-one-proto", this.getCtrlValue(this.type + "-one-proto"));
				getE(this.type + "-one-port").value = this.getCtrlValue(this.type + "-one-port");
			}
			this.resize_group_wrapper(false);
			var selections = this.val_sel[cat];
			if (selections.length>=1)
			{
				for(var sel_idx=0 ; sel_idx<selections.length ; sel_idx++)
				{
					var ctrl_select = this.getCtrlValue(this.type + "-" + selections[sel_idx]);

					if (!isEmpty(ctrl_select))
					{
						jq_show(this.type + "-" + this.sel_selectors[cat] + "-selection-rows");
						this.resize_group_wrapper(true);
						removeOptions(this.type + "-" + selections[sel_idx]);
						var elems = ctrl_select.split(";;;");
						var options = "";
						for(var e_idx=0 ; e_idx<elems.length ; e_idx++)
							options += "<option value='" + elems[e_idx] + "'>" + in_params2map(elems[e_idx])["id"] + "</option>";
						setHtml(this.type + "-" + selections[sel_idx], options);						
					}
				}
			}
			
			hide("source-network-access-origin");
			show("source-definition");
		}
		
		
		if (isX(this.getCtrlValue(this.type + "-uc"), "use"))
			disable_autocomplete_fields(this.type + "-autocomplete-rows");
		else
			enable_autocomplete_fields(this.type + "-autocomplete-rows");
		
		setHtml(this.name + "-dialog-header", capFirst(action) + " " + this.name);
		jq_hide(this.name + "-add-button");
		jq_show(this.name + "-edit-button");
		hideError("aefs");
		set_select_ro_inputs();
		//mask_on();
		//show(this.name + "-dialog");
		switch_flow_dialog_view(this.name + "-dialog");
		
	};
	
	this.open_source_nao = function ()
	{
		if (isX(this.name, "source"))
		{
			var flow_type = getRadio("dataflow-type").value;
			var flow_action	= getSelectE("flow-action").value;
			if (isEmpty(flow_type))
			{
				notify("Please, define the flow connection type first.");
				return -1;
			}
			else if (isU2H(flow_type) & isEmptyOrDefault(flow_action))
			{
				notify("Please, define the flow action first.");
				showError("flow-action");
				return -1;
			}
			else if (isNao(flow_action))
			{
				if (isEmpty(getE("profile").value))
				{
					notify("Please, define the user group first.");
					showError("profile");
					return -1;
				}
				
				if (isEncryption(flow_action))
				{				
					hide("tr-src-nao-auth");
					show("tr-src-nao-enc");
				}
				else// if (isUserAuthentication(flow_action))
				{
					hide("tr-src-nao-enc");
					show("tr-src-nao-auth");
				}
				hide("source-definition");
				show("source-network-access-origin");
				
				/** check if false nao : iff cat is not group of objects while nao is true **/
				/** cannot edit rule ctrl
				var ctrl_meta = this.getCtrlValue(this.type + "-meta");
				
				if (!isEmpty(ctrl_meta))
				{
					
					var ctrl_meta_map = in_params2map(ctrl_meta);
					if (isX(ctrl_meta_map['from-rule'],"true") & !isX(ctrl_meta_map['is-nao-rule'],"true"))
					{
						hide("source-network-access-origin");
						show("source-definition");
						return 0;
					}
				}
				*/
				return 1;
			}
		}
		hide("source-network-access-origin");
		show("source-definition");
		/** not nao **/
		return 0;
	};
	
	this.remove = function()
	{
		/** 
			- delete control_element 
			if control_element to delete is not last then:
			replace last control_element index by current idx;
		**/

		if (getE("output-" + this.name).options.length<=0)
		{
			notify("No " + this.name + "(s).");
			return -1;
		}
	
		var selections = getSelectValues("output-" + this.name);
		if (selections.length<=0)
		{
			notify("Please select some " + this.name + "(s) first.");
			return -1;
		}
		
		var del_sel = "";
		for (var s_idx=0 ; s_idx<selections.length ; s_idx++)
			del_sel += isEmpty(del_sel) ? selections[s_idx].text : ", " + selections[s_idx].text;

		if (isX(getE('debug-area').value,'true') || confirm("Delete " + this.name + "(s) : " + del_sel + " ?"))
		{
			var init_flow_idxb 	= this.swap2MasterFlow();
					
			for (var s_idx=1 ; s_idx<=selections.length ; s_idx++)
			{
				/** start to delete from last index **/
				var selected_idx = selections[selections.length - s_idx].value;
				
				/** if object already exist on firewall then change uc to remove **/
				this.setCtrlIdx(selected_idx);				
				var meta = meta_params2map(this.getCtrlValue(this.type + "-meta"));meta_params2map
				if (isX(meta['from-rule'], "true"))
				{
					this.addCtrlValue(this.type + "-uc", isX(this.getCtrlValue(this.type + "-uc"), "use") ? "delete" : "use");
				}
				else
				{
					/** in order to avoid gap in control indexes, reset last index control to current index. **/
					if (selected_idx < this.getNbCtrl())
					{
						this.replaceIndex(this.getNbCtrl(), selected_idx);
						this.removeValues(this.getNbCtrl());
					}
					else
						this.removeValues(selected_idx);			
					
					this.setNbCtrl(this.getNbCtrl() - 1);
					this.setCtrlIdx(this.getNbCtrl());		// set index to last ctrl
				}
				/*
				if (!isEmpty(this.getNbCtrl()) && this.getNbCtrl()>=1)	
					jq_show(this.name + "-edit");
				else
				*/
					jq_hide(this.name + "-edit");
				
			}
			
			this.flow.setIdx(init_flow_idxb);
			
			setHtml("output-" + this.name, map2select_options(this.getCtrlListOutput(), this.getCtrlListOutputColor()));
			
			this.updateNaoView();
		}		
	};
	
	/** 
		DIALOG CLOSE CONTROLS 
	**/
	this.check_pending_validation = function(elem)
	{
		var valid = true;
		if (isX(getE(this.type + "-" + elem + "-code").value, "0"))
		{
			notify("Please, wait a while until some validations terminates then retry.");
			valid = false;
		}
		else if(isX(getE(this.type + "-" + elem + "-code").value, "-1"))
		{
			valid = false;
		}
		return valid;
	};
	
	this.valid_subgroup = function (elem)
	{
		var valid = true;
		if (this.type != "srv")
		{
			if (isX(elem, "host"))
			{
				/** TODO check public host ip **/
				var host 	= getE(this.type + "-host").value;
				var ip 		= getE(this.type + "-ip").value;
				var h_sugg	= isInSuggestionList(this.type + "-host", host);
				var ip_sugg	= isInSuggestionList(this.type + "-ip", ip);
				if (isEmpty(host))
				{
					setHtml("warning-error-td-" + this.type + "-host", "Missing value.");
					showError(this.type + "-host");
					valid = false;
				}
				else if (isEmpty(ip) | !isIp(ip))
				{
					setHtml("warning-error-td-" + this.type + "-ip", "Missing value or incorrect ip address.");
					showError(this.type + "-ip");
					valid = false;
				}
				// host or ip is an autocomplete value but not both
				else if ((h_sugg & !ip_sugg) | (!h_sugg & ip_sugg))
				{
					setHtml("warning-error-td-" + this.type + "-host-opt", "this host name has already been created.");
					setHtml("warning-error-td-" + this.type + "-ip-opt", "this ip address has already been created.");
					if((!h_sugg & ip_sugg)) 
					{
						hideError(this.type + "-ip");
						showError(this.type + "-ip-opt");
					}
					else 
					{
						hideError(this.type + "-host");
						showError(this.type + "-host-opt");
					}
					valid = false;
				}
				
				if (isEmptyOrDefault(getSelectE(this.type + "-host-env").value))
				{
					showError(this.type + "-host-env");
					valid = false;
				}
				
				if (!isEmpty(getE(this.type + "-ip-tsl").value) && !isIp(getE(this.type + "-ip-tsl").value))
				{
					showError(this.type + "-ip-tsl");
					valid = false;
				}				

				if (isX(getSelectE(this.type + "-org").value, "MOBISTAR") && !h_sugg)
				{
					setError(this.type + "-host-opt", "You are not allowed to create a new host");
					hideError(this.type + "-host");
					showError(this.type + "-host-opt");
					valid = false;
				}
				else if (!this.check_pending_validation("ip"))	/** external validations **/
					valid = false;
			}
			else if (isX(elem, "subnet"))
			{
				var subnet 	= getE(this.type + "-subnet").value;
				if (isEmpty(subnet) || !isSubnetWithMask(subnet))
				{
					showError(this.type + "-subnet");
					valid = false;
				}
				/*
				var area = this.type + "-subnet-area";
				
				if ( isEmpty(getE(area).value) || isX(getSelectE(this.type + "-org").value, "MOBISTAR") && !isInSuggestionList(area, getE(area).value))
				{
					showError(area);
					valid = false;
				}
				*/
				if (isX(getSelectE(this.type + "-org").value, "MOBISTAR") && !isInSuggestionList(this.type + "-subnet", subnet))
				{
					setError(this.type + "-subnet-opt", "You are not allowed to create a new subnet");
					hideError(this.type + "-subnet");
					showError(this.type + "-subnet-opt");
					valid = false;
				}				
				else if (!this.check_pending_validation("subnet"))	/** external validations **/
					valid = false;
			}
			else if (isX(elem, "goo"))
			{
				var  next = this.type + "-goo";
				if (isEmpty(getE(next).value))
				{
					showError(next);
					valid = false;
				}
				else if (!isInSuggestionList(next, getE(next).value))
				{
					var total_objects = getE(this.type + "-hosts").options.length;
					total_objects += getE(this.type + "-subnets").options.length;
					total_objects += getE(this.type + "-goos").options.length;
					if (total_objects <= 0)
					{
						jq_show("warning-error-tr-" + this.type + "-hosts");
						valid = false;
					}
				}
				if (isEmptyOrDefault(getSelectE(this.type + "-goo-env").value))
				{
					showError(this.type + "-goo-env");
					valid = false;
				}
			}
			else if (isX(elem, "goo-selector"))
			{
				var  next = this.type + "-goo-selector";
				if (!isInSuggestionList(next, getE(next).value))
				{
					showError(next);
					valid = false;
				}
				if (isEmptyOrDefault(getSelectE(this.type + "-goo-sel-env").value))
				{
					showError(this.type + "-goo-sel-env");
					valid = false;
				}
			}
		}
		else
		{
			if (isX(elem, "srv"))
			{
				var srv 			= getE(this.type + "-one-name").value;
				var proto 			= getSelectE(this.type + "-one-proto").value;
				var port 			= getE(this.type + "-one-port").value;				
				var isSrvInList 	= isInSuggestionList(this.type + "-one-name", getE(this.type + "-one-name").value);
				var isSrvPortInList = isInSuggestionList(this.type + "-one-port", port) | isX(port, "-");
				
				if (isEmpty(srv) | isEmptyOrDefault(proto) | isEmpty(port))
				{
					if (isEmpty(srv))
						showError(this.type + "-one-name");
					else if (isEmptyOrDefault(proto))
						showError(this.type + "-one-proto");
					else 
					{
						setError("srv-one-port", "missing port value");
						showError(this.type + "-one-port");
					}
					valid = false;
				}
				else if (isX(proto, "icmp"))
				{
					/** skip **/
				}				
				else if (!isPortOrPortRange(port)) 
				{
					setError("srv-one-port", "incorrect port (range) value");
					showError(this.type + "-one-port");
					valid = false;
				}	
				/*
				else if (!isSrvPortInList && checkPortConflicts(this.type + "-one-port", port))
				{
					
						//check if this service port is in conflict with another service port 
						//- partial overlap is an error 
						//- perfect match is not an error
					
					setError("srv-one-port", "port value is overlaping with another service port (range)");
					showError(this.type + "-one-port");
					valid = false;
				}
				*/
				else if ((isSrvInList != isSrvPortInList))
				{							
					setError("srv-one-port", isSrvPortInList ? "port already in use" : "unexpected port value");
					showError(this.type + "-one-port");
					valid = false;					
				}
			}
			else if (isX(elem, "gsrv"))
			{
				var  next = this.type + "-gsrv-name";
				if (isEmpty(getE(next).value))
				{
					showError(next);
					valid = false;
				}
				else if (!isInSuggestionList(next, getE(next).value))
				{
					if (getE(this.type + "-gsrv-srvs").options.length <= 0)
					{
						jq_show("warning-error-tr-" + this.type + "-gsrv-srvs");
						valid = false;
					}
				}
			}
		}
		return valid;
	};
	
	this.valid = function(do_replace)
	{
		var valid = true;

		if (this.isSrcNao())
		{
			var id 		= getNaoId();			
			var ctrlid 	= "";
			if (isEmptyOrDefault(getSelectE(id).value))
			{
				showError(id);
				valid = false;
			}
			else
				ctrlid = householder[id + "-map"][getE(id).value];

			if (valid)
			{
				/** check rdd for nao **/
				var ctrlidx = this.getCtrlIdx();
				var rdd		= new Object();
				for (var i = 1 ; i <= this.getNbCtrl() ; i++)
				{
					this.setCtrlIdx(i);
					rdd[this.getCtrlValue(this.type + "-output")] = true;
				}
				this.setCtrlIdx(ctrlidx);
			
				if (rdd[ctrlid] == true)
				{
					valid = false;
					notify("the element " + ctrlid + " has already been added.");
				}
			}
			return valid;
		}
		else if(this.type != "srv")
		{
			if (isEmpty(getSelectE(this.type + "-org").value) | isX(getSelectE(this.type + "-org").value, default_choice))
			{
				showError(this.type + "-org");
				valid = false;
			}
			else if (isEmpty(getSelectE(this.type + "-cat").value))
			{
				showError(this.type + "-cat");
				valid = false;
			}
			else
			{
				var cat = getSelectE(this.type + "-cat").value;
				if (isX(cat, "Host"))
					valid = this.valid_subgroup("host");
				else if (isX(cat, "Subnet"))
					valid = this.valid_subgroup("subnet");
				else if (isX(cat, "Group of objects"))
					valid = this.valid_subgroup("goo");
			}			
		}else{
			if (isEmpty(getSelectE(this.type + "-cat").value))
			{
				showError(this.type + "-cat");
				valid = false;
			}
			else
			{
				var cat = getSelectE(this.type + "-cat").value;
				if (isX(cat, "Single service"))
					valid = this.valid_subgroup("srv");
				else if (isX(cat, "Group of services"))
					valid = this.valid_subgroup("gsrv");
			}
		}	
		
		/** if element with same category and identifier exist in the current flow **/
		if (valid)
		{
			var ctrlidx = this.getCtrlIdx();
			var cat 	= getE(this.type + "-cat").value;
			var ctrlid 	= getE(this.type + "-" + this.auto_id[cat]).value; 
			var rdd		= new Object();
			for (var i = 1 ; i <= this.getNbCtrl() ; i++)
			{
				this.setCtrlIdx(i);
				rdd[this.getCtrlValue(this.type + "-cat") + "---" + this.getCtrlValue(this.type + "-output")] = true;
			}
			this.setCtrlIdx(ctrlidx);

			if (rdd[cat + "---" + ctrlid] == true)
			{
				valid = false;
				notify("a " + this.name + " " + ctrlid /*+ " of category " + cat*/ + " already exist.");
			}
		}
		return valid;
	};
	
	this.val_attrs = 
	{
		"Host":['host','ip','ip-tsl'], "Subnet":['subnet','subnet-area'], "Group of objects":['goo'],
		"Single service":['one-name', 'one-type', 'one-port'], "Group of services":['gsrv-name']
	};
	
	this.val_sel = 
	{
		"Host":[], "Subnet":[], "Group of objects":['hosts','subnets','goos'],
		"Single service":[], "Group of services":['gsrv-srvs']
	};
	
	this.out_ids = 
	{
		"src":['host','subnet','goo'], "dest":['host','subnet','goo'],
		"srv":['one-name', 'gsrv-name']
	};
	
	this.sel_selectors = 
	{
		"Host":'', "Subnet":'', "Group of objects":'goo', 
		"Single service":'', "Group of services":'gsrv'
	};
	
	/** deprecated, use conventionnal naming instead **/
	this.auto_id = 
	{
		"Host":'host', "Subnet":'subnet', "Group of objects":'goo', 
		"Single service":'one-name', "Group of services":'gsrv-name'
	};
	
	this.add = function(){this.insert_replace(false);};
	this.edit = function(){this.insert_replace(true);};
	
	this.insert_replace = function (do_replace)
	{
		var init_flow_idxb 	= this.swap2MasterFlow();

		if(!this.valid(do_replace))
			return false;		
		
		if(!do_replace)
		{
			this.setNbCtrl(this.getNbCtrl() + 1);
			
			this.setCtrlIdx(this.getNbCtrl()); /** lead to bug? **/
		}
		else
			this.removeValues(this.getCtrlIdx());
		
		var meta = new Object();
		
		if (this.isSrcNao())
		{
			/** add nao in selection list **/
			var id		= getNaoId();
			var name 	= getSelectE(id).value;
			this.addCtrlValue(this.type + "-name", name);			
			this.addCtrlValue(this.type + "-output", householder[id + "-map"][name]);	
			this.addCtrlValue(this.type + "-output-opt", getE("profile").value + "@" + householder[id + "-map"][name]);	
			this.addCtrlValue(this.type + "-cat", "nao");
			this.addCtrlValue(this.type + "-uc", "use");
		}
		else
		{
			var cat 	= getE(this.type + "-cat").value;
			var attrs 	= ((this.type != "srv") ? ["org","cat"] : ["cat"]).concat(this.val_attrs[cat]);
			var sels 	= this.val_sel[cat];			

			for(var idx=0 ; idx<attrs.length ; idx++)
				this.addCtrlValue(this.type + "-" + attrs[idx], getE(this.type + "-" + attrs[idx]).value);
				
			if (cat == "Single service")
				this.addCtrlValue(this.type + "-one-proto", getSelectE(this.type + "-one-proto").value);
			else if (cat == "Host")
			{
				this.addCtrlValue(this.type + "-host-env", getSelectE(this.type + "-host-env").value);
				meta["area"] = getE(this.type + "-host-area").value;
			}
			else if (cat == "Subnet")
			{
				meta["area"] = getE(this.type + "-subnet-area").value;
			}
			else if (cat == "Group of objects")
				this.addCtrlValue(this.type + "-goo-env", getSelectE(this.type + "-goo-env").value);
				
				
			for (var idx=0 ; idx<sels.length ; idx++)
				this.addCtrlValue(this.type + "-" + sels[idx], select_list2values(getE(this.type + "-" + sels[idx]).options, ";;;"));

			var ctrl_id = this.type + "-" + this.auto_id[cat];
			this.addCtrlValue(this.type + "-uc" , (isInSuggestionList(ctrl_id, getE(ctrl_id).value)) ? "use" : "create");
			//this.addCtrlValue(this.type + "-output", getE(ctrl_id).value);
			this.addCtrlValue(this.type + "-output", this.getIdUsingNamingConvention());
			
			this.addCtrlValue(this.type + "-meta", meta_map2params(meta));
		}	
		
		this.flow.setIdx(init_flow_idxb);
		setHtml("output-" + this.name, map2select_options(this.getCtrlListOutput(), this.getCtrlListOutputColor()));
		
		this.clear(0);
		mask_off();
		hideError("output-" + this.name);
		//hide(this.name + "-dialog");
		switch_flow_dialog_view("flow");

		this.updateNaoView();		
		
		jq_hide(this.name + "-edit");
	}
	
	this.isSrcNao = function(){return isX(this.name, "source") && isNao();};
	
	this.updateNaoView = function()
	{
		if (this.flow.source.getNbCtrl()>0)
		{
			jq_disabled("dataflow-type");			
			show("edit-dataflow-type-button");
			
			if (isNao())
			{
				jq_disabled("flow-action");
				jq_disabled("profile");
				show("edit-flow-action-button");
				show("edit-profile-button");
			}
			else 
			{
				jq_enabled("flow-action");
				jq_enabled("profile");
				hide("edit-flow-action-button");
				hide("edit-profile-button");
			}
		}	
		else 
		{
			jq_enabled("dataflow-type");
			jq_enabled("flow-action");
			jq_enabled("profile");				
			hide("edit-dataflow-type-button");
			hide("edit-flow-action-button");
			hide("edit-profile-button");
		}
	};
	
	/** ctrl idx dependant **/
	this.getIdUsingNamingConvention = function()
	{
		var id = "";
		var cat = this.getCtrlValue(this.type + "-cat")
		if (this.type != "srv")
		{
			if (cat=="Host")
			{
				id = getOrgShort(this.getCtrlValue(this.type + "-org"));
				id += "-host-" + normalize(this.getCtrlValue(this.type + "-host"));
				id += "-" + this.getCtrlValue(this.type + "-host-env");
				id += "-" + this.getCtrlValue(this.type + "-ip");				
			}
			else if (cat=="Subnet")
			{
				id = getOrgShort(this.getCtrlValue(this.type + "-org"));
				id += "-net-" + replaceAllIn(this.getCtrlValue(this.type + "-subnet"), "/", "-");
			}
			else if (cat=="Group of objects")
			{
				var goo = this.getCtrlValue(this.type + "-goo");
				id = getOrgShort(this.getCtrlValue(this.type + "-org"));
				id += "-" + normalize(goo);
				id += "-" + this.getCtrlValue(this.type + "-goo-env");
				//id += "-" + (isInSuggestionList(this.type + "-global-goo", goo) ? "grp" : "lgrp");	
				id += "-grp";
			}
		}
		else 
		{
			if (cat=="Single service")
			{
				id = this.getCtrlValue(this.type + "-one-proto");
				id += "-" + normalize(this.getCtrlValue(this.type + "-one-name"));
				id += isX(this.getCtrlValue(this.type + "-one-proto"), "icmp") ? "" : ("-" + this.getCtrlValue(this.type + "-one-port"));
			}
			else if (cat=="Group of services")
			{
				id = normalize(this.getCtrlValue(this.type + "-gsrv-name")) + "-grp";
			}
		}
		return id;
	}
	
	this.cancel = function()
	{		
		this.clear(0);
		mask_off();
		//hide(this.name + "-dialog");
		switch_flow_dialog_view("flow");
		
	};
	
	this.clear = function(grade)
	{
		jq_hide("warning-error");
		jq_hide("control-create");
		if (this.type != "srv")
		{
			if (grade <= 1)
			{
				setSelectE(this.type + "-org", default_choice);				
			}
			if (grade <= 2)
			{
				setSelectE(this.type + "-cat", default_choice);
				uncheckAllRadio(this.type + "-action");
				jq_hide(this.type + "-primary-rows");
			}
			if (grade <= 3)
			{
			
				var fields = ['-host','-ip','-ip-tsl','-host-area','-subnet','-subnet-area','-goo','-goo-selector'];
				for (var f_idx=0 ; f_idx<fields.length ; f_idx++)
					getE(this.type + fields[f_idx]).value = "";

				setSelectEC(this.type + "-host-env", default_choice);
				setSelectEC(this.type + "-goo-env", default_choice);
				setSelectEC(this.type + "-goo-sel-env", default_choice);
				
				removeOptions(this.type + "-hosts");
				removeOptions(this.type + "-subnets");
				removeOptions(this.type + "-goos");
				jq_hide(this.type + "-host-rows");
				jq_hide(this.type + "-subnet-rows");
				jq_hide(this.type + "-goo-rows");				
				jq_hide(this.type + "-goo-selection-rows");
				this.resize_group_wrapper(false);
				/*
				hide("tr-" + this.type + "-host-area");
				hide("tr-" + this.type + "-subnet-area");
				*/

				var cons_checks = ["ip","subnet"];
				for (var k=0 ; k<cons_checks.length ; k++)
				{
					hide(this.type + "-" + cons_checks[k] + "-check");
					hide(this.type + "-" + cons_checks[k] + "-error");
					getE(this.type + "-" + cons_checks[k] + "-code").value = "";
					hideError(this.type + "-" + cons_checks[k] + "-opt");
				}
				
				if (isX(this.name, "source"))
				{
					getE("src-nao-auth").value = getE("src-nao-enc").value = "";
				}
			}
		}
		else
		{
			if (grade <= 1)
			{
				setSelectE(this.type + "-cat", default_choice);			
			}
			if (grade <= 2)
			{
				uncheckAllRadio(this.type + "-action");
						
				var fields = ['-one-name','-one-type','-one-port','-gsrv-name'];
				for (var f_idx in fields)
					getE(this.type + fields[f_idx]).value = "";	
				
				setSelectE(this.type + "-one-proto", default_choice);
				this.changeProto();
				
				removeOptions(this.type + "-gsrv-srvs");
				//clearMSelect(this.type + "-gsrv-srvs");
				jq_hide(this.type + "-primary-rows");
				jq_hide(this.type + "-one-rows");
				jq_hide(this.type + "-gsrv-rows");
				jq_hide(this.type + "-gsrv-selection-rows");
				
				jq_enabled("srv-autocomplete-rows");
			}
		}
	};
	
	this.close = function()
	{
		this.clear(0);
	};
	
	this.show_add_buttons = function()
	{
		if (!isEmpty(getSelectE("output-" + this.name).value))
		{
			jq_show(this.name + "-edit");
			jq_show(this.name + "-delete");
		}
	};
	
	this.clearInfo = function(option_delete)
	{
		if (this.type != "srv")
		{
			/** do clear iff doAutocompleteOnValueChange is called **/
			if (option_delete == 1) //delete host values
			{
				var fields = [/*'-host','-ip',*/'-ip-tsl'];
				for (var f_idx in fields)
					getE(this.type + fields[f_idx]).value = "";
					
				// setSelectE(this.type + "-host-env", default_choice);
				
				hide(this.type + "-ip-check");
				hide(this.type + "-ip-error");
				hide(this.type + "-ip-warning");
				hide("warning-error-tr-" + this.type + "-ip-opt");
				getE(this.type + "-ip-code").value = "";					
			}
			else if(option_delete == 2) //delete subnet values
			{
				getE(this.type + "-subnet-area").value = "";
			}
			else if(option_delete == 3) //delete goo values
			{
				setSelectE(this.type + "-goo-env", default_choice);
			}
		}
		else
		{
			if (option_delete == 1) //delete srv-one values
			{
				getE(this.type + "-one-port").value = "";	
				setSelectEC(this.type + "-one-proto", default_choice);
			}
			else if(option_delete == 2) //delete srv-gsrv values
			{
			}
		}
	};
		
	this.changeOrganization = function(func)
	{
		this.clear(2);
		if(!isEmptyOrDefault(getSelectE(this.type + "-org").value))
		{
			modal_on();
			send_request
			(
				{
					"method":'GET',
					"url":default_url,
					"callback": (func!=null) ? func : set_org_list_elements,
					"data":
					{
						"action":access('list-organization-elements'),
						"application-service":getE("application-service").value,
						"organization":getSelectE(this.type + "-org").value,
						"type":this.type
					}
				}
			);
			jq_show(this.type + "-primary-rows");
		}		
	};
	
	this.changeCategory = function()
	{
		if (this.type!="srv")
		{
			this.clear(3);
			var cat = getSelectE(this.type + "-cat").value;
					
			if (isX(cat,"Host"))
			{
				jq_hide(this.type + "-group-mandatory");
				jq_show(this.type + "-mandatory");
				jq_show(this.type + "-host-rows");
				
				if (isX(getSelectE(this.type + "-org").value,"MOBISTAR"))
					show("tr-" + type + "-host-area");
				else
					hide("tr-" + type + "-host-area");
			}
			else if(isX(cat,"Subnet"))
			{
				jq_hide(this.type + "-group-mandatory");
				jq_show(this.type + "-mandatory");
				jq_show(this.type + "-subnet-rows");
				
				if (isX(getSelectE(this.type + "-org").value,"MOBISTAR"))
					show("tr-" + type + "-subnet-area");
				else
					hide("tr-" + type + "-subnet-area");
			}
			else if(isX(cat,"Group of objects"))
			{
				jq_show(this.type + "-group-mandatory");
				jq_hide(this.type + "-mandatory");
				jq_show(this.type + "-goo-rows");
				this.resize_group_wrapper();
			}
		}
		else
		{
			this.clear(2);
			var cat = getSelectE(this.type + "-cat").value;
			if (isX(cat,"Single service"))
			{
				jq_show(this.type + "-mandatory");
				jq_show(this.type + "-one-rows");				
			}
			else if (isX(cat,"Group of services"))
			{
				jq_hide(this.type + "-mandatory");
				jq_show(this.type + "-gsrv-rows");
			}
		}
	};

	this.changeHost = function()
	{
		if (isX(getSelectE(this.type + "-org").value,"EXTERNAL"))
			getE(this.type + "-ip").value = "";
		this.changeHostIp("host");
	};
	this.changeIp = function(){this.changeHostIp("ip")};
	this.changeHostIp = function(elem)
	{
		
		var type 	= this.type;
		var tmp_k 	= type + '-' + elem;
		var tmp_v 	= getE(type + '-' + elem).value;
		//if (isX(householder[tmp_k + "-tmp"], tmp_v)){return -1;}
		this.clearInfo(1);
		getE(tmp_k).value = tmp_v;
		/** caution host-ip recursive interaction **/
		var isInList = doAutocompleteOnValueChange
		(
			type + "-" + elem, 
			getE(type + "-" + elem).value,
			{
				"type":type, 
				"element":elem, 
				"name":getE(type + '-' + elem).value, 
				"ip":getE(type + '-' + elem).value,
				"org":getSelectE(type + '-org').value
			}, 
			function(request)
			{
				getE(type + "-host").value 		= request['json'].name;
				getE(type + "-ip").value 		= request['json'].ip;
				getE(type + "-ip-tsl").value 	= request['json'].tsl_ip;
				
				if (isX(getSelectE(type + '-org').value, "MOBISTAR"))
				{
					getE(type + "-host-area").value = request['json'].area;
					show("tr-" + type + "-host-area");
				}
				else
					hide("tr-" + type + "-host-area");
				
				setSelectEC(type + "-host-env", (isEmpty(getE(type + "-host").value) ? default_choice : getEnvProposition(request['json'].environment)));
				
				if (!isEmpty(request['json'].ips))
				{
					setError(type + "-ip-opt", "Some other ip(s) matche(s) to this host: " + request['json'].ips);
					showError(type + "-ip-opt");
					show(type + "-ip-warning");
				}
				else
				{
					hideError(type + "-ip-opt");
					hide(type + "-ip-warning");
				}
				
				set_select_ro_inputs();
			}
		)
		if (isInList)
		{	
			disable_autocomplete_fields(this.type + "-host-autocomplete-rows");
			jq_hide("control-create-host");
		}
		else
		{
			enable_autocomplete_fields(this.type + "-host-autocomplete-rows");
			
			/** if External user attempt to create new host check if the ip is not in private range **/
			if (elem=="ip" && isIp(getE(type + '-ip').value) && getSelectE(this.type + "-org").value=="EXTERNAL")
			{
				getE(this.type + "-ip-code").value = "0";
				send_request
				(
					{
						"method":'GET', 
						"url":default_url, 
						"callback": checkExternalIpRanges, 
						"data":
						{
							"action":access('check-private-range-constraint'),
							"type":this.type,
							"element":'ip',
							"ip":getE(type + '-ip').value
						}
					}
				);
				modal_on();				
			}
			/*
			if (elem=="host")
				getE(type + '-ip').value = "";
			*/
			if (!isX(getSelectE(this.type + "-org").value, "MOBISTAR"))
				jq_show("control-create-host");
		}
	};

	this.changeSubnet = function()
	{
		var type = this.type;
		var id = type + "-subnet";		
		this.clearInfo(2);

		var isInList = doAutocompleteOnValueChange
		(
			type + "-subnet", 
			getE(type + "-subnet").value,
			{
				"type":type, 
				"element":"subnet", 
				"subnet":getE(type + '-subnet').value,
				"org":getSelectE(type + '-org').value
			}, 
			function(request)
			{
				getE(type + "-subnet-area").value = request['json'].area;
				
				if (isEmpty(getE(type + "-subnet-area").value))
					hide("tr-" + type + "-subnet-area");
					
				set_select_ro_inputs();
			}
		)
		
		if (isInList)
		{
			disable_autocomplete_fields(this.type + "-subnet-autocomplete-rows");
			jq_hide("control-create-subnet");
		}
		else
		{
			enable_autocomplete_fields(this.type + "-subnet-autocomplete-rows");
			
			if (isSubnetWithMask(getE(type + '-subnet').value) && getSelectE(this.type + "-org").value=="EXTERNAL")
			{
				getE(this.type + "-subnet-code").value = "0";
				send_request
				(
					{
						"method":'GET', 
						"url":default_url, 
						"callback": checkExternalIpRanges, 
						"data":
						{
							"action":access('check-private-range-constraint'),
							"type":this.type,
							"element":'subnet',
							"subnet": getE(type + '-subnet').value
						}
					}
				);
				modal_on();				
			}
			if (!isX(getSelectE(this.type + "-org").value, "MOBISTAR"))
				jq_show("control-create-subnet");
		}
	};
	
	this.changeSubnetArea = function()
	{

	};

	this.changeGOO = function()
	{
		var type = this.type;
		var id = type + "-goo";
		this.clearInfo(3);
		
		var isInList = doAutocompleteOnValueChange
		(
			type + "-goo", 
			getE(type + "-goo").value,
			{
				"type":type, 
				"element":"goo", 
				"name":getE(type + '-goo').value,
				"org":getSelectE(type + '-org').value
			}, 
			function(request)
			{
				setSelectEC(type + "-goo-env", getEnvProposition(request['json'].environment));
				setSelectEC(type + "-goo-env", (isEmpty(getE(type + "-goo").value) ? default_choice : getEnvProposition(request['json'].environment)));
				set_select_ro_inputs();
			}
		)
			
		if (isInList)
		{
			disable_autocomplete_fields(this.type + "-autocomplete-rows");
			jq_hide(type + "-goo-selection-rows");
			jq_hide("control-create");	// hide all child(s) create icon 
			removeOptions(this.type + "-hosts");
			removeOptions(this.type + "-subnets");
			removeOptions(this.type + "-goos");
		}
		else
		{
			enable_autocomplete_fields(this.type + "-autocomplete-rows");
			jq_show(type + "-goo-selection-rows");
			jq_show("control-create-goo");
			$('.group-element-scrollable').scrollTop(0);
		}		
		this.resize_group_wrapper(!isInList);
	};
	
	this.resize_group_wrapper = function(resize)
	{
		if (isIE8())
		{	
			if (resize)
				jq_css(this.type + "-group-element-wrapper", "height", "400px");
			else
				jq_css(this.type + "-group-element-wrapper", "height", "");
		}
	};
	
	this.changeGOO_sel = function ()
	{
		var type = this.type;
		var isInList = doAutocompleteOnValueChange
		(
			type + "-goo-selector", 
			getE(type + "-goo-selector").value,
			{
				"type":type, 
				"element":"goo", 
				"name":getE(type + '-goo-selector').value,
				"ts_name":getE('application-service').value,
				"org":getSelectE(type + '-org').value
			}, 
			function(request)
			{
				setSelectEC(type + "-goo-sel-env", getEnvProposition(request['json'].environment));
				set_select_ro_inputs();
			}
		);
	};	

	this.changeEnv = function(attr)
	{
	};
	
	this.changeService = function()
	{
		var type 	= this.type;
		var srv 	= getE('srv-one-name').value;
		this.clearInfo(1);
		var isInList = doAutocompleteOnValueChange
		(
			'srv-one-name', 
			srv,
			{"element":"service", "name":srv, "application-service":getE('application-service').value}, 
			function(request)
			{
				getE('srv-one-name').value 		= request['json'].name;
				setSelectE('srv-one-proto', request['json'].protocol);
				changeSrvProto();
				getE('srv-one-type').value 		= request['json'].type;
				getE('srv-one-port').value 		= request['json'].port;		
				set_select_ro_inputs();
			}
		)

		if (isInList)
		{
			jq_disabled("srv-autocomplete-rows");
			jq_hide("control-create-srv");
		}
		else 
		{
			jq_enabled("srv-autocomplete-rows");
			if (!isEmpty(srv))
				jq_show("control-create-srv");
			else 
				jq_hide("control-create-srv");
		}
	};

	this.changeProto = function()
	{
		if (isX(getSelectE(this.type + "-one-proto").value, "icmp"))
		{
			hide("row-srv-one-port");
			getE("srv-one-port").value = "-";			
		}
		else
		{
			show("row-srv-one-port");
			getE("srv-one-port").value = "";
		}
	};
	
	this.changePort = function()
	{
		var type 	= this.type;
		var port 	= getE('srv-one-port').value;
		this.clearInfo(1);
		var isInList = doAutocompleteOnValueChange
		(
			'srv-one-port', 
			port,
			{"element":"srv-port", "port":port, "application-service":getE('application-service').value}, 
			function(request)
			{
				getE('srv-one-name').value 		= request['json'].name;
				setSelectE('srv-one-proto', request['json'].protocol);
				changeSrvProto();
				getE('srv-one-type').value 		= request['json'].type;
				getE('srv-one-port').value 		= request['json'].port;		
				set_select_ro_inputs();
			}
		)

		if (isInList)
		{
			jq_disabled("srv-autocomplete-rows");
			jq_hide("control-create-srv");
		}
		else 
		{
			jq_enabled("srv-autocomplete-rows");
			if (!isEmpty(srv))
				jq_show("control-create-srv");
			else 
				jq_hide("control-create-srv");
		}
		/*
		var port = getE("srv-one-port").value;
		if (!isPort(port))
			return ;

		send_request
		(
			{
				"method":'GET',
				"url":default_url,
				"data":
				{
					"action":access('autocomplete'),
					"element":'srv-port',
					"port":port,
					"application-service":getE("application-service").value
				},
				"callback":function (request)
				{
					
					if (isEmpty(getE("srv-one-name").value) && !isEmpty(request['json'].name))
					{
						getE("srv-one-name").value = request['json'].name;
						setSelectE("srv-one-proto", request['json'].protocol);
						getE("srv-one-port").value = request['json'].port;
						
						jq_disabled("srv-autocomplete-rows");
						jq_hide("control-create-srv");
					}
					

					addNewAutoSuggest("srv-one-port", request['json']['srvs'], changePort);
				}
			}
		);
	/* */
	};
	
	this.changeGroupOfServices = function()
	{
		var id = "gsrv";
		this.clearInfo(2);
		if (isInSuggestionList("srv-gsrv-name",getE("srv-gsrv-name").value))
		{
			jq_disabled("srv-autocomplete-rows");
			jq_hide("srv-gsrv-selection-rows");
			jq_hide("control-create");		// hide all child(s) create icon 
			removeOptions(this.type + "-gsrv-srvs");
		}
		else
		{
			jq_enabled("srv-autocomplete-rows");
			jq_show("srv-gsrv-selection-rows");
			jq_show("control-create-gsrv");
		}
	};
	
	/** 
		if this ctrl (dest,srv) refers to a master flow ctrl then use that flow index ctrl values 
		@post flow index has been swapped to master flow index
		@return the initial flow index before index swap (even if no swap).
	**/
	this.swap2MasterFlow = function ()
	{
		var init_flow_idx = this.getFlowIdx();;
		if (!isX(this.type, "src"))
		{
			var fmeta = this.flow.getFlowInfo("meta");
			if (!isEmpty(fmeta))
			{
				var flow_meta = meta_params2map(fmeta);
				if (isX(flow_meta['from-rule'], "true") && !isX(flow_meta['f-master'], this.getFlowIdx() + ""))
				{
					this.flow.setIdx(flow_meta['f-master']);
				}
			}			
		}
		return init_flow_idx;
	};
	
	this.getCtrlListOutputColor = function()	
	{
		var out 			= new Object();		
		var init_flow_idxb 	= this.swap2MasterFlow();
		
		for (var i=1; i<=this.getNbCtrl() ; i++)
		{
			/** key=ctrl_idx : value=ctrl_output  **/
			var pfx 	= "f-" + this.getFlowIdx() + "." + i + "-" + this.type + "-";
			var opt 	= !isEmpty(this.elems[pfx + "output-opt"]) ? "-opt" : "";
			var uc 		= this.elems[pfx + "uc"];
			
			if (isX(uc, "remove") | isX(uc, "delete"))
				out[i] = getActionColor(uc);
			else
				out[i] = "";
		}
		this.flow.setIdx(init_flow_idxb);
		return out;
	};
	
	this.getCtrlListOutput = function()	
	{
		var out 			= new Object();		
		var init_flow_idxb 	= this.swap2MasterFlow();
		
		for (var i=1; i<=this.getNbCtrl() ; i++)
		{
			/** key=ctrl_idx : value=ctrl_output  **/
			var pfx 	= "f-" + this.getFlowIdx() + "." + i + "-" + this.type + "-";
			var opt 	= !isEmpty(this.elems[pfx + "output-opt"]) ? "-opt" : "";
			var uc 		= this.elems[pfx + "uc"];
			
			if (isX(uc, "remove") | isX(uc, "delete"))
				out[i] = "<span style='color:" + getActionColor(uc) + ";'>" + this.elems[pfx + "output" + opt] + "</span>";
			else
				out[i] = this.elems[pfx + "output" + opt];
		}
		
		this.flow.setIdx(init_flow_idxb);	
		return out;
	};

	this.getCtrlListOutput_fwo = function()
	{
		var out 			= new Object();
		var init_flow_idxb 	= this.swap2MasterFlow();
		for (var i=1 ; i<=this.getNbCtrl() ; i++)
		{
			var pfx 	= "f-" + this.getFlowIdx() + "." + i + "-" + this.type + "-";
			var opt 	= !isEmpty(this.elems[pfx + "output-opt"]) ? "-opt" : "";
			var fw_name = this.elems[pfx + "output"];
			var display	= this.elems[pfx + "output" + opt];
			var uc		= this.elems[pfx + "uc"];
			var cat		= this.elems[pfx + "cat"];
			if (hasX(cat,"group") && isX(uc, "create"))
			{
				var members = "";
				for (var j=0 ; j<this.val_sel[cat].length ; j++) 
				{
					var selections = this.elems[pfx + this.val_sel[cat][j]];
					
					if (!isEmpty(selections))
					{
						var selection_list = selections.split(";;;");
						for (var k=0 ; k<selection_list.length ; k++)
						{
							var selection 	= in_params2map(selection_list[k]);
							members += "<tr><td><span style='color:" + getActionColor(selection['uc']) + ";'>" + selection['id'] + "</span></td></tr>";
						}
					}
				}
				members = "<table id='members-" + fw_name + "' class='group-component grp-group-members' style='width:300px;'><tr><th id='" + fw_name + "-handle'>" + display + " member(s)</th></tr>" +  members + "</table>";

				setHtml("groups-members-list", getHtml("groups-members-list") + members);
				out[i] 	= "<span style='cursor:pointer;color:" + getActionColor(uc) + ";' onclick=openGrp('" + fw_name + "')>" + display + "</span>";
			}	
			else
				out[i] = "<span style='color:" + getActionColor(uc) + ";'>" + display + "</span>";
		}
		
		this.flow.setIdx(init_flow_idxb);
		return out;
	};
	
	/** conflict not occurs if conflicting objects belongs to different organizations **/
	this.checkCtrlValueConflict = function(conflict_name, org, id, info, conflicts, strong)
	{
		var ctrl_id = !isEmpty(org) ? org + "-" + id : id;
	
		if (isEmpty(conflicts[ctrl_id]))
			conflicts[ctrl_id] = info;
		else if (strong || !isX(conflicts[ctrl_id], info))	//soft contraint disallow two of same 'id' objects with different information
		{
			if (isEmpty(conflicts["error_" + ctrl_id]))
			{
				var message = "<tr class='warning-conflict'><td>conflict detected with " + conflict_name + " : '" + id + "'</td></tr>";
				conflicts["message"] = isEmpty(conflicts["message"]) ? message : conflicts["message"] + message;
				conflicts["error_" + ctrl_id] = true;	//avoid redundant error message;
			}
		}
	};
	/** check src, dest service objects creation conflicts **/
	/** 
		strong conflicts management, if two (or more) objects (creation mode) are detected with same id 
		then warn a conflict.
	**/
	this.checkCtrlConflicts = function(conflicts)
	{
		for (var ctrl_idx=1 ; ctrl_idx<=this.getNbCtrl() ; ctrl_idx++)
		{
			this.setCtrlIdx(ctrl_idx);
			if (isX(this.getCtrlValue(this.type + "-uc"), "create"))
			{				
				var ctrl_id = this.getCtrlValue(this.type + "-output");					
				if (this.type!="srv")
				{
					if (isX(this.getCtrlValue(this.type + "-cat"), "Host"))
					{
						var host 	= this.getCtrlValue(this.type + "-host");
						var ip 		= this.getCtrlValue(this.type + "-ip");
						var tsl 	= (!isEmpty(this.getCtrlValue(this.type + "-ip-tsl"))) ? this.getCtrlValue(this.type + "-ip-tsl") : "";
						var h_infos = ip + ":::" + tsl;
						var i_infos = host + ":::" + tsl;
						
						this.checkCtrlValueConflict("host name", this.getCtrlValue(this.type + "-org"), host, h_infos, conflicts, use_strong_constraint);
						this.checkCtrlValueConflict("host ip address", this.getCtrlValue(this.type + "-org"), ip, i_infos, conflicts, use_strong_constraint);
					}
					/** subnet has unique value id
					else if (isX(this.getCtrlValue(this.type + "-cat"), "Subnet"))
					{
						this.checkCtrlValueConflict("subnet", this.getCtrlValue(this.type + "-org"), ctrl_id, ctrl_id, conflicts, use_strong_constraint);
					}
					*/
					else if (isX(this.getCtrlValue(this.type + "-cat"), "Group of objects"))
					{						
						this.checkCtrlValueConflict("group of objects", this.getCtrlValue(this.type + "-org"), ctrl_id, ctrl_id, conflicts, true);
						/** checks for its host members **/
						var org = this.getCtrlValue(this.type + "-org");
						if (!isEmpty(this.getCtrlValue(this.type + "-hosts")))
						{
							var list = this.getCtrlValue(this.type + "-hosts").split(";;;");
							
							for (var idx=0 ; idx<list ; idx++)
							{
								var host = in_params2map(list[idx]);

								if (isX(host["uc"], "create"))
								{
									var name	= host["host"];
									var ip 		= host["ip"];
									var tsl 	= (!isEmpty(host["ip-tsl"])) ? host["ip-tsl"] : "";
									var h_infos = ip + ":::" + tsl;
									var i_infos = name + ":::" + tsl;									
									
									this.checkCtrlValueConflict("host name", org, name, h_infos, conflicts, use_strong_constraint);
									this.checkCtrlValueConflict("host ip address", org, ip, i_infos, conflicts, use_strong_constraint);
								}								
							}
						}						
					}
				}
				else
				{
									
					if (isX(this.getCtrlValue(this.type + "-cat"), "Single service"))
					{					
						var info = this.getCtrlValue(this.type + "-one-proto") + ":::";
						info += this.getCtrlValue(this.type + "-one-type") + ":::";
						info += this.getCtrlValue(this.type + "-one-port");
						this.checkCtrlValueConflict("service", "", ctrl_id, info, conflicts, use_strong_constraint);
					}
					else if (isX(this.getCtrlValue(this.type + "-cat"), "Group of services"))
					{
						//the group of services id must be unique
						this.checkCtrlValueConflict("group of services", "", ctrl_id, ctrl_id, conflicts, true);
						/** checks for its service members **/
						
						if (!isEmpty(this.getCtrlValue(this.type + "-gsrv-srvs")))
						{
							var list = this.getCtrlValue(this.type + "-gsrv-srvs").split(";;;");
							for (var idx=0 ; idx<list.length ; idx++);
							{
								var service = in_params2map(list[idx]);
								if (isX(service["uc"], "create"))
								{
									var info = service["proto"] + ":::";
									info += service["type"] + ":::";
									info += service["port"];
									this.checkCtrlValueConflict("service", "", service["name"], info, conflicts, use_strong_constraint);
								}								
							}
						}
					}
				}
			}
		}
	};
	
	return this.initialize(name, type, multiple);
};

/*****************
	FLOW OBJECT
******************/

function fcrweb_flow_object(src, dest, srv)
{
	this.initialize = function(src, dest, srv)
	{
		this.source 		= src;
		this.destination 	= dest;
		this.service 		= srv;
		//other flow(s) values are store in elems (description,action,end-date)
		this.elems 			= new Object();
		this.store			= new Object();
		this.setNbFlow(0);
		this.setIdx(-1);
		return this;
	};
	
	this.getNbFlow = function(){return this.elems['f-nb-flow'];};
	this.setNbFlow = function(nbFlow){this.elems['f-nb-flow'] = nbFlow;};
	this.getIdx = function(){return this.elems['f-idx'];};
	this.setIdx = function(idx){this.elems['f-idx'] = idx;};
	
	
	this.addFlowInfo = function(key, value)
	{
		this.elems["f-" + this.getIdx() + "-" + key] = value;
	};
	
	this.getFlowInfo = function(key)
	{
		return this.elems["f-" + this.getIdx() + "-" + key];
	};
	
	/**
		remove idx th flow information except sources,destinations,services
	**/
	this.flow_info = ['flow-description','profile','profile-owner','profile-description','end-date'];
	this.removeFlowInfo = function(idx)
	{
		var info = this.flow_info.concat(['dataflow-type','flow-action','profile-uc']);
		for (var info_idx=0 ; info_idx<info.length ; info_idx++)
			delete this.elems["f-" + idx + "-" + info[info_idx]];
	};
	
	/** remove flow at index specified by idx **/
	this.removeFlow = function(idx)
	{
		for (var key in this.elems)
		{
			if (hasX(key, "f-" + idx))
				delete this.elems[key];
		}
		source.removeFlowValues(idx);
		destination.removeFlowValues(idx);
		service.removeFlowValues(idx);
	};
	
	/** swap flow values index from old_idx to new_idx **/
	this.replaceFlow = function(old_idx, new_idx)
	{
		/** clean up new index **/
		this.removeFlow(new_idx);
		
		for (var key in this.elems)
		{
			if (hasX(key, "f-" + old_idx))
				this.elems[key.replace("f-" + old_idx, "f-" + new_idx)] = this.elems[key];				
		}
		source.replaceFlowIndex(old_idx, new_idx);
		destination.replaceFlowIndex(old_idx, new_idx);
		service.replaceFlowIndex(old_idx, new_idx);
		
		this.removeFlow(old_idx);
	};
	
	this.saveInStore = function()
	{
		this.store = new Object();
		for (var key in this.elems)
		{
			if (hasX(key, "f-" + this.getIdx()))
				this.store[key] = this.elems[key];
		}
		source.saveInStore();
		destination.saveInStore();
		service.saveInStore();
	};
	
	this.restoreFromStore = function()
	{
		this.removeFlow(this.getIdx());
		for (var key in this.store)
		{
			this.elems[key] = this.store[key];
		}
		source.restoreFromStore();
		destination.restoreFromStore();
		service.restoreFromStore();
	};
	
	this.leastOneFlow = function()
	{
		var hasLeast = isPos(this.getNbFlow());
		if (hasLeast)
		{
			jq_disabled("application-service");
			show("edit-application-service-button");
		} else {
			jq_enabled("application-service");
			hide("edit-application-service-button");
		} 
		return hasLeast;
	};

	

	/** 
		FLOW OPEN CONTROLS 
	**/
	this.create = function()
	{
		if (!check_app_srv_pre(true))
			return -1;
			
		hide("main-section");
		$('.flow-section').show();
		setFormHeight("flow");
		changeApplicationService();
			
		this.setIdx(this.getNbFlow() + 1);
		/** reset ctrl-output selection **/
		setHtml("output-source","");
		setHtml("output-destination","");
		setHtml("output-service","");
		this.clear(1);		
		
		this.source.updateNaoView();
		
		jq_hide("output-edit");
		jq_hide("output-delete");
		
		jq_hide("flow-edit-button");
		jq_show("flow-add-button");
	};
	
	this.load = function(selected_flow_idx)
	{
		
		if (!check_app_srv_pre(false))
		{
			notify("Error: undetermined application, service context")
			return -1;
		}
		
		changeApplicationService();
		jq_hide("flow-add-button");
		jq_show("flow-edit-button");
		
		this.setIdx(selected_flow_idx);		
		this.saveInStore();
		
		checkRadio(isX(this.getFlowInfo("dataflow-type"),"Host 2 Host") ? "H2H" : "U2H");
		changeDataflowType();
		setSelectE("flow-action", this.getFlowInfo("flow-action"));	
		
		for (var fi_idx in this.flow_info)
		{
			getE(this.flow_info[fi_idx]).value = this.getFlowInfo(this.flow_info[fi_idx]);
		}	
		getLdapUsers("profile-owner");
		/** TODO set ctrl-output selection **/
		setHtml("output-source", map2select_options(this.source.getCtrlListOutput(), this.source.getCtrlListOutputColor()));
		setHtml("output-destination", map2select_options(this.destination.getCtrlListOutput(), this.destination.getCtrlListOutputColor()));
		setHtml("output-service", map2select_options(this.service.getCtrlListOutput(), this.service.getCtrlListOutputColor()));
		
		var f_meta = meta_params2map(this.getFlowInfo("meta"));
		if (isX(f_meta['from-rule'], "true"))
		{
			jq_disabled("rule-flow");
			jq_hide("flow-edit-icon");
		}
		else
		{
			jq_enabled("rule-flow");
			this.source.updateNaoView();
		}
		
		jq_hide("output-edit");
		jq_hide("output-delete");
		
		hide("main-section");
		$('.flow-section').show();
		setFormHeight("flow");
		
		setReadonlyView($("#readonly").val());
	};
	
	this.remove = function(remove_idx)
	{
		/** rule flow cannot be deleted. **/
		this.setIdx(remove_idx);
		var flow_meta = meta_params2map(this.getFlowInfo("meta"));
		if (isX(flow_meta['from-rule'], "true"))
		{
			
			if (isX(flow_meta["rule-action"], "delete"))
				flow_meta["rule-action"] = "update";
			else if (isX(flow_meta["rule-action"], "update") && confirm("this rule will be removed from firewall?"))
				flow_meta["rule-action"] = "delete";
			
			this.addFlowInfo("meta", meta_map2params(flow_meta));
			this.refreshFlowsView();
			return ;
		}
		
		if (isX(getE('debug-area').value,'true') || confirm("confirm delete the flow."))
		{
			/** in order to avoid gap in control indexes, reset last index control to current index. **/
			if (remove_idx < this.getNbFlow())
				this.replaceFlow(this.getNbFlow(), remove_idx);
			else
				this.removeFlow(remove_idx);			
			
			this.setNbFlow(this.getNbFlow() - 1);
			this.setIdx(this.getNbFlow());		// set index to last ctrl
			
			this.leastOneFlow();			
			this.refreshFlowsView();
		}
	};
	
	/** 
		FLOW CLOSE CONTROLS 
	**/
	
	this.valid = function()
	{
		var valid 	= true;		
		var f_meta 	= meta_params2map(this.getFlowInfo("meta"));
		
		/** rule flow info cannot be changed **/
		if (isX(f_meta['from-rule'],"true"))
		{
			return true;
		}
		
		var next = "flow-description";
		if (isEmpty(getE(next).value))
		{
			showError(next)
			valid = false;
		}		
		next = "dataflow-type";
		var flow_type = getRadio(next).value;
		if (isEmpty(flow_type))
		{
			showError(next);
			valid = false;
		}
		next = "flow-action";
		if (isEmptyOrDefault(getSelectE(next).value))
		{
			showError(next);
			valid = false;
		}

		if (isU2H(flow_type))
		{
			next = "profile";
			var profile = getE(next).value;
			if (isEmpty(getE(next).value))
			{
				showError(next);
				valid = false;
			}
			next = "profile-owner";
			if (isEmpty(getE(next).value) || !isInSuggestionList(next, getE(next).value))
			{
				showError(next);
				valid = false;
			}
			next = "profile-description";
			if (!isInSuggestionList("profile", profile) && isEmpty(getE(next).value))
			{
				showError(next);
				valid = false;
			}			
		}
		
		var list_elements = ["output-source", "output-destination", "output-service"];
		for (var le_idx=0 ; le_idx<list_elements.length ; le_idx++)
		{			
			if (getE(list_elements[le_idx]).options.length<=0)
			{
				showError(list_elements[le_idx]);
				valid = false;
			}
		}
		
		next = "end-date";
		if (!isEmpty(getE(next).value))
		{
			if (!isDate(getE(next).value) || isPastDay(getE(next).value))
			{
				showError(next)
				valid = false;
			}
		}
		
		/** check if an element with same category and same identifier exist in the current flow source and destination **/
		/*
		if (valid)
		{			
			var srcidx 		= source.getCtrlIdx();
			var destidx 	= destination.getCtrlIdx();
			var rdd			= new Object();

			for (var i = 1 ; i <= source.getNbCtrl() ; i++)
			{
				source.setCtrlIdx(i);
				var id = source.getCtrlValue(source.type + "-org") + "---" ;
					id += source.getCtrlValue(source.type + "-cat") + "---" ;
					id += source.getCtrlValue(source.type + "-output");
				rdd[id] = true;
				
			}
			for (var i = 1 ; i <= source.getNbCtrl() ; i++)
			{
				destination.setCtrlIdx(i);
				var id = destination.getCtrlValue(destination.type + "-org") + "---" ;
					id += destination.getCtrlValue(destination.type + "-cat") + "---" ;
					id += destination.getCtrlValue(destination.type + "-output");

				if (rdd[id] != true)
					rdd[id] = true;
				else
				{						
					var err = "the element " + destination.getCtrlValue(destination.type + "-output") ;
						//err += " of category " + destination.getCtrlValue(destination.type + "-cat") ;
						err += " has been detected in source and destination of this flow."
					notify(err);
					valid = false;
				}				
			}
			source.setCtrlIdx(srcidx);
			destination.setCtrlIdx(destidx);
		}	
		*/
		return valid;
	};
	
	this.insert_replace = function (do_replace)
	{
		if(!this.valid())
			return false;		
		
		if(!do_replace)
			this.setNbFlow(this.getNbFlow() + 1);
		else
			this.removeFlowInfo(this.getIdx());
		
		for (var fi_idx in this.flow_info)
			this.addFlowInfo(this.flow_info[fi_idx], getE(this.flow_info[fi_idx]).value);
			
		next = "profile";
	
		if (!isEmpty(getE(next).value))
		{
			if (isInSuggestionList(next, getE(next).value))
				this.addFlowInfo("profile-uc", "use");
			else
				this.addFlowInfo("profile-uc", "create");
		}
		
		this.addFlowInfo("dataflow-type", getRadio("dataflow-type").value);
		this.addFlowInfo("flow-action", getSelectE("flow-action").value);

		$('.flow-section').hide();
		show("main-section");
		setFormHeight(getE("form-selector").value);
		this.leastOneFlow();		
		this.refreshFlowsView();
	}
	this.add = function(){this.insert_replace(false);};
	this.edit = function(){this.insert_replace(true);};
	
	/** 
		if has_copy then edit mode
		recover from copy then close
		else clean up then close;
	**/
	this.cancel = function(has_copy)
	{
		if (!has_copy)
			this.removeFlow(this.getIdx());
		else
			this.restoreFromStore();
			
		$('.flow-section').hide();
		show("main-section");
		setFormHeight(getE("form-selector").value);
		this.leastOneFlow();		
		this.refreshFlowsView();
	};
	
	this.checkConflicts = function()
	{
		/** check creation mode object only **/
		var conflicts 		= new Object();
		var message			= "";
		var src_dest_confs 	= new Object(); 
		var srv_confs 		= new Object(); 
		
		for (var idx=1 ; idx<=this.getNbFlow() ; idx++)
		{
			this.setIdx(idx);
			
			/** check profile conflicts **/
			var profile = this.getFlowInfo("profile");
			if (!isEmpty(profile) && isX(this.getFlowInfo("profile-uc"), "create"))
			{
				var infos = this.getFlowInfo("profile-owner");
				if (!isEmpty(this.getFlowInfo("profile-description")))
					infos += ":::" + this.getFlowInfo("profile-description");
					
				if (isEmpty(conflicts[profile]))
				{
					conflicts[profile] = infos
				}
				else if (use_strong_constraint || !isX(conflicts[profile], infos))	
				{
					if (isEmpty(conflicts["error_" + profile]))
					{
						message += "<tr class='warning-conflict'><td> Conflict in profiles definitions '" + profile + "'</tr></td>";
						conflicts["error_" + profile] = true;	//avoid redundant error message;
					}
					
				}
			}	
			source.checkCtrlConflicts(src_dest_confs)
			destination.checkCtrlConflicts(src_dest_confs);
			service.checkCtrlConflicts(srv_confs);			
		}

		message += (!isEmpty(src_dest_confs["message"])) ? src_dest_confs["message"] : "";
		message += (!isEmpty(srv_confs["message"])) ? srv_confs["message"] : "";
		
		if (!isEmpty(message))
		{
			getE("conflicts").value = "true";
			setHtml("conflicts-list", message);
			show("conflicts-div");
		}
		else
		{
			getE("conflicts").value = "";
			setHtml("conflicts-list", "");
			hide("conflicts-div");
		}
	}
	
	this.refreshFlowsView = function()
	{
		this.checkConflicts();
		
		setHtml("list-flows", "");
		setHtml("groups-members-list", "");
		for(var i=1 ; i<=this.getNbFlow() ; i++)
		{			
			this.setIdx(i);
		
			setHtml("list-flows", getHtml("list-flows") + getHtml("flow-template").replace(/template/g, "f-" + i).replace(/idx/g, i));			
			if (isFWO())
			{
				setHtml("f-" + i + "-source", map2chain(this.source.getCtrlListOutput_fwo()), null);
				setHtml("f-" + i + "-destination", map2chain(this.destination.getCtrlListOutput_fwo()), null);
				setHtml("f-" + i + "-service", map2chain(this.service.getCtrlListOutput_fwo()), null);
			}
			else
			{
				setHtml("f-" + i + "-source", map2chain(this.source.getCtrlListOutput(), null));
				setHtml("f-" + i + "-destination", map2chain(this.destination.getCtrlListOutput(), null));
				setHtml("f-" + i + "-service", map2chain(this.service.getCtrlListOutput(), null));
			}
			setHtml("f-" + i + "-description", this.getFlowInfo("flow-description"));
			setHtml("f-" + i + "-action", this.getFlowInfo("flow-action"));		

			var flow_meta 	= meta_params2map(this.getFlowInfo("meta"));
			var rule_action = isEmpty(flow_meta["rule-action"]) ? "create" : flow_meta["rule-action"]
			
			jQuery("#" + "f-" + i + "-fwo-action").css('color', getActionColor(rule_action));
			setHtml("f-" + i + "-fwo-action", rule_action);
		}
		
		for(var i=1 ; i<=this.getNbFlow() ; i++)
		{			
			this.setIdx(i);
			var flow_meta 	= meta_params2map(this.getFlowInfo("meta"));
			var flag_id 	= "f-" + i + "-flag";
			
			try
			{
				if (isX(flow_meta['flag'] ,"1"))
					getE(flag_id).checked = true;
				else 
					getE(flag_id).checked = false;
			}
			catch(e){}
			var rules 	= "";
			var nb_rule = this.getFlowInfo("nb-rule");
			
			if (nb_rule>0)
			{
				for (var j=1; j<=nb_rule ; j++)			
					rules += getHtml("rule-refs").replace(/ridx/g, j).replace(/template/g, "f-" + i).replace(/idx/g, i);
			}
			else
			{
				rules = getHtml("rule-refs").replace(/ridx/g, 1).replace(/template/g, "f-" + i).replace(/idx/g, i);
				this.addFlowInfo("nb-rule", 1);
				this.addFlowInfo("rule-" + 1 + "-fw", default_choice_sh);
				this.addFlowInfo("rule-" + 1 + "-rule-id", "");
			}

			setHtml("f-" + i + "-rule-refs", rules);
			
			if (nb_rule>0)
			{
				for (var j=1; j<=nb_rule ; j++)
				{
					var ref_fw 	= this.getFlowInfo("rule-" + j + "-fw");
					var ref_nbr = this.getFlowInfo("rule-" + j + "-rule-id");
					setSelectE("f-" + i + "-rule-" + j + "-fw", ref_fw);
					getE("f-" + i + "-rule-" + j + "-rule-id").value = ref_nbr;	
					
					if (isX(flow_meta['from-rule'], "true") && isX(flow_meta['base-fw'], ref_fw) && isX(flow_meta['base-rule-id'], ref_nbr))
					{
						jq_hide("f-ctrl-icon-rule-" + i + "-" + j);
						jq_disabled("f-ctrl-rule-" + i + "-" + j);
					}
				}
			}
		}
		
		$('.repeat-group-row:odd').css('background-color','#CCCCCC')
	};
	
	this.add_rule = function(idx)
	{
		this.setIdx(idx);
		var new_nb_rule = this.getFlowInfo("nb-rule") + 1;
		var new_rules = getHtml("f-" + idx + "-rule-refs");
			new_rules += getHtml("rule-refs").replace(/ridx/g, new_nb_rule).replace(/template/g, "f-" + idx).replace(/idx/g, idx);		
		setHtml("f-" + idx + "-rule-refs", new_rules);
		this.addFlowInfo("rule-" + new_nb_rule + "-fw", default_choice_sh);
		this.addFlowInfo("rule-" + new_nb_rule + "-rule-id", "");
		
		for (var j=1; j<=new_nb_rule ; j++)
		{
			setSelectE("f-" + idx + "-rule-" + j + "-fw", this.getFlowInfo("rule-" + j + "-fw"));
			getE("f-" + idx + "-rule-" + j + "-rule-id").value = this.getFlowInfo("rule-" + j + "-rule-id");	
		}
		
		this.addFlowInfo("nb-rule", new_nb_rule);
	};
	
	/**
		caution lazy update ! 
		the new values of fw and rule-id are pushed in flow.elems only when user clicks on button 'Update rule refs'
	**/
	this.remove_rule = function(idx, rule_idx)
	{
		try{
		this.setIdx(idx);
		var nb_rule = this.getFlowInfo("nb-rule");
		
		if (nb_rule<=1)
			return ;	//skip when remove unique rule
	
		if (rule_idx<nb_rule)
		{			
			setSelectE("f-" + idx + "-rule-" + rule_idx + "-fw", getSelectE("f-" + idx + "-rule-" + nb_rule + "-fw").value);
			getE("f-" + idx + "-rule-" + rule_idx + "-rule-id").value = getE("f-" + idx + "-rule-" + nb_rule + "-rule-id").value;
			this.addFlowInfo("rule-" + rule_idx + "-fw", this.getFlowInfo("rule-" + nb_rule + "-fw"));
			this.addFlowInfo("rule-" + rule_idx + "-rule-id", this.getFlowInfo("rule-" + nb_rule + "-rule-id"));
		}

		/** delete last flow rule **/
		$("#rule-refs-f-" + idx + "-rule-" + nb_rule).remove();

		delete this.elems["f-" + idx + "-rule-" + nb_rule + "-fw"];
		delete this.elems["f-" + idx + "-rule-" + nb_rule + "-rule-id"];
	
		this.addFlowInfo("nb-rule", nb_rule - 1);
		}catch(e){logout(e);}
	};
	
	this.clear = function(grade)
	{
		if (grade<=1)
		{
			source.clear(1);
			destination.clear(1);
			service.clear(1);
		}
		if (grade<=2)
		{
			var flow_attrs = this.flow_info;
			for (var fa_idx in flow_attrs)
				getE(flow_attrs[fa_idx]).value = "";
			
			setSelectE("flow-action", default_choice);
			checkRadio("H2H");
			changeDataflowType();
			
			jq_hide("flow-warnings");
		}
	};
	
	this.toHtmlParams = function()
	{
		var vars = "";
		for (var key in this.elems)
		vars += (vars!="") 	? fcr_object_e_sep + key + fcr_object_kv_sep + this.elems[key] 
							: key + fcr_object_kv_sep + this.elems[key]
		return vars;
	};
	
	this.fullHtmlParams = function()
	{
		return this.toHtmlParams() + fcr_object_e_sep 
		+ this.source.toHtmlParams() + fcr_object_e_sep 
		+ this.destination.toHtmlParams() + fcr_object_e_sep 
		+ this.service.toHtmlParams();	
	};
	
	return this.initialize(src, dest, srv);
};

