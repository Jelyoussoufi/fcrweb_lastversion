<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.RuleSummary" %>
<%@ page import="be.celsius.fcrweb.objects.Rule" %>
<%@ page import="be.celsius.fcrweb.objects.RuleObjectSummary" %>
<%@ page import="be.celsius.fcrweb.utils.ExtractorUtils" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/rules.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/util_function.js"></SCRIPT>
		
		<style type="text/css">
	        body { background-color: #fafafa; }	      
	        table { background-color: #D0D0D0 ; } 
		</style>
		
		<title>Rule Detail</title>		
		
	</head>
	<body>
		
		<div id="SearchResult"></div>
		
		<%
					Rule ruleBean=(Rule)request.getAttribute("rule");
				%>
		<h2>Rule <%= ruleBean.getName() %></h2>
		<table>
			<tr>
				<th>Created</th>
				<td><%= ruleBean.getCreated() %></td>
			</tr>
			<tr>
				<th>Comment</th>
				<td><%= ruleBean.getComment() %></td>
			</tr>
			<tr>
				<th>FCR</th>
				<% if (ruleBean.getFcr() != null) 
				{%>
				<td><a href="javascript:void(0)" onClick="window.open('/fcrweb/jsp/fcr-form.jsp?fid=<%= ruleBean.getFcr() %>', '_blank', 'scrollbars=1,resizable=yes, width=1050, height=700')"><%= ruleBean.getFcr() %></a></td>
				<%}
				else
				{%>
				<td></td>
				<%}%>
			</tr>
			<tr>
				<th>Last Used</th>
				<td><%= ruleBean.getLastUsed() %></td>
			</tr>
			<tr>
				<th>End Date</th>
				<td><%= ruleBean.getEndDate() %></td>
			</tr>
		</table>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="expLink" href="javascript:void(0)" onClick="switchSimpleExpanded()">Display expanded rule</a>
		<div id="simple_rule" style="font-size:10px">
		<table  style="width:98%;word-wrap:break-word;table-layout:fixed;">
			<tr>
				<th style="width:12%;" class="first"><strong>Name</strong></th>
				<th style="width:33%;">Source</th>
				<th style="width:27%;">Destination</th>
				<th style="width:18%;">Service</th>
				<th style="width:10%;">Action</th>
			</tr>


			<%				
				RuleSummary SelectedRule = ruleBean.getSummary();
				%>
						<tr class="row-a">
						<td class="first"><%= SelectedRule.getName() %></td>
						<%
						String src_objects = ExtractorUtils.listSourceObjectToHtml(SelectedRule.getSource());
						String dst_objects = ExtractorUtils.listRuleObjectToHtml(SelectedRule.getDestination());
						String svc_objects = ExtractorUtils.listRuleServiceToHtml(SelectedRule.getService());
						%>
							<td><%= src_objects %></td>
							<td><%= dst_objects %></td>
							<td><%= svc_objects %></td>
						<td><img src='ressources/mob_icons/<%= SelectedRule.getAction().replace(" ", "").toLowerCase() %>.png'>&nbsp;<%= SelectedRule.getAction() %></td>
					</tr>

		</table>
		</div>

		<div id="expanded_rule" style="font-size:10px;display:none;">
		<table  style="width:98%;word-wrap:break-word;table-layout:fixed;">
			<tr>
				<th style="width:12%;" class="first"><strong>Name</strong></th>
				<th style="width:33%;">Source</th>
				<th style="width:27%;">Destination</th>
				<th style="width:18%;">Service</th>
				<th style="width:10%;">Action</th>
			</tr>

			<%
				RuleSummary SelectedExpandedRule = ruleBean.getExpandedSummary();
				%>
					<tr class="row-a">
						<td class="first"><%= SelectedExpandedRule.getName() %></td>
						<%
						String exp_src_objects = ExtractorUtils.listSourceObjectToHtml(SelectedExpandedRule.getSource());
						String exp_dst_objects = ExtractorUtils.listRuleObjectToHtml(SelectedExpandedRule.getDestination());
						String exp_svc_objects = ExtractorUtils.listRuleServiceToHtml(SelectedExpandedRule.getService());
						%>
							<td><%= exp_src_objects %></td>
							<td><%= exp_dst_objects %></td>
							<td><%= exp_svc_objects %></td>
						<td><img src='ressources/mob_icons/<%= SelectedExpandedRule.getAction().replace(" ", "").toLowerCase() %>.png'>&nbsp;<%= SelectedExpandedRule.getAction() %></td>
					</tr>
		</table>
		</div>

		&nbsp;&nbsp;&nbsp;<input name="change_rule" id="change_rule" class="button" value="Change Rule" type="button" onClick="updateRule(<%= SelectedRule.getId() %>, filter('<%= SelectedRule.getTechnicalService() %>'))" /><br><br>
		<div id="change_log">
		&nbsp;&nbsp;&nbsp;<input class="button" value="Compute Change Log" type="button" onClick="getChangeLog('<%= ruleBean.getName() %>')" />
		</div>

	</body>
</html>