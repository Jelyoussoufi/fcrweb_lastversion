<%@ page language="java" pageEncoding="ISO-8859-1" contentType="text/html;charset=ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/taglib/c.tld" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglib/spring.tld" prefix="spring" %>
<%@ page isELIgnored="false" %>
<%@ page import="java.lang.String" %>
<% 
	String contextPath 	= request.getContextPath();
	String path 		= contextPath;
	String basePath 	= request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort() + "/";
	String realPath 	= request.getRealPath("\\");
	String getURL 		= request.getRequestURL().toString();
	String reqURI 		= request.getRequestURI();
	String servletPath 	= request.getServletPath();
	String queryString 	= request.getQueryString();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>FCR Web - Exception</title>
		<jsp:include page="part-head.jsp"/>
		<script type="text/javascript">
			function endLoad()
			{
				setMenus("", "");
			}
		</script>		
	</head>
	<body onload="load();">
		<div id="wrap">
			<div id="header">
				<jsp:include page="part-header.jsp"/>
			</div>
			<div id="menu">
				<jsp:include page="part-menu.jsp"/>
			</div>
			<div id="nav">
				<jsp:include page="part-nav.jsp"/>
			</div>
			<div id="content-wrap">
				<jsp:include page="part-content-wrap.jsp"/>
				<div id="sidebar">
					<jsp:include page="part-sidebar.jsp"/>					
				</div>	
				<div id="main">
					<jsp:include page="part-main.jsp"/>
				</div>				
			</div>
			<div id="footer">
				<jsp:include page="part-footer.jsp"/>
			</div>
		</div>
	</body>
</html>
