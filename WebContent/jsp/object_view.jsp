<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>
<%@ page import="be.celsius.fcrweb.utils.AuthenticationUtils" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/objects.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>

		<style type="text/css">
	        body { background-color: #fafafa; }	      
	        table { background-color: #D0D0D0 ; } 
		</style>
		
		<title>Object</title>		
		
	</head>
	<body>		
				
		<%
		be.celsius.fcrweb.objects.Object objectBean=(be.celsius.fcrweb.objects.Object)request.getAttribute("object");
		String user_profiles = (String)request.getAttribute("user_profile");
		%>
		<h2>Object <%= objectBean.getName() %></h2>
		<table>
			<tr>
				<th>Type</th>
				<td><%= objectBean.getType() %></td>
			</tr>
			<%
				ArrayList<String[]> temp = objectBean.getOtherParameters();
				for (int i = 0; i < temp.size(); i++)
				{
					String[] param = temp.get(i);
					
					%>
					<tr>
						<th><%= param[0] %></th>
						<td><%= param[1] %></td>
					</tr>
					<%					
				}
			%>
			<tr>
				<th>Used in Groups</th>
				<td><%= objectBean.getUsedInGroups() %></td>
			</tr>
			<tr>
				<th>Used in Rules</th>
				<td><%= objectBean.getUsedInRules() %></td>
			</tr>		
			<%
			if (objectBean.getType().equals("group") && AuthenticationUtils.hasAdministratorProfile(user_profiles))
			{
				%>
				<tr>
					<th>Global</th>
					<%
					if (objectBean.isPredefined())
					{
						%>
						<td><input type="checkbox" checked="checked" disabled="disabled" id="predefined"/></td>
						<%
					}
					else
					{
						%>
						<td><input type="checkbox" disabled="disabled" id="predefined"/>&nbsp;&nbsp;<input id="update_predefined" class="button" value="set Global" type="button" onClick="setObjectGroupAsPredefined(<%= objectBean.getId() %>)" /></td>
						<%
					}
					%>
				</tr>
				<%
			}
			%>
		</table>
	</body>
</html>