<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>
<%@ page import="be.celsius.fcrweb.objects.ObjectInconsistencies" %>
<%@ page import="be.celsius.fcrweb.objects.ObjectInconsistency" %>
<%@ page import="be.celsius.fcrweb.objects.ServiceInconsistencies" %>
<%@ page import="be.celsius.fcrweb.objects.ServiceInconsistency" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>
		
		<title>FCRWeb - Reports</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="reports.htm">Reports</a> -> Firewall objects consistency</h4>
		<h2>Firewall Objects consistency</h2>
		
		<%
		ArrayList<ObjectInconsistencies> object_inconsistencies =(ArrayList<ObjectInconsistencies>)request.getAttribute("object_consistency");
		ArrayList<ServiceInconsistencies> service_inconsistencies =(ArrayList<ServiceInconsistencies>)request.getAttribute("service_consistency");
		int objects_total =(Integer)request.getAttribute("objects_total");
		int service_total =(Integer)request.getAttribute("services_total");
		%>
		
			<h3>Network Object Inconsistencies: <%= object_inconsistencies.size() %> on <%= objects_total %> <a id="network_objects" href="javascript:void(0)" onClick="displayNotCompliant('network_objects')">(show)</a></h3>
			<div id="network_objects_div" style="display:none;">
			
			<% 
			if (object_inconsistencies.size() ==0)
			{
				%>
				<p>There are no inconsistent objects</p>	
				<% 
			}
			else
			{
				%>
				<table>
				<tr>
					<th>Object Name</th>
					<th>Type</th>
					<th>IP</th>
					<th>Netmask</th>
					<th>FireWalls</th>
				</tr>	
				<% 
				int i = 0;
				for (ObjectInconsistencies inconsistencies:object_inconsistencies)
				{
					Boolean first = true;
					if (i%2 == 0)
					{
						%>
							<tr class="row-a">
						<%
					}
					else
					{
						%>
							<tr class="row-b">
						<%
					}
					%>
						<td rowspan="<%= inconsistencies.getInconsistencies().size() %>"><%= inconsistencies.getObjectName() %></td>
					<%
					for (ObjectInconsistency inconsistency: inconsistencies.getInconsistencies())
					{
						if (!first)
						{
							if (i%2 == 0)
							{
								%>
									<tr class="row-a">
								<%
							}
							else
							{
								%>
									<tr class="row-b">
								<%
							}
						}
						%>							
							<td><a href="javascript:void(0)" onClick="window.open('rules.htm?object_id=<%= inconsistency.getId() %>', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')"><%= inconsistency.getType() %></a></td>
							<td><%= inconsistency.getIp() %></td>
							<td><%= inconsistency.getNetmask() %></td>
							<td><%= inconsistency.getFirewalls() %></td>
						</tr>
						<%
						first = false;
					}
					i=i+1;
				}
				
				%>
				</table>
				<%
			}
			 %>
			</div>
			
			<h3>Service Inconsistencies: <%= service_inconsistencies.size() %> on <%= service_total %> <a id="services" href="javascript:void(0)" onClick="displayNotCompliant('services')">(show)</a></h3>
			<div id="services_div" style="display:none;">
			
			<% 
			if (service_inconsistencies.size() ==0)
			{
				%>
				<p>There are no inconsistent services</p>	
				<% 
			}
			else
			{
				%>
				<table>
				<tr>
					<th>Service Name</th>
					<th>Type</th>
					<th>Ports</th>
					<th>FireWalls</th>
				</tr>	
				<% 
				int i = 0;
				for (ServiceInconsistencies inconsistencies:service_inconsistencies)
				{
					Boolean first = true;
					if (i%2 == 0)
					{
						%>
							<tr class="row-a">
						<%
					}
					else
					{
						%>
							<tr class="row-b">
						<%
					}
					%>
						<td rowspan="<%= inconsistencies.getInconsistencies().size() %>"><%= inconsistencies.getServiceName() %></td>
					<%
					for (ServiceInconsistency inconsistency: inconsistencies.getInconsistencies())
					{
						if (!first)
						{
							if (i%2 == 0)
							{
								%>
									<tr class="row-a">
								<%
							}
							else
							{
								%>
									<tr class="row-b">
								<%
							}
						}
						%>
							<td><a href="javascript:void(0)" onClick="window.open('rules.htm?service_id=<%= inconsistency.getId() %>', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')"><%= inconsistency.getType() %></a></td>
							<td><%= inconsistency.getPorts() %></td>
							<td><%= inconsistency.getFirewalls() %></td>
						</tr>
						<%
						first = false;
					}
					i=i+1;
				}
				
				%>
				</table>
				<%
			}
			 %>
			</div>
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>