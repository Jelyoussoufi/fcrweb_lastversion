<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.ObjectSummary" %>
<%@ page import="be.celsius.fcrweb.objects.ObjectsPageData" %>
<%@ page import="be.celsius.fcrweb.objects.Task" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/objects.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/util_function.js"></SCRIPT>

		
		<title>FCRWeb - Network Objects</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/Objects_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> Technical Service -> Object Groups</h4>
		<div id="SearchResult"></div>
		<h2>Global Object Groups</h2>
		
		<%
				String user_profiles = (String) request.getAttribute("user_profile");
				ObjectsPageData objectsBean=(ObjectsPageData)request.getAttribute("objects");				
				ArrayList<ObjectSummary> predefinedObjects = objectsBean.getPredefinedObjects();
				int i = 0;
				
				for (ObjectSummary predefinedObject:predefinedObjects)
				{	
					if (i == 0)
					{
					%>						
					<table >
					<tr>
						<th class="first"><strong>Name</strong></th>
						<th>Description</th>
					</tr>
					<%
					}
					if (i%2 == 0)
					{
						%>
						<tr class="row-a">
						<%
					}
					else
					{
						%>
						<tr class="row-b">
						<%
					}
					
					%>									
						<td ><a href="javascript:void(0)" onClick="window.open('objects.htm?id=<%= predefinedObject.getId() %>&type=group', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= predefinedObject.getName() %></a></td>
						<td><%= predefinedObject.getDescription() %></td>
					</tr>
					<%
					i += 1;					
				}
				
				if (i == 0)
				{
					%>
					No Global Object Groups to display.
					<%
				}
				else
				{
					%>
					</table>
					<%
				}
			%>	
		
		<h2>Local Object Groups</h2>
	
			<%				
				ArrayList<ObjectSummary> myObjects = objectsBean.getMyObjects();
				int list_id = 0;
				i = 0;
				Boolean hasResult = false;
				String oldTS = "";
				%>
				<script>var listTab = [];</script>
				<%
				for (ObjectSummary myObject:myObjects)
				{
					if (myObject.getTechnicalService().equals(oldTS))
					{
						if (i%2 == 1)
						{
							%>
							<tr class="row-a">
							<%
						}
						else
						{
							%>
							<tr class="row-b">
							<%
						}
						%>
							<td class="first"><input type="checkbox" onclick="objectChecked(<%= myObject.getId() %>, <%= list_id %>);"/></td>						
							<td><a href="javascript:void(0)" onClick="window.open('objects.htm?id=<%= myObject.getId() %>&type=group', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= myObject.getName() %></a></td>
							<td><%= myObject.getDescription() %></td>
						</tr>
	
						<%
						i=i+1;						
					}
					else
					{
						if (!oldTS.equals(""))
						{
							%>
							</table>
							<%
						}
						list_id = list_id +1;
						oldTS = myObject.getTechnicalService();
						%>
						<h3><%= oldTS %></h3>
						<input id="button<%= list_id %>" style="display: none;" class="button" value="Edit Object(s)" type="button" onClick="editSelectedObjects(<%= list_id %>, '<%= oldTS %>')" />
						
						<script>
							listTab[<%= list_id %>] = [];
						</script>
						
						<table >
						<tr>
							<th class="first"></th>
							<th><strong>Name</strong></th>
							<th>Description</th>
						</tr>
						<%
						if (i%2 == 0)
						{
							%>
							<tr class="row-a">
							<%
						}
						else
						{
							%>
							<tr class="row-b">
							<%
						}
						
						%>		
							<td class="first"><input type="checkbox" onclick="objectChecked(<%= myObject.getId() %>, <%= list_id %>);"></td>
							<td ><a href="javascript:void(0)" onClick="window.open('objects.htm?id=<%= myObject.getId() %>&type=group', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= myObject.getName() %></a></td>
							<td><%= myObject.getDescription() %></td>
						</tr>
						<%
						i = 0;
						hasResult = true;
					}
				}
				
				if (!hasResult)
				{
					%>
					No Local Object Groups to display.
					<%
				}
				else
				{
					%>
					</table>
					<%
				}
				
			%>			
				
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>