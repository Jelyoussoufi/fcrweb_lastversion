
<%@ page import="be.celsius.fcrweb.utils.AuthenticationUtils" %>

<div id="wrap">  <!-- THIS DIV TAG IS CLOSED IN THE FOOTER.JSP FILE -->	
	<img src="ressources/content.jpg" class="bgcontent">
	<!--header -->
	<div id="header">			
			
		<img src="ressources/header.jpg" class="bgheader">
		<h1 id="logo-text"><a href="index.htm">FCR Web</a></h1>

		<div id="header-links">
			<p>
				<a href="help.htm">| Help |</a>
			</p>
		</div>						
	</div>
	
	<!-- menu -->	
	<div  id="menu">
	<img src="ressources/menu.jpg" class="bgmenu">
		
		<ul id="nav" class="nav">
		
		<%
			String profile=(String)request.getAttribute("user_profile");
			String current=(String)request.getAttribute("page");
			String uid=(String)request.getAttribute("uid");
						
		%>

			<li <% if (current == "home" || current == null) %>id="current"<% %>><a href="index.htm">Home</a></li>
			<li <% if (current == "fcr") %>id="current"<% %>><a href="fcr.htm">Firewall CR</a></li>
			<li <% if (current == "technical_service") %>id="current"<% %>><a href="#">Technical Service</a>
				<ul>
                    <li <% if (current == "rules") %>id="current"<% %>><a href="rules.htm">Rules</a></li>
					<li <% if (current == "objects") %>id="current"<% %>><a href="objects.htm">Objects Groups</a></li>
					<li <% if (current == "services") %>id="current"<% %>><a href="services.htm">Services</a></li>
					<li <% if (current == "profiles") %>id="current"<% %>><a href="profiles.htm">User Groups</a></li>
                </ul>
              </li>
            <%
				if (AuthenticationUtils.hasAdministratorProfile(profile) || AuthenticationUtils.hasSecurityOfficerProfile(profile))
				{
			%>
				<li <% if (current == "reports") %>id="current"<% %>><a href="reports.htm">Reports</a></li>
			<%
				}
	            if (AuthenticationUtils.hasAdministratorProfile(profile))
				{
			%>
				<li <% if (current == "admin") %>id="current"<% %> class="last"><a href="admin.htm">Admin</a></li>	
			<%
				}
            %>
		</ul>
		<!-- <script>var user_id = "<%= uid %>";</script> -->
	</div>	
	