<%@ page language="java" pageEncoding="ISO-8859-1" contentType="text/html;charset=ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/taglib/c.tld" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglib/spring.tld" prefix="spring" %>
<%@ page isELIgnored="false" %>
<%@ page import="java.lang.String" %>
<% 
	String contextPath 	= request.getContextPath();
	String path 		= contextPath;
	String basePath 	= request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort() ; // + "/";
	String realPath 	= request.getRealPath("\\");
	String getURL 		= request.getRequestURL().toString();
	String reqURI 		= request.getRequestURI();
	String servletPath 	= request.getServletPath();
	String queryString 	= request.getQueryString();
	String userAgent 	= request.getHeader("user-agent");
	String isFox		= "false";
	if (userAgent.toLowerCase().contains("firefox"))
		isFox = "true";
		
	String form_type = "maintain";
%>
<jsp:useBean id="environment" class="be.celsius.util.session.Environment" />

<html id="root" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<%@ include file="/jsp/fcr-admin-form.jsp" %>		

							<a name="advanced_edition"></a>
							<h3>Technical information</h3>

							<div id="users-contain">
								<div style="width:100%;height:230px;overflow:auto;">
									<table id="users" class="repeat-group">
										<thead>
											<tr>
												<th width="30%">Action</th>
												<th width="30%">Group Name</th>
												<th width="30%">New Name / Member(s)</th>
												<th width="5%" class="control-display-editable"></th>
												<th width="3%" class="control-display-readonly"></th>
											</tr>
										</thead>
										<tbody id="list-maintain-groups"></tbody>
									</table>
								</div>
								<table style="display:none">
									<tbody id="flow-template">
										<tr id="row-template" class="maintain-groups-row">
											<td><label id="template-action" class="control control-select tiny" ></label></td>
											<td><label id="template-name" class="control control-select tiny" ></label></td>
											<td><label id="template-list" class="control control-select tiny" ></label></td>											
											<td class="control-display-editable" style="margin:0;padding:0;border:0;">
												<table>
													<tr>
														<td style="margin:0;padding:0;border:0;text-align:center;">
															<img 	src="<%= request.getContextPath()%>/ressources/document_edit.png"
																	class="img img-clickable control-display-editable" 
																	style="display:inline;" title="Edit group" onclick="mtn_groups.load(idx, 'edit')" />
														</td>
														<td style="margin:0;padding:0;border:0;text-align:center;">
															<img src="<%= request.getContextPath()%>/ressources/document_delete.png" 
																class="img img-clickable control-display-editable" 
																style="display:inline;" title="Delete group" 
																onclick="mtn_groups.remove(idx)" />
														</td>
													</tr>	
												</table>
											</td>
											<td class="control-display-readonly">
												<img src="<%= request.getContextPath()%>/ressources/document_view.png" 
													class="img img-clickable control-display-readonly" 
													style="display:inline;" title="Visualize flow" onclick="mtn_groups.load(idx, 'display')" />												
											</td>
											
										</tr>
									</tbody>
								</table>
								<table id="maintain-groups-conflicts-div" class="group-component" style="display:none">
										<tr>
											<th>Group(s) conflict(s)</th>
										</tr>
									</thead>
									<tbody id="maintain-groups-conflicts-list"></tbody>
								</table>
								<table class="group-component">	
									<tr id="warning-error-tr-maintain-groups" class="warning-error" style="display:none">
										<td colspan="2">No group(s) detected</td>
									</tr>
									<tr id="advanced-control-buttons"  class="control-display-editable">
										<td>
											<input type="button" id="add-group-button" class="button control-button normal-button preapproved-unauthorized" value="Add Group" onclick="mtn_groups.create();">
										</td>
									</tr>
								</table>
							</div>
							<table class="group-component group-button">
								<tr id="edition-control-buttons" class="control-display-editable">
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="Submit" onclick="button_submitForm_mtn();">
										<input type="button" class="button control-button small-button" value="Drop" onclick="button_dropForm_mtn();">
										<input type="button" class="button control-button small-button" value="Save" onclick="button_saveForm_mtn();">
									</td>
								</tr>
								<tr id="read-control-buttons" class="control-display-readonly">
									<td colspan=2>
										<input style="display:none" type="button" class="button control-button small-button" value="Close" onclick="button_closeForm_mtn()">
									</td>
								</tr>
							</table>
						</div>
						<div id="group-members" class="control-dialog control-list-member" style="display:none">
							<h2 id="group-members-handle">members of <span id="group-members-header"></span></h2>
							<br/>
							<table class="group-component" style="width:90%;">
								<tbody id="group-members-list"></tbody>
							</table>
							<table class="group-component group-button">
								<tr>
									<td>
										<input type="button" class="button control-button small-button" style="float:right;" value="Close" onclick="hideGroupMembers()">
									</td>
								</tr>
							</table>
						</div>
					
						
					</form>
					
					<form id="mtn-form">
					<div id="goo-dialog" class="control-dialog" style="display:none">
						<h3 id="goo-dialog-handle"><span id="goo-dialog-header"></span> group of objects</h3>
						<table id="goo-definition" class="group-component" style="width:95%">
							<tr>
								<th>Organization<font color="red">&nbsp;*</font></th>
								<td>
									<input id="goo-org-ro" class="goo-org-ro control-select-readonly control-text" disabled style="display:none;"/>
									<select id="goo-org" class="goo-org control control-select control-editable preapproved-fields" onchange="mtn_groups.changeOrg();">
										<option value="">[Select ...]</option>
										<option value="MOBISTAR">MOBISTAR</option>
										<option value="OLU">OLU</option>
										<option value="MES">MES</option>
										<option value="OTHER">OTHER</option>
									</select>
									<img id="help-goo-org" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
									<!--[if IE]><span style="width:100px;"><span><![endif]-->
								</td>
							</tr>
							<tr id="warning-error-tr-goo-org" class="warning-error goo-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>
							<tr id="row-goo-name" class="goo-rows">
								<th>Group of objects<font color="red">&nbsp;*</font></th>
								<td>
									<input type="text" id="goo-name" class="control control-text control-editable flow-control-action preapproved-fields">
									<img id="help-goo-name" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
									<img src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-goo" style="display:none;" title="this group of objects will be created"/>
									<img id="goo-show-list-member" src="<%= request.getContextPath()%>/ressources/document_view.png" onclick="showGroupMembers('goo-name');"class="img" style="display:none;" title="display this group members"/>									
								</td>
							</tr>	
							<tr class="goo-rows">
								<th>Environment<font color="red">&nbsp;*</font></th>
								<td>
									<input id="goo-env-ro" class="goo-env-ro control-select-readonly control-text" disabled style="display:none;"/>
									<select id="goo-env" class="control control-select control-editable src-autocomplete-rows preapproved-fields" onchange="mtn_groups.changeEnv('goo-env');">
										<option value="" selected>[Select ...]</option>
										<option value="Prod" selected>Production</option>
										<option value="nonProd">Non production</option>
									</select>
									<img id="help-goo-env" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>	
								</td>
							</tr>
							<tr id="warning-error-tr-goo-env" class="warning-error" style="display:none">
								<td id="warning-error-td-goo-env" colspan="2">Missing value</td>
							</tr>							
							<tr id="warning-error-tr-goo-name" class="warning-error goo-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>
							<tr class="row-goo-action-field row-goo-rename">
								<th>Renaming<font color="red">&nbsp;*</font></th>
								<td><input type="text" id="goo-new-name" class="control control-text control-editable flow-control-action"></td>
							</tr>									
							<tr id="warning-error-tr-goo-new-name" class="warning-error goo-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>
							<tr id="row-goo-action" class="row-goo-action">
								<th>Action<font color="red">&nbsp;*</font></th>
								<td>
									<table class="control-radio-group">
										<tr id="row-goo-create" class="row-goo-action row-goo-create preapproved-unauthorized">
											<td>
												<!--[if IE]><span style="display:inline;height:10px;vertical-align:middle;"><![endif]-->
												<input 	id="goo-action-create" 
														class="goo-action control-radio control-editable preapproved-fields"
														name="goo-action"
														type="radio"   
														value="create" 
														onclick="mtn_groups.change_goo_action();"/>&nbsp;
												<label class="control-radio-label" for="goo-action-create">Create group</label>
												<!--[if IE]></span><![endif]-->
											</td>
										</tr>
										<tr id="row-goo-rename" class="row-goo-action row-goo-rename preapproved-unauthorized">
											<td>
												<!--[if IE]><span style="display:inline;height:10px;vertical-align:middle;"><![endif]-->
												<input 	id="goo-action-rename" 
														class="goo-action control-radio control-editable preapproved-fields"
														name="goo-action"
														type="radio"   
														value="rename" 
														onclick="mtn_groups.change_goo_action();"/>&nbsp;
												<label class="control-radio-label" for="goo-action-rename">Update group</label>
												<!--[if IE]></span><![endif]-->
											</td>
										</tr>
										<tr id="row-goo-update" class="row-goo-action row-goo-update">
											<td>
												<!--[if IE]><span style="display:inline;height:10px;vertical-align:middle;"><![endif]-->
												<input 	id="goo-action-update" 
														class="goo-action control-radio control-editable preapproved-fields" 
														name="goo-action" 
														type="radio"
														value="update" 
														onclick="mtn_groups.change_goo_action();"/>&nbsp;
												<label class="control-radio-label" for="goo-action-update">Add / Delete object(s)</label>
												<!--[if IE]></span><![endif]-->
											</td>
										</tr>
										<tr id="row-goo-delete" class="row-goo-action row-goo-delete preapproved-unauthorized">
											<td>
												<!--[if IE]><span style="display:inline;height:10px;vertical-align:middle;"><![endif]-->
												<input 	id="goo-action-delete"
														class="goo-action control-radio control-editable preapproved-fields" 
														name="goo-action" 
														type="radio"
														value="delete" 
														onclick="mtn_groups.change_goo_action();" />&nbsp;
												<label class="control-radio-label" for="goo-action-delete">Delete group</label>
												<!--[if IE]></span><![endif]-->
											</td>
										</tr>
										<tr style="display:none">
											<td>
												<input id="goo-action-modify" disabled name="goo-action" type="radio" value="modify" />
												<label class="control-radio-label" for="goo-action-modify">Modify</label>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr id="warning-error-tr-goo-action" class="warning-error goo-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>					
							<tr>
								<td colspan="2" class="straight-box">
									<div class="goo-group-element-wrapper group-element-scrollable" style="max-height:275px;overflow-y:auto;overflow:auto">
										<table class="straight-box">
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th colspan="2" style="text-align:center;">Host(s) selection</th>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th>Host name</th>
												<td>
													<input type="text" id="goo-host" class="control control-text control-editable fixed-autosuggest flow-control-action">
													<img id="help-goo-host" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
													<img id="create-goo-host" src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-host" style="display:none;" title="this host will be created"/>										
												</td>
											</tr>
											<tr id="warning-error-tr-goo-host" class="warning-error goo-warning" style="display:none">
												<td id="warning-error-td-goo-host" colspan="2">Missing value or incorrect value</td>
											</tr>
											<tr id="warning-error-tr-goo-host-opt" class="warning-error warning-error-tr-goo-host" style="display:none">
												<td id="warning-error-td-goo-host-opt" colspan="2"></td>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th>Environment</th>
												<td>
													<input id="goo-host-env-ro" class="goo-host-env-ro control-select-readonly control-text" disabled style="display:none;"/>
													<select id="goo-host-env" class="control control-select control-editable goo-autocomplete-rows goo-host-autocomplete-rows" onchange="mtn_groups.changeEnv('host-env');">
														<option value="" selected>[Select ...]</option>
														<option value="Prod" selected>Production</option>
														<option value="nonProd">Non production</option>
													</select>
													<img id="help-goo-host-env" src="<%= request.getContextPath()%>/ressources/information.png"
													class="img control-help" style="display:none;" title=""/>	
												</td>
											</tr>
											<tr id="warning-error-tr-goo-host-env" class="warning-error" style="display:none">
												<td id="warning-error-td-goo-host-env" colspan="2">Missing value</td>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th>Ip address</th>
												<td>
													<input type="text" id="goo-ip" class="control control-text control-editable flow-control-action">																						
													<img id="help-goo-ip" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
													<img id="goo-ip-load" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/loader.gif" title="load" />
													<img id="goo-ip-check" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/check_16px.png" title="check" />
													<img id="goo-ip-error" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/error_16px.png" title="error" />
													<img id="goo-ip-warning" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/icon-warning.gif" title="warning" />
													<input id="goo-ip-code" type="hidden" value="" />
												</td>					
											</tr>
											<tr id="warning-error-tr-goo-ip" class="warning-error goo-warning" style="display:none">
												<td id="warning-error-td-goo-ip" colspan="2">Missing or incorrect value</td>
											</tr>
											<tr id="warning-error-tr-goo-ip-opt" class="warning-error goo-warning" style="display:none">
												<td id="warning-error-td-goo-ip-opt" colspan="2">Missing or incorrect value</td>
											</tr>
											<tr class="row-goo-action-field goo-autocomplete-rows row-goo-create row-goo-update">
												<th>Translated ip</th>
												<td>
													<input type="text" id="goo-ip-tsl" class="control control-text control-editable goo-autocomplete-rows goo-host-autocomplete-rows">
													<img id="help-goo-ip-tsl" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
												</td>
											</tr>
											<tr id="warning-error-tr-goo-ip-tsl" class="warning-error goo-warning" style="display:none">
												<td colspan="2">Missing value or incorrect ip value</td>
											</tr>
											<tr id="tr-goo-host-area" class="row-goo-action-field row-goo-create row-goo-update">
												<th>Area</th>
												<td>
													<input type="text" id="goo-host-area" class="control control-text control-editable goo-autocomplete-rows goo-host-autocomplete-rows">											
												</td>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th>Hosts<font color="yellow">&nbsp;*</font></th>
												<td>
													<select id="goo-hosts" multiple size="3" class="goo-hosts control control-select control-editable" onchange="mtn_groups.display_selection_action_button('host');" ></select>
													<span class="control-display-editable multiple-select-linked-button" >
														<input type="button" class="button control-button small-button" value="Add" onclick="mtn_groups.sel_add('host');" >
														<input id="goo-host-drop-rule-button" type="button" class="button control-button small-button drop-rule-button" value="Delete" onclick="mtn_groups.sel_rem('host');" style="display:none">
													</span>
												</td>
											</tr>
											<tr class="warning-error goo-warning warning-error-tr-goo-selections warning-error-tr-goo-hosts warning-error-tr-goo-subnets warning-error-tr-goo-goos" style="display:none">
												<td colspan="2">No host(s) selected nor created</td>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th colspan="2" style="text-align:center;">Subnet(s) selection</th>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th>Subnet</th>
												<td>
													<input type="text" id="goo-subnet" class="control control-text control-editable flow-control-action">
													<img id="help-goo-subnet" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
													<img id="create-goo-subnet" src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-subnet" style="display:none;" title="this subnet will be created"/>
													<img id="goo-subnet-load" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/loader.gif" title="load" />
													<img id="goo-subnet-check" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/check_16px.png" title="check" />
													<img id="goo-subnet-error" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/error_16px.png" title="error" />
													<img id="goo-subnet-warning" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/icon-warning.gif" title="warning" />
													<input id="goo-subnet-code" type="hidden" value="" />
												</td>
											</tr>
											<tr id="warning-error-tr-goo-subnet" class="warning-error goo-warning" style="display:none">
												<td colspan="2">Missing value or incorrect value ([subnet-ip]/[mask] e.g: 0.0.0.0/0)</td>
											</tr>
											<tr id="warning-error-tr-goo-subnet-opt" class="warning-error goo-warning warning-error-tr-goo-subnet" style="display:none">
												<td colspan="2"></td>
											</tr>							
											<tr id="tr-goo-subnet-area" class="row-goo-action-field row-goo-create row-goo-update">
												<th>Area</th><td>
													<input type="text" id="goo-subnet-area" class="control control-text control-readonly goo-autocomplete-rows goo-subnet-autocomplete-rows flow-control-action">
												</td>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th>Subnets<font color="yellow">&nbsp;*</font></th>
												<td>
													<select id="goo-subnets" multiple size="3" class="goo-subnets control control-select control-editable" onchange="mtn_groups.display_selection_action_button('subnet');"></select>
													<span class="control-display-editable multiple-select-linked-button" >
														<input type="button" class="button control-button small-button" value="Add" onclick="mtn_groups.sel_add('subnet');" >
														<input id="goo-subnet-drop-rule-button" type="button" class="button control-button small-button drop-rule-button" value="Delete" onclick="mtn_groups.sel_rem('subnet');" style="display:none" >
													</span>
												</td>
											</tr>
											<tr class="warning-error goo-warning warning-error-tr-goo-selections warning-error-tr-goo-hosts warning-error-tr-goo-subnets warning-error-tr-goo-goos" style="display:none">
												<td colspan="2">No subnet(s) selected nor created</td>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th colspan="2" style="text-align:center;">Group(s) of objects selection</th>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th>Group of objects</th>
												<td>
													<input type="text" id="goo-selector" class="control control-text control-editable flow-control-action">
													<img id="help-goo-sel" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
												</td>
											</tr>									
											<tr id="warning-error-tr-goo-selector" class="warning-error goo-warning" style="display:none">
												<td colspan="2">Incorrect value</td>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th>Environment</th>
												<td>
													<select id="goo-sel-env" class="control control-select control-readonly" onchange="source.changeEnv('goo-sel-env');">
														<option value="" selected>[...]</option>
														<option value="Prod" selected>Production</option>
														<option value="nonProd">Non production</option>
													</select>
													<img id="help-goo-sel-env" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
												</td>
											</tr>
											<tr id="warning-error-tr-goo-sel-env" class="warning-error" style="display:none">
												<td id="warning-error-td-goo-sel-env" colspan="2">Missing value</td>
											</tr>
											<tr class="row-goo-action-field row-goo-create row-goo-update">
												<th>Groups of objects<font color="yellow">&nbsp;*</font></th>
												<td>
													<select id="goo-goos" multiple size="3" class="goo-goos control control-select control-editable" onchange="mtn_groups.display_selection_action_button('selector');"></select>
													<span class="control-display-editable multiple-select-linked-button" >
														<input type="button" class="button control-button small-button" value="Add" onclick="mtn_groups.sel_add('goo-selector');" >
														<input id="goo-selector-drop-rule-button" type="button" class="button control-button small-button drop-rule-button" value="Delete" onclick="mtn_groups.sel_rem('goo-selector');" style="display:none;" >
													</span>
												</td>
											</tr>							
											<tr class="warning-error goo-warning warning-error-tr-goo-selections warning-error-tr-goo-hosts warning-error-tr-goo-subnets warning-error-tr-goo-goos" style="display:none">
												<td colspan="2">No object(s) selected nor created</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>							
						</table>
						<table class="group-component group-button">
							<tr class="control-display-editable">
								<td>
									<input type="button" class="button control-button small-button add-button" value="Add" onclick="mtn_groups.add();">	
									<input type="button" class="button control-button small-button edit-button" value="Edit" onclick="mtn_groups.edit();">	
									<input type="button" class="button control-button small-button" value="Clear" onclick="mtn_groups.clear(1)">	
									<input type="button" class="button control-button small-button" value="Cancel" onclick="mtn_groups.cancel();">
								</td>
							</tr>
							<tr class="control-display-readonly">
								<td>
									<input type="button" class="button control-button small-button" style="float:right;" value="Close" onclick="mtn_groups.cancel();">
								</td>
							</tr>
						</table>
					</div>
					<div id="gsrv-dialog" class="control-dialog" style="display:none">
						<h3 id="gsrv-dialog-handle"><span id="gsrv-dialog-header"></span> group of services</h3>
						<table id="gsrv-definition" class="group-component">
							<tr>
								<th>Group of services<font color="red">&nbsp;*</font></th>
								<td>
									<input type="text" id="gsrv-name" class="control control-text control-editable flow-control-action">
									<img id="help-gsrv-name" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
									<img src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-gsrv" style="display:none;" title="this group of services will be created"/>
									<img id="gsrv-show-list-member" src="<%= request.getContextPath()%>/ressources/document_view.png" onclick="showGroupMembers('gsrv-name');" class="img" style="display:none;" title="display this group members"/>										
								</td>
							</tr>
							<tr id="warning-error-tr-gsrv-name" class="warning-error gsrv-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>
							<tr class="row-gsrv-action-field row-gsrv-rename">
								<th>Renaming<font color="red">&nbsp;*</font></th>
								<td>
									<input type="text" id="gsrv-new-name" class="control control-text control-editable flow-control-action">
									<img id="help-gsrv-new-name" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
								</td>
							</tr>
							<tr id="warning-error-tr-gsrv-new-name" class="warning-error gsrv-warning" style="display:none">
								<td colspan="2">Service name already exists</td>
							</tr>
							<tr id="row-gsrv-action" class="row-gsrv-action">
								<th>Action<font color="red">&nbsp;*</font></th>
								<td>
									<table class="control-radio-group">
										<tr id="row-gsrv-create" class="row-gsrv-action row-gsrv-action row-gsrv-create">
											<td>
												<!--[if IE]><span style="display:inline;height:10px;vertical-align:middle;"><![endif]-->
												<input 	id="gsrv-action-create" 
														class="gsrv-action control-radio control-editable"
														name="gsrv-action"
														type="radio"   
														value="create" 
														onclick="mtn_groups.change_gsrv_action();"/>&nbsp;
												<label class="control-radio-label" for="gsrv-action-create">Create group</label>
												<!--[if IE]></span><![endif]-->
											</td>
										</tr>
										<tr id="row-gsrv-rename" class="row-gsrv-action row-gsrv-action row-gsrv-rename">
											<td>
												<!--[if IE]><span style="display:inline;height:10px;vertical-align:middle;"><![endif]-->
												<input 	id="gsrv-action-rename" 
														class="gsrv-action control-radio control-editable" 
														name="gsrv-action" 
														type="radio"
														value="rename" 
														onclick="mtn_groups.change_gsrv_action();"/>&nbsp;
												<label class="control-radio-label" for="gsrv-action-rename">Update group</label>
												<!--[if IE]></span><![endif]-->
											</td>
										</tr>
										<tr id="row-gsrv-update" class="row-gsrv-action row-gsrv-action row-gsrv-update">
											<td>
												<!--[if IE]><span style="display:inline;height:10px;vertical-align:middle;"><![endif]-->
												<input 	id="gsrv-action-update" 
														class="gsrv-action control-radio control-editable" 
														name="gsrv-action" 
														type="radio"
														value="update" 
														onclick="mtn_groups.change_gsrv_action();"/>&nbsp;
												<label class="control-radio-label" for="gsrv-action-update">Add / Delete service(s)</label>
												<!--[if IE]></span><![endif]-->
											</td>
										</tr>
										<tr id="row-gsrv-delete" class="row-gsrv-action row-gsrv-delete">
											<td>
												<!--[if IE]><span style="display:inline;height:10px;vertical-align:middle;"><![endif]-->
												<input 	id="gsrv-action-delete"
														class="gsrv-action control-radio control-editable" 
														name="gsrv-action" 
														type="radio"
														value="delete" 
														onclick="mtn_groups.change_gsrv_action();" />&nbsp;
												<label class="control-radio-label" for="gsrv-action-delete">Delete group</label>
												<!--[if IE]></span><![endif]-->
											</td>
										</tr>
										<tr style="display:none">
											<td>
												<input id="gsrv-action-modify" disabled name="gsrv-action" type="radio" value="modify" />
												<label <label class="control-radio-label" for="gsrv-action-modify">>Modify</label>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr id="warning-error-tr-gsrv-action" class="warning-error gsrv-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>
							<tr class="row-gsrv-action-field row-gsrv-create row-gsrv-update">
								<th colspan=2 style="text-align:center;">Service(s) selection</th>
							</tr>
							<tr class="row-gsrv-action-field row-gsrv-create row-gsrv-update">
								<th>Name</th>
								<td>
									<input type="text" id="gsrv-srv-name" class="control control-text control-editable flow-control-action">
									<img id="help-gsrv-srv-name" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
									<img src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-srv" style="display:none;" title="this group of services will be created"/>										
								</td>
							</tr>
							<tr id="warning-error-tr-gsrv-srv-name" class="warning-error gsrv-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>
							<tr class="row-gsrv-action-field row-gsrv-create row-gsrv-update">
								<th>Protocol</th>
								<td>
									<select id="gsrv-srv-proto" class="control control-text control-editable gsrv-autocomplete-rows" onchange="mtn_groups.changeProto();">
												<option value="">[Select ...]</option>
												<option value="udp">UDP</option>
												<option value="tcp">TCP</option>
												<option value="icmp">ICMP</option>
												<option value="other ip">Other IP</option>
											</select>
									<img id="help-gsrv-name" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
								</td>
							</tr>
							<tr id="warning-error-tr-gsrv-srv-proto" class="warning-error gsrv-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>
							<tr style="display:none;">
								<th>Type</th>
								<td><input type="text" id="gsrv-srv-type" class="control control-text control-editable gsrv-autocomplete-rows"></td>
							</tr>
							<tr id="warning-error-tr-gsrv-srv-type" class="warning-error gsrv-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>
							<tr id="row-gsrv-srv-port" class="row-gsrv-action-field row-gsrv-create row-gsrv-update">
								<th>Port</th>
								<td>
									<input type="text" id="gsrv-srv-port" class="control control-text control-editable gsrv-autocomplete-rows">
									<img id="help-gsrv-port" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
								</td>
							</tr>
							<tr id="warning-error-tr-gsrv-srv-port" class="warning-error gsrv-warning" style="display:none">
								<td colspan="2">Missing value</td>
							</tr>																		
							<tr class="row-gsrv-action-field row-gsrv-create row-gsrv-update">
								<th>Services<font color="red">&nbsp;*</font></th>
								<td>
									<select id="gsrv-srvs" multiple size="4" class="gsrv-srvs control control-select control-editable" onchange="mtn_groups.display_selection_action_button('srv')" ></select>
									<span class="control-display-editable multiple-select-linked-button" >
										<input type="button" class="button control-button small-button" value="Add" onclick="mtn_groups.sel_add('srv');" >
										<input id="gsrv-srv-drop-rule-button" type="button" class="button control-button small-button drop-rule-button" value="Delete" onclick="mtn_groups.sel_rem('srv');" style="display:none;">
									</span>
								</td>
							</tr>
							<tr class="warning-error warning-error-tr-gsrv-srvs gsrv-warning" style="display:none;">
								<td colspan="2">No service(s) detected</td>
							</tr>									
						</table>
						<table class="group-component group-button">										
							<tr class="control-display-editable">									
								<td>
									<input type="button" class="button control-button small-button add-button" value="Add" onclick="mtn_groups.add();">
									<input type="button" class="button control-button small-button edit-button" value="Edit" onclick="mtn_groups.edit();">
									<input type="button" class="button control-button small-button" value="Clear" onclick="mtn_groups.clear(1);">
									<input type="button" class="button control-button small-button" value="Cancel" onclick="mtn_groups.cancel();">
								</td>
							</tr>
							<tr class="control-display-readonly">
								<td>
									<input type="button" class="button control-button small-button" style="float:right;" value="Close" onclick="mtn_groups.cancel();">
								</td>
							</tr>
						</table>
					</div>
					</form>
					
					<div id="debug-tool" style="display:none">	
						<div id="debug-tool-handle"><h2>Debug Tool Box</h2></div>
							<table class="group-component">
								<tr>
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="clear" onclick="clearDT()" style="display:inline">
										<input type="button" class="button control-button small-button" value="Edit" onclick="getE('readonly').value = false;setReadonlyView(false);" style="display:inline">
										<input type="button" class="button control-button small-button" value="up-cat" onclick="updateCatalogMtn();" style="display:inline">
										<input type="button" class="button control-button small-button" value="FWO_V" onclick="getE('fwo').value = true;setUserGroupView();flow.refreshFlowsView();" style="display:inline">
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="CT-User" onclick="setCelsiusUser();" style="display:inline">
										<input id="adm-switch" type="button" class="button control-button small-button" value="ADM" onclick="toggleLdapGroup('adm');">
										<input id="adv-switch" type="button" class="button control-button small-button" value="ADV" onclick="toggleLdapGroup('adv');">
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<input id="fwe-switch" type="button" class="button control-button small-button" value="FWE" onclick="toggleLdapGroup('fwe');">
										<input id="so-switch" type="button" class="button control-button small-button" value="SO" onclick="toggleLdapGroup('so');">
										<input id="fwo-switch" type="button" class="button control-button small-button" value="FWO" onclick="toggleLdapGroup('fwo');">
									</td>
								</tr>
								<tr style="display:none">
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="dupform" onclick="DF();" style="display:inline">
										<input type="button" class="button control-button small-button" value="up-cat" onclick="updateCatalog();" style="display:inline">
										<input type="button" class="button control-button small-button" value="initFWN" onclick="initFWN();" style="display:inline">
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="ldap" onclick="ldapMe();" style="display:inline">
										<input type="button" class="button control-button small-button" value="WS" onclick="test_WS(getE('debug-area').value);" style="display:inline">
										<input type="button" class="button control-button small-button" value="upload" onclick="upload(getE('debug-area').value);" style="display:inline">
									</td>
								</tr>
								<tr id="warning-error-tr-debug-list-selection" class="warning-error" style="display:none">
									<td colspan="2">Blurp Blurp</td>
								</tr>								
								<tr>
									<td colspan=2>
										<span id="debug-section" style="color:red;font-weight:bold;"></span>
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="clear" onclick="getE('debug-area').value=''" style="display:inline">
										<input type="button" class="button control-button small-button" value="try" onclick="ant()" style="display:inline">
										<input type="button" class="button control-button small-button" value="show" onclick="showGroups();" style="display:inline">
										<input type="button" class="button control-button small-button" value="goos" onclick="addMtnSamples(1);" style="display:inline">
										<input type="button" class="button control-button small-button" value="gsrvs" onclick="addMtnSamples(2);" style="display:inline">
										<input type="button" class="button control-button small-button" value="householder" onclick="check_HH_keys();" style="display:inline">
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<textarea id="debug-area" class="control-textarea"></textarea>
									</td>
								</tr>
								<tr id="warning-error-tr-debug-area" class="warning-error" style="display:none">
									<td colspan="2">Blurp Blurp</td>
								</tr>
							</table>
						</span>
					</div>
					
				</div>
			</div>					
			<div id="footer">	
				<img src="<%= request.getContextPath()%>/ressources/footer.jpg" class="bgfooter">
			</div>				
		</div>
	</body>
</html>
