<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<LINK href="ressources/autosuggest.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		<title>FCRWeb - Admin</title>
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="admin.htm">Admin</a> -> Manage Private Ranges</h4>
		<h2>Manage Private Ranges</h2>

			<% 
			ArrayList<String[]> ranges =(ArrayList<String[]>)request.getAttribute("ranges");
			int i = 0;
			String ids = "";
			 %>
			<center>
			 <table id="rangeTable">
			
			<%
			for (String[] range: ranges)
			{
				ids = ids + "-" + i + "-";
			 %>			
				 <tr id="row<%= i %>">
				 	<td>From <input type="text" value="<%= range[0] %>" id="range<%= i %>start"/></td>
				 	<td>To <input type="text" value="<%= range[1] %>" id="range<%= i %>end"/></td>
				 	<td><a href="javascript:void(0)" onclick="removeRange(<%= i %>)"><img src="ressources/navigate_minus.png" alt="Remove this range" title="Remove this range"></a></td>
				 </tr>
			<%
			i+=1;
			}
			%>
			
			</table>
			<input id="add_button" class="button" value="Add range" type="button" onClick="addRange()" />
			<input id="update_button" class="button" value="Update ranges" type="button" onClick="updateRanges()" /></center><br>
			<input type="hidden" value="<%=ids %>" id="param_numbers"/>
			<input type="hidden" value="<%=i %>" id="last_id"/>
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>