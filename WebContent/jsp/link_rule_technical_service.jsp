<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<LINK href="ressources/autosuggest.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/link_rule_technical_service.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/autosuggest.js"></SCRIPT>
		
		<title>FCRWeb - Admin</title>
		
	</head>
	<body onload="checkRule();">
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="admin.htm">Admin</a> -> Manage Technical Service Rules</h4>
		<h2>Add Technical Service to Rule</h2>
			
			<% 
			//String rulenames =(String)request.getAttribute("rulenames");
			//String tsnames = (String)request.getAttribute("technical_services");
			 %>
			 
			<table>
				<tr>
					<th>Rule Name</th>
					<td>
						<input type="text" id="rule_name">
					</td>					
				</tr>
				<tr>
					<th>Technical Service</th>
					<td>
						<input type="text" id="technical_service_name">
					</td>					
				</tr>
				<tr>
					<td></td>
					<td><input class="button" value="Assign" type="button" onClick="assignTechnicalService('false')" />&nbsp;&nbsp;&nbsp;<img id="load_img" style="visibility:hidden" src="ressources/progress.gif" ></td>
				</tr>
			</table>
			
			<script>
				new autosuggest("rule_name", "", "admin.htm?id=1&ms=" + new Date().getTime() + "&autocomplete_rule=");
				new autosuggest("technical_service_name", "", "admin.htm?id=1&ms=" + new Date().getTime() + "&autocomplete_ts=");
			</script>
			
		<h2>Manage Rules of Technical Service</h2>
		
		<table>
				<tr>
					<th>Technical Service</th>
					<td><input type="text" id="technical_service"></td>
				</tr>
				<tr>
					<td></td>
					<td><input class="button" value="Manage" type="button" onClick="manageTechnicalService()" />&nbsp;&nbsp;&nbsp;<img id="load_img_second" style="visibility:hidden" src="ressources/progress.gif" ></td>
				</tr>
		</table>
		
			<script>
				new autosuggest("technical_service", "", "admin.htm?id=1&ms=" + new Date().getTime() + "&autocomplete_ts=");
			</script>
		
		<div id="managementPanel" style="display:none;">
		<br>
			<table>
				<tr>
					<td>
					    <select name="selectfrom" id="selectfrom" multiple size="10" STYLE="width: 200px;background:#FF9999">					      
					    </select>
					</td>
					<td>
						<center>
						    <input class="button" value="Add >" type="button" onClick="addRuleToTS()" /><br><br>		
						    <input class="button" value="< Remove" type="button" onClick="removeRuleFromTS()" />
					    </center>
					</td>
					<td>
					    <select name="selectto" id="selectto" multiple size="10" STYLE="width: 200px;background:#33FF99">					      
					    </select>
					</td>
					<td>
					    <center>
						    <input class="button" value="Save configuration" type="button" onClick="saveConfiguration('false')" />
					    </center>
					</td>
					<td>
						<input type="hidden" value="" id="technical_service_name_hidden">
					</td>
				</tr> 
			</table>
		</div>
		
		<div id="object_conflicts"></div>
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>