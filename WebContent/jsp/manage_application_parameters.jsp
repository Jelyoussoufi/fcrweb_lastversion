<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<LINK href="ressources/autosuggest.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		<title>FCRWeb - Admin</title>
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="admin.htm">Admin</a> -> Manage Application Parameters</h4>
		<h2>Manage Application Parameters</h2>

			<% 
			ArrayList<String[]> params =(ArrayList<String[]>)request.getAttribute("params");
			int i = 0;
			for (String[] param: params)
			{
			 %>

			 <table>
				 <tr>
				 	<td><input type="text" value="<%= param[0] %>" id="param<%= i %>name" disabled/></td>
				 	<td><input type="text" size=90 value="<%= param[1] %>" id="param<%= i %>value"/></td>
				 </tr>
			 </table>

			<%
			i+=1;
			}
			%>
			<center><input id="update_button" class="button" value="Update parameters" type="button" onClick="updateParameters()" /></center><br>
			<input type="hidden" value="<%= params.size() %>" id="param_number"/>
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>