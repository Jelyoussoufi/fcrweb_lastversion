<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.ObjectSummary" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		
		<title>FCRWeb - Preapproved Objects</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="fcr.htm">Firewall CR</a> -> Preapproved objects selection</h4>
		<h2>Preapproved objects selection</h2>		
			
		<% 
		ArrayList<ObjectSummary> preapproved =(ArrayList<ObjectSummary>)request.getAttribute("preapproved");
		int i = 0;
		
		for (ObjectSummary preapprovedobject:preapproved)
		{	
			if (i == 0)
			{
			%>						
			<table >
			<tr>
				<th style="width:25%;" class="first"><strong>Name</strong></th>
				<th style="width:60%;" >Description</th>
				<th style="width:15%;"></th>
			</tr>
			<%
			}
			if (i%2 == 0)
			{
				%>
				<tr class="row-a">
				<%
			}
			else
			{
				%>
				<tr class="row-b">
				<%
			}
			%>
				<td class="first"><%= preapprovedobject.getName() %></td>
				<td><%= preapprovedobject.getDescription() %></td>
				<td><a style="font-size:12px" href="javascript:void(0)" onClick="window.open('preapproved.htm?id=<%= preapprovedobject.getId() %>', '_blank', 'scrollbars=1,resizable=yes, width=1050, height=700')">Update Group</a></td>
			</tr>
			<%
			i += 1;
		}
		
		if (i == 0)
		{
			%>
			No Preapproved Object Groups to display.
			<%
		}
		else
		{
			%>
			</table>
			<%
		}
		 %>
		
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>