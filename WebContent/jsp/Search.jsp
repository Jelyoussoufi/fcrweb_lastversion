<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.RequestSummary" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" media="all" href="ressources/jsDatePick_ltr.min.css" />
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/search.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>
		<script type="text/javascript" src="<%= request.getContextPath()%>/js/jsDatePick.min.1.3.js"></script>
		<style type="text/css">
	        tr.row_even { background-color: #fafafa; }
	        tr.row_odd  { background-color: white; }
			td          { padding: 4px; vertical-align: top;}
			
			td.id       { text-align: right; }
			td.history  { background: white; }
			td.flag     { padding: 0 2px 0 2px; text-align: center; vertical-align: middle;}
			td.fcr_edition       { background: #FFFFAA; }
			td.ts_validation       { background: #FFDD00; }
			td.security_validation { background: #FFCC00; }
			td.closure, td.resolved           { background: #99FF00; }
			td.end      { background: #AADD11; }
			td.pending, td.information_required  { background: #CCCCFF; }
			td.validation, td.key_user_validation { background: #FFFF11; }
			td.implement, td.implementation, td.delete, td.Auto_impl_ongoing { background: #FF9900; }		
		</style>
	</head>
	<body onLoad="initDatePicker()">
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		
		<%
			String type = (String)request.getAttribute("type");
			if (type.equals("FCR"))
			{
		%>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> Firewall CR -> Advanced Search</h4>
		<%
			}
			else
			{
		%>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> Technical Service -> Rules -> Advanced Search</h4>
		<%
			}
		%>
		
		<h2>Advanced Search</h2>
		
		<%
			if (type.equals("FCR"))
			{
		%>
		<center >
			<table>
			<tr>
				<th>FCR Number</th>
				<td><input name="fcr_number" id="fcr_number" type="text"/></td>
			</tr>
			<tr>
				<th>Requestor</th>
				<td><input id="requestor" type="text"/></td>
			</tr>
			<!-- <tr>
				<th>Technical Service</th>
				<td><input id="technical_service" type="text"/></td>
			</tr> -->
			<tr>
				<th>Context</th>
				<td>
					<select id="context">
					  <option value="all"></option>
					  <option value="project">Project</option>
					  <option value="ctr">CTR</option>
					  <option value="remote">Remote Access</option>
					  <option value="other">Other</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>From</th>
				<td><input id="from" type="text"/></td>
			</tr>
			<tr>
				<th>To</th>
				<td><input id="to" type="text"/></td>
			</tr>
			<tr>
				<th>Task</th>
				<td>
					<select id="task">
					  <option value="all"></option>
					  <option value="FCR Edition">FCR Edition</option>
					  <option value="Validation">Validation</option>
					  <option value="TS Validation">TS Validation</option>
					  <option value="Security Validation">Security Validation</option>
					  <option value="Implement">Implement</option>
					  <option value="Closure">Closure</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>Source Object</th>
				<td><input id="source" type="text"/></td>
			</tr>
			<tr>
				<th>Destination Object</th>
				<td><input id="destination" type="text"/></td>
			</tr>
			<tr>
				<td></td>
				<td><input class="button" id="button" value="Clear" onClick="clearFCR()" type="button"/>&nbsp;&nbsp;&nbsp;<input class="button" id="button" value="Search" onClick="searchFCR()" type="button"/></td>
			</tr>
			</table>
		</center>
		<%
			}
			else
			{
		%>
		
			<center >
			<table>
			<tr>
				<th>Name</th>
				<td><input name="name" id="name" type="text"/></td>
			</tr>
			<tr style="display:none;">
				<th>Created after</th>
				<td><input id="from" type="text"/></td>
			</tr>
			<tr style="display:none;">
				<th>Created before</th>
				<td><input id="to" type="text"/></td>
			</tr>
			<tr>
				<th>Source</th>
				<td><input id="source" type="text"/></td>
			</tr>
			<tr>
				<th>Destination</th>
				<td><input id="destination" type="text"/></td>
			</tr>
			<tr>
				<th>Service</th>
				<td><input id="service" type="text"/></td>
			</tr>			
			<tr>
				<td></td>
				<td><input class="button" id="button" value="Clear" onClick="clearRule()" type="button"/>&nbsp;&nbsp;&nbsp;<input class="button" id="button" value="Search" onClick="searchRule()" type="button"/></td>
			</tr>
			</table>
		</center>
		
		<%
			}
		%>
		
		<div id="SearchResult"></div>
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>