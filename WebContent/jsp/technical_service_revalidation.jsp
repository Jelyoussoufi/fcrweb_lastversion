<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>
<%@ page import="be.celsius.fcrweb.objects.TechnicalServiceRevalidationData" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>


		<title>FCRWeb - Reports</title>

	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="reports.htm">Reports</a> -> Technical Service Revalidation</h4>
		<h2>Technical Service Revalidation</h2>		

		<% 
		ArrayList<TechnicalServiceRevalidationData> ts_list =(ArrayList<TechnicalServiceRevalidationData>)request.getAttribute("technical_services");

		 %>

		 <table>
		 	<tr>
		 		<th>Technical Service</th>
		 		<th>Owner</th>
		 		<th>Last Revalidation date</th>
		 		<th>Status</th>
		 		<th>Revalidation month</th>
		 	</tr>

			<%
			for (TechnicalServiceRevalidationData ts: ts_list)
			{
			%>
		 	<tr>
		 		<td><%= ts.getTechnicalService() %> </td>
		 		<td><%= ts.getOwner() %></td>
		 		<td><%= ts.getLastRevalidation() %></td>
		 		<td><%= ts.getStatus() %></td>
		 		<td><%= ts.getRevalidationMonth() %></td>
		 	</tr>
		 	<%
		 	}
		 	%>
		 </table>

		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>