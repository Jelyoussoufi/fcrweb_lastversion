<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.Service" %>
<%@ page import="be.celsius.fcrweb.utils.AuthenticationUtils" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/services.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>

		<style type="text/css">
	        body { background-color: #fafafa; }	      
	        table { background-color: #D0D0D0 ; } 
		</style>
		
		<title>Service</title>		
		
	</head>
	<body>		
		<%
			Service serviceBean=(Service)request.getAttribute("service");
			String user_profiles = (String)request.getAttribute("user_profile");
		%>
		<h2>Service <%= serviceBean.getName() %></h2>
		<table>
			<tr>
				<th>Type</th>
				<td><%= serviceBean.getType() %></td>
			</tr>
			<%
				ArrayList<String[]> temp = serviceBean.getOtherParameters();
				for (int i = 0; i < temp.size(); i++)
				{
					String[] param = temp.get(i);
					%>
					<tr>
						<th><%= param[0] %></th>
						<td><%= param[1] %></td>
					</tr>
					<%
				}
			%>
			<tr>
				<th>Used in Groups</th>
				<td><%= serviceBean.getUsedInGroups() %></td>
			</tr>
			<tr>
				<th>Used in Rules</th>
				<td><%= serviceBean.getUsedInRules() %></td>
			</tr>
			<%
			if (AuthenticationUtils.hasAdministratorProfile(user_profiles))
			{
				%>
				<tr>
					<th>Global</th>
					<%
					if (serviceBean.isPredefined())
					{
						%>
						<td><input type="checkbox" checked="checked" disabled="disabled" id="predefined"/></td>
						<%
					}
					else
					{
						%>
						<td><input type="checkbox" disabled="disabled" id="predefined"/>&nbsp;&nbsp;<input id="update_predefined" class="button" value="set Global" type="button" onClick="setServiceAsPredefined(<%= serviceBean.getId() %>, '<%= serviceBean.getType() %>')" /></td>
						<%
					}
					%>
				</tr>
				<%
			}
			%>
		</table>
	</body>
</html>