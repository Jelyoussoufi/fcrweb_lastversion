<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.Profile" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="be.celsius.fcrweb.utils.AuthenticationUtils" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/profiles.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>

		<style type="text/css">
	        body { background-color: #fafafa; }	      
	        table { background-color: #D0D0D0 ; } 
		</style>
		
		<title>Profile</title>		
		
	</head>
	<body>		
				
		<%
		Profile profileBean=(Profile)request.getAttribute("profile");
		String user_profiles = (String)request.getAttribute("user_profile");
		%>
		<h2>User Group <%= profileBean.getName() %></h2>
		<table>
			<tr>
				<th>Description</th>
				<td><%= profileBean.getDescription() %></td>
			</tr>			
			<tr>
				<th>Responsible UID</th>
				<td><%= profileBean.getResponsibleUid() %></td>
			</tr>
			<tr>
				<th>Used in Rules</th>
				<td><%= profileBean.getUsedInRules() %></td>
			</tr>		
			<%
			if (AuthenticationUtils.hasAdministratorProfile(user_profiles))
			{
				%>
				<tr>
					<th>Global</th>
					<%
					if (profileBean.isPredefined())
					{
						%>
						<td><input type="checkbox" checked="checked" disabled="disabled" id="predefined"/></td>
						<%
					}
					else
					{
						%>
						<td><input type="checkbox" disabled="disabled" id="predefined"/>&nbsp;&nbsp;<input id="update_predefined" class="button" value="set Global" type="button" onClick="setProfileAsPredefined(<%= profileBean.getId() %>)" /></td>
						<%
					}
					%>
				</tr>
				<%
			}
			%>
		</table>
	</body>
</html>