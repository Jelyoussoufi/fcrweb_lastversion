<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>
<%@ page import="be.celsius.fcrweb.objects.RequestSummary" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		<style type="text/css">	        
			td.rejected { background: #FF0000;color:white; }
		</style>
		
		<title>FCRWeb - Reports</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="reports.htm">Reports</a> -> FCR Rejected</h4>
		<h2>FCR rejected for implementation</h2>		
			
			<% 
			ArrayList<RequestSummary> fcr_rejected =(ArrayList<RequestSummary>)request.getAttribute("fcr_rejected");
			
			
			if (fcr_rejected.size() ==0)
			{
				%>
				<p>There are no rejected FCR</p>	
				<% 
			}
			else
			{
				%>
				<table>
					<tr>
						<th class="first"><strong>ID</strong></th>
						<th>Detail</th>
						<th>Requestor</th>
						<th>Date</th>
						<th>Task</th>
					</tr>
				
				
					<%	
						int i = 0;
						for (RequestSummary fcr:fcr_rejected)
						{			
							if (i%2 == 0)
							{
								%>
								<tr class="row-a">
								<%
							}
							else
							{
								%>
								<tr class="row-b">
								<%
							}
							%>		
								<td class="first"><%= fcr.getId() %></td>
								<td><a href="javascript:void(0)" onClick="window.open('/fcrweb/jsp/fcr-form.jsp?fid=<%= fcr.getId() %>', '_blank', 'scrollbars=1,resizable=yes, width=1050, height=700')"><%= fcr.getDetail() %></a></td>
								<td><%= fcr.getRequestor() %></td>
								<td><%= fcr.getDate() %></td>
								<td class="rejected">Rejected</td>
							</tr>
							
							<%
							i=i+1;
						}			
					%>
					
				</table>
		<%
			}
			 %>
		
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>