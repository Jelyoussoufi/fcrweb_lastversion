<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		
		<title>FCRWeb - Reports</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="reports.htm">Reports</a> -> Orphan Firewall Rules</h4>
		<h2>Orphan Firewall Rules</h2>		
		
			<% 
			String orphan_rules =(String)request.getAttribute("orphan_rules");
			String[] a = orphan_rules.split("orphanNumber");
			int orphanNumber = Integer.parseInt(a[0]);
			String[] b = a[1].split("totalNumber");
			int totalNumber = Integer.parseInt(b[0]);
			String[] rule_couples = b[1].split(";");
			
			
			if (rule_couples.length ==1 && rule_couples[0] == "")
			{
				%>
				<p>There are no orphan rules</p>	
				<% 
			}
			else
			{				
				%>
				<p><%= orphanNumber %> orphan rule(s) out of <%= totalNumber %> rules</p>	
				
				<table>
				<tr>
					<th>Name</th>
					<th></th>
				</tr>
				<%
				for (String couple:rule_couples)
				{
					String rule_id = couple.split(",")[0];
					String rule_name = couple.split(",")[1];
					%><tr>
						<td><a href="javascript:void(0)" onClick="window.open('rules.htm?id=<%= rule_id %>','_blank','resizable=yes,width=700,height=500')"><%= rule_name %></a></td>
						<td><a href="admin.htm?id=1&rule=<%= rule_name.replace(" ", "%20") %>">Assign technical service</a></td>
					</tr><%
				}			
			%>
			</table>
			<%
			}
			 %>
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>