<%@ page language="java" pageEncoding="ISO-8859-1" contentType="text/html;charset=ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/taglib/c.tld" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglib/spring.tld" prefix="spring" %>
<%@ page isELIgnored="false" %>
<%@ page import="java.lang.String" %>
<% 
	String contextPath 	= request.getContextPath();
	String path 		= contextPath;
	String basePath 	= request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort() ; // + "/";
	String realPath 	= request.getRealPath("\\");
	String getURL 		= request.getRequestURL().toString();
	String reqURI 		= request.getRequestURI();
	String servletPath 	= request.getServletPath();
	String queryString 	= request.getQueryString();
	String userAgent 	= request.getHeader("user-agent");
	String isFox		= "false";
	if (userAgent.toLowerCase().contains("firefox"))
		isFox = "true";
		
	String form_type = "request";
%>
<jsp:useBean id="environment" class="be.celsius.util.session.Environment" />

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head id="head">
		
		<c:set scope="session" var="atCelsius" value="${environment.environment==1}" />
		<c:if test="${atCelsius==true}" >
			<input type="hidden" id="contextPath" value="<%= contextPath%>" />
			<input type="hidden" id="atCelsius" value="true" />
			<c:set scope="session" var="path" value="<%= path%>" />
		</c:if>
		<c:if test="${atCelsius!=true}" >
			<input type="hidden" id="contextPath" value="<%= contextPath%>" />
			<base href="<%=basePath%>">
			<input type="hidden" id="atCelsius" value="false" />
			<c:set scope="session" var="path" value="" />
		</c:if>

		<input type="hidden" id="userAgent" value='<%= userAgent%>'/>
		<input type="hidden" id="homePage" value='false'/>
		<input type="hidden" id="trueContextPath" value="<%= contextPath%>" />
		<input type="hidden" id="basePath" value="<%= basePath%>" />
		<input type="hidden" id="servletPath" value="<%= servletPath%>" />
		<input type="hidden" id="realPath" value="<%= realPath%>" />
		<input type="hidden" id="getURL" value="<%= getURL%>" />
		<input type="hidden" id="reqURI" value="<%= reqURI%>" />
		<input type="hidden" id="queryString" value="<%= queryString%>" />
		<!--[if IE]><input type="hidden" id="ie" value="true" /><![endif]-->
		<!--[if lte IE 8]><input type="hidden" id="ieold" value="true" /><![endif]-->
		<input type="hidden" id="ie" value="false" />
		
		<input type="hidden" id="local" name="local" value="" />
		<input type="hidden" id="uid" value="" />	
		<input type="hidden" id="ts_id" value="" />
		<input type="hidden" id="ts_name" value="" />
		<input type="hidden" id="edition" value="" />
		<input type="hidden" id="fwo" value="" />
		<title>Firewall Change Request Form</title>
		
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/main_form.css" type="text/css" />
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/autosuggest.css" type="text/css" />
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/jsDatePick_ltr.min.css" type="text/css" />
		
		<style type="text/css">
			#main{width:93%;}
			#main-section{width:98%;}
			
			body { font-size: 62.5%;}
			label, input { display:block; }
			input.text { margin-bottom:12px; width:95%; padding: .4em; }
			fieldset { padding:0; border:0; margin-top:25px; }
			h1 { font-size: 1.2em; margin: .6em 0; }
			
			div#users-contain { width: 100%; margin: 0; }
			div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
			div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
			.ui-dialog .ui-state-error { padding: .3em; }
			.validateTips { border: 1px solid transparent; padding: 0.3em; }
			
			.warning-error td
			{
				color : red;
				font-italic : italic;				
				font-weight : bold;				
			}
			
			.warning-conflict td
			{
				color : orange;
				font-italic : italic;				
				font-weight : bold;	
			}
						
			.textarea-img
			{
				position: relative;
				top: 50%;
				vertical-align: top;
				padding:0px;
				margin:0px;
				display:inline;
			}
			
			.img
			{
				padding:0px;
				margin:0px;
				vertical-align:middle;
				align:center;
				display:inline;
			}
			
			.img-clickable
			{
				cursor: pointer;				
			}
			
			.mask_arrow 
			{
			/*
				background-image: url("<%= request.getContextPath()%>/ressources/more.gif");
			*/
			}
			
			.control{display:inline;}
			.control-radio{display:inline; height:10px; line-height:10px;}
			.control-radio-label{display:inline; height:10px; line-height:10px;}
			.control-radio-group{width:300px;padding:0px; margin:0px;}
			
			.control-help{display:none;}
			
			.control-link
			{
				cursor:pointer;
				color:#2180BC;				
			}
			
			.control-link:hover{color:#88AC0B;}
			.control-link:visited{color:#2180BC;}
			
			.group-component{margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;}
			.group-component tr			
			{font-size: 75%;}
					
			.group-component th {width:150px;}
			.group-component .control-text {width:300px;}
			.group-component .small-text{width:200px;}
			.group-component .tiny{width:50px;}
			.group-component select {width:300px;}
			
			input, label {
				padding:2px;
				border:1px solid #eee;
				font: normal 1em Verdana, sans-serif;
				color:#777;
			}
						
			.control-textarea
			{
				width:300px;
				height:100px;
				resize:none;
			}
			
			.mini-textarea
			{
				width:200px;
				height:50px;
				resize:none;
			}
			.tiny-textarea
			{
				width:120px;
				height:30px;
				resize:none;
			}
			
			.tiny
			{
				width:120px;
			}
			
			.repeat-group
			{font-size : 10px;}
			
			.repeat-group tr,
			.repeat-group td
			{
				padding:0px;
				margin:0px;
			}
			
			.repeat-group input,
			.repeat-group label
			{
				display:inline;
				padding:0;
				margin:0;
				border:0px;
			}	
			
			.flow-documentation-ddl
			{
				display:inline;
				height:16px;
				line-height:16px;
			}
			
			/* buttons */
			.group-button{margin-top:10px;}
			.button 
			{
				cursor: pointer; 
				display:inline;
				padding-top:0;
				padding-bottom:0;
				margin-top:0;
				margin-bottom:0;				
			}
						
			.small-button {width: 50px;}
			.normal-button {width: 120px;}	
			
			.multiple-select-linked-button
			{
				display: inline;
				position: relative;
				top: 15px;
				vertical-align: top;
			}
			
			/* default display mode
			.control-display-editable{display:none;}
			*/
			
			.maskass
			{
				background-color:black;
				position: fixed;
				top: 0;
				left: 0;
				min-width: 100%;
				width: 100%;
				min-height: 100%;
				height: 100%;
				opacity:0.5;
				Z-INDEX: 10; 
				filter: alpha(opacity = 50);
			}
			
			.processing
			{
				background-image: url("<%= request.getContextPath()%>/ressources/processing-1.gif");
                background-position: center center;
                background-repeat: no-repeat;				
				background-color:black;	
				position: fixed;
				top: 0;
				left: 0;
				min-width: 100%;
				width: 100%;
				min-height: 100%;
				height: 100%;
				opacity:0.5;
				Z-INDEX: 19; 
				filter: alpha(opacity = 50);
			}
			
			.maskass-ie8
			{
				background-color:black;
				opacity:0;
				filter: alpha(opacity = 0);
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 800px;				
				margin: 0px;
				padding: 0px;
				Z-INDEX: 10;
			}
			
			.control-dialog
			{
				background:white;
				border: thin #93BC0C solid;
				border-radius : 5px;
				position: absolute;
				top: 25%;
				left: 25%;
				Z-INDEX: 11; 		
				width:650px;
			}
			
			.control-list-member
			{
				background:white;
				border: thin #93BC0C solid;
				border-radius : 5px;
				position: absolute;
				top: 25%;
				left: 25%;
				Z-INDEX: 12; 		
				width:400px;
			}
			
			option .control-option-disabled,
			.control-option-disabled
			{
				color : black;
			}
			
			#display-group-members{}
			
			.autosuggest-body{Z-INDEX:12;}
			
			.control-dialog .group-button
			{float:right;}
		</style>
		
		<!--[if IE]><script LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></script><![endif]-->
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jsDatePick.min.1.3.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/autosuggest.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/util_function.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/util_html.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_rules_revalidation.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_form_util.js"></SCRIPT>		
		<!--[<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jquery-2.0.3.js"></SCRIPT>-->
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jquery.js"></SCRIPT>
		
	</head>
	<body id="body" onload="load();">
		
		<div id="wrap">
			<img src="<%= request.getContextPath()%>/ressources/content_forms.jpg" class="bgcontent">
			<div id="header">				
				<img src="<%= request.getContextPath()%>/ressources/header.jpg" class="bgheader">
			</div>
			<div id="content-wrap">				
				<div id="main">
					<div class="processing" style="display:none" ></div>
					<h2><span onclick="checkDebugTool();">My Rules</span></h2>						
					<div id="rules" style="width:100%;height:400px;overflow:auto;">
						
					</div>
					<p>
						<table class="group-component">
							<tr>
								<td colspan=2>
									<input type="button" class="button control-button small-button owner-view" value="Submit" onclick="send();" style="display:none">								
									<input type="button" class="button control-button fwo-view" value="Update flags" onclick="send_flags();" style="display:none">								
								</td>
							</tr>
						</table>
					</p>
					<div id="debug-tool" style="display:none">	
						<p></p>
						<table class="group-component">
							<tr>
								<td colspan=2>
									<input type="button" class="button control-button small-button" value="clear" onclick="setHtml('debug-section', '');" style="display:inline">
									<input type="button" class="button control-button small-button" value="reload" onclick="load()" style="display:inline">
									<input type="button" class="button control-button small-button" value="setCelsiusUser" onclick="setCelsiusUser()" style="display:inline">										
								</td>
							</tr>
							<tr>
								<td colspan=2>
									<div id="debug-section" style="color:red;font-weight:bold;"></div>
								</td>
							</tr>
							<tr>
								<td colspan=2>
									<input type="button" class="button control-button small-button" value="try" onclick="ant()" style="display:inline">
									<input type="button" class="button control-button small-button" value="clear" onclick="getE('debug-area').value=''" style="display:inline">
									
								</td>
							</tr>
							<tr>
								<td colspan=2>	
									<input type="button" class="button control-button button" value="ts_reval()" onclick="init_ts_reval()" style="display:inline">
								</td>
							</tr>
							<tr>
								<td colspan=2>
									<textarea id="debug-area" class="control-textarea"></textarea>
								</td>
							</tr>
						</table>
					</div>
				</div>				
			</div>					
			<div id="footer">	
				<img src="<%= request.getContextPath()%>/ressources/footer.jpg" class="bgfooter">
			</div>				
		</div>
	</body>
</html>