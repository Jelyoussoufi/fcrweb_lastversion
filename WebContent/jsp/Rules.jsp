<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.RuleSummary" %>
<%@ page import="be.celsius.fcrweb.objects.RuleObjectSummary" %>
<%@ page import="be.celsius.fcrweb.objects.RulesPageData" %>
<%@ page import="be.celsius.fcrweb.utils.ExtractorUtils" %>
<%@ page import="be.celsius.fcrweb.utils.AuthenticationUtils" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/rules.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/util_function.js"></SCRIPT>

		<title>FCRWeb - Rules</title>
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/Rules_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> Technical Service -> Rules</h4>

		
		<div id="SearchResult"></div>
		<h2>My Rules</h2>
		<div style="font-size:10px">
		<%
				String user_profiles = (String) request.getAttribute("user_profile");
				RulesPageData rulesBean=(RulesPageData)request.getAttribute("rules");
				ArrayList<RuleSummary> myRules = rulesBean.getMyRules();
				int list_id = 0;
				int i = 0;
				Boolean hasResult = false;
				String oldTS = "";
				
				%>
				<script>var listTab = [];</script>
				<%
				
				for (RuleSummary myRule:myRules)
				{
					if (myRule.getTechnicalService().equals(oldTS))
					{
						if (i%2 == 1)
						{
							%>
							<tr class="row-a">
							<%
						}
						else
						{
							%>
							<tr class="row-b">
							<%
						}
						%>
						<td class="first"><input type="checkbox" onclick="ruleChecked(<%= myRule.getId() %>, <%= list_id %>);"/></td>
						<%
						if (myRule.isActive())
						{
						%>
							<td><a href="javascript:void(0)" onClick="window.open('rules.htm?id=<%= myRule.getId() %>', '_blank', 'resizable=yes,scrollbars=yes, width=700, height=500')"><%= myRule.getName() %></a></td>
						<%
						}
						else
						{
						%>
							<td><a href="javascript:void(0)" onClick="window.open('rules.htm?id=<%= myRule.getId() %>', '_blank', 'resizable=yes,scrollbars=yes, width=700, height=500')"><strike><%= myRule.getName() %></strike></a></td>
						<%
						}
						String src_objects = ExtractorUtils.listSourceObjectToHtml(myRule.getSource());
						String dst_objects = ExtractorUtils.listRuleObjectToHtml(myRule.getDestination());
						String svc_objects = ExtractorUtils.listRuleServiceToHtml(myRule.getService());
						%>
							<td><%= src_objects %></td>
							<td><%= dst_objects %></td>
							<td><%= svc_objects %></td>
							<td><img src='ressources/mob_icons/<%= myRule.getAction().replace(" ", "").toLowerCase() %>.png'>&nbsp;<%= myRule.getAction() %></td>
							<td><%= myRule.getLast_used() %></td>
						</tr>

						<%
						i=i+1;
					}
					else
					{
						if (!oldTS.equals(""))
						{
							%>
							</table>
							<%
						}
						oldTS = myRule.getTechnicalService();	
						list_id = list_id +1;
						%>
						<h3><%= oldTS %></h3>
						<input id="button<%= list_id %>" style="display: none;" class="button" value="Edit Rule(s)" type="button" onClick="editSelectedRules(<%= list_id %>, '<%= oldTS %>')" />
						
						<script>
							listTab[<%= list_id %>] = [];
						</script>
						
						<table style="width:98%;word-wrap:break-word;table-layout:fixed;">
						<tr>			
							<th style="width:3%;" class="first"></th>						
							<th style="width:7%;"><strong>Name</strong></th>
							<th style="width:30%;">Source</th>
							<th style="width:26%;">Destination</th>
							<th style="width:17%;">Service</th>
							<th style="width:9%;">Action</th>
							<th style="width:8%;">Last Used</th>
						</tr>
						<%
						
						if (i%2 == 0)
						{
							%>
							<tr class="row-a">
							<%
						}
						else
						{
							%>
							<tr class="row-b">
							<%
						}						
						%>
						<td class="first"><input type="checkbox" onclick="ruleChecked(<%= myRule.getId() %>, <%= list_id %>);"></td>						
						<%
						if (myRule.isActive())
						{
						%>
							<td><a href="javascript:void(0)" onClick="window.open('rules.htm?id=<%= myRule.getId() %>', '_blank', 'resizable=yes,scrollbars=yes, width=700, height=500')"><%= myRule.getName() %></a></td>
						<%
						}
						else
						{
						%>
							<td><a href="javascript:void(0)" onClick="window.open('rules.htm?id=<%= myRule.getId() %>', '_blank', 'resizable=yes,scrollbars=yes, width=700, height=500')"><strike><%= myRule.getName() %></strike></a></td>
						<%
						}
						String src_objects = ExtractorUtils.listSourceObjectToHtml(myRule.getSource());
						String dst_objects = ExtractorUtils.listRuleObjectToHtml(myRule.getDestination());
						String svc_objects = ExtractorUtils.listRuleServiceToHtml(myRule.getService());
						%>
							<td><%= src_objects %></td>
							<td><%= dst_objects %></td>
							<td><%= svc_objects %></td>
							<td><img src='ressources/mob_icons/<%= myRule.getAction().replace(" ", "").toLowerCase() %>.png'>&nbsp;<%= myRule.getAction() %></td>
							<td><%= myRule.getLast_used() %></td>
						</tr>
						<%
						i = 0;
						hasResult = true;
					}
				}
				if (!hasResult)
				{
					%>
					No Rules to display.
					<%
				}
				else
				{
					%>
					</table>
					<%
				}
			%>
		
		</div>
		<%
			list_id = list_id+1;
		%>	
		<h2>Generic Rules <a id="generic_rules_link" href="javascript:void(0)" onClick="displayGenericRules()">(show)</a></h2>
		<div id="generic_rules" style="display:none">
		<input id="button<%= list_id %>" style="display: none;" class="button" value="Edit Rule(s)" type="button" onClick="editSelectedRules(<%= list_id %>, 'Generic')" />
		<script>
			listTab[<%= list_id %>] = [];
		</script>
		<div style="font-size:10px">
		<table  style="width:98%;word-wrap:break-word;table-layout:fixed;">
			<tr>
			<%
			if (AuthenticationUtils.hasAdministratorProfile(user_profiles) || AuthenticationUtils.hasSecurityOfficerProfile(user_profiles))
			{
			 %>
				<th style="width:3%;" class="first"></th>				
			<%
			}
			%>
				<th style="width:7%;"><strong>Name</strong></th>
				<th style="width:30%;">Source</th>
				<th style="width:26%;">Destination</th>
				<th style="width:17%;">Service</th>
				<th style="width:9%;">Action</th>
				<th style="width:8%;">Last Used</th>
			</tr>


			<%				
				ArrayList<RuleSummary> genericRules = rulesBean.getGenericRules();
				i = 0;
				for (RuleSummary myRule:genericRules)
				{
					if (i%2 == 0)
					{
						%>
						<tr class="row-a">
						<%
					}
					else
					{
						%>
						<tr class="row-b">
						<%
					}
					
					
					if (AuthenticationUtils.hasAdministratorProfile(user_profiles) || AuthenticationUtils.hasSecurityOfficerProfile(user_profiles))
					{
					 %>
					<td class="first"><input type="checkbox" onclick="ruleChecked(<%= myRule.getId() %>, <%= list_id %>);"></td>
					<%
					}
					if (myRule.isActive())
					{
					%>
						<td class="first"><a href="javascript:void(0)" onClick="window.open('rules.htm?id=<%= myRule.getId() %>', '_blank', 'resizable=yes,scrollbars=yes, width=700, height=500')"><%= myRule.getName() %></a></td>
					<%
					}
					else
					{
					%>
						<td class="first"><a href="javascript:void(0)" onClick="window.open('rules.htm?id=<%= myRule.getId() %>', '_blank', 'resizable=yes,scrollbars=yes, width=700, height=500')"><strike><%= myRule.getName() %></strike></a></td>
					<%
					}
						String src_objects = ExtractorUtils.listSourceObjectToHtml(myRule.getSource());
						String dst_objects = ExtractorUtils.listRuleObjectToHtml(myRule.getDestination());
						String svc_objects = ExtractorUtils.listRuleServiceToHtml(myRule.getService());
						%>
							<td><%= src_objects %></td>
							<td><%= dst_objects %></td>
							<td><%= svc_objects %></td>
						<td><img src='ressources/mob_icons/<%= myRule.getAction().replace(" ", "").toLowerCase() %>.png'>&nbsp;<%= myRule.getAction() %></td>
						<td><%= myRule.getLast_used() %></td>
					</tr>

					<%
					i = i+1;
				}
			%>
			
		</table>
		</div>
		</div>
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>