<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>
		<script LANGUAGE="JavaScript" src="<%= request.getContextPath()%>/js/Chart.js"></script>
		
		<title>FCRWeb - Reports</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="reports.htm">Reports</a> -> Average actions delay</h4>
		<h2>Average delay by action</h2>		
			
			<% 
			String actions_delay =(String)request.getAttribute("actions_delay");
			String[] vars = actions_delay.split(";");
			 %>

			<br><br><br>
			<center>
				<table>
					<tr>
						<td>
							<canvas id="canvas" height="350" width="550"></canvas>
						</td>
						<td>
							<table>
								<tr>
									<td bgcolor="#c8372a"></td>
									<td align="left">Validation</td>
								</tr>
								<tr>
									<td bgcolor="#97bbcd"></td>
									<td align="left">Security Validation</td>
								</tr>
								<tr>
									<td bgcolor="#97d26d"></td>
									<td align="left">Implementation</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</center>
		
		<script>

		var barChartData = {
			labels : [<%= vars[0] %>],
			datasets : [
				{
					fillColor : "rgba(200,55,42,0.5)",
					strokeColor : "rgba(200,55,42,1)",
					data : [<%= vars[1] %>]
				},
				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					data : [<%= vars[2] %>]
				},
				{
					fillColor : "rgba(151,210,109, 0.5)",
					strokeColor : "rgba(151,210,109,1)",
					data : [<%= vars[3] %>]
				}
			]
			
		}

		var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Bar(barChartData);
	
	</script>
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>