<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>
		<script LANGUAGE="JavaScript" src="<%= request.getContextPath()%>/js/Chart.js"></script>
		
		<title>FCRWeb - Reports</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="reports.htm">Reports</a> -> FCR Number per team</h4>
		<h2>FCR number per team</h2>		
			
			<% 
			ArrayList<String[]> fcr_per_team = (ArrayList<String[]>)request.getAttribute("fcr_per_team");
			
			String[] bgcolor = {"#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360", "#ED2A58", "#4B2AED", "#2AB6ED", "#2AED75", "#CCED2A", "#ED682A", "#ED2A2A"};
			 %>
		
		<br><br><br>
			<center>
				<table >
					<tr>
						<td>
							<canvas id="canvas" height="350" width="350"></canvas>
						</td>					
						<td>
							<table >
							<%
							for (int i = 0; i < fcr_per_team.size(); i++)
							{
								String[] temp = fcr_per_team.get(i);
							%>
								<tr>
									<td bgcolor="<%=bgcolor[i%bgcolor.length] %>"></td>
									<td align="left"><%=temp[0] %> (<%=temp[1] %> FCR)</td>
								</tr>							
							<%
							}
							%>
							</table>
						</td>
					</tr>
				</table>
			</center>
			
			<script>
		
					var doughnutData = [
							<%
							for (int i = 0; i < fcr_per_team.size(); i++)
							{
								String[] temp = fcr_per_team.get(i);
								%>
								{
			    					value: <%= temp[1]%>,
			    					color:"<%= bgcolor[i%bgcolor.length]%>"
			    				},
								<%
							}
							%>
			    			];

			    	var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut(doughnutData);
	
			</script>
		
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>