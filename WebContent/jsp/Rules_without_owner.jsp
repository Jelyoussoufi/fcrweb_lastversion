<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		
		<title>FCRWeb - Reports</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="reports.htm">Reports</a> -> Rules without owner</h4>
		<h2>Firewall Rules without owner</h2>
			
			<% 
			String rules_without_owner =(String)request.getAttribute("rules_without_owner");
			String[] rule_couples = rules_without_owner.split(";");
			
			if (rule_couples.length ==1 && rule_couples[0] == "")
			{
				%>
				<p>There are no rules without owner</p>
				<%
			}
			else
			{
				%>
				<table>
				<tr>
					<th>Name</th>
					<th></th>
				</tr>	
				<% 
				for (String couple:rule_couples)
				{
						String rule_id = couple.split(",")[0];
						String rule_name = couple.split(",")[1];
						%><tr>
							<td><a href="javascript:void(0)" onClick="window.open('rules.htm?id=<%= rule_id %>','_blank','resizable=yes,width=700,height=500')"><%= rule_name %></a></td>
							<td><a href="admin.htm?id=2&rule=<%= rule_id %>">Update technical service owner</a></td>
						</tr><%					
				}
				
				%>
				</table>
				<%
			}
			 %>
		
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>