<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>
<%@ page import="be.celsius.fcrweb.objects.FWObjectNamesNotCompliant" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		
		<title>FCRWeb - Reports</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="reports.htm">Reports</a> -> Firewall object naming convention</h4>
		<h2>Object names not compliant with naming convention</h2>
			
		<% 
			FWObjectNamesNotCompliant objects_not_compliant =(FWObjectNamesNotCompliant)request.getAttribute("objects_not_compliant");	
			int totalNumberNetworkObjectsNotCompliant = objects_not_compliant.getNetworkObjectGroups().size() + objects_not_compliant.getHosts().size() + objects_not_compliant.getSubnets().size();
			int totalNumberServicesNotCompliant = objects_not_compliant.getServiceGroups().size() + objects_not_compliant.getServices().size();
			int totalNumberUserGroupsNotCompliant = objects_not_compliant.getUserGroups().size();
			int objects_total =(Integer)request.getAttribute("objects_total");
			int service_total =(Integer)request.getAttribute("services_total");
			int profiles_total =(Integer)request.getAttribute("profiles_total");
		%>
		<br>
		<h3>Network Object names not compliant: <%= totalNumberNetworkObjectsNotCompliant %> on <%= objects_total %> <a id="network_objects" href="javascript:void(0)" onClick="displayNotCompliant('network_objects')">(show)</a></h3>
		<div id="network_objects_div" style="display:none;font-size:10px">
		
		<table style="width:98%;word-wrap:break-word;table-layout:fixed;">
		<tr>
			<th style="width:25%;">Object Groups</th>
			<th style="width:25%;">Hosts</th>
			<th style="width:25%;">Subnets</th>
		</tr>
		<%
		int maxObjectLength = Math.max(objects_not_compliant.getNetworkObjectGroups().size(), Math.max(objects_not_compliant.getHosts().size(), objects_not_compliant.getSubnets().size()));
		String obj_grp;
		String host;
		String subnet;
		String zone;
		int obj_grp_length = objects_not_compliant.getNetworkObjectGroups().size();
		int host_length = objects_not_compliant.getHosts().size();
		int subnet_length = objects_not_compliant.getSubnets().size();
		
		for (int i = 0; i < maxObjectLength; i++)
		{
			obj_grp = "";
			host = "";
			subnet = "";
			zone = "";
			
			if (i < obj_grp_length)
			{
				String[] temp = objects_not_compliant.getNetworkObjectGroups().get(i);
				obj_grp = "<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + temp[0] + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">"+temp[1]+"</a>";
			}
			
			if (i < host_length)
			{
				String[] temp = objects_not_compliant.getHosts().get(i);
				host = "<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + temp[0] + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">"+temp[1]+"</a>";
			}
			
			if (i < subnet_length)
			{
				String[] temp = objects_not_compliant.getSubnets().get(i);
				subnet = "<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + temp[0] + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">"+temp[1]+"</a>";
			}			
			
			%>
			<tr>
				<td><%=obj_grp %></td>
				<td><%=host %></td>
				<td><%=subnet %></td>
			</tr>
			<%
		}
		%>
		
		</table>		
		</div>
		
		
		
		
		
		
		<h3>Service names not compliant: <%= totalNumberServicesNotCompliant %> on <%= service_total %>  <a id="services" href="javascript:void(0)" onClick="displayNotCompliant('services')">(show)</a></h3>
		<div id="services_div" style="display:none;font-size:10px">
		<table >
		<tr>
			<th>Service Groups</th>
			<th>Services</th>
		</tr>
		<%
		int maxServiceLength = Math.max(objects_not_compliant.getServiceGroups().size(), objects_not_compliant.getServices().size());
		String service;
		String serviceGroup;
		int service_length = objects_not_compliant.getServices().size();
		int serviceGroup_length = objects_not_compliant.getServiceGroups().size();
		
		for (int i = 0; i < maxServiceLength; i++)
		{
			service = "";
			serviceGroup = "";
			
			if (i < serviceGroup_length)
			{
				String[] temp = objects_not_compliant.getServiceGroups().get(i);
				serviceGroup = "<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + temp[0] + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">"+temp[1]+"</a>";
			}
			
			if (i < service_length)
			{
				String[] temp = objects_not_compliant.getServices().get(i);
				service = "<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + temp[0] + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">"+temp[1]+"</a>";
			}
			
			%>
			<tr>
				<td><%=serviceGroup %></td>
				<td><%=service %></td>
			</tr>
			<%
		}
		%>
		
		</table>		
		
		</div>
		
		
		
		
		
		
		<h3>User Group names not compliant: <%= totalNumberUserGroupsNotCompliant %> on <%= profiles_total %>  <a id="user_groups" href="javascript:void(0)" onClick="displayNotCompliant('user_groups')">(show)</a></h3>
		<div id="user_groups_div" style="display:none;font-size:10px">
		
		<table >
		<tr>
			<th>User Groups</th>
		</tr>
		<%
		String userGroup;
		int userGroup_length = objects_not_compliant.getUserGroups().size();
		
		for (int i = 0; i < userGroup_length; i++)
		{		
			%>
			<tr>
				<td><a href="javascript:void(0)" onClick="window.open('rules.htm?profile_name=<%=objects_not_compliant.getUserGroups().get(i) %>', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%=objects_not_compliant.getUserGroups().get(i) %></a></td>
			</tr>
			<%
		}
		%>
		
		</table>	
		
		</div>
		
		
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>