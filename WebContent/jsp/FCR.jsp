<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.RequestSummary" %>
<%@ page import="be.celsius.fcrweb.objects.RequestTaskSummary" %>
<%@ page import="be.celsius.fcrweb.objects.FCRPageData" %>
<%@ page import="be.celsius.fcrweb.objects.Task" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcr.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>
		<style type="text/css">
	        tr.row_even { background-color: #fafafa; }
	        tr.row_odd  { background-color: white; }
			td          { padding: 4px; vertical-align: top;}
			
			td.id       { text-align: right; }
			td.history  { background: white; }
			td.flag     { padding: 0 2px 0 2px; text-align: center; vertical-align: middle;}
			td.fcr_edition       { background: #FFFFAA; }
			td.ts_validation       { background: #FFDD00; }
			td.fwe_edition       { background: #FFDD00; }
			td.security_validation { background: #FFCC00; }
			td.rejected { background: #FF0000;color:white; }
			td.closure, td.resolved           { background: #99FF00; }
			td.fcr_validation          { background: #FF9900; }
			td.end      { background: #AADD11; }
			td.pending, td.information_required  { background: #CCCCFF; }
			td.validation, td.key_user_validation { background: #FFFF11; }
			td.implement, td.implementation, td.delete, td.Auto_impl_ongoing { background: #FF9900; }
		</style>
		
		<title>FCRWeb - Requests</title>
		
	</head>
	<body onload="getPreviousRequests()">
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/FCR_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> Firewall CR</h4>
		<div id="SearchResult"></div>
		<h2>My Tasks</h2>
		
		<%
		FCRPageData requestsBean=(FCRPageData)request.getAttribute("requests");
		ArrayList<RequestTaskSummary> myTasks = requestsBean.getMyTasks();
		String bonita_url = (String)request.getAttribute("bonita_url");
		int i = 0;
		if (myTasks.size() > 0)
		{
		%>
		<table>
			<tr>
				<th class="first"><strong>ID</strong></th>
				<th>Detail</th>
				<th>Requestor</th>
				<th>Date</th>
				<th>Task</th>
			</tr>
		
		
			<%	
				for (RequestTaskSummary myTask:myTasks)
				{
					if (i%2 == 0)
					{
						%>
						<tr class="row-a">
						<%
					}
					else
					{
						%>
						<tr class="row-b">
						<%
					}	
					String form_type = "fcr-form";
					String link = bonita_url + "/bonita/console/homepage?locale=default&theme=FCR_Web_Form_Validation--1.0#form=FCR_Web_Form_Validation--1.0--Req___Form_Edition$entry&task="+myTask.getTask()+"&mode=app";
					if (myTask.getFormMode().contains("maintain"))
					{
						form_type = "fcr-maintain-form";
					}
					if (myTask.getStatus().equals("FCR Edition") || myTask.getStatus().equals("FWE Edition"))
					{
						link = "/fcrweb/jsp/"+form_type+".jsp?fid="+myTask.getId();
					}
					%>
						<td class="first"><%= myTask.getId() %></td>
						<td><a href="javascript:void(0)" onClick="window.open('/fcrweb/jsp/<%=form_type %>.jsp?fid=<%= myTask.getId() %>', '_blank', 'scrollbars=1,resizable=yes, width=1050, height=700')"><%= myTask.getDetail() %></a></td>
						<td><%= myTask.getRequestor() %></td>
						<td><%= myTask.getDate() %></td>
						<td><a href="javascript:void(0)" onClick="window.open('<%=link%>', '_blank', 'scrollbars=1,resizable=yes, width=1050, height=700')"><%= myTask.getLabelStatus() %></a></td>
					</tr>
					
					<%
					i=i+1;
				}			
			%>
			
		</table>
		<%
		}
		else
		{%>
			<p> You have no tasks </p>
		<%}
			%>

		<h2>My Ongoing Requests</h2>
		<%
		ArrayList<RequestSummary> myOngoings = requestsBean.getMyOngoing();
		i=0;
		if (myOngoings.size() > 0)
		{
		%>
		<table>
			<tr>
				<th></th>
				<th><strong>ID</strong></th>
				<th>Detail</th>
				<th>Requestor</th>
				<th>Date</th>
				<th>Task</th>
			</tr>
			
			
			<%
				for (RequestSummary myOngoing:myOngoings)
				{			
					if (i%2 == 0)
					{
						%>
						<tr class="row-a">
						<%
					}
					else
					{
						%>
						<tr class="row-b">
						<%
					}
					String form_type = "fcr-form";
					try
					{
						if (myOngoing.getFormMode().contains("maintain"))
						{
							form_type = "fcr-maintain-form";
						}
					}
					catch (Exception e)
					{}
					%>
			<td  rowspan="1" onClick="toggle_detail('<%= myOngoing.getId() %>')"><img src="ressources/more.gif" id="img_more_<%= myOngoing.getId() %>" border="0"><img src="ressources/less.gif" id="img_less_<%= myOngoing.getId() %>" border="0" style="display: none;"></td>
			<td><%= myOngoing.getId() %></td>
			<td><a href="javascript:void(0)" onClick="window.open('/fcrweb/jsp/<%=form_type %>.jsp?fid=<%= myOngoing.getId() %>', '_blank', 'scrollbars=1,resizable=yes, width=1050, height=700')" ><%= myOngoing.getDetail() %></a></td>
			<td><%= myOngoing.getRequestor() %></td>
			<td><%= myOngoing.getDate() %></td>
			<td class="<%= myOngoing.getStatus().toLowerCase().replace(' ', '_') %>"><%= myOngoing.getLabelStatus() %></td>			
		</tr>
		<tr style="display: none;" id="detail_<%= myOngoing.getId() %>" class="row_even">
			<td colspan="5" class="history">
				<table class="history">
					<tr>
						<td class="header h_task">Task</td>
						<td class="header h_user">User</td>
						<td class="header h_date">Date</td>
						<td class="header h_com">Comment</td>
					</tr>
	 
	 				<%				
						ArrayList<Task> tasks = myOngoing.getTasks();		
						for (Task task:tasks)
						{
							if (task.getDate() != null)
							{
								if (task.getTask() != "Closure")
								{
									%>
					    			<tr>
							   		 	<td class="<%= task.getTask().toLowerCase().replace(' ', '_') %>"><%= task.getLabelTask() %></td>
					   		 			<td class="history"><%= task.getUser() %></td>
					   		 			<td class="history"><%= task.getDate() %></td>
					   		 			<td class="history comment"><%= task.getComment() %></td>
									</tr>
									<%
								}
								else
								{
									%>
									<tr>
										<td colspan="4" class="end"><b>End</b></td>
									</tr>
									<%
								}
							}
							else
							{
								%>
								<tr>
									<td colspan="4" class="<%= task.getTask().toLowerCase().replace(' ', '_') %>"><b><%= task.getLabelTask() %> still ongoing</b></td>
								</tr>
								<%
							}
						}
					%>
				</table>
			</td>
		</tr>
			
		<%
				i = i+1;
				}
			%>
			
		</table>
		<%
		}
		else
		{%>
			<p> You have no ongoing requests </p>
		<%}
			%>
        <h2>My Previous Requests</h2>
        <div id="PreviousRequests">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your Previous Requests are being loaded ... <img src="ressources/progress.gif"/><br><br></div>
        
	
	
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>