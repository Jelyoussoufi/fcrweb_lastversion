<%@ page language="java" pageEncoding="ISO-8859-1" contentType="text/html;charset=ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/taglib/c.tld" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/taglib/spring.tld" prefix="spring" %>
<%@ page isELIgnored="false" %>
<%@ page import="java.lang.String" %>
<% 
	String contextPath 	= request.getContextPath();
	String path 		= contextPath;
	String basePath 	= request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort() ; // + "/";
	String realPath 	= request.getRealPath("\\");
	String getURL 		= request.getRequestURL().toString();
	String reqURI 		= request.getRequestURI();
	String servletPath 	= request.getServletPath();
	String queryString 	= request.getQueryString();
	String userAgent 	= request.getHeader("user-agent");
	String isFox		= "false";
	if (userAgent.toLowerCase().contains("firefox"))
		isFox = "true";
		
	String form_type = "request";
%>
<jsp:useBean id="environment" class="be.celsius.util.session.Environment" />

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<div id="head">
		
		<c:set scope="session" var="atCelsius" value="${environment.environment==1}" />
		<c:if test="${atCelsius==true}" >
			<input type="hidden" id="contextPath" value="<%= contextPath%>" />
			<input type="hidden" id="atCelsius" value="true" />
			<c:set scope="session" var="path" value="<%= path%>" />
		</c:if>
		<c:if test="${atCelsius!=true}" >
			<input type="hidden" id="contextPath" value="<%= contextPath%>" />
			<base href="<%=basePath%>">
			<input type="hidden" id="atCelsius" value="false" />
			<c:set scope="session" var="path" value="" />
		</c:if>

		<input type="hidden" id="userAgent" value='<%= userAgent%>'/>
		<input type="hidden" id="homePage" value='false'/>
		<input type="hidden" id="trueContextPath" value="<%= contextPath%>" />
		<input type="hidden" id="basePath" value="<%= basePath%>" />
		<input type="hidden" id="servletPath" value="<%= servletPath%>" />
		<input type="hidden" id="realPath" value="<%= realPath%>" />
		<input type="hidden" id="getURL" value="<%= getURL%>" />
		<input type="hidden" id="reqURI" value="<%= reqURI%>" />
		<input type="hidden" id="queryString" value="<%= queryString%>" />
		<!--[if IE]><input type="hidden" id="ie" value="true" /><![endif]-->
		<!--[if lte IE 8]><input type="hidden" id="ieold" value="true" /><![endif]-->
		<input type="hidden" id="ie" value="false" />

		<input type="hidden" id="local" name="local" value="" />
		<input type="hidden" id="form-id" name="fid" value="" />
		<input type="hidden" id="readonly" name="ro" value="" />
		<input type="hidden" id="req" name="req" value="false" />
		<input type="hidden" id="fwe" name="fwe" value="false" />
		<input type="hidden" id="fwo" name="fwo" value="false" />	
		<input type="hidden" id="change-rule" name="change-rule" value="false" />	<!-- change rule incoming data -->	
		<input type="hidden" id="tsrr" name="tsrr" value="false" />	<!-- ts revalidation incoming data -->	
		<input type="hidden" id="user-type" value="" />	<!-- user type simple or advanced -->		
		<input type="hidden" id="form-sel" value="" />	<!-- last form type saved simple or advanced or empty if first time load -->
		<input type="hidden" id="form-selector" value="" />	<!-- current form type simple or advanced or empty if first time load -->
		<input type="hidden" id="form-type" value="" />			<!-- fcr (usual) or maintain -->	
		<input type="hidden" id="conflicts" value="" />
		
		<title>Firewall Change Request Form</title>
		
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/main_form.css" type="text/css" />
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/autosuggest.css" type="text/css" />
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/jsDatePick_ltr.min.css" type="text/css" />
		
		<style type="text/css">
			#main{width:93%;}
			#main-section{width:98%;}
			
			body { font-size: 62.5%;}
			label, input { display:block; }
			input.text { margin-bottom:12px; width:95%; padding: .4em; }
			fieldset { padding:0; border:0; margin-top:25px; }
			h1 { font-size: 1.2em; margin: .6em 0; }
			
			div#users-contain { width: 100%; margin: 0; }
			div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
			div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
			.ui-dialog .ui-state-error { padding: .3em; }
			.validateTips { border: 1px solid transparent; padding: 0.3em; }
			
			.warning-error td
			{
				color : red;
				font-italic : italic;				
				font-weight : bold;				
			}
			
			.warning-conflict td
			{
				color : orange;
				font-italic : italic;				
				font-weight : bold;	
			}
						
			.textarea-img
			{
				position: relative;
				top: 50%;
				vertical-align: top;
				padding:0px;
				margin:0px;
				display:inline;
			}
			
			.img
			{
				padding:0px;
				margin:0px;
				vertical-align:middle;
				align:center;
				display:inline;
			}
			
			.img-clickable
			{
				cursor: pointer;				
			}
			
			.mask_arrow 
			{
			/*
				background-image: url("<%= request.getContextPath()%>/ressources/more.gif");
			*/
			}
			
			.straight-box
			{
				margin:0;
				padding:0;
				border:0px;
			}
			
			.control{display:inline;}
			.control-radio{display:inline; height:10px; line-height:10px;}
			.control-radio-label{display:inline; height:10px; line-height:10px;}
			.control-radio-group{width:300px;padding:0px; margin:0px;}
			
			.control-help{display:none;}
			
			.control-link
			{
				cursor:pointer;
				color:#2180BC;				
			}
			
			.control-link:hover{color:#88AC0B;}
			.control-link:visited{color:#2180BC;}
			
			.group-component{margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;}
			.group-component tr			
			{font-size: 75%;}
					
			.group-component th {width:150px;}
			.group-component .control-text {width:300px;}
			.group-component .small-text{width:200px;}
			.group-component .tiny{width:50px;}
			.group-component select {width:300px;}
			
			input, label {
				padding:2px;
				border:1px solid #eee;
				font: normal 1em Verdana, sans-serif;
				color:#777;
			}
						
			.control-textarea
			{
				width:300px;
				height:100px;
				resize:none;
			}
			
			.mini-textarea
			{
				width:200px;
				height:50px;
				resize:none;
			}
			.tiny-textarea
			{
				width:120px;
				height:30px;
				resize:none;
			}
			
			.tiny
			{
				width:120px;
			}
			
			.repeat-group
			{font-size : 10px;}
			
			.repeat-group tr,
			.repeat-group td
			{
				padding:0px;
				margin:0px;
			}
			
			.repeat-group input,
			.repeat-group label
			{
				display:inline;
				padding:0;
				margin:0;
				border:0px;
			}	
			
			.flow-documentation-ddl
			{
				display:inline;
				height:16px;
				line-height:16px;
			}
			
			/* buttons */
			.group-button{margin-top:10px;}
			.button 
			{
				cursor: pointer; 
				display:inline;
				padding-top:0;
				padding-bottom:0;
				margin-top:0;
				margin-bottom:0;
				
			}
						
			.small-button {width: 50px;}
			.normal-button {width: 120px;}	
			
			.multiple-select-linked-button
			{
				display: inline;
				position: relative;
				top: 15px;
				vertical-align: top;
			}
			
			/* default display mode
			.control-display-editable{display:none;}
			*/
			
			.maskass
			{
				background-color:black;
				position: fixed;
				top: 0;
				left: 0;
				min-width: 100%;
				width: 100%;
				min-height: 100%;
				height: 100%;
				opacity:0.5;
				Z-INDEX: 10; 
				filter: alpha(opacity = 50);
			}
			
			.processing
			{
				background-image: url("<%= request.getContextPath()%>/ressources/processing-1.gif");
                background-position: center center;
                background-repeat: no-repeat;				
				background-color:black;	
				position: fixed;
				top: 0;
				left: 0;
				min-width: 100%;
				width: 100%;
				min-height: 100%;
				height: 100%;
				opacity:0.5;
				Z-INDEX: 19; 
				filter: alpha(opacity = 50);
			}
			
			.maskass-ie8
			{
				background-color:black;
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 800px;
				opacity:0.5;
				margin: 0px;
				padding: 0px;
				Z-INDEX: 10; 
				filter: alpha(opacity = 50);
			}
			
			.control-dialog
			{
				background:white;
				border: thin #93BC0C solid;
				border-radius : 5px;
				position: absolute;
				top: 15%;
				left: 20%;
				Z-INDEX: 11; 		
				width:675px;
			}
			
			.control-list-member
			{
				background:white;
				border: thin #93BC0C solid;
				border-radius : 5px;
				position: absolute;
				top: 25%;
				left: 25%;
				Z-INDEX: 12; 		
				width:400px;
			}
			
			option .control-option-disabled,
			.control-option-disabled
			{
				color : black;
			}
			
			#display-group-members{}
			
			.autosuggest-body{Z-INDEX:12;}
			
			.control-dialog .group-button
			{float:right;}
		</style>
		
		<!--[if IE]><script LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></script><![endif]-->
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jsDatePick.min.1.3.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/autosuggest.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/util_function.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/util_html.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_form.js"></SCRIPT>		
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_objects.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_maintain.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_maintain_object.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_form_util.js"></SCRIPT>		
		<!--[<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jquery-2.0.3.js"></SCRIPT>-->
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jquery.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jq.js"></SCRIPT>
		
	</head>
	<body id="body" onload="load('<%= form_type%>');">
		
		<div id="wrap">
			<img src="<%= request.getContextPath()%>/ressources/content_forms.jpg" class="bgcontent">
			<div id="header">				
				<img src="<%= request.getContextPath()%>/ressources/header.jpg" class="bgheader">
			</div>
			<div id="content-wrap">				
				<div id="main">
					<div class="maskass" style="display:none" ></div>
					<div class="processing" style="display:none" ></div>
					<h2><span onclick="checkDebugTool();">FCR&nbsp;<span id="title-form-id" /></span>&nbsp;<span id="form-title"></span></h2>					
					<form id="main-form" action="#">
						<div id="display-group-members" class="control-dialog" style="display:none;width:350px;">
							<div id="groups-members-list" style="margin:10px;width:100%;height:100%;">
							</div>
							<table class="group-component group-button">
								<tr>
									<td>
										<input type="button" class="button control-button small-button" value="Close" onclick="hide('display-group-members');mask_off();">
									</td>
								</tr>
							</table>
						</div>
						<div id="main-section">							
							<div id="admin-info" class="group-component">
								<h3 onclick="">Administrative information</h3>
								<table>
									<tr>
										<th>Requestor</th>
										<td>
											<input id="requestor" class="control control-readonly control-text" type="text" value="" />										
										</td>
									</tr>
									<tr id="warning-error-tr-requestor" class="warning-error" style="display:none">
										<td colspan="2">Error occurs : requestor is undefined.</td>
									</tr>								
									<tr id="tr-on-behalf-of" class="advanced-form-fields">
										<th>On behalf of</th>
										<td>
											<input id="on-behalf-of" class="on-behalf-of control control-text control-editable" type="text" value="" autocomplete="off" />
											<img id="help-on-behalf-of"	src="<%= request.getContextPath()%>/ressources/information.png"
													class="img control-help" title="help me"/>
										</td>
									</tr>
									<tr id="warning-error-tr-on-behalf-of" class="warning-error" style="display:none">
										<td colspan="2">Missing or incorrect value</td>
									</tr>
									<tr>
										<th>Application or Service<font color="red">&nbsp;*</font></th>
										<td><input id="application-service" class="application-service control control-text control-editable" 
											type="text" value="" 
											style="display:inline;"
											autocomplete="off"/>
											<img id="help-application-service"	src="<%= request.getContextPath()%>/ressources/information.png"
											class="img control-help" title=""/>
											<span id="edit-application-service-button" style="display:none">
												<img 	style="display:inline" class="img img-clickable control-display-editable" 
														src="<%= request.getContextPath()%>/ressources/document_edit.png" 
														title="edit application service" 														
														onclick="setChangeApplicationService()" />
											</span>
											<span id="edit-application-service-button-mtn" style="display:none">
												<img 	style="display:inline" class="img img-clickable control-display-editable" 
														src="<%= request.getContextPath()%>/ressources/document_edit.png" 
														title="edit application service" 														
														onclick="setChangeApplicationService_mtn()" />
											</span>
										</td>									
									</tr>
									<tr id="warning-error-tr-application-service" class="warning-error" style="display:none">
										<td colspan="2">Missing value or incorrect value</td>
									</tr>
									<tr>
										<th>Request Context<font color="red">&nbsp;*</font></th>
										<td>
											<select id="request-context" class="request-context control control-editable control-select" onchange="get_list_context_detail()">
												<option selected value="">[Select ...]</option>
												<option value="Project">Project</option>
												<option value="CTR">CTR</option>
												<option value="Remote Access">Remote Access</option>
												<option value="Other">Other</option>
											</select>
											<img id="help-request-context" src="<%= request.getContextPath()%>/ressources/information.png"
											class="img control-help" title=""></img>
										</td>
									</tr>
									<tr id="warning-error-tr-request-context" class="warning-error warning-error-tr-request-context" style="display:none">
										<td colspan="2">Missing Value</td>
									</tr>
									<tr>
										<th>Context detail<font color="red">&nbsp;*</font></th>
										<td>
											<input id="context-detail" class="control control-text control-editable" type="text" value="" autocomplete="off" />
											<img id="help-context-detail" src="<%= request.getContextPath()%>/ressources/information.png"
												class="img control-help" style="vertical-align:middle;" title=""></img>
										</td>
									</tr>
									<tr id="warning-error-tr-context-detail" class="warning-error" style="display:none">
										<td colspan="2">Missing or incorrect value</td>
									</tr>
									<tr class="advanced-form-fields">
										<th>Detailed reason for this request</th>
										<td>
											<textarea id="detailed-reason-request" class="control control-editable control-textarea"></textarea>
											<img id="help-detailed-reason-request" src="<%= request.getContextPath()%>/ressources/information.png"
											class="control-help textarea-img" title=""></img>
										</td>
									</tr>
									<tr id="simple-form" style="display:none">
										<th><a name="simple_edition"></a>Flow details<font color="red">&nbsp;*</font></td>
										<td>
											<textarea id="flow-details" class="control control-editable control-textarea" ></textarea>											
										</td>
										<td>
											<img id="help-flow-details" src="<%= request.getContextPath()%>/ressources/information.png" class="textarea-img control-help" title=""></img>
										</td>
										<td>
											<h3 id="advanced-edition-link" style="display:none"><span class="control-link control-display-editable" onclick="switchFormEdition('advanced', false);">(Advanced Edition)</span></h3>
										</td>
									</tr>
									<tr id="warning-error-tr-flow-details" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr id="flow-documentation-upload">
										<th style="white-space:nowrap;">Flow&nbsp;documentation</th>
										<td>
											<form id='pingping' method="post" enctype="multipart/form-data" action="<%= path%>/document-submit.htm">			
												<span style="display:none">
													<input id="flow-documentation" name="flow-documentation" class="control control-file control-file-upload" type="file" />
													<input id="flow-documentation-request-id" name="flow-documentation-request-id" type="text" value=""/>
													<input  type="submit" value="Submit"/>
													<input type="button" value="submitFlowDocumentation" onclick="submitFlowDocumentation()"/>
													<input type="button" value="submitDocument" onclick="submitDocument()"/>
													<input type="hidden" value="DEPRECATED"/>
												</span>
											</form>
											<form id='pingForm' method="post" enctype="multipart/form-data" action="<%= path%>/document-submit.htm">
												<input class="control control-file control-file-upload" id="myFile" name="myFile" type="file"/>
												<img id="help-flow-documentation" src="<%= request.getContextPath()%>/ressources/information.png"
													class="control-help textarea-img" title=""></img>
												<input style="display:none" id="submit-flow-documentation" type="submit" value="Submit"/>
											</form>
										</td>
									</tr>
									<tr id="warning-error-tr-flow-documentation" class="warning-error" style="display:none">
										<td colspan="2">The document size must be smaller than 10 Mb</td>
									</tr>
									<tr id="flow-documentation-download" class="control-file control-file-download" style="display:none">
										<th style="white-space:nowrap;">Flow documentation</th>
										<td>
											<span id="flow-documentation-file" name="flow-documentation-file" class="flow-documentation-ddl" style="height:16px;line-height:16px;">
												<label id="flow-documentation-name" style="display:inline;cursor:pointer;color:blue;" onclick="getFlowDocumentation()"></label>
												<img id="delete-document-button" class="img img-clickable control-display-editable" 
													title="delete file"	style="display:inline;" 
													src="<%= request.getContextPath()%>/ressources/document_delete.png" 
													 onclick="deleteFlowDocumentation()" ></img>											
											</span>								
										</td>
									</tr>
								</table>							
							</div>
							<div id="">								
								<div id="advanced-form" style="display:none">							
									<a name="advanced_edition"></a>
									<h3>Technical information&nbsp;<span id="link-advanced-form" class="control-link control-display-editable" onclick="switchFormEdition('simple', false);">(Simple Edition)</span></h3>

									<div id="users-contain">
										<div style="width:100%;height:230px;overflow:auto;">
											<table id="users" class="repeat-group">
												<thead>
													<tr>
														<th width="4%" style="font-size:12px;" class="control-display-change-rule"></th>
														<th width="3%" class="control-display-fwo"></th>
														<th width="8%" style="font-size:12px;" class="control-display-change-rule">Rule</th>
														<th width="20%" style="font-size:12px;">Source</th>
														<th width="20%" style="font-size:12px;">Destination</th>
														<th width="12%" style="font-size:12px;">Service</th>														
														<th width="8%" style="font-size:12px;">Action</th>
														<th width="15%" style="font-size:12px;">Description</th>
														<th width="5%" class="control-display-editable "></th>
														<th width="3%" class="control-display-readonly "></th>
													</tr>
												</thead>
												<tbody id="list-flows"></tbody>
											</table>
										</div>
										<table style="display:none">
											<tbody id="flow-template">											
												<tr id="row-template" class="repeat-group repeat-group-row">
													<td class="control-display-change-rule">
														<label id="template-fwo-action"></label>
													</td>
													<td class="control-display-fwo">
														<input id="template-flag" type="checkbox" style="margin:0px;padding:0px;" onchange="flag(idx);">
													</td>
													<td class="control-display-change-rule">
														<table>
															<tbody id="template-rule-refs">
															</tbody>
														</table>
													</td>
													<td><label id="template-source" class=""></label></td>
													<td><label id="template-destination" class="" ></label></td>
													<td><label id="template-service" class="" ></label></td>													
													<td><label id="template-action" class=""></label></td>
													<td><label id="template-description" class=""></label></td>
													<td class="control-display-editable " style="margin:0;padding:0;border:0;">
														<table>
															<tr>
																<td style="margin:0;padding:0;border:0;text-align:center;">
																	<img 	src="<%= request.getContextPath()%>/ressources/document_edit.png"
																			class="img img-clickable control-display-editable" 
																			style="display:inline;" title="Edit flow" onclick="flow.load(idx)" />
																</td>
																<td style="margin:0;padding:0;border:0;text-align:center;">
																	<img src="<%= request.getContextPath()%>/ressources/document_delete.png" 
																		class="img img-clickable control-display-editable" 
																		style="display:inline;" title="Delete flow" 
																		onclick="flow.remove(idx)" />
																</td>
															</tr>	
														</table>
													</td>
													<td class="control-display-readonly ">
														<img src="<%= request.getContextPath()%>/ressources/document_view.png" 
															class="img img-clickable control-display-readonly" 
															style="display:inline;" title="Visualize flow" onclick="flow.load(idx)" />												
													</td>													
												</tr>
											</tbody>
										</table>
										<div>
											<table style="display:none">
												<tbody id="rule-refs">
													<tr id="rule-refs-template-rule-ridx">													
														<td class="control-display-fwo" style="padding:0;margin:0;border:0px;display:none;">
															<img src="<%= request.getContextPath()%>/ressources/navigate_plus.png" 
																class="img img-clickable" 
																style="display:inline;" title="Visualize flow" onclick="flow.add_rule(idx)" />	
														</td>
														<td class="control-display-fwo" style="padding:0;margin:0;border:0px;display:none;">
															<img src="<%= request.getContextPath()%>/ressources/navigate_minus.png" 
																class="img img-clickable f-ctrl-icon-rule-idx-ridx" 
																style="display:inline;" title="Visualize flow" onclick="flow.remove_rule(idx,ridx)" />	
														</td>
														<td style="padding:0;margin:0;border:0px;">
															<select id="template-rule-ridx-fw" 
																	class="control control-select flat f-ctrl-rule-idx-ridx" 
																	style="display:inline;width:55px;padding:0;margin:0;border:0px;font-size:75%;"
																	onchange="get_fw_rule_id('idx',ridx);">
																<option value="">[...]</option>
																<option value="BOFE">BOFE</option>
																<option value="CRPA">CRPA</option>
																<option value="FIKI">FIKI</option>
																<option value="HASO">HASO</option>
																<option value="KYKA">KYKA</option>
																<option value="LULO">LULO</option>
																<option value="MOTA">MOTA</option>
																<option value="SOBU">SOBU</option>
																<option value="BUSO">BUSO</option>
																<option value="TAGE">TAGE</option>
															</select>
														</td>
														<td style="padding:0;margin:0;border:0px;">
															<input id="template-rule-ridx-rule-id" 
																	class="control control-text flat f-ctrl-rule-idx-ridx" 
																	style="display:inline;width:50px;font-size:75%;padding:0;margin:0;border:0px;" 
																	value="" onkeyup="updateFlowRuleId('idx',ridx)" />														
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<table id="conflicts-div" class="group-component" style="display:none">
												<tr>
													<th>Flow(s) conflict(s)</th>
												</tr>
											</thead>
											<tbody id="conflicts-list"></tbody>
										</table>
										<table class="group-component">	
											<tr id="warning-error-tr-aefs" class="warning-error" style="display:none">
											<td colspan="2">No flow(s) detected</td>
										</tr>
											<tr id="advanced-control-buttons"  class="control-display-editable">
												<td>
													<input type="button" class="button control-button normal-button" value="Add Flow" onclick="flow.create();">
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<table class="group-component group-button">
								<tr id="edition-control-buttons" class="control-display-editable">
									<td colspan="2">
										<input type="button" class="button control-button small-button" value="Submit" onclick="button_submitForm();">
										<input type="button" class="button control-button small-button" value="Drop" onclick="button_dropForm();">
										<input type="button" class="button control-button small-button" value="Save" onclick="button_saveForm();">
									</td>
								</tr>
								<tr id="edition-control-buttons" class="control-display-fwo">
									<td colspan="2">
										<input type="button" class="button control-button control-display-fwo" style="display:none" value="Update rule refs" onclick="button_update_rule_refs();">
									</td>
								</tr>
								<tr id="read-control-buttons" class="control-display-readonly">
									<td colspan="2">
										<input style="display:none" type="button" class="button control-button small-button" value="Close" onclick="button_closeForm()">
									</td>
								</tr>
							</table>
						</div>
					</form>
					<form id="flow-form" action="#">
						<div id="flow-section" style="display:none">
							<table class="group-component">
								<tr>
									<th>Flow Description<font color="red">&nbsp;*</font></th>
									<td>
										<textarea id="flow-description" class="control control-editable control-textarea rule-flow"></textarea>
									</td>
									<td>
										<img id="help-flow-description" src="<%= request.getContextPath()%>/ressources/information.png" class="textarea-img control-help" style="display:none;" title=""/>
									</td>
								</tr>
								<tr id="warning-error-tr-flow-description" class="warning-error flow-warnings" style="display:none">
									<td colspan="2">Missing value</td>
								</tr>
								<tr>
									<th>Type<font color="red">&nbsp;*</font></th>
									<td style="width:300px;">
										<table style="display:inline;width:100%;" class="control-radio-group">
											<tr>
												<td>
													<input id="H2H" type="radio" class="control-radio control-editable dataflow-type rule-flow" name="dataflow-type" value="Host 2 Host" onclick="changeDataflowType()"/>&nbsp;
													<label class="control-radio-label" for="H2H">Host to host dataflow</label>
												</td>												
											</tr>
											<tr>
												<td>
													<input id="U2H" type="radio" class="control-radio control-editable dataflow-type rule-flow" name="dataflow-type" value="User 2 Host" onclick="changeDataflowType()"/>&nbsp;
													<label class="control-radio-label" for="U2H">User authenticated dataflow</label>
												</td>
											</tr>
										</table>										
									</td>
									<td>
										<img id="help-dataflow-type" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
										<span id="edit-dataflow-type-button" class="flow-edit-icon" style="display:none">
											<img 	style="display:inline" class="img img-clickable control-display-editable" 
													src="<%= request.getContextPath()%>/ressources/document_edit.png" 
													title="edit dataflow type" 														
													onclick="setChangeDataflowType()" />
										</span>
									</td>
								</tr>
							</table>
							<table class="group-component profile-section">
								<tr class="profile-def">
									<th>User group<font color="red">&nbsp;*</font></th>
									<td>
										<input id="profile" class="control control-editable control-text profile rule-flow" type="text" />
										<img id="help-profile" src="<%= request.getContextPath()%>/ressources/information.png"
										class="img control-help" style="display:none;" title=""/>
										<img id="create-profile" src="<%= request.getContextPath()%>/ressources/document_new.png"
										class="img control-create control-create-profile" style="display:none;" title="this user group will be created"/>
										<span id="edit-profile-button" class="flow-edit-icon" style="display:none">
											<img 	style="display:inline" class="img img-clickable control-display-editable" 
													src="<%= request.getContextPath()%>/ressources/document_edit.png" 
													title="edit dataflow type" 														
													onclick="setChangeProfile()" />
										</span>
									</td>
								</tr>
								<tr id="warning-error-tr-profile" class="warning-error profile-error flow-warnings" style="display:none">
									<td colspan="2">Missing value</td>
								</tr>
								<tr class="profile-def profile-def-elem">
									<th>Owner<font color="red">&nbsp;*</font></th>
									<td>
										<input id="profile-owner" class="control control-editable control-text control-profile-autocomplete ldap-user rule-flow" type="text" />
										<img id="help-profile-owner" src="<%= request.getContextPath()%>/ressources/information.png"
										class="img control-help" style="display:none;" title=""/>
									</td>
								</tr>
								<tr id="warning-error-tr-profile-owner" class="warning-error profile-error flow-warnings" style="display:none">
									<td colspan="2">Missing or Incorrect value</td>
								</tr>								
								<tr class="profile-def profile-def-elem">
									<th>Description<font color="red">&nbsp;*</font></th>
									<td>
										<input id="profile-description" class="control control-editable control-text control-profile-autocomplete rule-flow" type="text" />
										<img id="help-profile-description" src="<%= request.getContextPath()%>/ressources/information.png"
										class="img control-help" style="display:none;" title=""/>
									</td>
								</tr>
								<tr id="warning-error-tr-profile-description" class="warning-error profile-error flow-warnings" style="display:none">
									<td colspan="2">Missing value</td>
								</tr>
							</table>
							<table class="group-component">
								<tr>
									<th>Action<font color="red">&nbsp;*</font></th>
									<td>
										<select id="flow-action" style="display:inline;" class="flow-action control control-select control-editable rule-flow">
											<option class="" value="">[Select ...]</option>
											<option id="flow-action-accept" class="host2host" value="Accept" style="display:none">Accept</option>
											<option id="flow-action-reject" class="host2host" value="Deny" style="display:none">Deny</option>
											<option id="flow-action-auth" class="user2host" value="User authentication" style="display:none">User authentication</option>
											<option id="flow-action-vpn"class="user2host" value="Encryption (VPN)" style="display:none">Encryption (VPN)</option>
										</select>
										<img id="help-flow-action" src="<%= request.getContextPath()%>/ressources/information.png"
										class="img control-help" style="display:none;" title=""/>
										<span id="edit-flow-action-button" class="flow-edit-icon" style="display:none">
											<img 	style="display:inline" class="img img-clickable control-display-editable" 
													src="<%= request.getContextPath()%>/ressources/document_edit.png" 
													title="edit flow action" 														
													onclick="setChangeFlowAction()" />
										</span>
									</td>
								</tr>
								<tr id="warning-error-tr-flow-action" class="warning-error warning-error-tr-flow-action flow-warnings" style="display:none">
									<td colspan="2">Missing value</td>
								</tr>
								<tr class="control-output output-source">
									<th>Source(s)<font color="red">&nbsp;*</font></th>
									<td>
										<select id="output-source" class="control control-select" multiple size=3 onchange="source.show_add_buttons();"></select>
										<span class="control-display-editable multiple-select-linked-button">
											<input type="button" class="button control-button small-button" value="Add" onclick="source.open_add();" >
											<input type="button" class="button control-button small-button output-edit source-edit" value="Edit" onclick="source.open_edit('edit');" >
											<input type="button" class="button control-button small-button output-delete source-delete" value="Delete" onclick="source.remove();" >
										</span>
										<span class="control-display-readonly multiple-select-linked-button">
											<input type="button" class="button control-button small-button" value="View" onclick="source.open_edit('display');" >
										</span>
									</td>
									<td>
										<img id="help-output-source" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>										
									</td>
								</tr>
								<tr id="warning-error-tr-output-source" class="warning-error flow-warnings" style="display:none">
									<td colspan="2">Missing source(s)</td>
								</tr>
								<tr class="control-output output-destination">
									<th>Destination(s)<font color="red">&nbsp;*</font></th>
									<td>
										<select id="output-destination" class="control control-select" multiple size=3 onchange="destination.show_add_buttons();"></select>
										<span class="control-display-editable multiple-select-linked-button">
											<input type="button" class="button control-button small-button" value="Add" onclick="destination.open_add();" >
											<input type="button" class="button control-button small-button output-edit destination-edit" value="Edit" onclick="destination.open_edit('edit');" >
											<input type="button" class="button control-button small-button  output-delete destination-delete" value="Delete" onclick="destination.remove();" >
										</span>
										<span class="control-display-readonly multiple-select-linked-button">
											<input type="button" class="button control-button small-button" value="View" onclick="destination.open_edit('display');" >
										</span>
									</td>
									<td>
										<img id="help-output-destination" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>										
									</td>
								</tr>
								<tr id="warning-error-tr-output-destination" class="warning-error flow-warnings" style="display:none">
									<td colspan="2">Missing destination(s)</td>
								</tr>
								<tr class="control-output output-service">
									<th>Service<font color="red">&nbsp;*</font></th>
									<td>
										<select id="output-service" class="control control-select" multiple size=3 onchange="service.show_add_buttons();"></select>
										<span class="control-display-editable multiple-select-linked-button" >
											<input type="button" class="button control-button small-button" value="Add" onclick="service.open_add();" >
											<input type="button" class="button control-button small-button output-edit service-edit" value="Edit" onclick="service.open_edit('edit');" >
											<input type="button" class="button control-button small-button  output-delete service-delete" value="Delete" onclick="service.remove();" >
										</span>
										<span class="control-display-readonly multiple-select-linked-button">
											<input type="button" class="button control-button small-button" value="View" onclick="service.open_edit('display');" >
										</span>
									</td>
									<td>
										<img id="help-output-service" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>										
									</td>
								</tr>
								<tr id="warning-error-tr-output-service" class="warning-error flow-warnings" style="display:none">
									<td colspan="2">Missing service(s)</td>
								</tr>	
								<tr>
									<th>End Date</th>
									<td>
										<input id="end-date" class="control control-editable control-text" type="text" />
										<img id="help-end-date" src="<%= request.getContextPath()%>/ressources/information.png"
										class="img control-help" style="display:none;" title=""/>
									</td>
								</tr>
								<tr id="warning-error-tr-end-date" class="warning-error flow-warnings" style="display:none">
									<td colspan="2">Incorrect (futur) date value (format:[dd/mm/yyyy] e.g:31/12/2013)</td>
								</tr>								
							</table>
							<div class="maskass-ie8" style="display:none" ></div>
							<div class="processing-ie8" style="display:none" ></div>
							<div id="source-dialog" class="control-dialog" style="display:none">
								<h2 id="source-dialog-header"></h2>
								<table id="source-network-access-origin" class="group-component" style="display:none;">
									<tr id="tr-src-nao-auth" class="src-rows src-nao-auth">
										<th>Network Access Origin<font color="red">&nbsp;*</font></th>
										<td>
											<select id="src-nao-auth" class="control control-select control-editable"></select>
											<img id="help-src-nao-auth" src="<%= request.getContextPath()%>/ressources/information.png"
											class="img control-help" style="display:none;" title=""/>
										</td>
									</tr>
									<tr id="warning-error-tr-src-nao-auth" class="warning-error src-nao-errors" style="display:none">
										<td id="warning-error-td-src-nao-auth" colspan="2">Missing value or incorrect value</td>
									</tr>
									<tr id="tr-src-nao-enc" class="src-rows src-nao-enc">
										<th>Network Access Origin<font color="red">&nbsp;*</font></th>
										<td>
											<select id="src-nao-enc" class="control control-select control-editable"></select>
											<img id="help-src-nao-enc" src="<%= request.getContextPath()%>/ressources/information.png"
											class="img control-help" style="display:none;" title=""/>
										</td>
									</tr>
									<tr id="warning-error-tr-src-nao-enc" class="warning-error src-nao-errors" style="display:none">
										<td id="warning-error-td-src-nao-enc" colspan="2">Missing value or incorrect value</td>
									</tr>
								</table>
								<table id="source-definition" class="group-component" style="width:95%">								
									<tr>
										<th>Organization<font color="red">&nbsp;*</font></th>
										<td>
											<select id="src-org" class="src-org control control-select control-editable" onchange="source.changeOrganization();">
												<option value="">[Select ...]</option>
												<option value="MOBISTAR">MOBISTAR</option>
												<option value="OLU">OLU</option>
												<option value="MES">MES</option>
												<option value="EXTERNAL">EXTERNAL</option>
											</select>
											<img id="help-src-org" src="<%= request.getContextPath()%>/ressources/information.png"
											class="img control-help" style="display:none;" title=""/>	
										</td>
									</tr>
									<tr id="warning-error-tr-src-org" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr class="src-rows src-primary-rows">
										<th>Category<font color="red">&nbsp;*</font></th>
										<td>
											<select id="src-cat" class="src-cat control control-select control-editable" onchange="source.changeCategory();">
												<option value="">[Select ...]</option>
												<option value="Host">Host</option>
												<option value="Subnet">Subnet</option>
												<option value="Group of objects">Group of objects</option>
											</select>
											<img id="help-src-cat" src="<%= request.getContextPath()%>/ressources/information.png"
											class="img control-help" style="display:none;" title=""/>	
										</td>
									</tr>
									<tr id="warning-error-tr-src-cat" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr class="src-rows src-goo-rows">
										<th>Group of objects<font color="red">&nbsp;*</font></th>
										<td>
											<input type="text" id="src-goo" class="control control-text control-editable flow-control-action">
											<img id="help-src-goo" src="<%= request.getContextPath()%>/ressources/information.png"
											class="img control-help" style="display:none;" title=""/>	
											<img id="create-src-goo" src="<%= request.getContextPath()%>/ressources/document_new.png"
											class="img control-create control-create-goo" style="display:none;" title="this group of objects will be created"/>
										</td>
									</tr>									
									<tr id="warning-error-tr-src-goo" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr class="src-rows src-goo-rows">
										<th>Environment<font class="src-mandatory" color="red">&nbsp;*</font></th>
										<td>
											<select id="src-goo-env" class="control control-select control-editable src-autocomplete-rows" onchange="source.changeEnv('goo-env');">
												<option value="" selected>[Select ...]</option>
												<option value="Prod" selected>Production</option>
												<option value="nonProd">Non production</option>
											</select>
											<img id="help-src-goo-env" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>	
										</td>
									</tr>
									<tr id="warning-error-tr-src-goo-env" class="warning-error" style="display:none">
										<td id="warning-error-td-src-goo-env" colspan="2">Missing value</td>
									</tr>
									<tr>
										<td colspan="2" class="straight-box">
											<div class="src-group-element-wrapper group-element-wrapper" style="max-height:400px;overflow-y:auto;overflow:auto">
												<table class="straight-box">
													<tr class="src-rows src-goo-selection-rows">
														<th colspan="2" style="text-align:center;">Host(s) selection</th>
													</tr>									
													<tr class="src-rows src-host-rows src-goo-selection-rows">
														<th>Host name<font class="src-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<input type="text" id="src-host" class="control control-text control-editable fixed-autosuggest flow-control-action">
															<img id="help-src-host" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
															<img id="create-src-host" src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-host" style="display:none;" title="this host will be created"/>
														</td>
													</tr>
													<tr id="warning-error-tr-src-host" class="warning-error" style="display:none">
														<td id="warning-error-td-src-host" colspan="2">Missing value or incorrect value</td>
													</tr>
													<tr id="warning-error-tr-src-host-opt" class="warning-error warning-error-tr-src-host" style="display:none">
														<td id="warning-error-td-src-host-opt" colspan="2"></td>
													</tr>
													<tr class="src-rows src-host-rows src-goo-selection-rows">
														<th>Environment<font class="src-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<select id="src-host-env" class="control control-select control-editable src-autocomplete-rows" onchange="source.changeEnv('host-env');">
																<option value="" selected>[Select ...]</option>
																<option value="Prod" selected>Production</option>
																<option value="nonProd">Non production</option>
															</select>
															<img id="help-src-host-env" src="<%= request.getContextPath()%>/ressources/information.png"
															class="img control-help" style="display:none;" title=""/>	
														</td>
													</tr>
													<tr id="warning-error-tr-src-host-env" class="warning-error" style="display:none">
														<td id="warning-error-td-src-host-env" colspan="2">Missing value</td>
													</tr>
													<tr class="src-rows src-host-rows src-goo-selection-rows">
														<th>Ip address<font class="src-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<input type="text" id="src-ip" class="control control-text control-editable flow-control-action" style="display:inline;">
															<img id="help-src-ip" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>	
															<img id="src-ip-load" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/loader.gif" title="load" />
															<img id="src-ip-check" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/check_16px.png" title="check" />
															<img id="src-ip-error" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/error_16px.png" title="error" />
															<img id="src-ip-warning" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/icon-warning.gif" title="warning" />
															<input id="src-ip-code" type="hidden" value="" />
														</td>					
													</tr>
													<tr id="warning-error-tr-src-ip" class="warning-error" style="display:none">
														<td id="warning-error-td-src-ip" colspan="2">Missing value or incorrect ip address</td>
													</tr>
													<tr id="warning-error-tr-src-ip-opt" class="warning-error warning-error-td-src-ip" style="display:none">
														<td id="warning-error-td-src-ip-opt" colspan="2"></td>
													</tr>
													<tr class="src-rows src-host-rows src-goo-selection-rows src-autocomplete-rows">
														<th>Translated ip</th>
														<td>
															<input type="text" id="src-ip-tsl" class="control control-text control-editable src-autocomplete-rows">
															<img id="help-src-ip-tsl" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
														</td>
													</tr>
													<tr id="warning-error-tr-src-ip-tsl" class="warning-error" style="display:none">
														<td colspan="2">Missing value or incorrect ip value</td>
													</tr>
													<tr id="tr-src-host-area" class="src-rows src-host-rows src-goo-selection-rows src-autocomplete-rows">
														<th>Area</th>
														<td>
															<input type="text" id="src-host-area" class="control control-text control-readonly src-autocomplete-rows">											
														</td>
													</tr>
													<tr class="src-rows src-goo-selection-rows">
														<th>Hosts<font class="src-group-mandatory" color="yellow">&nbsp;*</font></th>
														<td>
															<select id="src-hosts" multiple size="3" class="src-hosts control control-select control-editable"></select>
															<span class="control-display-editable multiple-select-linked-button" >
																<input type="button" class="button control-button small-button" value="Add" onclick="source.sel_add('host');" >
																<input type="button" class="button control-button small-button" value="Delete" onclick="source.sel_rem('host');" >
															</span>
														</td>
													</tr>
													<tr class="warning-error warning-error-tr-src-hosts warning-error-tr-src-subnets warning-error-tr-src-goos" style="display:none">
														<td colspan="2">No host(s) selected nor created</td>
													</tr>
													<tr class="src-rows src-goo-selection-rows">
														<th colspan="2" style="text-align:center;">Subnet(s) selection</th>
													</tr>
													<tr class="src-rows src-subnet-rows src-goo-selection-rows">
														<th>Subnet<font class="src-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<input type="text" id="src-subnet" class="control control-text control-editable flow-control-action" style="display:inline;">
															<img id="help-src-subnet" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
															<img id="create-src-subnet" src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-subnet" style="display:none;" title="this subnet will be created"/>
															<img id="src-subnet-load" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/loader.gif" title="load" />
															<img id="src-subnet-check" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/check_16px.png" title="check" />
															<img id="src-subnet-error" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/error_16px.png" title="error" />
															<img id="src-subnet-warning" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/icon-warning.gif" title="warning" />
															<input id="src-subnet-code" type="hidden" value="" />
														</td>
													</tr>
													<tr id="warning-error-tr-src-subnet" class="warning-error warning-error-tr-src-subnet" style="display:none">
														<td id="warning-error-td-src-subnet" colspan="2">Missing value or incorrect value ([subnet-ip]/[mask] e.g: 0.0.0.0/0)</td>
													</tr>
													<tr id="warning-error-tr-src-subnet-opt" class="warning-error warning-error-tr-src-subnet" style="display:none">
														<td id="warning-error-td-src-subnet-opt" colspan="2"></td>
													</tr>
													<tr id="tr-src-subnet-area"  class="src-rows src-subnet-rows src-goo-selection-rows">
														<th>Area</th>
														<td>
															<input type="text" id="src-subnet-area" class="control control-text control-readonly src-autocomplete-rows flow-control-action">
														</td>
													</tr>									
													<tr class="src-rows src-goo-selection-rows">
														<th>Subnets<font class="src-group-mandatory" color="yellow">&nbsp;*</font></th>
														<td>
															<select id="src-subnets" multiple size="3" class="src-subnets control control-select control-editable"></select>
															<span class="control-display-editable multiple-select-linked-button" >
																<input type="button" class="button control-button small-button" value="Add" onclick="source.sel_add('subnet');" >
																<input type="button" class="button control-button small-button" value="Delete" onclick="source.sel_rem('subnet');" >
															</span>
														</td>
													</tr>
													<tr class="warning-error warning-error-tr-src-hosts warning-error-tr-src-subnets warning-error-tr-src-goos" style="display:none">
														<td colspan="2">No subnet(s) selected nor created</td>
													</tr>
													<tr class="src-rows src-goo-selection-rows">
														<th colspan="2" style="text-align:center;">Group(s) of objects selection</th>
													</tr>
													<tr class="src-rows src-goo-selection-rows">
														<th>Group</th>
														<td>
															<input type="text" id="src-goo-selector" class="control control-text control-editable flow-control-action">
															<img id="help-src-goo-sel" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
														</td>
													</tr>
													<tr id="warning-error-tr-src-goo-selector" class="warning-error" style="display:none">
														<td colspan="2">Incorrect value</td>
													</tr>
													<tr class="src-rows src-goo-selection-rows">
														<th>Environment<font class="src-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<select id="src-goo-sel-env" class="control control-select control-readonly" onchange="source.changeEnv('goo-sel-env');">
																<option value="" selected>[Select ...]</option>
																<option value="Prod" selected>Production</option>
																<option value="nonProd">Non production</option>
															</select>
															<img id="help-src-goo-sel-env" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
														</td>
													</tr>
													<tr id="warning-error-tr-src-goo-sel-env" class="warning-error" style="display:none">
														<td id="warning-error-td-src-goo-sel-env" colspan="2">Missing value</td>
													</tr>
													<tr class="src-rows src-goo-selection-rows">
														<th>Groups of objects<font class="src-group-mandatory" color="yellow">&nbsp;*</font></th>
														<td>
															<select id="src-goos" multiple size="3" class="src-goos control control-select control-editable"></select>
															<span class="control-display-editable multiple-select-linked-button" >
																<input type="button" class="button control-button small-button" value="Add" onclick="source.sel_add('goo-selector');" >
																<input type="button" class="button control-button small-button" value="Delete" onclick="source.sel_rem('goo-selector');" >
															</span>
														</td>
													</tr>
													<tr class="warning-error warning-error-tr-src-hosts warning-error-tr-src-subnets warning-error-tr-src-goos" style="display:none">
														<td colspan="2">No object(s) selected nor created</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
									<tr class="src-rows" style="display:none">
										<th>Action</th>
										<td>
											<table class="control-radio-group">
												<tr>
													<td>
														<input id="src-action-1" type="radio" class="src-action control-radio control-editable" name="src-action" value="add source"/>&nbsp;
														<label class="control-radio-label" for="src-action-1">add source</label>
													</td>
												</tr>
												<tr>
													<td>
														<input id="src-action-2" type="radio" class="src-action control-radio control-editable" name="src-action" value="remove source"/>&nbsp;
														<label class="control-radio-label" for="src-action-2">remove source</label>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr id="warning-error-tr-src-action" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
								</table>
								<table class="group-component group-button">
									<tr class="control-display-editable">
										<td>
											<input type="button" class="button control-button small-button source-add-button" value="Add" onclick="source.add();">	
											<input type="button" class="button control-button small-button source-edit-button" value="Edit" onclick="source.edit();">	
											<input type="button" class="button control-button small-button" value="Clear" onclick="source.clear(1)">	
											<input type="button" class="button control-button small-button" value="Cancel" onclick="source.cancel();">
										</td>
									</tr>
									<tr class="control-display-readonly">
										<td>
											<input type="button" class="button control-button small-button" style="float:right;" value="Close" onclick="source.cancel();">
										</td>
									</tr>
								</table>
							</div>
							<div id="destination-dialog" class="control-dialog" style="display:none">
								<h2 id="destination-dialog-header"></h2>
								<table id="destination-definition" class="group-component" style="width:95%">
									<tr>
										<th>Organization<font color="red">&nbsp;*</font></th>
										<td>
											<select id="dest-org" class="dest-org control control-select control-editable" onchange="destination.changeOrganization();">
												<option value="">[Select ...]</option>
												<option value="MOBISTAR">MOBISTAR</option>
												<option value="OLU">OLU</option>
												<option value="MES">MES</option>
												<option value="EXTERNAL">EXTERNAL</option>
											</select>
											<img id="help-dest-org" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
										</td>
									</tr>
									<tr id="warning-error-tr-dest-org" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr class="dest-rows dest-primary-rows">
										<th>Category<font color="red">&nbsp;*</font></th>
										<td>
											<select id="dest-cat" class="dest-cat control control-select control-editable" onchange="destination.changeCategory();">
												<option value="">[Select ...]</option>
												<option value="Host">Host</option>
												<option value="Subnet">Subnet</option>
												<option value="Group of objects">Group of objects</option>
											</select>
											<img id="help-dest-cat" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
										</td>
									</tr>
									<tr id="warning-error-tr-dest-cat" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr class="dest-rows dest-goo-rows">
										<th>Group of objects<font color="red">&nbsp;*</font></th>
										<td>
											<input type="text" id="dest-goo" class="control control-text control-editable flow-control-action">
											<img id="help-dest-goo" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
											<img id="create-dest-goo" src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-goo" style="display:none;" title="this group of objects will be created"/>
										</td>
									</tr>									
									<tr id="warning-error-tr-dest-goo" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr class="dest-rows dest-goo-rows">
										<th>Environment<font class="dest-mandatory" color="red">&nbsp;*</font></th>
										<td>
											<select id="dest-goo-env" class="control control-select control-editable dest-autocomplete-rows" onchange="destination.changeEnv('goo-env');">
												<option value="" selected>[Select ...]</option>
												<option value="Prod" selected>Production</option>
												<option value="nonProd">Non production</option>
											</select>
											<img id="help-dest-goo-env" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
										</td>
									</tr>
									<tr id="warning-error-tr-dest-goo-env" class="warning-error" style="display:none">
										<td id="warning-error-td-dest-goo-env" colspan="2">Missing value</td>
									</tr>
									<tr>
										<td colspan="2" class="straight-box">
											<div class="dest-group-element-wrapper group-element-wrapper" style="max-height:400px;overflow-y:auto;overflow:auto">
												<table class="straight-box">
													<tr class="dest-rows dest-goo-selection-rows">
														<th colspan="2" style="text-align:center;">Host(s) selection</th>
													</tr>									
													<tr class="dest-rows dest-host-rows dest-goo-selection-rows">
														<th>Host name<font class="dest-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<input type="text" id="dest-host" class="control control-text control-editable fixed-autosuggest flow-control-action">
															<img id="help-dest-host" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
															<img id="create-dest-host" src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-host" style="display:none;" title="this host will be created"/>
														</td>
													</tr>
													<tr id="warning-error-tr-dest-host" class="warning-error" style="display:none">
														<td id="warning-error-td-dest-host" colspan="2">Missing value or incorrect value</td>
													</tr>									
													<tr id="warning-error-tr-dest-host-opt" class="warning-error warning-error-tr-dest-host" style="display:none">
														<td id="warning-error-td-dest-host-opt" colspan="2"></td>
													</tr>
													<tr class="dest-rows dest-host-rows dest-goo-selection-rows">
														<th>Environment<font class="dest-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<select id="dest-host-env" class="control control-select control-editable dest-autocomplete-rows" onchange="destination.changeEnv('host-env');">
																<option value="" selected>[Select ...]</option>
																<option value="Prod" selected>Production</option>
																<option value="nonProd">Non production</option>
															</select>
															<img id="help-dest-host-env" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
														</td>
													</tr>	
													<tr id="warning-error-tr-dest-host-env" class="warning-error" style="display:none">
														<td id="warning-error-td-dest-host-env" colspan="2">Missing value</td>
													</tr>
													<tr class="dest-rows dest-host-rows dest-goo-selection-rows">
														<th>Ip address<font class="dest-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<input type="text" id="dest-ip" class="control control-text control-editable flow-control-action" style="display:inline;">
															<img id="help-dest-ip" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
															<img id="dest-ip-load" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/loader.gif" title="load" />
															<img id="dest-ip-check" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/check_16px.png" title="check" />
															<img id="dest-ip-error" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/error_16px.png" title="error" />
															<img id="dest-ip-warning" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/icon-warning.gif" title="warning" />
															<input id="dest-ip-code" type="hidden" value="" />
														</td>
													</tr>
													<tr id="warning-error-tr-dest-ip" class="warning-error" style="display:none">
														<td id="warning-error-td-dest-ip" colspan="2">Missing value or incorrect ip address</td>
													</tr>
													<tr id="warning-error-tr-dest-ip-opt" class="warning-error warning-error-td-dest-ip" style="display:none">
														<td id="warning-error-td-dest-ip-opt" colspan="2"></td>
													</tr>
													<tr class="dest-rows dest-host-rows dest-goo-selection-rows dest-autocomplete-rows">
														<th>Translated ip</th>
														<td>
															<input type="text" id="dest-ip-tsl" class="control control-text control-editable dest-autocomplete-rows">
															<img id="help-dest-ip-tsl" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>											
														</td>
													</tr>
													<tr id="warning-error-tr-dest-ip-tsl" class="warning-error" style="display:none">
														<td colspan="2">Missing value or incorrect ip value</td>
													</tr>
													<tr id="tr-dest-host-area" class="dest-rows dest-host-rows dest-goo-selection-rows dest-autocomplete-rows">
														<th>Area</th>
														<td>
															<input type="text" id="dest-host-area" class="control control-text control-readonly dest-autocomplete-rows">											
														</td>
													</tr>
													<tr class="dest-rows dest-goo-selection-rows">
														<th>Hosts<font class="dest-group-mandatory" color="yellow">&nbsp;*</font></th>
														<td>
															<select id="dest-hosts" multiple size="3" class="dest-hosts control control-select control-editable"></select>
															<span class="control-display-editable multiple-select-linked-button" >
																<input type="button" class="button control-button small-button" value="Add" onclick="destination.sel_add('host');" >
																<input type="button" class="button control-button small-button" value="Delete" onclick="destination.sel_rem('host');" >
															</span>
														</td>
													</tr>
													<tr class="warning-error warning-error-tr-dest-hosts warning-error-tr-dest-subnets warning-error-tr-dest-goos" style="display:none">
														<td colspan="2">No object(s) selected nor created</td>
													</tr>
													<tr class="dest-rows dest-goo-selection-rows">
														<th colspan="2" style="text-align:center;">Subnet(s) selection</th>
													</tr>
													<tr class="dest-rows dest-subnet-rows dest-goo-selection-rows">
														<th>Subnet<font class="dest-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<input type="text" id="dest-subnet" class="control control-text control-editable flow-control-action" style="display:inline;">
															<img id="help-dest-subnet" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
															<img id="create-dest-subnet" src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-subnet" style="display:none;" title="this subnet will be created"/>
															<img id="dest-subnet-load" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/loader.gif" title="load" />
															<img id="dest-subnet-check" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/check_16px.png" title="check" />
															<img id="dest-subnet-error" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/error_16px.png" title="error" />
															<img id="dest-subnet-warning" style="display:none" class="img" src="<%= request.getContextPath()%>/ressources/icon-warning.gif" title="warning" />
															<input id="dest-subnet-code" type="hidden" value="" />
														</td>
													</tr>
													<tr id="warning-error-tr-dest-subnet" class="warning-error" style="display:none">
														<td colspan="2">Missing value or incorrect value ([subnet-ip]/[mask] e.g: 0.0.0.0/0)</td>
													</tr>
													<tr id="warning-error-tr-dest-subnet-opt" class="warning-error warning-error-tr-dest-subnet" style="display:none">
														<td id="warning-error-td-dest-subnet-opt" colspan="2"></td>
													</tr>
													<tr id="tr-dest-subnet-area" class="dest-rows dest-subnet-rows dest-goo-selection-rows">
														<th>Area</th>
														<td>
															<input type="text" id="dest-subnet-area" class="control control-text control-readonly dest-autocomplete-rows flow-control-action">
														</td>
													</tr>									
													<tr class="dest-rows dest-goo-selection-rows">
														<th>Subnets<font class="dest-group-mandatory" color="yellow">&nbsp;*</font></th>
														<td>
															<select id="dest-subnets" multiple size="3" class="dest-subnets control control-select control-editable"></select>
															<span class="control-display-editable multiple-select-linked-button" >
																<input type="button" class="button control-button small-button" value="Add" onclick="destination.sel_add('subnet');" >
																<input type="button" class="button control-button small-button" value="Delete" onclick="destination.sel_rem('subnet');" >
															</span>
														</td>
													</tr>
													<tr class="warning-error warning-error-tr-dest-hosts warning-error-tr-dest-subnets warning-error-tr-dest-goos" style="display:none">
														<td colspan="2">No object(s) selected nor created</td>
													</tr>
													<tr class="dest-rows dest-goo-selection-rows">
														<th colspan="2" style="text-align:center;">Group(s) of objects selection</th>
													</tr>
													<tr class="dest-rows dest-goo-selection-rows">
														<th>Group</th>
														<td>
															<input type="text" id="dest-goo-selector" class="control control-text control-editable flow-control-action">
															<img id="help-dest-goo-sel" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
														</td>
													</tr>									
													<tr id="warning-error-tr-dest-goo-selector" class="warning-error" style="display:none">
														<td colspan="2">Incorrect value</td>
													</tr>
													<tr class="dest-rows dest-goo-selection-rows">
														<th>Environment<font class="dest-mandatory" color="red">&nbsp;*</font></th>
														<td>
															<select id="dest-goo-sel-env" class="control control-select control-readonly" onchange="destination.changeEnv('goo-sel-env');">
																<option value="" selected>[Select ...]</option>
																<option value="Prod" selected>Production</option>
																<option value="nonProd">Non production</option>
															</select>
															<img id="help-dest-goo-sel-env" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
														</td>
													</tr>
													<tr id="warning-error-tr-dest-goo-sel-env" class="warning-error" style="display:none">
														<td id="warning-error-td-dest-goo-sel-env" colspan="2">Missing value</td>
													</tr>
													<tr class="dest-rows dest-goo-selection-rows">
														<th>Groups of objects<font class="dest-group-mandatory" color="yellow">&nbsp;*</font></th>
														<td>
															<select id="dest-goos" multiple size="3" class="dest-goos control control-select control-editable"></select>
															<span class="control-display-editable multiple-select-linked-button" >
																<input type="button" class="button control-button small-button" value="Add" onclick="destination.sel_add('goo-selector');" >
																<input type="button" class="button control-button small-button" value="Delete" onclick="destination.sel_rem('goo-selector');" >
															</span>
														</td>
													</tr>
													<tr class="warning-error warning-error-tr-dest-hosts warning-error-tr-dest-subnets warning-error-tr-dest-goos" style="display:none">
														<td colspan="2">No object(s) selected nor created</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
									<tr class="dest-rows" style="display:none">
										<th>Action</th>
										<td>
											<table class="control-radio-group">
												<tr>
													<td>
														<input id="dest-action-1" type="radio" class="dest-action control-radio control-editable" name="dest-action" value="add destination"/>&nbsp;
														<label class="control-radio-label" for="dest-action-1">add destination</label>
													</td>
												</tr>
												<tr>
													<td>
														<input id="dest-action-2" type="radio" class="dest-action control-radio control-editable" name="dest-action" value="remove destination"/>&nbsp;
														<label class="control-radio-label" for="dest-action-2">remove destination</label>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr id="warning-error-tr-dest-action" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
								</table>
								<table class="group-component group-button">
									<tr class="control-display-editable">
										<td>
											<input type="button" class="button control-button small-button destination-add-button" value="Add" onclick="destination.add();">	
											<input type="button" class="button control-button small-button destination-edit-button" value="Edit" onclick="destination.edit();">	
											<input type="button" class="button control-button small-button" value="Clear" onclick="destination.clear(1)">	
											<input type="button" class="button control-button small-button" value="Cancel" onclick="destination.cancel();">
										</td>
									</tr>
									<tr class="control-display-readonly">
										<td>
											<input type="button" class="button control-button small-button" style="float:right;" value="Close" onclick="destination.cancel();">
										</td>
									</tr>
								</table>
							</div>
							<div id="service-dialog" class="control-dialog" style="display:none">
								<h2 id="service-dialog-header"></h2>
								<table id="service-definition" class="group-component">
									<tr>
										<th>Category<font color="red">&nbsp;*</font></th>
										<td>
											<select id="srv-cat" class="srv-cat control control-select control-editable" onchange="service.changeCategory();">
												<option value="">[Select ...]</option>
												<option value="Single service">Single service</option>
												<option value="Group of services">Group of services</option>
											</select>
											<img id="help-srv-cat" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
										</td>
									</tr>
									<tr id="warning-error-tr-srv-cat" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr class="srv-rows srv-gsrv-rows">
										<th>Group of services<font color="red">&nbsp;*</font></th>
										<td>
											<input type="text" id="srv-gsrv-name" class="control control-text control-editable flow-control-action">
											<img id="help-srv-gsrv-name" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
											<img id="create-srv-gsrv-name" src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-gsrv" style="display:none;" title="this group of services will be created"/>
										</td>
									</tr>
									<tr id="warning-error-tr-srv-gsrv-name" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr class="dest-rows srv-gsrv-selection-rows">
										<th colspan="2" style="text-align:center;">Service(s) selection</th>
									</tr>
									<tr class="srv-rows srv-one-rows srv-gsrv-selection-rows">
										<th>Name<font class="srv-mandatory" color="red">&nbsp;*</font></th>
										<td>
											<input type="text" id="srv-one-name" class="control control-text control-editable flow-control-action">
											<img id="help-srv-one-name" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
											<img id="create-srv-one-name" src="<%= request.getContextPath()%>/ressources/document_new.png" class="img control-create control-create-srv" style="display:none;" title="this service will be created"/>
										</td>
									</tr>
									<tr id="warning-error-tr-srv-one-name" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr class="srv-rows srv-one-rows srv-gsrv-selection-rows">
										<th>Protocol<font class="srv-mandatory" color="red">&nbsp;*</font></th>
										<td>
											<select id="srv-one-proto" class="control control-select control-editable srv-autocomplete-rows" onchange="service.changeProto()">
												<option value="">[Select ...]</option>
												<option value="udp">UDP</option>
												<option value="tcp">TCP</option>
												<option value="icmp">ICMP</option>
												<option value="other ip">Other IP</option>
											</select>
											<img id="help-srv-one-proto" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
										</td>
									</tr>
									<tr id="warning-error-tr-srv-one-proto" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr style="display:none">
										<th>Type<font class="srv-mandatory" color="red">&nbsp;*</font></th>
										<td><input type="text" id="srv-one-type" class="control control-text control-editable srv-autocomplete-rows"></td>
									</tr>
									<tr id="warning-error-tr-srv-one-type" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr id="row-srv-one-port" class="srv-rows srv-one-rows srv-gsrv-selection-rows">
										<th>Port<font class="srv-mandatory" color="red">&nbsp;*</font></th>
										<td>
											<input type="text" id="srv-one-port" class="control control-text control-editable">
											<img id="help-srv-one-port" src="<%= request.getContextPath()%>/ressources/information.png" class="img control-help" style="display:none;" title=""/>
										</td>
									</tr>
									<tr id="warning-error-tr-srv-one-port" class="warning-error" style="display:none">
										<td id="warning-error-td-srv-one-port" colspan="2">Missing value or incorrect port value or value already in use</td>
									</tr>
									<tr class="srv-rows srv-gsrv-selection-rows">
										<th>Services<font color="red">&nbsp;*</font></th>
										<td>
											<select id="srv-gsrv-srvs" multiple size="4" class="srv-gsrv-srvs control control-select control-editable"></select>
											<span class="control-display-editable multiple-select-linked-button" >
												<input type="button" class="button control-button small-button" value="Add" onclick="service.sel_add('srv');" >
												<input type="button" class="button control-button small-button" value="Delete" onclick="service.sel_rem('srv');" >
											</span>
										</td>
									</tr>
									<tr class="warning-error warning-error-tr-srv-gsrv-srvs" style="display:none">
										<td colspan="2">No service(s) selected nor created</td>
									</tr>
									<tr class="srv-rows" style="display:none">
										<th>Action</th>
										<td>
											<table class="control-radio-group">
												<tr>
													<td>
														<input id="srv-action-1" type="radio" class="srv-action control-radio control-editable" name="srv-action" value="add service"/>&nbsp;
														<label class="control-radio-label" for="srv-action-1">add service</label>
													</td>
												</tr>
												<tr>
													<td>
														<input id="srv-action-2" type="radio" class="srv-action control-radio control-editable" name="srv-action" value="remove service"/>&nbsp;
														<label class="control-radio-label" for="srv-action-2">remove service</label>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr id="warning-error-tr-srv-action" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
								</table>
								<table class="group-component group-button">										
									<tr class="control-display-editable">									
										<td>
											<input type="button" class="button control-button small-button service-add-button" value="Add" onclick="service.add();">
											<input type="button" class="button control-button small-button service-edit-button" value="Edit" onclick="service.edit();">
											<input type="button" class="button control-button small-button" value="Clear" onclick="service.clear(1);">
											<input type="button" class="button control-button small-button" value="Cancel" onclick="service.cancel();">
										</td>
									</tr>
									<tr class="control-display-readonly">
										<td>
											<input type="button" class="button control-button small-button" style="float:right;" value="Close" onclick="service.cancel();">
										</td>
									</tr>
								</table>
							</div>
							<table class="group-component group-button">
								<tr id="add-flow-control-buttons" class="control-display-editable">
									<td>
										<input type="button" class="button control-button small-button flow-add-button" value="Add" onclick="flow.add();">
										<input type="button" class="button control-button small-button flow-edit-button" value="Edit" onclick="flow.edit();">
										<input type="button" class="button control-button small-button flow-add-button" value="Cancel" onclick="flow.cancel(false);">
										<input type="button" class="button control-button small-button flow-edit-button" value="Cancel" onclick="flow.cancel(true);">
										<input type="button" class="button control-button small-button" value="Clear" onclick="flow.clear(2);" style="display:none">
									</td>
								</tr>
								<tr id="add-flow-control-buttons" class="control-display-readonly">
									<td>
										<input type="button" class="button control-button small-button" value="Close" onclick="hide('flow-section');show('main-section');">
									</td>
								</tr>
							</table>
						</div>
					</form>
					<div id="debug-tool" style="display:none">						
							<table class="group-component">
								<tr>
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="clear" onclick="clearDT()" style="display:inline">
										<input type="button" class="button control-button small-button" value="reload" onclick="load('request')" style="display:inline">
										<input type="button" class="button control-button small-button" value="Edit" onclick="getE('readonly').value = false;setReadonlyView(false);switchFormEdition('advanced',true);" style="display:inline">									
										<input type="button" class="button control-button small-button" value="FWO" onclick="getE('fwo').value = true;setUserView();flow.refreshFlowsView();" style="display:inline">									
																				
									</td>
								</tr>	
								<tr id="warning-error-tr-debug-list-selection" class="warning-error" style="display:none">
									<td colspan="2">Blurp Blurp</td>
								</tr>
								<tr>
									<td colspan=2>
										<span id="debug-section" style="color:red;font-weight:bold;"></span>
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="try" onclick="ant()" style="display:inline">
										<input type="button" class="button control-button small-button" value="clear" onclick="getE('debug-area').value=''" style="display:inline">
										<input type="button" class="button control-button small-button" value="showAefs" onclick="showAefs()" style="display:inline">
										<input type="button" class="button control-button small-button" value="sampleFlows" onclick="addSampleFlows()" style="display:inline">										
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="setCelsiusUser" onclick="setCelsiusUser()" style="display:inline">										
										<input type="button" class="button control-button small-button" value="dupform" onclick="DF();" style="display:inline">
										<input type="button" class="button control-button small-button" value="up-cat" onclick="updateCatalog();" style="display:inline">
										<input type="button" class="button control-button small-button" value="initFWN" onclick="initFWN();" style="display:inline">
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<input type="button" class="button control-button small-button" value="ldap" onclick="ldapMe();" style="display:inline">
										<input type="button" class="button control-button small-button" value="WS" onclick="test_WS(getE('debug-area').value);" style="display:inline">
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<textarea id="debug-area" class="control-textarea"></textarea>
									</td>
								</tr>
								<tr id="warning-error-tr-debug-area" class="warning-error" style="display:none">
									<td colspan="2">Blurp Blurp</td>
								</tr>
							</table>
						</span>
					</div>
				</div>				
			</div>					
			<div id="footer">	
				<img src="<%= request.getContextPath()%>/ressources/footer.jpg" class="bgfooter">
			</div>				
		</div>
	</body>
</html>
