<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.RuleProfile" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/objects.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>

		<style type="text/css">
	        body { background-color: #fafafa; }	      
	        table { background-color: #D0D0D0 ; } 
		</style>
		
		<title>Profiles</title>		
		
	</head>
	<body>
		
		<div id="SearchResult"></div>
		
		<%
					RuleProfile profileBean=(RuleProfile)request.getAttribute("profile");
				%>
		<h2>Profile <%= profileBean.getName() %></h2>
		<table>			
			<tr>
				<th>Used in Rules</th>
				<td><%= profileBean.getUsedInRules() %></td>
			</tr>
						
		</table>
	</body>
</html>