<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		
		<title>FCRWeb - Reports</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> Reports</h4>
		<h2>Reports</h2>
		
		<table>
			<tr>
				<th>Report</th>
				<th>Description</th>
			</tr>
			<tr>
				<td><h3><a href="reports.htm?id=1">Orphan Firewall Rules</a></h3></td>
				<td>This report shows all the rules that are not linked to a Technical Service</td>
			</tr>
			<tr>
				<td><h3><a href="reports.htm?id=2">Rules without owner</a></h3></td>
				<td>This report shows all the rules that are linked to a Technical Service without owner</td>
			</tr>
			<tr>
				<td><h3><a href="reports.htm?id=3">Firewall object naming convention</a></h3></td>
				<td>This report shows all the objects defined on the firewalls that are not compliant with the naming convention</td>
			</tr>
			<tr>
				<td><h3><a href="reports.htm?id=4">Firewall Objects definition</a></h3></td>
				<td>This report shows the result of the comparison of objects defined in FWRules and those defined in the CMDB</td>
			</tr>
			<tr>
				<td><h3><a href="reports.htm?id=5">Firewall Objects consistency</a></h3></td>
				<td>This report checks the consistency of objects and groups between the different Firewalls</td>
			</tr>
			<tr>
				<td><h3><a href="reports.htm?id=6">FCR rejected for implementation</a></h3></td>
				<td>This report shows the FCR that have been rejected for implementation</td>
			</tr>
			<tr>
				<td><h3><a href="reports.htm?id=7">FCR number per teams</a></h3></td>
				<td>This report shows the number of FCR created by each team</td>
			</tr>
			<tr>
				<td><h3><a href="reports.htm?id=8">Average actions delay</a></h3></td>
				<td>This report shows the average delay by action (Validation, Security Validation and Implementation)</td>
			</tr>
			<tr>
				<td><h3><a href="reports.htm?id=9">Technical Service revalidation</a></h3></td>
				<td>This report consist of the revalidation of Firewall rules defined in a Technical Service</td>
			</tr>
			
		</table>
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>