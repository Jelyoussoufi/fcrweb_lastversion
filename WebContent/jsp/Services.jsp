<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.ServiceSummary" %>
<%@ page import="be.celsius.fcrweb.objects.ServiceGroupSummary" %>
<%@ page import="be.celsius.fcrweb.objects.ServicesPageData" %>
<%@ page import="be.celsius.fcrweb.objects.Task" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/services.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/util_function.js"></SCRIPT>
		
		<title>FCRWeb - Services</title>
		
	</head>
	<body >
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/Services_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> Technical Service -> Services</h4>
		<div id="SearchResult"></div>
		<h2>Global Services</h2>
		
		<%
				ServicesPageData servicesBean=(ServicesPageData)request.getAttribute("services");
				ArrayList<ServiceSummary> predefinedServices = servicesBean.getPredefinedServices();
				int i = 0;
				
				for (ServiceSummary predefinedService:predefinedServices)
				{	
					if (i==0)
					{
					%>
					<table >
					<tr>
						<th class="first"><strong>Name</strong></th>
						<th>Protocol</th>
						<th>Port</th>
					</tr>
					<%
					}
					if (i%2 == 0)
					{
						%>
						<tr class="row-a">
						<%
					}
					else
					{
						%>
						<tr class="row-b">
						<%
					}
					%>		
						<td class="first"><a href="javascript:void(0)" onClick="window.open('services.htm?id=<%= predefinedService.getId() %>&type=<%= predefinedService.getProtocol() %>', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= predefinedService.getName() %></a></td>
						<td><%= predefinedService.getProtocol() %></td>
						<td><%= predefinedService.getPort() %></td>
					</tr>
					<%
					i += 1;					
				}
				

				if (i == 0)
				{
					%>
					No Global services to display.
					<%
				}
				else
				{
					%>
					</table>
					<%
				}
			%>
			
			<h2>Global Service Groups</h2>
		
		<%				
				ArrayList<ServiceGroupSummary> predefinedServiceGroups = servicesBean.getPredefinedServiceGroups();
				i = 0;
				
				for (ServiceGroupSummary predefinedServiceGroup:predefinedServiceGroups)
				{	
					if (i==0)
					{
					%>						
					<table >
					<tr>
						<th class="first"><strong>Name</strong></th>
						<th>Description</th>
					</tr>
					<%
					}
					if (i%2 == 0)
					{
						%>
						<tr class="row-a">
						<%
					}
					else
					{
						%>
						<tr class="row-b">
						<%
					}
					%>		
						<td class="first"><a href="javascript:void(0)" onClick="window.open('services.htm?id=<%= predefinedServiceGroup.getId() %>&type=group', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= predefinedServiceGroup.getName() %></a></td>
						<td><%= predefinedServiceGroup.getDescription() %></td>
					</tr>
					<%
					i += 1;					
				}
				

				if (i == 0)
				{
					%>
					No Global Service Groups to display.
					<%
				}
				else
				{
					%>
					</table>
					<%
				}
			%>
	
		
		<h2>Local Services and Service Groups</h2>
	
		<%				
				ArrayList<ServiceSummary> myServices = servicesBean.getMyServices();
				ArrayList<ServiceGroupSummary> myServiceGroups = servicesBean.getMyServiceGroups();
				i = 0;
				%>
				<script>var listTab = [];</script>
				<%
				String oldTS = "";
				ArrayList<String> tsList = new ArrayList<String>();
				
				for (ServiceSummary getTSServices: myServices)
				{
					if (oldTS.equals(""))
					{
						oldTS = getTSServices.getTechnicalService();
						tsList.add(oldTS);
					}
					
					if (!oldTS.equals(getTSServices.getTechnicalService()))
					{
						oldTS = getTSServices.getTechnicalService();
						tsList.add(oldTS);
					}
				}
				
				for (ServiceGroupSummary getTSServiceGroups: myServiceGroups)
				{
					Boolean alreadyInList = false;
					for (String tempTS: tsList)
					{
						if (tempTS.equals(getTSServiceGroups.getTechnicalService()))
						{
							alreadyInList = true;
						}
					}
					if (!alreadyInList)
					{
						tsList.add(getTSServiceGroups.getTechnicalService());
					}
				}
				
				for (int ts_list_id = 0; ts_list_id < tsList.size(); ts_list_id++)
				{
					i= 0;
					oldTS = tsList.get(ts_list_id);
					
					%>
						<h3><%= oldTS %></h3>					
					<%
					
					for (ServiceSummary myService:myServices)
					{
						if (myService.getTechnicalService().equals(oldTS))
						{
							if (i == 0)
							{
							%>
								<table >
								<tr>
									<th class="first"><strong>Name</strong></th>
									<th>Protocol</th>
									<th>Port</th>
								</tr>
							<%
							}
							if (i%2 == 1)
							{
								%>
								<tr class="row-a">
								<%
							}
							else
							{
								%>
								<tr class="row-b">
								<%
							}
							%>
								<td class="first"><a href="javascript:void(0)" onClick="window.open('services.htm?id=<%= myService.getId() %>&type=<%= myService.getProtocol() %>', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= myService.getName() %></a></td>
								<td><%= myService.getProtocol() %></td>
								<td><%= myService.getPort() %></td>
							</tr>
		
							<%
							i=i+1;
						}						
						
					}
					if (i > 0)
					{
						%>
							</table>
						<%
					}
					i = 0;
					for (ServiceGroupSummary myServiceGroup:myServiceGroups)
					{
						if (myServiceGroup.getTechnicalService().equals(oldTS))
						{
							if (i==0)
							{
								%>		
								<input id="button<%= ts_list_id %>" style="display: none;" class="button" value="Edit Service(s)" type="button" onClick="editSelectedServices(<%= ts_list_id %>, '<%= oldTS %>')" />
								<script>
									listTab[<%= ts_list_id %>] = [];
								</script>							
								<table>
								<tr>
									<th class="first"></th>
									<th><strong>Name</strong></th>
									<th>Description</th>
								</tr>
								<%
							}
						
							if (i%2 == 1)
							{
								%>
								<tr class="row-a">
								<%
							}
							else
							{
								%>
								<tr class="row-b">
								<%
							}
							%>		
								<td class="first"><input type="checkbox" onclick="serviceChecked(<%= myServiceGroup.getId() %>, <%= ts_list_id %>);"></td>
								<td class="first"><a href="javascript:void(0)" onClick="window.open('services.htm?id=<%= myServiceGroup.getId() %>&type=group', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= myServiceGroup.getName() %></a></td>
								<td><%= myServiceGroup.getDescription() %></td>
							</tr>
		
							<%
							i=i+1;
						}
					}
					if (i > 0)
					{
						%>
							</table>
						<%
					}
				}
				if (tsList.size() == 0)
				{
					%>
					No Local Services to display.
					<%
				}
				else
				{
					%>
					</table>
					<%
				}
			%>
		
		
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>