<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.RuleObject" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/objects.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/js/common.js"></SCRIPT>
			
		<style type="text/css">
	        body { background-color: #fafafa; }	      
	        table { background-color: #D0D0D0 ; } 
		</style>
		
		
		<title>Network Object</title>		
		
	</head>
	<body>
		
		<div id="SearchResult"></div>
		
		<%
					RuleObject objectBean=(RuleObject)request.getAttribute("object");
				%>
		<h2>Object <%= objectBean.getName() %></h2>
		<table>
			<tr>
				<th>Type</th>
				<td><%= objectBean.getType() %></td>
			</tr>
			<%
				ArrayList<String[]> temp = objectBean.getOtherParameters();
				for (int i = 0; i < temp.size(); i++)
				{
					String[] param = temp.get(i);
					%>
					<tr>
						<th><%= param[0] %></th>
						<td><%= param[1] %></td>
					</tr>
					<%
				}
			%>
			<tr>
				<th>Used in Groups</th>
				<td><%= objectBean.getUsedInGroups() %></td>
			</tr>
			<tr>
				<th>Used in Rules</th>
				<td><%= objectBean.getUsedInRules() %></td>
			</tr>
			
			
		</table>
	</body>
</html>