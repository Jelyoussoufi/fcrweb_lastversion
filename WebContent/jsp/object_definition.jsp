<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>
<%@ page import="be.celsius.fcrweb.objects.HostDefinitionProblemList" %>
<%@ page import="be.celsius.fcrweb.objects.HostDefinitionProblem" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/reports.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		
		<title>FCRWeb - Reports</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> <a href="reports.htm">Reports</a> -> Firewall object definition</h4>
					
			<% 
			HostDefinitionProblemList host_definition =(HostDefinitionProblemList)request.getAttribute("host_definition");
			
			%>
			<h2>Hosts definition</h2>
			<h3>Hosts defined on the Firewalls but not in the CMDB: <%= host_definition.getNotExistingCMDB().size() %> <a id="notInCMDB" href="javascript:void(0)" onClick="displayNotCompliant('notInCMDB')">(show)</a></h3>
			<div id="notInCMDB_div" style="display:none;">
			<table>
			<tr>
				<th>Name</th>
				<th>IP</th>
			</tr>	
			<% 
			for (HostDefinitionProblem problem:host_definition.getNotExistingCMDB())
			{				
					
					%><tr>
						<td><a href="javascript:void(0)" onClick="window.open('rules.htm?object_id=<%= problem.getId() %>','_blank','resizable=yes, scrollbars=1, width=300, height=400')"><%= problem.getName() %></a></td>
						<td><%= problem.getIp() %></td>
					</tr><%					
			}
			
			%>
			</table>
			</div>
			
			<h3>Hosts defined in the CMDB but not on the Firewalls: <%= host_definition.getNotExistingFW().size() %> <a id="notInFW" href="javascript:void(0)" onClick="displayNotCompliant('notInFW')">(show)</a></h3>
			<div id="notInFW_div" style="display:none;">
			<table>
			<tr>
				<th>Name</th>
				<th>IP</th>
			</tr>
			<% 
			for (HostDefinitionProblem problem:host_definition.getNotExistingFW())
			{				
					
					%><tr>
						<td><a href="javascript:void(0)" onClick="window.open('objects.htm?id=<%= problem.getId() %>&type=host','_blank','resizable=yes, scrollbars=1, width=300, height=400')"><%= problem.getName() %></a></td>
						<td><%= problem.getIp() %></td>
					</tr><%					
			}
			
			%>
			</table>
			</div>
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>