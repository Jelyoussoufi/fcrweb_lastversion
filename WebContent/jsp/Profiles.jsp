<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="be.celsius.fcrweb.objects.ProfileSummary" %>
<%@ page import="be.celsius.fcrweb.objects.ProfilesPageData" %>
<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/profiles.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>


		<title>FCRWeb - User Groups</title>

	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/Profiles_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> Technical Service -> User Groups</h4>
		<div id="SearchResult"></div>
		<h2>Global User Groups</h2>

		<%
				ProfilesPageData profilesBean=(ProfilesPageData)request.getAttribute("profiles");
				ArrayList<ProfileSummary> predefinedProfiles = profilesBean.getPredefinedProfiles();
				int i = 0;

				for (ProfileSummary predefinedProfile:predefinedProfiles)
				{
					if (i == 0)
					{
					%>
					<table >
					<tr>
						<th class="first"><strong>Name</strong></th>
						<th>Description</th>
					</tr>
					<%
					}
					if (i%2 == 0)
					{
						%>
						<tr class="row-a">
						<%
					}
					else
					{
						%>
						<tr class="row-b">
						<%
					}
					%>		
						<td class="first"><a href="javascript:void(0)" onClick="window.open('profiles.htm?id=<%= predefinedProfile.getId() %>', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= predefinedProfile.getName() %></a></td>
						<td><%= predefinedProfile.getDescription() %></td>
					</tr>
					<%
					i += 1;
				}

				if (i == 0)
				{
					%>
					No predefined User Groups to display.
					<%
				}
				else
				{
					%>
					</table>
					<%
				}
			%>	
		
		<h2>Local User Groups</h2>
	
			<%				
				ArrayList<ProfileSummary> myProfiles = profilesBean.getMyProfiles();
				i = 0;
				int counter = 0;
				String oldTS = "";
				for (ProfileSummary myProfile:myProfiles)
				{
					if (myProfile.getTechnicalService().equals(oldTS))
					{
						if (i%2 == 1)
						{
							%>
							<tr class="row-a">
							<%
						}
						else
						{
							%>
							<tr class="row-b">
							<%
						}
						%>		
							<td class="first"><a href="javascript:void(0)" onClick="window.open('profiles.htm?id=<%= myProfile.getId() %>', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= myProfile.getName() %></a></td>
							<td><%= myProfile.getDescription() %></td>
						</tr>
	
						<%
						i=i+1;
						counter+=1;
					}
					else
					{
						if (!oldTS.equals(""))
						{
							%>
							</table>
							<%
						}
						oldTS = myProfile.getTechnicalService();
						%>
						<h3><%= oldTS %></h3>
						<table >
						<tr>
							<th class="first"><strong>Name</strong></th>
							<th>Description</th>
						</tr>
						<%
						if (i%2 == 0)
						{
							%>
							<tr class="row-a">
							<%
						}
						else
						{
							%>
							<tr class="row-b">
							<%
						}
						%>		
							<td class="first"><a href="javascript:void(0)" onClick="window.open('profiles.htm?id=<%= myProfile.getId() %>', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')"><%= myProfile.getName() %></a></td>
							<td><%= myProfile.getDescription() %></td>
						</tr>
						<%
						i = 0;
						counter+=1;
					}
				}
				if (counter == 0)
				{
					%>
					No Local User Groups to display.
					<%
				}
				else
				{
					%>
					</table>
					<%
				}
			%>
							
		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>