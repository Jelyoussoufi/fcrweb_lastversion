<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<LINK href="ressources/main.css" rel="stylesheet" type="text/css">
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/admin.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/common.js"></SCRIPT>

		
		<title>FCRWeb - Admin</title>
		
	</head>
	<body>
		<%@ include file="/jsp/template/header.jsp" %>
		<%@ include file="/jsp/template/empty_sidebar.jsp" %>
		<h4 style="font-size:12px"><a href="index.htm">Home</a> -> Admin</h4>
		<h2>Admin</h2>
		
		<table>
			<tr>
				<th>Action</th>
				<th>Description</th>
			</tr>
			<tr>
				<td><h3><a href="admin.htm?id=1">Manage rules technical service</a></h3></td>
				<td>This page allows to modify the technical service that is linked to a rule</td>
			</tr>
			<tr>
				<td><h3><a href="admin.htm?id=2">Manage Application Parameters</a></h3></td>
				<td>This page allows to modify some application parameters such as webservice links</td>
			</tr>
			<tr>
				<td><h3><a href="admin.htm?id=3">Manage Private Ranges</a></h3></td>
				<td>This page allows to modify the Mobistar private IP ranges</td>
			</tr>
		</table>

		<%@ include file="/jsp/template/footer.jsp" %>
	</body>
</html>