
	<div id="head">
		
		<c:set scope="session" var="atCelsius" value="${environment.environment==1}" />
		<c:if test="${atCelsius==true}" >
			<input type="hidden" id="contextPath" value="<%= contextPath%>" />
			<input type="hidden" id="atCelsius" value="true" />
			<c:set scope="session" var="path" value="<%= path%>" />
		</c:if>
		<c:if test="${atCelsius!=true}" >
			<input type="hidden" id="contextPath" value="<%= contextPath%>" />
			<base href="<%=basePath%>">
			<input type="hidden" id="atCelsius" value="false" />
			<c:set scope="session" var="path" value="" />
		</c:if>

		<input type="hidden" id="userAgent" value='<%= userAgent%>'/>
		<input type="hidden" id="homePage" value='false'/>
		<input type="hidden" id="trueContextPath" value="<%= contextPath%>" />
		<input type="hidden" id="basePath" value="<%= basePath%>" />
		<input type="hidden" id="servletPath" value="<%= servletPath%>" />
		<input type="hidden" id="realPath" value="<%= realPath%>" />
		<input type="hidden" id="getURL" value="<%= getURL%>" />
		<input type="hidden" id="reqURI" value="<%= reqURI%>" />
		<input type="hidden" id="queryString" value="<%= queryString%>" />
		<!--[if IE]><input type="hidden" id="ie" value="true" /><![endif]-->
		<!--[if lte IE 8]><input type="hidden" id="ieold" value="true" /><![endif]-->
		<input type="hidden" id="ie" value="false" />

		<input type="hidden" id="local" name="local" value="" />
		<input type="hidden" id="form-id" name="fid" value="" />
		<input type="hidden" id="readonly" name="ro" value="" />
		<input type="hidden" id="req" name="req" value="false" />
		<input type="hidden" id="fwe" name="fwe" value="false" />
		<input type="hidden" id="fwo" name="fwo" value="false" />	
		<input type="hidden" id="change-rule" name="change-rule" value="false" />	<!-- change rule incoming data -->	
		<input type="hidden" id="tsrr" name="tsrr" value="false" />	<!-- ts revalidation incoming data -->	
		<input type="hidden" id="user-type" value="" />	<!-- user type simple or advanced -->		
		<input type="hidden" id="form-sel" value="" />	<!-- last form type saved simple or advanced or empty if first time load -->
		<input type="hidden" id="form-selector" value="" />	<!-- current form type simple or advanced or empty if first time load -->
		<input type="hidden" id="form-type" value="" />			<!-- fcr (usual) or maintain -->	
		<input type="hidden" id="conflicts" value="" />
		
		<title>Firewall Change Request Form</title>
		
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/main_form.css" type="text/css" />
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/autosuggest.css" type="text/css" />
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/jsDatePick_ltr.min.css" type="text/css" />
		
		<style type="text/css">
			#main{width:93%;}
			#main-section{width:98%;}
			
			body { font-size: 62.5%;}
			label, input { display:block; }
			input.text { margin-bottom:12px; width:95%; padding: .4em; }
			fieldset { padding:0; border:0; margin-top:25px; }
			h1 { font-size: 1.2em; margin: .6em 0; }
			
			div#users-contain { width: 100%; margin: 0; }
			div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
			div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
			.ui-dialog .ui-state-error { padding: .3em; }
			.validateTips { border: 1px solid transparent; padding: 0.3em; }
			
			.warning-error td
			{
				color : red;
				font-italic : italic;				
				font-weight : bold;				
			}
			
			.warning-conflict td
			{
				color : orange;
				font-italic : italic;				
				font-weight : bold;	
			}
						
			.textarea-img
			{
				position: relative;
				top: 50%;
				vertical-align: top;
				padding:0px;
				margin:0px;
				display:inline;
			}
			
			.img
			{
				padding:0px;
				margin:0px;
				vertical-align:middle;
				align:center;
				display:inline;
			}
			
			.img-clickable
			{
				cursor: pointer;				
			}
			
			.mask_arrow 
			{
			/*
				background-image: url("<%= request.getContextPath()%>/ressources/more.gif");
			*/
			}
			
			.straight-box
			{
				margin:0;
				padding:0;
				border:0px;
			}
			
			.control{display:inline;}
			.control-radio{display:inline; height:10px; line-height:10px;}
			.control-radio-label{display:inline; height:10px; line-height:10px;}
			.control-radio-group{width:300px;padding:0px; margin:0px;}
			
			.control-help{display:none;}
			
			.control-link
			{
				cursor:pointer;
				color:#2180BC;				
			}
			
			.control-link:hover{color:#88AC0B;}
			.control-link:visited{color:#2180BC;}
			
			.group-component{margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;}
			.group-component tr			
			{font-size: 75%;}
					
			.group-component th {width:150px;}
			.group-component .control-text {width:300px;}
			.group-component .small-text{width:200px;}
			.group-component .tiny{width:50px;}
			.group-component select {width:300px;}
			
			input, label {
				padding:2px;
				border:1px solid #eee;
				font: normal 1em Verdana, sans-serif;
				color:#777;
			}
						
			.control-textarea
			{
				width:300px;
				height:100px;
				resize:none;
			}
			
			.mini-textarea
			{
				width:200px;
				height:50px;
				resize:none;
			}
			.tiny-textarea
			{
				width:120px;
				height:30px;
				resize:none;
			}
			
			.tiny
			{
				width:120px;
			}
			
			.repeat-group
			{font-size : 10px;}
			
			.repeat-group tr,
			.repeat-group td
			{
				padding:0px;
				margin:0px;
			}
			
			.repeat-group input,
			.repeat-group label
			{
				display:inline;
				padding:0;
				margin:0;
				border:0px;
			}	
			
			.flow-documentation-ddl
			{
				display:inline;
				height:16px;
				line-height:16px;
			}
			
			/* buttons */
			.group-button{margin-top:10px;}
			.button 
			{
				cursor: pointer; 
				display:inline;
				padding-top:0;
				padding-bottom:0;
				margin-top:0;
				margin-bottom:0;
				
			}
						
			.small-button {width: 50px;}
			.normal-button {width: 120px;}			
			
			.multiple-select-linked-button
			{
				display: inline;
				position: relative;
				top: 15px;
				vertical-align: top;
			}
			
			/* default display mode
			.control-display-editable{display:none;}
			*/
			
			.maskass
			{
				background-color:black;
				position: fixed;
				top: 0;
				left: 0;
				min-width: 100%;
				width: 100%;
				min-height: 100%;
				height: 100%;
				opacity:0.5;
				Z-INDEX: 10; 
				filter: alpha(opacity = 50);
			}
			
			.processing
			{
				background-image: url("<%= request.getContextPath()%>/ressources/processing-1.gif");
                background-position: center center;
                background-repeat: no-repeat;				
				background-color:black;	
				position: fixed;
				top: 0;
				left: 0;
				min-width: 100%;
				width: 100%;
				min-height: 100%;
				height: 100%;
				opacity:0.5;
				Z-INDEX: 19; 
				filter: alpha(opacity = 50);
			}
			
			.process-icon
			{
				Z-INDEX: 10002;
				position:absolute;
				top:50%;
				left:50%;
			}
			
			.maskass-ie8
			{
				background-color:black;
				opacity:0;
				filter: alpha(opacity = 0);
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 400px;				
				margin: 0px;
				padding: 0px;
				Z-INDEX: 10; 
				
			}
			
			.control-dialog
			{
				/*
				background:white;
				border: thin #93BC0C solid;
				border-radius : 5px;
				position: absolute;
				top: 15%;
				left: 20%;
				Z-INDEX: 11; 		
				width:675px;
				*/
			}
			
			.control-list-member
			{
				background:white;
				border: thin #93BC0C solid;
				border-radius : 5px;
				position: absolute;
				top: 25%;
				left: 25%;
				Z-INDEX: 12; 		
				width:400px;
			}
			
			option .control-option-disabled,
			.control-option-disabled
			{
				color : black;
			}
			
			.autosuggest-body{Z-INDEX:10002;}
			
			.control-dialog .group-button
			{float:left;}
			
			.dialog-bg-header
			{
				width: 100%;
				height: 100%;
				top: 0px;
			}
			
			.dialog-bg
			{
				margin:0px;
				padding:0px;
				border:0px;
			}
			
			.dialog-bg-content {
	width: 100%;
	height: 100%;
	}

			th:disabled,
			td:disabled,
			input[type="button"]:disabled
			{
				font-color:white;
			}
		</style>
		
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/datepicker.css" type="text/css" />
		<!--[if IE]>
			<link rel="stylesheet" href="<%= request.getContextPath()%>/ressources/datepicker_ie.css" type="text/css" />
		<![endif]-->
		
		
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jsDatePick.min.1.3.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/autosuggest.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/util_function.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/util_html.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_form.js"></SCRIPT>		
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_objects.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_maintain.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_maintain_object.js"></SCRIPT>
		<!--<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/fcrweb_form_util.js"></SCRIPT>		-->
		<!--[<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jquery-2.0.3.js"></SCRIPT>-->
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jquery.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jquery-ui.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/jq.js"></SCRIPT>		
		
		<!--[if IE]>						
			<script LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/json2.js"></script>
			
			<script LANGUAGE="JavaScript" SRC="<%= request.getContextPath()%>/js/datepicker.js"></script>
			<script LANGUAGE="JavaScript" src="<%= request.getContextPath()%>/js/eye.js"></script>
			<script LANGUAGE="JavaScript" src="<%= request.getContextPath()%>/js/layout.js"></script>
		<![endif]-->
		
		
		
		 <script>
		$(function() {
			//$('#dialog-box').dialog({ autoOpen: false });
			//$('#source-dialog-box').dialog({ autoOpen: false });
		});
		</script>
		
		<link rel='stylesheet' type='text/css' href='<%= request.getContextPath()%>/ressources/zd_dialog.css'></link>
		<script language='JavaScript' type='text/JavaScript' src='<%= request.getContextPath()%>/js/zd_draganddrop.js'></script>
	</head>
	<body id="body" onload="load('<%= form_type%>');">
		
		<div class="maskass" style="display:none" ></div>
		<div class="processing" style="display:none" ></div>
		<div id="wrap" >
			<div class="mask-ie" style="display:none;height:100%;width:100%;position:absolute;top:0px;left:0px;z-index:90000;background:black;opacity:0.5;filter: alpha(opacity = 50);" ></div>
			<div id="header">				
				<img src="<%= request.getContextPath()%>/ressources/header.jpg" class="bgheader">
			</div>
			<img src="<%= request.getContextPath()%>/ressources/content_forms.jpg" class="bgcontent">
			<div id="content-wrap">		
				<img class="process-icon" src="<%= request.getContextPath()%>/ressources/processing-1.gif" style="display:none;z-index:90001;" title="processing..."/>
				<div id="main">
					<h2>
						<span onclick="checkDebugTool();">FCR&nbsp;<span id="title-form-id" /></span>&nbsp;<span id="form-title"></span>
						
					</h2>					
					<form id="main-form" action="#" >
						<div id="display-group-members" class="control-dialog control-list-member" style="display:none;width:350px;">
							<div id="groups-members-list" style="margin:10px;width:100%;height:100%;">
							</div>
							<table class="group-component group-button">
								<tr>
									<td>
										<input type="button" class="button control-button small-button" value="Close" onclick="hide('display-group-members');mask_off();">
									</td>
								</tr>
							</table>
						</div>
						<div id="main-section" class="main-form-element-mtn" style="height:90%;overflow:auto;">							
							<div id="admin-info" class="group-component">
								<h3 onclick="">Administrative information</h3>
								<table>
									<tr>
										<th>Requestor</th>
										<td>
											<input id="requestor" class="control control-readonly control-text" type="text" value="" />										
										</td>
									</tr>
									<tr id="warning-error-tr-requestor" class="warning-error" style="display:none">
										<td colspan="2">Error occurs : requestor is undefined.</td>
									</tr>								
									<tr id="tr-on-behalf-of" class="advanced-form-fields">
										<th>On behalf of</th>
										<td>
											<input id="on-behalf-of" class="on-behalf-of control control-text control-editable ldap-user" type="text" value="" autocomplete="off" />
											<img id="help-on-behalf-of"	src="<%= request.getContextPath()%>/ressources/information.png"
													class="img control-help" title="help me"/>
										</td>
									</tr>
									<tr id="warning-error-tr-on-behalf-of" class="warning-error" style="display:none">
										<td colspan="2">Incorrect value</td>
									</tr>
									<tr>
										<th>Application or Service<font color="red">&nbsp;*</font></th>
										<td><input id="application-service" class="application-service control control-text control-editable" 
											type="text" value="" 
											style="display:inline;"
											autocomplete="off"/>
											<img id="help-application-service"	src="<%= request.getContextPath()%>/ressources/information.png"
											class="img control-help" title=""/>
											<span id="edit-application-service-button" style="display:none">
												<img 	style="display:inline" class="img img-clickable control-display-editable" 
														src="<%= request.getContextPath()%>/ressources/document_edit.png" 
														title="edit application service" 														
														onclick="setChangeApplicationService()" />
											</span>
											<span id="edit-application-service-button-mtn" style="display:none">
												<img 	style="display:inline" class="img img-clickable control-display-editable" 
														src="<%= request.getContextPath()%>/ressources/document_edit.png" 
														title="edit application service" 														
														onclick="setChangeApplicationService_mtn()" />
											</span>
										</td>									
									</tr>
									<tr id="warning-error-tr-application-service" class="warning-error" style="display:none">
										<td colspan="2">Missing value or incorrect value</td>
									</tr>
									<tr>
										<th>Request Context<font color="red">&nbsp;*</font></th>
										<td>
											<input id="request-context-ro" class="request-context-ro control-select-readonly control-text" disabled style="display:none;"/>
											<select id="request-context" class="request-context control control-editable control-select" onchange="get_list_context_detail()">
												<option selected value="">[Select ...]</option>
												<option value="Project">Project</option>
												<option value="CTR">CTR</option>
												<option value="Remote Access">Remote Access</option>
												<option value="Other">Other</option>
											</select>
											<img id="help-request-context" src="<%= request.getContextPath()%>/ressources/information.png"
											class="img control-help" title=""></img>
										</td>
									</tr>
									<tr id="warning-error-tr-request-context" class="warning-error warning-error-tr-request-context" style="display:none">
										<td colspan="2">Missing Value</td>
									</tr>
									<tr>
										<th>Context detail<font color="red">&nbsp;*</font></th>
										<td>
											<input id="context-detail" class="control control-text control-editable" type="text" value="" autocomplete="off" />
											<img id="help-context-detail" src="<%= request.getContextPath()%>/ressources/information.png"
												class="img control-help" style="vertical-align:middle;" title=""></img>
										</td>
									</tr>
									<tr id="warning-error-tr-context-detail" class="warning-error" style="display:none">
										<td colspan="2">Missing or incorrect value</td>
									</tr>
									<tr class="advanced-form-fields">
										<th>Detailed reason for this request</th>
										<td>
											<textarea id="detailed-reason-request" class="control control-editable control-textarea"></textarea>
											<img id="help-detailed-reason-request" src="<%= request.getContextPath()%>/ressources/information.png"
											class="control-help textarea-img" title=""></img>
										</td>
									</tr>
									<tr id="simple-form" class="simple-form-fields" style="display:none">
										<th><a name="simple_edition"></a>Flow details<font color="red">&nbsp;*</font></td>
										<td>
											<textarea id="flow-details" class="control control-editable control-textarea" ></textarea>											
										</td>
										<td>
											<img id="help-flow-details" src="<%= request.getContextPath()%>/ressources/information.png" class="textarea-img control-help" title=""></img>
										</td>
										<td>
											<h3 id="advanced-edition-link" style="display:none"><span class="control-link control-display-editable" onclick="switchFormEdition('advanced', false);">(Advanced Edition)</span></h3>
										</td>
									</tr>
									<tr id="warning-error-tr-flow-details" class="warning-error" style="display:none">
										<td colspan="2">Missing value</td>
									</tr>
									<tr id="flow-documentation-upload">
										<th style="white-space:nowrap;">Flow&nbsp;documentation</th>
										<td>
											<span id="dummy-doc-form" style="display:none">
												<form id='pingping' method="post" enctype="multipart/form-data" action="<%= path%>/document-submit.htm">
													<input type="hidden" value="DEPRECATED BUT DO NO REMOVE THIS FORM"/>
													<input id="flow-documentation" name="flow-documentation" class="control control-file control-file-upload" type="file" />
													<input id="flow-documentation-request-id" name="flow-documentation-request-id" type="text" value=""/>
													<input  type="submit" value="Submit"/>
													<input type="button" value="submitFlowDocumentation" onclick="submitFlowDocumentation()"/>
													<input type="button" value="submitDocument" onclick="submitDocument()"/>
												</form>
											</span>
											<form id='pingForm' method="post" encoding="multipart/form-data" encType="multipart/form-data" action="<%= path%>/document-submit.htm">
												<input class="control control-file control-file-upload" 
												id="myFile" name="myFile" type="file"
												onchange="uploadFile();"
												/>
												<img id="help-flow-documentation" src="<%= request.getContextPath()%>/ressources/information.png"
													class="control-help textarea-img" title=""></img>
												<input style="display:none;" id="submit-flow-documentation" type="submit" value="Submit"/>
												<input type="hidden" id="enc-type" name="enc-type" value="multipart/form-data"/>
											</form>
										</td>
									</tr>
									<tr id="warning-error-tr-flow-documentation" class="warning-error" style="display:none">
										<td colspan="2">The document size must be smaller than 10 Mb</td>
									</tr>
									<tr id="flow-documentation-download" class="control-file control-file-download" style="display:none">
										<th style="white-space:nowrap;">Flow documentation</th>
										<td>
											<span id="flow-documentation-file" name="flow-documentation-file" class="flow-documentation-ddl" style="height:16px;line-height:16px;">
												<label id="flow-documentation-name" style="display:inline;cursor:pointer;color:blue;" onclick="getFlowDocumentation()"></label>
												<img id="delete-document-button" class="img img-clickable control-display-editable" 
													title="delete file"	style="display:inline;" 
													src="<%= request.getContextPath()%>/ressources/document_delete.png" 
													 onclick="deleteFlowDocumentation()" ></img>											
											</span>								
										</td>
									</tr>
								</table>							
							</div>