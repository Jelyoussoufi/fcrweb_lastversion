package be.celsius.util;

import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.bean.SeedBean;

public class TransientLogger extends AbstractLogger
{

	//BoxBean>BasketBean>BoxBean>SeedBean
	
	private BoxBean logs;
	private int expire_type;
	private int expire_time;
	
	public TransientLogger()
	{
		logs = new BoxBean();
		
		if (Config.testExpiration)
		{
			setExpirationType(Config.TIMER_DELAY_TYPE_LCL);
			setExpirationDelay(Config.TIMER_DELAY_VALUE_LCL);
		}
		else
		{
			setExpirationType(Config.TIMER_DELAY_TYPE_LCL);
			setExpirationDelay(Config.TIMER_DELAY_VALUE_LCL);
		}
	}
	
	public void log(String id, LeafBean log)
	{
		addLeastElements(log);
		
		if (logs.get(id).isNullSeed())
			logs.put(id, new BasketBean().setBeanId("logs"));
		
		//one log at the time
		logs.get(id).toBasket().clear();
		
		logs.get(id).toBasket().add(log);
	}
	
	private void addLeastElements(LeafBean logElem)
	{
		if (logElem.isBox())
		{
			BoxBean log = (BoxBean) logElem;
			if (log.get("expirable").isNullSeed())
				log.put("expirable", new SeedBean(false).setBeanId("value"));
		}
	}
	
	public void log(String id, String message)
	{
		if (logs.get(id).isNullSeed())
			logs.put(id, new BasketBean().setBeanId("logs"));
		
		BoxBean log = new BoxBean().setBeanId("log").toBox();
		log.put("message", new SeedBean(message).setBeanId("value"));
		log.put("expirable", new SeedBean(false).setBeanId("value"));

		//logs.get(id).toBasket().clear();		
		logs.get(id).toBasket().add(log);
	}
	
	public void concat(String id, int idx, String message)
	{
		if (!logs.get(id).isNullSeed())
		{
			SeedBean content = logs.get(id).toBasket().getBasket().get(idx).toBox().get("message").toSeed();
			content.setSeed_string(content.getSeed() + message);
		}
	}
	
	public void replace(String id, int idx, String message)
	{
		if (!logs.get(id).isNullSeed())
			logs.get(id).toBasket().getBasket().get(idx).toBox().get("message").toSeed().setSeed_string(message);
	}
	
	public LeafBean remove(String id, int idx)
	{
		LeafBean log = UtilApp.NULLSEED;
		try
		{
			log = logs.get(id).toBasket().getBasket().remove(idx);
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
		}
		return log;
	}
	
	public void clearLogs(String id)
	{
		if (!logs.get(id).isNullSeed())
			logs.get(id).toBasket().clear();
	}
	
	public int sizeOfLogs(String id)
	{
		try
		{
			return getLogs(id).toBasket().getBasket().size();
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
		}
		return -1;
	}
	
	/**
	 * @param id
	 * @return logs without any process
	 * => suggested to perform operations eg: concat, setExpiration, ...
	 */
	public LeafBean getLogs(String id)
	{
		try
		{
			return logs.get(id);
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
		}
		return UtilApp.NULLSEED;
	}
	
	/**
	 * @param id
	 * @return logs of id by applying all rules
	 * => suggested to display the logs
	 */
	public LeafBean exportLogs(String id)
	{
		try
		{
			applyRules(id);
			return logs.get(id);
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
		}
		return UtilApp.NULLSEED;
	}
	
	private void applyRules(String id)
	{
		try
		{
			if (!logs.get(id).isNullSeed())
			{
				for (int i=0 ; i<logs.get(id).toBasket().getBasket().size() ; i++)
				{
					checkExpiration(id, i);
					// other filterable method call
				}
			}
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
		}
	}
	
	public void setExpirable(String id, int idx, boolean expirable)
	{
		if (!logs.get(id).isNullSeed())
		{
			BoxBean log = logs.get(id).toBasket().getBasket().get(idx).toBox();
			log.get("expirable").toSeed().setSeed_boolean(expirable);
			
			if (expirable)
			{
				log.put("isExpired", new SeedBean(false).setBeanId("value"));
				log.put("insert_date", new SeedBean(UtilDT.now()).setBeanId("value"));
			}
		}
	}
	
	private void checkExpiration(String id, int idx)
	{
		UtilLog.out("checkExpiration:" + id + " - " + idx);
		try
		{
			BoxBean log = logs.get(id).toBasket().getBasket().get(idx).toBox();
			if (log.get("expirable").toSeed().getSeed_boolean())
			{
				String insertDate	= log.get("insert_date").toSeed().getSeed();
				String now 			= UtilDT.now();
				boolean lower 		= UtilDT.isLower(insertDate, now, expire_type, expire_time);
				if (lower)
				{
					log.get("isExpired").toSeed().setSeed_boolean(true);
					log.put("expiration_date", new SeedBean(now).setBeanId("value"));
				}
			}
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
		}
	}
	
	public void setExpirationType(int type)
	{
		this.expire_type = type;
	}
	
	public void setExpirationDelay(int delay)
	{
		this.expire_time = delay;
	}
	
}
