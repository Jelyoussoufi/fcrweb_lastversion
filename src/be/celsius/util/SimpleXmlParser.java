package be.celsius.util;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import be.celsius.util.datastructure.Umap;

public class SimpleXmlParser 
{
	private Element root;
	private Document document;
	private XPath xpath;
	
	public SimpleXmlParser(String xml)
	{
		document	= getDocument(xml);
		root 		= document.getDocumentElement();
		xpath 		= getXPath();
	}
	
	private Document getDocument(String xmlResponse)
	{
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(false);
			factory.setValidating(false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(xmlResponse.getBytes("UTF-8"));
			Document document 		= builder.parse(is);
			is.close();
			
			return document;
		}
		catch(Exception e){UtilLog.printException(e);}
		return null;
	}
	
	private XPath getXPath()
	{
		XPathFactory xPathFactory = XPathFactory.newInstance();
		return xPathFactory.newXPath();
	}
	
	public String getText(String nodeName){return getText(root, nodeName);}
	public String getTextByXPath(String xPathExpression){return getTextByXPath(root, xPathExpression);}	
	public String getText(Node node, String nodeName){return getTextByXPath(node, "//" + nodeName + "/text()");}
	
	public String getTextByXPath(Node node, String xPathExpression)
	{
		try
		{
			if (!xPathExpression.endsWith("/text()"))
				xPathExpression += "/text()";
			return getNodeByXPath(node, xPathExpression).getNodeValue();
		}
		catch(Exception e)
		{
			UtilLog.out("Exception occurs while getText: " + xPathExpression);
			//UtilLog.printException(e);
		}
		return "";
	}
	
	public Node getNode(String nodeName){return getNode(root, nodeName);}
	public Node getNode(Node node, String nodeName){return getNodeByXPath(node, "//" + nodeName);}	
	public Node getNodeByXPath(Node node, String xPathExpression)
	{
		if (node==null)
			node = root;
		
		try
		{
			Node n = XPathAPI.selectSingleNode(node, xPathExpression);
			return n;
		}
		catch(Exception e)
		{
			UtilLog.out("Exception occurs while getNode: " + xPathExpression);
			//UtilLog.printException(e);
		}
		return null;
	}
	
	
	public String getNodeAttr(String nodeName, String attrName){return getNodeAttr(root, nodeName, attrName);}
	public String getNodeAttr(Node node, String attrName){return getNodeAttr(node, "", attrName);}
	public String getNodeAttr(Node node, String nodeName, String attrName)
	{
		try
		{
			/*
			 	NDA: don't access text() because attributes remains inside open tag, 
			 	not in text content (<tag attr='attrVal'>text</tag>)
			 */
			if (node==null)	node = root;
			
			if (!UtilStr.isEmpty(nodeName))
				return XPathAPI.selectSingleNode(node, "//" + nodeName).getAttributes().getNamedItem(attrName).getNodeValue();
			else
				return node.getAttributes().getNamedItem(attrName).getNodeValue();
		}
		catch(Exception e)
		{
			UtilLog.out("Exception occurs while getNodeAttr: " + nodeName + " attr: " + attrName);
			//UtilLog.printException(e);
		}
		return "";
	}
	
	/**
	 * !!! parsing document !!!
	 * @param groupNamePath path from root to parent node of groups elements (e.g: actions)
	 * @param nodesName	child nodes (e.g: action)
	 */
	public List<Node> getNodes(Node node, String nodesName)
	{
		return getNodesByXPath(node, "//" + nodesName);
	}
	
	/**
	 * !!! parsing document !!!
	 * @param groupNamePath path from root to parent node of groups elements (e.g: Body/.../actions)
	 * @param nodesName	child nodes (e.g: action)
	 */
	public List<Node> getNodesByXPath(Node node, String xPathExpression)
	{
		try{
			if (node==null)
				node = root;
			if (!xPathExpression.endsWith("/text()"))
				xPathExpression += "/text()";
			
			NodeList nodes = XPathAPI.selectNodeList(node, xPathExpression);
			ArrayList<Node> elements = new ArrayList<Node>();
			for (int i=0 ; i<nodes.getLength() ; i++)
				elements.add(nodes.item(i));
			return elements;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ArrayList<Node>();
		}
	}
	
	public Document getDocument(){return document;}
	public static void main(String[] args) 
	{
		String xml = "";
		/*
		xml += "	<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:cyb='http://eland.ux.mobistar.be/sectools/webservices/CYBERARK_WS'>	";
		xml += "	   <soapenv:Header/>	";
		xml += "	   <soapenv:Body>	";
		xml += "	      <cyb:newSafeResponse soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>	";
		xml += "	         <return xsi:type='cyb:StdFeedback' xmlns:cyb='http://eland.ux.mobistar.be/sectools/webservices/cyberark_ws.php'>	";
		xml += "	            <value xsi:type='xsd:integer'>0</value>	";
		xml += "	            <message xsi:type='xsd:string'>OK</message>	";
		xml += "	         </return>	";
		xml += "	      </cyb:newSafeResponse>	";
		xml += "	   </soapenv:Body>	";
		xml += "	</soapenv:Envelope>	";
		*/
		/*
	<soapenv:Envelope 
	xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' 
	xmlns:xsd='http://www.w3.org/2001/XMLSchema' 
	xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>
	<soapenv:Header/>
	<soapenv:Body>
		<BonitaOnCerberus>
			<Request soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>
				<action_type>RemoveResourceFromApplication</action_type>
				<info>
					<requestorId>1</requestorId>
					<resourceId>109</resourceId>
					<applicationId>43</applicationId>
				</info>
				<chainActions>
					<chainAction>
						<action_type>RemoveResourceFromSafe</action_type>
						<required>1</required>
						<actionInfo>
							<requestorId>1</requestorId>
							<resourceId>109</resourceId>
							<safeId>216</safeId>
						</actionInfo>
					</chainAction>
				</chainActions>
			</Request>
			<Log>
				<statusCode>0</statusCode>
				<statusDescription>OK</statusDescription>
				<steps>null;GetApplicationStaffOwnerId;GetApplicationOwnerTeamId_done;GetStaffTeamId_done;CheckApplicationTeamOwnerConsistency_done;GetResourceSafeId;GetApplicationSafesId_done;CheckSafeConstraintIntegrity_done;RemoveResourceFromApplication_done;CheckRemoveResourceFromSafeRequired_done;InitNotificationRequest_done</steps>
			</Log>
		</BonitaOnCerberus>
	</soapenv:Body>
</soapenv:Envelope>
		 */
		xml += "<soapenv:Envelope  ";
		xml += "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'  ";
		xml += "xmlns:xsd='http://www.w3.org/2001/XMLSchema'  ";
		xml += "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'> ";
		xml += "<soapenv:Header/> ";
		xml += "<soapenv:Body> ";
		xml += "<BonitaOnCerberus> ";
		xml += "<Request soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'> ";
		xml += "<action_type>RemoveResourceFromApplication</action_type> ";
		xml += "<info> ";
		xml += "<requestorId>staffId</requestorId> ";
		xml += "<resourceId>resourceId</resourceId> ";
		xml += "<applicationId>applicationId</applicationId> ";
		xml += "</info> ";
		xml += "<chainActions> ";
		//xml += "<nbChainAction>2</nbChainAction>";
		xml += "<chainAction> ";
		xml += "<action_type>RemoveResourceFromSafe</action_type> ";
		xml += "<required>1</required> ";
		xml += "<actionInfo> ";
		xml += "<requestorId>staffId1</requestorId> ";
		xml += "<resourceId>resourceId1</resourceId> ";
		xml += "<safeId>safeId1</safeId> ";
		xml += "</actionInfo> ";
		xml += "</chainAction> ";
		xml += "<chainAction> ";
		xml += "<action_type>DoStuff</action_type> ";
		xml += "<required>0</required> ";
		xml += "<actionInfo> ";
		xml += "<requestorId>staffId2</requestorId> ";
		xml += "<resourceId>resourceId2</resourceId> ";
		xml += "<safeId>safeId2</safeId> ";
		xml += "</actionInfo> ";
		xml += "</chainAction> ";
		xml += "</chainActions> ";
		xml += "</Request> ";
		xml += "<Log> ";
		xml += "<statusCode>statusCode</statusCode> ";
		xml += "<statusDescription>statusDescription</statusDescription> ";
		xml += "</Log> ";
		xml += "</BonitaOnCerberus> ";
		xml += "</soapenv:Body> ";
		xml += "</soapenv:Envelope> ";
		xml = "<?xml version='1.0' encoding='UTF-8'?><form><administrative-information><context-detail>gis</context-detail><request-context>project</request-context><detailed-reason-request>access to gis server</detailed-reason-request><flow-documentation size='12107' mediatype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' filename='generator.xlsx'>file:/C:/Users/jhuilian/Desktop/BOS-5.9-Tomcat-6.0.35/temp/xforms_upload_5800603679775726521.tmp?filename=generator.xlsx&amp;mediatype=application%2Fvnd.openxmlformats-officedocument.spreadsheetml.sheet&amp;size=12107&amp;mac=4f08dd9a652c60d925ac1c278ebb3a5a7794befd</flow-documentation><application-service>Gis Application</application-service><on-behalf-of/><requestor>admin</requestor></administrative-information><technical-information><form-selector>sef</form-selector></technical-information><simple-edition-form/><hidden><form-valid>true</form-valid><srv-status-code>0</srv-status-code><edition-actor>req</edition-actor><me/><button-submit-form/><flow-mode/><least-one-flow/><list-flow-count/><form-mode>main</form-mode><button-drop-form/><srv-status-description>OK</srv-status-description><read-only>false</read-only><requestor-group/><form-id>9305</form-id><edition-actor-id>admin</edition-actor-id></hidden></form>";
		SimpleXmlParser parser = new SimpleXmlParser(xml);
		//System.out.println(parser.getTextByXPath(null, "Body/newSafeResponse/return/value/text()"));
		System.out.println(parser.getNodeAttr("flow-documentation", "filename"));
		System.out.println(parser.getText(parser.getNode("technical-information"),"form-selector"));
		System.out.println(parser.getNode("flow-documentation").getAttributes().getNamedItem("filename").getNodeValue());
		System.out.println(parser.getText("on-behalf-of"));
		//System.out.println(Util.getInteger(parser.getText(null, "nbChainAction")));
		/*
		List<Node> chainActions = parser.getNodes(null, "chainActions");
		
		System.out.println(chainActions.size());
		for (Node e : chainActions)
		{
			System.out.println(e.getChildNodes().getLength());
			System.out.println(parser.getText(e, "action_type"));	//RemoveResourceFromSafe not RemoveResourceFromApplication
			System.out.println(parser.getText(e, "required"));
			
		}
		*/
		/*
		NodeList chainActions = parser.getNodes("chainAction");
		System.out.println(chainActions.getLength());
		*/
	}

}
