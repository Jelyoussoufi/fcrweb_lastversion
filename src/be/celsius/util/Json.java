package be.celsius.util;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 
 * @author jhuilian
 * @deprecated
 */
public class Json {

	private JSONObject json;
	
	public Json()
	{
		json = new JSONObject();
	}
	
	public JSONObject getJson() {
		return json;
	}

	public Json put(String key, String value)
	{
		try	{ json.put(key, value); } catch (Exception e) {	UtilLog.printException(e); }
		return this;
	}
	
	/**
	 * add at begin of list
	 */
	public Json accumulate(String key, JSONObject obj)
	{
		try	{ json.accumulate(key, obj); } catch (Exception e) {	UtilLog.printException(e); }
		return this;
	}
	
	/**
	 * add at end of list
	 */
	public Json append(String key, JSONObject obj)
	{
		try	{ json.append(key, obj); } catch (Exception e) {	UtilLog.printException(e); }
		return this;
	}	
	public Json accumulate(String key, JSONArray obj)
	{
		try	{ json.accumulate(key, obj); } catch (Exception e) {	UtilLog.printException(e); }
		return this;
	}
	public Json append(String key, JSONArray obj)
	{
		try	{ json.append(key, obj); } catch (Exception e) {	UtilLog.printException(e); }
		return this;
	}
	public Json accumulate(String key, Json obj)
	{
		try	{ json.accumulate(key, obj.getJson()); } catch (Exception e) {	UtilLog.printException(e); }
		return this;
	}
	public Json append(String key, Json obj)
	{
		try	{ json.append(key, obj.getJson()); } catch (Exception e) {	UtilLog.printException(e); }
		return this;
	}	

	public String getJsonAttr(JSONObject source, String path)
	{
		String[] attrs = path.split("/");
		
		JSONObject obj = source;
		
		try
		{
			for (int i=0; i<attrs.length-1; i++)
			{
				obj = obj.getJSONObject(attrs[i]);
			}
			return obj.getString(attrs[attrs.length-1]); 
		}
		catch (Exception e){}
		
		return "";
	}
	
	public String getJsonAttr(JSONArray source, String path, int idx)
	{
		String[] attrs = path.split("/");
		
		try{
			JSONObject obj = source.getJSONObject(idx);
			
			for (int i=0; i<attrs.length-1; i++)
			{
				obj = obj.getJSONObject(attrs[i]);
			}
			return obj.getString(attrs[attrs.length-1]); 
		}
		catch (Exception e){}
		
		return "";
	}
	
	public String getJsonAttr(String path)
	{
		return getJsonAttr(json, path);
	}
	
	public JSONArray getJsonArray(String key)
	{
		try{
			return json.getJSONArray(key);
		}
		catch (Exception e){}
		return new JSONArray();
	}
	
	public String toString()
	{
		return json.toString();
	}
	
	public String toString(int indentFactor)
	{
		try	{ return json.toString(indentFactor); }
		catch (Exception e) { UtilLog.printException(e); }
		return toString();
	}
	
	public static void main(String[] args)
	{
		
		Json json = new Json();
		json.put("key1", "value1").put("key2", "value2").append("appendice", new JSONObject()).accumulate("accu", new JSONObject());
		System.out.println(json.toString());
		/* */
		
		//System.out.println(UtilApp.NULLSEED.toJson().toString());
	}
}
