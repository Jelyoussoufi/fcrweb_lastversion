/**
 * 
 */
package be.celsius.util.struct;

/**
 * @author jhuilian
 *
 */
public class Leaf extends SimpleType
{
	private String text;

	public Leaf(String id)
	{
		super(id);
	}
	
	public boolean isLeaf()
	{
		return true;
	}
	
	public String getText() {
		return text;
	}
	
	public Leaf setText(String text) {
		this.text = text;
		return this;
	}
	
	protected String getXmlBody()
	{
		return getText();
	}
}
