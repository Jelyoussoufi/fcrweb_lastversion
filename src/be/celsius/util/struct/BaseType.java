/**
 * 
 */
package be.celsius.util.struct;

import be.celsius.util.UtilStr;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Umap.KVP;

/**
 * @author jhuilian
 *
 */
public abstract class BaseType 
{
	private String id;		//once set, the identifier could not be changed
	private Umap meta;

	public BaseType(String id)
	{
		this.id = id;
	}
	
	public void reset()
	{
		if (hasMeta())
			meta.clear();
	}
		
	public Umap getMeta()
	{
		if (meta==null)
			meta = new Umap().setKVP(KVP.KVP_XML_ATTRS);
		return meta;
	}
	
	public BaseType addMeta(String id, String value)
	{
		getMeta().push(id, value);
		return this;
	}
	
	public BaseType dropMeta(String id)
	{
		getMeta().drop(id);
		return this;
	}
	
	private boolean hasMeta(){return meta!=null && meta.size()!=0;}
	
	public Leaf toLeaf(){try{return (Leaf)this;}catch(Exception e){}return null;}
	public ListType toList(){try{return (ListType)this;}catch(Exception e){}return null;}
	public MapType toMap(){try{return (MapType)this;}catch(Exception e){}return null;} 
	
	public boolean isLeaf(){return false;}
	public boolean isList(){return false;}
	public boolean isMap(){return false;}
	public boolean isNullType(){return false;}
	public boolean isSimpleType(){return false;}
	public boolean isComplexType(){return false;}
	
	private String meta2string()
	{
		if (hasMeta())
		{			
			String list_attr = "";
			for (String attrName : meta.keySet())
				list_attr += meta.getOs() + attrName + meta.getIs() + UtilStr.wrap(meta.get(attrName), "\"");
			return list_attr;
		}
		return "";
	}
	
	public String toXml()
	{
		String xmlBody = getXmlBody();
		return UtilStr.isEmpty(xmlBody) ? 
				UtilStr.wrap(id + meta2string(), "<", "/>") : 
				UtilStr.wrap(id + meta2string(), "<", ">") + xmlBody + UtilStr.wrap(id, "</", ">");
	}
	
	protected abstract String getXmlBody();
	
}
