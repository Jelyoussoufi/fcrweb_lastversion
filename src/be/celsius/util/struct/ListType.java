/**
 * 
 */
package be.celsius.util.struct;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jhuilian
 *
 */
public class ListType extends ComplexType{

	private List<BaseType> childrens;
	
	public ListType(String id)
	{
		super(id);
		childrens = new ArrayList<BaseType>();
	}
	
	public void reset() 
	{
		super.reset();
		childrens.clear();
	}
	
	public ListType pushLeaf(String key, String value)
	{
		return push(new Leaf(key).setText(value));
	}
	
	public ListType push(BaseType child)
	{
		childrens.add(child);
		return this;
	}
	
	public BaseType drop(int index)
	{
		childrens.remove(index);
		return this;
	}

	public BaseType get(int index)
	{
		return childrens.get(index);
	}
	
	public BaseType remove(int index)
	{
		return childrens.remove(index);
	}

	public String getXmlBody()
	{
		String xmlBody = "";
		for (BaseType elem : childrens)
			xmlBody += elem.toXml();
		return xmlBody;
	}
}
