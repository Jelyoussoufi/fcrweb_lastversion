/**
 * 
 */
package be.celsius.util.struct;

import java.util.HashMap;
import java.util.Map;

import be.celsius.util.datastructure.Umap;

/**
 * @author jhuilian
 *
 */
public class MapType extends ComplexType{

	private Map<String,BaseType> childrens;
	
	public MapType(String key)
	{
		super(key);
		childrens = new HashMap<String,BaseType>();
	}
	
	public MapType(Umap mapster)
	{
		this(mapster.getName());
		addAll(mapster);
	}
	
	public void reset()
	{
		super.reset();
		childrens.clear();
	}
	
	public MapType addAll(Map<String,String> map)
	{
		for (String key : map.keySet())
			pushLeaf(key, map.get(key));
		return this;
	}
	
	/**
	 * shortcut to insert a new String entry
	 */
	public MapType pushLeaf(String key, String value)
	{
		return push(key, new Leaf(key).setText(value==null?"":value));
	}
	
	public MapType push(String key, BaseType child)
	{
		childrens.put(key, child==null?new NullType():child);
		return this;
	}
	
	public BaseType get(String key)
	{
		return childrens.get(key)==null ? new NullType() : childrens.get(key);
	}
	
	public BaseType remove(String key)
	{
		return childrens.get(key)==null ? new NullType() : childrens.remove(key);
	}
	
	public MapType drop(String key)
	{
		childrens.remove(key);
		return this;
	}
	
	public boolean hasKey(String key)
	{
		return childrens.containsKey(key);
	}
	
	/**
	 * @return true iff the child is 
	 */
	public boolean hasChild(String key)
	{
		return !get(key).isNullType();
	}
	
	protected String getXmlBody()
	{
		String xml = "";
		for (BaseType elem : childrens.values())
			xml += elem.toXml();
		return xml;
	}
}
