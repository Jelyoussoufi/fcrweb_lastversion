/**
 * 
 */
package be.celsius.util.struct;

/**
 * @author jhuilian
 *
 */
public abstract class ComplexType extends BaseType
{
	public ComplexType(String key)
	{
		super(key);
	}
	
	public void reset()
	{
		super.reset();
	}
	
	public boolean isComplexType(){return true;}
	
	public abstract ComplexType pushLeaf(String key, String value);
}
