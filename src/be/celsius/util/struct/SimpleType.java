/**
 * 
 */
package be.celsius.util.struct;

/**
 * @author jhuilian
 *
 */
public abstract class SimpleType extends BaseType{

	public SimpleType(String id)
	{
		super(id);
	}
	
	public void reset() 
	{
		super.reset();
	}

	public boolean isSimpleType()
	{
		return true;
	}
}
