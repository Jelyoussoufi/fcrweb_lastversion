/**
 * 
 */
package be.celsius.util.struct;

/**
 * @author jhuilian
 *
 */
public class NullType extends BaseType
{
	public NullType()
	{
		super("null");
	}
	
	public boolean isNullType()
	{
		return true;
	}
	
	protected String getXmlBody()
	{
		return "";
	}
}
