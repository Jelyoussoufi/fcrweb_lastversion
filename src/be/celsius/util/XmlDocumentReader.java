package be.celsius.util;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.org.apache.xpath.internal.XPathAPI;


public class XmlDocumentReader {
	
	DocumentBuilderFactory docFactory;
	DocumentBuilder docBuilder;
	
	XPathFactory xPathFactory;
	XPath xpath;
	
	
	public XmlDocumentReader()
	{
		try{
		docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setNamespaceAware(false);
		docFactory.setValidating(false);
		docBuilder = docFactory.newDocumentBuilder();
		
		xPathFactory = XPathFactory.newInstance();
		xpath = xPathFactory.newXPath();
		}
		catch (Exception e) {}
	}
	
	public Document createXmlDocFromString(String chain)
	{
		try{
			ByteArrayInputStream is = new ByteArrayInputStream(chain.getBytes("UTF-8")); 
			//may thow UnsupportedEncodingException.
			
			Document document = docBuilder.parse(is); 
			//may throw SAXException.
			
			is.close();
			//may throw IOException.
			
			return document;
		}
		catch (Exception e) {if (Config.debug)printErrorDetails(e, chain);}
		
		return null;
	}	
	
	private void printErrorDetails(Exception e, String chain)
	{
		e.printStackTrace();
		UtilLog.out("RAISING ERROR SOURCEDOC");
		UtilLog.debug1(chain);
	}

	public String getNodeValue(Element element, String tag)
	{
		try 
		{
			return XPathAPI.selectSingleNode(element, "/"+tag).getNodeValue();
		
		} 
		catch (Exception e) 
		{
			return "";
		}
	}
	
	/**
	 * start from document root.
	 */
	public String getNodeValue(Document document, String expression)
	{
		try
		{
			XPathExpression exp = xpath.compile(expression);
			
			return (String)exp.evaluate(document,XPathConstants.STRING);
		}
		catch(Exception e)
		{
			return "";
		}
	}
	
	public String getValue(Document document, String expression)
	{
		return UtilStr.undoFilter(getAttributeValue(document, expression, "value"));
	}
	
	public String getAttributeValue(Document document, String expression, String attribute)
	{
		try
		{
			XPathExpression exp = xpath.compile(expression);

			Element e = (Element)(Node)exp.evaluate(document,XPathConstants.NODE);

			return UtilStr.undoFilter(e.getAttribute(attribute));	
		}
		catch(Exception e)
		{
			return "";
		}
	}
	
	public NodeList getNodeList(Document document, String expression, String groupTag)
	{
		try{
			XPathExpression exp = xpath.compile(expression);
			Element e = (Element)(Node)exp.evaluate(document,XPathConstants.NODE);
			return e.getElementsByTagName(groupTag);
		}
		catch(Exception e)
		{
			return null;
		}
	}
}
