package be.celsius.util;
import java.util.StringTokenizer;

import be.celsius.util.datastructure.Ulist;
import be.celsius.util.datastructure.Umap;

/**
 * TCP/IP Address Utility Class
 *
 * @author gkspencer
 */
public class UtilNet {

/**
 * IP IN SUBNETS  
 */
	private final static String[] SUBNETS = 
	{
		"0.0.0.0", "128.0.0.0", "192.0.0.0", "224.0.0.0",
		"240.0.0.0", "248.0.0.0", "252.0.0.0", "254.0.0.0",
		"255.0.0.0", "255.128.0.0", "255.192.0.0", "255.224.0.0",
		"255.240.0.0", "255.248.0.0", "255.252.0.0", "255.254.0.0",
		"255.255.0.0", "255.255.128.0", "255.255.192.0", "255.255.224.0",
		"255.255.240.0", "255.255.248.0", "255.255.252.0", "255.255.254.0",
		"255.255.255.0", "255.255.255.128", "255.255.255.192", "255.255.255.224",
		"255.255.255.240", "255.255.255.248", "255.255.255.252", "255.255.255.254",
		"255.255.255.255"
	};
	
	private static Umap cidrs_map = new Umap();

	public static String netmask2cidr(String netmask)
	{
		if (cidrs_map.size()==0)
		{
			for (int i=0; i<SUBNETS.length ; i++)
				cidrs_map.put(SUBNETS[i], i + "");
		}
		return cidrs_map.get(netmask);
	}
	/**
	 * 
	 * @param bitmask a bitmask value (0..32)
	 * @return its decimal representation value.
	 * @example 32 -> 255.255.255.255
	 * @exception return empty chain if bitmask is not valid
	 */
	public static String bitmask2ip(String bitmask)
	{
		try{return SUBNETS[Integer.parseInt(bitmask)];}
		catch(Exception e){}
		return "";
	}

	public final static boolean isInSubnetDebug(String ipaddr, String subnet, String mask) {
		boolean isInSubnet = isInSubnet(ipaddr,subnet,mask);
		UtilLog.out(ipaddr + " isInSubnet:" + subnet + "/" + mask + "?" + isInSubnet);
		return isInSubnet;
	}
  /**
   * Check if the specified address is within the required subnet
   * 
   * @param ipaddr String pattern x.x.x.x
   * @param subnet String pattern x.x.x.x
   * @param mask String	pattern x.x.x.x
   * @return boolean
   */
  public final static boolean isInSubnet(String ipaddr, String subnet, String mask) {
    
    //  Convert the addresses to longeger values

    long ipaddrlong = parseNumericAddress(ipaddr);
    if ( ipaddrlong == 0)
      return false;
      
    long subnetlong = parseNumericAddress(subnet);
    if ( subnetlong == 0)
      return false;
      
    long masklong = parseNumericAddress(mask);
    if ( masklong == 0)
      return false;
      
    //  Check if the address is part of the subnet
    if (( ipaddrlong & masklong) == subnetlong)
      return true;
    return false;  
  }
  /**
   * Check if the specified address is a valid numeric TCP/IP address and return as an longeger value
   * 
   * @param ipaddr String
   * @return long
   */
  private final static long parseNumericAddress(String ipaddr) {
  
    //  Check if the string is valid
    
    if ( ipaddr == null || ipaddr.length() < 7 || ipaddr.length() > 15)
      return 0;

    //  Check the address string, should be n.n.n.n format
    
    StringTokenizer token = new StringTokenizer(ipaddr,".");
    if ( token.countTokens() != 4)
      return 0;

    long iplong = 0;
    
    while ( token.hasMoreTokens()) {
      
      //  Get the current token and convert to an longeger value
      
      String ipNum = token.nextToken();
      
      try {
        
        //  Validate the current address part
        
        long ipVal = Long.valueOf(ipNum).longValue();
        if ( ipVal < 0 || ipVal > 255)
          return 0;
          
        //  Add to the longeger address
        
        iplong = (iplong << 8) + ipVal;
      }
      catch (NumberFormatException ex) {
        return 0;
      }
    }

    //  Return the longeger address
    
    return iplong;
  }

  public final static boolean isIpAddress(String ip)
  {
	  return parseNumericAddress(ip) != 0;
  }
  
  public static String parseAddress (long r)
  {
	  /** 255.255.255.255 <=> 4294967295 */
	  /** 0.0.0.0 <=> 0 */
	  	String ip 	= "";
		for (int i=3 ; i>=0 ; i--)
		{
			long infix = r / ((long)Math.pow(2, 8*i));
			if (infix<0 | infix>255)
				return "";
			ip += (ip.equals("") ? "" : "." ) + infix;
			r = (long)(r % ((long)Math.pow(2, 8*i)));
		}
		return ip;
  }
  
  /**
   * IP BETWEEN IP RANGE 
   */
  
  /**
   * @pre ip and start_ranges and end_ranges contains all valid ip addresses.
   * @pre start_ranges.length == end_ranges.length
   * @return true iff it exists an index i in (0..start_ranges.length-1) such that isInRange(ip, start_ranges[i], end_ranges[i]) == true
   */
  public static boolean isInRange(String ip, Ulist start_ranges, Ulist end_ranges)
  {
	  for (int i=0 ; i<start_ranges.size() ; i++)
	  {
		  if (isInRange(ip, start_ranges.get(i), end_ranges.get(i)))
			  return true;
	  }
	  return false;
  }
  
  /**
   * @pre ip is a valid ip address
   * @pre start_range and end_range are valid ips addresses and start_range lexicographically smaller than end_range; 
   */
  public static boolean isInRange(String ip, String start_range, String end_range)
  {
	  UtilLog.out(start_range +"<=" + ip +"<="+ end_range);
	  return parseNumericAddress(start_range) <= parseNumericAddress(ip) & parseNumericAddress(ip) <= parseNumericAddress(end_range);
  }
  
  /**
   * SUBNET/MASK BETWEEN IP RANGE  
   */
  
  /**
   * @pre subnet is valid ip/mask [x.x.x.x/0..32] and start_ranges and end_ranges contains all valid ip addresses.
   * @pre start_ranges.length == end_ranges.length
   * @return true iff it exists an index i in (0..start_ranges.length-1) such that isInRange(ip, start_ranges[i], end_ranges[i]) == true
   */
  public static boolean isSubnetInOrOverlapRange(String subnet_mask, Ulist start_ranges, Ulist end_ranges)
  {
	  for (int i=0 ; i<start_ranges.size() ; i++)
	  {
		  int in_or_overlap = isSubnetInOrOverlapRange(subnet_mask, start_ranges.get(i), end_ranges.get(i));
		  if (in_or_overlap >= 1)
		  {  
UtilLog.out("isSubnetInOrOverlapRange:"+in_or_overlap);
			  return true;
		  }
	  }
	  return false;
  }
  
  /**
   * @pre subnet_mask is a valid ip/mask subnet
   * @pre start_range and end_range are valid ips addresses and start_range lexicographically smaller than end_range; 
   */
  public static int isSubnetInOrOverlapRange(String subnet, String start_range, String end_range)
  {
	  int code = 0;
	  try
	  {
		  long sr 			= parseNumericAddress(start_range);
		  long er 			= parseNumericAddress(end_range);
		  String[] ip_mask 	= subnet.split("/");		  
		  long subnet_lower	= parseNumericAddress(ip_mask[0]);
		  long mask 		= Util.getInteger(ip_mask[1]);
		  long subnet_upper = subnet_lower + ((long)Math.pow(2, 32 - mask) -1) ;	
		  // the last machine address value
		  // @ -1 instead of -2 because of broadcast address
UtilLog.out(start_range +"<="+ ip_mask[0] +"<="+ parseAddress(subnet_upper)  +"<="+ end_range);
		  //UtilLog.out(sr +"<="+ subnet_lower +"&"+ subnet_lower +"<="+ subnet_upper +"&"+ subnet_upper +"<="+ er);
		  //UtilLog.out((sr <= subnet_lower) +"&"+ (subnet_lower <= subnet_upper) +"&"+ (subnet_upper <= er));
		  if (sr <= subnet_lower & subnet_upper <= er) //subnet inside range
			  code = 1;
		  else if (sr <= subnet_lower & subnet_lower <= er)	//subnet overlaps range on upper bound
			  code = 2;
		  else if (sr <= subnet_upper & subnet_upper <= er)	//subnet overlaps range on lower bound
			  code = 3;
	  }
	  catch (Exception e)
	  {
		  UtilLog.printException(e);
		  code = -9;
	  }	  
	  return code;
  }
  
  public static void main(String[] args) {
		//UtilLog.out(IPAddress.isInSubnet("127.0.0.1", "127.0.0.0", "128.0.0.0"));
		/*
		
	    192.44.78.0/26 (les adresses de 192.44.78.0 � 192.44.78.63)
	    192.44.78.64/26 (les adresses de 192.44.78.64 � 192.44.78.127)
	    192.44.78.128/26 (les adresses de 192.44.78.128 � 192.44.78.191)
	    192.44.78.192/26 (les adresses de 192.44.78.192 � 192.44.78.255)
	 
		 */
		//UtilLog.out(UtilNet.bitmask2ip("26"));
		//UtilLog.out(UtilNet.isInSubnet("192.44.71.1", "192.44.78.192", "255.255.255.192"));

		
		Ulist start_ranges 	= new Ulist().push("10.0.0.0").push("172.16.0.0").push("192.168.0.0").push("192.44.78.0");
		Ulist end_ranges 	= new Ulist().push("10.255.255.255").push("172.31.255.255").push("192.168.255.255").push("192.44.78.63");
		/*
		UtilLog.out(UtilNet.isInRange("10.0.0.1", start_ranges, end_ranges));
		UtilLog.out(UtilNet.isInRange("172.18.255.255", start_ranges, end_ranges));
		UtilLog.out(UtilNet.isInRange("192.169.0.0", start_ranges, end_ranges));
		*/

		UtilLog.out(""+UtilNet.isSubnetInOrOverlapRange("192.44.78.63/30", start_ranges, end_ranges));	
	
	}

}