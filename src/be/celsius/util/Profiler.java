package be.celsius.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

public class Profiler extends AbstractLogger{

	private final long SEC_IN_MILLIS 	= 1000;
	private final long MIN_IN_MILLIS 	= 60 * SEC_IN_MILLIS;
	private final long HOUR_IN_MILLIS 	= 60 * MIN_IN_MILLIS;
	private final long DAY_IN_MILLIS 	= 24 * HOUR_IN_MILLIS;
	// year in millis will lead in inaccurate computation because of leap years
	
	private final int DAY 	= 0;
	private final int HR 	= 1;
	private final int MIN 	= 2;
	private final int SEC 	= 3;
	private final int MS 	= 4;
	
	private static String[] PRETTY_SEPARATORS = {" day(s) "," hr(s) "," min(s) "," sec(s) "," ms"};
	private static String[] SIMPLE_SEPARATORS = {":",":",":",":",":"};
	
	private String name;
	private long[] momentums;
	private String[] titles;
	private String profile;
	private List<String> report;
	private int idx;		// current momentum
	
	
	private int reportMaxLine = 100;
	private final String NL = "&nl;";
	private final String TAB = "&tab;";
	
	public Profiler()
	{
		this("UNAMED", Config.PROFILER_DEF_PROF_SIZE);
	}
	
	public Profiler(String name)
	{
		this(name, Config.PROFILER_DEF_PROF_SIZE);
	}
	
	public Profiler(String name, int size)
	{
		this.name 		= name;
		resize(size);
		this.profile 	= "";
		this.report		= new ArrayList<String>();
	}
		
	public void resize(int size)
	{
		if (size<2)size = 2;
		momentums 	= new long[size];
		titles 		= new String[size];
		reset();
	}
	
	public void reset() 
	{
		idx = 0;
		for (int i=0 ; i< momentums.length ; i++)
		{	
			momentums[i] 	= 0;
			titles[i]		= "";
		}
		titles[0] = "Start";
		titles[titles.length-1] = "End";
	}
	
	/**
	 * if momentums[length] reached then successif method call will be dropped.
	 */
	public void snap(String title)
	{
		try
		{
			if(idx<momentums.length)
			{
				momentums[idx] 	= System.currentTimeMillis();
				if(title.equals(""))
					title = "step "+getIdxRep(idx);
				titles[idx]	= title;
				idx++;
			}
		}
		catch (Exception e){UtilLog.printException(e);}
	}
		
	public int getIdx()
	{
		return idx;
	}
	
	/**
	 * print the time elapsed between the first moment and the last moment
	 */
	public void printElapsedTime()
	{
		printElapsedTime(getIdx()-1);
	}
	
	/**
	 * print the time elapsed between the first moment and the i th moment
	 */
	public void printElapsedTime(int i)
	{
		printElapsedTime(0, i, titles[i]);
	}
	
	/**
	 * print the time elapsed between the i th moment and the j th moment
	 */
	public void printElapsedTime(int i, int j, String title)
	{
		if (Config.profilerverb)
		{
			profil(NL);
			profil(title);
			printElapsedTimeOptions(Math.min(i, j),Math.max(i, j),true,true,0);
		}
	}
	
	/**
	 * print the time elapsed between the first moment and the i th moment while i={1..idx}
	 */
	public void printElapsedTimeList()
	{
		if (Config.profilerverb)
		{
			profil(NL);
			for(int i=0 ; i<getIdx() ; i++)
			{
				profil(titles[i]);
				printElapsedTimeOptions(0,i,true,false,0);
				profil(NL);
			}
		}
	}
	
	public void printElapsedTimeAll()
	{
		if (Config.profilerverb)
		{
			profil(NL);
			for(int i=0 ; i<getIdx() ; i++)
			{
				profil(i+ ":= ");
				for(int j=i ; i<getIdx() ; i++)
				{
					printElapsedTimeOptions(0,i,true,false,15);
					profil(TAB);
				}
				profil(NL);
			}
		}
	}
	private void printElapsedTimeOptions(int idx1, int idx2, boolean usePretty, boolean printIdx, int leastSize)
	{
		try
		{
			int start = 0;
			int end = idx-1;
			if (0<=idx1 & idx1<=idx2 & idx2<=idx)
			{
				start 	= idx1;
				end 	= idx2;
			}
			if(end<0)end = 0;
	
			long[] et = tm2d(momentums[end] - momentums[start]);
			String[] seps = SIMPLE_SEPARATORS;
			if (usePretty)
				seps = PRETTY_SEPARATORS;
			String indexes = "";
			if (printIdx)
				indexes = getIdxRep(start) + "-" + getIdxRep(end) + ": ";
			
			profil(indexes+addSpace(getRep(et, seps),leastSize));
		}
		catch (Exception e){UtilLog.printException(e);}
	}
	
	private String addSpace(String s, int sizeLimit)
	{
		if (sizeLimit<=0)return s;
		while (s.length()<sizeLimit)
			s += " ";
		return s;
	}
	
	private long[] tm2d(long tot_milli)
	{
		long nb_day = tot_milli / DAY_IN_MILLIS;
		
		long reste_milli = tot_milli - (nb_day * DAY_IN_MILLIS);
		long nb_hh = tot_milli / HOUR_IN_MILLIS;
     
        reste_milli = tot_milli - (nb_hh * HOUR_IN_MILLIS);
        long nb_min = reste_milli / (MIN_IN_MILLIS);
     
        reste_milli = reste_milli - (nb_min * MIN_IN_MILLIS);
        long nb_sec = reste_milli / SEC_IN_MILLIS;
        
        reste_milli = reste_milli - (nb_sec * SEC_IN_MILLIS);
        long nb_mil = reste_milli;
        
        //return nb_day + " day(s) " +  nb_hh + " hr(s) " + nb_min + " min(s) " + nb_sec + " sec(s) " + nb_mil + " ms";
        long[] time = {nb_day, nb_hh, nb_min, nb_sec, nb_mil};
        
        return time;
	}
	
	private String getRep(long[] time, String[] sep)
	{
		String rep = time[MS]+sep[MS];
		if (time[SEC]>0)
			rep = time[SEC]+sep[SEC]+rep;
		if (time[MIN]>0)
			rep = time[MIN]+sep[MIN]+rep;
		if (time[HR]>0)
			rep = time[HR]+sep[HR]+rep;
		if (time[DAY]>0)
			rep = time[DAY]+sep[DAY]+rep;
		
		return rep;
	}
	
	private String getIdxRep(int i)
	{
		/*
		if (i==0)return "f";
		else if (i==idx-1) return "l";
		*/
		return i+"";
	}
	
	public static void main(String[] args)
	{
		Profiler p = new Profiler("test",5);
		/*
		p.snap("Start: ");
		UtilProc.wait(1);
		p.snap("wait(1): ");
		UtilProc.wait(1);
		p.snap("wait(2): ");
		UtilProc.wait(2);
		p.snap("wait(3): ");
		UtilProc.wait(3);
		p.snap("wait(2): ");
		UtilProc.wait(2);
		p.snap("Stop: ");
		
		p.printElapsedTimeList();
		p.printElapsedTime(1,4,"");
		p.printElapsedTime(1,6,"");
		System.out.println(p.getProfile());
		*/
		
		p.reportln("hello");
		p.reportln("world");
		p.reportln("what's up");
		p.reportln("bro");
		p.reportln("donkey kong");
		p.reportln("tu peach");
		p.reportln("en tutu kaki");
		p.report(" sur le lac titicaca");
		
		System.out.println(p.getReport());
	}
	
	private String getPrettyPrint(String title, String body)
	{
		String ans = "";
		ans += NL + " ***** START " + name + " " + title + " ***** " + NL;
		ans += body;
		ans += NL + " ***** " + name + " " + title + " END ***** " + NL;
		
		return ans;
	}
	
	
	
	private void dropOuterLines()
	{
		try
		{
			while(report.size() > reportMaxLine)
				report.remove(0);
		}
		catch(Exception e){e.getStackTrace();}
	}
	
	public String getProfile(){return getPrettyPrint("PROFILE", profile);}
	public void resetProfile(){profile = "";}
	private void profil(String s){profile += s;}
	
	public String getReport(){return getPrettyPrint("REPORT", Util.list2string(report, NL, "", "", ""));}
	public void resetReport(){report.clear();}
	public void report(String s)
	{
		dropOuterLines(); 
		try
		{
			int last = report.size()-1;
			if (last<0)
				last = 0;
			if (report.size()>0)
				report.set(last, report.get(last) + s);
			else
				report.add(s);
		}
		catch(Exception e){}
	}
	
	
	public void reportln(String s){dropOuterLines(); report.add(s + NL);}
	
	public String getAll(){return getProfile() + getReport();}
	public void resetAll(){resetProfile();resetReport();}

	public int getReportMaxLine() {
		return reportMaxLine;
	}

	public void setReportMaxLine(int reportMaxLine) {
		this.reportMaxLine = reportMaxLine;
	}
}
