package be.celsius.util;

import java.util.Hashtable;
import java.util.Iterator;

import javax.naming.Context;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.bean.SeedBean;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPSearchResults;

public class Ldap {
	private int ldapVersion;
	private int ldapPort;
	private String ldapHost;
	private String login;
	private String password;
	private Profiler ldapProfiler;

	public static final int BASE = LDAPConnection.SCOPE_BASE;
	public static final int ONE = LDAPConnection.SCOPE_ONE;
	public static final int SUB = LDAPConnection.SCOPE_SUB;
	public static final int SUBTREE = LDAPConnection.SCOPE_SUBORDINATESUBTREE;

	public Ldap() {
		resetConnectionConfiguration();

		ldapVersion = LDAPConnection.LDAP_V3;
		ldapProfiler = new Profiler("LDAPManager", 0);
	}

	public void resetConnectionConfiguration() {
		ldapHost = login = password = "";
		ldapPort = -1;
	}

	public void setConnectionConfiguration(String host, int port, String user,
			String pwd) {
		ldapHost = host;
		ldapPort = port;
		login = user;
		password = pwd;

	}

	private boolean isConnectionConfigurationSet() {
		return !(UtilStr.isEmpty(ldapHost) | ldapPort == -1
				| UtilStr.isEmpty(login) | UtilStr.isEmpty(password));
	}

	public LeafBean getLdapData(String dn, int scope, String filter,
			String[] attrs) {
		BasketBean ldapResults = new BasketBean().setBeanId("ldapResults")
				.toBasket();

		if (!isConnectionConfigurationSet()) {
			ldapProfiler.reportln("Ldap Connection configuration not set.");
			return ldapResults;
		}

		try {
			ldapProfiler.reportln("connection configuration ldap://" + ldapHost
					+ ":" + ldapPort + " - " + login + ":" + password);
			ldapProfiler.reportln("search dn:" + dn + " filter:" + filter
					+ " sc:" + scope + " "
					+ Util.array2string(attrs, ",", "attrs"));
			LDAPConnection lc = new LDAPConnection();
			lc.connect(ldapHost, ldapPort);
			lc.bind(ldapVersion, login, password.getBytes("UTF8"));
			LDAPSearchResults searchResults = lc.search(dn, scope, filter,
					attrs, false);

			if (!searchResults.hasMore()) {
				ldapProfiler.reportln("results not found in LDAP");
				return ldapResults;
			}

			while (searchResults.hasMore()) {
				LDAPEntry nextEntry = null;
				BoxBean ldapEntry = new BoxBean().setBeanId("ldapEntry")
						.toBox();
				try {
					nextEntry = searchResults.next();
				} catch (Exception e) {
					ldapProfiler.reportln(UtilLog.getStackTrace(e));
					continue;
				}

				LDAPAttributeSet attributeSet = nextEntry.getAttributeSet();
				Iterator allAttributes = attributeSet.iterator();

				ldapProfiler.reportln("nextEntry.attributeSet.hasNext():"
						+ allAttributes.hasNext());

				while (allAttributes.hasNext()) {
					LDAPAttribute attribute = (LDAPAttribute) allAttributes
							.next();
					// String attributeName = attribute.getName().toLowerCase();
					// String attr = attributeName + " - " +
					// attribute.getStringValue();
					String[] values = attribute.getStringValueArray();
					BasketBean array = new BasketBean().setBeanId("values")
							.toBasket();
					ldapProfiler.reportln(attribute.getName() + ":");
					for (int i = 0; i < values.length; i++) {
						array.add(new SeedBean(UtilStr.spec2Symbol(
								UtilStr.removeAccent(values[i].trim()), '?'))
								.setBeanId("value"));
						ldapProfiler.report(values[i].trim() + ";");
					}
					ldapEntry.put(attribute.getName(), array);
				}
				ldapResults.add(ldapEntry);
			}
			lc.disconnect();

			ldapProfiler.reportln("ldapResults:"
					+ ldapResults.toJson().toString());
		} catch (Exception e) {
			ldapResults = new BasketBean().setBeanId("ldapResults").toBasket();
			ldapProfiler.reportln(UtilLog.getStackTrace(e));
		}

		return ldapResults;
	}

	/**
	 * 
	 * @param attrs
	 *            .length <=> values.length
	 * @return a notification message OK if no errors Exception: ... if error(s)
	 *         occurs
	 */
	public String addLdapEntry(String dn, String[] attributes, String[] values) {
		try {
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, "ldap://" + ldapHost + ":" + ldapPort);
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, login);
			env.put(Context.SECURITY_CREDENTIALS, password);
			DirContext ctx = new InitialDirContext(env);

			ldapProfiler.reportln("dn:" + dn);
			Attributes attrs = new BasicAttributes();
			String params = "";

			for (int i = 0; i < attributes.length; i++) {
				attrs.put(new BasicAttribute(attributes[i], values[i]));
				params += attributes[i] + "=" + values[i];
				if (i != attributes.length - 1)
					params += ",";
			}

			ldapProfiler.reportln("elements:" + params);
			ctx.createSubcontext(dn, attrs);
			ldapProfiler.reportln("OK");
		} catch (Exception e) {
			ldapProfiler.reportln(UtilLog.getStackTrace(e));
		}
		return ldapProfiler.getReport();
	}

	public Profiler getLdapProfiler() {
		return ldapProfiler;
	}

	public void setLdapProfiler(Profiler ldapProfiler) {
		this.ldapProfiler = ldapProfiler;
	}
	
	
	 private static void main(String... aArgs){
		    Ldap ldp = new Ldap();
		    ldp.setConnectionConfiguration("08GDYJ-PC", 389, "cn=Manager,dc=maxcrc,dc=com", "jawad");
		    System.out.println(ldp.getLdapData("dc=maxcrc,dc=com", 0, "",null).toString());
		  }
	
}
