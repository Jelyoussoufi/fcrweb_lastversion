package be.celsius.util;

import java.sql.Timestamp;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class UtilDT 
{


	/**
	 * date & time methods
	 */
	
	public static Date stringToDate(String sDate) throws Exception
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ParsePosition pos = new ParsePosition(0);

		return formatter.parse(sDate,pos);	 	
	}
	
	private static String leadChar(String s, String pattern, int size)
	{
		while(s.length()<size)s = pattern + s;
		return s;
	}
	
	/**
	 * splitDate("1/7/2011", "2/8/2011", 10)
	 * 1/7/2011 - 10/7/2011 
	 * 11/7/2011 - 20/7/2011
	 * 21/7/2011 - 30/7/2011
	 * 31/7/2011 - 2/8/2011	
	 */
	public static List<String> splitDate(String startTime, String endTime, int interval)
	{
		String[] t1 = startTime.split("/");
		String[] t2 = endTime.split("/");
			
		GregorianCalendar date1 = new GregorianCalendar(Util.getInteger(t1[2]), Util.getInteger(t1[1])-1, Util.getInteger(t1[0]));
		GregorianCalendar date2 = new GregorianCalendar(Util.getInteger(t2[2]), Util.getInteger(t2[1])-1, Util.getInteger(t2[0]));
	 
		int gap = 
			date2.get(Calendar.DAY_OF_YEAR)
			+(365*(date2.get(Calendar.YEAR)-date1.get(Calendar.YEAR)))
			-date1.get(Calendar.DAY_OF_YEAR);
	
		int div = gap / interval;
		
		List<String> intervals = new ArrayList<String>();
		
		intervals.add(cal2String(date1));
		for(int i=1; i<=div; i++)
		{
			date1.add(Calendar.DATE, interval-1);
			intervals.add(cal2String(date1));
			date1.add(Calendar.DATE, 1);
			intervals.add(cal2String(date1));
		}
		intervals.add(cal2String(date2));
		
		return intervals;
	}
	
	public static String cal2String(Calendar cal)
	{
		return cal.get(Calendar.DATE)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.YEAR);
	}
	
	public static void main(String[] args)
	{
		System.out.println(isLower("2013-01-10 10:00:00", "2013-01-15 10:00:01", 3, 5));
		System.out.println(getTimeOffset(0, 0));
	}
	
	
	/*
	 * seconds in month would be inaccurate because of month with 28,29,30,31 days.
	 */
		
	public static boolean isLower(String datetime1, String datetime2)
	{
		return isLower(datetime1, datetime2, 0, 0);
	}
	
	/*
	 * isLower("2013-01-10 10:00:00", "2013-01-15 10:00:00", 3, 5) => false
	 * isLower("2013-01-10 10:00:00", "2013-01-15 10:00:01", 3, 5) => true
	 * read it as follows it is 5 days later between the first and second date
	 */
	public static boolean isLower(String datetime1, String datetime2, int slot, int value)
	{
		if (!UtilStr.isEmpty(datetime1) & !UtilStr.isEmpty(datetime2))
		{
			long timeOffset = getTimeOffset(slot, value);
			
			long dt1 = datetime2Sec(datetime1);
			long dt2 = datetime2Sec(datetime2);
			
			if (dt1==-1 | dt2==-1 | dt2<dt1)
				return false;
						
			return dt1 + timeOffset < dt2;
		}
		return false;
	}
	
	public static final int TYPE_DELAY_SEC			= 0;
	public static final int TYPE_DELAY_MIN			= 1;
	public static final int TYPE_DELAY_HR			= 2;
	public static final int TYPE_DELAY_DAY			= 3;
	public static final int TIMER_DELAY_TYPE_DEF	= 2;
	public static final int TIMER_DELAY_VALUE_DEF	= 6;
	
	public static final int MIN2SEC 	= 60;
	public static final int HR2SEC	 	= 3600;
	public static final int DAY2SEC 	= 86400;
	
	public static long getTimeOffset(int slot, int value)
	{
		long timeOffset = 0;
		switch (slot)
		{
			case TYPE_DELAY_SEC: 							 		break;	//sec
			case TYPE_DELAY_MIN: 	timeOffset = value * MIN2SEC; 	break;	//min
			case TYPE_DELAY_HR: 	timeOffset = value * HR2SEC;	break;	//hour
			case TYPE_DELAY_DAY: 	timeOffset = value * DAY2SEC;	break;	//day
			default: 				timeOffset = value * DAY2SEC; 	break;	//day
		}
		return timeOffset;
	}
	
	/* !!! caution date format YYYY-MM-DD
	 * 2013-01-10 09:41:57.0
	 */
	public static long datetime2Sec(String datetime)
	{
		try
		{
			if (UtilStr.isEmpty(datetime))
				return -1;
			if (datetime.contains("."))
				datetime = dropNanos(datetime);
			
			String[] datetime_array = datetime.split(" ");
			String splitDateSymbol = "-";
			if (datetime_array[0].contains("/"))
				splitDateSymbol = "/";
			String[] date_array 	= datetime_array[0].split(splitDateSymbol);
			String[] time_array		= datetime_array[1].split(":");
			int year 	= Util.getInteger(date_array[0]);
			int month 	= Util.getInteger(date_array[1]);
			int date	= Util.getInteger(date_array[2]);
			int hour	= Util.getInteger(time_array[0]);
			int min		= Util.getInteger(time_array[1]);
			int sec		= Util.getInteger(time_array[2]);
			
			Calendar cal = Calendar.getInstance();
			cal.set(year, month, date, hour, min, sec);
			return cal.getTimeInMillis() / 1000;
		}
		catch (Exception e){UtilLog.printException(e); return -1;}
	}
	
	
	/* ex: 20:15:10 => 72910 sec */
	public static long time2Sec(String time)
	{
		try
		{
			String[] timedata = time.split(":");
			
			int timeInSec = (Integer.parseInt(timedata[0]) * 3600) +
							(Integer.parseInt(timedata[1]) * 60) +
							Integer.parseInt(timedata[2]);
			
			return timeInSec;
		}
		catch (Exception e){UtilLog.printException(e); return -1;}
	}
	
	/* 72910 sec => 20:15:10 */
	public static String sec2Time(String seconds)
	{
		try
		{
			int timeInSec = Integer.parseInt(seconds);
			
			String time = leadChar((timeInSec / 3600)+"","0",2) + ":";
			timeInSec = timeInSec - ((timeInSec / 3600)*3600);
			time += leadChar((timeInSec / 60)+"","0",2) + ":";
			timeInSec = timeInSec - ((timeInSec / 60)*60);
			time += leadChar(timeInSec+"","0",2) + "";
			
			return time;
		}
		catch (Exception e){UtilLog.printException(e); return "00:00:00";}
	}
	
	
	
	/**
	 * @deprecated
	 */
	public static String now()
	{
		Date date = new Date();                 
		Calendar cal = Calendar.getInstance();   
		cal.setTime(date);                         
		cal.set(Calendar.MILLISECOND, 0);         
		Timestamp ts = new Timestamp(cal.getTimeInMillis());
		return dropNanos(ts.toString());
	}

	/**
	 * @deprecated
	 */
	public static String nowNoS()
	{
		Date date = new Date();                 
		Calendar cal = Calendar.getInstance();   
		cal.setTime(date);                     
		cal.set(Calendar.SECOND, 0);              
		cal.set(Calendar.MILLISECOND, 0);         
		Timestamp ts = new Timestamp(cal.getTimeInMillis());
		return ts.toString();
		//return dropNanos(ts.toString());
	}
	
	public static String now(int dHr, int dMin, int dSec, int dMil, int pHr, int pMin, int pSec, int pMil, boolean noMil)
	{
		Date date = new Date();                 
		Calendar cal = Calendar.getInstance();   
		cal.setTime(date);                         
		
		if (dMil>=0)
			cal.set(Calendar.MILLISECOND, dMil);
		cal.set(Calendar.MILLISECOND, cal.get(Calendar.MILLISECOND)+pMil); 
		
		if (dSec>=0) 
			cal.set(Calendar.SECOND, dSec);
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND)+pSec);
		
		if (dMin>=0) 
			cal.set(Calendar.MINUTE, dMin);
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+pMin);
		
		if (dHr>=0) 
			cal.set(Calendar.HOUR, dHr);
		cal.set(Calendar.HOUR, cal.get(Calendar.HOUR)+pHr);
		
		Timestamp ts = new Timestamp(cal.getTimeInMillis());
		
		if(noMil)
		{
			cal.set(Calendar.MILLISECOND, 0);
			return dropNanos(ts.toString());
		}
		return ts.toString();
	}
	
	
	
	/**
	 * @pre nano seconds is after the single char '.'.
	 * @post drop nano sec part of time
	 * @example: 2011-03-15 09:52:00.0 -> 2011-03-15 09:52:00
	 */
	private static String dropNanos(String time)
	{
		return time.substring(0, time.lastIndexOf("."));
	}
	
	public static String asDate1(String date) //Eg:5-10_11-27-56 
	{
		String res="";
		try
		{
			String[] dh = date.split("_");
			String[] d = dh[0].split("-");
			
			res = d[1]+"/"+ d[0] + " " + dh[1].replace("-", ":");
		}
		catch (Exception e){res = date;}
		
		return res;
	}
	
	public static String toDate(String s) throws Exception
	{
		s = s.replaceAll("T", " "); s = s.replaceAll(".000Z", "");
		
		return s;
	}
	
	public static String gmtToBelgian(Date date)
	{
		String result = "";
		try{
		    SimpleDateFormat formatter = new SimpleDateFormat ("dd.MM.yyyy HH:mm:ss" );
		    Calendar here = Calendar.getInstance();
		    int gmtoffset = here.get(Calendar.DST_OFFSET) + here.get(Calendar.ZONE_OFFSET);       
		    Date GMTDate = new Date(date.getTime() + gmtoffset);
		    result = formatter.format(GMTDate);
		}
		catch (Exception e){UtilLog.printException(e); result = "";}
		
		return result;
	}
	
	public static String convertTenorTime(String time)
	{
		String result = ""; 
		if(!time.contains("T")){return time;}
		try
		{
			result = gmtToBelgian(stringToDate(toDate(time)));
		}
		catch (Exception e){UtilLog.printException(e); result = "";}
		
		return result;
	}
	
}
