package be.celsius.util;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class IdVal implements Serializable
{
	private static final long serialVersionUID = -6366700699511952412L;
	
	private String id;
	private String val;
	
	public IdVal()
	{
		reset();
	}
	
	public IdVal(String id, String val)
	{
		this.id = id;
		this.val = val;
	}
	
	public void reset() 
	{
		id = "";
		val = "";
	}

	public JSONObject getJson()
	{
		try
		{
			return new JSONObject().put(id, val);
		}
		catch (Exception e){UtilLog.printException(e);}
		return null;
	}
	
	public JSONObject toJson()
	{
		try
		{
			return new JSONObject().put("id", id).put("val", val);
		}
		catch (Exception e){UtilLog.printException(e);}
		return null;
	}
	
	public String toXml() {
		String xml = "";

		return xml;
	}

	public String toString() {
		return "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
