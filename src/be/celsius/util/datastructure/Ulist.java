/**
 * 
 */
package be.celsius.util.datastructure;

import java.util.ArrayList;
import java.util.List;

import be.celsius.util.JsonArray;
import be.celsius.util.JsonObject;

/**
 * @author jhuilian
 *
 */
public class Ulist extends ArrayList<String>{

	/**
	 * @param serialVersionUID
	 */
	private static final long serialVersionUID = 2029806906287039181L;

	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String name;
	
	public Ulist()
	{
		setName("list");
	}
	
	public Ulist(String name)
	{
		this();
		setName(name);
	}
	
	public Ulist reset()
	{
		setName("");
		clear();
		return this;
	}
	
	public Ulist push(String elem)
	{
		add(elem);
		return this;
	}
	
	public Ulist addList(List<String> list)
	{
		for (String elem : list)
			add(elem);
		return this;
	}
	
	public Ulist clone()
	{
		return new Ulist().setName(getName()).addList(this);
	}

	public Uset toSet()
	{
		Uset set = new Uset();
		for (int i=0 ; i<size() ; i++)
			set.push(get(i));
		return set.setName(getName());
	}
	
	public JsonObject toJson()
	{
		JsonArray list = new JsonArray();
			for (String elem : this)
				list.put(elem);
			
		return new JsonObject().put(getName(), list) ;
	}
	
	public String getName() {
		return name;
	}

	public Ulist setName(String name) {
		this.name = name;
		return this;
	}
	
}
