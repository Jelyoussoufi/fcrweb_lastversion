package be.celsius.util.datastructure;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import be.celsius.util.JsonObject;
import be.celsius.util.Util;
import be.celsius.util.UtilStr;
import be.celsius.util.struct.MapType;

/**
 * @author jhuilian
 *
 */
public class Umap extends HashMap<String,String>{

	/**
	 * @param serialVersionUID
	 */
	private static final long serialVersionUID = 891972307702568300L;
	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public enum KVP 
	{
		KVP0, 	// recommended value list separator: 
		KVP1,	// recommended value list separator: ,
		KVP2,	// recommended value list separator: 
		KVP3,
		KVP4,	//not recommended when paramter flush over http headers (like bonita API)
		KVP5,
		KVP_HTTP_HEADER,
		KVP_XML_ATTRS,
		KVP_DB_UP,		// k v in db update operation eg: k1='v1' k2='v2' => parameterize(true) for quoting value
		KVP_LDAP
	};
	
	public enum XmlOut {Standard, Attribute};
	private String name;
	private XmlOut xmlType;
	private KVP parameterType;
	private String is;
	private String os;
	
	public Umap()
	{
		reset();
		
	}
	
	public Umap reset()
	{
		clear();
		name		= "parameters";
		xmlType		= XmlOut.Standard;
		setKVP(KVP.KVP0);
		return this;
	}
	
	public String get(String key)
	{
		if (super.get(key)==null)
			return "";
		return super.get(key);
	}
	
	public String remove(String key)
	{
		return containsKey(key)?super.remove(key):"";
	}
	
	public Umap leastKeys(List<String> least)
	{
		for (String key : least)
			push(key, "");
		return this;
	}
	
	public Umap leastKeys(Set<String> least)
	{
		for (String key : least)
			push(key, "");
		return this;
	}
	
	public Umap addAll(Umap map)
	{
		for (String key : map.keySet())
			put(key, map.get(key));
		return this;
	}
	
	public Umap removeAll(List<String> keys)
	{
		for (String key : keys)
			remove(key);
		return this;
	}
	
	public Umap push(String key, String value)
	{
		put(key, value);
		return this;
	}
	
	public Umap drop(String key)
	{
		remove(key);
		return this;
	}
	
	public Umap clone()
	{
		return new Umap().setName(getName()).addAll(this);
	}
	
	public MapType toMapType()
	{
		return new MapType(this);
	}
				
	public Uset keySet()
	{
		return new Uset().addSet(super.keySet());	
	}
	
	public Ulist keyList()
	{
		return new Ulist().addList(Util.set2list(keySet()));
	}

	public JsonObject toJson()
	{
		JsonObject json = new JsonObject();
			for (String key : keySet())
				json.put(key, get(key));
			
		return new JsonObject().put(getName(), json) ;
	}

	public String toXml()
	{
		switch(xmlType)
		{
			case Standard: return toXmlStandard();
			case Attribute: return toXmlAttribute();
			default: return toXmlStandard();
		}
	}
	
	private String toXmlStandard() {
		String xml = ""; Iterator<String> iter = keySet().iterator();
		xml += "<" + name + ">";		
		while(iter.hasNext())
		{
			String key = iter.next();
			xml += "<" + key + ">" + get(key) + "</" + key + ">";
		}
		xml += "</" + name + ">";
		return xml;
	}
	
	private String toXmlAttribute() {
		String xml = ""; Iterator<String> iter = keySet().iterator();
		xml += "<" + name + ">";		
		while(iter.hasNext())
		{
			String key = iter.next();
			xml += "<" + key + " value='" + get(key) + "' />";
		}			
		xml += "</" + name + ">";
		return xml;
	}
	
	/**************
	 	CAUTION
	 	DO NOT USE FOLLOWING SYMBOL:
	 	'*' because of regex token 
	 	'.' because of regex token
	 	AVOID
	 	'&' if data go through ajax because of param separator
	 	'=' if data go through ajax because of param separator
	 	',' if data go may contains list comma separated
	 **************/
	
	
	public String getKVSep()
	{
		switch(parameterType)
		{
			case KVP0: return ":";
			case KVP1: return ":";
			case KVP2: return "-";
			case KVP3: return "---";
			case KVP4: return "&sep;";
			case KVP5: return "###";
			case KVP_HTTP_HEADER: return "=";
			case KVP_XML_ATTRS: return "=";
			case KVP_DB_UP: return "=";
			case KVP_LDAP: return "=";
			default: return ":";
		}
	}	
	
	/** beware of KVP0, KVP1, KVP2 value list eg: key0:val01,val02;key1:val11,val12 => is=: os=; listsep=, **/
	public String getParamSep()
	{
		switch(parameterType)
		{
			case KVP0: return ",";
			case KVP1: return ";";
			case KVP2: return ",";
			case KVP3: return ":::";
			case KVP4: return "&new;";
			case KVP5: return "���";
			case KVP_HTTP_HEADER: return "&";
			case KVP_XML_ATTRS: return " ";
			case KVP_DB_UP: return ",";
			case KVP_LDAP: return ",";
			default: return ";";
		}
	}
	
	public Umap setKVP(KVP kvp)
	{
		parameterType = kvp;
		setIs(getKVSep());
		setOs(getParamSep());
		return this;
	}
	
	public Umap mapsterize(String parameters)
	{
		try{
			String[] key_value_pairs = parameters.split(getOs());
			for (String key_value_pair : key_value_pairs)
			{
				try{
					String[] key_value = key_value_pair.split(getIs());
					put(key_value[0], key_value[1]);
				}catch(Exception e){}
			}
		}catch(Exception e){}
		return this;
	}
	
	public String parameterize(){return parameterize(false);}
	
	public String parameterize(boolean toQuoteValue)
	{
		String parameters = ""; Iterator<String> iter = keySet().iterator();		
		while (iter.hasNext())
		{
			String key = iter.next();
			String val = toQuoteValue ? UtilStr.wrap(get(key), "'") : get(key);
			parameters += key + getIs() + val;
			if (iter.hasNext())parameters += getOs();
		}
		return parameters;
	}

	
	public String toString() {
		return name + "[" + parameterize() + "]";
	}

	public String getName() {
		return name;
	}

	public Umap setName(String name) {
		this.name = name;
		return this;
	}

	public XmlOut getXmlType() {
		return xmlType;
	}

	public Umap setXmlType(XmlOut xmlType) {
		this.xmlType = xmlType;
		return this;
	}

	public KVP getParameterType() {
		return parameterType;
	}

	public Umap setParameterType(KVP parameterType) {
		this.parameterType = parameterType;
		return this;
	}

	public String getIs() {
		return is;
	}

	public Umap setIs(String is) {
		this.is = is;
		return this;
	}

	public String getOs() {
		return os;
	}

	public Umap setOs(String os) {
		this.os = os;
		return this;
	}
		
	public static void main(String[] args) {
		Umap m = new Umap();
		/*
		m.setParameterType(KVP.KVP0);
		m.put("k0", "v0");
		m.put("k1", "v1");
		m.put("k2", "v2");
		m.setXmlType(XmlOut.Attribute);
		System.out.println(m.toJson().toString());
		*/
		m.setKVP(KVP.KVP_HTTP_HEADER);
		m.mapsterize("p1=toto&p2=azimov&p3=lil et wayne");
		System.out.println(m.toJson().toString());
		
	}
}
