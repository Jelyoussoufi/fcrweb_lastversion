package be.celsius.util.datastructure;

/**
 * 
 */

/**
 * @author jhuilian
 *
 */

public class TestReflectObject extends ReflectiveObject{

	private int age;
	private String name;
	private String blurp;
	
	public TestReflectObject()
	{
		super();
		setObject_name("test");
		//setObject_fields(new SunSet().insert("age").insert("name"));
	}
	
	public TestReflectObject(int a, String n)
	{
		this();
		age = a;
		name = n;
	}
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static void main(String[] args) {
		TestReflectObject t = new TestReflectObject(25, "jian");
		System.out.println(t.toString());
		System.out.println(t.toXml());
		System.out.println(t.toJson().toString());
	}
	
	
}
