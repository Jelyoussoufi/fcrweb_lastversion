package be.celsius.util.datastructure;

import java.lang.reflect.Field;
import java.util.Iterator;

import org.json.JSONObject;

/**
 * @author jhuilian
 *
 */
public class ReflectiveObject {

	private String object_name;
	private Uset object_fields;
	
	public ReflectiveObject()
	{
		object_name 	= getClass().getName();
		object_fields 	= new Uset();
	}
	
	public void initialize(Umap attributes)
	{
		try
		{
			Iterator<String> iter = attributes.keySet().iterator();		
			
			while (iter.hasNext())
			{	
				try
				{
					Field field = this.getClass().getDeclaredField(iter.next());
					field.setAccessible(true);
					setFieldValue(field, attributes.get(field.getName()));
					field.setAccessible(false);
				}
				// IllegalAccess, NoSuchField
				catch(Exception e){}
			}			
		}
		catch(Exception e){}
	}

	public JSONObject toJson() 
	{
		return toMap().toJson();
	}

	public String toXml() {
		return toMap().toXml();
	}

	public String toString() {
		return toMap().toString();
	}
	
	private Umap toMap()
	{
		Umap map = new Umap();	
		map.setName(object_name);

		// if no fields have been declared then assume all fields of the class to be accessible fields
		if (object_fields==null || object_fields.size()==0)
		{
			for (Field field : this.getClass().getDeclaredFields())
				addFieldValue2Map(field, map);
		}
		else
		{
			Iterator<String> iter = object_fields.iterator();		
			
			while (iter.hasNext())
			{	
				try
				{
					Field field = this.getClass().getDeclaredField(iter.next());
					addFieldValue2Map(field, map);
				}
				catch(Exception e){}
			}
		}		
		return map;
	}
	
	private void addFieldValue2Map(Field field, Umap map)
	{
		try
		{
			field.setAccessible(true);
			Object obj 		= field.get(this);
			String value 	= null;
			if (obj!=null)
				value = obj.toString();
			map.put(field.getName(), value);
			field.setAccessible(false);
		}
		catch(Exception e)
		{
			map.put(field.getName(), null);
		}
	}
	
	private void setFieldValue(Field field, String value)
	{
		try
		{
			field.setAccessible(true);		
			if (field.getType().toString().equals(value.getClass().toString()))
				field.set(this, value);
			field.setAccessible(false);
		}
		catch(Exception e){}
	}

	public void setObject_name(String object_name) {
		this.object_name = object_name;
	}

	public void setObject_fields(Uset object_fields) {
		this.object_fields = object_fields;
	}
}
