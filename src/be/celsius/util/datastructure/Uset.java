package be.celsius.util.datastructure;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import be.celsius.util.JsonArray;
import be.celsius.util.JsonObject;

/**
 * 
 */

/**
 * @author jhuilian
 *
 */
public class Uset extends HashSet<String>
{	
	/**
	 * @param serialVersionUID
	 */
	private static final long serialVersionUID = 3556453219902112090L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String name;
	
	public Uset()
	{
		setName("set");
	}
	
	public Uset(String name)
	{
		this();
		setName(name);
	}
	
	public Uset reset()
	{
		setName("");
		clear();
		return this;
	}
	
	public Uset clone()
	{
		return new Uset().setName(getName()).addSet(this);
	}
	
	/**
	 * allows chaining insert(o1).insert(o2) but not warned if some object is not inserted in case of duplication.
	 * @param o 
	 * @return
	 */
	public Uset push(String o)
	{
		add(o);
		return this;
	}
	
	public Uset drop(String o)
	{
		remove(o);
		return this;
	}
	
	public Uset addSet(Set<String> set)
	{
		for (String elem : set)
			add(elem);
		return this;
	}	
	
	public Uset removeSet(Set<String> set)
	{
		for (String elem : set)
			remove(elem);
		return this;
	}	
	
	public Ulist toList()
	{
		Ulist list = new Ulist();
		for (String elem : this)
			list.add(elem);
		return list;
	}
	
	public JsonObject toJson()
	{
		JsonArray list = new JsonArray();
			for (String elem : this)
				list.put(elem);
			
		return new JsonObject().put(getName(), list) ;
	}
	
	public String getName() {
		return name;
	}

	public Uset setName(String name) {
		this.name = name;
		return this;
	}
}

