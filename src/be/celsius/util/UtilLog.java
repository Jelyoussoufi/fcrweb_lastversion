package be.celsius.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class UtilLog 
{
	/**
	 * @category logging
	 * @param aThrowable an exception
	 * @return the exception stacktrace wrapped in a string 
	 */
	public static String getStackTrace(Exception aThrowable) 
	{
		Writer result 			= new StringWriter();
	    PrintWriter printWriter = new PrintWriter(result);

	    aThrowable.printStackTrace(printWriter);
	    
	    return result.toString(); 
	}
	
	/**
	 * @category logging
	 * @param aThrowable an exception
	 * the exception stackTrace is printed in standard output if debug mode is active 
	 */
	public static void printException(Exception aThrowable)
	{	
		if (Config.debug) aThrowable.printStackTrace();
	}
	
	/** 
	 * @category logging
	 * @param name a string used for identify the object obj 
	 * @param obj the object to be tested
	 * print on the standard output if the object obj is null.
	 * @return true if obj is null else return false.
	 */
	public static boolean isNull(String name, Object obj)
	{
		UtilLog.debug(name+" is null? : "+(obj==null));
		return obj==null;
	}

	/**
	 * @category logging
	 * generic use
	 */
	public static void out(String s){print(s,Config.info);}
	public static void out1(String s){print1(s,"*",Config.info);}

	/**
	 * @category logging
	 * use it only for debugging purpose.
	 */
	public static void debug(String s){print(s,Config.debug);}
	public static void debug1(String s){print1(s,"!",Config.debug);}
	
	/**
	 * @category logging
	 * use it only when accessing database
	 */
	public static void dbo(String s){print(s,Config.dbverb);}
	public static void dbo1(String s){print1(s,"-",Config.dbverb);}
	
	
	/**
	 * @category logging
	 * @param s the information to be printed on the standard output
	 * @param verb type of verbose mode, if true then the information will be printed
	 */
	private static void print(String s, boolean verb)
	{
		if(verb & Config.atCelsius) System.out.println(s+": "+UtilDT.now());
	}
	
	/**
	 * pretty print version of print
	 */
	private static void print1(String s, String symbol, boolean verb)
	{
		if(verb & Config.atCelsius)
		{ 
			line(50,symbol);
			System.out.println(s+": "+UtilDT.now());
			line(50,symbol);
		}
	}
	
	public static void sep()
	{
		line(50, "+");
	}
	
	public static void line(int repeat, String symbol)
	{
		if(Config.atCelsius)
			System.out.println(getLineOf(repeat, symbol));
	}

	public static String getLineOf(int repeat, String symbol)
	{
		String s="";for(int i=0; i<repeat; i++){s+=symbol;}return s;
	}
}
