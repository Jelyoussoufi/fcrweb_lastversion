/**
 * 
 */
package be.celsius.util.session;

import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.ReflectiveObject;
import be.celsius.util.datastructure.Uset;

/**
 * @author jhuilian
 *
 */
public class User extends ReflectiveObject{

	private String id;
	private String login;
	private String firstname;
	private String lastname;
	private String address;
	private String email;
	private String phone;
	private String mobile;
	private String profile;
	private String ldapgroup;
	
	public User(){}
	{
		reset();
		setObject_name("user");
		setObject_fields(new Uset()
							.push("id")
							.push("login")
							.push("firstname")
							.push("lastname")
							.push("address")
							.push("email")
							.push("phone")
							.push("mobile")
							.push("profile")
							.push("ldapgroup"));
	}
	
	public static void main(String[] args) {
		Umap map = new Umap();
		map.push("login","dcarels").push("email","dcarels@celsius-tech.com");
		User u = new User();
		u.initialize(map);
		System.out.println(u.toString());
	}
	
	public void reset() 
	{
		setId("");
		setLogin("");
		setFirstname("");
		setLastname("");
		setAddress("");
		setEmail("");
		setPhone("");
		setMobile("");
		setProfile("");
		setLdapgroup("");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getLdapgroup() {
		return ldapgroup;
	}

	public void setLdapgroup(String ldapgroup) {
		this.ldapgroup = ldapgroup;
	}
}
