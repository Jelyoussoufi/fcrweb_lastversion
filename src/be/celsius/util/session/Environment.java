package be.celsius.util.session;

import be.celsius.util.Config;
import be.celsius.util.datastructure.ReflectiveObject;
import be.celsius.util.datastructure.Uset;

public class Environment extends ReflectiveObject {

	private int environment;
	
	public Environment()
	{
		setObject_name("user");
		setObject_fields(new Uset().push("environment"));
		
		if (Config.atCelsius)
			setEnvironment(Config.env_atCelsius);
		else
			setEnvironment(Config.env_onAcceptance);
	}

	public int getEnvironment() {
		return environment;
	}

	public void setEnvironment(int environment) {
		this.environment = environment;
	}
}
