package be.celsius.util;

public class UtilErr {

	public static final int ERR_C_SAFE_CONFLICT			= -11;
	public static final String ERR_DESC_SAFE_CONFLICT	= "";
	
	public static final int ERR_C_DEFAULT				= -99;
	public static final String ERR_DESC_DEFAULT			= "Unknown Error";
	public static enum AppError {ERR_SAFE_CONFLICT};
	
	public static int getErrCode(AppError err)
	{
		switch(err)
		{
			case ERR_SAFE_CONFLICT : return ERR_C_SAFE_CONFLICT;
			default : return ERR_C_DEFAULT;
		}
	}
	
	public static String getErrDesc(AppError err)
	{
		switch(err)
		{
			case ERR_SAFE_CONFLICT : return ERR_DESC_SAFE_CONFLICT;
			default : return ERR_DESC_DEFAULT;
		}
	} 
	
	public static String getStackTrace(Exception aThrowable)
	{
		return UtilLog.getStackTrace(aThrowable);
	}
	
	public static void printException(Exception aThrowable)
	{
		UtilLog.printException(aThrowable);
	}
}
