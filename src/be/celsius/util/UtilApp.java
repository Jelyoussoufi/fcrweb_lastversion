package be.celsius.util;

import java.util.List;
import java.util.Map;

import be.celsius.util.bean.NullLeaf;
import be.celsius.util.datastructure.Umap;

public class UtilApp 
{
	public static NullLeaf NULLSEED 		= new NullLeaf();
	
	public static final int SC_OK			= 1;
	public static final int SC_NONE			= 0;
	public static final int SC_KO			= -1;
	public static final int SC_MISS_VAL		= -2;
	public static final int SC_NO_RIGHT		= -8;
	public static final int SC_FAULT		= -9;
	
	public static final String SRC 			= "source";
	public static final String DST 			= "destination";
	
	
	public static final String CA_KV_SEP 	= "---";
	public static final String CA_E_SEP 	= ":::";
	public static final String GOO_SEP 		= ";;;";
	public static final String CGS_SEP 		= GOO_SEP;

	public static final String HD_AI 	= "administrative-information";
	public static final String HD_TI 	= "technical-information";
	public static final String HD_SEF 	= "simple";
	public static final String HD_AEF 	= "advanced";
	public static final String HD_MTN 	= "maintain";	
	public static final String HD_AEF_E	= "list-flow";
	public static final String HD_FI 	= "flow-info";
	public static final String HD_HD 	= "hidden";
	public static final String HD_RET 	= "request-edition-task";
	
	public static final String OBJ_SVC_UND = "0";
	public static final String OBJ_SVC_ADD = "1";
	public static final String OBJ_SVC_RMV = "2"; 	
	
	public static final String default_celsius_user 		= "jvoisin";
	public static final String user_type_simple				= "simple";
	public static final String user_type_advanced			= "advanced";
	//public static final String default_celsius_user_type 	= user_type_simple;
	
	public static final String ADM	= "cn=SecTools - FCR Web - Administrators,ou=applications groups,ou=groups,ou=staff,o=mobistar.be";
	public static final String ADV	= "cn=SecTools - FCR Web - Advanced users,ou=applications groups,ou=groups,ou=staff,o=mobistar.be";
	public static final String FWE	= "cn=SecTools - FCR Web - FW Experts,ou=applications groups,ou=groups,ou=staff,o=mobistar.be";
	public static final String SO	= "cn=SecTools - FCR Web - Security Officers,ou=applications groups,ou=groups,ou=staff,o=mobistar.be";
	public static final String FWO	= "cn=SecTools - FCR Web - FW Operations,ou=applications groups,ou=groups,ou=staff,o=mobistar.be";
	
	private static Umap adminInfo;
	private static Umap techInfo;
	private static Umap docInfo;	
	private static Umap sefInfo;
	private static Umap aefInfo;
	private static Umap flowInfo;
	private static Umap hiddenInfo;
	
	private static Umap ai_mapper;
	
	private static Umap content_type_map;
	
	public static String getMimeType0(String filename)
	{
		if (UtilStr.endsWith(filename, ".pdf"))return "application/pdf";
		else if (UtilStr.endsWith(filename, ".docx"))return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";	
		else if (UtilStr.endsWith(filename, ".doc"))return "application/msword";			
		else if (UtilStr.endsWith(filename, ".xlsx"))return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		else if (UtilStr.endsWith(filename, ".xls"))return "application/vnd.ms-excel";
		else if (UtilStr.endsWith(filename, ".vsd"))return "application/vnd.Visio";
		else return "application/octet-stream";
	}
	
	
	public static String getContentType(String filename)
	{
		if (content_type_map==null)
		{
			content_type_map = new Umap();
			content_type_map
			.push("doc", "application/msword")
			.push("dot", "application/msword")
			.push("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
			.push("dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template")
			.push("docm", "application/vnd.ms-word.document.macroEnabled.12")
			.push("dotm", "application/vnd.ms-word.template.macroEnabled.12")
			.push("xls", "application/vnd.ms-excel")
			.push("xlt", "application/vnd.ms-excel")
			.push("xla", "application/vnd.ms-excel")
			.push("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
			.push("xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template")
			.push("xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12")
			.push("xltm", "application/vnd.ms-excel.template.macroEnabled.12")
			.push("xlam", "application/vnd.ms-excel.addin.macroEnabled.12")
			.push("xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12")
			.push("ppt", "application/vnd.ms-powerpoint")
			.push("pot", "application/vnd.ms-powerpoint")
			.push("pps", "application/vnd.ms-powerpoint")
			.push("ppa", "application/vnd.ms-powerpoint")
			.push("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation")
			.push("potx", "application/vnd.openxmlformats-officedocument.presentationml.template")
			.push("ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow")
			.push("ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12")
			.push("pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12")
			.push("potm", "application/vnd.ms-powerpoint.template.macroEnabled.12")
			.push("ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12")
			.push("dwg", "application/acad")
			.push("ccad", "application/clariscad")
			.push("drw", "application/drafting")
			.push("dxf", "application/dxf")
			.push("unv", "application/i-deas")
			.push("igs", "application/iges")
			.push("iges", "application/iges")
			.push("bin", "application/octet-stream")
			.push("oda", "application/oda")			
			.push("ai", "application/postscript")
			.push("eps", "application/postscript")
			.push("ps", "application/postscript")
			.push("prt", "application/pro_eng")
			.push("rtf", "application/rtf")
			.push("set", "application/set")
			.push("stl", "application/sla")
			.push("dwg", "application/solids")
			.push("step", "application/step")
			.push("vda", "application/vda")
			.push("mif", "application/x-mif")
			.push("dwg", "application/x-csh")
			.push("dvi", "application/x-dvi")
			.push("hdf", "application/hdf")
			.push("latex", "application/x-latex")
			.push("nc", "application/x-netcdf")
			.push("cdf", "application/x-netcdf")
			.push("dwg", "application/x-sh")
			.push("tcl", "application/x-tcl")
			.push("tex", "application/x-tex")
			.push("texinfo", "application/x-texinfo")
			.push("texi", "application/x-texinfo")
			.push("t", "application/x-troff")
			.push("tr", "application/x-troff")
			.push("troff", "application/x-troff")
			.push("man", "application/x-troff-man")
			.push("me", "application/x-troff-me")
			.push("ms", "application/x-troff-ms")
			.push("src", "application/x-wais-source")
			.push("bcpio", "application/x-bcpio")
			.push("cpio", "application/x-cpio")
			.push("gtar", "application/x-gtar")
			.push("shar", "application/x-shar")
			.push("sv4cpio", "application/x-sv4cpio")
			.push("sc4crc", "application/x-sv4crc")
			.push("tar", "application/x-tar")
			.push("man", "application/x-ustar")
			.push("man", "application/zip")
			.push("au", "audio/basic")
			.push("snd", "audio/basic")
			.push("aif", "audio/x-aiff")
			.push("aiff", "audio/x-aiff")
			.push("aifc", "audio/x-aiff")
			.push("wav", "audio/x-wav")
			.push("man", "image/gif")
			.push("ief", "image/ief")
			.push("jpg", "image/jpeg")
			.push("jpeg", "image/jpeg")
			.push("jpe", "image/jpeg")
			.push("tiff", "image/tiff")
			.push("tif", "image/tiff")
			.push("cmu", "image/x-cmu-raster")
			.push("pnm", "image/x-portable-anymap")
			.push("pbm", "image/x-portable-bitmap")
			.push("pgm", "image/x-portable-graymap")
			.push("ppm", "image/x-portable-pixmap")
			.push("rgb", "image/x-rgb")
			.push("xbm", "image/x-xbitmap")
			.push("xpm", "image/x-xpixmap")
			.push("man", "image/x-xwindowdump")
			.push("zip", "multipart/x-zip")
			.push("gz", "multipart/x-gzip")
			.push("gzip", "multipart/x-gzip")
			.push("htm", "text/html")
			.push("html", "text/html")
			.push("txt", "text/plain")
			.push("g", "text/plain")
			.push("h", "text/plain")
			.push("c", "text/plain")
			.push("cc", "text/plain")
			.push("hh", "text/plain")
			.push("m", "text/plain")
			.push("f90", "text/plain")
			.push("rtx", "text/richtext")
			.push("tsv", "text/tab-separated-value")
			.push("etx", "text/x-setext")
			.push("mpeg", "video/mpeg")
			.push("mpg", "video/mpeg")
			.push("mpe", "video/mpeg")
			.push("qt", "video/quicktime")
			.push("mov", "video/quicktime")
			.push("avi", "video/msvideo")
			.push("movie", "video/x-sgi-movie")			
			;
		}
		String def_type = "text/plain";
		String mediatype = def_type;
		
		if (!UtilStr.isEmpty(filename) && filename.contains("."))
		{
			
			try
			{
				String[] elements = filename.split("\\.");
				String ext = elements[elements.length-1];
				if (!UtilStr.isEmpty(ext) && !UtilStr.isEmpty(content_type_map.get(ext)))		
					mediatype = content_type_map.get(ext)	;
				else
					mediatype = def_type;
			}
			catch (Exception e){mediatype = def_type;}
		}
		else
			mediatype = def_type;			
			
		return mediatype;
	}
	
	public static Umap getAdminInfoList()
	{
		if (adminInfo==null){
			adminInfo = new Umap().setName(HD_AI)
			.push("requestor", "")
			.push("on-behalf-of", "")
			.push("application-service", "")
			.push("request-context", "")
			.push("context-detail", "")
			.push("detailed-reason-request", "")
			;
		}
		return adminInfo.clone();
	}
	
	public static Umap getTechInfoList()
	{
		if (techInfo==null){
			techInfo = new Umap().setName(HD_TI)
			.push("form-selector", "");
		}
		return techInfo.clone();
	}
	
	public static Umap getDocInfoList()
	{
		if (docInfo==null){
			docInfo = new Umap()
			.push("filename", "")
			.push("mediatype", "")
			.push("size", "")
			//.push("flow_documentation", "")
			;
		}
		return docInfo.clone();
	}
	
	public static Umap getSefInfoList()
	{
		if (sefInfo==null){
			sefInfo = new Umap().setName(HD_SEF)
			.push("flow-details", "");
		}
		return sefInfo.clone();
	}
	
	public static Umap getAefInfoList()
	{
		if (aefInfo==null){
			aefInfo = new Umap().setName(HD_AEF_E)
			.push("list-flow-source", "")
			.push("list-flow-source-selection", "")                            
			.push("list-flow-dest-organization", "")
			.push("list-flow-src-organization", "")
			.push("list-flow-src-host-auto", "")
			.push("list-flow-dest-host-auto", "")
			.push("list-flow-srv-selection", "")
			.push("list-flow-source-action-selection", "")
			.push("list-flow-destination-action-selection", "")
			.push("list-flow-srv-action-selection", "")					
			.push("list-flow-src-host-selection", "")
			.push("list-flow-src-host-manual", "")                    
			.push("list-flow-src-host-ip-selection", "")
			.push("list-flow-src-host-ip-auto", "")
			.push("list-flow-src-host-ip-manual", "")
			.push("list-flow-src-host-tsl-ip", "")
			.push("list-flow-src-subnet-selection", "")
			.push("list-flow-src-subnet-auto", "")
			.push("list-flow-src-subnet-manual", "")
			.push("list-flow-src-subnet-mask", "")
			.push("list-flow-user2host-profile-selection", "")
			.push("list-flow-user2host-profile-auto", "")
			.push("list-flow-user2host-profile-manual", "")
			.push("list-flow-user2host-profile-owner", "")
			.push("list-flow-user2host-profile-description", "")
			.push("list-flow-src-goo-selection", "")                    
			.push("list-flow-src-goo-auto", "")                    
			.push("list-flow-src-goo-manual", "")
			.push("list-flow-src-goo-object-group-selection", "")
			.push("list-flow-src-goo-host-selection", "")
			.push("list-flow-src-goo-subnet-selection", "")
			.push("list-flow-src-goo-zone-selection", "")
			.push("list-flow-destination", "")
			.push("list-flow-destination-selection", "")
			.push("list-flow-dest-host-selection", "")
			.push("list-flow-dest-host-manual", "")                    
			.push("list-flow-dest-host-ip-selection", "")
			.push("list-flow-dest-host-ip-auto", "")
			.push("list-flow-dest-host-ip-manual", "")
			.push("list-flow-dest-host-tsl-ip", "")
			.push("list-flow-dest-subnet-selection", "")
			.push("list-flow-dest-subnet-auto", "")
			.push("list-flow-dest-subnet-manual", "")
			.push("list-flow-dest-subnet-mask", "")
			.push("list-flow-dest-goo-selection", "")                    
			.push("list-flow-dest-goo-auto", "")                    
			.push("list-flow-dest-goo-manual", "")
			.push("list-flow-dest-goo-object-group-selection", "")
			.push("list-flow-dest-goo-host-selection", "")
			.push("list-flow-dest-goo-subnet-selection", "")
			.push("list-flow-dest-goo-zone-selection", "")					
			.push("list-flow-srv-service-auto", "")
			.push("list-flow-srv-cgs-auto", "")
			.push("list-flow-srv-service-manual", "")
			.push("list-flow-srv-protocol", "")
			.push("list-flow-srv-type", "")
			.push("list-flow-srv-port", "")
			.push("list-flow-srv-cgs-manual", "")                    
			.push("list-flow-srv-cgs-service-selection", "")
			.push("list-flow-srv-cgs-service-group-selection", "")
			.push("list-flow-end-date", "")
			.push("list-flow-position", "")
			.push("button-edit-flow", "")
			.push("button-view-flow", "")
			.push("list-flow-service", "")
			.push("list-flow-action", "")
			.push("list-flow-description", "")
			.push("list-flow-dataflow-type", "");
		}
		return aefInfo.clone();
	}
	
	public static Umap getHiddenInfoList()
	{
		if (hiddenInfo==null){
			hiddenInfo = new Umap().setName(HD_HD)
			.push("form-id", "")		
			.push("readonly", "")
			;
		}
		return hiddenInfo.clone();
	}
	
	public static Umap getFlowInfoList()
	{
		if (flowInfo==null)
		{			
			flowInfo = new Umap().setName(HD_FI)
			.push("flow-source-selection", "")
			.push("flow-src-host-selection", "")
			.push("flow-src-host-ip-manual", "")
			.push("flow-src-host-tsl-ip", "")
			.push("flow-src-subnet-selection", "")
			.push("flow-user2host-profile-selection", "")
			.push("flow-user2host-profile-auto", "")
			.push("flow-user2host-profile-manual", "")
			.push("flow-user2host-profile-owner", "")
			.push("flow-src-goo-selection", "")
			.push("flow-destination-selection", "")
			.push("flow-dest-host-selection", "")
			.push("flow-dest-host-ip-manual", "")
			.push("flow-dest-host-tsl-ip", "")
			.push("flow-dest-subnet-selection", "")
			.push("flow-dest-goo-selection", "")
			.push("flow-srv-selection", "")
			.push("flow-srv-output", "")
			.push("flow-src-goo-object-group-selection-values", "")
			.push("flow-dest-goo-object-group-selection-values", "")
			.push("flow-src-goo-host-selection-values", "")
			.push("flow-dest-goo-host-selection-values", "")
			.push("flow-src-goo-subnet-selection-values", "")
			.push("flow-dest-goo-subnet-selection-values", "")
			.push("flow-srv-cgs-service-selection-values", "")
			.push("flow-srv-cgs-service-group-selection-values", "")
			.push("button-add-src-goo-object-group", "")
			.push("button-remove-src-goo-object-group", "")
			.push("button-add-src-goo-host", "")
			.push("button-remove-src-goo-host", "")
			.push("button-add-src-goo-subnet", "")
			.push("button-remove-src-goo-subnet", "")
			.push("button-add-dest-goo-object-group", "")
			.push("button-remove-dest-goo-object-group", "")
			.push("button-add-dest-goo-host", "")
			.push("button-remove-dest-goo-host", "")
			.push("button-add-dest-goo-subnet", "")
			.push("button-remove-dest-goo-subnet", "")
			.push("button-add-srv-cgs-service", "")
			.push("button-remove-srv-cgs-service", "")
			.push("button-add-srv-cgs-service-group", "")
			.push("button-remove-srv-cgs-service-group", "")
			.push("flow-src-goo-object-group-selector", "")
			.push("flow-dest-goo-object-group-selector", "")
			.push("flow-src-goo-host-selector", "")
			.push("flow-dest-goo-host-selector", "")
			.push("flow-src-goo-subnet-selector", "")
			.push("flow-dest-goo-subnet-selector", "")
			.push("flow-srv-cgs-service-selector", "")
			.push("flow-srv-cgs-service-group-selector", "")
			.push("flow-service-hidden", "")
			.push("flow-src-organization", "")
			.push("flow-dest-organization", "")                        
			.push("flow-source-hidden", "")
			.push("flow-destination-hidden", "")
			.push("flow-srv-service-selection", "")
			.push("flow-srv-cgs-selection", "")
			.push("flow-src-host-auto", "")
			.push("flow-dest-host-auto", "")
			.push("flow-src-host-manual", "")
			.push("flow-dest-host-manual", "")
			.push("flow-src-host-ip-auto", "")
			.push("flow-dest-host-ip-auto", "")
			.push("flow-src-host-ip-selection", "")
			.push("flow-dest-host-ip-selection", "")
			.push("flow-src-goo-auto", "")
			.push("flow-dest-goo-auto", "")
			.push("flow-src-goo-manual", "")
			.push("flow-dest-goo-manual", "")
			.push("flow-src-goo-object-group-selection", "")
			.push("flow-dest-goo-object-group-selection", "")
			.push("flow-src-goo-host-selection", "")
			.push("flow-dest-goo-host-selection", "")
			.push("flow-src-goo-subnet-selection", "")
			.push("flow-dest-goo-subnet-selection", "")
			.push("flow-src-goo-zone-selection", "")
			.push("flow-dest-goo-zone-selection", "")
			.push("flow-srv-cgs-manual", "")
			.push("flow-src-subnet-auto", "")
			.push("flow-dest-subnet-auto", "")
			.push("flow-src-subnet-manual", "")
			.push("flow-dest-subnet-manual", "")
			.push("flow-src-subnet-mask", "")
			.push("flow-dest-subnet-mask", "")
			.push("flow-user2host-profile-description", "")
			.push("flow-srv-cgs-service-selection", "")
			.push("flow-srv-cgs-service-group-selection", "")
			.push("flow-srv-type", "")
			.push("flow-srv-port", "")
			.push("flow-srv-protocol", "")
			.push("flow-srv-service-manual", "")
			.push("flow-srv-cgs-auto", "")
			.push("flow-srv-service-auto", "")
			.push("flow-action", "")
			.push("flow-source-output", "")
			.push("flow-destination-output", "")
			.push("flow-source-action-selection", "")
			.push("flow-destination-action-selection", "")
			.push("flow-srv-action-selection", "")
			.push("flow-end-date", "")
			.push("flow-description", "")
			.push("flow-dataflow-type", "");
		}		
		return flowInfo;
	}
	
	/**
	 * @deprecated
	 */
	public static Umap getAdminInfoMapper()
	{
		if(ai_mapper==null)
		{	ai_mapper = new Umap()
			.push("requestor_uid","requestor")
			.push("validator_uid","on-behalf-of")
			.push("scope","application-service")
			.push("reason","detailed-reason-request")
			.push("type","request-context")
			.push("type_description","context-detail");
		}
		return ai_mapper;
	}
	
	public static Map<String,String> genMapper(List<String> list)
	{
		Umap mapper = new Umap();
		for (String elem : list)
			mapper.put(elem.replace("-", "_"), elem);
		return mapper;
	}
	
	public static boolean isSrc(String type){return UtilStr.isX(type, "source") | UtilStr.isX(type, "src");}
	public static boolean isDest(String type){return UtilStr.isX(type, "destination") | UtilStr.isX(type, "dest") | UtilStr.isX(type, "dst");}
	public static boolean isSrv(String type){return UtilStr.isX(type, "service") | UtilStr.isX(type, "srv") | UtilStr.isX(type, "svc");}
	
	public static boolean isSef(String type){return UtilStr.isX(type, "simple") | UtilStr.isX(type, "sef");}
	public static boolean isAef(String type){return UtilStr.isX(type, "advanced") | UtilStr.isX(type, "aef");}
	
	public static boolean isMtn(String type){return UtilStr.hasX(type, "maintain") | UtilStr.hasX(type, "mtn");}
	public static boolean isGOO(String type){return UtilStr.hasX(type, "goo") | (UtilStr.hasX(type, "group") & UtilStr.hasX(type, "object"));}
	public static boolean isGSRV(String type){return UtilStr.hasX(type, "gsrv") | (UtilStr.hasX(type, "group") & UtilStr.hasX(type, "service"));}
	public static boolean isMtnGOO(String type){return isMtn(type) & isGOO(type);}
	public static boolean isMtnGSRV(String type){return isMtn(type) & isGSRV(type);}
	
	public static String action2code(String action)
	{
		if (UtilStr.hasX(action, "add"))
			return OBJ_SVC_ADD; 
		else if (UtilStr.hasX(action, "remove"))
			return OBJ_SVC_RMV;
		else
			return OBJ_SVC_UND;
	}
	
	private static String code2action(String num, String type)
	{
		if (UtilStr.isX(num, "1"))
			return "add " + type;
		else if (UtilStr.isX(num, "2"))
			return "remove" + type;
		else
			return "";
	}
	
	public static String getShortOrg(String org)
	{
		if (UtilStr.isX(org, "mobistar"))
			return "mob";
		else if (UtilStr.isX(org, "olu"))
			return "olu";
		else if (UtilStr.isX(org, "mes"))
			return "mes";
		else //if (UtilStr.isX(org, "external"))
			return "ext";
	}
	
	
	public static boolean isEncryption(String flow_action){return UtilStr.hasX(flow_action, "encryption");}
	public static boolean isUserAuthentication(String flow_action){return UtilStr.hasX(flow_action, "authentication");}
	public static boolean isNao(String flow_action)
	{
		return isEncryption(flow_action) | isUserAuthentication(flow_action);
	}
	
	public static boolean isFWE(String groups){return UtilStr.hasX(groups, "fwe");}
	public static boolean isTSO(String groups){return UtilStr.hasX(groups, "tso");}
	public static boolean isSO(String groups){return UtilStr.hasX(groups, "so");}
	public static boolean isFWO(String groups){return UtilStr.hasX(groups, "fwo");}
	
	public static boolean isChangeRule(String fcr_mode){return UtilStr.hasX(fcr_mode, "rule");}
	/** TSRR stands for technical service rules revalidation **/
	public static boolean isTSRR(String fcr_mode){return UtilStr.hasX(fcr_mode, "ts-rules-revalidation");}
	
}
