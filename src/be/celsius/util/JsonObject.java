package be.celsius.util;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonObject extends JSONObject
{
	public JsonObject()
	{
		super();
	}

	public JsonObject put(String key, String obj)
	{
		try{super.put(key, obj);}catch(Exception e){}return this;
	}
	
	public JsonObject put(String key, JSONObject obj)
	{
		try{super.put(key, obj);}catch(Exception e){}return this;
	}

	public JsonObject put(String key, JSONArray obj)
	{
		try{super.put(key, obj);}catch(Exception e){}return this;
	}
	
	public JsonObject getJsonObject(String key)
	{
		try{return (JsonObject)super.get(key);}catch(Exception e){}return new JsonObject();
	}
	
	public JsonArray getJsonArray(String key)
	{
		try{return (JsonArray)super.get(key);}catch(Exception e){}return new JsonArray();
	}
}
