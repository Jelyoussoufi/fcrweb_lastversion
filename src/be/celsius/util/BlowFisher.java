package be.celsius.util;

import java.math.BigInteger;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class BlowFisher {
	
	private SecretKeySpec secretKey;

	public BlowFisher(String key)
	{
		this.secretKey = new SecretKeySpec(key.getBytes(), "Blowfish"); 
	}
	
	public String encryptBlowfish(String to_encrypt) 
    {
		try 
		{
    	    Cipher cipher = Cipher.getInstance("Blowfish");
    	    cipher.init(Cipher.ENCRYPT_MODE, secretKey);
    	    return new BigInteger(cipher.doFinal(to_encrypt.getBytes())).toString();
    	} 
		catch (Exception e) 
    	{
    		UtilLog.printException(e);
    		return ""; 
    	}
    }
    
    public String decryptBlowfish(String to_decrypt) 
    {
    	try 
    	{
    		BigInteger bigint = new BigInteger(to_decrypt);
    		Cipher cipher = Cipher.getInstance("Blowfish");
    		cipher.init(Cipher.DECRYPT_MODE, secretKey);
    		byte[] decrypted = cipher.doFinal(bigint.toByteArray());
    		return new String(decrypted);
    	} 
    	catch (Exception e)
    	{
    		UtilLog.printException(e);
    		return ""; 
    	}
    }
    
	public static void main(String[] args) 
	{
		String pass = "ozymandia";
		pass = "";
		String to_decrypt = "1587173753632694643";
		
    	String key	= "Cerberus2013";
    	BlowFisher fish = new BlowFisher(key);
    	String encoded = fish.encryptBlowfish(pass);
    	String decoded = fish.decryptBlowfish(to_decrypt);
    	System.out.println("password:" + pass);
    	System.out.println("encoded password:" + encoded);
    	System.out.println("decoded password:" + decoded);
    	System.out.println(pass.equals(decoded));
    	
	}
}
