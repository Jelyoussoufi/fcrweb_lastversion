package be.celsius.util;

public class Entry 
{
	private String labelType = "string";
	private String type;
	private String name;
	private String value;
	
	public Entry(String l, String t, String n, String v)
	{
		setLabelType(l);
		setType(t);
		setName(n);
		setValue(v);
	}

	public String getLabelType() {
		return labelType;
	}

	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String toString()
	{
		return 	"<entry>" 
					+ "<" + getLabelType() + ">" + getName() + "</" + getLabelType() + ">" 
					+ "<" + getType() + ">" + getValue() + "</" + getType() + ">" 
					+ "</entry>";
	}
}
