package be.celsius.util.bean;

import org.json.JSONObject;

import be.celsius.util.Json;

public class NullLeaf extends LeafBean
{
	/**
	 * @param serialVersionUID
	 */
	private static final long serialVersionUID = -5364271135257955958L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void reset(){}
	
	public void clear(){}

	public boolean isLeaf()
	{
		return true;
	}
	
	public boolean isNullSeed()
	{
		return true;
	}

	public JSONObject toJson()
	{
		return new Json().put("null","true").getJson();
	}
	
	public String toXml()
	{
		return "null";
	}
	
	public String toString()
	{
		return "null";
	}
}
