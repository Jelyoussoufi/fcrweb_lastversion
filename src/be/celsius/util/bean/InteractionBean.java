package be.celsius.util.bean;

public class InteractionBean {

	private String action;
	private String formId;
	
	
	public InteractionBean()
	{
		
	}


	public String getAction() {
		return action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public String getFormId() {
		return formId;
	}


	public void setFormId(String formId) {
		this.formId = formId;
	}
	
	
}
