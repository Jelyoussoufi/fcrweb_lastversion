package be.celsius.util.bean;
import java.io.Serializable;

import org.json.JSONObject;

import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;


public class SeedBean extends LeafBean implements Serializable
{

	private static final long serialVersionUID = -4033336753136825542L;
	
	private static final int TYPE_BYTE 		= 0;
	private static final int TYPE_SHORT 	= TYPE_BYTE		+ 1;
	private static final int TYPE_INT 		= TYPE_SHORT	+ 1;
	private static final int TYPE_LONG 		= TYPE_INT		+ 1;
	private static final int TYPE_FLOAT		= TYPE_LONG		+ 1;
	private static final int TYPE_DOUBLE	= TYPE_FLOAT	+ 1;
	private static final int TYPE_BOOLEAN	= TYPE_DOUBLE	+ 1;
	private static final int TYPE_CHAR 		= TYPE_BOOLEAN	+ 1;
	private static final int TYPE_STRING	= TYPE_CHAR		+ 1;	
	
	private byte seed_byte;
	private short seed_short;
	private int	seed_int;
	private long seed_long;
	private float seed_float;
	private double seed_double;
	private boolean seed_boolean;
	private char seed_char;
	private String seed_string;
	
	private int type;
	
	public SeedBean()
	{
		beanId = "Seed";
	}
	
	public SeedBean(byte seed)
	{
		this();
		setSeed_byte(seed);
	}
	
	public SeedBean(short seed)
	{
		this();
		setSeed_short(seed);
	}
	
	public SeedBean(int seed)
	{
		this();
		setSeed_int(seed);
	}
	
	public SeedBean(long seed)
	{
		this();
		setSeed_long(seed);
	}
	
	public SeedBean(float seed)
	{
		this();
		setSeed_float(seed);
	}
	
	public SeedBean(double seed)
	{
		this();
		setSeed_double(seed);
	}
	
	public SeedBean(boolean seed)
	{
		this();
		setSeed_boolean(seed);
	}
		
	public SeedBean(char seed)
	{
		this();
		setSeed_char(seed);
	}
	
	public SeedBean(String seed)
	{
		this();
		setSeed_string(seed);
	}

	
	public String getSeed()
	{
		String seed = "";
		switch (type)
		{
			case TYPE_BYTE : 	seed += seed_byte ; 	break;
			case TYPE_SHORT : 	seed += seed_short ; 	break;
			case TYPE_INT : 	seed += seed_int ; 		break;
			case TYPE_LONG : 	seed += seed_long ; 	break;
			case TYPE_FLOAT : 	seed += seed_float ; 	break;
			case TYPE_DOUBLE : 	seed += seed_double ; 	break;
			case TYPE_BOOLEAN : seed += seed_boolean ; 	break;
			case TYPE_CHAR : 	seed += seed_char ; 	break;
			case TYPE_STRING : 	seed += seed_string ; 	break;
			default : break;
		}
		return seed;
	}
	
	public void reset()
	{
		switch (type)
		{
			case TYPE_BYTE : 	seed_byte = 0; 			break;
			case TYPE_SHORT : 	seed_short = 0;			break;
			case TYPE_INT : 	seed_int = 0;			break;
			case TYPE_LONG : 	seed_long = 0L;			break;
			case TYPE_FLOAT : 	seed_float = 0F; 		break;
			case TYPE_DOUBLE : 	seed_double = 0D;		break;
			case TYPE_BOOLEAN : seed_boolean = false; 	break;
			case TYPE_CHAR : 	seed_char = ' ';		break;
			case TYPE_STRING : 	seed_string = ""; 		break;
			default : break;
		}
	}
	
	public void clear()
	{
		reset();
	}
	
	public boolean isLeaf()
	{
		return true;
	}
	
	public JSONObject toJson()
	{
		try
		{
			JSONObject root = new JSONObject();
			root.put(beanId, getSeed());
			
			return root;
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
		}
		return null;
	}
	
	public String toXml()
	{
		return getSeed();
	}
	
	public String toString()
	{
		String seed = getSeed();
		if (UtilStr.isEmpty(seed))
			return "''";
		return seed;
	}
	

	public byte getSeed_byte() {
		return seed_byte;
	}

	public void setSeed_byte(byte seed_byte) {
		type = TYPE_BYTE;
		this.seed_byte = seed_byte;
	}
	
	public short getSeed_short() {
		return seed_short;
	}

	public void setSeed_short(short seed_short) {
		type = TYPE_SHORT;
		this.seed_short = seed_short;
	}

	public int getSeed_int() {
		return seed_int;
	}

	public void setSeed_int(int seed_int) {
		type = TYPE_INT;
		this.seed_int = seed_int;
	}

	public long getSeed_long() {
		return seed_long;
	}

	public void setSeed_long(long seed_long) {
		type = TYPE_LONG;
		this.seed_long = seed_long;
	}

	public float getSeed_float() {
		return seed_float;
	}

	public void setSeed_float(float seed_float) {
		type = TYPE_FLOAT;
		this.seed_float = seed_float;
	}

	public double getSeed_double() {
		return seed_double;
	}

	public void setSeed_double(double seed_double) {
		type = TYPE_DOUBLE;
		this.seed_double = seed_double;
	}

	public boolean getSeed_boolean() {
		return seed_boolean;
	}

	public void setSeed_boolean(boolean seed_boolean) {
		type = TYPE_BOOLEAN;
		this.seed_boolean = seed_boolean;
	}

	public char getSeed_char() {
		return seed_char;
	}

	public void setSeed_char(char seed_char) {
		type = TYPE_CHAR;
		this.seed_char = seed_char;
	}

	public String getSeed_string() {
		return seed_string;
	}

	public void setSeed_string(String seed_string) {
		type = TYPE_STRING;
		this.seed_string = seed_string;
	}

	public static void main(String[] args)
	{
		
	}
	
}
