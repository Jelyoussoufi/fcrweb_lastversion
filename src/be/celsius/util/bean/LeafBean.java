package be.celsius.util.bean;
import java.io.Serializable;

import org.json.JSONObject;

import be.celsius.util.UtilApp;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;


public abstract class LeafBean implements Serializable
{
	private static final long serialVersionUID 	= 1L;
	protected String beanId 					= "Leaf";
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getBeanId() {
		return beanId;
	}
	
	public LeafBean setBeanId(String id){
		beanId = id;
		return this;
	}

	public boolean isNullSeed()
	{
		return false;
	}
	
	public abstract void reset();
	
	public abstract void clear();

	public boolean isLeaf(){return false;}
	
	public boolean isBox(){return false;}
	
	public boolean isBasket(){return false;}

	public boolean isFruit(){return false;}
	
	public abstract JSONObject toJson();
	
	public abstract String toXml();
	
	public abstract String toString();
	
	/**
	 * CAST LEAF
	 */
	public SeedBean toSeed()
	{
		return (SeedBean) this;
	}
	
	public BoxBean toBox()
	{
		return (BoxBean) this;
	}
	
	public BasketBean toBasket()
	{
		return (BasketBean) this;
	}
	
	public FruitBean toFruit()
	{
		return (FruitBean) this;
	}
}
