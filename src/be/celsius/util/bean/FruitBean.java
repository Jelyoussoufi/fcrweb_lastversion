package be.celsius.util.bean;
import java.io.Serializable;
import java.util.HashMap;

import org.json.JSONObject;

import be.celsius.util.UtilLog;


public class FruitBean extends LeafBean implements Serializable
{

	private static final long serialVersionUID 	= -1856655872641115058L;

	private String fleshId; 
	private String peelId; 
	private LeafBean flesh;		// main data
	private LeafBean peel;		// metadata
	
	public FruitBean(LeafBean flesh, LeafBean peel)
	{
		beanId			= "Fruit";
		this.peel 		= peel;
		this.flesh 		= flesh;
		this.peelId 	= "Peel";
		this.fleshId	= "Flesh";
	}
		
	public FruitBean(String fleshId, LeafBean flesh, String peelId, LeafBean peel)
	{
		this(flesh, peel);
		this.peelId 	= peelId;
		this.fleshId	= fleshId;
	}
	
	public void reset()
	{
		getFlesh().reset();
		getPeel().reset();
	}
	
	public void clear()
	{
		getFlesh().clear();
		getPeel().clear();
	}
	
	public LeafBean getFlesh() {
		return flesh;
	}

	public LeafBean getPeel() {
		return peel;
	}
	
	public boolean isFruit()
	{
		return true;
	}

	public JSONObject toJson() {
		JSONObject json = new JSONObject();
		JSONObject root = new JSONObject();

		try {
			root.put(beanId, json);
			json.put(peelId, peel.toJson());
			json.put(fleshId, flesh.toJson());

			return root;
		} catch (Exception e) 
		{
			UtilLog.printException(e);
		}

		return null;
	}

	public String toXml() {
		String xml = "";

		xml += "<" + beanId + ">";
		xml += 	"<" + peelId + ">";
		/*
		if (peel.isLeaf())
			xml += 	"<leaf value='" + peel.toXml() + "' />";
		else
		*/
			xml += 		peel.toXml();	
		xml += 	"</" + peelId + ">";
		xml += 	"<" + fleshId + ">";
		/*
		if (flesh.isLeaf())
			xml += 	"<leaf value='"	+ flesh.toXml() + "' />";
		else
		*/
			xml += 		flesh.toXml();	
		xml += 	"</" + fleshId + ">";
		xml += "</" + beanId + ">";

		return xml;
	}

	public String toString() {
		return beanId + "[" + peelId + ":" + peel.toString() + ", " + fleshId + ":" + flesh.toString() + "]";
	}
	
	public static void main(String [] args)
	{
		FruitBean result 	= new FruitBean(new BoxBean().setBeanId("ServerInfo"),new BoxBean().setBeanId("Resources")).setBeanId("Server").toFruit();
		System.out.println(result.toString());
	}
}
