package be.celsius.util.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;


public class BasketBean extends LeafBean implements Serializable
{
	private static final long serialVersionUID 	= 7836065092367323511L;
	
	private List<LeafBean> basket;
	
	public BasketBean()
	{
		beanId = "Basket"; 
		reset();
	}
	
	public void reset() 
	{
		basket = new ArrayList<LeafBean>();
	}
	
	public void clear()
	{
		reset();
	}
	
	public void add(LeafBean leaf)
	{
		basket.add(leaf);
	}
		
	public List<LeafBean> getBasket() {
		return basket;
	}

	public boolean isBasket()
	{
		return true;
	}
	
	public JSONObject toJson() 
	{
		JSONArray array = new JSONArray();
		JSONObject root = new JSONObject();

		try 
		{
			root.put(beanId, array);
			for (LeafBean leaf : basket)
				array.put(leaf.toJson());

			return root;
		} catch (JSONException e) 
		{
			UtilLog.printException(e);
		}
		return null;
	}

	public String toXml() {
		String xml = "";

		xml += "<" + beanId + ">";
		for (LeafBean leaf : basket)
		{
			/*
			if (leaf.isLeaf())
				xml += 	"<leaf value='"	+ leaf.toXml() + "' />";
			else
			*/
				xml += leaf.toXml();
		}
		xml += "</" + beanId + ">";

		return xml;
	}

	public String toString() 
	{
		String leafs = "";
		for (LeafBean leaf : basket)
			leafs += leaf.toString() + ",";
		return beanId + "[" + UtilStr.unTail(leafs, ",") + "]";
	}
	
	public static void main(String[] args)
	{
		BasketBean basket = new BasketBean();
		
		basket.add(new SeedBean("peer"));
		basket.add(new SeedBean("apple"));
		basket.add(new SeedBean("peach"));
		basket.add(new FruitBean(new SeedBean("comestible"),new SeedBean("cherry")));
		basket.add(new FruitBean(new SeedBean("non comestible"),new SeedBean("mushroom")));
		basket.add(new FruitBean(new SeedBean("non comestible"),new SeedBean(true)));
		
		System.out.println(basket.toJson().toString());
		System.out.println(basket.toXml());
		System.out.println(basket.toString());
	}
}
