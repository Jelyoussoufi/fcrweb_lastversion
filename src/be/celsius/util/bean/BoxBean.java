package be.celsius.util.bean;
import java.io.Serializable;
import java.util.HashMap;

import org.json.JSONObject;

import be.celsius.util.UtilApp;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;


public class BoxBean extends LeafBean implements Serializable
{
	private static final long serialVersionUID 	= 5937205711321978755L;
		
	private HashMap<String,LeafBean> box;
	
	public BoxBean()
	{
		beanId 	= "Box";
		box 	= new HashMap<String,LeafBean>();
	}
	
	public BoxBean(String[] leastData)
	{
		this();
		hadLeastElements(leastData);
	}
	
	
	public void put(String key, LeafBean value)
	{
		box.put(key, value);
	}
		
	public LeafBean get(String key)
	{
		if (box.containsKey(key))
			return box.get(key);
		else
			return UtilApp.NULLSEED;
	}
	
	
	public void hadLeastElements(String[] leastElements)
	{
		for (String key : leastElements)
		{
			if (!box.containsKey(key))
				box.put(key, UtilApp.NULLSEED);
		}
	}
	
	public BasketBean getKeys()
	{
		BasketBean keys = new BasketBean();
		
		for (String key : box.keySet())
			keys.add(new SeedBean(key));
		
		return keys;
	}
	
	public void reset()
	{
		for(String key : box.keySet())
			box.put(key, UtilApp.NULLSEED);
	}
	
	public void clear()
	{
		box.clear();
	}
	
	public boolean isBox()
	{
		return true;
	}
	
	public JSONObject toJson() 
	{
		JSONObject json = new JSONObject();
		JSONObject root = new JSONObject();

		try {
			root.put(beanId, json);
			for (String key : box.keySet())
			{
				json.put(key, get(key).toJson());
			}

			return root;
		} catch (Exception e) {
			UtilLog.printException(e);
		}

		return null;
	}

	public String toXml() 
	{
		String xml = "";

		xml += "<" + beanId +">";
		for (String key : box.keySet())
		{
			/*
			if (get(key).isLeaf())
				xml += "<" + key + " value='" + get(key).toXml() + "' />";
			else
			*/
			{
				xml += "<" + key + ">";
					xml += get(key).toXml();
				xml += "</" + key + ">";
			}
				
		}
		xml += "</" + beanId +">";

		return xml;
	}

	public String toString() 
	{
		String rep = beanId + "[";
		for (String key : box.keySet())
			rep +=  key + ":" + get(key).toString() + ",";
		return UtilStr.unTail(rep, ",") + "]";
	}
	
	public static void main(String[] args)
	{
		BoxBean box = new BoxBean();
		
		box.put("a", new SeedBean("1"));
		box.put("b", new SeedBean("2"));
		System.out.println(box.toString());
		box.put("b", new SeedBean("555"));
		System.out.println(box.toString());
		String params = "";
		
		for(LeafBean leaf : box.getKeys().getBasket())
		{
			String key = leaf.toSeed().getSeed();
			params += "&" + key + "=" + box.get(key).toSeed().getSeed();
		}
		params = UtilStr.unHead(params, "&");
		System.out.println(params);
	}
	
}
