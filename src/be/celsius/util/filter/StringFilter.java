/**
 * 
 */
package be.celsius.util.filter;

/**
 * @author jhuilian
 *
 */
public interface StringFilter extends Filter
{	
	public String doFilter(String element);
}
