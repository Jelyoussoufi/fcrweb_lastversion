/**
 * 
 */
package be.celsius.util.filter;

import be.celsius.util.UtilStr;

/**
 * @author jhuilian
 *
 */
public class WrapperFilter implements StringFilter{

	private String headWrapper;
	private String tailWrapper;
	
	public WrapperFilter(String hw, String tw)
	{
		this.headWrapper = hw;
		this.tailWrapper = tw;				
	}
	
	public WrapperFilter(String wrapper)
	{
		this(wrapper, wrapper);
	}
	
	public String doFilter(String toFilter)
	{
		return UtilStr.wrap(toFilter, headWrapper, tailWrapper);
	}
}
