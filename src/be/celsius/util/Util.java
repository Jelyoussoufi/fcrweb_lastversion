package be.celsius.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import be.celsius.util.datastructure.Ulist;
import be.celsius.util.datastructure.Uset;

public class Util 
{	
	public static void main(String[] args) {
		
		System.out.println(Util.array2string(Util.wrapAll("g1^g2^g3^g4".split("\\^"), "'"), ",", "", "(", ")"));
		System.out.println(Util.array2string(Util.wrapAll("".split("\\^"), "'"), ",", "", "(", ")"));
	}
	/**
	 * @param lower the lower bound of random value
	 * @param higher the upper bound of random value
	 * @return a number comprises between lower <= x <= higher.
	 */
	public static int rand(int lower, int higher)
	{
		return (int)(Math.random() * (higher+1-lower)) + lower;
	}
	
	/**
	 * @param s a string representation of an boolean 
	 * @return the boolean value
	 * false if s is not representing a boolean
	 */
	public static boolean getBool(String bool)
	{
		try { return Boolean.parseBoolean(bool.trim()); }
		catch (Exception e){ return false; }
	}
	
	/**
	 * @param s a string representation of an int 
	 * @return the int value
	 * -1 if s is not representing an int
	 */
	public static int getInteger(String s)
	{
		try { return Integer.parseInt(s.trim()); }
		catch (Exception e){ return -1; }
	}
	
	/**
	 * 
	 * @param chain any chain
	 * @return all digit character concatenated with leading zeros (if any)
	 * @e.g 'b0b4 2 4bb4 4 l1fe' => '0424441'
	 */
	public static String getNumber(String chain)
	{
		String number = "";
		for (char c : chain.toCharArray())
		{
			if (Character.isDigit(c))
				number += c;
		}
		return number;
	}
	
	public static String getCharacters(String chain)
	{
		String number = "";
		for (char c : chain.toCharArray())
		{
			if (Character.isLetter(c))
				number += c;
		}
		return number;
	}
	
	/**
	 * @return true if the string s is a valid representation of an integer
	 */
	public static boolean isInteger(String s)
	{
		try{ Integer.parseInt(s.trim()); return true; }
		catch (Exception e) { return false; }
	}
	
	/**
	 * @param s a string representation of a long 
	 * @return the long value
	 * -1 if s is not representing a long
	 */
	public static long getLong(String s)
	{
		try { return Long.parseLong(s.trim()); }
		catch (Exception e){ return -1; }
	}
	
	/**
	 * @return true if the string s is a valid representation of a long
	 */
	public static boolean isLong(String s)
	{
		try{ Long.parseLong(s.trim()); return true; }
		catch (Exception e) { return false; }
	}
	
	/**
	 * @param s a string representation of a double 
	 * @return the double value
	 * -1 if s is not representing a double
	 */
	public static double getDouble(String s)
	{
		try { return Double.parseDouble(s.trim()); }
		catch (Exception e){ return -1.0; }
	}
	
	/**
	 * @return true if the string s is a valid representation of a double
	 */
	public static boolean isDouble(String s)
	{
		try{ Double.parseDouble(s.trim()); return true; }
		catch (Exception e) { return false; }
	}
	

	/**
	 * @param d a double to be formatted
	 * @return a string representation of double in decimal format
	 */
	public static String prettyPrint(double d)
	{
		NumberFormat formatter = new DecimalFormat("#,###,###.####");
		return formatter.format(d);
	}

	
	/**
	 * @return true if all boolean of tests are true
	 */
	public static boolean isAllTrue(boolean[] tests){return isAllBool(tests, true);}
	
	/**
	 * @return true if all boolean of tests are false
	 */
	public static boolean isAllFalse(boolean[] tests){return isAllBool(tests, false);}
	
	private static boolean isAllBool(boolean[] tests, boolean bool)
	{
		for (int i=0; i<tests.length; i++)
		{
			if (tests[i]!=bool) return false;
		}
		return true;
	}
	
	/**
	 * @return the number of boolean set to true in tests
	 */
	public static int countTrue(boolean[] tests)
	{
		int nbTrue = 0;
		for (int i=0; i<tests.length; i++)
			if (tests[i])nbTrue ++;
		
		return nbTrue;
	}
	
	/**
	 * @return the number of boolean set to true in tests
	 */
	public static int countFalse(boolean[] tests)
	{
		return tests.length - countTrue(tests);
	}
	
	/**********************
	 * Array A1 Functions *
	 **********************/
	
	/**
	 * @param a1 an array of objects
	 * @param a2 an array of objects
	 * @return an array [a1_0 .. a1_m-1 a2_0 .. a2_n-1] with m = size of a1 and n = size of a2.
	 */
	public static Object[] mergeA1(Object[] a1, Object[] a2)
	{
		Object [] a3 = new Object[a1.length+a2.length];
		
		for (int i=0; i<a1.length; i++)a3[i] = a1[i];
		int idx=0;
		for (int i=a1.length; i<a1.length+a2.length; i++)
		{
			a3[i] = a2[idx];
			idx++;
		}
		return a3;
	} 
	
	/**
	 * @param a an array of objects
	 * return a string representation of the array
	 */
	public static String getPrintA1(Object[] a)
	{
		String res = "[";
		for(int i=0; i<a.length; i++)
			res += a[i].toString() + ", ";
		return UtilStr.unTail(res, ", ") + "]";
	}
	
	/**********************
	 * Array A2 Functions *
	 **********************/
	
	/**
	 * 
	 * @param x number of subArray
	 * @param y size of subArray
	 * @return an array a2[x][y] of empty strings
	 */
	public static String[][] newA2(int x, int y, String init)
	{	
		String [][] a2 = new String[x][y];
		for(int i=0; i<a2.length; i++)
			for(int j=0; j<a2[i].length; j++)
				a2[i][j]=init;
		return a2;
	}
	
	public static String[][] newA2(int x, int y){return newA2(x,y);}
	
	/**
	 * @param a an array a2 of objects
	 * return a string representation of the array a2
	 */
	public static String getPrintA2(Object[][] a2)
	{
		String res = "[\n";
		for(int i=0; i<a2.length; i++)
		{
			res += "[";
			for(int j=0; j<a2[i].length; j++)
				res += a2[i][j].toString()+", ";
			res = UtilStr.unTail(res, ", ") + "],\n";
		}
		return UtilStr.unTail(res, ",\n") + "\n]";
	}
	
	
	public static String[] wrapAll(String[] array, String wrapper)
	{
		String[] newarray = new String[array.length];
		for (int i=0 ; i<array.length ; i++)
		{
			newarray[i] = UtilStr.wrap(array[i],wrapper);
		}
		return newarray;
	}
	
	public static List<String> wrapAll(List<String> list, String wrapper)
	{
		Ulist newlist = new Ulist();
		for (int i=0 ; i<list.size() ; i++)
			newlist.add(i, UtilStr.wrap(list.get(i),wrapper));
		return newlist;
	}
	
	public static Set<String> wrapAll(Set<String> set, String wrapper)
	{
		Uset newset 			= new Uset();
		Iterator<String> iter 	= set.iterator();
		
		while(iter.hasNext())
		{
			newset.add(UtilStr.wrap(iter.next(), wrapper));
		}
		
		return newset;
	}
	
	
	
	/*******************
	 *  DATASTRUCTURES *
	 *******************/
	
	/**************************************
	 * CASTING ARRAY, LIST, SET => STRING *
	 **************************************/
	
	/**
	 * @return name + beg + setAsString(sep) + end
	 * eg: set { "Cetelem1", "Cetelem2", "Cetelem3" } 
	 */
	@SuppressWarnings({ "rawtypes"})
	public static String set2string(Set set, String sep, String name)
	{
		return set2string(set, sep, name, "{", "}");
	}
	
	@SuppressWarnings({ "rawtypes"})
	public static String set2string(Set set, String sep, String name, String beg, String end)
	{
		String s = "";
		for (Object o: set) {s += o.toString() + sep;}
		return name + beg + UtilStr.unTail(s, sep) + end;
	}
	
	/**
	 * @return name + beg + listAsString(sep) + end
	 * eg: list ( "elem1", "elem2", "elem3" )
	 */
	@SuppressWarnings({ "rawtypes"})
	public static String list2string(List list, String sep, String name)
	{
		return list2string(list, sep, name, "(", ")");
	}
	
	@SuppressWarnings({ "rawtypes"})
	public static String list2string(List list, String sep, String name, String beg, String end)
	{
		String s = "";
		for (Object o: list) {s += o.toString() + sep;}
		return name + beg + UtilStr.unTail(s, sep) + end;
	}
	
	/**
	 * @return name + beg + array2string(sep) + end
	 * eg: array [ "elem1", "elem2", "elem3" ]
	 */
	public static String array2string(Object[] array, String sep, String name)
	{
		return array2string(array, sep, name, "[", "]");
	}

	public static String array2string(Object[] array, String sep, String name, String beg, String end)
	{
		String s = "";
		for (int i = 0; i < array.length; i++) {s += array[i].toString() + sep;}
		return name + beg + UtilStr.unTail(s, sep) + end;
	}
	
	
	
	/**********************************
	 * CASTING ARRAY <=> LIST <=> SET *
	 **********************************/
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Set array2set(Object[] collections)
	{
		Set newCollections = new HashSet();
		for (Object s: collections) newCollections.add(s);
		return newCollections;
	}
	
	@SuppressWarnings({ "rawtypes"})
	public static Object[] set2array(Set collections)
	{
		Object[] newCollections 	= new Object[collections.size()];
		int i						= 0;
		for (Object obj : collections){newCollections[i] = obj;i++;}
		return newCollections;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List array2list(Object[] collections)
	{
		List newCollections = new ArrayList();
		for (int i = 0; i < collections.length; i++) newCollections.add(collections[i]);		
		return newCollections;
	}
	
	@SuppressWarnings({ "rawtypes"})
	public static Object[] list2array(List collections)
	{
		Object[] newCollections = new Object[collections.size()];
		for (int i = 0; i < collections.size(); i++) newCollections[i] = collections.get(i);	
		return newCollections;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List set2list(Set collections)
	{
		List newCollections = new ArrayList();
		for (Object element : collections) newCollections.add(element);
		return newCollections;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Set list2set(List collections)
	{
		Set newCollections = new HashSet();
		for (Object element : collections) newCollections.add(element);
		return newCollections;
	}

	public static Map<String, String> merge(Map<String, String> map1, Map<String, String> map2)
	{
		Map<String, String> map = new HashMap<String, String>();
		
		if (map1!=null)
			for (String key: map1.keySet())
				map.put(key, map1.get(key));
		if (map2!=null)
			for (String key: map2.keySet())
				map.put(key, map2.get(key));
		
		return map;
	}
	
	
	/*****************************
	 * CAST PRIMITIVE <=> OBJECT *
	 *****************************/
	
	
	public static boolean[] getA1(int size, boolean defaultValue)
	{
		boolean[] array = new boolean[size];
		for (int i=0; i<array.length; i++)array[i] = defaultValue;
		return array;
	}
	
	public static char[] getA1(int size, char defaultValue)
	{
		char[] array = new char[size];
		for (int i=0; i<array.length; i++)array[i] = defaultValue;
		return array;
	}
	
	public static byte[] getA1(int size, byte defaultValue)
	{
		byte[] array = new byte[size];
		for (int i=0; i<array.length; i++)array[i] = defaultValue;
		return array;
	}
	
	public static short[] getA1(int size, short defaultValue)
	{
		short[] array = new short[size];
		for (int i=0; i<array.length; i++)array[i] = defaultValue;
		return array;
	}
	
	public static int[] getA1(int size, int defaultValue)
	{
		int[] array = new int[size];
		for (int i=0; i<array.length; i++)array[i] = defaultValue;
		return array;
	}
	
	public static long[] getA1(int size, long defaultValue)
	{
		long[] array = new long[size];
		for (int i=0; i<array.length; i++)array[i] = defaultValue;
		return array;
	}
	
	public static float[] getA1(int size, float defaultValue)
	{
		float[] array = new float[size];
		for (int i=0; i<array.length; i++)array[i] = defaultValue;
		return array;
	}
	
	public static double[] getA1(int size, double defaultValue)
	{
		double[] array = new double[size];
		for (int i=0; i<array.length; i++)array[i] = defaultValue;
		return array;
	}
	
	public static Boolean[] convert(boolean[] array)
	{
		Boolean[] tab = new Boolean[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}

	public static Character[] convert(char[] array)
	{
		Character[] tab = new Character[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static Byte[] convert(byte[] array)
	{
		Byte[] tab = new Byte[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}

	public static Short[] convert(short[] array)
	{
		Short[] tab = new Short[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}

	public static Integer[] convert(int[] array)
	{
		Integer[] tab = new Integer[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static Long[] convert(long[] array)
	{
		Long[] tab = new Long[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static Float[] convert(float[] array)
	{
		Float[] tab = new Float[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static Double[] convert(double[] array)
	{
		Double[] tab = new Double[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static boolean[] convert(Boolean[] array)
	{
		boolean[] tab = new boolean[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static char[] convert(Character[] array)
	{
		char[] tab = new char[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static byte[] convert(Byte[] array)
	{
		byte[] tab = new byte[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static short[] convert(Short[] array)
	{
		short[] tab = new short[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static int[] convert(Integer[] array)
	{
		int[] tab = new int[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static long[] convert(Long[] array)
	{
		long[] tab = new long[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static float[] convert(Float[] array)
	{
		float[] tab = new float[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	public static double[] convert(Double[] array)
	{
		double[] tab = new double[array.length];
		for (int i=0 ; i<tab.length ; i++)
			tab[i] = array[i];
		return tab;
	}
	
	/**
	 * 
	 * @param map a list of pairs key:value
	 * @param keyValueSeparator (KVS) : | &sep; | :::
	 * @param entrySeparator (ES)	; | &new; | ---
	 * @return return a string representation of the map 
	 * MK0 * KVS_0 * MV0 * ES_0 * MK1 * KVS_1 * MV1 ES_1 ... ES_n-2 MK_n-1 * KVS_n-1 * MV_n-1
	 * (* concatenation operation)
	 */
	public static String map2string(Map<String,String> map, String keyValueSeparator, String entrySeparator)
	{
		String result = "";
		try
		{
		if (map==null)
			return "";
		for (String key : map.keySet())
			result += key + keyValueSeparator + map.get(key) + entrySeparator;
		
		return UtilStr.unTail(result, entrySeparator);
		}
		catch (Exception e)
		{
			return "";
		}
	}
	
}
