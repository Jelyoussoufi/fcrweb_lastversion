package be.celsius.util;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SimpleDomParser 
{
	private Element root;
	private Document document;
	private HashMap<String, Node> nodeMap;
	
	public SimpleDomParser(String xml)
	{
		document	= getDocument(xml);
		root 		= document.getDocumentElement();
		nodeMap		= new HashMap<String, Node>();
		nodeMap.put("root", root);
	}
	
	private Document getDocument(String xmlResponse)
	{
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(false);
			factory.setValidating(false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(xmlResponse.getBytes("UTF-8"));
			Document document 		= builder.parse(is);
			is.close();
			
			return document;
		}
		catch(Exception e){}
		return null;
	}

	private Element getElementSource(String src)
	{
		Element element = root;
		
		if (!UtilStr.isEmpty(src) & nodeMap.containsKey(src))
			element = (Element)nodeMap.get(src);
			
		return element;
	}
	
	public Node getNode(String element, String nodeName)
	{
		try
		{
			return getElementSource(element).getElementsByTagName(nodeName).item(0);
		}
		catch (Exception e){}
		return null;
	}
	
	
	
	public Element getNodeByE(Element element, String nodeName)
	{
		try
		{
			if (element==null)
				element = root;
			return (Element)element.getElementsByTagName(nodeName).item(0);
		}
		catch (Exception e){}
		return null;
	}
	
	public List<Element> getNodes(String element, String nodeName)
	{
		try
		{
			return list2array(getElementSource(element).getElementsByTagName(nodeName));
		}
		catch (Exception e){}
		return new ArrayList<Element>();
	}
	
	public List<Element> getNodesByE(Element element, String nodeName)
	{
		try
		{
			if (element==null)
				element = root;
			return list2array(element.getElementsByTagName(nodeName));
		}
		catch (Exception e){}
		return new ArrayList<Element>();
	}
	
	private List<Element> list2array(NodeList list)
	{
		ArrayList<Element> array = new ArrayList<Element>();
		
		try
		{
			for (int i=0 ; i<list.getLength() ; i++)
				 array.add((Element)list.item(i));
		}
		catch (Exception e)
		{
			array = new ArrayList<Element>();
		}
		return array;
	}
	
	public String getValue(Element elem, String nodeName)
	{
		boolean found = false;
		try
		{
			String value = "";
			if (elem==null)
				elem = root;
		    NodeList elements = elem.getElementsByTagName(nodeName);
	
		    for (int j = 0; j < elements.getLength(); j++)
		    {
		        Element element = (Element) elements.item(j);
		        NodeList children = element.getChildNodes();
		        StringBuffer sb = new StringBuffer();
		        for (int k = 0; k < children.getLength(); k++) 
		        {
		            Node child = children.item(k);
		            if (child.getNodeType() == Node.TEXT_NODE) 
		            {
//System.out.println(j + " - " + k + " - " + child.getNodeValue());
		                sb.append(child.getNodeValue());
		                value +=sb.toString();
		                found = true;	// stop when first occurence have been found
		            }
		            if (found)break;
		        }
		        if (found)break;
		    }
		    return value;
		}
		catch (Exception e)
		{
			return "";
		}
	}

	public static void main1(String[] args) {

		String xml = "";

		xml += "	<soapenv:Envelope 	 ";
		xml += "	xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' 	 ";
		xml += "	xmlns:xsd='http://www.w3.org/2001/XMLSchema' 	 ";
		xml += "	xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>	 ";
		xml += "	<soapenv:Header/>	 ";
		xml += "	<soapenv:Body>	 ";
		xml += "	<BonitaOnCerberus>	 ";
		xml += "	<Request soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>	 ";
		xml += "	<action_type>RemoveResourceFromApplication</action_type>	 ";
		xml += "	<info>	 ";
		xml += "	<requestorId>1</requestorId>	 ";
		xml += "	<resourceId>109</resourceId>	 ";
		xml += "	<applicationId>43</applicationId>	 ";
		xml += "	</info>	 ";
		xml += "	<chainActions>	 ";
		xml += "	<chainAction>	 ";
		xml += "	<action_type>RemoveResourceFromSafe</action_type>	 ";
		xml += "	<required>1</required>	 ";
		xml += "	<actionInfo>	 ";
		xml += "	<requestorId>1</requestorId>	 ";
		xml += "	<resourceId>109</resourceId>	 ";
		xml += "	<safeId>216</safeId>	 ";
		xml += "	</actionInfo>	 ";
		xml += "	</chainAction>	 ";
		xml += "	</chainActions>	 ";
		xml += "	</Request>	 ";
		xml += "	<Log>	 ";
		xml += "	<statusCode>0</statusCode>	 ";
		xml += "	<statusDescription>OK</statusDescription>	 ";
		xml += "	<steps>null;GetApplicationStaffOwnerId;GetApplicationOwnerTeamId_done;GetStaffTeamId_done;CheckApplicationTeamOwnerConsistency_done;GetResourceSafeId;GetApplicationSafesId_done;CheckSafeConstraintIntegrity_done;RemoveResourceFromApplication_done;CheckRemoveResourceFromSafeRequired_done;InitNotificationRequest_done</steps>	 ";
		xml += "	</Log>	 ";
		xml += "	</BonitaOnCerberus>	 ";
		xml += "	</soapenv:Body>	 ";
		xml += "	</soapenv:Envelope>	 ";

		SimpleDomParser parser = new SimpleDomParser(xml);
		
		Element log = (Element)parser.getNode(null, "Log");
		System.out.println(parser.getValue(log, "statusDescription"));
		System.out.println(parser.getValue(null, "action_type"));
		System.out.println("nonExistNode:" + " - " + parser.getValue(null, "nonExistNode"));
		for (Element n : parser.getNodes("", "chainAction"))
		{
			System.out.println(parser.getValue(n, "action_type"));
		}
		/* */
		
		for (Element action : parser.getNodes("", "chainAction"))
		{
			if (parser.getValue(action, "required").equals("1"))
			{
				String requestorId 	= parser.getValue(action, "requestorId");
				String action_type 	= parser.getValue(action, "action_type");
				String version		= "1.0";
				String statusDescription 	= "";
				String statusError			= "";
				UtilLog.out("handleChainActions : requestorId:" + requestorId + " - action_type:" + action_type);
			}
		}
		
		/*
		NodeList books = doc.getElementsByTagName("chainAction");
		for (int i = 0; i < books.getLength(); i++) {
		    Element book = (Element) books.item(i);
		    NodeList authors = book.getElementsByTagName("action_type");
		    boolean stephenson = false;
		    for (int j = 0; j < authors.getLength(); j++) {
		        Element author = (Element) authors.item(j);
		        NodeList children = author.getChildNodes();
		        StringBuffer sb = new StringBuffer();
		        for (int k = 0; k < children.getLength(); k++) {
		            Node child = children.item(k);
		            // really should to do this recursively
		            if (child.getNodeType() == Node.TEXT_NODE) {
		                sb.append(child.getNodeValue());
		            }
		        }
		        if (sb.toString().equals("RemoveResourceFromSafe")) {
		            stephenson = true;
		            System.out.println(sb.toString());
		            break;
		        }
		   }

		    if (stephenson) {
		        NodeList titles = book.getElementsByTagName("title");
		        for (int j = 0; j < titles.getLength(); j++) {
		            result.add(titles.item(j));
		        }
		        
		    }

		}
		*/
	}
	
	public static void main(String[] args) {
		String xml = "";
		xml += "<?xml version='1.0' encoding='UTF-8'?>";
		xml += "<data xmlns:ev='http://www.w3.org/2001/xml-events' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xf='http://www.w3.org/2002/xforms' xmlns:chiba='http://chiba.sourceforge.net/xforms' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' local='en'>";
		xml += "<form>my form 3</form>";
		xml += "<repeated>";
		xml += "<input a='text1' b='2012-01-01' c='true' d='1.1'/>";
		xml += "<input a='text2' b='2008-02-02' c='false' d='2.2'/>";
		xml += "<input a='text3' b='2008-03-03' c='true' d='3.3'/>";
		xml += "<input a='text4' b='2008-04-04' c='false' d='4.4'/>";
		xml += "<input a='text5' b='2008-05-05' c='true' d='5.5'/>";
		xml += "<input a='text6' b='2008-06-06' c='false' d='6.6'/>";
		xml += "<input a='text7' b='2008-07-07' c='true' d='7.7'/>";
		xml += "<input a='text8' b='2008-08-08' c='false' d='8.8'/>";
		xml += "<input a='text9' b='2008-09-09' c='true' d='9.9'/>";
		xml += "<input a='text10' b='2008-10-10' c='false' d='10.10'/>";
		xml += "</repeated>";
		xml += "<input a='text11' b='2008-11-11' c='true' d='11.11'/>";
		xml += "</data>";
		
		SimpleDomParser parser = new SimpleDomParser(xml);
		System.out.println(parser.getValue(null, "form"));
		//Element repeated = (Element)parser.getNode(null, "repeated");
		Element repeated = (Element)parser.getNode(null, "repeated");
		System.out.println("repeated?" + (repeated==null));
		List<Element> inputs = parser.getNodesByE(repeated, "input");
		System.out.println("inputs?" + inputs.size());
		for (Element input : inputs)
			System.out.println("b=" + input.getAttribute("b"));
		//System.out.println(parser.getValue(null, "input"));
		/*for (Element n : parser.getNodes("repeated", "input"))
			System.out.println(parser.getValue(n, "action_type"));
		*/

	}
}
