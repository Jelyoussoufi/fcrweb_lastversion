package be.celsius.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.SeedBean;


public class BonitaJ extends BonitaUtil{

	private Map<String,Entry> variables;
	
	  public BonitaJ()
	  {
		  super();
		  variables	= new HashMap<String,Entry>();
	  }
	  
	  /**
	   * => <bonitaServer>:<bonitaPort>/<restAPI>/<instantiateProcessAPI>/<processName>--<processVersion>
	   * @param {@code processName} the name of the process to create a new instance (e.g : myProcess)
	   * @param {@code processVersion} the version of the process (default : 1.0) 
	   * @pre {@code processName} is not null.
	   * @return the response of the bonita-rest-server that contains the processUUID in xml format
	   * @exception the result is the code error or the error message
	   */
	  public String instantiateProcess(String processName, String processVersion)
	  {
		  if (isEmpty(processVersion))
			  processVersion = "1.0";
		  return submitRestCommand
			(
				restUrl("runtimeAPI/instantiateProcessWithVariables/" + processName + "--" + processVersion),
				addOptionUser("")
			);
	  }
	  
	  /**
	   * => <bonitaServer>:<bonitaPort>/<restAPI>/<instantiateProcessAPI>/<processName>--<processVersion>
	   * @param {@code processName} the name of the process to create a new instance (e.g : myProcess)
	   * @param {@code processVersion} the version of the process (default : 1.0) 
	   * @pre {@code processName} is not null.
	   * @post a new instance of process <processName>--<processVersion> is created 
	   * with variables presents in {@code variables} map. 
	   * @return the response of the bonita-rest-server that contains the processUUID in xml format
	   * @exception the result is the code error or the error message
	   */
	  public String instantiateProcessWithVariables(String processName, String processVersion)
	  {
		  	return submitRestCommand
			(
				restUrl("runtimeAPI/instantiateProcessWithVariables/" + processName + "--" + processVersion),
				addOptionUser("variables=" + getVariables())
			);
	  }
	  
	  
	  /**
	   * @deprecated because security-constraint
	   */
	  public String setVariable(String taskUUID, String varId, String varVal)
	  {
		  	return submitRestCommand
			(
				restUrl("runtimeAPI/setVariable/" + taskUUID ),
				addOptionUser("variableId=" + varId + "&variableValue=" + varVal)
			);
	  }
	  
	  /**
	   * @deprecated because security-constraint
	   */
	  public String setProcessInstanceVariables(String processInstanceUUID)
	  {
		  	return submitRestCommand
			(
				restUrl("runtimeAPI/setProcessInstanceVariables/" + processInstanceUUID ),
				addOptionUser("variables=" + getVariables())
			);
	  }
	  
	  /**
	   * @deprecated because security-constraint
	   */
	  public String executeTask(String taskUUID, boolean assignTask)
	  {
		  return submitRestCommand
			(
				restUrl("runtimeAPI/executeTask/" + taskUUID + "/" + assignTask),
				addOptionUser("")
			);
	  }
	  /**
	   * @return the response of the bonita-rest-server
	   * @exception the result is the code error or the error message
	   */
	  public String getLightProcessInstances()
	  {
	  	return submitRestCommand
	  	(
	  		restUrl("queryRuntimeAPI/getLightProcessInstances"),
	  		addOptionUser("")
	  	); 
	  }
	  
	  public String deleteAllProcesses()
	  {
		  return submitRestCommand
			(
				restUrl("managementAPI/deleteAllProcesses"),
				addOptionUser("")
			);
	  }
	  
	  /**
	   * @pre if options parameter has been defined then user login has been also defined.
	   * @param parameters a chain that defines the parameters needed for rest operation
	   * @return the options=user:login has been added to list of parameters
	   */
	  private String addOptionUser(String parameters)
	  {
		  if (isEmpty(parameters) && !isEmpty(login))
		  {
			  parameters = "options=user:" + login;
		  }
		  else if(!parameters.contains("options=user:") && !isEmpty(login))
		  {
			  parameters = "options=user:" + login + "&" + parameters;
		  }
		  return parameters;
	  }

	  private String submitRestCommand(String restUrl, String parameters)
	  {
		  UtilLog.out1(restUrl + "\n" + parameters);
		  String result = "";
		  
		  try
		  {
			  result =  processResponse(getConnection(restUrl, parameters));
		  }
		  catch (Exception e)
		  {
			  result = "EXCEPTION: while establishing connection\n" + getStackTrace(e);
		  }
		  try
		  {
			  log = new BoxBean();
			  log.put("url", new SeedBean(restUrl));
			  log.put("parameters", new SeedBean(parameters));
			  log.put("result", new SeedBean(result));
			  UtilLog.out(log.toJson().toString());
		  }
		  catch (Exception e){}
		  return result;
	  } 
	  
	/* DEBUG purpose */
	private BoxBean log;
	public BoxBean getLog() {return log;}

	private HttpURLConnection getConnection(final String url, final String parameters) throws IOException,
	      MalformedURLException, ProtocolException , Exception
	      {

		    final HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
		    connection.setUseCaches (false);
		    connection.setDoInput(true);
		    connection.setDoOutput(true);
		    connection.setInstanceFollowRedirects(false);
		    connection.setRequestMethod("POST");
		    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		    
		    /**
			 * restuser:id0NotTrust
			 * cmVzdHVzZXI6aWQwTm90VHJ1c3Q=
			 * 
			 * restuser:restbpm
			 * cmVzdHVzZXI6cmVzdGJwbQ==
			 * 
			 * sshgateway:cyberark
			 * c3NoZ2F0ZXdheTpjeWJlcmFyaw==
			 * 
			 * cerberus:cerberus
			 * Y2VyYmVydXM6Y2VyYmVydXM=
			 *
			 */	
		    
		    String auth = "Basic " + getBonitaUPB64();
		    UtilLog.out("Authorization:" + auth);
			connection.setRequestProperty("Authorization", auth);
		   
		    final DataOutputStream output = new DataOutputStream(connection.getOutputStream());
		    output.writeBytes(parameters);
		    output.flush();
		    output.close();
		    connection.disconnect();
		   
		    return connection;
	  }

	  /**
	   * @param connection
	   * @throws IOException
	   */
	  private String processResponse(HttpURLConnection connection)
	  {
		  try
		  {
		    int responseCode = connection.getResponseCode();
		    String responseDescription = connection.getResponseMessage();
		    if(responseCode != HttpURLConnection.HTTP_OK)
		    {
		    	return "ERROR: errorCode-" + responseCode + "; errorDescription-" + responseDescription;
		    } 
		    else {

		      final InputStream is = connection.getInputStream();
		      final BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		      String line;
		      StringBuffer response = new StringBuffer();
		      try {
		        while((line = reader.readLine()) != null) {
		          response.append(line);
		          response.append('\n');
		        }
		      } finally {
		        reader.close();
		        is.close();
		      }

		      return response.toString().trim();
		    }
	  	}
		  catch (Exception e)
		  {
			  return "EXCEPTION: while processing response\n" + getStackTrace(e);
		  }
	  }

	private String getVariables()
	{
		String vars = "";
		
		for (String variable : variables.keySet())
		{
			vars+= variables.get(variable).toString();		
		}
		
		return "<map>" + vars + "</map>";
	}
		
	public void addVariable(String name, String value) 
	{
		addVariable("string", "string", name, value);
	}
	
	public void addVariable(String type, String name, String value) 
	{
		if (UtilStr.isEmpty(value))
			value = "";
		
		variables.put(name, new Entry("string", type, name, value));
	}
	
	public void addVariable(String labelType, String type, String name, String value)
	{
		if (UtilStr.isEmpty(value))
			value = "";
		
		variables.put(name, new Entry(labelType, type, name, value));
	}

	public void clearVariables() {
		variables.clear();
	}
	
	
	
	public static void main(String[] args) 
	{
	    //bonita.base64 package is not well detected. 
		/*
	    String userPwd = getBonitaAuthUser() + ":" + getBonitaAuthPassword();
		String base64 = new String(Base64.encodeBase64(userPwd.getBytes()));
		UtilLog.out("apache userPwd:" + userPwd + " - base64:" + base64);
		*/
		
	}
}	
