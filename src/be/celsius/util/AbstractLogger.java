package be.celsius.util;

import be.celsius.fcrweb.application.ApplicationContextLoader;

public class AbstractLogger 
{
	public static AbstractLogger getLoggerByApplicationContext(String name)
	{
		return (AbstractLogger) ApplicationContextLoader.getApplicationContext().getBean(name);
	}
}
