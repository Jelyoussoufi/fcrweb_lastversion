package be.celsius.util;

public class UtilStr 
{
	/**
	 * @param s the chain to read
	 * @param idx the index of character to read an int
	 * @return the value of int represented by the char at position idx if it is an instance of int
	 * else return -1;
	 * @example "56789",2 => 7 (index start at 0)
	 * 			"hello",2 => -1
	 */
	public static int readIntAt(String s, int idx)
    {
    	try
    	{
    		return Integer.parseInt(""+s.charAt(idx-1));
    	}
    	catch (Exception e)
    	{}
    	return -1;
    }
	
	public static String i2ascii(int i)
	{
		if (32<=i & i<=126)
		{	
			char c = (char) i;
			return c+"";
		}
		else return "null";
	}
	
	public static int ascii2i(char c){int i = c; return i;}
	
	/**
	 * 
	 * @param s the string to transform
	 * @param symbol the replacement char
	 * @return all character of s that do not belongs to: 
	 * SPACE!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~ 
	 * are replaced by symbol
	 * @example "�l�phant",'?' => "?l?phant"
	 */
	public static String spec2Symbol(String s, char symbol) 
    {
    	char[] chars = s.toCharArray();
    	
    	for (int i=0 ; i<chars.length ; i++)
    	{
    		int val = chars[i];
    		if (!(32<=val & val<=126))
    			chars[i] = symbol;
    	}
    	return ca2s(chars);
    }
    
	/**
	 * @param chars
	 * @return a string that represents all char concatened [ca_0 .. ca_n-1] where n = size of ca
	 * ['e','v','e'] => "eve"
	 */
    public static String ca2s(char[] ca)
    {
    	String result = "";
    	for (int i=0 ; i<ca.length ; i++)
    		result += ca[i];
    	return result;
    }
    
	public static boolean isEmpty(String s)
	{
		return s==null || s.trim().equals("null") || s.trim().equals("%null") || s.trim().equals("''") || s.trim().length()==0;
	}
	
	/**
	 * case non sensitive.
	 * @return true if x is equal to value else false
	 */
	public static boolean isX(String value, String x)
	{
		return (isEmpty(value) ? "" : value).trim().toLowerCase().equals((isEmpty(x) ? "" : x).trim().toLowerCase());
	}
	
	/**
	 * case sensitive.
	 * @return true if x is equal to value else false
	 */
	public static boolean isXC(String value, String x)
	{
		return (isEmpty(value) ? "" : value).trim().equals((isEmpty(x) ? "" : x).trim());
	}

	/**
	 * case non sensitive.
	 * @return true if x is a substring of value else false
	 */
	public static boolean hasX(String value, String x)
	{
		return (isEmpty(value) ? "" : value).trim().toLowerCase().contains((isEmpty(x) ? "" : x).trim().toLowerCase());
	}
	
	/**
	 * case sensitive.
	 * @return true if x is a substring of value else false
	 */
	public static boolean hasXC(String value, String x)
	{
		return (isEmpty(value) ? "" : value).trim().contains((isEmpty(x) ? "" : x).trim());
	}
	
	public static boolean startsWith(String value, String x)
	{
		return (isEmpty(value) ? "" : value).trim().toLowerCase().startsWith((isEmpty(x) ? "" : x).trim().toLowerCase());
	}
	
	public static boolean endsWith(String value, String x)
	{
		return (isEmpty(value) ? "" : value).trim().toLowerCase().endsWith((isEmpty(x) ? "" : x).trim().toLowerCase());
	}
	
	public static boolean isAllX(String[] tests, String val)
	{
		for (int i=0; i<tests.length; i++)
		{
			if (!isX(tests[i],val)) return false;
		}
		return true;
	}
	
	
	

	public static String wrap(String value, String wrapper)
	{
		return wrap(value, wrapper, wrapper);
	}
	
	/**
	 * @param value
	 * @param wrapper
	 * @return the value headed with headWrapper and tailed by tailWrapper
	 * if value starts with headWrapper then result will not be headed with headWrapper
	 * and/or value ends with tailWrapper then result will not be tailed with tailWrapper
	 */
	public static String wrap(String value, String headWrapper, String tailWrapper)
	{
		if (isEmpty(value))
			return headWrapper + tailWrapper;
		else 
			return head(tail(value, tailWrapper), headWrapper);
	}
	
	
	/**
	 * 
	 * @param value the chain to complete with symbol
	 * @param symbol to add at the beginning of the value chain
	 */
	public static String head(String value, String symbol)
	{
		if (!isEmpty(value) && !value.startsWith(symbol))
			value = symbol + value;
		return value;
	}
	
	/**
	 * 
	 * @param value the chain to complete with symbol
	 * @param symbol to add at the end of the value chain
	 * @param size the least size of chain value
	 * @return the chain value augmented of symbol such as new length of value >= size
	 * @example "hello","*",10 => "hello*****"
	 */
	public static String tail(String value, String symbol)
	{
		if (!isEmpty(value) && !value.endsWith(symbol))
			value += symbol;
		return value;
	}
	
	/**
	 * 
	 * @param value the chain to be trim at beginning
	 * @param pattern the substring to trim off 
	 * @return a chain that represent value where all leading pattern have been trimmed off
	 * @example "''hello world''","'" => "'hello world''"
	 */
	public static String unHead(String value, String pattern)
	{
		if (value.startsWith(pattern)) 
			return value.replaceFirst(pattern, "");
		return value;
	}
	
	/**
	 * 
	 * @param value the chain to be trim at end
	 * @param pattern the substring to trim off 
	 * @return a chain that represent value where all trailing pattern have been trimmed off
	 * @example "''hello world''","'" => "''hello world'"
	 */
	public static String unTail(String value, String pattern)
	{
		if (value.endsWith(pattern)) 
			return value.substring(0, value.lastIndexOf(pattern));
		return value;
	}
	
	/**
	 * 
	 * @param value the chain to be trim
	 * @param pattern the substring to trim off 
	 * @return a chain that represent value where all heading and trailing pattern have been trimmed off
	 * @example "''hello world''","''" => "hello world"
	 */
	public static String trim(String value, String pattern)
	{
		while(value.startsWith(pattern)){value = unHead(value, pattern);}
		while(value.endsWith(pattern)){value = unTail(value, pattern);}
		return value;
	}
	
	public static String html2server(String val)
	{
		val = val.replace("&#38;", "&");		
		val = val.replace("&#60;", "<");
		val = val.replace("&#61;", "=");
		val = val.replace("&#62;", ">");
		val = val.replace("&#63;", "?");
		val = val.replace("&#33;", "!");
		val = val.replace("&#34;", "\"");
		return val;
	}
	
	public static String server2html(String val)
	{
		val = val.replace("&", "&#38;");		
		val = val.replace("<", "&#60;");
		val = val.replace("=", "&#61;");
		val = val.replace(">", "&#62;");
		val = val.replace("?", "&#63;");
		val = val.replace("!", "&#33;");
		val = val.replace("\"", "&#34;");
		/*
		val = val.replace("#", "&#35;");
		val = val.replace("$", "&#36;");
		val = val.replace("%", "&#37;");
		val = val.replace("'", "&#39;");
		val = val.replace("(", "&#40;");
		val = val.replace(")", "&#41;");
		val = val.replace("*", "&#42;");
		val = val.replace("+", "&#43;");
		val = val.replace(",", "&#44;");
		val = val.replace("-", "&#45;");
		val = val.replace(".", "&#46;");
		val = val.replace("/", "&#47;");
		val = val.replace(":", "&#58;");
		val = val.replace(";", "&#59;");
		val = val.replace("@", "&#64;");
		val = val.replace("[", "&#91;");
		val = val.replace("\\", "&#92;");
		val = val.replace("]", "&#93;");
		val = val.replace("^", "&#94;");
		val = val.replace("_", "&#95;");
		val = val.replace("`", "&#96;");
		val = val.replace("{", "&#123;");
		val = val.replace("|", "&#124;");
		val = val.replace("}", "&#125;");
		val = val.replace("~", "&#126;");
		*/
		return val;
	} 
	
	private static String undoPER(String s){return s.replaceAll("_XCode_PER%", "\\%");}
	private static String undoUND(String s){return s.replaceAll("_XCode_UND_", "\\_");}
	private static String undoSLA(String s){return s.replaceAll("_XCode_SLA_", "\\/");}
	private static String undoAND(String s){return s.replaceAll("_XCode_AND_", "\\&");}
	private static String undoQUO(String s){return s.replaceAll("_XCode_QUO_", "\\\"");}
	private static String undoACC(String s){return s.replaceAll("_XCode_ACC_", "\\'");}
	private static String undoCR(String s){return s.replaceAll("_XCode_CR_", "\n");}
	private static String undoSPA(String s){return s.replaceAll("_XCode_SPA_", "\\ ");}
	private static String undoESC(String s){return s.replaceAll("_XCode_ESC_", "\\\\");}
	private static String undoOR(String s){return s.replaceAll("_XCode_OR_", "\\|");}
	private static String undoNEG(String s){return s.replaceAll("_XCode_NEG_", "\\~");}
	private static String undoEQ(String s){return s.replaceAll("_XCode_EQ_", "\\=");}
	private static String undoADD(String s){return s.replaceAll("_XCode_ADD_", "\\+");}
	private static String undoSUB(String s){return s.replaceAll("_XCode_SUB_", "\\-");}
	private static String undoOBX(String s){return s.replaceAll("_XCode_OBX_", "\\[");}
	private static String undoCBX(String s){return s.replaceAll("_XCode_CBX_", "\\]");}
	private static String undoOBR(String s){return s.replaceAll("_XCode_OBR_", "\\{");}
	private static String undoCBR(String s){return s.replaceAll("_XCode_CBR_", "\\}");}
	private static String undoLT(String s){return s.replaceAll("_XCode_LT_", "\\<");}
	private static String undoGT(String s){return s.replaceAll("_XCode_GT_", "\\>");}
	private static String undoOP(String s){return s.replaceAll("_XCode_OP_", "\\(");}
	private static String undoCP(String s){return s.replaceAll("_XCode_CP_", "\\)");}
	private static String undoTIM(String s){return s.replaceAll("_XCode_TIM_", "\\*");}
	private static String undoDD(String s){return s.replaceAll("_XCode_DD_", "\\:");}
	private static String undoDOT(String s){return s.replaceAll("_XCode_DOT_", "\\.");}
	private static String undoCOL(String s){return s.replaceAll("_XCode_COL_", "\\,");}
	private static String undoSEC(String s){return s.replaceAll("_XCode_SEC_", "\\;");}
	private static String undoINT(String s){return s.replaceAll("_XCode_INT_", "\\?");}
	private static String undoEXC(String s){return s.replaceAll("_XCode_EXC_", "\\!");}
	private static String undoARO(String s){return s.replaceAll("_XCode_ARO_", "\\@");}
	private static String undoLAC(String s){return s.replaceAll("_XCode_LAC_", "\\�");}
	private static String undoRAC(String s){return s.replaceAll("_XCode_RAC_", "\\`");}
	private static String undoCIR(String s){return s.replaceAll("_XCode_CIR_", "\\^");}
	private static String undoEYE(String s){return s.replaceAll("_XCode_EYE_", "\\�");}
	private static String undoDEG(String s){return s.replaceAll("_XCode_DEG_", "\\�");}
	private static String undoPD(String s){return s.replaceAll("_XCode_PD_", "\\�");}
	private static String undoDOL(String s){return s.replaceAll("_XCode_DOL_", "\\$");}
	private static String undoSQ(String s){return s.replaceAll("_XCode_SQ_", "\\�");}
	private static String undoCUB(String s){return s.replaceAll("_XCode_CUB_", "\\�");}
	private static String undoEUR(String s){return s.replaceAll("_XCode_EUR_", "\\�");}
	private static String undoEA(String s){return s.replaceAll("_XCode_EA_", "\\�");}
	private static String undoEG(String s){return s.replaceAll("_XCode_EG_", "\\�");}
	private static String undoDS(String s){return s.replaceAll("_XCode_DS_", "\\�");}
	private static String undoCED(String s){return s.replaceAll("_XCode_CED_", "\\�");}
	private static String undoAG(String s){return s.replaceAll("_XCode_AG_", "\\�");}
	private static String undoUG(String s){return s.replaceAll("_XCode_UG_", "\\�");}
	private static String undoMU(String s){return s.replaceAll("_XCode_MU_", "\\�");}
	private static String undoSHA(String s){return s.replaceAll("_XCode_SHA_", "\\#");}
	private static String undoTD(String s){return s.replaceAll("_XCode_TD_", "\\�");}
	
	
	private static String doPER(String s){return s.replaceAll("\\%","_XCode_PER_");}
	private static String doUND(String s){return s.replaceAll("\\_","_XCode_UND_");}
	private static String doSLA(String s){return s.replaceAll("\\/","_XCode_SLA_");}
	private static String doAND(String s){return s.replaceAll("\\&","_XCode_AND_");}
	private static String doQUO(String s){return s.replaceAll("\\\"","_XCode_QUO_");}
	private static String doACC(String s){return s.replaceAll("\\'","_XCode_ACC_");}
	private static String doCR(String s){return s.replaceAll("\\n","_XCode_CR_");}
	private static String doSPA(String s){return s.replaceAll("\\ ","_XCode_SPA_");}
	private static String doESC(String s){return s.replaceAll("\\\\","_XCode_ESC_");}
	private static String doOR(String s){return s.replaceAll("\\|","_XCode_OR_");}
	private static String doNEG(String s){return s.replaceAll("\\~","_XCode_NEG_");}
	private static String doEQ(String s){return s.replaceAll("\\=","_XCode_EQ_");}
	private static String doADD(String s){return s.replaceAll("\\+","_XCode_ADD_");}
	private static String doSUB(String s){return s.replaceAll("\\-","_XCode_SUB_");}
	private static String doOBX(String s){return s.replaceAll("\\[","_XCode_OBX_");}
	private static String doCBX(String s){return s.replaceAll("\\]","_XCode_CBX_");}
	private static String doOBR(String s){return s.replaceAll("\\{","_XCode_OBR_");}
	private static String doCBR(String s){return s.replaceAll("\\}","_XCode_CBR_");}
	private static String doLT(String s){return s.replaceAll("\\<","_XCode_LT_");}
	private static String doGT(String s){return s.replaceAll("\\>","_XCode_GT_");}
	private static String doOP(String s){return s.replaceAll("\\(","_XCode_OP_");}
	private static String doCP(String s){return s.replaceAll("\\)","_XCode_CP_");}
	private static String doTIM(String s){return s.replaceAll("\\*","_XCode_TIM_");}
	private static String doDD(String s){return s.replaceAll("\\:","_XCode_DD_");}
	private static String doDOT(String s){return s.replaceAll("\\.","_XCode_DOT_");}
	private static String doCOL(String s){return s.replaceAll("\\,","_XCode_COL_");}
	private static String doSEC(String s){return s.replaceAll("\\;","_XCode_SEC_");}
	private static String doINT(String s){return s.replaceAll("\\?","_XCode_INT_");}
	private static String doEXC(String s){return s.replaceAll("\\!","_XCode_EXC_");}
	private static String doARO(String s){return s.replaceAll("\\@","_XCode_ARO_");}
	private static String doLAC(String s){return s.replaceAll("\\�","_XCode_LAC_");}
	private static String doRAC(String s){return s.replaceAll("\\`","_XCode_RAC_");}
	private static String doCIR(String s){return s.replaceAll("\\^","_XCode_CIR_");}
	private static String doEYE(String s){return s.replaceAll("\\�","_XCode_EYE_");}
	private static String doDEG(String s){return s.replaceAll("\\�","_XCode_DEG_");}
	private static String doPD(String s){return s.replaceAll("\\�","_XCode_PD_");}
	private static String doDOL(String s){return s.replaceAll("\\$","_XCode_DOL_");}
	private static String doSQ(String s){return s.replaceAll("\\�","_XCode_SQ_");}
	private static String doCUB(String s){return s.replaceAll("\\�","_XCode_CUB_");}
	private static String doEUR(String s){return s.replaceAll("\\�","_XCode_EUR_");}
	private static String doEA(String s){return s.replaceAll("\\�","_XCode_EA_");}
	private static String doEG(String s){return s.replaceAll("\\�","_XCode_EG_");}
	private static String doDS(String s){return s.replaceAll("\\�","_XCode_DS_");}
	private static String doCED(String s){return s.replaceAll("\\�","_XCode_CED_");}
	private static String doAG(String s){return s.replaceAll("\\�","_XCode_AG_");}
	private static String doUG(String s){return s.replaceAll("\\�","_XCode_UG_");}
	private static String doMU(String s){return s.replaceAll("\\�","_XCode_MU_");}
	private static String doSHA(String s){return s.replaceAll("\\#","_XCode_SHA_");}
	private static String doTD(String s){return s.replaceAll("\\�", "_XCode_TD_");}
	
	public static String undoFilter(String value)
	{
		value = undoSLA(value);
		value = undoAND(value);
		value = undoQUO(value);
		value = undoACC(value);
		value = undoCR(value);
		value = undoSPA(value);
		value = undoESC(value);
		value = undoOR(value);
		value = undoNEG(value);
		value = undoEQ(value);
		value = undoADD(value);
		value = undoSUB(value);
		value = undoOBX(value);
		value = undoCBX(value);
		value = undoOBR(value);
		value = undoCBR(value);
		value = undoLT(value);
		value = undoGT(value);
		value = undoOP(value);
		value = undoCP(value);
		value = undoTIM(value);
		value = undoDD(value);
		value = undoDOT(value);
		value = undoCOL(value);
		value = undoSEC(value);
		value = undoINT(value);
		value = undoEXC(value);
		value = undoARO(value);
		value = undoLAC(value);
		value = undoRAC(value);
		value = undoCIR(value);
		value = undoEYE(value);
		value = undoDEG(value);
		value = undoPD(value);
		value = undoDOL(value);
		value = undoSQ(value);
		value = undoCUB(value);
		value = undoEUR(value);
		value = undoEA(value);
		value = undoEG(value);
		value = undoDS(value);
		value = undoCED(value);
		value = undoAG(value);
		value = undoUG(value);
		value = undoMU(value);
		value = undoSHA(value);
		value = undoTD(value);
		value = undoUND(value);
		value = undoPER(value); // last change !!!
		
		return value;
	}
	
	public static String doFilter(String value)
	{
		value = doPER(value); // first change !!!
		value = doUND(value); 
		value = doSLA(value);
		value = doAND(value);
		value = doQUO(value);
		value = doACC(value);
		value = doCR(value);
		value = doSPA(value);
		value = doESC(value);
		value = doOR(value);
		value = doNEG(value);
		value = doEQ(value);
		value = doADD(value);
		value = doSUB(value);
		value = doOBX(value);
		value = doCBX(value);
		value = doOBR(value);
		value = doCBR(value);
		value = doLT(value);
		value = doGT(value);
		value = doOP(value);
		value = doCP(value);
		value = doTIM(value);
		value = doDD(value);
		value = doDOT(value);
		value = doCOL(value);
		value = doSEC(value);
		value = doINT(value);
		value = doEXC(value);
		value = doARO(value);
		value = doLAC(value);
		value = doRAC(value);
		value = doCIR(value);
		value = doEYE(value);
		value = doDEG(value);
		value = doPD(value);
		value = doDOL(value);
		value = doSQ(value);
		value = doCUB(value);
		value = doEUR(value);
		value = doEA(value);
		value = doEG(value);
		value = doDS(value);
		value = doCED(value);
		value = doAG(value);
		value = doUG(value);
		value = doMU(value);
		value = doSHA(value);
		value = doTD(value);
		return value;
	}
	
	public static String removeBaskSlash(String s)
	{
		return s.replace("\\", "");
	}
	
	public static String removeAccent(String s)
	{
		return removeMajAccent(removeMinAccent(s));
	}
	
	public static String removeMajAccent(String s)
	{
		s = s.replace("�", "A");
		s = s.replace("�", "A");
		s = s.replace("�", "A");
		s = s.replace("�", "A");
		s = s.replace("�", "A");
		s = s.replace("�", "E");
		s = s.replace("�", "E");
		s = s.replace("�", "E");
		s = s.replace("�", "E");
		s = s.replace("�", "I");
		s = s.replace("�", "I");
		s = s.replace("�", "I");
		s = s.replace("�", "I");
		s = s.replace("�", "O");
		s = s.replace("�", "O");
		s = s.replace("�", "O");
		s = s.replace("�", "O");
		s = s.replace("�", "O");
		s = s.replace("�", "U");
		s = s.replace("�", "U");
		s = s.replace("�", "U");
		s = s.replace("�", "U");
		
		return s;
	}
	
	public static String removeMinAccent(String s)
	{
		s = s.replace("�", "a");
		s = s.replace("�", "a");
		s = s.replace("�", "a");
		s = s.replace("�", "a");
		s = s.replace("�", "a");
		s = s.replace("�", "e");
		s = s.replace("�", "e");
		s = s.replace("�", "e");
		s = s.replace("�", "e");
		s = s.replace("�", "i");
		s = s.replace("�", "i");
		s = s.replace("�", "i");
		s = s.replace("�", "i");
		s = s.replace("�", "o");
		s = s.replace("�", "o");
		s = s.replace("�", "o");
		s = s.replace("�", "o");
		s = s.replace("�", "o");
		s = s.replace("�", "u");
		s = s.replace("�", "u");
		s = s.replace("�", "u");
		s = s.replace("�", "u");
		
		return s;
	}
	
	public static String string2ByteString(String content)
	{
		String byteString = "";
		for (byte b : content.getBytes())
			byteString += isEmpty(byteString) ? b : "." + b;
		return byteString;
	}
	
	public static String byteString2String(String byteString)
	{
		String content = "";
		for(String b : byteString.split("\\."))
		{
			content +=  ((char) Util.getInteger(b));
		}
		return content;
	}
	
	public static void main(String[] args)
	{
		UtilLog.out(UtilStr.byteString2String(UtilStr.string2ByteString("Halleluyah bordel !!!")));
	}
	
}
