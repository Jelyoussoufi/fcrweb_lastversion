package be.celsius.util;

/**
 * 
 * @author jhuilian
 *
 * @category Constants
 * @category Functionnal
 */
public class Config {

	/**
	 * global modules constants.
	 */

	public static final String XML_CONTENTTYPE 		= "text/xml";
	public static final String XML_HEAD 			= "<?xml version='1.0' encoding='ISO-8859-1'?>";
	public static final String responseContentType 	= "text/xml;charset=UTF-8";
	public static final String HTML 				= "text/html;charset=UTF-8";
	public static final String TEXT 				= "text;charset=UTF-8";
		
	/***************
	 * Environment *
	 ***************/
	
	/* *
	public static final boolean atCelsius 		= false;
	public static final boolean testExpiration 	= false;
	
	/* */
	public static final boolean atCelsius 		= false;
	public static final boolean testExpiration 	= false;
	/* */
	
	public static final boolean verb	 		= atCelsius;	
	public static final boolean info			= verb;//localTesting;
	public static final boolean debug			= verb;//localTesting;
	public static final boolean dbverb 			= verb;//localTesting;
	public static final boolean profilerverb	= false;//localTesting;
	
	public static final int env_atCelsius 		= 1;
	public static final int env_onAcceptance	= 2;

	public static final String  DB_FCRWEB_JNDI	= "fcrweb";
	public static final String  DB_FWRULES_JNDI	= "fwrules";
	
	public static final String 	SECRETKEY				= "FCR_WEB_2013";		//do not modify this value
	public static final int PROFILER_DEF_PROF_SIZE		= 2;	// min size;
	
	public static final int TIMER_DELAY_TYPE_LCL	= 0;
	public static final int TIMER_DELAY_VALUE_LCL	= 30;
}
