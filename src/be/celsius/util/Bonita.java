package be.celsius.util;

/**
 * Copyright (C) 2011  BonitaSoft S.A.
 * BonitaSoft, 31 rue Gustave Eiffel - 38000 Grenoble
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation
 * version 2.1 of the License.
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 **/


import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.security.auth.login.LoginContext;

import org.ow2.bonita.facade.ManagementAPI;
import org.ow2.bonita.facade.QueryDefinitionAPI;
import org.ow2.bonita.facade.QueryRuntimeAPI;
import org.ow2.bonita.facade.RuntimeAPI;
import org.ow2.bonita.facade.def.element.BusinessArchive;
import org.ow2.bonita.facade.def.majorElement.ProcessDefinition;
import org.ow2.bonita.facade.runtime.ActivityInstance;
import org.ow2.bonita.facade.runtime.ActivityState;
import org.ow2.bonita.facade.runtime.ProcessInstance;
import org.ow2.bonita.facade.runtime.TaskInstance;
import org.ow2.bonita.facade.uuid.ProcessDefinitionUUID;
import org.ow2.bonita.facade.uuid.ProcessInstanceUUID;
import org.ow2.bonita.light.LightProcessInstance;
import org.ow2.bonita.light.LightTaskInstance;
import org.ow2.bonita.util.AccessorUtil;
import org.ow2.bonita.util.BonitaConstants;
import org.ow2.bonita.util.BusinessArchiveFactory;
import org.ow2.bonita.util.ProcessBuilder;
import org.ow2.bonita.util.SimpleCallbackHandler;

/**
 * @author Lian Jian hui.
 *
 */
enum TestCase { INC, ADD, AIS, AIAS };

public class Bonita{
 
  private final String LOGIN 	= "sshgateway";
  private final String PSSWD 	= "cyberark";
  private final String pathPref	= "src/bonita/";
  private final String jaasFile = pathPref + "jaas-standard.cfg";
  
  private LoginContext loginContext;
  private ProcessDefinition process;
  private Map<String,ProcessDefinition> deployedProcesses; 
  
  private final String nl = "\n";
  
  
  public Bonita()
  {
	  deployedProcesses = new HashMap<String,ProcessDefinition>();
  }
  
  /**
   * 
   * @param processUUIDName e.g: myProcess
   * @return
   * @throws Exception
   */
  public int execAddResourceInSafe(String resourceId, String safeId, TestCase tc) throws Exception 
  {
	  
	  ProcessInstanceUUID instanceUUID 			= instantiateProcess("AddResourceInSafe--1.0");
	  //Collection<LightTaskInstance> taskList 	= getQueryRuntimeAPI().getLightTaskList(instanceUUID, ActivityState.READY);
      Set<ProcessInstanceUUID> childProcessUUIDs= getQueryRuntimeAPI().getChildrenInstanceUUIDsOfProcessInstance(instanceUUID);
      /* *
      UtilLog.out(instanceUUID + " child processUUIDs:");
      for (ProcessInstanceUUID childProcessUUID : childProcessUUIDs)
      {
    	  UtilLog.out(childProcessUUID);
      }
      /* */
      if (childProcessUUIDs.size()==1)
      {
    	  ProcessInstanceUUID childProcessUUID 		= childProcessUUIDs.iterator().next();
    	  Collection<LightTaskInstance> taskList 	= getQueryRuntimeAPI().getLightTaskList(childProcessUUID, ActivityState.READY);
    	  LightTaskInstance taskInstance 			= taskList.iterator().next();
 
          getRuntimeAPI().setProcessInstanceVariable(taskInstance.getProcessInstanceUUID(), "resourceId", resourceId);
    	  getRuntimeAPI().setProcessInstanceVariable(taskInstance.getProcessInstanceUUID(), "safeId", safeId);
          getRuntimeAPI().executeTask(taskInstance.getUUID(), true);
          
          while (!getQueryRuntimeAPI().getProcessInstance(instanceUUID).isArchived())
          {
        	  wait(1);
        	  //UtilLog.out("wait until archived");
          }
          
          //String result = getQueryRuntimeAPI().getProcessInstance(instanceUUID).getLastKnownVariableValues().get("errorMsg").toString();
          //UtilLog.out("resourceId:" + resourceId + " - safeId:" + safeId + " - msg:" + result);
          //UtilLog.out("current activity:" + getQueryRuntimeAPI().getTask(taskInstance.getUUID()).toString());
          //UtilLog.out("current process:" + getQueryRuntimeAPI().getProcessInstance(instanceUUID).toString());
          //LightProcessInstance lpi = getQueryRuntimeAPI().getLightProcessInstance(instanceUUID);
          /*
          Map<String,Object> elements =  getQueryRuntimeAPI().getProcessInstance(instanceUUID).getLastKnownVariableValues();
          Map<String,Object> processInstanceVariables = getQueryRuntimeAPI().getProcessInstanceVariables(instanceUUID);
          UtilLog.out(instanceUUID + " parameters:");
          for (String key : elements.keySet())
          {
        	  String last = elements.get(key)+"";
        	  String curr = getQueryRuntimeAPI().getProcessInstanceVariable(instanceUUID, key)+"";
        	  UtilLog.out(key + ":" + curr + ":" + last);
          }
          */
          String errMsg 	= getQueryRuntimeAPI().getProcessInstanceVariable(instanceUUID, "errorMsg").toString();
          boolean isTestOk 	= false;
          switch (tc)
          {
          	case INC : // inconsistency
          		if (errMsg.contains("resource safe consistency break detected"))
          			isTestOk = true;
          		break;
          	case ADD : // add in safe
          		if (errMsg.contains("have been added into safe"))
          			isTestOk = true;
          		break;
          	case AIS : // add in safe
          		if (errMsg.contains("the resource is already in the safe"))
          			isTestOk = true;
          		break;
          	case AIAS : // already in another safe
          		if (errMsg.contains("the resource is in another safe"))
          			isTestOk = true;
          		break;
          }
          
          UtilLog.out("resourceId: " + resourceId + " - safeId:" + safeId + " TEST OK? " + isTestOk + " - " + errMsg.replaceAll("\n", ","));
      }
      
	  return 0;
  }
  
  	public int initDeployedProcesses() throws Exception 
	{
		String procs = "";

		QueryDefinitionAPI queryDefinitionAPI = AccessorUtil.getQueryDefinitionAPI();

		Set<ProcessDefinition> setProcess = queryDefinitionAPI.getProcesses();

		procs += "List of deployed processes : " + nl;

		for (ProcessDefinition process : setProcess)
		{	
			deployedProcesses.put(process.getUUID().toString(), process);
			procs +=process.getUUID() + nl;
		}
		
		UtilLog.out(procs);
		return 0;
	}
  	
  	
  
  /**
   * @param barFile the bar file which contains the process to be deployed. (e.g: example_1.0)
   * @pre the bar file should putted in the directory: src/main/resources/
   * @pre the bar file has the following extension: .bar
   */
  public int deployBarFileProcess(String barFileName) throws Exception
  {
	  final File barFile = new File(pathPref + barFileName + ".bar");
      final BusinessArchive businessArchive = BusinessArchiveFactory.getBusinessArchive(barFile);
      
      process = getManagementAPI().deploy(businessArchive);
      ProcessDefinitionUUID processUUID = process.getUUID();
      
      UtilLog.out("processUUID:" + processUUID);
      return 0;
  }
  
  public int deployInBuildProcess() throws Exception 
  {
      //create a simple process with process builder:
      // - one step with LOGIN as actor
      // - a Global data of String Type
      process = ProcessBuilder.createProcess("myProcess", "1.1")
        .addStringData("userLogin", "dcarels")
        .addHuman(LOGIN)
        .addHumanTask("step1", LOGIN)
        .addStringData("step1Var", "var1")
        .addHumanTask("step2", LOGIN)
        .addStringData("step2Var", "var2")
        .done();
     
      //deploy process
      process = getManagementAPI().deploy(BusinessArchiveFactory.getBusinessArchive(process));
      UtilLog.out("---------------- Process deployed ----------------");

      return 0;
  }
  
  private ProcessInstanceUUID instantiateProcess(String processUUIDName) throws Exception 
  {
	  process = deployedProcesses.get(processUUIDName);
      ProcessInstanceUUID instanceUUID = getRuntimeAPI().instantiateProcess(process.getUUID());
      UtilLog.out("---------------- New process instance Created:" + instanceUUID + " ----------------");
      
      return instanceUUID;
  }
  
  /**
   * @param processUUIDName eg:DumbProcess
   */
  public int execBarFileProcessInstance(String processUUIDName,String name) throws Exception 
  {
	  ProcessInstanceUUID instanceUUID = instantiateProcess(processUUIDName);
     
      final Collection<LightTaskInstance> taskList = getQueryRuntimeAPI().getLightTaskList(instanceUUID, ActivityState.READY);
      
      /* 
      if (taskList.size() != 1)
      {
        throw new Exception("Incorrect list size. Actual size: " + taskList.size());
      }
      /* */
      final LightTaskInstance taskInstance = taskList.iterator().next();
      
      //getRuntimeAPI().setActivityInstanceVariable(taskInstance.getUUID(), "", "");
      //getRuntimeAPI().startTask(taskInstance.getUUID(), true);
      
      getRuntimeAPI().setProcessInstanceVariable(taskInstance.getProcessInstanceUUID(), "name", name);
      getRuntimeAPI().executeTask(taskInstance.getUUID(), true);
      UtilLog.out("current activity:" + getQueryRuntimeAPI().getTask(taskInstance.getUUID()).toString());
      //getRuntimeAPI().setProcessInstanceVariable(taskInstance.getProcessInstanceUUID(), "name", "jhuilian");
      //UtilLog.out("current activity:" + getQueryRuntimeAPI().getTask(taskInstance.getUUID()).toString());
      
      UtilLog.out("----------------Application executed sucessfully ----------------");
      return 0;
  }
  
  /**
   * 
   * @param processUUIDName e.g: myProcess
   * @return
   * @throws Exception
   */
  public int execInBuildProcessInstance(String processUUIDName) throws Exception 
  {
	  ProcessInstanceUUID instanceUUID 			= instantiateProcess(processUUIDName);
	  Collection<LightTaskInstance> taskList 	= getQueryRuntimeAPI().getLightTaskList(instanceUUID, ActivityState.READY);
      LightTaskInstance taskInstance = taskList.iterator().next();
      
	  getRuntimeAPI().setProcessInstanceVariable(taskInstance.getProcessInstanceUUID(), "globalVar", "none");
      getRuntimeAPI().executeTask(taskInstance.getUUID(), true);
      UtilLog.out("current activity:" + getQueryRuntimeAPI().getTask(taskInstance.getUUID()).toString());
	  
	  return 0;
  }
  
  	public int showTasks() throws Exception 
  	{
		showTasks("READY", ActivityState.READY);
		showTasks("EXECUTING", ActivityState.EXECUTING);
		showTasks("FINISHED", ActivityState.FINISHED);
		showTasks("ABORTED", ActivityState.ABORTED);
		showTasks("CANCELLED", ActivityState.CANCELLED);
		showTasks("SUSPENDED", ActivityState.SUSPENDED);
		showTasks("FAILED", ActivityState.FAILED);
		showTasks("SKIPPED", ActivityState.SKIPPED);
		
		return 0;
	}
  	
  	private int showTasks(String activityStateName, ActivityState state)
  	{
  		String whatAction;
		ActivityInstance currentActivity;
		Collection<TaskInstance> tasks 		= getQueryRuntimeAPI().getTaskList(state);
		Iterator<TaskInstance> myIterator 	= tasks.iterator();
		
  		UtilLog.out("***** " + activityStateName + " tasks: *****");
  		
		while (myIterator.hasNext())
		{
			currentActivity = myIterator.next();
			String taskInfos = "";
			taskInfos += "*** currentActivity uuid:" + currentActivity.getUUID() + nl; 
			taskInfos += " - currentActivity:" + currentActivity.toString() + nl;
			UtilLog.out(taskInfos);
			
			/*
			taskUUID 		= currentActivity.getUUID();
			getRuntimeAPI().startTask(taskUUID, true);

			whatAction 		= (String) getQueryRuntimeAPI().getVariable(currentActivity.getUUID(), "Action");

			if (whatAction.compareTo("SUSPEND") == 0)
			{
				getRuntimeAPI().suspendTask(taskUUID, true);
			} else {
				getRuntimeAPI().setVariable(currentActivity.getUUID(), "Action", "SUSPEND");
				getRuntimeAPI().finishTask(taskUUID, true);
			}
			*/
		}
		
  		return 0;
  	}
  	
  	public int showProcessInstances()
  	{
  		Set<ProcessInstance> pis 	= getQueryRuntimeAPI().getUserInstances();
  		
		UtilLog.out("***** " + LOGIN + " process instances: *****");
		
		for (LightProcessInstance lpi : pis)
			UtilLog.out(" * " + lpi.toString());
		
		UtilLog.out("");
		
  		return 0;
  	}
  	
  	public int showLightProcessInstances() throws Exception
  	{
  		Set<LightProcessInstance> lpis 	= getQueryRuntimeAPI().getLightUserInstances();
  		
		UtilLog.out("***** " + LOGIN + " LIGHT process instances: *****");
		
		for (LightProcessInstance lpi : lpis)
		{	
			UtilLog.out(" * " + lpi.toString());
			
			Map<String,Object> processInstanceVariables = getQueryRuntimeAPI().getProcessInstanceVariables(lpi.getUUID());
			for (String key : processInstanceVariables.keySet())
				UtilLog.out("   - " + key + ":" + processInstanceVariables.get(key));
		}
		
		UtilLog.out("");
		
  		return 0;
  	}
  	
  	
  	public int deleteAllProcesses() throws Exception 
  	{
  		getManagementAPI().deleteAllProcesses();
  		UtilLog.out("-------------------- all processes have been deleted --------------------");
  		return 0;
  	}
  	
  	public void testInstanceVarUpdate() throws Exception 
  	{
  		ProcessDefinition process = ProcessBuilder.createProcess("instanceVar", "1.0")
	    .addHuman(LOGIN)
	    .addStringData("var", "initial")
	    .addSystemTask("init")
	    .addHumanTask("human", LOGIN)
	    .addTransition("init", "human")
	    .done();

  		process = getManagementAPI().deploy(BusinessArchiveFactory.getBusinessArchive(process));
  	    //process = getManagementAPI().deploy(BusinessArchiveFactory.getBusinessArchive(process, null, SetVarConnector.class));
  	    ProcessDefinitionUUID processUUID = process.getUUID();
  	    
  	    ProcessInstanceUUID instanceUUID = getRuntimeAPI().instantiateProcess(processUUID);
  	    
  	    ProcessInstance instance = getQueryRuntimeAPI().getProcessInstance(instanceUUID);
  	    
  	    //check initial value
  	    UtilLog.out(getQueryRuntimeAPI().getProcessInstanceVariable(instanceUUID, "var").toString());
  	   	UtilLog.out(instance.getLastKnownVariableValues().get("var").toString());
  	 	UtilLog.out(instance.getInitialVariableValue("var").toString());
  	    //check no var update
  		UtilLog.out((Collections.emptyList().size() == instance.getVariableUpdates().size()) + "");
  	    
  	    final long readyTime = System.currentTimeMillis();
  	    
  	    //wait to check last update date...
  	    Thread.sleep(2000);
  	    
  	    getRuntimeAPI().setProcessInstanceVariable(instanceUUID, "var", "newvalue");
  	    
  	    instance = getQueryRuntimeAPI().getProcessInstance(instanceUUID);
  	    
  	    //check initial value
  	    UtilLog.out(getQueryRuntimeAPI().getProcessInstanceVariable(instanceUUID, "var").toString());
  	    UtilLog.out(instance.getLastKnownVariableValues().get("var").toString());
  	    UtilLog.out(instance.getInitialVariableValue("var").toString());
  	    //check no var update
  	    UtilLog.out((Collections.emptyList().size() == instance.getVariableUpdates().size()) + "");
  	    
  	    //check last update date
  	    UtilLog.out("update:" + instance.getLastUpdate().after(new Date(readyTime)));
  	    
  	    getManagementAPI().deleteProcess(processUUID);
  	  }
  	
    public ManagementAPI getManagementAPI(){return AccessorUtil.getManagementAPI();}
    public RuntimeAPI getRuntimeAPI(){return AccessorUtil.getRuntimeAPI();}
    public QueryRuntimeAPI getQueryRuntimeAPI(){return AccessorUtil.getQueryRuntimeAPI();}
    
    public void login() throws Exception
    {
    
      //set system properties
      System.setProperty(BonitaConstants.API_TYPE_PROPERTY, "REST");
      //System.setProperty(BonitaConstants.REST_SERVER_ADDRESS_PROPERTY, "http://192.168.254.9:8080/bonita-server-rest/");	//@gregory 
    //  System.setProperty(BonitaConstants.REST_SERVER_ADDRESS_PROPERTY, "http://localhost:8081/bonita-server-rest/");	//@localhost
      System.setProperty(BonitaConstants.REST_SERVER_ADDRESS_PROPERTY, "http://mink.prod.mobistar.be:8087/bonita-server-rest/");
      System.setProperty(BonitaConstants.JAAS_PROPERTY, jaasFile);
      System.setProperty (BonitaConstants.HOME, "conf\\bonita\\");
     
      //login
      //verify the user exists
     	loginContext = new LoginContext("BonitaAuth",
          new SimpleCallbackHandler(LOGIN, PSSWD));
      loginContext.login();
      loginContext.logout();

      //propagate the user credentials
      loginContext = new LoginContext("BonitaStore",
          new SimpleCallbackHandler(LOGIN, PSSWD));
      loginContext.login();
    }
    
    public void logout() throws Exception
    {
  	  loginContext.logout();
    }
    
    public void wait(int seconds)
    {
    	try 
    	{
    		Thread.sleep(seconds*1000);
    	}
    	catch (Exception e)
    	{
    		
    	}
    }
}