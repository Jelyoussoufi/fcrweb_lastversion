package be.celsius.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class UtilSoap 
{
	private Map<String,String> logs;
	private boolean useLogs;
	
	public UtilSoap()
	{
		logs 	= new HashMap<String,String>();
		useLogs = false;
	}
	
	public HttpURLConnection submitRequest(String url, String request)
	{
		return submitRequest(url, request, true);
	}
	
	public HttpURLConnection submitRequest(String url, String request, boolean doInput)
	{
		return submitRequest(url, request, doInput, false);
	}
	
	
	public HttpURLConnection submitRequest(String url, String request, boolean doInput, boolean ignoreTrust)
	{
log("url", url);
log("request", request);

		if (ignoreTrust)/** if secured server then you should bypass trust certificate check **/
		{
			//Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[]
			{
				new X509TrustManager() 
			 	{
			     	public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null;}
			     	public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
			     	public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
			    }
			};
			
			//Install the all-trusting trust manager
			try {
			 SSLContext sc = SSLContext.getInstance("SSL");
			 sc.init(null, trustAllCerts, new java.security.SecureRandom());
			 HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			 log("trust_certification","attempt to ignore trust certification");
			} catch (Exception e) {				
				log("trust_certification_exception", UtilLog.getStackTrace(e));
			}
			try {
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String host, SSLSession sess) {return true;}
            });
			log("HostnameVerifier","use custom HostnameVerifier");
			}
			catch(Exception e)
			{
				log("HostnameVerifier","cannot use custom HostnameVerifier");
			}
			
		}
		else
		{
			log("trust_certification","trust certification not handled");
		}
		
		try
		{
			HttpURLConnection httpConn 	= (HttpURLConnection) new URL(url).openConnection();			 
			ByteArrayOutputStream bout 	= new ByteArrayOutputStream();
	  		PrintStream ps 				= new PrintStream( bout ); 
	  		ps.print(request);
	  		ps.close();
			byte[] b = bout.toByteArray();
			bout.close();
			
			httpConn.setRequestProperty("Content-Length", String.valueOf( b.length ) );  
		    httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");  
		    httpConn.setRequestProperty("Cache-Control", "no-cache");
			httpConn.setRequestProperty("Pragma", "no-cache"); 
			httpConn.setRequestMethod( "POST" );  
		    httpConn.setDoOutput(true);  
		    httpConn.setDoInput(doInput);  
		    
		    OutputStream out = httpConn.getOutputStream();  
		    out.write( b );
		    out.close();  
		    
		    return httpConn;
		}
	    catch (Exception e)
	    {
log("submitRequest_exception", UtilLog.getStackTrace(e));
UtilLog.printException(e);
	    }
		return null;
	}
	
	/**  read a soap response from an established connection **/
	public String extractResponse(HttpURLConnection httpConnection){return extractResponse(httpConnection, true);}
	
	/**
	 * @param disconnect the connection will disconnect once response is read.
	 * @warning if !disconnect, you must call httpConn.disconnect(); later in order to shut the connection down.
	 */
	public String extractResponse(HttpURLConnection httpConnection, boolean disconnect)
	{
		try 
		{
			String response = extractResponse(new InputStreamReader(httpConnection.getInputStream()));
			if (disconnect)
				httpConnection.disconnect();
			return response;			
		}
		catch (Exception e)
	    {
log("extractResponse_http_conn_exception", UtilLog.getStackTrace(e));
UtilLog.printException(e);
	    }
		return "";
	}
	
	private String extractResponse(InputStreamReader isr)
	{		
		String response = "";
		try 
		{
			String xmlResponse 		= "";
			String currentLine 		= "";
			BufferedReader br 	= new BufferedReader(isr);
			
			while((currentLine = br.readLine()) != null)
				xmlResponse += currentLine + "\n";
			isr.close();
			br.close();			
			response = xmlResponse;
		}
	    catch (Exception e)
	    {
log("extractResponse_exception", UtilLog.getStackTrace(e));
UtilLog.printException(e);
	    }
log("response", response);
		return response;
	}
	
	/**  read a request from HttpServletRequest **/
	public static String extractRequest(HttpServletRequest request)
	{
		try 
		{
			InputStreamReader isr	= new InputStreamReader(request.getInputStream());
			String xmlResponse 		= "";
			String currentLine 		= "";
			BufferedReader br 		= new BufferedReader(isr);
			
			while((currentLine = br.readLine()) != null)
				xmlResponse += currentLine + "\n";
			isr.close();
			br.close();
			return xmlResponse;
		}
		catch (Exception e)
	    {
	    }
		return "";
	}
	
	public static void reply(HttpServletResponse res, String xmlResponse)
	{
		try
		{
			PrintWriter printer = res.getWriter();
			printer.print(xmlResponse);
			printer.close();
		}
		catch (Exception e)
	    {
	    }
	}
	
	
	public Document getDocument(String xmlResponse)
	{
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(false);
			factory.setValidating(false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			ByteArrayInputStream is = new ByteArrayInputStream(xmlResponse.getBytes("UTF-8"));
			Document document 		= builder.parse(is);
			is.close();
			return document;
		}
		catch (Exception e)
		{
log("getDocument_exception", UtilLog.getStackTrace(e));
UtilLog.printException(e);
		}
		return null;
	}
			
	public String getDomNodeValue(Element root, String xPathExpression)
	{
		try
		{
			Node n = XPathAPI.selectSingleNode(root, xPathExpression);
			return n.getNodeValue().trim();
		}
		catch(Exception e)
		{
log("getDomNodeValue_exception", UtilLog.getStackTrace(e));
UtilLog.printException(e);
		}
		return "";
	}
	
	private Map<String,String> log(String key, String value)
	{
		if (logs!=null && useLogs)
			logs.put(key,value);
		return logs;
	}
	
	public Map setUseLogs(boolean use)
	{
		useLogs = use;
		return logs;
	}
	
	public Map<String,String> clearLogs()
	{
		logs.clear();
		return logs;
	}
	
	public Map<String,String> getLogs()
	{
		return logs;
	}
}
