package be.celsius.util;

import java.util.Calendar;

public class UtilProc 
{

	public static void sleep(int timeMillis)
	{
		try { Thread.sleep(timeMillis); } 
		catch (InterruptedException e){ UtilLog.printException(e); }
	}
	
	public static void wait(int seconds)
	{
		try {Thread.sleep(seconds*1000);}
		catch (InterruptedException e){UtilLog.printException(e);}
	}
	
	public static void elapseTime(int timeToElapse)
	{
		long x = 0;
		long start 	= Calendar.getInstance().getTimeInMillis();
		long end 	= Calendar.getInstance().getTimeInMillis();
		while(x<(timeToElapse*1000))
		{
			x = end-start;
			end = Calendar.getInstance().getTimeInMillis();
		}
		UtilLog.out("get Time elapsed:"+(end - start));
	}
	
	
}
