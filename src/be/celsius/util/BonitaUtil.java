package be.celsius.util;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import be.celsius.util.datastructure.Umap;

public abstract class BonitaUtil
{
	protected String className 			= "Bonita";
	protected String bonitaAuthUser 	= "restuser";	//deprecated
	protected String bonitaAuthPassword = "restbpm";	//deprecated
	protected String bonitaUPB64		= "";
	protected String login				= "";
	protected String bonitaServer		= "http://mink.prod.mobistar.be";//"http://localhost";
	protected int bonitaPort			= 8087;//8081;
	private final static String API	= "/bonita-server-rest/API/";
	
	private Map<String,Object> map;
	
	public BonitaUtil()
	{
		map			= new HashMap<String,Object>();
	}
	
	public String getClassName() 
	{
        return className;
    }

	/**
	 * 
	 * @param uri the operation name with its parameters if needed
	 * @sample runtimeAPI/instantiateProcessWithVariables/ and the processName parameter myProcess--1.0
	 * @return <bonitaServer> : <bonitaPort> / <restAPI> / uri
	 * => http://localhost:8081/bonita-server-rest/API/runtimeAPI/instantiateProcessWithVariables/myProcess--1.0
	 */
	protected String restUrl(String uri)
	{
		return bonitaServer + ":" + bonitaPort + API + uri;
	}

	public String getCurrentDir()
	{
		String dirs = "";
		File dir1 = new File (".");
		File dir2 = new File ("..");
		
		try 
		{
			dirs += "Current dir : " + dir1.getCanonicalPath() + "\n";
			dirs += "Parent  dir : " + dir2.getCanonicalPath() + "\n";
			dirs += "user dir : " + System.getProperty("user.dir") + "\n";
		}
		catch(Exception e) {dirs += getStackTrace(e);}

		return dirs;		
	}
	
	public String getFolderFiles()
	{
		  String path = "."; 
		  String files = "";
		  File folder = new File(path);
		  File[] listOfFiles = folder.listFiles(); 
		 
		  for (int i = 0; i < listOfFiles.length; i++) 
		  {
			  files += listOfFiles[i].getName() + "\n";
			  /*
			   if (listOfFiles[i].isFile()) 
				   files = listOfFiles[i].getName();
			   */
		  }
		  
		  return files;
	}
	
	public boolean isEmpty(String s)
	{
		return s==null || s.trim().equals("null") || s.trim().equals("''") || s.trim().length()==0;
	}
	
	public String getStackTrace(Exception aThrowable) 
	{
		Writer result 			= new StringWriter();
	    PrintWriter printWriter = new PrintWriter(result);

	    aThrowable.printStackTrace(printWriter);
	    
	    return result.toString(); 
	} 
	
	public String getBonitaAuthUser() {
		return bonitaAuthUser;
	}

	public void setBonitaAuthUser(String bonitaAuthUser) {
		this.bonitaAuthUser = bonitaAuthUser;
	}

	public String getBonitaAuthPassword() {
		return bonitaAuthPassword;
	}

	public void setBonitaAuthPassword(String bonitaAuthPassword) {
		this.bonitaAuthPassword = bonitaAuthPassword;
	}

	public String getBonitaUPB64() {
		return bonitaUPB64;
	}

	public void setBonitaUPB64(String bonitaUPB64) {
		this.bonitaUPB64 = bonitaUPB64;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getBonitaServer() {
		return bonitaServer;
	}

	public void setBonitaServer(String bonitaServer) {
		this.bonitaServer = bonitaServer;
	}

	public int getBonitaPort() {
		return bonitaPort;
	}

	public void setBonitaPort(int bonitaPort) {
		this.bonitaPort = bonitaPort;
	}

	public Map<String, Object> getMap() {
		return map;
	}
	
	public Map getConnectionConfiguration()
	{
		return new Umap().setName("config")
			.push("host", bonitaServer).push("port", "" + bonitaPort)
			.push("user", bonitaAuthUser).push("pwd",bonitaAuthPassword)
			.push("UPB64",bonitaUPB64);		
	}
	
	public String getMapS(String key)
	{
		String value = "";
		
		try
		{
			if (map.get(key)==null)
				value = "";
			else
				value = (String) map.get(key);
		}
		catch (Exception e){value = "";}
		
		return value;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}
}
