package be.celsius.fcrweb.beans;

public class FormIdBean 
{
	String fid;
	
	public FormIdBean()
	{
		fid = "";
	}
	
	public void setFid(String fid)
	{
		this.fid = fid;
	}
	
	public String getFid()
	{
		return this.fid;
	}
}
