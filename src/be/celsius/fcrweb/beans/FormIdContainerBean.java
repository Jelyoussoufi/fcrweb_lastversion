package be.celsius.fcrweb.beans;

import java.util.HashMap;
import java.util.Map;

public class FormIdContainerBean 
{
	private Map<String, String> fids;
	
	public FormIdContainerBean()
	{
		fids = new HashMap();
	}

	public Map<String, String> getFids() {
		return fids;
	}
	
	public String getFid(String user) {
		return fids.get(user);
	}
	
	public void removeFid(String user) {
		fids.remove(user);
	}
	
	public void addFid(String user, String fid)
	{
		fids.put(user, fid);
	}

	public void setFids(Map<String, String> fids) {
		this.fids = fids;
	}
	
}
