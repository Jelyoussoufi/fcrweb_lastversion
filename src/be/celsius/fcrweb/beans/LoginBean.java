package be.celsius.fcrweb.beans;

import javax.servlet.http.HttpServletRequest;

import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.utils.AuthenticationUtils;

public class LoginBean 
{
	String login;
	String profiles;
	String technicalServices;
	
	public LoginBean()
	{
		login = "EMPTY";
		profiles = "EMPTY";
		technicalServices = "EMPTY";
	}

	public String getLogin(HttpServletRequest request) 
	{
		if (login.equals("EMPTY"))
		{
			login = AuthenticationUtils.getUserId(request);
		}
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getProfiles(HttpServletRequest request, MySqlAccessor sqlAccessor)
	{
		if (profiles.equals("EMPTY"))
		{
			profiles = AuthenticationUtils.getUserProfiles(login, request);
		}
		return profiles;
	}

	public void setProfiles(String profiles) {
		this.profiles = profiles;
	}
	
	public String getTechnicalServices(MySqlAccessor sqlAccessor)
	{
		if (technicalServices.equals("EMPTY"))
		{
			technicalServices = sqlAccessor.getOwnedTechnicalServices(profiles);
		}
		
		return this.technicalServices;
			
	}
}
