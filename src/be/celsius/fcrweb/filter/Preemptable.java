package be.celsius.fcrweb.filter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;

public abstract class Preemptable
{

	public Preemptable()
	{
	}
	
	protected boolean dispatchRequest(HttpServletRequest request, HttpServletResponse response)
	{
		UtilLog.out("AccessFilter - URI:" + request.getRequestURI());
				
		/**
		 * test whether session has expired or not 
		 */
		
		if(request.getRequestedSessionId() != null && request.isRequestedSessionIdValid() == false) 
		{
			UtilLog.out("Session has expired or is invalid !!!");
            RequestDispatcher rd = request.getRequestDispatcher("index.jsp");           
            request.getSession().setAttribute("userInfo",null);
            
            try{rd.forward(request, response);}
            catch (Exception e){UtilLog.printException(e);} 
            
			return false;
        }
		
		return true;
	}
		
	private String getHeader(HttpServletRequest httpReq, String type)
	{
		String param = httpReq.getHeader(type);
		if(UtilStr.isEmpty(param))return "";
		else return param.trim();
	}
}
