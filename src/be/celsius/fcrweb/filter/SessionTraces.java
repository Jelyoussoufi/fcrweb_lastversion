package be.celsius.fcrweb.filter;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.log4j.Logger;

/* A bean to store the last session traces .This bean is placed 
 * in the session context (see the file 'applicationContext.xml').
 * In other words, each user has his own SessionTraces bean.
 */
public class SessionTraces implements Serializable{

	private static final long serialVersionUID = 1L;				/* an id used for the serialization */
	
	protected final transient Logger logger = Logger.getLogger(getClass());	/* the Log4J logger */
	private ArrayList traces;												/* an UNORDERED list of session traces */
	private int nbrSessionTraces = 6;										/* i.e. only the last 6 session traces will be stored */
	private int first;														/* the index, in the list, of the first session trace (in other words, the oldest one) */
	private int next;														/* the index, in the list, of the next session trace */
	private boolean isListFull;												/* a boolean indicating wether the list is full or not */
	
	public SessionTraces(){
		traces = new ArrayList();
		for(int i=0; i<nbrSessionTraces; i++)
			traces.add(null);	/* mandatory to use the 'set' method on the list in the 'logTrace' method */
		first = 0;
		next = 0;
		isListFull = false;
		logger.debug("The sessionTraces bean has been placed in the session context.");
	}
	
	public int getFirst() {
		return first;
	}
	
	public ArrayList getTraces() {
		return traces;
	}

	public void setTraces(ArrayList traces) {
		this.traces = traces;
	}
	
	public void logTrace(SessionTrace trace) {
		traces.set(next, trace);
		
		next ++;
		if(next == nbrSessionTraces)
		{
			next = 0;
			isListFull = true;
		}
		
		if(isListFull)
			first = next;
	}
	
	public String toString(){
		String result = "";
		SessionTrace current;
		for(int i=0; i<traces.size(); i++)
		{
			current = (SessionTrace)traces.get(i);
			if(current != null)
			{
				result += "\t" 			+ current.getDate() + "\n";
				result += "\t\t"		+ current.getType() + "\n";
				result += "\t\tFROM\t"	+ current.getFrom() + "\n";
				result += "\t\tTO\t"	+ current.getTo() 	+ "\n\n";
			}
		}
		return result;
	}
}
