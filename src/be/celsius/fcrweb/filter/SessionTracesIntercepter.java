package be.celsius.fcrweb.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;


/* the intercepter that logs the session traces */
public class SessionTracesIntercepter extends Preemptable implements HandlerInterceptor {
	
	/* the session traces bean (provided by the application context) */
	private SessionTraces sessionTraces;
	
	public SessionTraces getSessionTraces() {
		return sessionTraces;
	}
	
	public void setSessionTraces(SessionTraces sessionTraces) {
		this.sessionTraces = sessionTraces;
	}
	
	/* processing before the controller */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object controller) throws Exception 
	{	
		return dispatchRequest(request, response);
	}

	/* processing after the controller */
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object controller, ModelAndView modelAndView) throws Exception {

	}
	
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception 
	{
	}
}
