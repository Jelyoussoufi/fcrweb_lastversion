package be.celsius.fcrweb.filter;

import java.io.Serializable;

/* a bean to store a session trace */
public class SessionTrace implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String type; 	/* REQUEST or RESPONSE */
	private String date;	/* the date at which the REQUEST/RESPONSE has been sent */
	private String from;	/* the sender of the REQUEST/RESPONSE */
	private String to;		/* the receiver of the REQUEST/RESPONSE */
	private String view;	/* the view associated to a controller RESPONSE */
	
	/* a constructor without parameter -- to respect the Java bean specifications */
	public SessionTrace(){
		this.type	= "";
		this.date	= "";
		this.from	= "";
		this.to		= "";
		this.view	= "";
	}
	
	/* a constructor for the REQUEST */
	public SessionTrace(String type, String date, String from, String to){
		this.type	= type;
		this.date	= date;
		this.from	= from;
		this.to		= to;
		this.view	= "";
	}
	
	/* a constructor for the RESPONSE */
	public SessionTrace(String type, String date, String from, String to, String view){
		this.type	= type;
		this.date	= date;
		this.from	= from;
		this.to		= to;
		this.view	= view;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}
}
