package be.celsius.fcrweb.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.celsius.util.Config;

public class AccessFilter extends Preemptable implements Filter
{
	private FilterConfig config = null;
	private String errorPage = "";

	public void init(FilterConfig config) throws ServletException {

		this.config = config;
		
		/**
		 * for config.getInitParameters @see web.xml Filter declarations 
		 */
		errorPage = config.getInitParameter("errorPage");
		if(errorPage == null) 
		{
			throw new ServletException("errorPage init param missing");
		}
		
	}
	
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException 
	{		
		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpResp = (HttpServletResponse) response;

		if (dispatchRequest(httpReq, httpResp))
			chain.doFilter(request, response);
	}
	
	public void destroy() {config = null;}
}
