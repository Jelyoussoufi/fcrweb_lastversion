package be.celsius.fcrweb.dataAccess;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;


import be.celsius.fcrweb.objects.ChangeLogRuleSummary;
import be.celsius.fcrweb.objects.FCRPageData;
import be.celsius.fcrweb.objects.FWObjectNamesNotCompliant;
import be.celsius.fcrweb.objects.HostDefinitionProblem;
import be.celsius.fcrweb.objects.HostDefinitionProblemList;
import be.celsius.fcrweb.objects.ObjectInconsistencies;
import be.celsius.fcrweb.objects.Profile;
import be.celsius.fcrweb.objects.ProfileSummary;
import be.celsius.fcrweb.objects.ProfilesPageData;
import be.celsius.fcrweb.objects.RequestTaskSummary;
import be.celsius.fcrweb.objects.RuleConflictObjects;
import be.celsius.fcrweb.objects.RuleObject;
import be.celsius.fcrweb.objects.ObjectSummary;
import be.celsius.fcrweb.objects.ObjectsPageData;
import be.celsius.fcrweb.objects.RequestSummary;
import be.celsius.fcrweb.objects.Rule;
import be.celsius.fcrweb.objects.RuleObjectSummary;
import be.celsius.fcrweb.objects.RuleProfile;
import be.celsius.fcrweb.objects.RuleSummary;
import be.celsius.fcrweb.objects.RulesPageData;
import be.celsius.fcrweb.objects.RuleService;
import be.celsius.fcrweb.objects.Service;
import be.celsius.fcrweb.objects.ServiceGroupSummary;
import be.celsius.fcrweb.objects.ServiceInconsistencies;
import be.celsius.fcrweb.objects.ServiceSummary;
import be.celsius.fcrweb.objects.ServicesPageData;
import be.celsius.fcrweb.objects.ServicesSearchData;
import be.celsius.fcrweb.objects.Task;
import be.celsius.fcrweb.objects.TechnicalServiceRevalidationData;
import be.celsius.fcrweb.resultSetExtractors.ChangeLogRuleSummaryExtractor;
import be.celsius.fcrweb.resultSetExtractors.IdAndNameExtractor;
import be.celsius.fcrweb.resultSetExtractors.NameExtractor;
import be.celsius.fcrweb.resultSetExtractors.ObjectExtractor;
import be.celsius.fcrweb.resultSetExtractors.ObjectInconsistenciesExtractor;
import be.celsius.fcrweb.resultSetExtractors.ProfileExtractor;
import be.celsius.fcrweb.resultSetExtractors.ProfileSummaryExtractor;
import be.celsius.fcrweb.resultSetExtractors.RequestTaskSummaryExtractor;
import be.celsius.fcrweb.resultSetExtractors.RuleObjectExtractor;
import be.celsius.fcrweb.resultSetExtractors.ObjectSummaryExtractor;
import be.celsius.fcrweb.resultSetExtractors.RequestSummaryExtractor;
import be.celsius.fcrweb.resultSetExtractors.RuleExtractor;
import be.celsius.fcrweb.resultSetExtractors.RuleProfileExtractor;
import be.celsius.fcrweb.resultSetExtractors.RuleServiceExtractor;
import be.celsius.fcrweb.resultSetExtractors.ServiceExtractor;
import be.celsius.fcrweb.resultSetExtractors.ServiceGroupSummaryExtractor;
import be.celsius.fcrweb.resultSetExtractors.ServiceInconsistenciesExtractor;
import be.celsius.fcrweb.resultSetExtractors.ServiceSummaryExtractor;
import be.celsius.fcrweb.resultSetExtractors.TaskExtractor;
import be.celsius.fcrweb.resultSetExtractors.TechnicalServiceRulesExtractor;
import be.celsius.fcrweb.utils.AuthenticationUtils;
import be.celsius.fcrweb.utils.ExtractorUtils;
import be.celsius.util.UtilLog;

public class MySqlAccessor {

	private JdbcTemplate jdbcTemplateFCR;
	private JdbcTemplate jdbcTemplateRules;
	private DataSource dataSourceRules;

	public void setDataSourceFCR(DataSource dataSourceFCR) {
		this.jdbcTemplateFCR = new JdbcTemplate(dataSourceFCR);
	}

	public void setDataSourceRules(DataSource dataSourceRules) {
		this.jdbcTemplateRules = new JdbcTemplate(dataSourceRules);
		this.dataSourceRules = dataSourceRules;
	}

	public FCRPageData getRequests(String uid) {
		FCRPageData req = new FCRPageData();

		@SuppressWarnings("unchecked")
		ArrayList<RequestTaskSummary> myTasks = (ArrayList<RequestTaskSummary>) this.jdbcTemplateFCR
				.query("select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail, edition_task.task as task from request request, request_task task, request_edition_task edition_task where task.actor like '%"+uid+"%' and request.id = task.req_id and request.id = edition_task.req_id and task.ts_completed is null",
						new Object[] { }, new RequestTaskSummaryExtractor());
		req.setMyTasks(myTasks);

		@SuppressWarnings("unchecked")
		ArrayList<RequestSummary> myOngoing = (ArrayList<RequestSummary>) this.jdbcTemplateFCR
				.query("select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail from request request, request_task task where request.requestor_uid = ? and request.id = task.req_id and request.status != 'Closure' and request.status != 'Dropped' and task.ts_completed is null",
						new Object[] { uid }, new RequestSummaryExtractor());
		for (RequestSummary request : myOngoing) {
			@SuppressWarnings("unchecked")
			ArrayList<Task> linkedTasks = (ArrayList<Task>) this.jdbcTemplateFCR
					.query("select TASK, actor as USER, ts_completed as DATE, COMMENT from request_task where req_id = ? order by id asc",
							new Object[] { request.getId() },
							new TaskExtractor());
			request.setTasks(linkedTasks);
		}
		req.setMyOngoing(myOngoing);

		/*
		 * PREVIOUS REQUESTS ARE NOT ANYMORE ADDED AT THIS PLACE SINCE IT TOOK
		 * TOO MUCH TIME TO LOAD THE PAGE. THE PREVIOUS REQUESTS ARE LOADED
		 * AFTER THE PAGE LOADING.
		 * 
		 * @SuppressWarnings("unchecked") ArrayList<RequestSummary> myPrevious =
		 * (ArrayList<RequestSummary>) this.jdbcTemplateFCR.query(
		 * "select request.ID, request.requestor_uid, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail from request request LEFT OUTER JOIN request_task task ON request.id = task.req_id and task.next_task = request.status where request.requestor_uid = ? and request.status = 'Closure' order by request.id desc"
		 * , new Object[]{uid}, new RequestSummaryExtractor()); for
		 * (RequestSummary request: myPrevious) {
		 * 
		 * @SuppressWarnings("unchecked") ArrayList<Task> linkedTasks =
		 * (ArrayList<Task>) this.jdbcTemplateFCR.query(
		 * "select TASK, actor as USER, ts_completed as DATE, COMMENT from request_task where req_id = ? order by id asc"
		 * , new Object[]{request.getId()}, new TaskExtractor());
		 * request.setTasks(linkedTasks); } req.setMyPrevious(myPrevious);
		 */
		return req;
	}

	public ArrayList<RequestSummary> getPreviousRequests(String uid) {
		@SuppressWarnings("unchecked")
		ArrayList<RequestSummary> myPrevious = (ArrayList<RequestSummary>) this.jdbcTemplateFCR
				.query("select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail from request request LEFT OUTER JOIN request_task task ON request.id = task.req_id and task.next_task = request.status where request.requestor_uid = ? and (request.status = 'Closure' or request.status= 'Rejected') order by request.id desc",
						new Object[] { uid }, new RequestSummaryExtractor());
		for (RequestSummary request : myPrevious) {
			@SuppressWarnings("unchecked")
			ArrayList<Task> linkedTasks = (ArrayList<Task>) this.jdbcTemplateFCR
					.query("select TASK, actor as USER, ts_completed as DATE, COMMENT from request_task where req_id = ? order by id asc",
							new Object[] { request.getId() },
							new TaskExtractor());
			request.setTasks(linkedTasks);
		}
		return myPrevious;
	}

	public ArrayList<RequestSummary> getSearchResultRequests(String searchValue, String uid, String profiles) {
		
		ArrayList<RequestSummary> myResult;
		
		if (!AuthenticationUtils.isCompleteSearchAllowed(profiles))
		{
			@SuppressWarnings("unchecked")
			ArrayList<RequestSummary> simpleResult = (ArrayList<RequestSummary>) this.jdbcTemplateFCR
				.query("select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail "
						+ "from request request LEFT OUTER JOIN request_task task ON request.id = task.req_id "
						+ "where (request.requestor_uid = '"+uid+"' or task.actor like '%"+uid+"%') and (request.id = ? or request.type_description like CONCAT('%',?,'%') or f_request_contains_ip_or_name_in_objects(request.id, ?)=1 or f_request_contains_port_or_name_in_services(request.id, ?)=1) and request.status = task.task and request.status != 'Dropped' "
						+ "union "
						+ "select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail "
						+ "from request request LEFT OUTER JOIN request_task task ON request.id = task.req_id "
						+ "where request.requestor_uid = '"+uid+"' and (request.id = ? or request.type_description like CONCAT('%',?,'%') or f_request_contains_ip_or_name_in_objects(request.id, ?)=1 or f_request_contains_port_or_name_in_services(request.id, ?)=1) "
						+ "and request.id not in (select distinct req_id from request_task) and request.status != 'Dropped' ",
						new Object[] { searchValue,
								searchValue, searchValue, searchValue,
								searchValue, searchValue, searchValue,
								searchValue }, new RequestSummaryExtractor());
			myResult = simpleResult;
		}
		else
		{
			@SuppressWarnings("unchecked")
			ArrayList<RequestSummary> completeResult = (ArrayList<RequestSummary>) this.jdbcTemplateFCR
				.query("select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail "
						+ "from request request LEFT OUTER JOIN request_task task ON request.id = task.req_id "
						+ "where (request.requestor_uid = '"+searchValue+"' or request.id = ? or request.type_description like CONCAT('%',?,'%') or f_request_contains_ip_or_name_in_objects(request.id, ?)=1 or f_request_contains_port_or_name_in_services(request.id, ?)=1) and request.status = task.task and request.status != 'Dropped' "
						+ "union "
						+ "select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail "
						+ "from request request LEFT OUTER JOIN request_task task ON request.id = task.req_id "
						+ "where (request.requestor_uid = '"+searchValue+"' or request.id = ? or request.type_description like CONCAT('%',?,'%') or f_request_contains_ip_or_name_in_objects(request.id, ?)=1 or f_request_contains_port_or_name_in_services(request.id, ?)=1) "
						+ "and request.id not in (select distinct req_id from request_task) and request.status != 'Dropped' ",
						new Object[] { searchValue,
								searchValue, searchValue, searchValue,
								searchValue, searchValue, searchValue,
								searchValue }, new RequestSummaryExtractor());
			myResult = completeResult;
		}
		
		
		
		for (RequestSummary request : myResult) {
			@SuppressWarnings("unchecked")
			ArrayList<Task> linkedTasks = (ArrayList<Task>) this.jdbcTemplateFCR
					.query("select TASK, actor as USER, ts_completed as DATE, COMMENT from request_task where req_id = ? order by id asc",
							new Object[] { request.getId() },
							new TaskExtractor());
			request.setTasks(linkedTasks);
		}
		return myResult;
	}

	public ArrayList<RequestSummary> getAdvancedSearchResultRequests(String searchQuery, String uid, String profiles) {
		
		String query = "select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail "
				+ "from request request LEFT OUTER JOIN request_task task ON request.id = task.req_id ";
		if (AuthenticationUtils.isCompleteSearchAllowed(profiles))
		{
			query+= " where ";
		}
		else
		{
			query+= " where (request.requestor_uid = '"+uid+"' or task.actor like '%"+uid+"%') and ";
		}				
		query+= searchQuery	+ " and request.status = task.task and request.status != 'Dropped' " 
				+ "union "
				+ "select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope, ' (', request.id, ')') as detail "
				+ "from request request LEFT OUTER JOIN request_task task ON request.id = task.req_id ";
		
		if (AuthenticationUtils.isCompleteSearchAllowed(profiles))
		{
			query+= " where ";
		}
		else
		{
			query+= " where request.requestor_uid = '"+uid+"' and ";
		}	
		query+= searchQuery	+ " and request.id not in (select distinct req_id from request_task) and request.status != 'Dropped' ";
		
		@SuppressWarnings("unchecked")
		ArrayList<RequestSummary> myResult = (ArrayList<RequestSummary>) this.jdbcTemplateFCR.query(query, new Object[] {}, new RequestSummaryExtractor());
		for (RequestSummary request : myResult) {
			@SuppressWarnings("unchecked")
			ArrayList<Task> linkedTasks = (ArrayList<Task>) this.jdbcTemplateFCR
					.query("select TASK, actor as USER, ts_completed as DATE, COMMENT from request_task where req_id = ? order by id asc",
							new Object[] { request.getId() },
							new TaskExtractor());
			request.setTasks(linkedTasks);
		}

		return myResult;
	}

	public RulesPageData getRules(String ts) 
	{
		java.util.Date date= new java.util.Date();
		
		RulesPageData rules = new RulesPageData();

		ArrayList<RuleSummary> myRules = new ArrayList<RuleSummary>();
		ArrayList<String[]> tsRules = getOwnedTechnicalServiceRulesWithoutGeneric(ts);
		for (int i = 0; i < tsRules.size(); i++) {
			@SuppressWarnings("unchecked")
			List<Map> rows = this.jdbcTemplateRules
					.queryForList("select rule.id as ID, rule.ref as REF from rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and ref in ('"+ tsRules.get(i)[1] + "')");
			for (Map row : rows) {
				Long id = (Long) row.get("ID");	
				
				myRules.add(fillRuleSummary(id, tsRules.get(i)[0]));
			}
		}
		
		ArrayList<RuleSummary> genericRules = new ArrayList<RuleSummary>();
		tsRules = getGenericTechnicalServiceRules();
		
		@SuppressWarnings("unchecked")
		List<Map> rows = this.jdbcTemplateRules
				.queryForList("select rule.id as ID, rule.ref as REF from rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and ref in ('"
						+ tsRules.get(0)[1] + "')");
		for (Map row : rows) {
			Long id = (Long) row.get("ID");
			
			genericRules.add(fillRuleSummary(id, "GENERIC"));
		}

	
		rules.setMyRules(myRules);
		rules.setGenericRules(genericRules);
		return rules;
	}
	
	public ArrayList<RuleSummary> getUserRules(String ts, String technical_service)
	{
		ArrayList<RuleSummary> myRules = new ArrayList<RuleSummary>();
		ArrayList<String[]> tsRules = getSpecificTechnicalServiceRules(ts, technical_service);
		for (int i = 0; i < tsRules.size(); i++) {
			@SuppressWarnings("unchecked")
			List<Map> rows = this.jdbcTemplateRules
					.queryForList("select rule.id as ID, rule.ref as REF from rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and ref in ('"+ tsRules.get(i)[1] + "')");
			for (Map row : rows) {
				Long id = (Long) row.get("ID");	
				
				myRules.add(fillRuleSummary(id, tsRules.get(i)[0]));
			}
		}
		return myRules;
	}
	
	public RuleSummary fillRuleSummary(Long id, String technical_serv)
	{
		return fillRuleSummaryGeneric(id, technical_serv, "");
	}
	
	public RuleSummary fillExpandedRuleSummary(Long id, String technical_serv)
	{
		return fillRuleSummaryGeneric(id, technical_serv, "expanded_");
	}

	
	public RuleSummary fillRuleSummaryGeneric(Long id, String technical_serv, String prefix)
	{
		RuleSummary myRule = new RuleSummary();
		@SuppressWarnings("unchecked")
		List<Map> ruleD = this.jdbcTemplateRules.queryForList("select distinct rule.id as ID, rule.ref as NAME, '"	+ technical_serv + "' as TECHNICAL_SERVICE, rule.active as ACTIVE, rule.nr as NR, rule.action as ACTION, rule.last_use as LAST_USED "
						+ " from rule "
						+ " where rule.id = " + id);
		
		Map ruleDetail = ruleD.get(0);
		myRule.setId(id.intValue());
		myRule.setName((String) ruleDetail.get("NAME"));
		myRule.setTechnicalService((String) ruleDetail.get("TECHNICAL_SERVICE"));
		myRule.setAction((String) ruleDetail.get("ACTION"));
		myRule.setNr(Float.toString((Float)ruleDetail.get("NR")));
		Boolean active = true;
		String act_str = (String) ruleDetail.get("ACTIVE");
		if (act_str.equals("N"))
		{
			active = false;
		}
		myRule.setActive(active);
		myRule.setLast_used((String) ruleDetail.get("LAST_USED"));
		
		@SuppressWarnings("unchecked")
		List<Map> rule_src = this.jdbcTemplateRules.queryForList("select distinct object.id as ID, object.type as TYPE, object.name as NAME, rule_src.group_name as PROFILE "
				+ " from "+prefix+"rule_src rule_src, object object "
				+ " where rule_src.rule_id = " + id + " and rule_src.object_id = object.id");
		
		for (Map ruleSrc : rule_src) 
		{
			RuleObjectSummary sum = new RuleObjectSummary();
			sum.setId(((Long) ruleSrc.get("ID")).intValue());
			sum.setName((String) ruleSrc.get("NAME"));
			sum.setType((String) ruleSrc.get("TYPE"));
			sum.setProfile((String) ruleSrc.get("PROFILE"));
			String predef = "";
			try
			{
				predef = (String) this.jdbcTemplateFCR.queryForObject("select predefined from cat_object_group where name = '"+ sum.getName() + "'", String.class);
			}
			catch (Exception e)
			{
				predef = "unknown";
				//OBJECT NOT FOUND IN FCRWEB SO WE DONT KNOW IF IT IS A GLOBAL OR LOCAL OBJECT
			}
			sum.setPredefined(predef);
			myRule.addSource(sum);
		}
		
		@SuppressWarnings("unchecked")
		List<Map> rule_dst = this.jdbcTemplateRules.queryForList("select distinct object.id as ID, object.type as TYPE, object.name as NAME"
				+ " from "+prefix+"rule_dst rule_dst, object object "
				+ " where rule_dst.rule_id = " + id + " and rule_dst.object_id = object.id");
		
		for (Map ruleDst : rule_dst) 
		{
			RuleObjectSummary sum = new RuleObjectSummary();
			sum.setId(((Long) ruleDst.get("ID")).intValue());
			sum.setName((String) ruleDst.get("NAME"));
			sum.setType((String) ruleDst.get("TYPE"));
			String predef = "";
			try
			{
				predef = (String) this.jdbcTemplateFCR.queryForObject("select predefined from cat_object_group where name = '"+ sum.getName() + "'", String.class);
			}
			catch (Exception e)
			{
				predef = "unknown";
				//OBJECT NOT FOUND IN FCRWEB SO WE DONT KNOW IF IT IS A GLOBAL OR LOCAL OBJECT
			}
			sum.setPredefined(predef);
			myRule.addDestination(sum);
		}
		
		@SuppressWarnings("unchecked")
		List<Map> rule_svc = this.jdbcTemplateRules.queryForList("select distinct service.id as ID, service.type as TYPE, service.name as NAME"
				+ " from "+prefix+"rule_service rule_service, service service "
				+ " where rule_service.rule_id = " + id + " and rule_service.service_id = service.id");
		
		for (Map ruleSvc : rule_svc) 
		{
			RuleObjectSummary sum = new RuleObjectSummary();
			sum.setId(((Long) ruleSvc.get("ID")).intValue());
			sum.setName((String) ruleSvc.get("NAME"));
			sum.setType((String) ruleSvc.get("TYPE"));
			String predef = "";
			try
			{
				if (sum.getType().equals("group"))
				{
					predef = (String) this.jdbcTemplateFCR.queryForObject("select predefined from cat_service_group where name = '"+ sum.getName() + "'", String.class);
				}
				else
				{
					predef = (String) this.jdbcTemplateFCR.queryForObject("select predefined from cat_service where name = '"+ sum.getName() + "'", String.class);
				}
			}
			catch (Exception e)
			{
				predef = "unknown";
				//SERVICE NOT FOUND IN FCRWEB SO WE DONT KNOW IF IT IS A GLOBAL OR LOCAL SERVICE
			}
			sum.setPredefined(predef);
			myRule.addService(sum);
		}
		return myRule;
	}
	
	public String getOwnedTechnicalServices(String profiles)
	{
		String ts = "'HASNOTECHNICALSERVICE'";
		boolean first = true;
		String[] ldapGroups = profiles.split("\\^");
		
		for (String ldapGroup: ldapGroups)
		{			
			@SuppressWarnings("unchecked")
			ArrayList<String> names = (ArrayList<String>) this.jdbcTemplateFCR
					.query("select distinct name as NAME from used_technical_service where LCASE(owner) = '" + ldapGroup +"'", new Object[] {}, new NameExtractor());
			
			for (String name: names)
			{				
				if (first)
				{
					first = false;
					ts = "'"+name+"'";	
				}
				else
				{
					ts += ",'" + name+ "'";
				}
			}
		}
		
		return ts;		
	}
	
	public ArrayList<String[]> getOwnedTechnicalServiceRules(String ts) {
		// Each table of the following list is constructed like this: first
		// position is the technical service name and the second position
		// is all the rulenames that are linked to that technical service
		@SuppressWarnings("unchecked")
		ArrayList<String[]> rulesPerTechnicalService = (ArrayList<String[]>) this.jdbcTemplateFCR
				.query("select ts.name as TSNAME, GROUP_CONCAT(link.rule_name SEPARATOR 'quote,quote') as RULESLIST from used_technical_service ts, link_rule_technical_service link where ts.id = link.technical_service_id and (ts.name in ("+ ts+ ") or ts.name like 'Generic') group by name",
						new Object[] {}, new TechnicalServiceRulesExtractor());

		return rulesPerTechnicalService;
	}
	
	public ArrayList<String[]> getOwnedTechnicalServiceRulesWithoutGeneric(String ts) {
		// Each table of the following list is constructed like this: first
		// position is the technical service name and the second position
		// is all the rulenames that are linked to that technical service
		@SuppressWarnings("unchecked")
		ArrayList<String[]> rulesPerTechnicalService = (ArrayList<String[]>) this.jdbcTemplateFCR
				.query("select ts.name as TSNAME, GROUP_CONCAT(link.rule_name SEPARATOR 'quote,quote') as RULESLIST from used_technical_service ts, link_rule_technical_service link where ts.id = link.technical_service_id and ts.name in ("+ ts+ ") and ts.name not like 'Generic' group by name",
						new Object[] {}, new TechnicalServiceRulesExtractor());

		return rulesPerTechnicalService;
	}

	public ArrayList<String[]> getGenericTechnicalServiceRules() {
		// Each table of the following list is constructed like this: first
		// position is the technical service name and the second position
		// is all the rulenames that are linked to that technical service
		@SuppressWarnings("unchecked")
		ArrayList<String[]> rulesPerTechnicalService = (ArrayList<String[]>) this.jdbcTemplateFCR
				.query("select ts.name as TSNAME, GROUP_CONCAT(link.rule_name SEPARATOR 'quote,quote') as RULESLIST from used_technical_service ts, link_rule_technical_service link where ts.id = link.technical_service_id and ts.name = 'Generic'",
						new Object[] {}, new TechnicalServiceRulesExtractor());

		return rulesPerTechnicalService;
	}

	public ArrayList<String[]> getAllTechnicalServiceRules(String ts) {
		// Each table of the following list is constructed like this: first
		// position is the technical service name and the second position
		// is all the rulenames that are linked to that technical service
		@SuppressWarnings("unchecked")
		ArrayList<String[]> rulesPerTechnicalService = (ArrayList<String[]>) this.jdbcTemplateFCR
				.query("select ts.name as TSNAME, GROUP_CONCAT(link.rule_name SEPARATOR 'quote,quote') as RULESLIST from used_technical_service ts, link_rule_technical_service link where ts.id = link.technical_service_id group by name",
						new Object[] {}, new TechnicalServiceRulesExtractor());

		return rulesPerTechnicalService;
	}
	
	public ArrayList<String[]> getSpecificTechnicalServiceRules(String ts, String technical_service) {
		// Each table of the following list is constructed like this: first
		// position is the technical service name and the second position
		// is all the rulenames that are linked to that technical service
		@SuppressWarnings("unchecked")
		ArrayList<String[]> rulesPerTechnicalService = (ArrayList<String[]>) this.jdbcTemplateFCR
				.query("select ts.name as TSNAME, GROUP_CONCAT(link.rule_name SEPARATOR 'quote,quote') as RULESLIST from used_technical_service ts, link_rule_technical_service link where ts.id = link.technical_service_id  " +
						"and ts.name = '"+technical_service+"' group by name", new Object[] {}, new TechnicalServiceRulesExtractor());

		return rulesPerTechnicalService;
	}
	
	public String getAllTechnicalServiceNames() {
		String tsnames = "";
		@SuppressWarnings("unchecked")
		List<Map> rowsTS = this.jdbcTemplateFCR
				.queryForList("select distinct name as NAME from cat_technical_service order by name");
		boolean first = true;
		for (Map row : rowsTS) {
			if (first) {
				first = false;
				tsnames = (String) row.get("NAME");
			} else {
				tsnames += "," + (String) row.get("NAME");
			}
		}

		return tsnames;
	}
	
	public String getAutocompleteTechnicalServices(String tsStart)
	{
		String tsnames = "";		
		@SuppressWarnings("unchecked")
		List<Map> rowsTS = this.jdbcTemplateFCR
				.queryForList("select distinct name as NAME from cat_technical_service where name like '%"+tsStart+"%' order by name");
		boolean first = true;
		for (Map row : rowsTS) {
			if (first) {
				first = false;
				tsnames = (String) row.get("NAME");
			} else {
				tsnames += "|" + (String) row.get("NAME");
			}
		}

		return tsnames;
	}
	
	public String getAutocompleteRules(String ruleStart)
	{
		String rulenames = "";		
		@SuppressWarnings("unchecked")
		List<Map> rowsRules = this.jdbcTemplateRules
				.queryForList("select distinct rule.ref as NAME from rule where rule.ref like '%"+ruleStart+"%' and rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase))");
		boolean first = true;
		for (Map row : rowsRules) {
			if (first) {
				first = false;
				rulenames = (String) row.get("NAME");
			} else {
				rulenames += "|" + (String) row.get("NAME");
			}
		}

		return rulenames;
	}

	public ArrayList<String[]> getSpecificTechnicalServiceRules(String technical_service) {
		// Each table of the following list is constructed like this: first
		// position is the technical service name and the second position
		// is all the rulenames that are linked to that technical service
		@SuppressWarnings("unchecked")
		ArrayList<String[]> rulesPerTechnicalService = (ArrayList<String[]>) this.jdbcTemplateFCR
				.query("select ts.name as TSNAME, GROUP_CONCAT(link.rule_name SEPARATOR 'quote,quote') as RULESLIST from used_technical_service ts, link_rule_technical_service link where ts.id = link.technical_service_id and ts.name = '"
						+ technical_service + "'", new Object[] {},
						new TechnicalServiceRulesExtractor());

		return rulesPerTechnicalService;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<RuleSummary> getSearchResultRules(String searchValue, String ts) {

		ArrayList<RuleSummary> rules = new ArrayList<RuleSummary>();

		ArrayList<String[]> tsRules = getOwnedTechnicalServiceRules(ts);

		for (int i = 0; i < tsRules.size(); i++) {

			List<Map> rows = this.jdbcTemplateRules
					.queryForList("select rule.id as ID, rule.ref as REF from rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and ref in ('"+ tsRules.get(i)[1]	+ "') "
							+ "and (rule.ref like '%"+ searchValue+ "%' or f_rule_contains_ip_or_name_in_objects(rule.id, '"	+ searchValue+ "') = 1 " 
							+ "or f_rule_contains_port_or_name_in_services(rule.id, '"+ searchValue + "') = 1) ");
			for (Map row : rows) {
				Long id = (Long) row.get("ID");				
				rules.add(fillRuleSummary(id, tsRules.get(i)[0]));
			}
		}
		return rules;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<RuleSummary> getAdvancedSearchResultRules(String searchQuery, String ts, String profiles) {

		ArrayList<RuleSummary> rules = new ArrayList<RuleSummary>();

		ArrayList<String[]> tsRules;
		if (AuthenticationUtils.isCompleteSearchAllowed(profiles))
		{
			tsRules= getAllTechnicalServiceRules(ts);
		}
		else
		{
			tsRules= getOwnedTechnicalServiceRules(ts);
		}

		for (int i = 0; i < tsRules.size(); i++) {

			List<Map> rows = this.jdbcTemplateRules
					.queryForList("select rule.id as ID, rule.ref as REF from rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and ref in ('"+ tsRules.get(i)[1] + "') and " + searchQuery);
			for (Map row : rows) {
				Long id = (Long) row.get("ID");
				rules.add(fillRuleSummary(id, tsRules.get(i)[0]));
			}
		}
		return rules;
	}

	public ArrayList<ObjectSummary> getSearchResultObjects(String searchValue, String ts, String profiles) {
		
		String query = "select distinct grp.id as ID, grp.name as NAME, grp.description as DESCRIPTION, ts.name as TECHNICAL_SERVICE "+
						" from cat_object_group grp "+
						" left join used_technical_service ts on ts.id = grp.technical_service_id "+
						" where f_cat_object_group_contains_ip_or_name_in_members(grp.id, '"+searchValue+"') = 1 ";
		
		if (!AuthenticationUtils.isCompleteSearchAllowed(profiles))
		{
			query += " and ((grp.predefined = 0 and grp.technical_service_id in (select id from used_technical_service where name in ("+ts+"))) or grp.predefined = 1)";
		}

		@SuppressWarnings("unchecked")
		ArrayList<ObjectSummary> objects = (ArrayList<ObjectSummary>) this.jdbcTemplateFCR
				.query(query, new Object[] {},
						new ObjectSummaryExtractor());

		return objects;
	}

	public ServicesSearchData getSearchResultServices(String searchValue, String ts, String profiles) 
	{		
		ServicesSearchData result = new ServicesSearchData();
		
		String query = "select grp.id as ID, grp.name as NAME, grp.description as DESCRIPTION, ts.name as TECHNICAL_SERVICE "+
				" from  cat_service_group grp "+
				" left join used_technical_service ts on ts.id = grp.technical_service_id "+
				" where f_cat_service_group_contains_port_or_name_in_services(grp.id, '"+searchValue+"') = 1 ";
		
		if (!AuthenticationUtils.isCompleteSearchAllowed(profiles))
		{
			query += " and ((grp.predefined = 0 and grp.technical_service_id in (select id from used_technical_service where name in ("+ts+"))) or grp.predefined = 1)";
		}
		@SuppressWarnings("unchecked")
		ArrayList<ServiceGroupSummary> serviceGroups = (ArrayList<ServiceGroupSummary>) this.jdbcTemplateFCR
				.query(query, new Object[] {},
						new ServiceGroupSummaryExtractor());
		result.setServiceGroups(serviceGroups);
		
		
		
		query = "select grp.id as ID, grp.name as NAME, grp.protocol as PROTOCOL, grp.port as PORT, ts.name as TECHNICAL_SERVICE "+
				" from  cat_service grp "+
				" left join used_technical_service ts on ts.id = grp.technical_service_id "+
				" where (grp.name like '%"+searchValue+"%' or grp.port like '%"+searchValue+"%') ";
		
		if (!AuthenticationUtils.isCompleteSearchAllowed(profiles))
		{
			query += " and ((grp.predefined = 0 and grp.technical_service_id in (select id from used_technical_service where name in ("+ts+"))) or grp.predefined = 1)";
		}
		@SuppressWarnings("unchecked")
		ArrayList<ServiceSummary> services = (ArrayList<ServiceSummary>) this.jdbcTemplateFCR
				.query(query, new Object[] {},
						new ServiceSummaryExtractor());
		result.setServices(services);
		
		return result;
	}

	public Rule getRule(String id, String ts) 
	{
		ArrayList<String[]> tsRules = getOwnedTechnicalServiceRules(ts);

		Rule rule = (Rule) this.jdbcTemplateRules
				.query("select ruleref.creation_date as CREATION_DATE, rule.ref as NAME, rule.comment as COMMENT, '' as ENDDATE,  rule.last_use as LASTUSED "+
						" from rule rule "+
						" left join ruleref ruleref on rule.ref = ruleref.ref "+
						" where rule.id = "
						+ id, new Object[] {}, new RuleExtractor());
		
		try
		{
			String fcr = (String) this.jdbcTemplateFCR.queryForObject("select request_id from link_rule_fcr where concat(firewall_name, ' ', firewall_id) = '"+ rule.getName() + "'", String.class);
			rule.setFcr(fcr);
		}
		catch (Exception e)
		{
			// NO RULE REFERENCE IN LINK_RULE_FCR (HAPPENS FOR OLD FCR)
		}
		@SuppressWarnings("unchecked")
		List<Map> rows = this.jdbcTemplateFCR
				.queryForList("select ts.name as TS_NAME from used_technical_service ts, link_rule_technical_service link where ts.id = link.technical_service_id and link.rule_name = '"
						+ rule.getName() + "'");
		String tsName = "";
		for (Map row : rows) {
			tsName = (String) row.get("TS_NAME");
		}

		rule.setSummary(fillRuleSummary(Long.parseLong(id), tsName));
		rule.setExpandedSummary(fillExpandedRuleSummary(Long.parseLong(id), tsName));		
		
		return rule;
	}
	
	public ArrayList<ChangeLogRuleSummary> getRuleChangeLog(String name)
	{
		@SuppressWarnings("unchecked") 
		ArrayList<ChangeLogRuleSummary>	changeLogRuleList = (ArrayList<ChangeLogRuleSummary>)
		this.jdbcTemplateRules.query(
		"select rule.id as ID, ch.new_version as DATE, ch.changetype as CHANGETYPE, rule.action as ACTION, rule.nr as NR " +
		"from ruleref_changelog ch, rulebase rb, rule rule " +
		"where ch.ref = '"+name+"' and (rb.time_start = ch.new_version) and rule.rb_id = rb.id and rule.ref = '"+name+"' order by ch.new_version desc", new Object[]{}, new ChangeLogRuleSummaryExtractor());
		
		for (int i = 0; i < changeLogRuleList.size(); i++)
		{
			int firstId = changeLogRuleList.get(i).getId();
			int secondId = 0;
			if (i == changeLogRuleList.size()-1)
			{
				secondId = changeLogRuleList.get(i).getId(); //The last rule id must not be compared to a newer one. It is compared to itself so all the objects (src, dst, services) are added to src_existing, dst_existing or service_existing
			}
			else
			{
				secondId = changeLogRuleList.get(i+1).getId();
			}
			String src_existing = "";
			String src_new = "";
			String src_removed = "";
			String dst_existing = "";
			String dst_new = "";
			String dst_removed = "";
			String svc_existing = "";
			String svc_new = "";
			String svc_removed = "";
			
			//SRC difference computation			
			@SuppressWarnings("unchecked")
			List<Map> src_names_first = this.jdbcTemplateRules.queryForList("select object.id as ID, concat(object.name, '') as NAME from rule_src src, object object where src.object_id = object.id and src.rule_id = " + firstId);
			List<Map> src_names_second = this.jdbcTemplateRules.queryForList("select object.id as ID, concat(object.name, '') as NAME from rule_src src, object object where src.object_id = object.id and src.rule_id = " + secondId);
			
			
			for (Map row_first : src_names_first) 
			{
				String first_name = (String) row_first.get("NAME");
				String first_id = (Long) row_first.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_second: src_names_second)
				{
					String second_name = (String) row_second.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}
				if (hasEquivalent)
				{
					src_existing+=(("<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
				else
				{
					src_new+=(("<a style=\"color: green\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			
			for (Map row_second : src_names_second) 
			{
				String second_name = (String) row_second.get("NAME");
				String second_id = (Long) row_second.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_first: src_names_first)
				{
					String first_name = (String) row_first.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}				
				if (!hasEquivalent)
				{
					src_removed+=(("<a style=\"color: red\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + second_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + second_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			changeLogRuleList.get(i).setSrc_existing(src_existing);
			changeLogRuleList.get(i).setSrc_new(src_new);
			changeLogRuleList.get(i).setSrc_removed(src_removed);
			
			//DST difference computation
			@SuppressWarnings("unchecked")
			List<Map> dst_names_first = this.jdbcTemplateRules.queryForList("select object.id as ID, concat(object.name, '') as NAME from rule_dst dst, object object where dst.object_id = object.id and dst.rule_id = " + firstId);
			List<Map> dst_names_second = this.jdbcTemplateRules.queryForList("select object.id as ID, concat(object.name, '') as NAME from rule_dst dst, object object where dst.object_id = object.id and dst.rule_id = " + secondId);
			
			
			for (Map row_first : dst_names_first) 
			{
				String first_name = (String) row_first.get("NAME");
				String first_id = (Long) row_first.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_second: dst_names_second)
				{
					String second_name = (String) row_second.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}
				if (hasEquivalent)
				{
					dst_existing+=(("<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
				else
				{
					dst_new+=(("<a style=\"color: green\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			
			for (Map row_second : dst_names_second) 
			{
				String second_name = (String) row_second.get("NAME");
				String second_id = (Long) row_second.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_first: dst_names_first)
				{
					String first_name = (String) row_first.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}
				if (!hasEquivalent)
				{
					dst_removed+=(("<a style=\"color: red\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + second_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + second_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			changeLogRuleList.get(i).setDst_existing(dst_existing);
			changeLogRuleList.get(i).setDst_new(dst_new);
			changeLogRuleList.get(i).setDst_removed(dst_removed);
			
			//SERVICE difference computation
			@SuppressWarnings("unchecked")
			List<Map> svc_names_first = this.jdbcTemplateRules.queryForList("select service.id as ID, concat(UPPER(service.type), ' ' , service.name) as NAME from rule_service rs, service service where rs.service_id = service.id and rs.rule_id = " + firstId);
			List<Map> svc_names_second = this.jdbcTemplateRules.queryForList("select service.id as ID, concat(UPPER(service.type), ' ' , service.name) as NAME from rule_service rs, service service where rs.service_id = service.id and rs.rule_id = " + secondId);
			
			
			for (Map row_first : svc_names_first) 
			{
				String first_name = (String) row_first.get("NAME");
				String first_id = (Long) row_first.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_second: svc_names_second)
				{
					String second_name = (String) row_second.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}
				if (hasEquivalent)
				{
					svc_existing+=(("<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
				else
				{
					svc_new+=(("<a style=\"color: green\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			
			for (Map row_second : svc_names_second) 
			{
				String second_name = (String) row_second.get("NAME");
				String second_id = (Long) row_second.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_first: svc_names_first)
				{
					String first_name = (String) row_first.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}
				if (!hasEquivalent)
				{
					svc_removed+=(("<a style=\"color: red\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + second_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + second_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			changeLogRuleList.get(i).setSvc_existing(svc_existing);
			changeLogRuleList.get(i).setSvc_new(svc_new);
			changeLogRuleList.get(i).setSvc_removed(svc_removed);

			String expanded_src_existing = "";
			String expanded_src_new = "";
			String expanded_src_removed = "";
			String expanded_dst_existing = "";
			String expanded_dst_new = "";
			String expanded_dst_removed = "";
			String expanded_svc_existing = "";
			String expanded_svc_new = "";
			String expanded_svc_removed = "";
			
			//EXPANDED_SRC difference computation			
			@SuppressWarnings("unchecked")
			List<Map> expanded_src_names_first = this.jdbcTemplateRules.queryForList("select object.id as ID, concat(object.name, '') as NAME from expanded_rule_src src, object object where src.object_id = object.id and src.rule_id = " + firstId);
			List<Map> expanded_src_names_second = this.jdbcTemplateRules.queryForList("select object.id as ID, concat(object.name, '') as NAME from expanded_rule_src src, object object where src.object_id = object.id and src.rule_id = " + secondId);
			
			
			for (Map row_first : expanded_src_names_first) 
			{
				String first_name = (String) row_first.get("NAME");
				String first_id = (Long) row_first.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_second: expanded_src_names_second)
				{
					String second_name = (String) row_second.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}
				if (hasEquivalent)
				{
					expanded_src_existing+=(("<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
				else
				{
					expanded_src_new+=(("<a style=\"color: green\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			
			for (Map row_second : expanded_src_names_second) 
			{
				String second_name = (String) row_second.get("NAME");
				String second_id = (Long) row_second.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_first: expanded_src_names_first)
				{
					String first_name = (String) row_first.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}				
				if (!hasEquivalent)
				{
					expanded_src_removed+=(("<a style=\"color: red\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + second_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + second_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			changeLogRuleList.get(i).setExpanded_src_existing(expanded_src_existing);
			changeLogRuleList.get(i).setExpanded_src_new(expanded_src_new);
			changeLogRuleList.get(i).setExpanded_src_removed(expanded_src_removed);
			
			//EXPANDED_DST difference computation
			@SuppressWarnings("unchecked")
			List<Map> expanded_dst_names_first = this.jdbcTemplateRules.queryForList("select object.id as ID, concat(object.name, '') as NAME from expanded_rule_dst dst, object object where dst.object_id = object.id and dst.rule_id = " + firstId);
			List<Map> expanded_dst_names_second = this.jdbcTemplateRules.queryForList("select object.id as ID, concat(object.name, '') as NAME from expanded_rule_dst dst, object object where dst.object_id = object.id and dst.rule_id = " + secondId);
			
			
			for (Map row_first : expanded_dst_names_first) 
			{
				String first_name = (String) row_first.get("NAME");
				String first_id = (Long) row_first.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_second: expanded_dst_names_second)
				{
					String second_name = (String) row_second.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}
				if (hasEquivalent)
				{
					expanded_dst_existing+=(("<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
				else
				{
					expanded_dst_new+=(("<a style=\"color: green\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			
			for (Map row_second : expanded_dst_names_second) 
			{
				String second_name = (String) row_second.get("NAME");
				String second_id = (Long) row_second.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_first: expanded_dst_names_first)
				{
					String first_name = (String) row_first.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}
				if (!hasEquivalent)
				{
					expanded_dst_removed+=(("<a style=\"color: red\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + second_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + second_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			changeLogRuleList.get(i).setExpanded_dst_existing(expanded_dst_existing);
			changeLogRuleList.get(i).setExpanded_dst_new(expanded_dst_new);
			changeLogRuleList.get(i).setExpanded_dst_removed(expanded_dst_removed);
			
			//EXPANDED_SERVICE difference computation
			@SuppressWarnings("unchecked")
			List<Map> expanded_svc_names_first = this.jdbcTemplateRules.queryForList("select service.id as ID, concat(UPPER(service.type), ' ' , service.name) as NAME from expanded_rule_service rs, service service where rs.service_id = service.id and rs.rule_id = " + firstId);
			List<Map> expanded_svc_names_second = this.jdbcTemplateRules.queryForList("select service.id as ID, concat(UPPER(service.type), ' ' , service.name) as NAME from expanded_rule_service rs, service service where rs.service_id = service.id and rs.rule_id = " + secondId);
			
			
			for (Map row_first : expanded_svc_names_first) 
			{
				String first_name = (String) row_first.get("NAME");
				String first_id = (Long) row_first.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_second: expanded_svc_names_second)
				{
					String second_name = (String) row_second.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}
				if (hasEquivalent)
				{
					expanded_svc_existing+=(("<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
				else
				{
					expanded_svc_new+=(("<a style=\"color: green\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + first_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + first_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			
			for (Map row_second : expanded_svc_names_second) 
			{
				String second_name = (String) row_second.get("NAME");
				String second_id = (Long) row_second.get("ID") +"";
				Boolean hasEquivalent = false;
				
				for (Map row_first: expanded_svc_names_first)
				{
					String first_name = (String) row_first.get("NAME");
					if (first_name.equals(second_name))
					{
						hasEquivalent = true;
					}
				}				
				if (!hasEquivalent)
				{
					expanded_svc_removed+=(("<a style=\"color: red\" href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + second_id + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + second_name + "</a><br>").replace("\"", "doublequote"));
				}
			}
			changeLogRuleList.get(i).setExpanded_svc_existing(expanded_svc_existing);
			changeLogRuleList.get(i).setExpanded_svc_new(expanded_svc_new);
			changeLogRuleList.get(i).setExpanded_svc_removed(expanded_svc_removed);
			
		}
		
		return changeLogRuleList;
	}

	public ArrayList<ProfileSummary> getSearchResultProfiles(String searchValue, String ts, String profiles) 
	{
		String query = "";
		
		if (AuthenticationUtils.isCompleteSearchAllowed(profiles))
		{
			query = "select profile.id as ID, profile.name as NAME, profile.description as DESCRIPTION, '' as TECHNICAL_SERVICE "
					+ " from cat_profile profile "
					+ " where profile.name like '%"	+ searchValue + "%'";
		}
		else
		{
			query = "select profile.id as ID, profile.name as NAME, profile.description as DESCRIPTION, ts.name as TECHNICAL_SERVICE "
					+ " from cat_profile profile, used_technical_service ts "
					+ " where profile.name like '%"	+ searchValue+ "%' and  ts.id = profile.technical_service_id and ts.name in ("+ ts	+ ") and profile.predefined = 0 "
					+ " union "
					+ " select profile.id as ID, profile.name as NAME, profile.description as DESCRIPTION, '' as TECHNICAL_SERVICE "
					+ " from cat_profile profile "
					+ " where profile.name like '%"	+ searchValue + "%' and profile.predefined = 1";
		}
		
		@SuppressWarnings("unchecked")
		ArrayList<ProfileSummary> user_groups = (ArrayList<ProfileSummary>) this.jdbcTemplateFCR
				.query(query, new Object[] {},
						new ProfileSummaryExtractor());
		return user_groups;
	}

	public ObjectsPageData getObjects(String ts) {
		ObjectsPageData objects = new ObjectsPageData();
		@SuppressWarnings("unchecked")
		ArrayList<ObjectSummary> predefinedObjects = (ArrayList<ObjectSummary>) this.jdbcTemplateFCR
				.query("select grp.id as ID, grp.name as NAME, grp.description as DESCRIPTION, '' as TECHNICAL_SERVICE from cat_object_group grp where grp.predefined = 1",
						new Object[] {}, new ObjectSummaryExtractor());
		objects.setPredefinedObjects(predefinedObjects);
		@SuppressWarnings("unchecked")
		ArrayList<ObjectSummary> myObjects = (ArrayList<ObjectSummary>) this.jdbcTemplateFCR
				.query("select grp.id as ID, grp.name as NAME, grp.description as DESCRIPTION, ts.name as TECHNICAL_SERVICE from cat_object_group grp, used_technical_service ts where grp.predefined = 0 and grp.technical_service_id = ts.id and ts.name in ("+ ts + ") order by ts.name", new Object[] {},
						new ObjectSummaryExtractor());
		objects.setMyObjects(myObjects);
		return objects;
	}

	public ServicesPageData getServices(String ts) {
		ServicesPageData services = new ServicesPageData();
		@SuppressWarnings("unchecked")
		ArrayList<ServiceSummary> predefinedServices = (ArrayList<ServiceSummary>) this.jdbcTemplateFCR
				.query("select id as ID, name as NAME, protocol as PROTOCOL, port as PORT, '' as TECHNICAL_SERVICE from cat_service where predefined = 1",
						new Object[] {}, new ServiceSummaryExtractor());
		services.setPredefinedServices(predefinedServices);
		@SuppressWarnings("unchecked")
		ArrayList<ServiceGroupSummary> predefinedServiceGroups = (ArrayList<ServiceGroupSummary>) this.jdbcTemplateFCR
				.query("select svc.id as ID, svc.name as NAME, svc.description as DESCRIPTION, '' as TECHNICAL_SERVICE from cat_service_group svc where svc.predefined = 1",
						new Object[] {}, new ServiceGroupSummaryExtractor());
		services.setPredefinedServiceGroups(predefinedServiceGroups);
		@SuppressWarnings("unchecked")
		ArrayList<ServiceSummary> myServices = (ArrayList<ServiceSummary>) this.jdbcTemplateFCR
				.query("select service.id as ID, service.name as NAME, service.protocol as PROTOCOL, service.port as PORT, ts.name as TECHNICAL_SERVICE from cat_service service, used_technical_service ts where service.technical_service_id = ts.id and predefined = 0 and ts.name in ("+ ts + ") order by ts.name", new Object[] {},
						new ServiceSummaryExtractor());
		services.setMyServices(myServices);
		@SuppressWarnings("unchecked")
		ArrayList<ServiceGroupSummary> myServiceGroups = (ArrayList<ServiceGroupSummary>) this.jdbcTemplateFCR
				.query("select svc.id as ID, svc.name as NAME, svc.description as DESCRIPTION, ts.name as TECHNICAL_SERVICE from cat_service_group svc, used_technical_service ts where svc.predefined = 0 and svc.technical_service_id = ts.id and ts.name in ("+ ts + ") order by ts.name", new Object[] {},
						new ServiceGroupSummaryExtractor());
		services.setMyServiceGroups(myServiceGroups);

		return services;
	}

	public ProfilesPageData getProfiles(String ts) {
		ProfilesPageData profiles = new ProfilesPageData();
		@SuppressWarnings("unchecked")
		ArrayList<ProfileSummary> predefinedProfiles = (ArrayList<ProfileSummary>) this.jdbcTemplateFCR
				.query("select prof.id as ID, prof.name as NAME, prof.description as DESCRIPTION, 'GENERIC' as TECHNICAL_SERVICE from cat_profile prof where prof.predefined = 1",
						new Object[] {}, new ProfileSummaryExtractor());
		profiles.setPredefinedProfiles(predefinedProfiles);
		@SuppressWarnings("unchecked")
		ArrayList<ProfileSummary> myProfiles = (ArrayList<ProfileSummary>) this.jdbcTemplateFCR
				.query("select prof.id as ID, prof.name as NAME, prof.description as DESCRIPTION, ts.name as TECHNICAL_SERVICE from cat_profile prof, used_technical_service ts where prof.predefined = 0 and ts.id = prof.technical_service_id and ts.name in ("+ ts + ") order by ts.name", new Object[] {},
						new ProfileSummaryExtractor());
		profiles.setMyProfiles(myProfiles);
		return profiles;
	}

	public RuleObject getRuleObject(String id) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar now = Calendar.getInstance();
		
		RuleObject object = new RuleObject();
		@SuppressWarnings("unchecked")
		List<Map> rows = this.jdbcTemplateRules
				.queryForList("select TYPE from object where id = " + id);
		String type = "";
		for (Map row : rows) {
			type = (String) row.get("TYPE");
		}
		now = Calendar.getInstance();
		
		if (type.equals("group")) {
			@SuppressWarnings("unchecked")
			RuleObject objectGroup = (RuleObject) this.jdbcTemplateRules
					.query("select object.id as ID, concat('', object.name) as NAME, object.type as TYPE, GROUP_CONCAT(distinct concat(memberof.id , ',', memberof.name, ',', memberof.type) SEPARATOR ';') as USED_IN_GROUPS, CONCAT(GROUP_CONCAT(distinct CONCAT(rule_src.id, ',', rule_src.ref) SEPARATOR ';'),';', GROUP_CONCAT(distinct CONCAT(rule_dst.id, ',', rule_dst.ref) SEPARATOR ';')) as USED_IN_RULES, GROUP_CONCAT(distinct CONCAT(memberb.id, ',', memberb.name, ',', memberb.type) SEPARATOR ';') as MEMBERS "
							+ "from object_member used_in "
							+ "  right join object object on object.id = used_in.member_id "
							+ "  left join object memberof on used_in.object_id = memberof.id, "
							+ "    rule_src src "
							+ "  right join object object_src on src.object_id = object_src.id "
							+ "  left join rule rule_src on src.rule_id = rule_src.id, "
							+ "    rule_dst dst "
							+ "  right join object object_dst on dst.object_id = object_dst.id "
							+ "  left join rule rule_dst on dst.rule_id = rule_dst.id, "
							+ "    object_member members "
							+ "  right join object object_member on members.object_id = object_member.id "
							+ "  left join object memberb on members.member_id = memberb.id "
							+ "where object.id = "+ id
							+ " and object_src.id = "+ id
							+ " and object_dst.id = "+ id
							+ " and object_member.id = " + id + "",
							new Object[] {}, new RuleObjectExtractor(type));
			object = objectGroup;
		} else if (type.equals("host")) {
			@SuppressWarnings("unchecked")
			RuleObject objectHost = (RuleObject) this.jdbcTemplateRules
					.query("select object.id as ID, concat('', object.name) as NAME, object.type as TYPE, GROUP_CONCAT(distinct concat(memberof.id , ',', memberof.name, ',', memberof.type) SEPARATOR ';') as USED_IN_GROUPS, CONCAT(GROUP_CONCAT(distinct CONCAT(rule_src.id, ',', rule_src.ref) SEPARATOR ';'),';', GROUP_CONCAT(distinct CONCAT(rule_dst.id, ',', rule_dst.ref) SEPARATOR ';')) as USED_IN_RULES, object.ip_address as IP_ADDRESS "
							+ " from object_member used_in "
							+ "  right join object object on object.id = used_in.member_id "
							+ "  left join object memberof on used_in.object_id = memberof.id, "
							+ "    rule_src src "
							+ "  right join object object_src on src.object_id = object_src.id "
							+ "  left join rule rule_src on src.rule_id = rule_src.id, "
							+ "    rule_dst dst "
							+ "  right join object object_dst on dst.object_id = object_dst.id "
							+ "  left join rule rule_dst on dst.rule_id = rule_dst.id "
							+ "where object.id = "+ id
							+ " and object_src.id = "+ id
							+ " and object_dst.id = " + id + "",
							new Object[] {}, new RuleObjectExtractor(type));
			object = objectHost;
			String[] area = new String[2];
			area[0] = "Area";
			try
			{
				area[1] = (String) this.jdbcTemplateFCR.queryForObject("select zone.name from cat_host host, cat_subnet subnet, cat_zone zone where host.subnet_id = subnet.id and subnet.zone_id = zone.id and host.ip = '"+object.getOtherParameters().get(0)[1]+"'", String.class);;
			}
			catch(Exception e)
			{
				//Area not found
			}
			object.getOtherParameters().add(area);
		} else if (type.equals("network")) {
			@SuppressWarnings("unchecked")
			RuleObject objectNetwork = (RuleObject) this.jdbcTemplateRules
					.query("select object.id as ID, concat('', object.name) as NAME, object.type as TYPE, GROUP_CONCAT(distinct concat(memberof.id , ',', memberof.name, ',', memberof.type) SEPARATOR ';') as USED_IN_GROUPS, CONCAT(GROUP_CONCAT(distinct CONCAT(rule_src.id, ',', rule_src.ref) SEPARATOR ';'),';', GROUP_CONCAT(distinct CONCAT(rule_dst.id, ',', rule_dst.ref) SEPARATOR ';')) as USED_IN_RULES, object.ip_address as IP_ADDRESS, object.netmask as NETMASK "
							+ " from object_member used_in "
							+ "  right join object object on object.id = used_in.member_id "
							+ "  left join object memberof on used_in.object_id = memberof.id, "
							+ "    rule_src src "
							+ "  right join object object_src on src.object_id = object_src.id "
							+ "  left join rule rule_src on src.rule_id = rule_src.id, "
							+ "    rule_dst dst "
							+ "  right join object object_dst on dst.object_id = object_dst.id "
							+ "  left join rule rule_dst on dst.rule_id = rule_dst.id "
							+ "where object.id = "+ id
							+ " and object_src.id = "+ id
							+ " and object_dst.id = " + id + "",
							new Object[] {}, new RuleObjectExtractor(type));
			object = objectNetwork;
			String[] area = new String[2];
			area[0] = "Area";
			try
			{
				area[1] = (String) this.jdbcTemplateFCR.queryForObject("select zone.name from cat_subnet subnet, cat_zone zone where subnet.zone_id = zone.id and concat(subnet.ip, '/', subnet.mask) = '"+object.getOtherParameters().get(0)[1] + "/" +object.getOtherParameters().get(1)[1]+"'", String.class);;
			}
			catch(Exception e)
			{
				//Area not found
			}
			object.getOtherParameters().add(area);
		} else {
			@SuppressWarnings("unchecked")
			RuleObject objectElse = (RuleObject) this.jdbcTemplateRules
					.query("select object.id as ID, concat('', object.name) as NAME, object.type as TYPE, GROUP_CONCAT(distinct concat(memberof.id , ',', memberof.name, ',', memberof.type) SEPARATOR ';') as USED_IN_GROUPS, CONCAT(GROUP_CONCAT(distinct CONCAT(rule_src.id, ',', rule_src.ref) SEPARATOR ';'),';', GROUP_CONCAT(distinct CONCAT(rule_dst.id, ',', rule_dst.ref) SEPARATOR ';')) as USED_IN_RULES, object.ip_address as IP_ADDRESS "
							+ " from object_member used_in "
							+ "  right join object object on object.id = used_in.member_id "
							+ "  left join object memberof on used_in.object_id = memberof.id, "
							+ "    rule_src src "
							+ "  right join object object_src on src.object_id = object_src.id "
							+ "  left join rule rule_src on src.rule_id = rule_src.id, "
							+ "    rule_dst dst "
							+ "  right join object object_dst on dst.object_id = object_dst.id "
							+ "  left join rule rule_dst on dst.rule_id = rule_dst.id "
							+ "where object.id = "
							+ id
							+ " and object_src.id = "
							+ id
							+ " and object_dst.id = " + id + "",
							new Object[] {}, new RuleObjectExtractor(type));
			object = objectElse;
		}
		now = Calendar.getInstance();
		
		return object;
	}

	public RuleService getRuleService(String id) {
		RuleService service = new RuleService();
		@SuppressWarnings("unchecked")
		List<Map> rows = this.jdbcTemplateRules
				.queryForList("select TYPE from service where id = " + id);
		String type = "";
		for (Map row : rows) {
			type = (String) row.get("TYPE");
		}
		if (type.equals("group")) {
			RuleService serviceGroup = (RuleService) this.jdbcTemplateRules
					.query("select service.id as ID, concat('', service.name) as NAME, service.type as TYPE, GROUP_CONCAT(distinct concat(memberof.id , ',', memberof.name, ',', memberof.type) SEPARATOR ';') as USED_IN_GROUPS, GROUP_CONCAT(distinct CONCAT(rule_svc.id, ',', rule_svc.ref) SEPARATOR ';') as USED_IN_RULES, GROUP_CONCAT(distinct CONCAT(memberb.id, ',', memberb.name, ',', memberb.type) SEPARATOR ';') as MEMBERS "
							+ "from service_member used_in "
							+ "	right join service service on service.id = used_in.member_id "
							+ "	left join service memberof on used_in.service_id = memberof.id, "
							+ "		rule_service svc "
							+ "	right join service service_svc on svc.service_id = service_svc.id "
							+ "	left join rule rule_svc on svc.rule_id = rule_svc.id, "
							+ "    service_member members "
							+ "	right join service service_member on members.service_id = service_member.id "
							+ "	left join service memberb on members.member_id = memberb.id "
							+ "where service.id = "	+ id
							+ " and service_svc.id = "+ id
							+ " and service_member.id = " + id + "",
							new Object[] {}, new RuleServiceExtractor(type));
			service = serviceGroup;
		} else {
			RuleService serviceElse = (RuleService) this.jdbcTemplateRules
					.query("select service.id as ID, concat('', service.name) as NAME, service.type as TYPE, GROUP_CONCAT(distinct concat(memberof.id , ',', memberof.name, ',', memberof.type) SEPARATOR ';') as USED_IN_GROUPS, GROUP_CONCAT(distinct CONCAT(rule_svc.id, ',', rule_svc.ref) SEPARATOR ';') as USED_IN_RULES, service.dst_port as DESTINATION_PORT "
							+ "	from service_member used_in "
							+ "		right join service service on service.id = used_in.member_id "
							+ "		left join service memberof on used_in.service_id = memberof.id, "
							+ "			rule_service svc "
							+ "		right join service service_svc on svc.service_id = service_svc.id "
							+ "		left join rule rule_svc on svc.rule_id = rule_svc.id "
							+ "	where service.id = "+ id
							+ " and service_svc.id = " + id + "",
							new Object[] {}, new RuleServiceExtractor(type));
			service = serviceElse;
		}
		return service;
	}
	
	public RuleProfile getRuleProfile(String profile_name) {
		RuleProfile profile = new RuleProfile();
		
		profile = (RuleProfile) this.jdbcTemplateRules
				.query("select distinct rule_src.group_name as NAME, GROUP_CONCAT(distinct CONCAT(rule.id, ',', rule.ref) SEPARATOR ';') as USED_IN_RULES "+
					" from rule rule, rule_src rule_src "+
						" where rule.rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and rule_src.rule_id = rule.id and rule_src.group_name = '"+profile_name+"'",
						new Object[] {}, new RuleProfileExtractor());		
		
		return profile;
	}

	public Service getService(String id, String type) {
		Service service = new Service();
		if (type.equals("group")) {
			Service serviceGroup = (Service) this.jdbcTemplateFCR
					.query("select grp.id, grp.name, grp.predefined, GROUP_CONCAT(distinct concat(memberof.id , ',group,', memberof.name) SEPARATOR ';') as USED_IN_GROUPS, '' as USED_IN_RULES, GROUP_CONCAT(distinct concat(service.id , ',service,', service.name) SEPARATOR ';') as SERVICE_MEMBERS,  GROUP_CONCAT(distinct concat(groupo.id , ',group,', groupo.name) SEPARATOR ';') as GROUP_MEMBERS "
							+ "from cat_service_group_member used_in "
							+ "  right join cat_service_group grp on grp.id = used_in.service_id "
							+ "  left join cat_service_group memberof on used_in.service_group_id = memberof.id and used_in.service_type = 'group', "
							+ "    cat_service_group_member service_member "
							+ "  right join cat_service_group service_grp on service_grp.id = service_member.service_group_id "
							+ "  left join cat_service service on service.id = service_member.service_id and service_member.service_type = 'service', "
							+ "    cat_service_group_member group_member "
							+ "  right join cat_service_group group_grp on group_grp.id = group_member.service_group_id "
							+ "  left join cat_service_group groupo on groupo.id = group_member.service_id and group_member.service_type = 'group' "
							+ "where grp.id = "
							+ id
							+ " and service_grp.id = "
							+ id + " and group_grp.id = " + id,
							new Object[] {}, new ServiceExtractor(type));
			service = serviceGroup;
		} else {
			Service svc = (Service) this.jdbcTemplateFCR
					.query("select service.id, service.name, service.predefined, service.protocol as type, service.port, service.confidentiality, service.integrity, GROUP_CONCAT(distinct concat(memberof.id , ',group,', memberof.name) SEPARATOR ';') as USED_IN_GROUPS, '' as USED_IN_RULES "
							+ "from cat_service_group_member used_in "
							+ "  right join cat_service service on service.id = used_in.service_id "
							+ "  left join cat_service_group memberof on used_in.service_group_id = memberof.id and used_in.service_type = 'service' "
							+ "where service.id = " + id, new Object[] {},
							new ServiceExtractor(type));
			service = svc;
		}

		// SEARCH IN WHICH RULES SERVICE IS USED
		String list = (String) this.jdbcTemplateRules
				.queryForObject("select GROUP_CONCAT(distinct concat(rule.id,',', rule.ref) SEPARATOR ';') "
								+ "from service service, service_set service_set, rule_service rule_service, rule rule "
								+ "where service.id = rule_service.service_id and service_set.id = service.sset_id and rule_service.rule_id = rule.id and service_set.import_id = (select max(import_id) from rulebase) and concat('', service.name) = '"
								+ service.getName() + "'", String.class);

		service.setUsedInRules(ExtractorUtils.listRuleToHtml(list));

		return service;
	}

	public be.celsius.fcrweb.objects.Object getObject(String id, String type) {
		be.celsius.fcrweb.objects.Object object = new be.celsius.fcrweb.objects.Object();
		if (type.equals("group")) {
			be.celsius.fcrweb.objects.Object objectGroup = (be.celsius.fcrweb.objects.Object) this.jdbcTemplateFCR
					.query("select grp.id, grp.name, grp.predefined, GROUP_CONCAT(distinct concat(memberof.id , ',group,', memberof.name) SEPARATOR ';') as USED_IN_GROUPS, '' as USED_IN_RULES, GROUP_CONCAT(distinct concat(host.id , ',host,', host.name) SEPARATOR ';') as HOST_MEMBERS,GROUP_CONCAT(distinct concat(subnet.id , ',subnet,', subnet.ip) SEPARATOR ';') as SUBNET_MEMBERS, GROUP_CONCAT(distinct concat(zone.id , ',zone,', zone.name) SEPARATOR ';') as ZONE_MEMBERS,  GROUP_CONCAT(distinct concat(groupo.id , ',group,', groupo.name) SEPARATOR ';') as GROUP_MEMBERS "
							+ "from cat_object_group_member used_in "
							+ "  right join cat_object_group grp on grp.id = used_in.object_id "
							+ "  left join cat_object_group memberof on used_in.object_group_id = memberof.id and used_in.object_type = 'group', "
							+ "    cat_object_group_member host_member "
							+ "  right join cat_object_group host_grp on host_grp.id = host_member.object_group_id "
							+ "  left join cat_host host on host.id = host_member.object_id and host_member.object_type = 'host', "
							+ "    cat_object_group_member subnet_member "
							+ "  right join cat_object_group subnet_grp on subnet_grp.id = subnet_member.object_group_id "
							+ "  left join cat_subnet subnet on subnet.id = subnet_member.object_id and subnet_member.object_type = 'subnet', "
							+ "    cat_object_group_member zone_member "
							+ "  right join cat_object_group zone_grp on zone_grp.id = zone_member.object_group_id "
							+ "  left join cat_zone zone on zone.id = zone_member.object_id and zone_member.object_type = 'zone', "
							+ "    cat_object_group_member group_member "
							+ "  right join cat_object_group group_grp on group_grp.id = group_member.object_group_id "
							+ "  left join cat_object_group groupo on groupo.id = group_member.object_id and group_member.object_type = 'group' "
							+ "where grp.id = " +id+ " and host_grp.id = " +id	+ " and subnet_grp.id = "+ id+ " and zone_grp.id = "+ id+ " and group_grp.id = " + id, new Object[] {},
							new ObjectExtractor(type));
			object = objectGroup;
		} else if (type.equals("host")) {
			be.celsius.fcrweb.objects.Object objectHost = (be.celsius.fcrweb.objects.Object) this.jdbcTemplateFCR
					.query("select host.id, host.name, host.ip as IP_ADDRESS, GROUP_CONCAT(distinct concat(memberof.id , ',group,', memberof.name) SEPARATOR ';') as USED_IN_GROUPS, '' as USED_IN_RULES "
							+ "from cat_object_group_member used_in "
							+ "	right join cat_host host on host.id = used_in.object_id "
							+ "	left join cat_object_group memberof on used_in.object_group_id = memberof.id and used_in.object_type = 'host' "
							+ "where host.id = " + id, new Object[] {},
							new ObjectExtractor(type));
			object = objectHost;
		} else if (type.equals("subnet")) {
			be.celsius.fcrweb.objects.Object objectHost = (be.celsius.fcrweb.objects.Object) this.jdbcTemplateFCR
					.query("select subnet.id, subnet.ip as NAME, subnet.ip as IP_ADDRESS, subnet.mask as NETMASK, GROUP_CONCAT(distinct concat(memberof.id , ',group,', memberof.name) SEPARATOR ';') as USED_IN_GROUPS, '' as USED_IN_RULES "
							+ "from cat_object_group_member used_in "
							+ "	right join cat_subnet subnet on subnet.id = used_in.object_id "
							+ "	left join cat_object_group memberof on used_in.object_group_id = memberof.id and used_in.object_type = 'subnet' "
							+ "where subnet.id = " + id, new Object[] {},
							new ObjectExtractor(type));
			object = objectHost;
		} else if (type.equals("zone")) {
			be.celsius.fcrweb.objects.Object objectHost = (be.celsius.fcrweb.objects.Object) this.jdbcTemplateFCR
					.query("select zone.id, zone.name as NAME, zone.description as DESCRIPTION, GROUP_CONCAT(distinct concat(memberof.id , ',group,', memberof.name) SEPARATOR ';') as USED_IN_GROUPS, '' as USED_IN_RULES "
							+ "from cat_object_group_member used_in "
							+ "	right join cat_zone zone on zone.id = used_in.object_id "
							+ "	left join cat_object_group memberof on used_in.object_group_id = memberof.id and used_in.object_type = 'zone' "
							+ "where zone.id = " + id, new Object[] {},
							new ObjectExtractor(type));
			object = objectHost;
		}

		// SEARCH IN WHICH RULES OBJECT IS USED
		String list = (String) this.jdbcTemplateRules
				.queryForObject(
						"select GROUP_CONCAT(distinct concat(rules.ID,',', rules.REF) SEPARATOR ';') from "
								+ "( "
								+ "select rule.id as ID, rule.ref as REF "
								+ "from object object, object_set object_set, rule_src rule_src, rule rule "
								+ "where object.id = rule_src.object_id and object_set.id = object.oset_id and rule_src.rule_id = rule.id and object_set.import_id = (select max(import_id) from rulebase) and concat('', object.name) = '"
								+ object.getName()
								+ "' "
								+ "union "
								+ "select rule.id as ID, rule.ref as REF "
								+ "from object object, object_set object_set, rule_dst rule_dst, rule rule "
								+ "where object.id = rule_dst.object_id and object_set.id = object.oset_id and rule_dst.rule_id = rule.id and object_set.import_id = (select max(import_id) from rulebase) and concat('', object.name) = '"
								+ object.getName() + "' " + ") rules ",
						String.class);

		object.setUsedInRules(ExtractorUtils.listRuleToHtml(list));

		return object;
	}

	public Profile getProfile(String id) {
		Profile profile = new Profile();

		Profile prof = (Profile) this.jdbcTemplateFCR
				.query("select prof.id as ID, prof.name as NAME, prof.predefined as PREDEFINED, prof.description as DESCRIPTION, prof.responsible_uid as RESPONSIBLEUID from cat_profile prof where prof.id = "
						+ id, new Object[] {}, new ProfileExtractor());
		profile = prof;

		// SEARCH IN WHICH RULES PROFILE IS USED
		String list = (String) this.jdbcTemplateRules
				.queryForObject(
						"select GROUP_CONCAT(distinct concat(rule.id,',', rule.ref) SEPARATOR ';') "
								+ " from rule_src rule_src,rule rule "
								+ " where rule.id = rule_src.rule_id and rule.rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and (rule_src.group_name = '"+ profile.getName() + "' or rule_src.group_name = '("+ profile.getName() + ")')", String.class);

		profile.setUsedInRules(ExtractorUtils.listRuleToHtml(list));

		return profile;
	}

	public String getOrphanRules() {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		List<Map> rowsRules = this.jdbcTemplateRules
				.queryForList("select distinct id as ID, ref as REF from rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and ref is not null");
		@SuppressWarnings("unchecked")
		List<Map> rowsFCR = this.jdbcTemplateFCR
				.queryForList("select distinct concat('-',rule_name,'-') as RULE from link_rule_technical_service");
		String allAssignedRules = "";
		for (Map row : rowsFCR) {
			allAssignedRules += (String) row.get("RULE");
		}

		String orphanRules = (rowsRules.size() - rowsFCR.size())
				+ "orphanNumber" + rowsRules.size() + "totalNumber";
		Boolean first = true;
		for (Map row : rowsRules) {
			if (!(allAssignedRules.indexOf("-" + (String) row.get("REF") + "-") >= 0)) {
				if (first) {
					orphanRules += row.get("ID") + ","
							+ (String) row.get("REF");
					first = false;
				} else {
					orphanRules = orphanRules + ";" + row.get("ID") + ","
							+ (String) row.get("REF");
				}
			}
		}

		return orphanRules;
	}

	public String getRulesWithoutOwner() {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		List<Map> rowsRules = this.jdbcTemplateRules
				.queryForList("select distinct concat(rule.id,',', rule.ref) as ORPHAN from rule rule where  rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase))");
		@SuppressWarnings("unchecked")
		List<Map> rowsFCR = this.jdbcTemplateFCR
				.queryForList("select distinct concat('-',link.rule_name,'-') as RULE from link_rule_technical_service link, used_technical_service ts where ts.owner is null");
		String allAssignedRulesWithoutOwner = "";
		for (Map row : rowsFCR) {
			allAssignedRulesWithoutOwner += (String) row.get("RULE");
		}

		String RulesWithoutOwner = "";
		Boolean first = true;
		for (Map row : rowsRules) {
			if (allAssignedRulesWithoutOwner.indexOf("-"
					+ (String) row.get("REF") + "-") >= 0) {
				if (first) {
					RulesWithoutOwner = row.get("ID") + ","
							+ (String) row.get("REF");
					first = false;
				} else {
					RulesWithoutOwner += ";" + row.get("ID") + ","
							+ (String) row.get("REF");
				}
			}
		}

		return RulesWithoutOwner;
	}

	public String getActionsDelay() {
		String result = "";
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, -6);
		String[] sqlDates = new String[7];
		String[] validationTypes = { "Validation", "Security Validation",
				"Implement" };
		for (int i = 0; i < 6; i++) {
			if (i != 0) {
				calendar.add(Calendar.MONTH, +1);
				result += ",";
			}
			result += "\""
					+ new SimpleDateFormat("MMM yy").format(calendar.getTime())
					+ "\"";
			sqlDates[i] = new SimpleDateFormat("01/MM/yyyy").format(calendar
					.getTime());
		}
		calendar.add(Calendar.MONTH, +1);
		sqlDates[6] = new SimpleDateFormat("01/MM/yyyy").format(calendar
				.getTime());

		for (int i = 0; i < validationTypes.length; i++) {
			String type = validationTypes[i];
			result += ";";
			for (int j = 0; j < sqlDates.length - 1; j++) {
				String fromDate = sqlDates[j];
				String toDate = sqlDates[j + 1];
				if (j != 0) {
					result += ",";
				}
				result += (String) this.jdbcTemplateFCR.queryForObject("select IFNULL(ROUND(AVG(TIMESTAMPDIFF(HOUR, ts_assigned, ts_completed))), 0) from request_task " +
						"where task = '"+ type+ "' and ts_assigned >= STR_TO_DATE('"+ fromDate+ "', '%d/%m/%Y') " +
								" and ts_completed < STR_TO_DATE('"+ toDate + "', '%d/%m/%Y')", String.class);
			}
		}

		return result;
	}

	public String getAllRuleNames() {
		String rulenames = "";
		@SuppressWarnings("unchecked")
		List<Map> rowsRules = this.jdbcTemplateRules
				.queryForList("select ref as REF from rule rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and ref is not null order by ref");
		boolean first = true;
		for (Map row : rowsRules) {
			if (first) {
				first = false;
				rulenames = (String) row.get("REF");
			} else {
				rulenames += "," + (String) row.get("REF");
			}
		}

		return rulenames;
	}

	public ArrayList<RequestSummary> getRejectedFCR() {
		@SuppressWarnings("unchecked")
		ArrayList<RequestSummary> rejectedFCR = (ArrayList<RequestSummary>) this.jdbcTemplateFCR
				.query("select request.ID, request.requestor_uid, request.form_mode, task.ts_assigned as date, request.status, CONCAT(request.type_description, ' / ', request.scope) as detail from request request LEFT OUTER JOIN request_task task ON request.id = task.req_id and task.next_task = request.status where request.status = 'Rejected' order by request.id desc",
						new Object[] {}, new RequestSummaryExtractor());
		return rejectedFCR;
	}

	public String[][] getFCRUsers() {
		@SuppressWarnings("unchecked")
		List<Map> rowsFCR = this.jdbcTemplateFCR
				.queryForList("select requestor_uid as UID, count(*) as COUNT from request where requestor_uid <> '' group by requestor_uid");
		String[][] result = new String[rowsFCR.size()][2];
		int i = 0;
		for (Map row : rowsFCR) {
			result[i][0] = (String) row.get("UID");
			result[i][1] = "" + (Long) row.get("COUNT");
			i++;
		}
		return result;
	}
	
	public String getTechnicalServiceID(String ts)
	{
		String ts_id = (String) this.jdbcTemplateFCR.queryForObject("select IFNULL(min(id), -1) from used_technical_service where name = '"	+ ts + "'", String.class);

		if (ts_id.equals("-1")) {
			String cat_ts_id = (String) this.jdbcTemplateFCR.queryForObject(
					"select IFNULL(min(id), -1) from cat_technical_service where name = '"
							+ ts + "'", String.class);

			if (cat_ts_id.equals("-1")) {
				// TECHNICAL_SERVICE INFORMATION MUST BE EXTRACTED FROM CMDB AND
				// COPIED IN USED_TECHNICAL_SERVICE TABLE
			} else {
				this.jdbcTemplateFCR
						.update("INSERT INTO used_technical_service (name, owner, description) SELECT name, owner, description from cat_technical_service where name = '"
								+ ts + "'");
			}
			ts_id = (String) this.jdbcTemplateFCR.queryForObject(
					"select IFNULL(min(id), -1) from used_technical_service where name = '"
							+ ts + "'", String.class);
		}
		
		return ts_id;
	}

	public ArrayList<RuleConflictObjects> updateTechnicalServiceRules(String ts, String[] rules, Boolean forceAssign) 
	{
		String ts_id = getTechnicalServiceID(ts);		
		
		ArrayList<RuleConflictObjects> conflicts = checkIfObjectsAlreadyAssignedToTechnicalService(rules, ts_id);
		
		if (!forceAssign && conflicts.size() > 0)
		{
			//CHECKING FOR WHICH RULES THERE IS NO CONFLICT AND ASSIGNING TECHNICAL SERVICE TO THOSE RULES. FOR THE OTHERS, CONFLICT MESSAGE IS DISPLAYED
			for (String ruleN: rules)
			{
				Boolean hasConflicts = false;
				for (RuleConflictObjects conflict: conflicts)
				{
					if(ruleN.equals(conflict.getRuleName()))
					{
						hasConflicts = true;
					}					
				}
				if (!hasConflicts)
				{
					assignTechnicalServiceToRuleAndObjectsAndServices(ruleN, ts_id);
				}
			}
		}
		
		
		if (conflicts.size() == 0 || forceAssign)
		{
			this.jdbcTemplateFCR
					.update("DELETE FROM link_rule_technical_service WHERE technical_service_id = "+ ts_id + "");
			for (String rule : rules) 
			{
				assignTechnicalServiceToRuleAndObjectsAndServices(rule, ts_id);
			}
			conflicts = new ArrayList<RuleConflictObjects>(); //So if forceAssign is true, admincontroller will send ok in place of the conflicts list
		}
		return conflicts;
	}
		
	public ArrayList<RuleConflictObjects> assignTechnicalServiceToRule(String ts, String rule, Boolean forceAssign) 
	{
		String ts_id = getTechnicalServiceID(ts);
		String[] tab = {rule};
		
		ArrayList<RuleConflictObjects> conflicts = checkIfObjectsAlreadyAssignedToTechnicalService(tab, ts_id);
		
		if (conflicts.size() == 0 || forceAssign)
		{
			assignTechnicalServiceToRuleAndObjectsAndServices(rule, ts_id);
			conflicts = new ArrayList<RuleConflictObjects>(); //So if forceAssign is true, admincontroller will send ok in place of the conflicts list
		}
		return conflicts;
	}
	
	public void assignTechnicalServiceToRuleAndObjectsAndServices(String rule, String ts_id)
	{
		if (!rule.equals(""))
		{
			try
			{
				this.jdbcTemplateFCR
					.update("INSERT INTO link_rule_technical_service (rule_name, technical_service_id) VALUES ('"+ rule + "' ," + ts_id + ")");
			}
			catch (Exception e)
			{
				// RULE IS ALREADY ASSIGNED, BUT MUST BE CHANGED
				this.jdbcTemplateFCR
				.update("update link_rule_technical_service set technical_service_id = "+ts_id+" where rule_name = '"+rule+"'");
			}
			String[] objectNames = getRuleObjectGroupNames(rule);
			for (String objectName: objectNames)
			{				
				this.jdbcTemplateFCR.queryForObject("select f_assign_technical_service_to_cat_object_group('"+objectName+"',"+ts_id+")", String.class);
			}
			
			String[] serviceNames = getRuleServiceNames(rule);
			for (String serviceName: serviceNames)
			{
				this.jdbcTemplateFCR
				.update("UPDATE cat_service set technical_service_id = " + ts_id + " where name = '"+serviceName+"' and predefined = 0");
			}
			
			String[] serviceGroupNames = getRuleServiceGroupNames(rule);
			for (String serviceGroupName: serviceGroupNames)
			{
				this.jdbcTemplateFCR.queryForObject("select f_assign_technical_service_to_cat_service_group('"+serviceGroupName+"',"+ts_id+")", String.class);
			}
		}
	}
	
	public String[] getRuleObjectGroupNames(String rule) 
	{
		@SuppressWarnings("unchecked")
		List<Map> rowsRules = this.jdbcTemplateRules
				.queryForList("select distinct object_src.name "+
						" from rule rule, object object_src, rule_src rule_src "+
						" where rule.rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and rule.ref = '"+rule+"' "+
						" and rule_src.rule_id = rule.id and object_src.id = rule_src.object_id and object_src.type = 'group' "+
						" union "+
						" select distinct object_dst.name "+
						" from rule rule, object object_dst, rule_dst rule_dst "+
						" where rule.rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and rule.ref = '"+rule+"' "+
						" and rule_dst.rule_id = rule.id and object_dst.id = rule_dst.object_id and object_dst.type = 'group'");
		String[] result = new String[rowsRules.size()];
		int i = 0;
		for (Map row : rowsRules) {
			result[i] = (String) row.get("NAME");
			i++;
		}
		return result;
	}
	
	public String[] getRuleServiceNames(String rule) 
	{
		@SuppressWarnings("unchecked")
		List<Map> rowsRules = this.jdbcTemplateRules
		.queryForList("select distinct service.name "+
				" from rule rule, service service, rule_service rule_service "+
				" where rule.rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and rule.ref = '"+rule+"' "+
				" and rule_service.rule_id = rule.id and service.id = rule_service.service_id and service.type <> 'group'");
		String[] result = new String[rowsRules.size()];
		int i = 0;
		for (Map row : rowsRules) {
			result[i] = (String) row.get("NAME");
			i++;
		}
		return result;
	}
	
	public String[] getRuleServiceGroupNames(String rule) 
	{
		@SuppressWarnings("unchecked")
		List<Map> rowsRules = this.jdbcTemplateRules
				.queryForList("select distinct service.name "+
						" from rule rule, service service, rule_service rule_service "+
						" where rule.rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and rule.ref = '"+rule+"' "+
						" and rule_service.rule_id = rule.id and service.id = rule_service.service_id and service.type = 'group'");
		String[] result = new String[rowsRules.size()];
		int i = 0;
		for (Map row : rowsRules) {
			result[i] = (String) row.get("NAME");
			i++;
		}
		return result;
	}
	
	public ArrayList<RuleConflictObjects> checkIfObjectsAlreadyAssignedToTechnicalService(String[] rules, String ts_id)
	{
		ArrayList<RuleConflictObjects> list = new ArrayList<RuleConflictObjects>();
		
		for (String rule: rules)
		{	
			RuleConflictObjects obj = new RuleConflictObjects();
			obj.setRuleName(rule);
			//CHECK IF RULE IS ALREADY ASSIGNED
			try
			{
				String rule_id = (String) this.jdbcTemplateRules.queryForObject("select id "+
						" from rule "+
						" where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase)) and ref = '"+rule+"'", String.class);
				
				String result = (String) this.jdbcTemplateFCR.queryForObject("select concat('"+rule_id+"', ',', link.rule_name,',', ts.name) "+
						" from link_rule_technical_service link, used_technical_service ts "+
						" where link.technical_service_id = ts.id and link.rule_name = '"+rule+"' and link.technical_service_id <> "+ts_id, String.class);
				String[] serv_split = result.split(",");
				String[] temp = {serv_split[0],serv_split[1],"Rule",serv_split[2]};
				obj.addConflictObject(temp);
			}
			catch (Exception e)
			{
				//HAPPENS IF QUERYFOROBJECT RETURNS NO RESULT
			}
			// END RULE CHECK
			
			
			String[] objectNames = getRuleObjectGroupNames(rule);
			for (String objectName: objectNames)
			{				
				try
				{
					@SuppressWarnings("unchecked")
					String result = (String) this.jdbcTemplateFCR.queryForObject("select f_get_cat_object_groups_with_bad_technical_service('"+objectName+"', "+ts_id+")", String.class);
					if (!result.equals(""))
					{
						String[] objects = result.split(";");
						for (int i = 0; i < objects.length; i++) //objects.length -1 because the String ends with a ;
						{
							String[] obj_split = objects[i].split(",");
							String[] temp = {obj_split[0],obj_split[1],"Object Group",obj_split[2]};
							obj.addConflictObject(temp);
						}
					}
				}
				catch (Exception e)
				{
					//HAPPENS IF QUERYFOROBJECT RETURNS NO RESULT
				}
			}
			
			String[] serviceNames = getRuleServiceNames(rule);
			for (String serviceName: serviceNames)
			{
				try
				{
					String result = (String) this.jdbcTemplateFCR.queryForObject("select concat(service.id, ',', service.name,',', ts.name) from cat_service service, used_technical_service ts where service.technical_service_id = ts.id and service.name = '"+serviceName+"' and service.technical_service_id <> "+ts_id, String.class);
					String[] serv_split = result.split(",");
					String[] temp = {serv_split[0],serv_split[1],"Service",serv_split[2]};
					obj.addConflictObject(temp);
				}
				catch (Exception e)
				{
					//HAPPENS IF QUERYFOROBJECT RETURNS NO RESULT
				}
			}
			
			String[] serviceGroupNames = getRuleServiceGroupNames(rule);
			for (String serviceGroupName: serviceGroupNames)
			{
				try
				{
					@SuppressWarnings("unchecked")
					String result = (String) this.jdbcTemplateFCR.queryForObject("select f_get_cat_services_with_bad_technical_service('"+serviceGroupName+"', "+ts_id+")", String.class);
					if (!result.equals(""))
					{
						String[] objects = result.split(";");
						for (int i = 0; i < objects.length-1; i++) //objects.length -1 because the String ends with a ;
						{
							String[] serv_split = objects[i].split(",");
							String service_type = "Service";
							if (serv_split[0].equals("group"))
							{
								service_type = "Service Group";
							}
							
							String[] temp = {serv_split[1],serv_split[2],service_type,serv_split[3]};
							obj.addConflictObject(temp);
						}
					}
				}
				catch (Exception e)
				{
					//HAPPENS IF QUERYFOROBJECT RETURNS NO RESULT
				}
			}
			if (obj.getConflictObjects().size() > 0)
			{
				list.add(obj);
			}
		}
		
		
		return list;
	}
	
	public Boolean setObjectGroupAsPredefined(String object_id)
	{
		Boolean ok = true;
		String changeAllowed = (String) this.jdbcTemplateFCR.queryForObject("select f_cat_object_group_is_predefinable(id) from cat_object_group where id = "+object_id, String.class);
		
		if (changeAllowed.equals("0"))
		{
			ok = false;
		}
		else
		{
			ok = true;
			this.jdbcTemplateFCR.update("update cat_object_group set predefined = 1 where id = "+object_id);
		}		
		
		return ok;
	}
	
	public Boolean setServiceAsPredefined(String service_id, String service_type)
	{
		Boolean ok = true;
		if (service_type.equals("group"))
		{
			String changeAllowed = (String) this.jdbcTemplateFCR.queryForObject("select f_cat_service_group_is_predefinable(id) from cat_service_group where id = "+service_id, String.class);
			if (changeAllowed.equals("0"))
			{
				ok = false;
			}
			else
			{
				ok = true;
				this.jdbcTemplateFCR.update("update cat_service_group set predefined = 1 where id = "+service_id);
			}
		}
		else
		{
			ok = true;
			this.jdbcTemplateFCR.update("update cat_service set predefined = 1 where id = "+service_id);
		}
		return ok;
	}
	
	public void setProfileAsPredefined(String profile_id)
	{
		this.jdbcTemplateFCR.update("update cat_profile set predefined = 1 where id = "+profile_id);
	}
	
	public String getApplicationParameter(String param_name)
	{
		String param_value = (String) this.jdbcTemplateFCR.queryForObject("select val from configs where param = '"+param_name+"'", String.class);
		return param_value;
	}
	
	public ArrayList<String[]> getApplicationParameters()
	{
		ArrayList<String[]> parameters = new ArrayList<String[]>();
		
		@SuppressWarnings("unchecked")
		List<Map> rowsFCR = this.jdbcTemplateFCR.queryForList("select param, val from configs");
			
		for (Map row : rowsFCR) 
		{			  			
			String[] temp = {(String) row.get("param"), (String) row.get("val")};
			parameters.add(temp);
		}
		
		return parameters;
	}

	public ArrayList<String[]> getPrivateRanges()
	{
		ArrayList<String[]> ranges = new ArrayList<String[]>();

		@SuppressWarnings("unchecked")
		List<Map> rowsFCR = this.jdbcTemplateFCR.queryForList("select range_start, range_end from private_ranges");

		for (Map row : rowsFCR) 
		{
			String[] temp = {(String) row.get("range_start"), (String) row.get("range_end")};
			ranges.add(temp);
		}

		return ranges;
	}
	
	public void resetPrivateRanges()
	{
		this.jdbcTemplateFCR.update("delete from private_ranges");
	}
	
	public void addPrivateRange(String rangeStart, String rangeEnd)
	{
		this.jdbcTemplateFCR.update("insert into private_ranges (range_start, range_end) values ('"+rangeStart+"','"+rangeEnd+"')");
	}	

	public void updateApplicationParameter(String paramName, String paramValue)
	{
		this.jdbcTemplateFCR.update("update configs set val = '"+paramValue+"' where param = '"+paramName+"'");
	}
	
	public FWObjectNamesNotCompliant getObjectNamesNotCompliant()
	{
		FWObjectNamesNotCompliant objects = new FWObjectNamesNotCompliant();

		@SuppressWarnings("unchecked")
		ArrayList<String[]> objectGroups = (ArrayList<String[]>) this.jdbcTemplateRules
				.query("select distinct object.id, object.name from object object, rulebase rb where rb.import_id = (select max(import_id) from rulebase) and object.oset_id = rb.oset_id and (object.name not REGEXP '^(mes-|ext-|olu-|mob-)?[A-Za-z_]+-(Prod-|nonProd-)?grp$' and object.name not REGEXP '^(mes-|ext-|olu-|mob-)?zone-[A-Za-z_]+-[A-Za-z_]+$' and object.name not REGEXP '^(mes-|ext-|olu-|mob-)?plane-[A-Za-z_]+$') and type = 'group'",
						new Object[] {}, new IdAndNameExtractor());
		objects.setNetworkObjectGroups(objectGroups);

		@SuppressWarnings("unchecked")
		ArrayList<String[]> hosts = (ArrayList<String[]>) this.jdbcTemplateRules
				.query("select distinct object.id, object.name from object object, rulebase rb where rb.import_id = (select max(import_id) from rulebase) and object.oset_id = rb.oset_id and object.name not REGEXP '^(mes-|ext-|olu-|mob-)?host-[A-Za-z_]+-(Prod-|nonProd-)?[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$' and type = 'host'",
						new Object[] {}, new IdAndNameExtractor());
		objects.setHosts(hosts);

		@SuppressWarnings("unchecked")
		ArrayList<String[]> subnets = (ArrayList<String[]>) this.jdbcTemplateRules
				.query("select distinct object.id, object.name from object object, rulebase rb where rb.import_id = (select max(import_id) from rulebase) and object.oset_id = rb.oset_id and object.name not REGEXP '^(mes-|ext-|olu-|mob-)?net-[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}-[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$' and type = 'network'",
						new Object[] {}, new IdAndNameExtractor());
		objects.setSubnets(subnets);

		@SuppressWarnings("unchecked")
		ArrayList<String[]> serviceGroups = (ArrayList<String[]>) this.jdbcTemplateRules
				.query("select distinct service.id, service.name from service service, rulebase rb where rb.import_id = (select max(import_id) from rulebase) and service.sset_id = rb.sset_id and service.name not REGEXP '^[A-Za-z_]+-grp$' and type = 'group'",
						new Object[] {}, new IdAndNameExtractor());
		objects.setServiceGroups(serviceGroups);

		@SuppressWarnings("unchecked")
		ArrayList<String[]> services = (ArrayList<String[]>) this.jdbcTemplateRules
				.query("select distinct service.id, service.name from service service, rulebase rb where rb.import_id = (select max(import_id) from rulebase) and service.sset_id = rb.sset_id and service.name not REGEXP '^[A-Za-z]+-[A-Za-z_]+-[0-9]{1,5}$' and type <> 'group'",
						new Object[] {}, new IdAndNameExtractor());
		objects.setServices(services);

		@SuppressWarnings("unchecked")
		ArrayList<String> userGroups = (ArrayList<String>) this.jdbcTemplateRules
				.query("select distinct group_name as NAME from rule_src where rule_id in (select id from (select rule.* from rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase))) rule) and group_name <> '' and group_name not REGEXP '^(mes-|ext-|olu-|mob-)?Usr[A-Za-z_]+grp$'",
						new Object[] {}, new NameExtractor());
		objects.setUserGroups(userGroups);

		return objects;
	}
	
	public ArrayList<ObjectInconsistencies> getFWObjectInconsistencies()
	{
		ArrayList<ObjectInconsistencies> areNotEqual = new ArrayList<ObjectInconsistencies>();
		@SuppressWarnings("unchecked")
		ArrayList<String> names = (ArrayList<String>) this.jdbcTemplateFCR
				.query("select distinct object_name as NAME from control_object_inconsistencies", new Object[] {}, new NameExtractor());
		
		for (String name: names)
		{			
			ObjectInconsistencies inconsistencies = (ObjectInconsistencies) this.jdbcTemplateFCR
					.query("select object_id as ID, object_name as NAME, object_type as TYPE, object_ip as IP_ADDRESS, object_netmask as NETMASK, fw_names as fw_names from control_object_inconsistencies where object_name = '" + name+"'" , new Object[] {}, new ObjectInconsistenciesExtractor());
			areNotEqual.add(inconsistencies);			
		}
		return areNotEqual;
	}
	
	public ArrayList<ServiceInconsistencies> getFWServiceInconsistencies()
	{
		ArrayList<ServiceInconsistencies> areNotEqual = new ArrayList<ServiceInconsistencies>();
		@SuppressWarnings("unchecked")
		ArrayList<String> names = (ArrayList<String>) this.jdbcTemplateFCR
				.query("select distinct service_name as NAME from control_service_inconsistencies", new Object[] {}, new NameExtractor());
		
		for (String name: names)
		{
			ServiceInconsistencies inconsistencies = (ServiceInconsistencies) this.jdbcTemplateFCR
					.query("select service_id as ID, service_name as NAME, service_type as TYPE, service_port as PORTS, fw_names from control_service_inconsistencies where service_name = '" + name+"'", new Object[] {}, new ServiceInconsistenciesExtractor());
			areNotEqual.add(inconsistencies);
		}
		return areNotEqual;
	}
	
	public int getObjectsNumber()
	{
		return Integer.parseInt((String) this.jdbcTemplateRules.queryForObject("select count(*) from (select distinct object.id from object object, rulebase rb where rb.import_id = (select max(import_id) from rulebase) and object.oset_id = rb.oset_id) objects", String.class));
	}
	
	public int getServicesNumber()
	{
		return Integer.parseInt((String) this.jdbcTemplateRules.queryForObject("select count(*) from (select distinct service.id from service service, rulebase rb where rb.import_id = (select max(import_id) from rulebase) and service.sset_id = rb.sset_id) services", String.class));
	}

	public int getProfilesNumber()
	{
		return Integer.parseInt((String) this.jdbcTemplateRules.queryForObject("select count(*) from (select distinct group_name from rule_src where rule_id in (select rule.id from rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase))) and group_name <> '') profiles", String.class));
	}
	
	public HostDefinitionProblemList getHostDefinitionProblems()
	{
		HostDefinitionProblemList problems = new HostDefinitionProblemList();
		ArrayList<HostDefinitionProblem> notInCMDB = new ArrayList<HostDefinitionProblem>();
		ArrayList<HostDefinitionProblem> notInFW = new ArrayList<HostDefinitionProblem>();
		
		@SuppressWarnings("unchecked")
		List<Map> rowsRules = this.jdbcTemplateFCR.queryForList("select host_id as ID, host_name as NAME, host_ip as IP from control_host_definition where notInCmdb = 1");
		@SuppressWarnings("unchecked")
		List<Map> rowsFCR = this.jdbcTemplateFCR.queryForList("select host_id as ID, host_name as NAME, host_ip as IP from control_host_definition where notInFW = 1");
		
		for (Map rowRules : rowsRules) 
		{
			HostDefinitionProblem problem = new HostDefinitionProblem();
			problem.setId(((Long) rowRules.get("ID")));
			problem.setName((String) rowRules.get("NAME"));
			problem.setIp((String) rowRules.get("IP"));
			notInCMDB.add(problem);			
		}
		for (Map rowFCR : rowsFCR) 
		{
			HostDefinitionProblem problem = new HostDefinitionProblem();
			problem.setId((Long) rowFCR.get("ID"));
			problem.setName((String) rowFCR.get("NAME"));
			problem.setIp((String) rowFCR.get("IP"));
			notInFW.add(problem);
		}
		problems.setNotExistingCMDB(notInCMDB);
		problems.setNotExistingFW(notInFW);
		return problems;
	}
	
	public String getLocalGlobalObjectGroup(String objectName)
	{
		String type = "none";
		try
		{
			Object typ = this.jdbcTemplateFCR.queryForObject("select predefined from cat_object_group where firewall_name = '"+objectName+"'", String.class);
			if (typ != null)
			{
				type = (String) typ;
				if (type.equals("1"))
				{
					type = "global";
				}
				else
				{
					type = "local";
				}
			}
		}
		catch (Exception e)
		{}
		return type;
	}
	
	public ArrayList<ObjectSummary> getPreapprovedObjects(int[] ids) 
	{
		if (ids.length > 0)
		{
			String all_ids = "";
			for (int i = 0; i < ids.length; i++)
			{
				if (i == 0)
				{
					all_ids = ""+ids[0];
				}
				else
				{
					all_ids += ","+ids[i];
				}
			}
			
			@SuppressWarnings("unchecked")
			ArrayList<ObjectSummary> objects = (ArrayList<ObjectSummary>) this.jdbcTemplateFCR.query("select id as ID, name as NAME, description as DESCRIPTION, '' as TECHNICAL_SERVICE from cat_preapproved where id in ("+all_ids+")",
							new Object[] {}, new ObjectSummaryExtractor());
					
			return objects;
		}
		else
			return new ArrayList<ObjectSummary>();
	}
	
	public String getPreapprovedObjectIds(String id)
	{
		String ids = "";
		try
		{
			ids = (String) this.jdbcTemplateFCR.queryForObject("select group_concat(obj.id SEPARATOR ',') from cat_preapproved_group preapproved, cat_object_group obj where preapproved.id_cpa = "+id+" and obj.name = preapproved.name", String.class);
		}
		catch(Exception e)
		{
			ids = "none";
		}
		return ids;
	}
	
	public String getPreapprovedObjectechnicalService(String id)
	{
		String ts = "";
		try
		{
			ts = (String) this.jdbcTemplateFCR.queryForObject("select ts.name from cat_object_group obj, used_technical_service ts where obj.technical_service_id = ts.id and obj.id = " + id, String.class);
		}
		catch(Exception e)
		{
			ts = "error";
		}
		return ts;
	}
	
	public int[] getPreapprovedAllowedIds(String profiles)
	{
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		@SuppressWarnings("unchecked")
		List<Map> rows = this.jdbcTemplateFCR.queryForList("select id_cpa as ID, ldap_group as LDAP_GROUP from cat_preapproved_requestor");
		for (Map row : rows)
		{
			int id = (Integer) row.get("ID");
			String ldap_group = (String) row.get("LDAP_GROUP");
			
			if (profiles.contains(ldap_group.toLowerCase()))
			{
				addToList(id, list);
			}
		}
		int[] toreturn = new int[list.size()];		
		for (int i = 0; i<list.size(); i++)
		{
			toreturn[i] = list.get(i);
		}
		return toreturn;
	}
	
	private ArrayList<Integer> addToList(int id, ArrayList<Integer> list)
	{
		Boolean exists = false;
		for (int t: list)
		{
			if (t == id)
			{
				exists = true;
			}
		}
		if (!exists)
		{
			list.add(id);
		}
		return list;
	}
	
	public ArrayList<TechnicalServiceRevalidationData> getTechnicalServiceRevalidationData()
	{
		ArrayList<TechnicalServiceRevalidationData> data = new ArrayList<TechnicalServiceRevalidationData>();
		List<Map> rows = this.jdbcTemplateFCR.queryForList("select name, owner, reval_last_update, reval_status, reval_month from cat_technical_service");
		for (Map row : rows)
		{
			TechnicalServiceRevalidationData tsr = new TechnicalServiceRevalidationData();
			String name = (String) row.get("name");
			String owner = (String) row.get("owner");
			String last_reval = "";
			try
			{
				last_reval = ((java.sql.Date) row.get("reval_last_update")).toString();
			}
			catch(Exception e)
			{
				last_reval = "Not yet revalidated";
			}
			String reval_status = (String) row.get("reval_status");
			String reval_month = (String) row.get("reval_month");
			tsr.setTechnicalService(name);
			tsr.setOwner(owner);
			tsr.setLastRevalidation(last_reval);
			tsr.setStatus(reval_status);
			tsr.setRevalidationMonth(reval_month);
			data.add(tsr);
		}
		return data;
	}
	
	public Boolean ruleExists(String ruleName)
	{		
		try
		{
			String r = (String) this.jdbcTemplateRules.queryForObject("select rule.ref as NAME from rule where rule.ref = '"+ruleName+"' and rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase))", String.class);
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}
	
	public Boolean technicalServiceExists(String ts)
	{
		try
		{
			String r = (String) this.jdbcTemplateFCR.queryForObject("select distinct name as NAME from cat_technical_service where name = '"+ts+"' order by name", String.class);
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}
}
