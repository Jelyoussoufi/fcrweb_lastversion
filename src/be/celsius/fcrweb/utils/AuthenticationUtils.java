package be.celsius.fcrweb.utils;

import javax.servlet.http.HttpServletRequest;

import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.util.Config;
import be.celsius.util.Ldap;
import be.celsius.util.Util;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Uset;
import be.celsius.util.datastructure.Umap.KVP;

public class AuthenticationUtils 
{	
	public static String getUserId(HttpServletRequest request)
	{
		if (!Config.atCelsius)
		{
			return request.getHeader("SM_USER");
		}
		else
		{
			return "jvoisin";
		}
	}
	
	public static String getUserProfiles(String login, HttpServletRequest request)
	{
		if (!Config.atCelsius)
		{			
			return request.getHeader("memberofgroups").toLowerCase();
		}
		else
		{
			return "SecTools - FCR Web - Security Officers^SecTools - FCR Web - Administrators".toLowerCase();
		}
	}
	
	public static Boolean isCompleteSearchAllowed(String profiles)
	{
		if (hasAdministratorProfile(profiles) || hasSecurityOfficerProfile(profiles))
		{
			return true;
		}
		return false;
	}	
	
	public static Boolean hasAdministratorProfile(String profiles)
	{
		if (profiles.contains("SecTools - FCR Web - Administrators".toLowerCase()))
		{
			return true;
		}
		return false;		
	}
	
	public static Boolean hasSecurityOfficerProfile(String profiles)
	{
		if (profiles.contains("SecTools - FCR Web - Security Officers".toLowerCase()))
		{
			return true;
		}
		return false;		
	}

	/* OBSOLETE */
	private static String getLdapGroup(String login, MySqlAccessor sqlAccessor)
	{
		String user_group ="";
		if (getAdministrators(sqlAccessor).contains(login))
			user_group += UtilStr.isEmpty(user_group) ? "Administrators": ",Administrators";
		if (getSecurity_Officers(sqlAccessor).contains(login))
			user_group += UtilStr.isEmpty(user_group) ? "Security_Officers": ",Security_Officers";
		if (getFW_Experts(sqlAccessor).contains(login))
			user_group += UtilStr.isEmpty(user_group) ? "FW_Experts": ",FW_Experts";
		
		return user_group;
	}

	/* OBSOLETE */
	private static Uset getLdapLogins(String dn, String filter, String attr, MySqlAccessor sqlAccessor)
	{
		Ldap ldap = new Ldap();
		ldap.resetConnectionConfiguration();
		ldap.setConnectionConfiguration
		(
			sqlAccessor.getApplicationParameter("users-ldap-host"),
			Util.getInteger(sqlAccessor.getApplicationParameter("users-ldap-port")),
			sqlAccessor.getApplicationParameter("users-ldap-user"),
			sqlAccessor.getApplicationParameter("users-ldap-pwd")
		);
		
		Uset users = new Uset();
		try
		{
			String[] attrs 			= {attr};			
			BasketBean ldapResults 	= ldap.getLdapData(dn, Ldap.SUB, filter, attrs).toBasket();

			for (LeafBean ldapE : ldapResults.getBasket())
			{
				BoxBean entry = ldapE.toBox();
				if (!entry.get(attr).isNullSeed())
				{
					try {
						for (LeafBean leaf : entry.get(attr).toBasket().getBasket())
						{
							String login = leaf.toSeed().getSeed();
							if (!UtilStr.isEmpty(login))
							users.push(login);
						}
					}catch(Exception e){};
				}
			}
		}
		catch(Exception e){UtilLog.printException(e);users = new Uset();}

		return users;
	}
	
	/* OBSOLETE */
	private static Uset getLdapGroupUsers(String group_filter, MySqlAccessor sqlAccessor)
	{
		Uset members = getLdapLogins
		(
			Config.atCelsius ? "ou=groups,ou=staff,o=mobistar.be" : "ou=Applications Groups,ou=groups,ou=staff,o=mobistar.be", 
			"cn=SecTools - FCR Web - " + group_filter,
			"uniqueMember",
			sqlAccessor
		);
		
		Uset logins = new Uset();
		
		for 
		(
			String member :	members
		)
		{
			logins.push(new Umap().setKVP(KVP.KVP_LDAP).mapsterize(member).get("uid"));
		}
		UtilLog.out("flatten members:" + logins.toJson().toString());
		return logins;
	}
	
	/* OBSOLETE */
	private static Uset getLdapGroupUsersGeneric(String ldap_grp, MySqlAccessor sqlAccessor)
	{		
		Uset logins = new Uset();
		try
		{
			String filter = ldap_grp.substring(0,ldap_grp.indexOf(','));
			String dn = ldap_grp.substring(ldap_grp.indexOf(',') + 1);
			
			Uset members = getLdapLogins
			(
				dn, 
				filter,
				"uniqueMember",
				sqlAccessor
			);
			
			for 
			(
				String member :	members
			)
			{
				logins.push(new Umap().setKVP(KVP.KVP_LDAP).mapsterize(member).get("uid"));
			}
			UtilLog.out("flatten members:" + logins.toJson().toString());
		}
		catch (Exception e)
		{}
		return logins;
	}
	
	/* OBSOLETE */
	private static Uset getAdministrators(MySqlAccessor sqlAccessor){return getLdapGroupUsers("Administrators", sqlAccessor);}
	private static Uset getSecurity_Officers(MySqlAccessor sqlAccessor){return getLdapGroupUsers("Security Officers", sqlAccessor);}
	private static Uset getFW_Experts(MySqlAccessor sqlAccessor){return getLdapGroupUsers("FW Experts", sqlAccessor);}
	
	/* OBSOLETE */
	public static Boolean isMemberOfLdapGroup(String group_name, String uid, MySqlAccessor sqlAccessor)
	{
		Uset logins = getLdapGroupUsersGeneric(group_name, sqlAccessor);
		for (String log: logins)
		{
			if (log.equals(uid))
			{
				return true;
			}
		}
		
		return false;
	}	
}
