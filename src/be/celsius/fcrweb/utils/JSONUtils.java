package be.celsius.fcrweb.utils;

import java.util.ArrayList;

import be.celsius.fcrweb.objects.ChangeLogRuleSummary;
import be.celsius.fcrweb.objects.ObjectSummary;
import be.celsius.fcrweb.objects.ProfileSummary;
import be.celsius.fcrweb.objects.RequestSummary;
import be.celsius.fcrweb.objects.RuleConflictObjects;
import be.celsius.fcrweb.objects.RuleSummary;
import be.celsius.fcrweb.objects.ServiceGroupSummary;
import be.celsius.fcrweb.objects.ServiceSummary;
import be.celsius.fcrweb.objects.ServicesSearchData;

public class JSONUtils 
{

	public static String requestListToJSON(ArrayList<RequestSummary> list)
	{
		String json = "{\"requestSummary\": [";
		Boolean first = true;
		for (RequestSummary req: list)
		{
			if (first)
			{
				json += req.toJson();
				first = false;
			}
			else
			{
				json += "," + req.toJson();
			}
		}
		json += "] }";
		return json;
	}
	
	public static String ruleListToJSON(ArrayList<RuleSummary> list)
	{
		String json = "{\"ruleSummary\": [";
		Boolean first = true;
		for (RuleSummary req: list)
		{
			if (first)
			{
				json += req.toJson();
				first = false;
			}
			else
			{
				json += "," + req.toJson();
			}
		}
		json += "] }";
		return json;
	}
	
	public static String ruleChangeLogListToJSON(ArrayList<ChangeLogRuleSummary> list)
	{
		String json = "{\"changeLogSummary\": [";
		Boolean first = true;
		for (ChangeLogRuleSummary req: list)
		{
			if (first)
			{
				json += req.toJson();
				first = false;
			}
			else
			{
				json += "," + req.toJson();
			}
		}
		json += "] }";
		return json;
	}
	
	public static String objectListToJSON(ArrayList<ObjectSummary> list)
	{
		String json = "{\"objectSummary\": [";
		Boolean first = true;
		for (ObjectSummary req: list)
		{
			if (first)
			{
				json += req.toJson();
				first = false;
			}
			else
			{
				json += "," + req.toJson();
			}
		}
		json += "] }";
		return json;
	}
	
	public static String serviceSearchDataToJSON(ServicesSearchData data)
	{
		String json = "{\"serviceSummary\": [";
		Boolean first = true;
		ArrayList<ServiceSummary> list = data.getServices();
		for (ServiceSummary req: list)
		{
			if (first)
			{
				json += req.toJson();
				first = false;
			}
			else
			{
				json += "," + req.toJson();
			}
		}
		json += "], \"serviceGroupSummary\": [";
		first = true;
		ArrayList<ServiceGroupSummary> list2 = data.getServiceGroups();
		for (ServiceGroupSummary req: list2)
		{
			if (first)
			{
				json += req.toJson();
				first = false;
			}
			else
			{
				json += "," + req.toJson();
			}
		}
		json += "] }";
		return json;
	}
		
	public static String profileListToJSON(ArrayList<ProfileSummary> list)
	{
		String json = "{\"profileSummary\": [";
		Boolean first = true;
		for (ProfileSummary req: list)
		{
			if (first)
			{
				json += req.toJson();
				first = false;
			}
			else
			{
				json += "," + req.toJson();
			}
		}
		json += "] }";
		return json;
	}
	
	public static String AssignedRulesToJson(ArrayList<String[]> assigned, String allRules)
	{
		String json = "{\"assignedRules\": \"";
		
		Boolean first = true;
		for (String[] rule: assigned)
		{
			if (first)
			{
				json += rule[1].replace("'", "");
				first = false;
			}
			else
			{
				json += "," + rule[1].replace("'", "");
			}
		}
		json += "\",\"unassignedRules\":\"";
		first = true;
		String[] unassigned = allRules.split(",");
		for (String rule: unassigned)
		{
			Boolean alreadyExists = false;
			for (String ruleAssigned: assigned.get(0)[1].replace("'", "").split(","))
			{
				if (rule.equals(ruleAssigned))
				{
					alreadyExists = true;
				}
			}
			if (!alreadyExists)
			{
				if (first)
				{
					json += rule;
					first = false;
				}
				else
				{
					json += "," + rule;
				}
			}
		}
		json += "\" }";
		return json;
	}
	
	public static String RuleConflictObjectsListToJSON(ArrayList<RuleConflictObjects> list)
	{
		String json = "{\"ruleConflictObjects\": [";
		Boolean first = true;
		for (RuleConflictObjects req: list)
		{
			if (first)
			{
				json += req.toJson();
				first = false;
			}
			else
			{
				json += "," + req.toJson();
			}
		}
		json += "] }";
		return json;
	}
}
