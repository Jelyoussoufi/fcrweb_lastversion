package be.celsius.fcrweb.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import be.celsius.fcrweb.objects.RuleObjectSummary;

public class ExtractorUtils 
{

	public static String listSourceObjectToHtml(ArrayList<RuleObjectSummary> list)
	{		
		String result = "";
		if (list != null)
		{
			
			for (RuleObjectSummary obj: list)
			{				
				if (!result.equals(""))
				{
					result = result + "<br>";
				}
				String temp_result = "<img src='ressources/mob_icons/"+obj.getType()+".png'>";
				if (!obj.getPredefined().equals("unknown"))
				{
					String glocal = "local";
					if (obj.getPredefined().equals("1"))
					{
						glocal = "global";
					}
					temp_result += "<img src='ressources/mob_icons/"+glocal+".png'>";
				}
				if (obj.getProfile().equals(""))
				{
					temp_result += "&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + obj.getId() + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + obj.getName() + "</a>";
				}
				else
				{
					temp_result += "&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?profile_name=" + obj.getProfile() + "', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">" + obj.getProfile() + "</a>@<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + obj.getId() + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + obj.getName() + "</a>";
				}
				
				result += temp_result;
				
			}
		}
		return result;
	}
	
	public static String listRuleObjectToHtml(ArrayList<RuleObjectSummary> list)
	{
		String result = "";
		if (list != null)
		{
			for (RuleObjectSummary obj: list)
			{				
				String temp_result = "";
				if (!result.equals(""))
				{
					result = result + "<br>";
				}
				temp_result = "<img src='ressources/mob_icons/"+obj.getType()+".png'>";
				if (!obj.getPredefined().equals("unknown"))
				{
					String glocal = "local";
					if (obj.getPredefined().equals("1"))
					{
						glocal = "global";
					}
					temp_result += "<img src='ressources/mob_icons/"+glocal+".png'>";
				}
				temp_result+="&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + obj.getId() + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + obj.getName() + "</a>";
				result += temp_result;
				
			}
		}
		return result;
	}
	
	public static String listRuleServiceToHtml(ArrayList<RuleObjectSummary> list)
	{
		String result = "";
		if (list != null)
		{			
			for (RuleObjectSummary obj: list)
			{
				String temp_result = "";
				if (!result.equals(""))
				{
					result = result + "<br>";
				}
				temp_result = "<img src='ressources/mob_icons/"+obj.getType()+".png'>";
				if (!obj.getPredefined().equals("unknown"))
				{
					String glocal = "local";
					if (obj.getPredefined().equals("1"))
					{
						glocal = "global";
					}
					temp_result += "<img src='ressources/mob_icons/"+glocal+".png'>";
				}
				temp_result += "&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + obj.getId() + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + obj.getName() + "</a>";
				result += temp_result;
				
			}
		}
		return result;
	}
	
	
	public static String listSourceObjectToHtml(String list)
	{		
		String result = "";
		if (list != null)
		{
			String[] object = list.split(";");
			for (String obj: object)
			{
				String[] t = obj.split(",");
				if (t.length == 4)
				{
					if (!result.equals(""))
					{
						result = result + "<br>";
					}
					if (t[0].equals(""))
					{
						result = result + "<img src='ressources/mob_icons/"+t[3]+".png'>&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + t[1] + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + t[2] + "</a>";
					}
					else
					{
						result = result + "<img src='ressources/mob_icons/"+t[3]+".png'>&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?profile_name=" + t[0] + "', '_blank', 'resizable=yes, scrollbars=1, width=300, height=400')\">" + t[0] + "</a>@<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + t[1] + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + t[2] + "</a>";
					}
				}
			}
		}
		return result;
	}
	
	public static String listRuleObjectToHtml(String list)
	{
		String result = "";
		if (list != null)
		{
			String[] object = list.split(";");
			for (String obj: object)
			{
				String[] t = obj.split(",");
				if (t.length == 3)
				{
					if (!result.equals(""))
					{
						result = result + "<br>";
					}
					result = result + "<img src='ressources/mob_icons/"+t[2]+".png'>&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?object_id=" + t[0] + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + t[1] + "</a>";
				}
			}
		}
		return result;
	}
	
	public static String listRuleServiceToHtml(String list)
	{
		String result = "";
		if (list != null)
		{
			String[] object = list.split(";");
			for (String obj: object)
			{
				String[] t = obj.split(",");
				if (t.length == 3)
				{
					if (!result.equals(""))
					{
						result = result + "<br>";
					}
					result = result + "<img src='ressources/mob_icons/"+t[2]+".png'>&nbsp;<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?service_id=" + t[0] + "', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + t[1] + "</a>";
				}
			}
		}
		return result;
	}
	
	
	public static String listRuleToHtml(String list)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar now = Calendar.getInstance();
		
		String result = "";
		if (list != null)
		{
			String[] object = list.split(";");
			
			for (String obj: object)
			{
				String[] t = obj.split(",");
				if (t.length == 2)
				{
					if (!result.equals(""))
					{
						result = result + "<br>";
					}
					result = result + "<a href=\"javascript:void(0)\" onClick=\"window.open('rules.htm?id=" + t[0] + "', '_blank', 'resizable=yes, scrollbars=1, width=700, height=500')\">" + t[1] + "</a>";
				}
			}
			
		}
		return result;
	}
	
	public static String listObjectToHtml(String list)
	{
		String result = "";
		if (list != null)
		{
			String[] object = list.split(";");
			for (String obj: object)
			{
				String[] t = obj.split(",");
				if (t.length == 3)
				{
					if (!result.equals(""))
					{
						result = result + "<br>";
					}
					result = result + "<a href=\"javascript:void(0)\" onClick=\"window.open('objects.htm?id=" + t[0] + "&type="+t[1]+"', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + t[2] + "</a>";
				}
			}
		}
		return result;
	}
	
	public static String listServiceToHtml(String list)
	{
		String result = "";
		if (list != null)
		{
			String[] object = list.split(";");
			for (String obj: object)
			{
				String[] t = obj.split(",");
				if (t.length == 3)
				{
					if (!result.equals(""))
					{
						result = result + "<br>";
					}
					result = result + "<a href=\"javascript:void(0)\" onClick=\"window.open('services.htm?id=" + t[0] + "&type="+t[1]+"', '_blank', 'resizable=yes, scrollbars=1, width=400, height=500')\">" + t[2] + "</a>";
				}
			}
		}
		return result;
	}
}
