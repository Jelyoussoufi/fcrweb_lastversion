package be.celsius.fcrweb.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import be.celsius.util.Config;
import be.celsius.util.SimpleDomParser;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilSoap;
import be.celsius.util.UtilStr;

public class WebServiceUtils 
{	
	public ArrayList<String[]> getFCRPerTeam(String[][] users, String url)
	{
		ArrayList<String[]> fcr_per_team = new ArrayList<String[]>();
		UtilSoap soap = new UtilSoap();
		for (int i = 0; i < users.length; i++)
		{
			String request 	= "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:bon=\"https://websec.ux.mobistar.be/sectools/webservices/BONITA_WS\">"
					+ "<soapenv:Header/>"
					+ "<soapenv:Body>"
					+ "<bon:getUserInformation soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><user_login xsi:type=\"xsd:string\">"+users[i][0]+"</user_login></bon:getUserInformation>"
					+ "</soapenv:Body>"
					+ "</soapenv:Envelope>";
			
			String response = soap.extractResponse(soap.submitRequest(url, request, true, true));
			String error 	= getErrorString(response);
			String team = "";
			if (UtilStr.isEmpty(error))
			{
				try
				{
					SimpleDomParser dom = new SimpleDomParser(response);
					System.out.println(response);
					Element res			= (Element)dom.getNode(null, "return");
					team = res.getElementsByTagName("team").item(0).getNodeValue();
				}catch (Exception e){
					System.out.println("ERROR WHILE DECODING BONITA WS RESPONSE:\n\n"+e.getLocalizedMessage()+"\n"+e.getMessage() + "\n"+e.getStackTrace());
					team = "error while asking bonita";
				}
			}
			else
			{
				System.out.println("ERROR WHILE SUBMITING REQUEST TO BONITA WS:\n\n"+error);
				team = "error while asking bonita";
			}
									
			boolean exists = false;
			for (String[] temp: fcr_per_team)
			{
				if (temp[0].equals(team))
				{
					exists = true;
					temp[1] = ""+ (Integer.parseInt(temp[1]) + Integer.parseInt(users[i][1]));
				}
			}
			if (!exists)
			{
				String[] n = new String[2];
				n[0] = team;
				n[1] = users[i][1];
				fcr_per_team.add(n);
			}
		}
		
		return fcr_per_team;
	}
	
	public String getErrorString(String msg)
	{
		try
		{
			if (UtilStr.hasX(msg, "DOCTYPE") | UtilStr.hasX(msg, "<html>"))
				return msg.split("<title>")[1].split("</title>")[0];
			else
				return "";
		}
		catch (Exception e){}
		return "";
	}
}
