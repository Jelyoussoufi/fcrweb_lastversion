/**
 * 
 */
package be.celsius.fcrweb.sql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import be.celsius.fcrweb.controllers.Fcrweb_Webservices;
import be.celsius.util.Util;
import be.celsius.util.UtilApp;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.datastructure.Ulist;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Umap.KVP;

/**
 * @author jhuilian
 *
 */
public class FcrWebAccess extends FcrWebFormAdminAccess{
	
	public FcrWebAccess()
	{
		super();
	}

	public Map<String,Umap> retrieveForm(String formId)
	{
		UtilLog.out("retrieveForm("+formId+")");
		Map<String,Umap> collections 	= new HashMap<String,Umap>();				
		Umap ai 						= retrieveAdminAndTechInfo(formId);
		Umap ti							= UtilApp.getTechInfoList();		
		Umap sef						= retrieveSefInfo(formId);
		Umap aefs 						= retrieveAefInfo(formId);		 
		Umap ret						= getRequestEditionTaskRefs(formId);
		
		ti.push("form-selector", (UtilStr.isEmpty(aefs.get("f-nb-flow")) || Util.getInteger(aefs.get("f-nb-flow"))<=0) ? "simple" : "advanced");
		collections.put("adminInfo", ai);
		collections.put("techInfo", ti);
		collections.put("requestEditionTask", ret);
		collections.put("docInfo", retrieveDocInfo(formId));
		collections.put("sef", sef);
		collections.put("aefs", aefs);
		return collections;
	}

	protected Umap retrieveSefInfo(String formId) 
	{
		Umap sef = UtilApp.getSefInfoList();
		try 
		{
			Ulist sefInfo 	= UtilApp.getSefInfoList().keyList(); 
			String attrs 	= Util.list2string(sefInfo, ",", "", "", "").replace("-", "_");
			BasketBean rows = execute("select " + attrs + " from request where id='" + formId + "'", attrs, UtilApp.genMapper(sefInfo));

			if (rows.getBasket().size() > 0)
			{
				BoxBean sefValues = rows.getBasket().get(0).toBox();
				for (String sefParam : sefInfo)
					sef.put(sefParam, sefValues.get(sefParam).toSeed().getSeed());
				return sef;
			}
		} catch (Exception e) {
			UtilLog.printException(e);			
		}
		return new Umap();
	}
	
	protected Umap retrieveAefInfo(String formId) 
	{
		Umap aefs 			= new Umap().setName(UtilApp.HD_AEF);
		BoxBean rule_refs 	= getFwRuleIds(formId);
		try {
			String flow_attrs 			= "id,flow_content_description,action,end_date,connection_type,existing_rule,mdata";
			BasketBean flow_ids_list 	= execute(
											"select id,flow_content_description,action,IFNULL(end_date, '0000-00-00') end_date,connection_type,existing_rule,mdata " + 
											"from flow where request_id=" + dbS(formId), flow_attrs);
			
			aefs.put("f-nb-flow", flow_ids_list.getBasket().size() + "");
			
			String prof_query = "select fso.flow_id f_id, fso.profile_id p_id, p.name p_name, p.responsible_uid p_owner, p.description p_description, p.action p_action, p.mdata p_mdata ";
			prof_query += " from flow_src_object fso, flow f, profile p ";
			prof_query += " where f.request_id="+ dbS(formId) +" and fso.flow_id=f.id and p.id=fso.profile_id";
			prof_query += " union ";
			prof_query += " select fsg.flow_id f_id, fsg.profile_id p_id, p.name p_name, p.responsible_uid p_owner, p.description p_description, p.action p_action, p.mdata p_mdata ";
			prof_query += " from flow_src_group fsg, flow f, profile p ";
			prof_query += " where f.request_id="+ dbS(formId) +" and fsg.flow_id=f.id and p.id=fsg.profile_id";
			BasketBean rows = execute(prof_query, "f_id,p_id,p_name,p_owner,p_description,p_action,p_mdata");

			for (LeafBean leaf : rows.getBasket())
			{
				BoxBean f_p = leaf.toBox();
				profiles.put(f_p.get("f_id").toSeed().getSeed(), f_p);
			}
			
			for (int i=0 ; i<flow_ids_list.getBasket().size() ; i++)
			{					
				BoxBean flow 	= flow_ids_list.getBasket().get(i).toBox();
				String flow_id 	= flow.get("id").toSeed().getSeed();
				String flow_pre = "f-" + (i + 1) + "-";
				Umap flow_meta	= new Umap().setKVP(KVP.KVP5).mapsterize(flow.get("mdata").toSeed().getSeed());
				aefs.put(flow_pre +"flow-id", flow_id);
				aefs.put(flow_pre + "flow-action", flow.get("action").toSeed().getSeed());
				aefs.put(flow_pre + "flow-description", flow.get("flow_content_description").toSeed().getSeed());
				aefs.put(flow_pre + "end-date", getMySqlDate(flow.get("end_date").toSeed().getSeed()));
				
				String connection_type = flow.get("connection_type").toSeed().getSeed();
				aefs.put(flow_pre + "dataflow-type", (UtilStr.isEmpty(connection_type) ? "Host 2 Host" : connection_type));
					
				
UtilLog.out("rule_refs:" + rule_refs.toJson());
				if (!rule_refs.get(flow_id).isNullSeed())
				{
					BasketBean rules = rule_refs.get(flow_id).toBasket();
					aefs.put(flow_pre + "nb-rule", rules.getBasket().size() + "");
					for (int r_idx=0 ; r_idx<rules.getBasket().size() ; r_idx++)
					{
						BoxBean rule = rules.getBasket().get(r_idx).toBox();
						aefs.put(flow_pre + "rule-" + (r_idx + 1) + "-fw", rule.get("fw").toSeed().getSeed());
						aefs.put(flow_pre + "rule-" + (r_idx + 1) + "-rule-id", rule.get("rule_id").toSeed().getSeed());
					}
				}
				else if (UtilStr.isX(flow_meta.get("from-rule"), "true"))
				{
					UtilLog.out("LOAD RULE BASE FW RULE ID");
					aefs.put(flow_pre + "nb-rule", "1");
					aefs.put(flow_pre + "rule-1-fw", flow_meta.get("base-fw"));
					aefs.put(flow_pre + "rule-1-rule-id", flow_meta.get("base-rule-id"));
				}
				else
					aefs.put(flow_pre + "nb-rule", "0");
				
				aefs.put(flow_pre +"meta", flow.get("mdata").toSeed().getSeed());
				
				/** retrieve flow profile **/
				if (!profiles.get(flow_id).isNullSeed())
				{					
					BoxBean profile = profiles.get(flow_id).toBox();
					aefs.put(flow_pre + "profile", profile.get("p_name").toSeed().getSeed());
					aefs.put(flow_pre + "profile-owner", profile.get("p_owner").toSeed().getSeed());
					aefs.put(flow_pre + "profile-description", profile.get("p_description").toSeed().getSeed());
					aefs.put(flow_pre + "profile-uc", profile.get("p_action").toSeed().getSeed());
					aefs.put(flow_pre + "profile-meta", profile.get("p_mdata").toSeed().getSeed());
				}
				
				//initAreaNameMap();
				retrieveAefSrcDstDetails(formId, flow_id, UtilStr.unTail(flow_pre, "-"), aefs, UtilApp.SRC );
				retrieveAefSrcDstDetails(formId, flow_id, UtilStr.unTail(flow_pre, "-"), aefs, UtilApp.DST);
				retrieveAefServiceDetails(formId, flow_id, UtilStr.unTail(flow_pre, "-"), aefs);
			}
			return aefs;
		} catch (Exception e) {
			UtilLog.printException(e);			
		}
		return new Umap().setName(UtilApp.HD_AEF).push("f-nb-flow", "0");
	}
	
	
	protected Umap areaid_2_areaname = new Umap();
	
	public Umap initAreaNameMap()
	{
		if (areaid_2_areaname==null)
			areaid_2_areaname = new Umap();
		areaid_2_areaname.clear();
		for (LeafBean row : getAreas("").getBasket())
		{
			BoxBean area = row.toBox();
			areaid_2_areaname.push(area.get("id").toSeed().getSeed(), area.get("name").toSeed().getSeed());
		}		
		
		return areaid_2_areaname;
	}
	/*	
	// areas map org:::area.name => area.id
	protected Umap org_area_2_id = new Umap(); 
	
	protected void initAreaIdMap()
	{
		if (org_area_2_id==null)
			org_area_2_id = new Umap();
		org_area_2_id.clear();
		for (LeafBean row : getAreas("").getBasket())
		{
			BoxBean area = row.toBox();
			org_area_2_id.push(area.get("organization").toSeed().getSeed() + ":::" + area.get("name").toSeed().getSeed(), area.get("id").toSeed().getSeed());
		}		
	}
	*/
	protected BoxBean profiles = new BoxBean();
	
	protected void retrieveAefSrcDstDetails(String formId, String flowId, String flow_pre, Umap aef, String type)
	{
		String elemType 	= UtilApp.isSrc(type) ? "src" : "dest";
		String elemType_db 	= UtilApp.isSrc(type) ? "src" : "dst";
		int nb_ctrl 		= 0;
		int ctrl_idx		= 0; 

		/** retrieve host(s) and subnet(s) **/
		String attrs = "o.req_id,o.type,o.name,o.ostype,o.ipaddress,o.netmask,o.zone_id,o.action,o.environment,o.ipaddress_nat,o.organization,o.firewall_name,o.mdata,fso.action";
		if (UtilStr.isX(type, UtilApp.SRC)) attrs += ",fso.profile_id";
		String query = " select " + attrs + " from flow_" + elemType_db + "_object fso, object o ";
			   query += " where o.id=fso.object_id and fso.flow_id=" + dbS(flowId);
	
		BasketBean objects 	= execute(query, attrs);			
		ctrl_idx			= 0;
		nb_ctrl				= objects.getBasket().size();
		
		for (int i=0 ; i<objects.getBasket().size() ; i++)
		{
			
			
			ctrl_idx++;
			String ctrl_pre		= flow_pre + "." + ctrl_idx + "-" + elemType + "-";			
			BoxBean object 		= objects.getBasket().get(i).toBox();
			String object_type 	= object.get("o.type").toSeed().getSeed();
			Umap meta			= new Umap().setKVP(KVP.KVP5).mapsterize(object.get("o.mdata").toSeed().getSeed());
			
			aef.put(ctrl_pre + "meta", meta.parameterize());
			aef.put(ctrl_pre + "output", object.get("o.firewall_name").toSeed().getSeed());			
			aef.put(ctrl_pre + "uc", object.get("o.action").toSeed().getSeed());
			aef.put(ctrl_pre + "meta" , object.get("o.mdata").toSeed().getSeed());
UtilLog.out("retrieved object:" + object_type + " - " + object.get("o.firewall_name").toSeed().getSeed() + " - " + object.get("o.mdata").toSeed().getSeed());
			if (UtilStr.isX(object_type,"host"))
			{	
				String host = object.get("o.name").toSeed().getSeed();
				aef.put(ctrl_pre + "cat", "Host");
				aef.put(ctrl_pre + "org", object.get("o.organization").toSeed().getSeed());
				aef.put(ctrl_pre + "host", host);
				aef.put(ctrl_pre + "ip", object.get("o.ipaddress").toSeed().getSeed());
				aef.put(ctrl_pre + "ip-tsl", object.get("o.ipaddress_nat").toSeed().getSeed());
				aef.put(ctrl_pre + "host-env", object.get("o.environment").toSeed().getSeed());
			}
			else if (UtilStr.isX(object_type,"subnet"))
			{
				String ip_mask = object.get("o.ipaddress").toSeed().getSeed() + "/" + object.get("o.netmask").toSeed().getSeed();
				aef.put(ctrl_pre + "cat", "Subnet");
				aef.put(ctrl_pre + "org", object.get("o.organization").toSeed().getSeed());
				aef.put(ctrl_pre + "subnet", ip_mask);
				String area_id = object.get("o.zone_id").toSeed().getSeed();
				//aef.put(ctrl_pre + "subnet-area-id" , area_id);
				//aef.put(ctrl_pre + "subnet-area", areaid_2_areaname.get(area_id));
			}
			else	//undefined type object
			{
				UtilLog.out("undefined type object:" + object.get("o.firewall_name").toSeed().getSeed());
				aef.put(ctrl_pre + "cat", object_type);
			}
		}
			
		/** retrieve group(s) of objects **/
		attrs 	= "gr.id,gr.name,gr.action,gr.organization,gr.environment,gr.firewall_name,gr.mdata,fsg.action";
		if (UtilStr.isX(type, UtilApp.SRC)) attrs += ",fsg.profile_id";
		query 	= " select " + attrs + " from flow_" + elemType_db + "_group fsg, object_group gr ";
		query 	+= " where gr.id=fsg.group_id and fsg.flow_id=" + dbS(flowId);
		
		objects = execute(query, attrs);
		nb_ctrl += objects.getBasket().size();
		for (int i=0 ; i<objects.getBasket().size() ; i++)
		{
			ctrl_idx++;	
			String ctrl_pre			= flow_pre + "." + ctrl_idx + "-" + elemType + "-";					
			BoxBean object_group 	= objects.getBasket().get(i).toBox();				
			String group_action		= object_group.get("gr.action").toSeed().getSeed();
			String mdata			= object_group.get("gr.mdata").toSeed().getSeed();
			Umap meta				= new Umap().setKVP(KVP.KVP5).mapsterize(mdata);
			aef.put(ctrl_pre + "meta", mdata);
			if 
			(
				UtilApp.isSrc(type) &&
				(
					UtilApp.isNao(aef.get(flow_pre + "-flow-action"))) & 
					(!UtilStr.isX(meta.get("from-rule"), "true") | UtilStr.isX(meta.get("is-nao-rule"), "true")
				)
			)					
			{
				String fw_name	= objects.getBasket().get(i).toBox().get("gr.firewall_name").toSeed().getSeed();
				aef.put(ctrl_pre + "output", fw_name);
				aef.put(ctrl_pre + "output-opt", aef.get(flow_pre + "-profile") + "@" + fw_name);				 
				aef.put(ctrl_pre + "name", objects.getBasket().get(i).toBox().get("gr.name").toSeed().getSeed());
				aef.put(ctrl_pre + "cat", "nao");
				aef.put(ctrl_pre + "uc", "use");
			}
			else
			{
				aef.put(ctrl_pre + "cat", "Group of objects");			
				aef.put(ctrl_pre + "org", object_group.get("gr.organization").toSeed().getSeed());
				aef.put(ctrl_pre + "uc", group_action);			
				aef.put(ctrl_pre + "goo", object_group.get("gr.name").toSeed().getSeed());
				aef.put(ctrl_pre + "goo-env", object_group.get("gr.environment").toSeed().getSeed());
				aef.put(ctrl_pre + "output", object_group.get("gr.firewall_name").toSeed().getSeed());
			}
			
			/** retrieve group objects**/
			if (UtilStr.isX(group_action,"create"))
			{
				attrs 			= "o_type,o_name,o_ip,o_mask,o_zone_id,o_environment,o_firewall_name,o_action,o_mdata,gr_name,gr_environment,gr_firewall_name,gr_mdata";
				query 			= " select o.type o_type, o.name o_name, o.ipaddress o_ip, o.netmask o_mask, o.action o_action, o.zone_id o_zone_id, o.environment o_environment, o.firewall_name o_firewall_name, o.mdata o_mdata, '' gr_name, '' gr_environment, '' gr_firewall_name, '' gr_mdata ";
				query 			+= " from object o, object_group gr, object_group_member ogm";
				query 			+= " where o.id=ogm.obj_id and ogm.grp_id=gr.id and gr.id=" + dbS(object_group.get("gr.id").toSeed().getSeed());
				query 			+= " union ";
				query 			+= " select '' o_type, '' o_name, '' o_ip, '' o_mask, '' o_action, '' o_zone_id, '' o_environment, '' o_firewall_name, '' o_mdata, gr_c.name gr_name, gr_c.environment gr_environment, gr_c.firewall_name gr_firewall_name, gr_c.mdata gr_mdata ";
				query 			+= " from object_group gr_p, object_group gr_c, object_group_member_grp ogmg";
				query 			+= " where ogmg.grp_id=gr_p.id and ogmg.child_grp_id=gr_c.id and gr_p.id=" + dbS(object_group.get("gr.id").toSeed().getSeed());				
				String goos		= "";
				String hosts 	= "";
				String subnets	= "";		
				for (LeafBean element : execute(query, attrs).getBasket())
				{
					BoxBean elem = element.toBox();
					if (!UtilStr.isEmpty(elem.get("gr_name").toSeed().getSeed()))	// the elemect is a group of elemects
					{
						goos += UtilStr.isEmpty(goos) ? "" : UtilApp.GOO_SEP;
						goos += "cat" + UtilApp.CA_KV_SEP + "goo" + UtilApp.CA_E_SEP;						
						goos += "id" + UtilApp.CA_KV_SEP + elem.get("gr_firewall_name").toSeed().getSeed() + UtilApp.CA_E_SEP;
						goos += "env" + UtilApp.CA_KV_SEP + elem.get("gr_environment").toSeed().getSeed() + UtilApp.CA_E_SEP;
						goos += "goo" + UtilApp.CA_KV_SEP + elem.get("gr_name").toSeed().getSeed() + UtilApp.CA_E_SEP;
						goos += "uc" + UtilApp.CA_KV_SEP + "use";
						goos += "meta" + UtilApp.CA_KV_SEP + elem.get("gr_mdata").toSeed().getSeed();
					}
					else if (UtilStr.isX(elem.get("o_type").toSeed().getSeed(), "host"))
					{	
						hosts += UtilStr.isEmpty(hosts) ? "" : UtilApp.GOO_SEP;
						hosts += "cat" + UtilApp.CA_KV_SEP + "host" + UtilApp.CA_E_SEP;
						hosts += "uc" + UtilApp.CA_KV_SEP + elem.get("o_action").toSeed().getSeed() + UtilApp.CA_E_SEP;
						hosts += "id" + UtilApp.CA_KV_SEP + elem.get("o_firewall_name").toSeed().getSeed() + UtilApp.CA_E_SEP;
						hosts += "env" + UtilApp.CA_KV_SEP + elem.get("o_environment").toSeed().getSeed() + UtilApp.CA_E_SEP;
						hosts += "host" + UtilApp.CA_KV_SEP + elem.get("o_name").toSeed().getSeed() + UtilApp.CA_E_SEP;
						hosts += "ip" + UtilApp.CA_KV_SEP + elem.get("o_ip").toSeed().getSeed();
						hosts += "meta" + UtilApp.CA_KV_SEP + elem.get("o_mdata").toSeed().getSeed();
					}
					else if (UtilStr.isX(elem.get("o_type").toSeed().getSeed(), "subnet"))
					{
						subnets += UtilStr.isEmpty(subnets) ? "" : UtilApp.GOO_SEP;
						subnets += "cat" + UtilApp.CA_KV_SEP + "subnet" + UtilApp.CA_E_SEP;
						subnets += "uc" + UtilApp.CA_KV_SEP + elem.get("o_action").toSeed().getSeed() + UtilApp.CA_E_SEP;
						subnets += "id" + UtilApp.CA_KV_SEP + elem.get("o_firewall_name").toSeed().getSeed() + UtilApp.CA_E_SEP;
						String area_id = elem.get("o_zone_id").toSeed().getSeed();
						/*
						subnets += "area-id" + UtilApp.CA_KV_SEP + area_id + UtilApp.CA_E_SEP;
						subnets += "area" + UtilApp.CA_KV_SEP + areaid_2_areaname.get(area_id) + UtilApp.CA_E_SEP;
						*/
						subnets += "subnet" + UtilApp.CA_KV_SEP + elem.get("o_ip").toSeed().getSeed() + "/" + elem.get("o_mask").toSeed().getSeed();
						subnets += "meta" + UtilApp.CA_KV_SEP + elem.get("o_mdata").toSeed().getSeed();
					}					
				}
				aef.put(ctrl_pre + "goos", goos);
				aef.put(ctrl_pre + "hosts", hosts);
				aef.put(ctrl_pre + "subnets", subnets);
			}
		}		
		aef.put(flow_pre + "-nb-" + elemType , nb_ctrl + "");
		aef.put(flow_pre + "-" + elemType + "-idx" , nb_ctrl>0 ? "1" : "-1");
	}
	
	protected void retrieveAefServiceDetails(String formId, String flowId, String flow_pre, Umap aef)
	{
		/** retrieve service(s) **/
		String attrs 		= "svc.req_id,svc.name,svc.protocol,svc.svc_type,svc.port,svc.firewall_name,svc.action,svc.mdata,fss.action";
		String query 		= "select " + attrs + " from flow_svc_service fss, service svc ";
			   query 		+= "where svc.id=fss.service_id and fss.flow_id=" + dbS(flowId);			  
		BasketBean services = execute(query, attrs);		
		int nbElems 		= services.getBasket().size();
		int srv_idx			= 0;

		for (int i=0 ; i<nbElems ; i++)
		{
			srv_idx++;
			String srv_pre	= flow_pre + "." + srv_idx + "-srv-";
			BoxBean service = services.getBasket().get(i).toBox();
			aef.put(srv_pre + "cat", "Single service");
			aef.put(srv_pre + "output", service.get("svc.firewall_name").toSeed().getSeed());
			aef.put(srv_pre + "uc", service.get("svc.action").toSeed().getSeed());
			aef.put(srv_pre + "one-name", service.get("svc.name").toSeed().getSeed());
			aef.put(srv_pre + "one-proto", service.get("svc.protocol").toSeed().getSeed());
			aef.put(srv_pre + "one-type", service.get("svc.svc_type").toSeed().getSeed());
			aef.put(srv_pre + "one-port", service.get("svc.port").toSeed().getSeed());
			aef.put(srv_pre + "meta", service.get("svc.mdata").toSeed().getSeed());
			
		}
		
		/** retrieve group(s) of services **/
		attrs 		= "gr.id,gr.name,gr.firewall_name,gr.action,gr.mdata,fsg.action";
		query 		= "select " + attrs + " from flow_svc_group fsg, service_group gr ";
		query 		+= "where gr.id=fsg.group_id and fsg.flow_id=" + dbS(flowId);
		services 	= execute(query, attrs);
		nbElems 	+= services.getBasket().size();		

		for (int i=0 ; i<services.getBasket().size() ; i++)
		{		
			srv_idx++;
			String srv_pre		= flow_pre + "." + srv_idx + "-srv-";
			BoxBean svc_group 	= services.getBasket().get(i).toBox();
			
			aef.put(srv_pre + "cat", "Group of services");
			aef.put(srv_pre + "output", svc_group.get("gr.firewall_name").toSeed().getSeed());
			aef.put(srv_pre + "gsrv-name", svc_group.get("gr.name").toSeed().getSeed());
			String srv_action = svc_group.get("gr.action").toSeed().getSeed();
			aef.put(srv_pre + "uc", srv_action);
			aef.put(srv_pre + "meta", svc_group.get("gr.mdata").toSeed().getSeed());
			
			if (UtilStr.isX(srv_action,"create"))
			{
				attrs 			= "svc_name,svc_protocol,svc_type,svc_port,svc_action,svc_firewall_name,svc_mdata";
				query 			= " select svc.name svc_name, svc.protocol svc_protocol, svc.svc_type svc_type, svc.port svc_port, svc.action svc_action, svc.firewall_name svc_firewall_name, svc.mdata svc_mdata from service svc, service_group gr, service_group_member gm";
				query 			+= " where svc.id=gm.svc_id and gm.grp_id=gr.id and gr.id=" + dbS(svc_group.get("gr.id").toSeed().getSeed());
				String elems	= "";
				
				for (LeafBean service : execute(query, attrs).getBasket())
				{
					BoxBean svc = service.toBox();
					elems += UtilStr.isEmpty(elems) ? "" : UtilApp.GOO_SEP;
					elems += "cat" + UtilApp.CA_KV_SEP + "srv" + UtilApp.CA_E_SEP;
					elems += "uc" + UtilApp.CA_KV_SEP + svc.get("svc_action").toSeed().getSeed() + UtilApp.CA_E_SEP;
					elems += "id" + UtilApp.CA_KV_SEP + svc.get("svc_firewall_name").toSeed().getSeed() + UtilApp.CA_E_SEP;
					elems += "name" + UtilApp.CA_KV_SEP + svc.get("svc_name").toSeed().getSeed() + UtilApp.CA_E_SEP;
					elems += "proto" + UtilApp.CA_KV_SEP + svc.get("svc_protocol").toSeed().getSeed() + UtilApp.CA_E_SEP;
					elems += "type" + UtilApp.CA_KV_SEP + svc.get("svc_type").toSeed().getSeed() + UtilApp.CA_E_SEP;
					elems += "port" + UtilApp.CA_KV_SEP + svc.get("svc_port").toSeed().getSeed();
					elems += "meta" + UtilApp.CA_KV_SEP + svc.get("svc_mdata").toSeed().getSeed();
				}				      
				aef.put(srv_pre + "gsrv-srvs", elems);
			}
		}
		aef.put(flow_pre + "-nb-srv", nbElems + "");
		aef.put(flow_pre + "-srv-idx" ,  nbElems>0 ? "1" : "-1");
	}
	
	
	
	/**
	 * in order to handle redundancy of host, subnet, goo and service
	 * these map contains all obj name:object.id (if srv name:service.id)
	 **/
	protected Umap fcr_hosts 		= new Umap();
	protected Umap fcr_subnets 		= new Umap();
	protected Umap fcr_goos 		= new Umap();
	protected Umap fcr_unknown 		= new Umap();
	protected Umap fcr_services 	= new Umap();
	protected Umap fcr_gsrv 		= new Umap();
	protected Umap fcr_nao			= new Umap();
	
	public void updateFormInDatabase(String formId, Map<String, Umap> collections, boolean isDupForm)
	{
		Umap ai		= collections.get("adminInfo");
		Umap ti		= collections.get("techInfo");
		Umap sef 	= collections.get("sef");
		Umap aefs	= collections.get("aefs");
			
		updateAdminFormInDatabase(formId, collections, isDupForm);
		deleteAllFlows(formId);
		if (UtilApp.isSef(ti.get("form-selector")))
			update("update request set form_sel='simple', flow_details='" + sef.get("flow-details") + "' where id='" + formId + "'");
		else
		{
			update("update request set form_sel='advanced', flow_details='' where id='" + formId + "'");
			fcr_hosts.clear();
			fcr_subnets.clear();
			fcr_goos.clear();
			fcr_services.clear();	
			fcr_gsrv.clear();
			fcr_nao.clear();
			
			int nb_flow = Util.getInteger(aefs.get("f-nb-flow"));
			
			if (nb_flow>0)
			{				
				for (int flow_idx = 1 ; flow_idx <= nb_flow ; flow_idx++)
				{
					try
					{
						//add flow general information 
						String f_idx 		= "f-" + flow_idx;
						String flow_action 	= aefs.get(f_idx + "-flow-action");
						String f_meta 		= aefs.get(f_idx + "-meta");
						String query 		= "insert into flow (request_id,action,flow_content_description,end_date,connection_type,mdata)";
								query 		+= "values(";
								query 		+= dbS(formId) + ",";
								query 		+= dbS(flow_action) + ",";
								query 		+= dbS(aefs.get(f_idx + "-flow-description"))+ ",";
								query 		+= dbSN(toMySqlDate(aefs.get(f_idx + "-end-date"))) + ",";
								query 		+= dbSN(aefs.get(f_idx + "-dataflow-type")) + ",";	
								query 		+= dbSN(f_meta) + ")";
						String flow_id 		= updateReturnId(query) + "";

						String ts_name = ai.get("application-service");
						aefs.push(f_idx + "-profile-id", addProfile(formId, ts_name, aefs, f_idx, isDupForm));					
						aefs.push("flow_action", flow_action);
						//initAreaIdMap();
						addAdvancedFlowSrcDestDetails(formId, flow_id, ts_name, aefs, f_idx, UtilApp.SRC, isDupForm);
						addAdvancedFlowSrcDestDetails(formId, flow_id, ts_name, aefs, f_idx, UtilApp.DST, isDupForm);
						addServiceDetails(formId, flow_id, ts_name, aefs, f_idx, isDupForm);
					}
					catch(Exception e){UtilLog.printException(e);}
				}
			}
		}
	}
	
	protected String addProfile(String formId, String ts_name, Umap aef, String flow_idx, boolean isDupForm)
	{
		String profileId = "";

		if (UtilStr.isX(aef.get(flow_idx + "-dataflow-type"), "User 2 Host"))
		{
			String prof	= aef.get(flow_idx + "-profile");	
			
			if (UtilStr.isEmpty(prof))
				return "";
			else
			{
				String query = "insert into profile (req_id,name,responsible_uid,description,action,mdata)";
				query += "values (";
				query += dbS(formId) + ",";
				query += dbS(prof) + ",";
				query += dbS(aef.get(flow_idx +"-profile-owner")) + ",";
				query += dbS(aef.get(flow_idx +"-profile-description")) + ",";
				query += dbS(!isDupForm ? aef.get(flow_idx + "-profile-uc") : "use") + ",";
				query += dbS(aef.get(flow_idx +"-profile-meta")) + ")";
				
				profileId = updateReturnId(query) + "";
				return (!UtilStr.isEmpty(profileId) && Util.getLong(profileId)>=0) ? profileId : "";
			}				
		}	
		return "";
	}
	
	/**
	 * 
	 * @param elementType source | destination
	 */
	protected void addAdvancedFlowSrcDestDetails
	(
		String formId, 
		String flow_id,
		String ts_name, 
		Umap aef, 
		String flow_idx, 
		String elementType, 
		boolean isDupForm
	)
	{
		String query 		= "";
		String shortType 	= UtilApp.isSrc(elementType) ? "src" : "dest";
		String dbType 		= UtilApp.isSrc(elementType) ? "src" : "dst";
		String profileId	= aef.get(flow_idx + "-profile-id");

		String flow_action 	= aef.get("flow_action");
		boolean isNao		= UtilApp.isNao(flow_action);
		Map<String,Umap> fw	= new HashMap<String,Umap>();
		
		/** get nao groups (vpn_enc or user_auth)**/
		if (UtilApp.isSrc(elementType) & isNao)
		{
			for (LeafBean leaf : getObjectGroups("", ts_name, "", (UtilApp.isEncryption(flow_action) ? "vpn_encryption" : "user_authentication")).getBasket())
			{
				BoxBean goo = leaf.toBox();
				fw.put
				(
					goo.get("firewall_name").toSeed().getSeed(), 
					new Umap().push("organization", goo.get("organization").toSeed().getSeed())
						.push("name", goo.get("name").toSeed().getSeed())
						.push("environment", goo.get("environment").toSeed().getSeed())
						.push("firewall_name", goo.get("firewall_name").toSeed().getSeed())
				);
			}
		}
		
		for (int obj_idx = 1 ; obj_idx <= Util.getInteger(aef.get(flow_idx + "-nb-" + shortType)) ; obj_idx++)
		{		
			String prefix 	= flow_idx + "." + obj_idx + "-" + shortType + "-";
			String fw_name	= aef.get(prefix + "output");
			
			Umap ctrl_meta  = new Umap().setKVP(KVP.KVP5).mapsterize(aef.get(prefix + "meta"));			
			UtilLog.out(prefix + "meta:" + ctrl_meta.toJson());
			
			if (UtilApp.isSrc(elementType) && isNao && (!UtilStr.isX(ctrl_meta.get("from-rule"), "true") | UtilStr.isX(ctrl_meta.get("is-nao-rule"), "true")))
			{
				
				/** @see add goo **/
				Umap rdd_map = fcr_nao;
				if (!rdd_map.containsKey(fw_name))
				{
					Umap goo = fw.get(fw_name);
					query = "insert into object_group(req_id,name,organization,action,firewall_name,environment,mdata)";
					query += "values(";
					query += dbS(formId) + ",";
					query += dbS(goo.get("name")) + ",";
					query += dbS(goo.get("organization")) + ",";
					query += dbS("use") + ",";
					query += dbS(fw_name) + ",";
					query += dbS(goo.get("environment")) + ",";
					query += dbS(aef.get(prefix + "meta")) + ")";
					rdd_map.put(fw_name, updateReturnId(query) + "");
				}
				String profile_ext = (UtilApp.isSrc(elementType) && !UtilStr.isEmpty(profileId)) ? "profile_id,":"";
				query = "insert into flow_" + dbType + "_group(flow_id," + profile_ext + "group_id,action) values(";
				query += dbS(flow_id) + ",";
				if ((UtilApp.isSrc(elementType) && !UtilStr.isEmpty(profileId)))
					query += dbS(profileId) + ",";
				query += dbS(rdd_map.get(fw_name)) + ",";
				query += dbS(UtilApp.OBJ_SVC_ADD) + ")";
				update(query);
			}
			else
			{
				String org 		= aef.get(prefix + "org");
				String cat 		= aef.get(prefix + "cat");				
				String uc		= !isDupForm ? aef.get(prefix + "uc") : "use";			
				Umap rdd_map	= null;
	
				if (UtilStr.isX(cat, "Host") | UtilStr.isX(cat, "Subnet"))
				{
					if (UtilStr.isX(cat, "Host"))
					{
						rdd_map 	= fcr_hosts;
						if (!rdd_map.containsKey(fw_name))
						{
							query = "insert into object(req_id,type,name,ipaddress,action,ipaddress_nat,organization,firewall_name,environment,mdata)";
							query += "values (";
							query += dbS(formId) + ",";
							query += dbS("host") + ",";
							query += dbS(aef.get(prefix +"host")) + ",";
							query += dbS(aef.get(prefix +"ip")) + ",";
							query += dbS(uc) + ",";
							query += dbS(aef.get(prefix +"ip-tsl")) + ",";
							query += dbS(org) + ",";
							query += dbS(fw_name) + ",";
							query += dbS(aef.get(prefix +"host-env")) + ",";
							query += dbS(aef.get(prefix +"meta")) + ")";
							rdd_map.put(fw_name, updateReturnId(query) + "");
						}
					}
					else if (UtilStr.isX(cat, "Subnet"))
					{
						String subnet 	= aef.get(prefix +"subnet");
						rdd_map 		= fcr_subnets;
						if (!rdd_map.containsKey(fw_name))
						{
							String[] ip_mask = subnet.split("/");
							String header = "insert into object (req_id,type,name,ipaddress,netmask,action,organization,firewall_name,mdata)"; //zone_id
							query = "";
							query += "values (";
							query += dbS(formId) + ",";
							query += dbS("subnet") + ",";
							query += dbS(subnet) + ",";
							query += dbS(ip_mask.length==2 ? ip_mask[0] : "") + ",";
							query += dbS(ip_mask.length==2 ? ip_mask[1] : "") + ",";				
							query += dbS(uc) + ",";
							query += dbS(org) + ",";
							query += dbS(fw_name) + ",";
							//query += dbS(org_area_2_id.get(org + ":::" + aef.get(prefix +"subnet-area"))) + ",";
							query += dbS(aef.get(prefix +"meta")) + ")";
							rdd_map.put(fw_name, updateReturnId(header + query) + "");
						}
					}
	
					String profile_ext = (UtilApp.isSrc(elementType) && !UtilStr.isEmpty(profileId)) ? "profile_id,":"";				
					query = "insert into flow_" + dbType + "_object(flow_id," + profile_ext + "object_id,action) values(";
					query += dbS(flow_id) + ",";
					if ((UtilApp.isSrc(elementType) && !UtilStr.isEmpty(profileId)))
						query += dbS(profileId) + ",";
					query += dbS(rdd_map.get(fw_name)) + ",";
					query += dbS(UtilApp.OBJ_SVC_ADD) + ")";
					update(query);
				}			
				else if (UtilStr.isX(cat, "Group of objects"))
				{
					/** @see add nao goo **/
					if (!fcr_goos.containsKey(fw_name))
					{
						query = "insert into object_group(req_id,name,organization,action,firewall_name,environment,mdata)";
						query += "values(";
						query += dbS(formId) + ",";
						query += dbS(aef.get(prefix + "goo")) + ",";
						query += dbS(org) + ",";
						query += dbS(uc) + ",";
						query += dbS(fw_name) + ",";
						query += dbS(aef.get(prefix +"goo-env")) + ",";
						query += dbS(aef.get(prefix +"meta")) + ")";
						String goo_id = updateReturnId(query) + "";
						fcr_goos.put(fw_name, goo_id);
	
						if (UtilStr.isX(uc, "create"))
						{
							String list_hosts 	= aef.get(prefix + "hosts");
							String list_subnets = aef.get(prefix + "subnets");
							String list_goos 	= aef.get(prefix + "goos");
							String ctrl_fw_name	= "";
							if (!UtilStr.isEmpty(list_hosts))
							{
								rdd_map 			= fcr_hosts;
								List<String> hosts 	= (List<String>)Util.array2list(list_hosts.split(UtilApp.GOO_SEP));
								for (String host_elems : hosts)
								{
									if (!UtilStr.isEmpty(host_elems))
									{
										Umap host 	= new Umap().setKVP(KVP.KVP3).mapsterize(host_elems);
										ctrl_fw_name= host.get("id");
										
										if (!rdd_map.containsKey(ctrl_fw_name))
										{
											query = "insert into object(req_id,type,name,ipaddress,action,ipaddress_nat,organization,firewall_name,environment,mdata)";
											query += "values (";
											query += dbS(formId) + ",";
											query += dbS("host") + ",";
											query += dbS(host.get("host")) + ",";
											query += dbS(host.get("ip")) + ",";
											query += dbS(host.get("uc")) + ",";
											query += dbS(host.get("ip-tsl")) + ",";
											query += dbS(org) + ",";
											query += dbS(ctrl_fw_name) + ",";
											query += dbS(host.get("env")) + ",";
											query += dbS(host.get("meta")) + ")";						
											rdd_map.put(ctrl_fw_name, updateReturnId(query) + "");
										}								
										query = "insert into object_group_member(grp_id,obj_id)";
										query += "values(" + dbS(goo_id) + "," + dbS(rdd_map.get(ctrl_fw_name)) + ")";
										update(query);
									}
								}
							}
							if (!UtilStr.isEmpty(list_subnets))
							{
								rdd_map 			= fcr_subnets;
								List<String> subnets= (List<String>)Util.array2list(list_subnets.split(UtilApp.GOO_SEP));
								
								for (String subnet_elems : subnets)
								{
									if (!UtilStr.isEmpty(subnet_elems))
									{
										Umap subnet		= new Umap().setKVP(KVP.KVP3).mapsterize(subnet_elems);							
										String value 	= subnet.get("subnet");
										ctrl_fw_name 	= subnet.get("id");
										
										if (!rdd_map.containsKey(ctrl_fw_name))
										{
											String[] ip_mask = subnet.get("subnet").split("/");
											String header = "insert into object (req_id,type,name,ipaddress,netmask,action,organization,firewall_name,mdata)"; //zone_id
											query = "";
											query += "values (";
											query += dbS(formId) + ",";
											query += dbS("subnet") + ",";
											query += dbS(subnet.get("subnet")) + ",";
											query += dbS(ip_mask.length==2 ? ip_mask[0] : "") + ",";
											query += dbS(ip_mask.length==2 ? ip_mask[1] : "") + ",";	
											query += dbS(subnet.get("uc")) + ",";
											query += dbS(org) + ",";
											query += dbS(ctrl_fw_name) + ",";
											//query += dbS(org_area_2_id.get(org + ":::" + subnet.get("area"))) + ",";
											query += dbS(subnet.get("meta")) + ")";
											rdd_map.put(ctrl_fw_name, updateReturnId(header + query) + "");
										}								
										query = "insert into object_group_member(grp_id,obj_id)";
										query += "values(" + dbS(goo_id) + "," + dbS(rdd_map.get(ctrl_fw_name)) + ")";
										update(query);
									}
								}
							}						
							if (!UtilStr.isEmpty(list_goos))
							{
								rdd_map 			= fcr_goos;
								List<String> subnets= (List<String>)Util.array2list(list_goos.split(UtilApp.GOO_SEP));
								for (String goo_elems : subnets)
								{
									if (!UtilStr.isEmpty(goo_elems))
									{
										Umap goomap		= new Umap().setKVP(KVP.KVP3).mapsterize(goo_elems);							
										String google 	= goomap.get("goo");
										ctrl_fw_name 	= goomap.get("id");
										if (!rdd_map.containsKey(ctrl_fw_name))
										{
											query = "insert into object_group(req_id,name,action,firewall_name,environment,mdata)";
											query += "values(";
											query += dbS(formId) + ",";
											query += dbS(google) + ",";
											query += dbS("use") + ",";
											query += dbS(ctrl_fw_name) + ",";
											query += dbS(goomap.get("env")) + ",";
											query += dbS(goomap.get("meta")) + ")";
											rdd_map.put(ctrl_fw_name, updateReturnId(query) + "");
										}								
										query = "insert into object_group_member_grp(grp_id,child_grp_id)";
										query += "values(" + dbS(goo_id) + "," + dbS(rdd_map.get(ctrl_fw_name)) + ")";
										update(query);
									}
								}
							}
						}				
					}
					String profile_ext = (UtilApp.isSrc(elementType) && !UtilStr.isEmpty(profileId)) ? "profile_id,":"";
					query = "insert into flow_" + dbType + "_group(flow_id," + profile_ext + "group_id,action) values(";
					query += dbS(flow_id) + ",";
					if ((UtilApp.isSrc(elementType) && !UtilStr.isEmpty(profileId)))
						query += dbS(profileId) + ",";
					query += dbS(fcr_goos.get(fw_name)) + ",";
					query += dbS(UtilApp.OBJ_SVC_ADD) + ")";
					update(query);
				}
				else
				{
					rdd_map 	= fcr_unknown;
					String oid	= ctrl_meta.get("oid");
					UtilLog.out("UNKNOWN OBJECT:" + oid + " - " + aef.get(prefix +"cat") + " - " + fw_name);
					if (!rdd_map.containsKey(oid))
					{
						query = "insert into object(req_id,type,name,action,firewall_name,mdata)";
						query += "values (";
						query += dbS(formId) + ",";
						query += dbS(aef.get(prefix +"cat")) + ",";
						query += dbS(fw_name) + ",";
						query += dbS(uc) + ",";
						query += dbS(fw_name) + ",";
						query += dbS(aef.get(prefix +"meta")) + ")";
						rdd_map.put(oid, updateReturnId(query) + "");
					}
					
					String profile_ext = (UtilApp.isSrc(elementType) && !UtilStr.isEmpty(profileId)) ? "profile_id,":"";				
					query = "insert into flow_" + dbType + "_object(flow_id," + profile_ext + "object_id,action) values(";
					query += dbS(flow_id) + ",";
					if ((UtilApp.isSrc(elementType) && !UtilStr.isEmpty(profileId)))
						query += dbS(profileId) + ",";
					query += dbS(rdd_map.get(oid)) + ",";
					query += dbS(UtilApp.OBJ_SVC_ADD) + ")";
					update(query);
				}
			}
		}
	}
	
	protected int addServiceDetails(String formId, String flow_id, String ts_name, Umap aef, String fl_idx, boolean isDupForm)
	{
		String query = "";

		for (int srv_idx = 1 ; srv_idx <= Util.getInteger(aef.get(fl_idx + "-nb-srv")) ; srv_idx++)
		{			
			String prefix 	= fl_idx + "." + srv_idx + "-srv-";
			String cat 		= aef.get(prefix + "cat");
			String uc		= !isDupForm ? aef.get(prefix + "uc") : "use";
			String fw_name 	= aef.get(prefix + "output");
			
			if (UtilStr.isX(cat, "Single service"))
			{
				String srv_id 	= "";
				
				if (!fcr_services.containsKey(fw_name))
				{
					query = "insert into service(req_id,name,protocol,svc_type,port,action,firewall_name,mdata) values(";
					query += dbS(formId) + ",";
					query += dbS(aef.get(prefix + "one-name")) + ",";
					query += dbS(aef.get(prefix + "one-proto")) + ",";
					query += dbS(aef.get(prefix + "one-type")) + ",";
					query += dbS(aef.get(prefix + "one-port")) + ",";
					query += dbS(uc) + ",";
					query += dbS(fw_name) + ",";
					query += dbS(aef.get(prefix + "meta")) + ")";				
					srv_id = updateReturnId(query) + "";					
					fcr_services.put(fw_name, srv_id);
				}
				else 
					srv_id = fcr_services.get(fw_name);
				
				query = "insert into flow_svc_service(flow_id,service_id,action)";
				query += "values(";
				query += dbS(flow_id) + ",";
				query += dbS(srv_id) + ",";
				query += dbS(UtilApp.OBJ_SVC_ADD) + ")";
				update(query);
			}
			else if (UtilStr.isX(cat, "Group of services"))
			{
				String gsrv = aef.get(prefix + "gsrv-name");
				
				if (!fcr_gsrv.containsKey(fw_name))
				{
					query = "insert into service_group(req_id,name,action,firewall_name,mdata)";
					query += "values(";
					query += dbS(formId) + ",";
					query += dbS(gsrv) + ",";
					query += dbS(uc) + ",";
					query += dbS(fw_name) + ",";
					query += dbS(aef.get(prefix + "meta")) + ")";
					String gsrv_id 	= updateReturnId(query) + "";
					
					fcr_gsrv.put(fw_name, gsrv_id);
					
					String list_srvs = aef.get(prefix + "gsrv-srvs");
					if (UtilStr.isX(uc, "create") && !UtilStr.isEmpty(list_srvs))
					{						
						List<String> srvs = Util.array2list(list_srvs.split(UtilApp.CGS_SEP));
						for (String srv_elems : srvs)
						{
							if (!UtilStr.isEmpty(srv_elems))
							{
								Umap srv 				= new Umap().setKVP(KVP.KVP3).mapsterize(srv_elems);
								String member_srv_id 	= "";
								String srv_fw_name		= srv.get("id");
								if (!fcr_services.containsKey(srv_fw_name))
								{
									query = "insert into service(req_id,name,protocol,svc_type,port,action,firewall_name,mdata) values(";
									query += dbS(formId) + ",";
									query += dbS(srv.get("name")) + ",";
									query += dbS(srv.get("proto")) + ",";
									query += dbS(srv.get("type")) + ",";
									query += dbS(srv.get("port")) + ",";
									query += dbS(srv.get("uc")) +  ",";
									query += dbS(srv_fw_name) +  ",";
									query += dbS(srv.get("meta")) + ")";
									member_srv_id = updateReturnId(query) + "";							
									fcr_services.put(srv_fw_name, member_srv_id);
								}
								else 
									member_srv_id = fcr_services.get(srv_fw_name);
								
								query = "insert into service_group_member(grp_id,svc_id)";
								query += "values(" + dbS(gsrv_id) + "," + dbS(member_srv_id) + ")";
								update(query);
							}
						}
					}
				}								
				query = "insert into flow_svc_group(flow_id,group_id,action)";
				query += "values(";
				query += dbS(flow_id) + ",";
				query += dbS(fcr_gsrv.get(fw_name)) + ",";
				query += dbS(UtilApp.OBJ_SVC_ADD) + ")";
				update(query);
			}
		}
		return 0;
	}
	
	protected void updateCatalogWithFcrForm(String formId, Umap ai)
	{
UtilLog.out("updateCatalogWithFcrForm");
		
		String ts_name	= ai.get("application-service");
		String ts_id 	= getValue("select id from used_technical_service where name=" + dbS(ts_name) , "id");
		
		if (!UtilStr.isEmpty(ts_id))
		{
			Umap aefs 		= retrieveAefInfo(formId);

			aefs.push("ts_id", ts_id);
			aefs.push("ts_name", ts_name);
			int nb_flow 	= Util.getInteger(aefs.get("f-nb-flow"));
			
			Fcrweb_Webservices myriam_ws = new Fcrweb_Webservices();
			for (int i=1 ; i<=nb_flow ; i++)
			{
				String flow_pre = "f-" + i + "-";
				if (UtilStr.isX(aefs.get(flow_pre + "profile-uc"), "create"))
				{
					String profile 	= aefs.get(flow_pre + "profile");
					String owner	= aefs.get(flow_pre + "profile-owner");
					String desc		= aefs.get(flow_pre + "profile-description");
					String pr_q 	= " insert into cat_profile(name,responsible_uid,description,technical_service_id,myriam_id) "
									+ " values(#name,#owner,#desc,#ts_id,#myriam)";
					String myriam 	= myriam_ws.create_user_group_in_myriam_ad_ldap(profile, owner, desc);
					pr_q = pr_q.replace("#name", dbS(profile))
								.replace("#owner", dbS(owner))
								.replace("#desc", dbS(desc))
								.replace("#ts_id", dbS(ts_id))
								.replace("#myriam", dbS(myriam));
					UtilLog.out("profile creation:" + pr_q);
					update(pr_q);					
				}
				
				updateCatalogWithForm_src_dest(aefs, UtilStr.unTail(flow_pre, "-"), "src");
				updateCatalogWithForm_src_dest(aefs, UtilStr.unTail(flow_pre, "-"), "dest");
				updateCatalogWithForm_srv(aefs, UtilStr.unTail(flow_pre, "-"));				
			}
		}
	}
	
	private void updateCatalogWithForm_src_dest(Umap aefs, String flow_pre, String elemType)
	{
		int nb_ctrl 			= Util.getInteger(aefs.get(flow_pre + "-nb-" + elemType));
		for (int i=1 ; i<=nb_ctrl ; i++)
		{
			String ctrl_pre			= flow_pre + "." + i + "-" + elemType + "-";
			if (UtilStr.isX(aefs.get(ctrl_pre + "uc"), "create"))
			{
				String fw_name 	= aefs.get(ctrl_pre + "output");
				String cat 		= aefs.get(ctrl_pre + "cat");
				
				if (UtilStr.isX(cat, "Host"))
				{
					/** check if object has not been already created by fcrweb **/
					if (UtilStr.isEmpty(getValue("select firewall_name from cat_host where firewall_name="+ dbS(fw_name) + " and manually_created=" + dbS("1") , "firewall_name")))
					{					 
						String query 	= "insert into cat_host (organization,environment,name,ip,tsl_ip,firewall_name,manually_created) ";
						query 			+= "values(#organization,#environment,#name,#ip,#tsl_ip,#firewall_name,'1')";
						query 			= query.replace("#organization",dbS(aefs.get(ctrl_pre + "org")))
										.replace("#environment",dbS(aefs.get(ctrl_pre + "host-env")))
										.replace("#name",dbS(aefs.get(ctrl_pre + "host")))
										.replace("#ip",dbS(aefs.get(ctrl_pre + "ip")))
										.replace("#tsl_ip",dbS(aefs.get(ctrl_pre + "ip-tsl")))
										.replace("#firewall_name",dbS(fw_name));
						update(query);
					}					
				}
				else if (UtilStr.isX(cat, "Subnet"))
				{
					/** check if object has not been already created by fcrweb **/
					if (UtilStr.isEmpty(getValue("select firewall_name from cat_subnet where firewall_name="+ dbS(fw_name) + " and manually_created=" + dbS("1") , "firewall_name")))
					{
						String query 		= "insert into cat_subnet (organization,zone_id,ip,mask,firewall_name,subnet_type,manually_created) ";
								query 		+= "values(#organization,#zone_id,#ip,#mask,#firewall_name,'public','1')";
						String[] ip_mask 	= aefs.get(ctrl_pre + "subnet").split("/");
						query 				= query.replace("#organization",dbS(aefs.get(ctrl_pre + "org")))
											.replace("#zone_id",dbS(aefs.get(ctrl_pre + "subnet-area-id")))
											.replace("#ip",dbS(ip_mask[0]))
											.replace("#mask",dbS(ip_mask[1]))
											.replace("#firewall_name",dbS(fw_name));
						update(query);
					}					
				}
				else if (UtilStr.isX(cat, "Group of objects"))
				{
					/** check if object has not been already created by fcrweb **/
					if (UtilStr.isEmpty(getValue("select firewall_name from cat_object_group where firewall_name="+ dbS(fw_name) + " and manually_created=" + dbS("1") , "firewall_name")))
					{
						String org 		= aefs.get(ctrl_pre + "org");
						String query 	= "insert into cat_object_group(organization,environment,name,technical_service_id,firewall_name,manually_created)";
								query	+= "values(#organization,#environment,#name,#technical_service_id,#firewall_name,'1')";
								query 	= query.replace("#organization",dbS(org))
										.replace("#environment",dbS(aefs.get(ctrl_pre + "goo-env")))
										.replace("#name",dbS(aefs.get(ctrl_pre + "goo")))
										.replace("#technical_service_id",dbS(aefs.get("ts_id")))
										.replace("#firewall_name",dbS(fw_name));
						String og_id 	= updateReturnId(query) + "";
						
						if (!UtilStr.isEmpty(og_id))
						{
							for (String host : aefs.get(ctrl_pre + "hosts").split(UtilApp.GOO_SEP))
							{
								if (!UtilStr.isEmpty(host))
								{
									Umap map 			= new Umap().setKVP(KVP.KVP3).mapsterize(host);
									String o_id 		= "";
									String o_fw_name 	= map.get("id");									
									o_id 				= getValue("select id from cat_host where firewall_name="+ dbS(o_fw_name) , "id");
									/** check if object has not been already created by fcrweb **/
									if (UtilStr.isEmpty(o_id) & UtilStr.isX(map.get("uc"), "create"))
									{	
										String o_q 	= "insert into cat_host (organization,environment,name,ip,tsl_ip,firewall_name,manually_created) ";
										o_q 		+= "values(#organization,#environment,#name,#ip,#tsl_ip,#firewall_name,'1')";
										o_q 		= o_q.replace("#organization",dbS(org)).replace("#environment",dbS(map.get("env"))).replace("#name",dbS(map.get("host")))
													.replace("#ip",dbS(map.get("ip"))).replace("#tsl_ip",dbS(map.get("ip-tsl"))).replace("#firewall_name",dbS(o_fw_name));
										o_id 		= updateReturnId(o_q) + "";										
									}
									
									if (!UtilStr.isEmpty(o_id))
									update
									(
										"insert into cat_object_group_member(object_group_id,object_id,object_type)" + 
										"values(" + dbS(og_id) + "," + dbS(o_id) + ",'host')"
									);
								}
							}
							for (String subnet : aefs.get(ctrl_pre + "subnets").split(UtilApp.GOO_SEP))
							{
								if (!UtilStr.isEmpty(subnet))
								{
									Umap map 			= new Umap().setKVP(KVP.KVP3).mapsterize(subnet);
									String o_id 		= "";
									String o_fw_name 	= map.get("id");									
									o_id 				= getValue("select id from cat_subnet where firewall_name="+ dbS(o_fw_name) , "id");
									/** check if object has not been already created by fcrweb **/
									if (UtilStr.isEmpty(o_id) & UtilStr.isX(map.get("uc"), "create"))
									{	
										String[] ip_mask 	= map.get("subnet").split("/");
										String o_q 			= "insert into cat_subnet (organization,zone_id,ip,mask,firewall_name,manually_created) ";
										o_q 				+= "values(#organization,#zone_id,#ip,#mask,#firewall_name,'1')";
										o_q 				= o_q.replace("#organization",dbS(org)).replace("#ip",dbS(ip_mask[0]))
															.replace("#mask",dbS(ip_mask[1])).replace("#firewall_name",dbS(o_fw_name))
															.replace("#zone_id",dbS(map.get("area-id")));
										o_id 				= updateReturnId(o_q) + "";										
									}
									
									if (!UtilStr.isEmpty(o_id))
									update
									(
										"insert into cat_object_group_member(object_group_id,object_id,object_type)" + 
										"values(" + dbS(og_id) + "," + dbS(o_id) + ",'subnet')"
									);
								}
							}
							for (String goo : aefs.get(ctrl_pre + "goos").split(UtilApp.GOO_SEP))
							{
								if (!UtilStr.isEmpty(goo))
								{
									Umap map 			= new Umap().setKVP(KVP.KVP3).mapsterize(goo);
									String o_fw_name 	= map.get("id");									
									String o_id 		= getValue("select id from cat_object_group where firewall_name="+ dbS(o_fw_name) , "id");
									
									if (!UtilStr.isEmpty(o_id))
									update
									(
										"insert into cat_object_group_member(object_group_id,object_id,object_type)" + 
										"values(" + dbS(og_id) + "," + dbS(o_id) + ",'group')"
									);
								}
							}
						}
					}
				}
			}
		}		
	}
	
	private void updateCatalogWithForm_srv(Umap aefs, String flow_pre)
	{
		String elemType	= "srv";
		String ts_id	= aefs.get("ts_id");
		int nb_ctrl 	= Util.getInteger(aefs.get(flow_pre + "-nb-" + elemType));
		for (int i=1 ; i<=nb_ctrl ; i++)
		{
			String ctrl_pre			= flow_pre + "." + i + "-" + elemType + "-";
			if (UtilStr.isX(aefs.get(ctrl_pre + "uc"), "create"))
			{
				String fw_name 	= aefs.get(ctrl_pre + "output");
				String cat 		= aefs.get(ctrl_pre + "cat");
				
				if (UtilStr.isX(cat, "Single service"))
				{
					/** check if object has not been already created by fcrweb **/
					if (UtilStr.isEmpty(getValue("select firewall_name from cat_service where firewall_name="+ dbS(fw_name) + " and manually_created=" + dbS("1") , "firewall_name")))
					{					 
						String query 	= "insert into cat_service (name,protocol,port,technical_service_id,firewall_name,manually_created) ";
						query 			+= "values(#name,#protocol,#port,#technical_service_id,#firewall_name,'1')";
						query 			= query.replace("#name",dbS(aefs.get(ctrl_pre + "one-name")))
										.replace("#protocol",dbS(aefs.get(ctrl_pre + "one-proto")))
										.replace("#port",dbS(aefs.get(ctrl_pre + "one-port")))
										.replace("#technical_service_id",dbS(ts_id))
										.replace("#firewall_name",dbS(fw_name));
						update(query);
					}					
				}
				else if (UtilStr.isX(cat, "Group of services"))
				{
					/** check if object has not been already created by fcrweb **/
					if (UtilStr.isEmpty(getValue("select firewall_name from cat_service_group where firewall_name="+ dbS(fw_name) + " and manually_created=" + dbS("1") , "firewall_name")))
					{
						String query 	= "insert into cat_service_group(name,technical_service_id,firewall_name,manually_created)";
								query	+= "values(#name,#technical_service_id,#firewall_name,'1')";
								query 	= query.replace("#name",dbS(aefs.get(ctrl_pre + "gsrv-name")))
										.replace("#technical_service_id",dbS(ts_id))
										.replace("#firewall_name",dbS(fw_name));
						String sg_id 	= updateReturnId(query) + "";
						
						if (!UtilStr.isEmpty(sg_id))
						{
							for (String host : aefs.get(ctrl_pre + "gsrv-srvs").split(UtilApp.GOO_SEP))
							{
								if (!UtilStr.isEmpty(host))
								{
									Umap map 			= new Umap().setKVP(KVP.KVP3).mapsterize(host);
									String s_id 		= "";
									String s_fw_name 	= map.get("id");									
									s_id 				= getValue("select id from cat_service where firewall_name="+ dbS(s_fw_name) , "id");
									/** check if object has not been already created by fcrweb **/
									if (UtilStr.isEmpty(s_id) & UtilStr.isX(map.get("uc"), "create"))
									{	
										String s_q 	= "insert into cat_service (name,protocol,port,technical_service_id,firewall_name,manually_created) ";
										s_q 		+= "values(#name,#protocol,#port,#technical_service_id,#firewall_name,'1')";
										s_q 		= s_q.replace("#name",dbS(map.get("name"))).replace("#protocol",dbS(map.get("proto")))
													.replace("#port",dbS(map.get("port"))) .replace("#technical_service_id",dbS(ts_id))
													.replace("#firewall_name",dbS(s_fw_name));
										s_id 		= updateReturnId(s_q) + "";										
									}
									
									if (!UtilStr.isEmpty(s_id))
									update
									(
										"insert into cat_service_group_member(service_group_id,service_id,service_type)" + 
										"values(" + dbS(sg_id) + "," + dbS(s_id) + ",'service')"
									);
								}
							}							
						}
					}
				}
			}
		}		
	}
	
}
