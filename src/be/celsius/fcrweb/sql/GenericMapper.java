/**
 * 
 */
package be.celsius.fcrweb.sql;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import com.sun.xml.internal.fastinfoset.stax.events.Util;

import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.SeedBean;

/**
 * @author jhuilian
 *
 */
public class GenericMapper implements RowMapper{

	private String attributes;					//table column names as string eg:"id,name"	
	private Map<String,String> column_mapper;	//database table columns as map eg {id:identifier,name:lastname}
	
	public GenericMapper(String attributes)
	{
		this.attributes 	= attributes;
		this.column_mapper 	= new HashMap<String,String>();
	}
	
	public GenericMapper(String attributes, Map<String,String> column_mapper)
	{
		this.attributes 	= attributes;
		this.column_mapper = column_mapper;
	}
	
	public BoxBean mapRow(ResultSet rs, int rowNum)
	{
		BoxBean rowElement = new BoxBean();
		
		try{
			for (String attr : attributes.split(","))
			{
				String value = rs.getString(attr);
				SeedBean val = new SeedBean("");
				if (!UtilStr.isEmpty(value))	
					val = new SeedBean(value);
					//val = new SeedBean(UtilStr.server2html(value));
				rowElement.put(column_mapper.containsKey(attr)?column_mapper.get(attr):attr, val);
			}
		}
		catch(Exception e)
		{
			UtilLog.printException(e);
		}
		return rowElement;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}	
}
