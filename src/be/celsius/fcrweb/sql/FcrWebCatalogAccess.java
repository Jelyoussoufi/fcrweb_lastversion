package be.celsius.fcrweb.sql;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import be.celsius.util.BonitaJ;
import be.celsius.util.Config;
import be.celsius.util.JsonArray;
import be.celsius.util.JsonObject;
import be.celsius.util.Util;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilNet;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.datastructure.Ulist;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Uset;
import be.celsius.util.datastructure.Umap.KVP;

public class FcrWebCatalogAccess extends GenericJDBC 
{
	private BonitaJ bonita;
	
	public FcrWebCatalogAccess()
	{
		super(Config.DB_FCRWEB_JNDI);
		bonita = new BonitaJ();
	}
	
	public Umap getApplicationConfigs()
	{
		BasketBean rows = execute("select param,val from configs", "param,val");
		Umap configs = new Umap();
		
		for (LeafBean row : rows.getBasket())
		{
			BoxBean config = row.toBox();
			configs.put(config.get("param").toSeed().getSeed(), config.get("val").toSeed().getSeed());
		}
		return configs;
	}
	
	public JsonArray getApplicationHelpMsg()
	{
		BasketBean rows = execute("select name,form_type,msg_type,description,text from configs_help_msg", "name,form_type,msg_type,description,text");
		UtilLog.out(rows.toJson().toString());
		
		JsonArray configs = new JsonArray(); 
		
		for (LeafBean leaf : rows.getBasket())
		{
			BoxBean chm = leaf.toBox();
			configs.put
			(
				new JsonObject()
					.put("name", chm.get("name").toSeed().getSeed())
					.put("form_type", chm.get("form_type").toSeed().getSeed())
					.put("msg_type", chm.get("msg_type").toSeed().getSeed())
					.put("description", chm.get("description").toSeed().getSeed())
					.put("text", chm.get("text").toSeed().getSeed())
			);
		}
		return configs;
	}
	
	public BasketBean getHosts(String where_order_clause)
	{
		String attrs = "id,name,ip,tsl_ip,organization,environment,firewall_name";
		return execute("select " + attrs + " from cat_host " + where_order_clause, attrs); 
	}
	
	/**
	 * 
	 * @return the area of a given ip address (only for mobistar)
	 */
	public String getHostArea(String ip)
	{
		for (LeafBean leaf : getSubnets("where organization='MOBISTAR'").getBasket())
		{
			BoxBean subnet = leaf.toBox();
			if (UtilNet.isInSubnet(ip, subnet.get("ip").toSeed().getSeed(), UtilNet.bitmask2ip(subnet.get("mask").toSeed().getSeed())))
				return subnet.get("area").toSeed().getSeed();
		}
		return "";
	}
	
	public BasketBean getSubnets(String where_order_clause)
	{
		String attrs = "id,area,ip,mask,description,subnet_type,organization,firewall_name";
		String attrs_r = "cs.id id, cz.name area, cs.ip ip, cs.mask mask, cs.description description,cs.subnet_type subnet_type,cs.organization organization, cs.firewall_name firewall_name";
		// caution if whereclause is defined then it must be written as 'where'
		where_order_clause = UtilStr.hasX(where_order_clause, "where") ? where_order_clause.replace("where", "where cz.id=cs.zone_id and ") : " where cz.id=cs.zone_id " + where_order_clause;
		if (UtilStr.hasX(where_order_clause, "organization")) 
			where_order_clause = where_order_clause.replace("organization", "cs.organization");
		
		String query = "select " + attrs_r + " from cat_subnet cs, cat_zone cz " + where_order_clause;
		query = "(" + query + ") union (" + query.replace("cz.name", "''").replace("cz.id=cs.zone_id", "(cs.zone_id='' or cs.zone_id is null)") + ")";
		return execute(query, attrs); 
	}
	
	public BasketBean getAreas(String where_order_clause)
	{
		String attrs = "id,name,description,organization";
		return execute("select " + attrs + " from cat_zone " + where_order_clause, attrs); 
	}
	
	/**
	 * @param ts_name the technical service name
	 * @param [organization]  MOBISTAR|OLU|MES|EXTERNAL
	 * @param [action_type_set] ::= vpn_encryption | user_authentication 
	 * @return
	 */
	public BasketBean getObjectGroups(String elem_name, String ts_name, String org, String wc_ext)
	{
		String whereClause = "";
		if (!UtilStr.isEmpty(org))
			whereClause += "and ( organization=" + dbS(org) + " or organization='') ";
		if (!UtilStr.isEmpty(wc_ext))
			whereClause += "and " + wc_ext + " "; 
		return getElements_TSC
		(
			elem_name, 
			ts_name, 
			whereClause, 
			"order by name asc", 
			"cat_object_group", 
			"id,name,organization,environment,predefined,firewall_name,vpn,user", 
			"t_id.id id, t_id.name name, t_id.organization organization, t_id.environment environment, t_id.predefined predefined, t_id.firewall_name firewall_name, t_id.vpn_encryption vpn, t_id.user_authentication user");
	}
		
	public BasketBean getServices(String elem_name, String ts_name, String whereClause)
	{
		if (!UtilStr.isEmpty(whereClause))
			whereClause = "and " + whereClause + " "; 
		return getElements_TSC(elem_name, ts_name, whereClause, "order by name asc", "cat_service", "id,name,type,protocol,port,firewall_name", "t_id.id id, t_id.name name, t_id.svc_type type, t_id.protocol protocol, t_id.port port, t_id.firewall_name firewall_name");
	}
	
	public BasketBean getServiceGroups(String elem_name, String ts_name, String where_clause)
	{
		if (!UtilStr.isEmpty(where_clause))
			where_clause = "and " + where_clause + " "; 
		return getElements_TSC(elem_name, ts_name, where_clause, "order by name asc", "cat_service_group", "id,name,firewall_name,predefined", "t_id.id id, t_id.name name, t_id.firewall_name firewall_name, t_id.predefined predefined");
	}
	
	public BasketBean getProfiles(String elem_name, String ts_name)
	{
		return getElements_TSC(elem_name, ts_name, "", "order by name asc", "cat_profile", "id,name,owner,description", "t_id.id id, t_id.name name, t_id.responsible_uid owner, t_id.description description");
	}

	protected BasketBean getElements_TSC(String elem_name, String ts_name, String whereClause_ext, String orderClause, String table, String attrs, String attrs_map)
	{

		String query = "select distinct " + attrs_map + ", t_id.predefined global from " + table + " t_id, used_technical_service uts ";

		String whereClause = "";
		if (!UtilStr.isEmpty(elem_name))
			whereClause += "t_id.name='" + elem_name + "'"; 
		if (!UtilStr.isEmpty(ts_name))
			whereClause += "and (t_id.predefined='1' or (t_id.technical_service_id=uts.id and uts.name='" + ts_name + "') )";		

		whereClause = UtilStr.unHead(whereClause + whereClause_ext, "and ");
		
		if (!UtilStr.isEmpty(whereClause))
			query += " where " + whereClause;
		query += " " + orderClause;
		
		return execute(query, attrs + ",global");
	}	
	
	public BasketBean getServiceGroupMembers(String group, String ts_name)
	{
		String 	query = " select distinct cs.firewall_name fw_name, cs.name name, cs.protocol proto, cs.port port"; 
				query += " from cat_service cs, cat_service_group csg, cat_service_group_member csgm, used_technical_service uts ";
				query += " where csg.name='" + group + "' and csgm.service_group_id=csg.id and cs.id=csgm.service_id ";
				query += " and (csg.predefined='1' or (csg.technical_service_id=uts.id and uts.name='" + ts_name + "')) ";
		return execute(query, "fw_name,name,proto,port");
	}
	
	public BasketBean getObjectGroupMembers(String group, String ts_name, String org)
	{
		String 	query = " select distinct 'host' type,ch.firewall_name fw_name, ch.name arg1, ch.ip arg2, ch.tsl_ip arg3 , cog1.predefined global";
				query += " from cat_host ch, cat_object_group cog1, cat_object_group_member cogm1, used_technical_service uts1 ";
				query += " where cog1.name='" + group + "' and cogm1.object_group_id=cog1.id and ch.id=cogm1.object_id and cogm1.object_type='host' ";
				query += " and (cog1.predefined='1' or (cog1.technical_service_id=uts1.id and uts1.name='" + ts_name + "')) ";
				query += " and (cog1.organization=" + dbS(org) + " or cog1.organization='' ) ";
				query += " union ";
				query += " select distinct 'subnet' type, cs.firewall_name fw_name, concat(cs.ip, '/', cs.mask) arg1, cs.zone_id arg2, '' arg3, cog2.predefined global ";
				query += " from cat_subnet cs, cat_object_group cog2, cat_object_group_member cogm2, used_technical_service uts2 ";
				query += " where cog2.name='" + group + "' and cogm2.object_group_id=cog2.id and cs.id=cogm2.object_id and cogm2.object_type='subnet' ";
				query += " and (cog2.predefined='1' or (cog2.technical_service_id=uts2.id and uts2.name='" + ts_name + "')) ";
				query += " and (cog2.organization=" + dbS(org) + " or cog2.organization='' ) ";
				query += " union ";
				query += " select distinct 'goo' type, co.firewall_name fw_name, co.name arg1, '' arg2, '' arg3, cog3.predefined global ";
				query += " from cat_object_group co, cat_object_group cog3, cat_object_group_member cogm3, used_technical_service uts3 ";
				query += " where cog3.name='" + group + "' and cogm3.object_group_id=cog3.id and co.id=cogm3.object_id and cogm3.object_type='group' ";
				query += " and (cog3.predefined='1' or (cog3.technical_service_id=uts3.id and uts3.name='" + ts_name + "')) ";
				query += " and (cog3.organization=" + dbS(org) + " or cog3.organization='') ";
				
		return execute(query, "type,fw_name,arg1,arg2,arg3");
	}	
	
	public Uset getTS_Owners()
	{
		Uset tso 		= new Uset();
		String query 	= " select distinct owner from cat_technical_service where owner is not null ";
				query 	+= " union ";
				query 	+= " select distinct owner from used_technical_service where owner is not null ";
		for (LeafBean leaf : execute(query, "owner").getBasket())
		{
			tso.push(leaf.toBox().get("owner").toSeed().getSeed());
		}
		return tso;
	}
	
	public void request_TS_revalidation()
	{
		/** set to all TS a period (month) for revalidation **/
		init_TS_reval_month();
		int reval_month = Calendar.getInstance().get(Calendar.MONTH);
		update("update cat_technical_service set reval_status='revalidation', reval_task_uid='', reval_proc_uid='' where reval_month=" + dbS( reval_month + ""));
		
		for (LeafBean leaf : getTechnicalServices("where reval_status='revalidation'").getBasket())
		{
			BoxBean box = leaf.toBox();	
			
			if (UtilStr.isX(box.get("reval_month").toSeed().getSeed(), reval_month + ""))
				update("update link_rule_technical_service set decision='Keep', flag='false' where technical_service_id=" + dbS(box.get("id").toSeed().getSeed()));
			
			startBonitaProcess
			(
				box.get("owner").toSeed().getSeed(), 
				"FCR_Web_TSO_Reval", 
				new Umap()
					.push("a_at_celsius", Config.atCelsius + "")
					.push("pr_ts_id", box.get("id").toSeed().getSeed())
					.push("pr_ts_name", box.get("name").toSeed().getSeed())
			);
		}
	}
	
	public Map<String,String> init_TS_reval_month()
	{
		Map<String,String> debug 	= new HashMap<String,String>();
		int[] ts_r_month_count 		= new int[12];
		
		for (LeafBean leaf : getTechnicalServices("where reval_month is not null").getBasket())
			ts_r_month_count[Util.getInteger(leaf.toBox().get("reval_month").toSeed().getSeed())]++;
		
		for (LeafBean leaf : getTechnicalServices("where reval_month is null").getBasket())
		{		
			int min_index 	= getMinIndex(ts_r_month_count);
			update("update cat_technical_service set reval_month=" + dbS(min_index + "")  + " where id=" + dbS(leaf.toBox().get("id").toSeed().getSeed()));
			ts_r_month_count[min_index]++;
		}		
		return debug;
	}
	
	private int getMinIndex(int[] array)
	{
		int min=Integer.MAX_VALUE;
		int min_index = -1;
		for (int i=0 ; i<array.length ; i++)
		{
			if (array[i]<min)
			{
				min = array[i];
				min_index = i;
			}
		}
		return min_index;
	}
	
	public BasketBean getTechnicalServices(String whereclause)
	{
		String attrs 	= "id,name,owner,description,reval_month,reval_status,reval_last_update,reval_proc_uid,reval_task_uid"; 
		String query 	= " select " + attrs + " "
						+ " from cat_technical_service "
						+ (UtilStr.isEmpty(whereclause) ? "" : whereclause);
		return execute(query, attrs);
	}
	
	public BasketBean getLinkRuleTS(String whereclause)
	{
		String attrs 	= "rule_name,technical_service_id,decision,flag"; 
		String query 	= " select " + attrs + " "
						+ " from link_rule_technical_service "
						+ (UtilStr.isEmpty(whereclause) ? "" : whereclause);
		return execute(query, attrs);
	}
	
	public Ulist[] getPrivateRanges()
	{
		Ulist[] ranges = new Ulist[2];
		ranges[0] =  new Ulist();
		ranges[1] =  new Ulist();
		for (LeafBean row : execute("select range_start start,range_end end from private_ranges", "start,end").getBasket())
		{
			BoxBean range = row.toBox();
			ranges[0].push(range.get("start").toSeed().getSeed());
			ranges[1].push(range.get("end").toSeed().getSeed());
		}
		return ranges;
	}
	
	protected String toMySqlDate(String date)
	{
		if (UtilStr.isEmpty(date))
			return "0000-00-00";
		else
		{
			String[] dd_mm_yyyy = date.split("/");			
			return dd_mm_yyyy[2]  + "-" + dd_mm_yyyy[1] + "-" + dd_mm_yyyy[0];
		}
	}
	
	protected static String getMySqlDate(String date)
	{
		if (UtilStr.isEmpty(date) | UtilStr.isX(date, "0000-00-00"))
			return "";
		else
		{
			String[] yyyy_mm_dd = date.split("-");			
			return yyyy_mm_dd[2]  + "/" + yyyy_mm_dd[1] + "/" + yyyy_mm_dd[0];
		}
	}
	
	public String startBonitaProcess(String login, String processName, Umap args)
	{
		Umap configs = getApplicationConfigs();
		bonita.clearVariables();
		bonita.setBonitaServer(configs.get("bonitaRestServer"));
		bonita.setBonitaPort(Util.getInteger(configs.get("bonitaRestPort")));
		bonita.setBonitaAuthUser(configs.get("bonitaRestUser"));
		bonita.setBonitaAuthPassword(configs.get("bonitaRestPWD"));
		bonita.setBonitaUPB64(UtilStr.html2server(configs.get("bonitaRestUPB64")));		
		bonita.setLogin(login);
		for (String key : args.keySet())
			bonita.addVariable(key, args.get(key));
		String logs = bonita.instantiateProcessWithVariables(processName, "1.0");
UtilLog.out(((Umap)bonita.getConnectionConfiguration()).parameterize());
UtilLog.out("login:" + login);
UtilLog.out("args:" + args);
UtilLog.out("logs:" + logs);
		return logs;
	}
}
