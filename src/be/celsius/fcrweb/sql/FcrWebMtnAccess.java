package be.celsius.fcrweb.sql;

import java.util.HashMap;
import java.util.Map;

import be.celsius.util.Util;
import be.celsius.util.UtilApp;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Umap.KVP;

public class FcrWebMtnAccess extends FcrWebAccess
{
	public FcrWebMtnAccess()
	{
		super();
	}
	
	public void initializeMtnGrp(String formId, Umap ai)
	{
		try{
		String ts_name 		= ai.get("application-service");
		String wc 			= " t_id.id in (" + ai.get("maintain-ids") + ")";
		boolean isMtnGOO 	= UtilApp.isMtnGOO(ai.get("fcr-mode"));
		BasketBean grps		= isMtnGOO ? getObjectGroups("", ts_name, "", wc) : getServiceGroups("", ts_name, wc);				
		Umap mtns 			= new Umap()
							.push("g-nb-group", grps.getBasket().size() + "")
							.push("g-type", isMtnGOO ? "mtn-goo" : "mtn-gsrv");

		for(int i=0 ; i<grps.getBasket().size() ; i++)
		{
			BoxBean grp 	= grps.getBasket().get(i).toBox();
			String grp_pfx 	= isMtnGOO ? "g-" + (i+1) + "-goo-" : "g-" + (i+1) + "-gsrv-";
			mtns.push(grp_pfx + "output", grp.get("firewall_name").toSeed().getSeed());
			if (isMtnGOO)
			{
				mtns.push(grp_pfx + "org", grp.get("organization").toSeed().getSeed());
				mtns.push(grp_pfx + "env", grp.get("environment").toSeed().getSeed());
			}
			mtns.push(grp_pfx + "name", grp.get("name").toSeed().getSeed());			
			mtns.push(grp_pfx + "action", "modify");
		}
		updateMtnFormInDatabase(formId, mtns);
		}catch (Exception e){UtilLog.printException(e);}
	}
	
	public Map<String,Umap> retrieveMtnForm(String formId)
	{

		Map<String,Umap> collections 	= new HashMap<String,Umap>();				
		Umap ai 						= retrieveAdminAndTechInfo(formId);	 
		Umap ret						= getRequestEditionTaskRefs(formId);
		Umap mtns 						= new Umap().setName(UtilApp.HD_MTN);

		if (UtilApp.isMtnGOO(ai.get("fcr-mode")))
			mtns = retrieveMtnGoo(formId);
		else if (UtilApp.isMtnGSRV(ai.get("fcr-mode")))
			mtns = retrieveMtnGsrv(formId);
UtilLog.out("form:" + mtns.toJson().toString());
		collections.put("adminInfo", ai);
		collections.put("requestEditionTask", ret);
		collections.put("docInfo", retrieveDocInfo(formId));
		collections.put("mtns", mtns);
		return collections;
	}
	
	private Umap retrieveMtnGoo(String formId)
	{
		try
		{
	UtilLog.out("retrieveMtnGoo:" + formId);
			//initAreaNameMap();
			
			String attrs = "f_id,fsg_id,head_id,head_name,head_action,head_org,head_rename,head_fw_name,head_env,ogm_id,o_id,o_type,o_name,o_ip,o_mask,o_area_id,o_action,o_nat,o_fw_name,o_env,ogmg_id,og_id,og_name,og_action,og_fw_name,og_env"; 
			String query = "";
			// retrieve object group and its object members
			query += " SELECT f.id f_id, fsg.id fsg_id, head.id head_id, head.name head_name, head.action head_action, head.organization head_org, '' head_rename, head.firewall_name head_fw_name, head.environment head_env, ogm.id ogm_id, ";
			query += " o.id o_id, o.type o_type, o.name o_name, o.ipaddress o_ip, o.netmask o_mask, o.zone_id o_area_id, o.action o_action, o.ipaddress_nat o_nat, o.firewall_name o_fw_name, o.environment o_env, '' ogmg_id, '' og_id, '' og_name, '' og_action, '' og_fw_name, '' og_env "; 
			query += " FROM flow f, flow_src_group fsg, object_group head, object_group_member ogm, object o "; 
			query += " WHERE f.request_id = " + dbS(formId) + " AND fsg.flow_id = f.id AND head.id = fsg.group_id AND ogm.grp_id = head.id AND o.id = ogm.obj_id ";
			query += " UNION ";
			// retrieve object group and its object group members
			query += " SELECT f.id f_id, fsg.id fsg_id, head.id head_id, head.name head_name, head.action head_action, head.organization head_org, '' head_rename, head.firewall_name head_fw_name, head.environment head_env, '' ogm_id, "; 
			query += " '' o_id, '' o_type, '' o_name, '' o_ip, '' o_mask, '' o_area_id, '' o_action, '' o_nat, '' o_fw_name, '' o_env, ogmg.id ogmg_id, og.id og_id, og.name og_name, og.action og_action, og.firewall_name og_fw_name, og.environment og_env"; 
			query += " FROM flow f, flow_src_group fsg, object_group head, object_group_member_grp ogmg, object_group og "; 
			query += " WHERE f.request_id = " + dbS(formId) + " AND fsg.flow_id = f.id AND head.id = fsg.group_id AND ogmg.grp_id = head.id AND og.id = ogmg.child_grp_id ";
			query += " UNION ";
			// retrieve update and delete object group without members
			query += " SELECT f.id f_id, fsg.id fsg_id, head.id head_id, head.name head_name, head.action head_action, head.organization head_org, head.opt_parameter head_rename, head.firewall_name head_fw_name, head.environment head_env, '' ogm_id, "; 
			query += " '' o_id, '' o_type, '' o_name, '' o_ip, '' o_mask, '' o_area_id, '' o_action, '' o_nat, '' o_fw_name, '' o_env, '' ogmg_id, '' og_id, '' og_name, '' og_action, '' og_fw_name, '' og_env"; 
			query += " FROM flow f, flow_src_group fsg, object_group head  ";
			query += " WHERE f.request_id = " + dbS(formId) + " AND fsg.flow_id = f.id AND head.id = fsg.group_id  ";
			query += " and (head.action='rename' or head.action='delete' or head.action='modify') ";
			Umap mtn		= new Umap().setName(UtilApp.HD_MTN);
			Umap rdd		= new Umap();
			int count 		= 0;		
			String gp		= "g-";
			String ifx		= "-goo-";
			
			for (LeafBean leaf : execute(query, attrs).getBasket())
			{
				BoxBean o 		= leaf.toBox();
				String head_id 	= o.get("head_id").toSeed().getSeed();
				String grp_idx	= "";
				if (rdd.containsKey(head_id))
				{
					/** existent object group **/
					grp_idx = rdd.get(head_id);
				}else{
					/** new object group **/
					count ++;
					grp_idx = count + "";
					rdd.push(head_id, count + "");
					String action 	= o.get("head_action").toSeed().getSeed();
					String name		= o.get("head_name").toSeed().getSeed();
					String fw_name 	= o.get("head_fw_name").toSeed().getSeed();
					mtn.push(gp + grp_idx + ifx + "action", action);				
					mtn.push(gp + grp_idx + ifx + "output", fw_name);
					mtn.push(gp + grp_idx + ifx + "name", name);				
					mtn.push(gp + grp_idx + ifx + "org", o.get("head_org").toSeed().getSeed());
					mtn.push(gp + grp_idx + ifx + "env", o.get("head_env").toSeed().getSeed());
					if (UtilStr.isX(action, "rename"))
					{
						String rename = o.get("head_rename").toSeed().getSeed();
						mtn.push(gp + grp_idx + ifx + "new-name", rename);
						mtn.push(gp + grp_idx + ifx + "new-name-fw", fw_name.replaceAll(name.replaceAll(" ", "_"), rename.replaceAll(" ", "_")));
					}
				}
				
				/** add object group member **/
				String action = o.get("head_action").toSeed().getSeed();			
				if (UtilStr.isX(action, "create") | UtilStr.isX(action, "update"))
				{
					String members 	= "";
					String grp		= "";
					Umap member 	= new Umap().setKVP(KVP.KVP3);				
					String o_type 	= o.get("o_type").toSeed().getSeed();
					if (UtilStr.isX(o_type,"host"))
					{
						grp		= "hosts";
						members = mtn.get(gp + grp_idx + ifx + grp);
						member
						.push("cat", o.get("o_type").toSeed().getSeed())
						.push("uc", o.get("o_action").toSeed().getSeed())
						.push("output", o.get("o_fw_name").toSeed().getSeed())					
						.push("host", o.get("o_name").toSeed().getSeed())
						.push("ip", o.get("o_ip").toSeed().getSeed())
						.push("ip-tsl", o.get("o_nat").toSeed().getSeed())
						.push("env", o.get("o_env").toSeed().getSeed());
					}
					else if (UtilStr.isX(o_type,"subnet"))
					{
						grp				= "subnets";
						members 		= mtn.get(gp + grp_idx + ifx + grp);
						String subnet 	= o.get("o_ip").toSeed().getSeed() + "/" + o.get("o_mask").toSeed().getSeed();
						String area_id	= o.get("o_area_id").toSeed().getSeed();
						member
						.push("cat", o.get("o_type").toSeed().getSeed())					
						.push("uc", o.get("o_action").toSeed().getSeed())
						.push("subnet", subnet)					
						.push("area-id", area_id)
						//.push("area", areaid_2_areaname.get(area_id))					
						.push("output", o.get("o_fw_name").toSeed().getSeed())
						.push("ip", subnet);
					}
					else if (!UtilStr.isEmpty(o.get("og_name").toSeed().getSeed()))
					{
						grp		= "goos";
						members = mtn.get(gp + grp_idx + ifx + grp);
						member
						.push("cat", "goo")					
						.push("uc", o.get("og_action").toSeed().getSeed())
						.push("output", o.get("og_fw_name").toSeed().getSeed())
						.push("name", o.get("og_name").toSeed().getSeed())
						.push("env", o.get("og_env").toSeed().getSeed());
						
					}
					mtn.push(gp + grp_idx + ifx + grp, UtilStr.isEmpty(members) ? member.parameterize() : members + UtilApp.GOO_SEP + member.parameterize());
				}
			}
			return mtn.push("g-nb-group", count + "");
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
			return new Umap();
		}	
	}
	
	private Umap retrieveMtnGsrv(String formId)
	{
		String attrs = "gsrv_id,gsrv_req_id,gsrv_name,gsrv_action,gsrv_opt,gsrv_fw_name,srv_name,srv_proto,srv_port,srv_action,srv_fw_name"; 
		String query = "";
		// retrieve object group and its object members
		query += " select sg.id gsrv_id, sg.req_id gsrv_req_id, sg.name gsrv_name, sg.action gsrv_action, sg.opt_parameter gsrv_opt, sg.firewall_name gsrv_fw_name, s.name srv_name, s.protocol srv_proto, s.port srv_port, s.action srv_action, s.firewall_name srv_fw_name";
		query += " from flow f, flow_svc_group fsg, service_group sg, service s, service_group_member sgm ";
		query += " where f.request_id=" + dbS(formId) + " and fsg.flow_id=f.id and sg.id=fsg.group_id and sgm.grp_id=sg.id and s.id=sgm.svc_id "; 
		query += " union ";
		query += " select sg.id gsrv_id, sg.req_id gsrv_req_id, sg.name gsrv_name, sg.action gsrv_action, sg.opt_parameter gsrv_opt, sg.firewall_name gsrv_fw_name, '' srv_name, '' srv_proto, '' srv_port, '' srv_action, '' srv_fw_name ";
		query += " from flow f, flow_svc_group fsg, service_group sg ";
		query += " where f.request_id=" + dbS(formId) + " and fsg.flow_id=f.id and sg.id=fsg.group_id and ( sg.action='delete' or sg.action='rename' or sg.action='modify') ";
		Umap mtn		= new Umap().setName(UtilApp.HD_MTN);
		Umap rdd		= new Umap();
		int count 		= 0;		
		String gp		= "g-";
		String ifx		= "-gsrv-";
		
		for (LeafBean leaf : execute(query, attrs).getBasket())
		{
			BoxBean o 		= leaf.toBox();
			String gsrv_id 	= o.get("gsrv_id").toSeed().getSeed();
			String grp_idx	= "";
			if (rdd.containsKey(gsrv_id))
			{
				/** existent object group **/
				grp_idx = rdd.get(gsrv_id);
			}else{
				/** new object group **/
				count ++;
				grp_idx 		= count + "";
				String fw_name 	= o.get("gsrv_fw_name").toSeed().getSeed();
				String name 	= o.get("gsrv_name").toSeed().getSeed();		
				rdd.push(gsrv_id, count + "");
				mtn.push(gp + grp_idx + ifx + "output", fw_name);
				mtn.push(gp + grp_idx + ifx + "name", name);
				String action = o.get("gsrv_action").toSeed().getSeed();
				mtn.push(gp + grp_idx + ifx + "action", action);
				if (UtilStr.isX(action, "rename"))
				{
					String rename = o.get("gsrv_opt").toSeed().getSeed();
					mtn.push(gp + grp_idx + ifx + "new-name", rename);
					mtn.push(gp + grp_idx + ifx + "new-name-fw", fw_name.replaceAll(name.replaceAll(" ", "_"), rename.replaceAll(" ", "_")));
				}				
			}
			
			/** add object group member **/
			String action = o.get("gsrv_action").toSeed().getSeed();			
			if (UtilStr.isX(action, "create") | UtilStr.isX(action, "update"))
			{
				String members 	= "";
				String grp		= "srvs";
				Umap member 	= new Umap().setKVP(KVP.KVP3);			
				members 		= mtn.get(gp + grp_idx + ifx + grp);
				member
				.push("uc", o.get("srv_action").toSeed().getSeed())
				.push("output", o.get("srv_fw_name").toSeed().getSeed())					
				.push("srv-name", o.get("srv_name").toSeed().getSeed())
				.push("srv-proto", o.get("srv_proto").toSeed().getSeed())
				.push("srv-port", o.get("srv_port").toSeed().getSeed());
			
				mtn.push(gp + grp_idx + ifx + grp, UtilStr.isEmpty(members) ? member.parameterize() : members + UtilApp.CGS_SEP + member.parameterize());
			}
		}
		return mtn.push("g-nb-group", count + "");
	}
	
	public void updateMtnFormInDatabase(String formId, Map<String, Umap> collections)
	{
		updateAdminFormInDatabase(formId, collections, false);
		updateMtnFormInDatabase(formId, collections.get("mtns"));
	}
	
	private void updateMtnFormInDatabase(String formId, Umap mtns)
	{		
		deleteAllFlows(formId);
		if (UtilApp.isMtnGOO(mtns.get("g-type")))
			updateMtnGoo(formId, mtns);
		else if (UtilApp.isMtnGSRV(mtns.get("g-type")))
			updateMtnGsrv(formId, mtns);
	}
	
	protected void updateMtnGoo(String formId, Umap mtns)
	{
		//initAreaIdMap();
		String pfx		= "g";
		String grp		= "goo";
		String flow_q	= " insert into flow(request_id,action,flow_content_description) ";
				flow_q 	+= " values(" + dbS(formId) + "," + dbS("") + "," + dbS("#desc") + ") ";
		String fl_src_q = " insert into flow_src_group(flow_id,group_id,action) values(#flow_id,#group_id,'0') ";
		String obj_gr_q	= "insert into object_group(req_id,name,action,organization,opt_parameter,firewall_name,environment,mdata) "
						+ "values(" + dbS(formId) + ",#name,#action,#org,#opt,#fw_name,#env,#mdata)";
		String 	obj_q	= " insert into object(req_id,type,name,ipaddress,netmask,action,ipaddress_nat,organization,firewall_name,environment,mdata) "	//zone_id
						+ " values(#req_id,#type,#host,#ip,#mask,#action,#nat,#org,#fw_name,#env,#mdata) "; //zone
		String ogm_q	= " insert into object_group_member(grp_id,obj_id) values(#grp_id,#obj_id)";				
		String ogmg_q	= " insert into object_group_member_grp(grp_id,child_grp_id) values(#grp_id,#child_grp_id)";
		
		for (int idx = 1 ; idx <= Util.getInteger(mtns.get(pfx + "-" + "nb-group")) ; idx++)
		{
			String grpfix 	= pfx + "-" + idx + "-" + grp + "-";
			String org		= mtns.get(grpfix + "org");
			String name		= mtns.get(grpfix + "name");
			String action	= mtns.get(grpfix + "action");
			long obj_grp_id		= updateReturnId
			(
				obj_gr_q.replace("#name", dbS(name))
				.replace("#action", dbS(action))
				.replace("#org", dbS(org))
				.replace("#opt", dbS(UtilStr.isX(action, "rename") ? mtns.get(grpfix + "new-name") : ""))
				.replace("#fw_name", dbS(mtns.get(grpfix + "output")))
				.replace("#env", dbS(mtns.get(grpfix + "env")))
				.replace("#mdata", dbS(mtns.get(grpfix + "meta")))
			);
			
			updateReturnId(fl_src_q.replace("#flow_id", updateReturnId(flow_q.replace("#desc", action + " group of objects " + name)) + "").replace("#group_id", obj_grp_id + ""));

			if (UtilStr.isX(action, "create") | UtilStr.isX(action, "update"))
			{
				String[] hosts 		= !UtilStr.isEmpty(mtns.get(grpfix + "hosts")) ? mtns.get(grpfix + "hosts").split(UtilApp.GOO_SEP) : new String[0];
				String[] subnets 	= !UtilStr.isEmpty(mtns.get(grpfix + "subnets")) ? mtns.get(grpfix + "subnets").split(UtilApp.GOO_SEP) : new String[0];
				String[] goos 		= !UtilStr.isEmpty(mtns.get(grpfix + "goos")) ? mtns.get(grpfix + "goos").split(UtilApp.GOO_SEP) : new String[0];				
				
				for (String elem : hosts)
				{
					if (!UtilStr.isEmpty(elem))
					{
						Umap map 	= new Umap().setKVP(KVP.KVP3).mapsterize(elem);
						String in_q = obj_q.replace("#req_id", dbS(formId))
										.replace("#type", dbS("host"))
										.replace("#host", dbS(map.get("host")))
										.replace("#ip", dbS(map.get("ip")))
										.replace(",netmask", "").replace(",#mask", "")	//remove
										.replace("#action", dbS(map.get("uc")))
										.replace("#nat", dbS(map.get("ip-tsl")))									
										.replace("#org", dbS(org))
										.replace("#fw_name", dbS(map.get("output")))
										.replace("#env", dbS(map.get("env")))
										//.replace(",zone_id", "").replace(",#zone", "");	//remove
										.replace("#mdata", dbS(map.get("meta")))
										;
						update(ogm_q.replace("#grp_id", obj_grp_id + "").replace("#obj_id", updateReturnId(in_q) + ""));
					}
				}
				for (String elem : subnets)
				{
					if (!UtilStr.isEmpty(elem))
					{
						Umap map 	= new Umap().setKVP(KVP.KVP3).mapsterize(elem);
						String[] ip_mask = map.get("subnet").split("/");
						String in_q = obj_q.replace("#req_id", dbS(formId))
										.replace("#type", dbS("subnet"))
										.replace("#host", dbS(map.get("subnet")))
										.replace("#ip", dbS(ip_mask[0]))
										.replace("#mask", dbS(ip_mask[1]))
										.replace("#action", dbS(map.get("uc")))
										.replace(",ipaddress_nat", "").replace(",#nat", "")	//remove
										.replace("#org", dbS(org))
										.replace("#fw_name", dbS(map.get("output")))
										.replace(",environment", "").replace(",#env", "") //remove
										//.replace("#zone", dbS(org_area_2_id.get(org + ":::" + map.get("area"))))
										.replace("#mdata", dbS(map.get("meta")))
										;
										
						update(ogm_q.replace("#grp_id", obj_grp_id + "").replace("#obj_id", updateReturnId(in_q) + ""));
					}
				}				
				for (String elem : goos)
				{
					if (!UtilStr.isEmpty(elem))
					{
						Umap map 	= new Umap().setKVP(KVP.KVP3).mapsterize(elem);
						String in_q = obj_gr_q.replace("#req_id", dbS(formId))
										.replace("#name", dbS(map.get("goo")))
										.replace("#action", dbS(map.get("uc")))							
										.replace("#org", dbS(org))
										.replace(",opt_parameter", "").replace(",#opt", "")
										.replace("#fw_name", dbS(map.get("output")))
										.replace("#env", dbS(map.get("env")))
										.replace("#mdata", dbS(map.get("meta")))
										;
						update(ogmg_q.replace("#grp_id", obj_grp_id + "").replace("#child_grp_id", updateReturnId(in_q) + ""));
					}
				}				
			}
		}		
	}
	
	protected void updateMtnGsrv(String formId, Umap mtns)
	{
UtilLog.out("updateMtnGsrv:" + formId);
		String pfx	= "g";
		String grp	= "gsrv";
UtilLog.out("nb-group:" + Util.getInteger(mtns.get(pfx + "-" + "nb-group")));
		
		String flow_q		= " insert into flow(request_id,action,flow_content_description) " 
							+ " values(" + dbS(formId) + "," + dbS("") + "," + dbS("#desc") + ") ";
		String fl_srv_q 	= " insert into flow_svc_group(flow_id,group_id,action) values(#flow_id,#group_id,'0') ";
		String srv_gr_q		= "insert into service_group(req_id,name,action,opt_parameter,firewall_name,mdata) "
							+ "values(" + dbS(formId) + ",#name,#action,#opt,#fw_name,#mdata)";
		String 	srv_q		= " insert into service(req_id,name,protocol,svc_type,port,action,firewall_name,mdata) "
							+ " values(#req_id,#name,#proto,#type,#port,#action,#fw_name,#mdata) ";
		String sgm_q		= " insert into service_group_member(grp_id,svc_id) values(#grp_id,#svc_id)";
		
		for (int idx = 1 ; idx <= Util.getInteger(mtns.get(pfx + "-" + "nb-group")) ; idx++)
		{
			String grpfix 	= pfx + "-" + idx + "-" + grp + "-";
			String name		= mtns.get(grpfix + "name");
			String action	= mtns.get(grpfix + "action");
			
			long svc_grp_id	= updateReturnId
			(
				srv_gr_q.replace("#name", dbS(name))
				.replace("#action", dbS(action))
				.replace("#opt", dbS(UtilStr.isX(action, "rename") ? mtns.get(grpfix + "new-name") : ""))
				.replace("#fw_name", dbS(mtns.get(grpfix + "output")))
				.replace("#mdata", dbS(mtns.get(grpfix + "meta")))
			);
			
			updateReturnId(fl_srv_q.replace("#flow_id", updateReturnId(flow_q.replace("#desc", mtns.get(grpfix + "action")  + " group of services " + name)) + "").replace("#group_id", svc_grp_id + ""));
		
			if (UtilStr.isX(action, "create") | UtilStr.isX(action, "update"))
			{								
				String[] srvs 	= !UtilStr.isEmpty(mtns.get(grpfix + "srvs")) ? mtns.get(grpfix + "srvs").split(UtilApp.CGS_SEP) : new String[0];				
				
				for (String elem : srvs)
				{
					if (!UtilStr.isEmpty(elem))
					{
						Umap map 	= new Umap().setKVP(KVP.KVP3).mapsterize(elem);
						String in_q = srv_q.replace("#req_id", dbS(formId))
										.replace("#name", dbS(map.get("srv-name")))
										.replace("#proto", dbS(map.get("srv-proto")))
										.replace("#type", dbS(map.get("srv-type")))
										.replace("#port", dbS(map.get("srv-port")))
										.replace("#action", dbS(map.get("uc")))
										.replace("#fw_name", dbS(map.get("output")))
										.replace("#mdata", dbS(map.get("meta")))
										;
						update(sgm_q.replace("#grp_id", svc_grp_id + "").replace("#svc_id", updateReturnId(in_q) + ""));
					}
				}				
			}
		}		
	}
	
	public void updateCatalog(String formId)
	{
		Umap ai	= retrieveAdminAndTechInfo(formId);
		
		if (!UtilApp.isMtn(ai.get("fcr-mode")))
			/**@see FcrWebAccess.updateCatalogWithFcrForm() **/
			updateCatalogWithFcrForm(formId, ai);
		else 
			updateCatalogWithMtnForm(formId, ai);
	}
	
	
	public void updateCatalogWithMtnForm(String formId, Umap ai)
	{
		UtilLog.out("updateCatalogWithMtnForm:" + formId);
		
		String ts_name	= ai.get("application-service");
		String ts_id 	= getValue("select id from used_technical_service where name=" + dbS(ts_name) , "id");
		
		if (!UtilStr.isEmpty(ts_id))
		{
			Umap mtns = new Umap().setName(UtilApp.HD_MTN);
			if (UtilApp.isMtnGOO(ai.get("fcr-mode")))			
				updateCatalogWithMtnForm_goo(retrieveMtnGoo(formId).push("ts_id", ts_id).push("ts_name", ts_name));
			else if (UtilApp.isMtnGSRV(ai.get("fcr-mode")))			
				updateCatalogWithMtnForm_gsrv(retrieveMtnGsrv(formId).push("ts_id", ts_id).push("ts_name", ts_name));
		}
	}
	
	private void updateCatalogWithMtnForm_goo(Umap mtns)
	{
		//initAreaIdMap();
		String ts_id 	= mtns.get("ts_id");
		int nb_group 	= Util.getInteger(mtns.get("g-nb-group"));
UtilLog.out("update expected nb group:" + nb_group);
		for (int i=1 ; i<=nb_group ; i++)
		{
			String grp_pre 	= "g-" + i + "-goo-";
			String fw_name 	= mtns.get(grp_pre + "output");			
			
			String action 	= mtns.get(grp_pre + "action");
			
			if (UtilStr.isX(action, "rename"))
			{
				String rename 	= mtns.get(grp_pre + "new-name");
				String re_fw 	= mtns.get(grp_pre + "new-name-fw");
				String query	= " update cat_object_group set name=" + dbS(rename) + ", firewall_name=" + dbS(re_fw);
				query += " where firewall_name=" + dbS(fw_name) + " and technical_service_id=" + dbS(ts_id);
				update(query);
			}
			else if (UtilStr.isX(action, "delete"))
			{
				String cat_og_id_q 	= " select id from cat_object_group ";
				cat_og_id_q 		+= " where firewall_name=" + dbS(fw_name); 
				cat_og_id_q 		+= " and technical_service_id=" + dbS(ts_id);
				String cat_og_id 	= getValue(cat_og_id_q, "id");
				
				if(!UtilStr.isEmpty(cat_og_id))
				{
					/** delete object group only if its member relationship have been deleted **/
					if (update("delete from cat_object_group_member where object_group_id=" + dbS(cat_og_id))>0)
						update("delete from cat_object_group where id=" + dbS(cat_og_id));
				}				
			}
			else if (UtilStr.isX(action, "update") | UtilStr.isX(action, "create"))
			{
				String org 			= mtns.get(grp_pre + "org");
				String cat_og_id 	= "";
				
				/** if object group must be created then create the object group in cat_object_group **/
				if (UtilStr.isX(action, "create"))
				{
					String cat_og_cr_q 	= "insert into cat_object_group(organization,environment,name,technical_service_id,firewall_name,manually_created)";
					cat_og_cr_q			+= "values(#organization,#environment,#name,#technical_service_id,#firewall_name,'1')";
					cat_og_cr_q 		= cat_og_cr_q.replace("#organization",dbS(org))
										.replace("#environment",dbS(mtns.get(grp_pre + "env")))
										.replace("#name",dbS(mtns.get(grp_pre + "name")))
										.replace("#technical_service_id",dbS(ts_id))
										.replace("#firewall_name",dbS(fw_name));
					cat_og_id 			= updateReturnId(cat_og_cr_q) + "";
				}
				else	/** if object group must be updated then retrieve its id in cat_object_group **/
				{
					String cat_og_id_q 	= " select id from cat_object_group ";
					cat_og_id_q 		+= " where firewall_name=" + dbS(fw_name); 
					cat_og_id_q 		+= " and technical_service_id=" + dbS(ts_id);
					cat_og_id 			= getValue(cat_og_id_q, "id");
				}
				
				if (!UtilStr.isEmpty(cat_og_id))
				{
					Umap grp = new Umap().push("cat_og_id", cat_og_id).push("grp_pre", grp_pre).push("grp_type", "object");
					
					if (!UtilStr.isEmpty(mtns.get(grp_pre + "hosts")))
						createOrUpdateGooInCatalog(mtns, grp.clone().push("o_list", mtns.get(grp_pre + "hosts")).push("o_type", "host").push("o_table", "cat_host"));
					if (!UtilStr.isEmpty(mtns.get(grp_pre + "subnets")))
						createOrUpdateGooInCatalog(mtns, grp.clone().push("o_list", mtns.get(grp_pre + "subnets")).push("o_type", "subnet").push("o_table", "cat_subnet"));
					if (!UtilStr.isEmpty(mtns.get(grp_pre + "goos")))
						createOrUpdateGooInCatalog(mtns, grp.clone().push("o_list", mtns.get(grp_pre + "goos")).push("o_type", "group").push("o_table", "cat_object_group"));
				}
			}
		}
	}
	
	private void createOrUpdateGooInCatalog(Umap mtns, Umap grp)
	{
		for (String object : grp.get("o_list").split(UtilApp.GOO_SEP))
		{
			UtilLog.out("Handle GOO object:" + object);
			if (!UtilStr.isEmpty(object))
			{
				Umap map 			= new Umap().setKVP(KVP.KVP3).mapsterize(object);
				String o_fw_name 	= map.get("output");	
				String o_action		= map.get("uc");
				String o_id			= getValue("select id from " + grp.get("o_table") + " where firewall_name="+ dbS(o_fw_name) , "id");
				/** check if object has not been already created by fcrweb **/
				if (UtilStr.isX(o_action, "use") | UtilStr.isX(o_action, "create"))
				{	
					if (UtilStr.isEmpty(o_id) & UtilStr.isX(map.get("uc"), "create"))
					{
						String o_q = ""; 
						if (UtilStr.isX(grp.get("o_type"), "host"))
						{
							o_q += "insert into " + grp.get("o_table") + " (organization,environment,name,ip,tsl_ip,firewall_name,manually_created) ";
							o_q += "values(#organization,#environment,#name,#ip,#tsl_ip,#firewall_name,'1')";
							o_q = o_q.replace("#organization",dbS(mtns.get(grp.get("grp_pre") + "org"))).replace("#environment",dbS(map.get("env"))).replace("#name",dbS(map.get("host")))
								.replace("#ip",dbS(map.get("ip"))).replace("#tsl_ip",dbS(map.get("ip-tsl"))).replace("#firewall_name",dbS(o_fw_name));
						}
						else if (UtilStr.isX(grp.get("o_type"), "subnet"))
						{
							String[] ip_mask 	= map.get("subnet").split("/");
							o_q 				= "insert into " + grp.get("o_table") + " (organization,zone_id,ip,mask,firewall_name,manually_created) ";
							o_q 				+= "values(#organization,#zone_id,#ip,#mask,#firewall_name,'1')";
							o_q 				= o_q.replace("#organization",dbS(mtns.get(grp.get("grp_pre") + "org"))).replace("#ip",dbS(ip_mask[0]))
												.replace("#zone_id",dbS(map.get("area-id")))
												.replace("#mask",dbS(ip_mask[1])).replace("#firewall_name",dbS(o_fw_name));
						}
						
						if (!UtilStr.isEmpty(o_q))
							o_id = updateReturnId(o_q) + "";
					}
					if (!UtilStr.isEmpty(o_id))
					{
						update
						(
							"insert into cat_object_group_member(object_group_id,object_id,object_type)" + 
							"values(" + dbS(grp.get("cat_og_id")) + "," + dbS(o_id) + "," + dbS(grp.get("o_type")) + ")"
						);
					}
				}
				else if (!UtilStr.isEmpty(o_id) & UtilStr.isX(o_action, "remove"))						
				{
					update
					(
						" delete from cat_object_group_member " + 
						" where object_group_id=" + dbS(grp.get("cat_og_id")) + 
						" and object_id=" + dbS(o_id) + 
						" and object_type=" + dbS(grp.get("o_type"))
					);
				}
			}
		}
	}
	
	private void updateCatalogWithMtnForm_gsrv(Umap mtns)
	{
UtilLog.out("mtns:" + mtns);
UtilLog.out("update expected nb group:" + Util.getInteger(mtns.get("g-nb-group")));
		for (int i=1 ; i<=Util.getInteger(mtns.get("g-nb-group")) ; i++)
		{
			String grp_pre 	= "g-" + i + "-gsrv-";
			String fw_name 	= mtns.get(grp_pre + "output");				
			String action 	= mtns.get(grp_pre + "action");
			UtilLog.out(fw_name + " - " + action);
			if (UtilStr.isX(action, "rename"))
			{
				String rename 	= mtns.get(grp_pre + "new-name");
				String re_fw 	= mtns.get(grp_pre + "new-name-fw");
				String query	= " update cat_service_group set name=" + dbS(rename) + ", firewall_name=" + dbS(re_fw);
				query += " where firewall_name=" + dbS(fw_name) + " and technical_service_id=" + dbS(mtns.get("ts_id"));
				update(query);
			}
			else if (UtilStr.isX(action, "delete"))
			{
				String cat_sg_id_q 	= " select id from cat_service_group ";
				cat_sg_id_q 		+= " where firewall_name=" + dbS(fw_name); 
				cat_sg_id_q 		+= " and technical_service_id=" + dbS(mtns.get("ts_id"));
				String cat_sg_id 	= getValue(cat_sg_id_q, "id");
				
				if(!UtilStr.isEmpty(cat_sg_id))
				{
					/** delete service group only if its member relationship have been deleted **/
					if (update("delete from cat_service_group_member where service_group_id=" + dbS(cat_sg_id))>0)
						update("delete from cat_service_group where id=" + dbS(cat_sg_id));
				}				
			}
			else if (UtilStr.isX(action, "update") | UtilStr.isX(action, "create"))
			{
				String cat_sg_id 	= "";
				
				/** if service group must be created then create the service group in cat_service_group **/
				if (UtilStr.isX(action, "create"))
				{
					String cat_sg_cr_q 	= "insert into cat_service_group(name,technical_service_id,firewall_name,manually_created)";
					cat_sg_cr_q			+= "values(#name,#technical_service_id,#firewall_name,'1')";
					cat_sg_cr_q 		= cat_sg_cr_q.replace("#name",dbS(mtns.get(grp_pre + "name")))
										.replace("#technical_service_id",dbS(mtns.get("ts_id")))
										.replace("#firewall_name",dbS(fw_name));
					cat_sg_id 			= updateReturnId(cat_sg_cr_q) + "";
				}
				else	/** if service group must be updated then retrieve its id in cat_service_group **/
				{
					String cat_sg_id_q 	= " select id from cat_service_group ";
					cat_sg_id_q 		+= " where firewall_name=" + dbS(fw_name); 
					cat_sg_id_q 		+= " and technical_service_id=" + dbS(mtns.get("ts_id"));
					cat_sg_id 			= getValue(cat_sg_id_q, "id");
				}
				
				if (!UtilStr.isEmpty(cat_sg_id) && !UtilStr.isEmpty(mtns.get(grp_pre + "srvs")))
				{
					createOrUpdateGsrvInCatalog(
					mtns, 
					new Umap()
						.push("cat_sg_id", cat_sg_id)
						.push("grp_type", "service")
						.push("grp_pre", grp_pre)
						.push("s_list", mtns.get(grp_pre + "srvs"))
						.push("s_table", "cat_service")
						.push("s_type", "service")
						.push("ts_id", mtns.get("ts_id"))						
					);
				}
			}
		}
	}
	
	private void createOrUpdateGsrvInCatalog(Umap mtns, Umap grp)
	{
		for (String service : grp.get("s_list").split(UtilApp.CGS_SEP))
		{
UtilLog.out("Handle GSRV service:" + service);
UtilLog.out("grp:" + grp.parameterize());
			if (!UtilStr.isEmpty(service))
			{
				Umap map 			= new Umap().setKVP(KVP.KVP3).mapsterize(service);
				String s_fw_name 	= map.get("output");	
				String s_action		= map.get("uc");
				String s_id			= getValue("select id from " + grp.get("s_table") + " where firewall_name="+ dbS(s_fw_name) , "id");
				/** check if service has not been already created by fcrweb **/
				if (UtilStr.isX(s_action, "use") | UtilStr.isX(s_action, "create"))
				{	
					if (UtilStr.isEmpty(s_id) & UtilStr.isX(map.get("uc"), "create"))
					{
						String s_q = ""; 
						s_q += "insert into " + grp.get("s_table") + " (name,protocol,port,firewall_name,technical_service_id,manually_created) ";
						s_q += "values(#name,#protocol,#port,#firewall_name,#technical_service_id,'1')";
						s_q = s_q.replace("#name",dbS(map.get("srv-name"))).replace("#protocol",dbS(map.get("srv-proto")))
							.replace("#port",dbS(map.get("srv-port"))).replace("#firewall_name",dbS(s_fw_name))
							.replace("#technical_service_id",dbS(grp.get("ts_id")));
						s_id = updateReturnId(s_q) + "";
					}
					if (!UtilStr.isEmpty(s_id))
					{
						update
						(
							"insert into cat_service_group_member(service_group_id,service_id,service_type)" + 
							"values(" + dbS(grp.get("cat_sg_id")) + "," + dbS(s_id) + "," + dbS(grp.get("s_type")) +")"
						);
					}
				}
				else if (!UtilStr.isEmpty(s_id) & UtilStr.isX(s_action, "remove"))						
				{
					update
					(
						" delete from cat_service_group_member " + 
						" where service_group_id=" + dbS(grp.get("cat_sg_id")) + 
						" and service_id=" + dbS(s_id) + 
						" and service_type=" + dbS(grp.get("s_type"))
					);
				}
			}
		}
	}
}
