/**
 * 
 */
package be.celsius.fcrweb.sql;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import be.celsius.fcrweb.application.ApplicationContextLoader;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.datastructure.Umap;

/**
 * @author jhuilian
 *
 */
public class GenericJDBC {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	   
	public GenericJDBC(String dataSourceName)
	{
		this.dataSource = (DriverManagerDataSource) ApplicationContextLoader.getApplicationContext().getBean(dataSourceName);
	    this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	public BasketBean execute(String query, String attributes){return execute(query, attributes, new Umap());}
	
	/**
	 * @param query the query to be executed
	 * @return all results matching the requested query
	 * * properties *
	 * results of form BasketBean>BoxBean>SeedBean
	 * BasketBean.size = # rows
	 * BoxBean.size = # selected columns
	 * SeedBean.id = column name
	 */
	public BasketBean execute(String query, String attributes, Map renaming)
	{
		UtilLog.dbo("query:" + query);
		//UtilLog.dbo("attributes:" + attributes);
		BasketBean results = new BasketBean();
		try{
			List<BoxBean> rows = jdbcTemplateObject.query(query, new GenericMapper(attributes, renaming));
			for (BoxBean row : rows)
				results.add(row);
		}catch (Exception e) {UtilLog.printException(e);}
		UtilLog.dbo("results:" + results.toJson().toString());
		return results;
	}
	
	public String getValue(String query, String attr)
	{
		try
		{
			UtilLog.dbo("query:" + query);
			return execute(query, attr).getBasket().get(0).toBox().get(attr).toSeed().getSeed();
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
			return "";
		}
	 }
	
	public long updateReturnId(String query)
	{return updateReturnId(query, null);}
	
	/**
	 * @param optionalIdAttr the id attribute name (iff null then id is used instead) 
	 */
	public long updateReturnId(final String query, final String optionalIdAttr)
	{		
UtilLog.dbo("updateReturnId" + query);
		try{
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplateObject.update(
		    new PreparedStatementCreator() {
		        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		            PreparedStatement ps =
		                connection.prepareStatement(query, new String[] {UtilStr.isEmpty(optionalIdAttr)?"id":optionalIdAttr});
		            //ps.setString(1, name);
		            return ps;
		        }
		    },
		    keyHolder);
		    
		return new Long(keyHolder.getKey().longValue());
		} 
		catch (Exception e){UtilLog.printException(e);}
		return -1;
	}
	
	public int update(String query)
	{
if (!UtilStr.hasX(query, "delete from"))UtilLog.dbo("update query:" + query);
		try	
		{ 
			return jdbcTemplateObject.update(query);
		}
		catch (Exception e) {UtilLog.printException(e);}
		return -1;
	}
	
	/**
	 * @deprecated
	 */
	public long updateFile(String formId, String name, long size, String type, String content, InputStream is)
	{
		return saveFile(formId, name, size, "", is);
	}
	
	/**
	 * @deprecated
	 */
	public long saveFile(final String form_id, final String filename, final long filesize, final String filetype, final InputStream fis)
	{		
		try{
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplateObject.update(
		    new PreparedStatementCreator() {
		        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		            PreparedStatement ps = connection.prepareStatement("update request set filename=?, size=?, mediatype=?, flow_documentation_bin=? where id=?");
		            ps.setString(1, filename);
		            ps.setString(2, filesize + "");
		  	      	ps.setString(3, filetype);
		  	      	//ps.setBlob(4, fis, filesize);
		  	      	ps.setBinaryStream(4, fis, filesize);
		  	      	ps.setString(5, form_id);
		            return ps;
		        }
		    },
		    keyHolder);
		    
		return 0;
		//new Long(keyHolder.getKey().longValue());
		} 
		catch (Exception e){UtilLog.printException(e);}
		return -1;
	}
	
	public String get_JDBC_Value(String query, String attr)
	{
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try{
			conn = dataSource.getConnection();
			preparedStatement = conn.prepareStatement(query);
			ResultSet rs = preparedStatement.executeQuery();
			//while (rs.next()){}
			return rs.next() ?rs.getString(attr) : "";

		} catch (SQLException e) {			 
			e.printStackTrace();
 
		} finally { 
			try
			{
				if (preparedStatement != null)
					preparedStatement.close();
				if (conn != null)
					conn.close();
			}catch(Exception e){}
		}
		return "";
	}
	
	public String ns(String table)
	{
		/* no owner in mysql, just ensure user has enough privilege to perform the operation */
		/*  
		if (!UtilStr.isEmpty(table))
			return UtilStr.head(table, Config.ns() + ".");
		*/
		return table;
	}
	
	public String dbS(String s)
	{
		return UtilStr.isEmpty(s)?"''":UtilStr.wrap(UtilStr.html2server(s), "'");
	}
	
	public String dbSN(String s)
	{
		return UtilStr.isEmpty(s)?"null":dbS(s);
	}
	
}