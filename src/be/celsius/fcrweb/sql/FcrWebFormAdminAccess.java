package be.celsius.fcrweb.sql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import be.celsius.util.Util;
import be.celsius.util.UtilApp;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.datastructure.Ulist;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Umap.KVP;

public class FcrWebFormAdminAccess extends FcrWebCatalogAccess
{
	public FcrWebFormAdminAccess()
	{
		super();
	}


	/**
	 * In order to provide the user decision to bonita (submit or drop)
	 * we must retrieve the process instance UUID and the current task UUID
	 * the process instance UUID is stored in request table while task UUID 
	 * is stored in request_edition_task table.
	 */
	public Umap getEditionTaskNameAndProcess(String form_id, String user, String user_ldap_group)
	{
		Umap result = new Umap().push("code", "-1") ;
		try
		{
			String 	query = " select r.processInstanceId process, ret.task task "; 
					query += " from request r, request_edition_task ret ";
					query += " where r.id='" + form_id + "' "; 
					query += " and ret.req_id=r.id ";
					/*
					 * may be ignored
					 * since only req and fwe can edit form and if req = fwe
					 * then fwe edition step is ignored. 
					 */
					//query += " and ret.actor='" + user_ldap_group + "' "; 
					query += " and ret.editor='" + user + "' ";
UtilLog.out("retrieve task query:" + query);
			BasketBean task = execute(query, "process,task");
UtilLog.out("task:" + task.toJson());			
			if (task.getBasket().size()<=0)
				result.put("code", UtilApp.SC_MISS_VAL + "") ;
			else
			{
				BoxBean row = task.getBasket().get(0).toBox();
				if (UtilStr.isEmpty(row.get("process").toSeed().getSeed()) | 
					UtilStr.isEmpty(row.get("task").toSeed().getSeed()))
					result.put("code", UtilApp.SC_KO + "") ;
				else
					result.push("code", UtilApp.SC_OK + "")
						.push("process", row.get("process").toSeed().getSeed())
						.push("task", row.get("task").toSeed().getSeed());
			}				
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
			result.put("code", "-9") ;
		}
		return result;
	}	
	
	public Umap getRequestEditionTaskRefs(String formId)
	{
		Umap ret = new Umap().setName(UtilApp.HD_RET);
		String query = " select r.processInstanceId process, ret.task task, ret.actor, ret.editor "; 
		query += " from request r, request_edition_task ret ";
		query += " where r.id='" + formId + "' "; 
		query += " and ret.req_id=r.id ";
		BasketBean rows = execute(query, "process,task,actor,editor");
		
		if (rows.getBasket().size()<1)
			ret.push("statusCode", UtilApp.SC_FAULT + "").push("statusDescription", "Cannot load form: Missing Bonita reference.");
		else if (rows.getBasket().size()>1)
			ret.push("statusCode", UtilApp.SC_FAULT + "").push("statusDescription", "Cannot load form: Too many Bonita references.");
		else
		{
			/** @see bonita fcr validation process tasks name (identifier)**/
			BoxBean bonita_ref 	= rows.getBasket().get(0).toBox();
			ret.put("task", bonita_ref.get("task").toSeed().getSeed());
			ret.put("actor", bonita_ref.get("actor").toSeed().getSeed());
			ret.put("editor", bonita_ref.get("editor").toSeed().getSeed());			
			ret.push("statusCode", UtilApp.SC_OK + "").push("statusDescription", "OK");
		}		
		return ret;
	}
	
	public Umap getFlowDocumentation(String formId)
	{
		Umap doc = new Umap().setName("flow-documentation").push("filename", "").push("content", "").push("content-type", "");
		BasketBean docs = execute("select filename,size,mediatype from request where id=" + dbS(formId), "filename,size,mediatype");
		if (docs.getBasket().size()>0)
		{
			BoxBean document 	= docs.getBasket().get(0).toBox();		
			doc.put("name", document.get("filename").toSeed().getSeed());
			doc.put("size", document.get("size").toSeed().getSeed());
			doc.put("content-type", document.get("mediatype").toSeed().getSeed());
			doc.put("file", "OK");
		}
		else
			doc.put("file", "KO");
		return doc;
	}
	
	public int duplicateFlowDocumentation(String form_id, String dup_form_id)
	{
		String query = " update request copy, request orig ";
				query += " set copy.filename=orig.filename, copy.size=orig.size, copy.mediatype=orig.mediatype, copy.flow_documentation=orig.flow_documentation ";
				query += " where orig.id=" + dbS(dup_form_id) + " and copy.id=" + dbS(form_id); 
		return update(query);
	}
	
	public Umap retrieveAdminAndTechInfo(String formId) {
		Umap ai = UtilApp.getAdminInfoList();
		try {
			String attrs 		= "requestor_uid,validator_uid,scope,reason,type,type_description,form_mode,dup_form_id,maintain_ids,status,filename,on_behalf_of,form_sel";
			BasketBean rows 	= execute("select " + attrs + " from request where id='" + formId + "'", attrs);
			BoxBean aiValues 	= rows.getBasket().get(0).toBox();
			ai.put("requestor", aiValues.get("requestor_uid").toSeed().getSeed());
			ai.put("on-behalf-of", aiValues.get("on_behalf_of").toSeed().getSeed());
			ai.put("application-service", aiValues.get("scope").toSeed().getSeed());
			ai.put("detailed-reason-request", aiValues.get("reason").toSeed().getSeed());
			ai.put("request-context", aiValues.get("type").toSeed().getSeed());
			ai.put("context-detail", aiValues.get("type_description").toSeed().getSeed());
			ai.put("fcr-mode", aiValues.get("form_mode").toSeed().getSeed());		// new,dup,duplicated,maintain-group-objects,maintain-group-services
			ai.put("fcr-dup-id", aiValues.get("dup_form_id").toSeed().getSeed());	// duplicate form id
			ai.put("fcr-status", aiValues.get("status").toSeed().getSeed());		// Edition,Validation,Closure,...
			ai.put("form-sel", aiValues.get("form_sel").toSeed().getSeed());		// new, simple, advanced, empty if maintain-group
			
			ai.put("maintain-ids", aiValues.get("maintain_ids").toSeed().getSeed());
			
			ai.put("filename", aiValues.get("filename").toSeed().getSeed());		 
			return ai;
		} catch (Exception e) {
			UtilLog.printException(e);
			return UtilApp.getAdminInfoList();
		}
	}
	
	protected Umap retrieveDocInfo(String formId)
	{
		Umap document = UtilApp.getDocInfoList();
		try {
			
			Ulist docInfo 		= UtilApp.getDocInfoList().keyList();
			String attrs 		= Util.list2string(docInfo, ",", "", "", "");
			BasketBean rows 	= execute("select id," + attrs + " from request where id='" + formId + "'", "id," + attrs, UtilApp.genMapper(docInfo));
			BoxBean doc 		= rows.getBasket().get(0).toBox();
			
			for (String docAttr : docInfo)
				document.put(docAttr, doc.get(docAttr).toSeed().getSeed());			
			
			return document;
		} catch (Exception e) {
			UtilLog.printException(e);
			return UtilApp.getDocInfoList();
		}
	}
	
	public void updateAdminFormInDatabase(String formId, Map<String, Umap> collections, boolean isDupForm)
	{
		Umap ai		= collections.get("adminInfo");
		Umap ti		= collections.get("techInfo");
		Umap di		= collections.get("docInfo");
		String query = "update request set ";
		query += "requestor_uid=" + dbS(ai.get("requestor")) + ",";
		query += "on_behalf_of=" + dbS(!isDupForm ? ai.get("on-behalf-of") : "") + ",";
		query += "scope=" + dbS(ai.get("application-service")) + ",";
		query += "reason=" + dbS(ai.get("detailed-reason-request")) + ",";
		query += "type=" + dbS(ai.get("request-context")) + ",";
		query += "type_description=" + dbS(ai.get("context-detail"));
		String fi = di.setKVP(KVP.KVP_DB_UP).parameterize(true);
		query += !UtilStr.isEmpty(fi) ? "," + fi : "";
		query += " where id='" + formId + "'";	
		update(query);
	}
	
	protected void deleteAllFlows(String formId)
	{
		BasketBean flow_ids_list = execute("select id from flow where request_id=" + dbS(formId), "id");
		List<String> flow_ids = new ArrayList<String>();
		for (LeafBean flow : flow_ids_list.getBasket())
			flow_ids.add(flow.toBox().get("id").toSeed().getSeed());			
		String flows = Util.list2string(flow_ids, ",", "", "", "" );
		flows = UtilStr.isEmpty(flows)?"('')":UtilStr.wrap(flows, "(", ")");
		update("delete from flow_src_object where flow_id in " + flows);	
		update("delete from flow_src_group where flow_id in " + flows);		
		update("delete from flow_dst_object where flow_id in " + flows);	
		update("delete from flow_dst_group where flow_id in " + flows);
		
		update("delete from object where req_id=" + dbS(formId));
		update("delete from object_group where req_id=" + dbS(formId));
		// check if no cascade delete then
		// TODO delete object_group_member
		// TODO delete object_group_member_group
		
		update("delete from flow_svc_service where flow_id in " + flows);	
		update("delete from flow_svc_group where flow_id in " + flows);
		
		update("delete from service where req_id=" + dbS(formId));
		update("delete from service_group where req_id=" + dbS(formId));
		// check if no cascade delete then
		// TODO delete service_group_member
		
		update("delete from flow where request_id=" + dbS(formId));
		
		update("delete from profile where req_id=" + dbS(formId));
		
		delete_rule_refs(formId);
	}

	protected int delete_rule_refs(String formId)
	{
		return update("delete from link_rule_fcr where request_id=" + dbS(formId));
	}
	
	/**
	 * function called when fw operation user update rule references
	 */
	public Umap update_rule_refs(String formId, Umap flows)
	{
		try
		{
			delete_rule_refs(formId);
			
			int nb_flow = Util.getInteger(flows.get("f-nb-flow"));

			for (int i=1 ; i<=nb_flow ; i++)
			{
				/** update rule refs**/
				int flow_nb_rule 	= Util.getInteger(flows.get("f-" + i + "-nb-rule"));
				String flow_id 		= flows.get("f-" + i + "-flow-id");
				
				for (int j=1 ; j<=flow_nb_rule ; j++)
				{	
					String query = dbS(formId) + ",";
							query += dbS(flow_id) + "," ;
							query += dbS(flows.get("f-" + i + "-rule-" + j + "-fw")) + "," ;
							query += dbS(flows.get("f-" + i + "-rule-" + j + "-rule-id"));
					query = "insert into link_rule_fcr(request_id,flow_id,firewall_name,firewall_id) values(" + query + ")";				
					update(query);
				}
				
				/** update flow flags **/
				String query = "update flow set mdata=" + dbS(flows.get("f-" + i + "-meta")) + " where id=" + dbS(flow_id) + " and request_id=" + dbS(formId);
				update(query);				
			}
			return new Umap().push("statusCode", UtilApp.SC_OK + "").push("statusDescription", "fcr " + formId + " rule references updated successfully.");
		}
		catch (Exception e){UtilLog.printException(e);}
		
		return new Umap().push("statusCode", UtilApp.SC_FAULT + "").push("statusDescription","Exception occurs while updating fcr " + formId + " rule references.");
	}
	
	/**
	 * retrieve rule references linked to fcr identified by 'fcr_id'
	 */
	public BoxBean getFwRuleIds(String fcr_id)
	{
		BasketBean rows = execute("select id,flow_id,firewall_name fw,firewall_id rule_id from link_rule_fcr where request_id=" + dbS(fcr_id), "id,flow_id,fw,rule_id");
		BoxBean rules 	= new BoxBean();
		for (LeafBean row : rows.getBasket())
		{
			BoxBean rule 	= row.toBox();
			String flow_id 	= rule.get("flow_id").toSeed().getSeed(); 
			if (rules.get(flow_id).isNullSeed())
				rules.put(flow_id, new BasketBean());			
			rules.get(flow_id).toBasket().add(rule);
		}
		return rules;
	}
	
	/**
	 * @see FWRulesAccess.getFwLastRuleId(fw)
	 * @return the last rule id for firewall 'fw'
	 */
	public int getFwLastRuleId(String fw)
	{
		BasketBean rows = execute("select val from configs where param=" + dbS("FW-RULE-ID-" + fw), "val");
		try	{return Util.getInteger(Util.getNumber(rows.getBasket().get(0).toBox().get("val").toSeed().getSeed()));} catch (Exception e){}		
		return UtilApp.SC_FAULT;
	}
	
	/**
	 * update the value of rule id with 'rule_id' of firewall 'fw'
	 */
	public void updateNextFwId(String fw, String rule_id)
	{
		if (update("update configs set val=" + dbS(rule_id) + " where param=" + dbS("FW-RULE-ID-" + fw))<=0)
			update("insert into configs(param, val, description) values(" + dbS("FW-RULE-ID-" + fw) + "," + rule_id + ", " + dbS("Firewall " + fw + " next rule id") + ")");
	}
}
