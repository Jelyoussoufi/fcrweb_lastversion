package be.celsius.fcrweb.sql;

import be.celsius.util.Config;
import be.celsius.util.Util;
import be.celsius.util.UtilApp;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Umap.KVP;

public class FWRulesAccess extends GenericJDBC
{

	public FWRulesAccess()
	{
		super(Config.DB_FWRULES_JNDI);
	}
	
	/**
	 * @see FcrWebAccess.getFwLastRuleId(fw)
	 * @return the last rule id for firewall 'fw'
	 */
	public int getFwLastRuleId(String fw)
	{
		try
		{
			return 	
			Util.getInteger(Util.getNumber(execute
			(
				"select max(ref) next_id from (select * from rule where rb_id in (select id from rulebase where import_id = (select max(import_id) from rulebase))) rule where ref like '" + fw + "%'", 
				"next_id"
			).getBasket().get(0).toBox().get("next_id").toSeed().getSeed()));
		}
		catch (Exception e){UtilLog.printException(e);}
		return UtilApp.SC_FAULT;
	}
	

	public Umap init_fcr_with_rules(String formId, Umap ai , FcrWebCatalogAccess catalog)
	{
		String ts_name 	= ai.get("application-service");
		String rule_ids = ai.get("maintain-ids");
		String query = "";
		query += " select r.id rule_id, r.action rule_action, r.comment rule_desc, r.ref rule_ref, src.group_name user_group, o.id elem_id, o.name name, o.type type, ip_address opt1, netmask opt2, 'src' elem "; 
		query += " from rule r, rule_src src, object o ";
		query += " where src.rule_id=r.id and o.id=src.object_id and r.id in (" + rule_ids + ") ";
		query += " union ";
		query += " select r.id rule_id, r.action rule_action, r.comment rule_desc, r.ref rule_ref, '' user_group, o.id elem_id, o.name name, o.type type, ip_address opt1, netmask opt2, 'dest' elem "; 
		query += " from rule r, rule_dst dst, object o ";
		query += " where dst.rule_id=r.id and o.id=dst.object_id and r.id in (" + rule_ids + ") ";
		query += " union ";
		query += " select r.id rule_id, r.action rule_action, r.comment rule_desc, r.ref rule_ref, '' user_group, s.id elem_id, s.name name, s.type type, '' opt1, '' opt2, 'srv' elem "; 
		query += " from rule r, rule_service srv, service s ";
		query += " where srv.rule_id=r.id and s.id=srv.service_id and r.id in (" + rule_ids + ") ";
		
		Umap aefs				= new Umap().setName(UtilApp.HD_AEF);
		Umap rule_flow_map 		= new Umap();
		int flow_idx 			= 0;
		
		BasketBean rows = execute(query, "rule_id,rule_action,rule_desc,rule_ref,user_group,elem_id,name,type,elem,opt1,opt2");
		UtilLog.out("retrieved aefs from fwrules:" + rows.toJson().toString());
		/** in order to handle several distinct user groups (say n group) for one rule, 
		 * we need to define 'n' flows for that rule. 
		 * in order to achieve that, we create a map rule_flow_map which aims is two folds:
		 * - Firstly, it aims to map a key [rule.id * ':' * group name] or [rule.id] (if group name is empty) to a flow index
		 * e.g [33256:developpers] => 2 means: the flow for which a source object with rule id '33256' and user group 'developpers' has index 2 
		 * 
		 * - Secondly, it aims to define a flow master which will contains the destination object and services
		 * all subsequent flow will refer to its master flow to get destination objects and services values.
		 **/
		try{
		for (LeafBean leaf : rows.getBasket())
		{
			BoxBean box = leaf.toBox();
			String elem = box.get("elem").toSeed().getSeed();
			
			if (UtilStr.isX(elem,"src"))
			{
				String rule_id 	= box.get("rule_id").toSeed().getSeed();
				String group	= box.get("user_group").toSeed().getSeed();
				String map_id 	= UtilStr.isEmpty(group) ? rule_id : rule_id + ":" + group;
				if (!rule_flow_map.containsKey(map_id))
				{
					flow_idx ++;
					rule_flow_map.put(map_id, flow_idx + "");
					
					/** define the master flow for a given rule **/
					if (!rule_flow_map.containsKey("master-" + rule_id))
					{
						/** for each rule, keep a ref to its master flow **/
						rule_flow_map.put("master-" + rule_id, flow_idx + "");					
					}
				}
			}
		}
UtilLog.out("rule_flow_map:" + rule_flow_map.parameterize());
		aefs.push("f-nb-flow", flow_idx + "");
		
		String UC_MISS = "use";
		for (LeafBean leaf : rows.getBasket())
		{
			BoxBean box 		= leaf.toBox();
			String rule_id 		= box.get("rule_id").toSeed().getSeed();
			String rule_action 	= box.get("rule_action").toSeed().getSeed();
			String rule_ref 	= box.get("rule_ref").toSeed().getSeed();
			String group		= box.get("user_group").toSeed().getSeed();			
			String elem 		= box.get("elem").toSeed().getSeed();
			String type 		= box.get("type").toSeed().getSeed();
			String name 		= box.get("name").toSeed().getSeed();	/** assumed to be firewall_name in order to handle org attr **/
			String elem_id		= box.get("elem_id").toSeed().getSeed();
			String opt1	 		= box.get("opt1").toSeed().getSeed();
			String opt2	 		= box.get("opt2").toSeed().getSeed();			
			String flow_id		= UtilStr.isEmpty(group) ? rule_id : rule_id + ":" + group;
			String flow_pre 	= "f-" + rule_flow_map.get(flow_id) + "-";
			if (!aefs.containsKey(flow_pre + "dataflow-type"))
			{	
				/**
				 * flow meta aims to check if data is coming from fwrules,
				 * if so then flow must keep fwrule ref [FW_name rule_id]
				 */
				boolean is_ts_rules_reval = UtilApp.isTSRR(ai.get("fcr-mode"));
				
				Umap flow_meta 	= new Umap().setKVP(KVP.KVP5)
						.push("from-rule", "true")
						.push("rule-id", rule_id)
						.push("rule-action", is_ts_rules_reval ? "delete" : "update")
						.push("tsrr", is_ts_rules_reval + "")
						.push("f-master", rule_flow_map.get("master-" + rule_id));
				
				/** ref.rule_id is not same as rule.id (may need to change nomination to avoid confusion) **/
				/** keep trace of rule base fw, and rule base rule id
				 * fwo may add several rule refs
				 * the base rule ref cannot be changed
				 **/
				flow_meta.push("base-fw", Util.getCharacters(rule_ref));
				flow_meta.push("base-rule-id", Util.getNumber(rule_ref));
				aefs.put(flow_pre + "meta", flow_meta.parameterize());
				aefs.push(flow_pre + "dataflow-type", ruleAction2FlowConnectionType(rule_action));
				aefs.push(flow_pre + "flow-action", ruleAction2FlowAction(rule_action));
				aefs.push(flow_pre + "flow-description", box.get("rule_desc").toSeed().getSeed());				
			}
			
			/** if several flow for a rule then get master flow id **/
			flow_pre 			= UtilStr.isX(elem, "src") ? "f-" + rule_flow_map.get(flow_id) : "f-" + rule_flow_map.get("master-" + rule_id);
			String nb_ctrl_id 	= flow_pre + "-nb-" + elem;
			if (!aefs.containsKey(nb_ctrl_id))
				aefs.put(nb_ctrl_id, "0");				
			int nb_ctrl 	= Util.getInteger(aefs.get(nb_ctrl_id)) + 1;			
			String ctrl_pre = flow_pre + "." + nb_ctrl + "-" + elem + "-";
			
			/** number of (src,dest,srv) control in a flow **/ 
			aefs.put(nb_ctrl_id, nb_ctrl + "");	
			
			/** ctrl meta aims to check if data is coming from fwrules, if data is existing in fcrweb, etc ... **/
			Umap ctrl_meta = new Umap().setKVP(KVP.KVP5).push("from-rule", "true").push("is-nao-rule", "false");
			
			if (UtilStr.isX(elem, "src") | UtilStr.isX(elem, "dest"))
			{				
				Umap prof_meta = new Umap().setKVP(KVP.KVP5).push("from-rule", "true");
				
				if (UtilStr.isX(elem, "src") & !UtilStr.isEmpty(group))
				{
					BasketBean profs = catalog.getProfiles(group, ts_name);
					//id,name,owner,description
					if (profs.getBasket().size()<=0)
					{
						aefs.put(flow_pre + "-profile", group);
						aefs.put(flow_pre + "-profile-owner", "");
						aefs.put(flow_pre + "-profile-description", "");
						aefs.put(flow_pre + "-profile-uc", UC_MISS);
						prof_meta.push("in-fcrweb", "false");
					}
					else
					{//
						BoxBean profile = profs.getBasket().get(0).toBox();
						aefs.put(flow_pre + "-profile", profile.get("name").toSeed().getSeed());
						aefs.put(flow_pre + "-profile-owner", profile.get("owner").toSeed().getSeed());
						aefs.put(flow_pre + "-profile-description", profile.get("description").toSeed().getSeed());
						aefs.put(flow_pre + "-profile-uc", "use");
						prof_meta.push("in-fcrweb", "true");
					}
					aefs.put(flow_pre + "-profile-meta", prof_meta.parameterize());
				}
				if (UtilStr.isX(type, "host"))
				{//id,name,ip,tsl_ip,organization,environment,firewall_name
					aefs.put(ctrl_pre + "cat", "Host");
					BasketBean objects = catalog.getHosts("where firewall_name=" + dbS(name));
					if (objects.getBasket().size()<=0)
					{
						aefs.put(ctrl_pre + "output", name);
						aefs.put(ctrl_pre + "host", name);
						aefs.put(ctrl_pre + "uc", UC_MISS);
						ctrl_meta.push("in-fcrweb", "false");
					}
					else
					{
						BoxBean object = objects.getBasket().get(0).toBox();
						aefs.put(ctrl_pre + "output", object.get("firewall_name").toSeed().getSeed());
						aefs.put(ctrl_pre + "host", object.get("name").toSeed().getSeed());
						aefs.put(ctrl_pre + "org", object.get("organization").toSeed().getSeed());
						aefs.put(ctrl_pre + "host-env", object.get("environment").toSeed().getSeed());
						aefs.put(ctrl_pre + "ip", object.get("ip").toSeed().getSeed());
						aefs.put(ctrl_pre + "ip-tsl", object.get("tsl_ip").toSeed().getSeed());
						aefs.put(ctrl_pre + "uc", "use");
						ctrl_meta.push("in-fcrweb", "true");
					}						
				}
				else if (UtilStr.isX(type, "network"))
				{//
					aefs.put(ctrl_pre + "cat", "Subnet");
					BasketBean objects = catalog.getSubnets("where firewall_name=" + dbS(name));
					if (objects.getBasket().size()<=0)
					{
						aefs.put(ctrl_pre + "output", name);
						aefs.put(ctrl_pre + "subnet", name);
						aefs.put(ctrl_pre + "uc", UC_MISS);
						ctrl_meta.push("in-fcrweb", "false");
					}
					else
					{//id,area,ip,mask,description,subnet_type,organization,firewall_name
						BoxBean object = objects.getBasket().get(0).toBox();
						aefs.put(ctrl_pre + "output", object.get("firewall_name").toSeed().getSeed());
						aefs.put(ctrl_pre + "org", object.get("organization").toSeed().getSeed());
						aefs.put(ctrl_pre + "subnet", object.get("ip").toSeed().getSeed() + "/" + object.get("mask").toSeed().getSeed());
						aefs.put(ctrl_pre + "subnet-area", object.get("area").toSeed().getSeed());
						aefs.put(ctrl_pre + "uc", "use");
						ctrl_meta.push("in-fcrweb", "true");
					}
				}
				else if (UtilStr.isX(type, "group"))
				{
					aefs.put(ctrl_pre + "cat", "Group of objects");
					BasketBean objects = catalog.getObjectGroups("", ts_name, "", "firewall_name=" + dbS(name));
					if (objects.getBasket().size()<=0)
					{
						aefs.put(ctrl_pre + "output", name);
						aefs.put(ctrl_pre + "goo", name);
						aefs.put(ctrl_pre + "uc", UC_MISS);
						aefs.put(ctrl_pre + "hosts", "");
						aefs.put(ctrl_pre + "subnets", "");
						aefs.put(ctrl_pre + "goos", "");
						ctrl_meta.push("in-fcrweb", "false");
					}
					else
					{//
						BoxBean object = objects.getBasket().get(0).toBox();
						aefs.put(ctrl_pre + "output", object.get("firewall_name").toSeed().getSeed());
						aefs.put(ctrl_pre + "goo", object.get("name").toSeed().getSeed());
						aefs.put(ctrl_pre + "org", object.get("organization").toSeed().getSeed());
						aefs.put(ctrl_pre + "goo-env", object.get("environment").toSeed().getSeed());
						aefs.put(ctrl_pre + "uc", "use");
						
						String grp_id = elem_id;
						
						/** retrieve fwrules object group members **/
						/*
						String grp_q = " select o.name name, o.type type, ip_address ip, netmask mask from object_member grp_mbr, object o ";
						grp_q += " where grp_mbr.object_id=" + dbS(grp_id) + " and o.id=grp_mbr.member_id ";
						*/
						aefs.put(ctrl_pre + "hosts", "");
						aefs.put(ctrl_pre + "subnets", "");
						aefs.put(ctrl_pre + "goos", "");
						ctrl_meta.push("in-fcrweb", "true");
						
						/** check if this ctrl is NAO group of object and vpn_enc=1 or user_auth=1**/
						ctrl_meta.push("is-nao-rule",  (UtilStr.isX(object.get("user").toSeed().getSeed(), "1") |  UtilStr.isX(object.get("vpn").toSeed().getSeed(), "1")) + "");
					}						
				}
				else
				{
UtilLog.out("UNDETERMINE OBJECT:" + type + " - " + elem_id + " - " + name);
					// undefined type object
					aefs.put(ctrl_pre + "output", name);
					aefs.put(ctrl_pre + "cat", type);
					aefs.put(ctrl_pre + "uc", UC_MISS);
					ctrl_meta.push("in-fcrweb", "false").push("oid", elem_id);
				}
			}
			else	/** SERVICES, GROUP SERVICES **/
			{
				if (UtilStr.isX(type, "group"))
				{
					aefs.put(ctrl_pre + "cat", "Group of services");
					BasketBean objects = catalog.getServiceGroups("", ts_name, " firewall_name=" + dbS(name));
					if (objects.getBasket().size()<=0)
					{
						aefs.put(ctrl_pre + "output", name);
						aefs.put(ctrl_pre + "gsrv-name", name);
						aefs.put(ctrl_pre + "uc", UC_MISS);
						ctrl_meta.push("in-fcrweb", "false");
					}
					else
					{//
						BoxBean object = objects.getBasket().get(0).toBox();
						aefs.put(ctrl_pre + "output", object.get("firewall_name").toSeed().getSeed());
						aefs.put(ctrl_pre + "gsrv-name", object.get("name").toSeed().getSeed());
						aefs.put(ctrl_pre + "uc", "use");
						String grp_id = elem_id;
						
						/** retrieve fwrules object group members **/
						/*
						String grp_q = " select o.name name, o.type type, ip_address ip, netmask mask from object_member grp_mbr, object o ";
						grp_q += " where grp_mbr.object_id=" + dbS(grp_id) + " and o.id=grp_mbr.member_id ";
						*/
						aefs.put(ctrl_pre + "gsrv-srvs", "");
						ctrl_meta.push("in-fcrweb", "true");
					}						
				}
				else  // single service but type contains protocol 
				{//id,name,type,protocol,port,firewall_name
					aefs.put(ctrl_pre + "cat", "Single service");
					BasketBean objects = catalog.getServices("",ts_name," firewall_name=" + dbS(name));
					if (objects.getBasket().size()<=0)
					{
						aefs.put(ctrl_pre + "output", name);
						aefs.put(ctrl_pre + "one-name", name);
						aefs.put(ctrl_pre + "uc", UC_MISS);
						ctrl_meta.push("in-fcrweb", "false");
					}
					else
					{
						BoxBean object = objects.getBasket().get(0).toBox();
						aefs.put(ctrl_pre + "output", object.get("firewall_name").toSeed().getSeed());
						aefs.put(ctrl_pre + "one-name", object.get("name").toSeed().getSeed());
						aefs.put(ctrl_pre + "one-proto", object.get("protocol").toSeed().getSeed());
						aefs.put(ctrl_pre + "one-port", object.get("port").toSeed().getSeed());
						aefs.put(ctrl_pre + "uc", "use");
						ctrl_meta.push("in-fcrweb", "true");
					}						
				}
			}
			aefs.put(ctrl_pre + "meta", ctrl_meta.parameterize());
		}
	}catch(Exception e){UtilLog.printException(e);}
UtilLog.out("aefs form rules:" + aefs.parameterize());
		return aefs;
	}
	
	private String ruleAction2FlowConnectionType(String rule_action)
	{
		if (UtilStr.hasX(rule_action, "accept") | UtilStr.hasX(rule_action, "reject") | UtilStr.hasX(rule_action, "deny") | UtilStr.hasX(rule_action, "drop"))
			return "Host 2 Host";
		else if 
		(
			UtilStr.hasX(rule_action, "client") | UtilStr.hasX(rule_action, "user") | 
			UtilStr.hasX(rule_action, "auth") | 
			UtilStr.hasX(rule_action, "encrypt") | UtilStr.hasX(rule_action, "vpn")
		)
			return "User 2 Host";
		else 
			return	"";
	}
	
	private String ruleAction2FlowAction(String rule_action)
	{
		if (UtilStr.hasX(rule_action, "accept"))
			return "Accept";
		else if (UtilStr.hasX(rule_action, "reject") | UtilStr.hasX(rule_action, "deny") | UtilStr.hasX(rule_action, "drop"))
			return "Deny";
		else  if (UtilStr.hasX(rule_action, "auth"))
			return "User authentication";
		else  if (UtilStr.hasX(rule_action, "encrypt") | UtilStr.hasX(rule_action, "vpn"))
			return "Encryption (VPN)";
		else
			return "";
	}
}
