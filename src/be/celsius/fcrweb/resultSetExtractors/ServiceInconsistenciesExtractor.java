package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.ServiceInconsistencies;
import be.celsius.fcrweb.objects.ServiceInconsistency;

public class ServiceInconsistenciesExtractor implements ResultSetExtractor
{
	
	public ServiceInconsistenciesExtractor()
	{
	}
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{		
		ServiceInconsistencies service = new ServiceInconsistencies();
		ServiceInconsistency svc_inc = new ServiceInconsistency();
		
		while(rs.next())
		{
			service.setServiceName(rs.getString("NAME"));
			svc_inc = new ServiceInconsistency();
			svc_inc.setId(rs.getInt("ID"));
			svc_inc.setType(rs.getString("TYPE"));
			svc_inc.setPorts(rs.getString("PORTS"));
			svc_inc.setFirewalls(rs.getString("FW_NAMES"));
			service.getInconsistencies().add(svc_inc);
		}
		return service;
	}
}
