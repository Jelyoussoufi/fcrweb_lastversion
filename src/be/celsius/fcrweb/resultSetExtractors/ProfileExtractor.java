package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.Profile;
import be.celsius.fcrweb.utils.ExtractorUtils;

public class ProfileExtractor implements ResultSetExtractor
{
	
	public ProfileExtractor()
	{		
	}
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{		
		Profile profile = new Profile();
		
		while(rs.next())
		{
			profile = new Profile();
			
			profile.setId(rs.getDouble("ID"));
			profile.setName(rs.getString("NAME"));
			profile.setDescription(rs.getString("DESCRIPTION"));
			profile.setResponsibleUid(rs.getString("RESPONSIBLEUID"));
			String predef = rs.getString("PREDEFINED");
			if (predef.equals("true"))
			{
				profile.setPredefined(true);
			}
		}
		return profile;
	}
}
