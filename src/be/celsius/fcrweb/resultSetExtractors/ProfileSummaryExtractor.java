package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.ProfileSummary;

public class ProfileSummaryExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		ArrayList<ProfileSummary> list = new ArrayList<ProfileSummary>();
		ProfileSummary summary;
		while(rs.next())
		{
			summary = new ProfileSummary();
			summary.setId(rs.getInt("ID"));
			summary.setName(rs.getString("NAME"));
			summary.setDescription(rs.getString("DESCRIPTION"));
			summary.setTechnicalService(rs.getString("TECHNICAL_SERVICE"));
			list.add(summary);
		}
		return list;
	}
}
