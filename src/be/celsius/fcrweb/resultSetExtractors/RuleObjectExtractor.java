package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;


import be.celsius.fcrweb.objects.RuleObject;
import be.celsius.fcrweb.objects.ObjectSummary;
import be.celsius.fcrweb.utils.ExtractorUtils;

public class RuleObjectExtractor implements ResultSetExtractor
{
	private String type;
	public RuleObjectExtractor(String type)
	{
		this.type = type;
	}
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar now = Calendar.getInstance();		
		RuleObject object = new RuleObject();			
		
		while(rs.next())
		{
			object = new RuleObject();
			ArrayList<String[]> otherParams = new ArrayList<String[]>();
			object.setId(rs.getInt("ID"));
			object.setName(rs.getString("NAME"));
			object.setType(rs.getString("TYPE"));
			now = Calendar.getInstance();
			
			object.setUsedInGroups(ExtractorUtils.listRuleObjectToHtml(rs.getString("USED_IN_GROUPS")));
			now = Calendar.getInstance();
			
			object.setUsedInRules(ExtractorUtils.listRuleToHtml(rs.getString("USED_IN_RULES")));
			if (type.equals("group"))
			{
				String[] temp = new String[2];
				temp[0] = "Members";
				now = Calendar.getInstance();
				
				temp[1] = ExtractorUtils.listRuleObjectToHtml(rs.getString("MEMBERS"));
				otherParams.add(temp);
			}
			else if (type.equals("host"))
			{
				String[] ip = new String[2];
				ip[0] = "IP Address";
				ip[1] = rs.getString("IP_ADDRESS");
				otherParams.add(ip);				
			}
			else if (type.equals("network"))
			{
				String[] ip = new String[2];
				ip[0] = "IP Address";
				ip[1] = rs.getString("IP_ADDRESS");
				otherParams.add(ip);
				String[] netmask = new String[2];
				netmask[0] = "Netmask";
				netmask[1] = rs.getString("NETMASK");
				otherParams.add(netmask);	
			}
			else
			{
				String[] ip = new String[2];
				ip[0] = "IP Address";
				ip[1] = rs.getString("IP_ADDRESS");
				otherParams.add(ip);				
			}	
			object.setOtherParameters(otherParams);
		}
		now = Calendar.getInstance();
		
		return object;
	}
}
