package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.Object;
import be.celsius.fcrweb.utils.ExtractorUtils;

public class ObjectExtractor implements ResultSetExtractor
{
	private String type;
	
	public ObjectExtractor(String type)
	{
		this.type = type;
	}
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{		
		Object object = new Object();
		
		while(rs.next())
		{
			object = new Object();
			ArrayList<String[]> otherParams = new ArrayList<String[]>();
			object.setId(rs.getDouble("ID"));
			object.setName(rs.getString("NAME"));
			object.setType(type);			
			object.setUsedInGroups(ExtractorUtils.listObjectToHtml(rs.getString("USED_IN_GROUPS")));
			object.setUsedInRules(ExtractorUtils.listRuleToHtml(rs.getString("USED_IN_RULES")));
			if (type.equals("group"))
			{
				String[] temp = new String[2];
				temp[0] = "Members";
				temp[1] = ExtractorUtils.listObjectToHtml(rs.getString("HOST_MEMBERS") + ";"+rs.getString("SUBNET_MEMBERS") + ";"+rs.getString("ZONE_MEMBERS") + ";"+rs.getString("GROUP_MEMBERS"));
				otherParams.add(temp);
				String predef = rs.getString("PREDEFINED");
				if (predef.equals("true"))
				{
					object.setPredefined(true);
				}
			}
			else if (type.equals("host"))
			{
				String[] ip = new String[2];
				ip[0] = "IP Address";
				ip[1] = rs.getString("IP_ADDRESS");
				otherParams.add(ip);				
			}
			else if (type.equals("subnet"))
			{
				String[] ip = new String[2];
				ip[0] = "IP Address";
				ip[1] = rs.getString("IP_ADDRESS");
				otherParams.add(ip);
				String[] netmask = new String[2];
				netmask[0] = "Netmask";
				netmask[1] = rs.getString("NETMASK");
				otherParams.add(netmask);				
			}
			else
			{
				String[] ip = new String[2];
				ip[0] = "Description";
				ip[1] = rs.getString("DESCRIPTION");
				otherParams.add(ip);
			}	
			object.setOtherParameters(otherParams);
		}
		return object;
	}
}
