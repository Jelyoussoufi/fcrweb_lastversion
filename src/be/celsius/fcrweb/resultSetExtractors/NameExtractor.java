package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.ObjectSummary;

public class NameExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		ArrayList<String> list = new ArrayList<String>();
		String temp = "";
		while(rs.next())
		{
			temp= rs.getString("NAME");
			list.add(temp);
		}
		return list;
	}
}
