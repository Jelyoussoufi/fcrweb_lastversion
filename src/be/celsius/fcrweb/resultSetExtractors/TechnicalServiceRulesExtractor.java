package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.RuleSummary;

public class TechnicalServiceRulesExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		ArrayList<String[]> list = new ArrayList<String[]>();
		
		while(rs.next())
		{
			String[] temp = new String[2];
			String tsName = rs.getString("TSNAME");
			String rulesList = rs.getString("RULESLIST");
			if (rulesList == null)
			{
				rulesList = "";
			}
			else
			{
				rulesList = rulesList.replace("quote", "'");
			}
			
			temp[0] = tsName;
			temp[1] = rulesList;
			list.add(temp);
		}
		return list;
	}
}
