package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.ServiceSummary;

public class ServiceSummaryExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		ArrayList<ServiceSummary> list = new ArrayList<ServiceSummary>();
		ServiceSummary summary;
		while(rs.next())
		{
			summary = new ServiceSummary();
			summary.setId(rs.getInt("ID"));
			summary.setName(rs.getString("NAME"));
			summary.setProtocol(rs.getString("PROTOCOL"));
			summary.setPort(rs.getString("PORT"));
			summary.setTechnicalService(rs.getString("TECHNICAL_SERVICE"));
			list.add(summary);
		}
		return list;
	}
}
