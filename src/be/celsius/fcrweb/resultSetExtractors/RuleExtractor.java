package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.Rule;

public class RuleExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		Rule rule = new Rule();
		while(rs.next())
		{
			rule.setName(rs.getString("NAME"));
			rule.setCreated(rs.getString("CREATION_DATE"));
			rule.setComment(rs.getString("COMMENT"));
			rule.setEndDate(rs.getString("ENDDATE"));
			rule.setLastUsed(rs.getString("LASTUSED"));
		}
		return rule;
	}
}
