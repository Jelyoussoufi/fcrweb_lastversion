package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.RuleProfile;
import be.celsius.fcrweb.utils.ExtractorUtils;

public class RuleProfileExtractor implements ResultSetExtractor
{	
	public RuleProfileExtractor()
	{
	}
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{		
		RuleProfile profile = new RuleProfile();			
		
		while(rs.next())
		{
			profile = new RuleProfile();
			profile.setName(rs.getString("NAME"));
			profile.setUsedInRules(ExtractorUtils.listRuleToHtml(rs.getString("USED_IN_RULES")));
		}
		return profile;
	}
}
