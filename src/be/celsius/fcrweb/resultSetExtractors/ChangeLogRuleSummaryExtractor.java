package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.ChangeLogRuleSummary;
import be.celsius.fcrweb.objects.RuleSummary;
import be.celsius.fcrweb.utils.ExtractorUtils;

public class ChangeLogRuleSummaryExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		ArrayList<ChangeLogRuleSummary> list = new ArrayList<ChangeLogRuleSummary>();
		ChangeLogRuleSummary summary;
		while(rs.next())
		{
			summary = new ChangeLogRuleSummary();
			summary.setId(rs.getInt("ID"));
			summary.setDate(rs.getString("DATE"));
			summary.setChange(rs.getString("CHANGETYPE"));
			summary.setAction(rs.getString("ACTION"));
			summary.setNr(rs.getString("NR"));
			list.add(summary);
		}
		return list;
	}
}
