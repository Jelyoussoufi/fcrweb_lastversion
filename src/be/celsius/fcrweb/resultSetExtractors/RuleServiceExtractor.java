package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.RuleObject;
import be.celsius.fcrweb.objects.ObjectSummary;
import be.celsius.fcrweb.objects.RuleService;
import be.celsius.fcrweb.utils.ExtractorUtils;

public class RuleServiceExtractor implements ResultSetExtractor
{
	private String type;
	
	public RuleServiceExtractor(String type)
	{
		this.type = type;
	}
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{		
		RuleService service = new RuleService();			
		
		while(rs.next())
		{
			service = new RuleService();
			ArrayList<String[]> otherParams = new ArrayList<String[]>();
			service.setId(rs.getInt("ID"));
			service.setName(rs.getString("NAME"));
			service.setType(rs.getString("TYPE"));
			service.setUsedInGroups(ExtractorUtils.listRuleServiceToHtml(rs.getString("USED_IN_GROUPS")));
			service.setUsedInRules(ExtractorUtils.listRuleToHtml(rs.getString("USED_IN_RULES")));
			if (type.equals("group"))
			{
				String[] temp = new String[2];
				temp[0] = "Members";
				temp[1] = ExtractorUtils.listRuleServiceToHtml(rs.getString("MEMBERS"));
				otherParams.add(temp);
			}
			else
			{
				String[] destination = new String[2];
				destination[0] = "Destination Port(s)";
				destination[1] = rs.getString("DESTINATION_PORT");
				otherParams.add(destination);				
			}	
			service.setOtherParameters(otherParams);
		}
		return service;
	}
}
