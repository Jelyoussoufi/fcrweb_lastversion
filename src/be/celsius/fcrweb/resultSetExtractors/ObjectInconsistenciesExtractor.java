package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.ObjectInconsistencies;
import be.celsius.fcrweb.objects.ObjectInconsistency;
import be.celsius.fcrweb.utils.ExtractorUtils;

public class ObjectInconsistenciesExtractor implements ResultSetExtractor
{
	
	public ObjectInconsistenciesExtractor()
	{
	}
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{		
		ObjectInconsistencies object = new ObjectInconsistencies();
		ObjectInconsistency obj_inc = new ObjectInconsistency();
		
		while(rs.next())
		{
			object.setObjectName(rs.getString("NAME"));
			obj_inc = new ObjectInconsistency();
			obj_inc.setId(rs.getInt("ID"));
			obj_inc.setType(rs.getString("TYPE"));
			obj_inc.setIp(rs.getString("IP_ADDRESS"));
			obj_inc.setNetmask(rs.getString("NETMASK"));
			obj_inc.setFirewalls(rs.getString("FW_NAMES"));
			object.getInconsistencies().add(obj_inc);
		}
		return object;
	}
}
