package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.Task;

public class TaskExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		ArrayList<Task> list = new ArrayList<Task>();
		Task task;
		while(rs.next())
		{
			task = new Task();
			task.setTask(rs.getString("TASK"));
			task.setUser(rs.getString("USER"));
			task.setDate(rs.getString("DATE"));
			String comment = rs.getString("COMMENT");
			if (comment == null)
			{
				comment = "";
			}
			else if (comment.equals("null"))
			{
				comment = "";
			}
			task.setComment(comment);
			list.add(task);
		}
		return list;
	}
}
