package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.Service;
import be.celsius.fcrweb.utils.ExtractorUtils;

public class ServiceExtractor implements ResultSetExtractor
{
	private String type;
	
	public ServiceExtractor(String type)
	{
		this.type = type;
	}
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{		
		Service service = new Service();			
		
		while(rs.next())
		{
			service = new Service();
			ArrayList<String[]> otherParams = new ArrayList<String[]>();
			service.setId(rs.getDouble("ID"));
			service.setName(rs.getString("NAME"));
			service.setType(type);			
			service.setUsedInGroups(ExtractorUtils.listServiceToHtml(rs.getString("USED_IN_GROUPS")));
			service.setUsedInRules(ExtractorUtils.listRuleToHtml(rs.getString("USED_IN_RULES")));
			String predef = rs.getString("PREDEFINED");
			if (predef.equals("true"))
			{
				service.setPredefined(true);
			}
			if (type.equals("group"))
			{
				String[] temp = new String[2];
				temp[0] = "Members";
				temp[1] = ExtractorUtils.listServiceToHtml(rs.getString("SERVICE_MEMBERS") + ";"+rs.getString("GROUP_MEMBERS"));
				otherParams.add(temp);
			}
			else
			{
				String[] port = new String[2];
				port[0] = "Port";
				port[1] = rs.getString("PORT");
				otherParams.add(port);
				
				String[] confidentiality = new String[2];
				confidentiality[0] = "Confidentiality";
				confidentiality[1] = rs.getString("CONFIDENTIALITY");
				otherParams.add(confidentiality);	
				
				String[] integrity = new String[2];
				integrity[0] = "Integrity";
				integrity[1] = rs.getString("INTEGRITY");
				otherParams.add(integrity);	
			}	
			service.setOtherParameters(otherParams);
		}
		return service;
	}
}
