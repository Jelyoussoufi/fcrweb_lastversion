package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.RequestTaskSummary;

public class RequestTaskSummaryExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		ArrayList<RequestTaskSummary> list = new ArrayList<RequestTaskSummary>();
		RequestTaskSummary summary;
		while(rs.next())
		{
			summary = new RequestTaskSummary();
			summary.setId(rs.getInt("ID"));
			String detail = rs.getString("DETAIL");			
			if (detail == null)
			{
				summary.setDetail("none");
			}
			else
			{
				if (detail.substring(1, 2).equals("/"))
				{
					detail = detail.substring(2, detail.length());
				}
				summary.setDetail(detail);
			}			
			summary.setRequestor(rs.getString("REQUESTOR_UID"));
			summary.setDate(rs.getString("DATE"));
			summary.setStatus(rs.getString("STATUS"));
			summary.setFormMode(rs.getString("FORM_MODE"));
			summary.setTask(rs.getString("TASK"));
			list.add(summary);
		}
		return list;
	}
}
