package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.ObjectSummary;

public class ObjectSummaryExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		ArrayList<ObjectSummary> list = new ArrayList<ObjectSummary>();
		ObjectSummary summary;
		while(rs.next())
		{
			summary = new ObjectSummary();
			summary.setId(rs.getInt("ID"));
			summary.setName(rs.getString("NAME"));
			summary.setDescription(rs.getString("DESCRIPTION"));
			summary.setTechnicalService(rs.getString("TECHNICAL_SERVICE"));
			list.add(summary);
		}
		return list;
	}
}
