package be.celsius.fcrweb.resultSetExtractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import be.celsius.fcrweb.objects.ObjectSummary;

public class IdAndNameExtractor implements ResultSetExtractor
{

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException 
	{
		ArrayList<String[]> list = new ArrayList<String[]>();
		String[] temp = new String[2];
		while(rs.next())
		{
			temp = new String[2];
			temp[0] = rs.getString("ID");
			temp[1] = rs.getString("NAME");
			list.add(temp);
		}
		return list;
	}
}
