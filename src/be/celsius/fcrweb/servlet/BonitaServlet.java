package be.celsius.fcrweb.servlet;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import be.celsius.fcrweb.beans.FormIdBean;
import be.celsius.fcrweb.beans.FormIdContainerBean;
import be.celsius.fcrweb.sql.FcrWebMtnAccess;
import be.celsius.util.SimpleDomParser;
import be.celsius.util.TransientLogger;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilSoap;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.SeedBean;

/**
 * 
 * @author jhuilian
 *
 * The BonitaLogController handles all notification coming from bonita server
 * When a notification is caught the followings operation are performed:
 * 
 * - update the task status which the bonita process is linked to 
 * - address a notification message to user who started (is responsible) the bonita process
 * - start nested bonita processes if any
 */
public class BonitaServlet extends ServletControl
{
	/**
	 * @param serialVersionUID
	 */
	private static final long serialVersionUID = 8037830258673003719L;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private String VERSION = "1.0";
	private FormIdContainerBean formIdBeans;
	private FcrWebMtnAccess db;
	
	public BonitaServlet()
	{
		super();
		db = new FcrWebMtnAccess();
	}
	
	public void doGet()
	{
		handleRequest();
	}
	public void doPost()
	{	
		handleRequest();
	}
	
	public void handleRequest()
	{	
		WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		formIdBeans =(FormIdContainerBean)springContext.getBean("formIdBeans");
		BoxBean debugLog 	= new BoxBean();
		try
		{
			UtilLog.out("BonitaLogController.handleRequest:");
			
			UtilLog.out(msgRequest); // msgRequest extracted in ServletControl before entering in handleRequest
			
			// notify the bonita server about reception of his request 
			try
			{
				String xml = "";
				xml += "	<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>	";
				xml += "	   <soapenv:Header/>	";
				xml += "	   <soapenv:Body>	";
				xml += "	      <FCRWebResponse>	";
				xml += "	         <Response>	";
				xml += "	            <returnCode>1</returnCode>	";
				xml += "	            <returnFaultMessage>RESULT OK</returnFaultMessage>	";
				xml += "	         </Response>	";
				xml += "	      </FCRWebResponse>	";
				xml += "	   </soapenv:Body>	";
				xml += "	</soapenv:Envelope>	";
				
				UtilSoap.reply(response, xml);
			}
			catch(Exception e)
			{
				
			}
			SimpleDomParser parser = new SimpleDomParser(msgRequest);
			String action = parser.getValue(null, "action");
			if (action.equals("updateCatalog"))
		    {
		        String form_id = parser.getValue(null, "form-id");
		        if (!UtilStr.isEmpty(form_id))
		        {
		            db.updateCatalog(form_id);
		        }
		    }
			//else if (UtilStr.isX(action, "TS_revalidation"))
			//    db.request_TS_revalidation();
			else
			{
				String fid 				= parser.getValue(null, "form-id");
				String requestor 		= parser.getValue(null, "requestor");
				
				formIdBeans.addFid(requestor, fid);
				debugLog.put("bonitaResponse", new SeedBean(msgRequest));
			}
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
			debugLog.put("exception", new SeedBean(UtilLog.getStackTrace(e)));
		}
		debugLog.put("hasLog", new SeedBean(true));
	}

	public void setFormIdBeans(FormIdContainerBean formIdBean) {
		this.formIdBeans = formIdBean;
	}
	

}
