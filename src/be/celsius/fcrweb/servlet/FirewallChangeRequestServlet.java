/**
 * 
 */
package be.celsius.fcrweb.servlet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipFile;

import be.celsius.fcrweb.sql.FcrWebAccess;
import be.celsius.util.BonitaJ;
import be.celsius.util.Config;
import be.celsius.util.JsonArray;
import be.celsius.util.JsonObject;
import be.celsius.util.Ldap;
import be.celsius.util.Util;
import be.celsius.util.UtilApp;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilNet;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.datastructure.Ulist;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Umap.KVP;
import be.celsius.util.datastructure.Uset;

/**
 * @author jhuilian
 * 
 *         This class all Firewall change request (form submitted)
 */
public class FirewallChangeRequestServlet extends ServletControl {

	/**
	 * @param serialVersionUID
	 */
	private static final long serialVersionUID = 6457099488418612665L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * form actions error prefixes
	 */
	
	private BonitaJ bonita;
	private FcrWebAccess store;	
	private Uset users_fwe;
	private Uset users_tso;
	private Uset users_so;
	private Uset users_fwo;
	private Uset users_other;
	
	private Ldap ldap;
	
	private String celsiusUser = UtilApp.default_celsius_user;
	private Umap interaction;
	private boolean s_b_i;	//SessionBeansInitialized;

	public FirewallChangeRequestServlet() {		
		bonita 		= new BonitaJ();
		store 		= new FcrWebAccess();
		ldap		= new Ldap();
		s_b_i 		= false;
	}
	
	private void initializeSessionBeans()
	{
		if (!s_b_i)
		{
			if (getSessionParam("interaction")==null)
			{
				interaction = new Umap();
				setSessionParam("interaction", interaction);		
			}
			s_b_i = true;
		}
	}
	
	protected void doGet()
	{
		UtilLog.out("FirewallChangeRequestServlet:" + uri());
		msgResponse = "not supported.";
		doReply();
	}
	
	protected void doPost()
	{
		UtilLog.out("FirewallChangeRequestServlet:" + uri());
		msgResponse = "not supported.";
		doReply();
	}
	
}
