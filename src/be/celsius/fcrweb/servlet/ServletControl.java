package be.celsius.fcrweb.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.celsius.util.Config;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilSoap;
import be.celsius.util.UtilStr;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Umap.KVP;

/**
 * superclass of singular servlets
 * 
 * @author jhuilian
 *
 * Common behaviors of a type ServletController
 */
public abstract class ServletControl extends HttpServlet
{

	private static final long serialVersionUID = -2196818980645347293L;

	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected Umap params;
	protected String msgRequest;
	protected String msgResponse;
	
	public ServletControl()
	{
		params 	= new Umap();
	}

	public synchronized void doGet(HttpServletRequest req, HttpServletResponse res)
	{
		preprocess(req,res);
		params.setKVP(KVP.KVP_HTTP_HEADER);
		if (UtilStr.hasX(uri(),"?"))
		{
			params.mapsterize(uri().split("?")[1]);
UtilLog.out1(params.toString());
		}
		doGet();
	}
	
	public synchronized void doPost(HttpServletRequest req, HttpServletResponse res)
	{
		preprocess(req,res);
		doPost();
	}
	
	protected String uri(){return request.getRequestURI();}
	private void preprocess(HttpServletRequest req, HttpServletResponse res)
	{
		this.request 		= req;
		this.response 		= res;
		this.msgResponse 	= "";
		params.reset(); 
		doRequest();
		
	}
	
	protected abstract void doGet();
	protected abstract void doPost();
	
	protected void doRequest()
	{
		try
		{
			msgRequest = UtilSoap.extractRequest(request);
UtilLog.out1(uri() + "\n" + msgRequest);
		}
		catch (Exception e)
		{
UtilLog.out1("Error occurs attempt to doRequest");
			msgRequest = "";
		}
	}
	
	protected void doReply()
	{
		try 
		{
UtilLog.out1(msgResponse);
			if (msgResponse.startsWith("<") & msgResponse.endsWith(">"))
				response.setContentType(Config.responseContentType);
			else 
				response.setContentType(Config.TEXT);	

			response.getWriter().print(msgResponse);
			response.getWriter().close();
		}
		catch (Exception e) 
		{
UtilLog.out1("Error occurs while trying to send response");
		}
	}
	
	protected String getParam(String paramName)
	{
		try 
		{
			String param = request.getParameter(paramName).trim();
			param = UtilStr.undoFilter(param);
			UtilLog.out("param " + paramName + " gotted: "+param);
			if(UtilStr.isEmpty(param))param = "";
			return param;
		}
		catch (Exception e)
		{
			UtilLog.out("Error inexistant Request parameter name: " + paramName);
			return "";
		}
	}
	
	protected String getHeader(String header)
	{
		try 
		{
			String param = request.getHeader(header).trim();
			param = UtilStr.undoFilter(param);
			UtilLog.out("header " + header + " gotted: "+param);
			if(UtilStr.isEmpty(param))param = "";
			return param;
		}
		catch (Exception e)
		{
			UtilLog.out("Error inexistant Request header name: " + header);
			return "";
		}
	}
	
	protected Object getSessionParam(String paramName)
	{
		try 
		{
			Object param = request.getSession().getAttribute(paramName);
			if (param==null)
				UtilLog.out("param " + paramName + " gotted from session is null");
			return param;
		}
		catch (Exception e)
		{
			UtilLog.out("Error inexistant Session parameter name: " + paramName);
			return null;
		}
	}
	
	protected void setSessionParam(String paramName, Object param)
	{
		UtilLog.out("set the param: " + paramName + " into the session parameters list.");
		request.getSession().setAttribute(paramName, param);
	}
}
