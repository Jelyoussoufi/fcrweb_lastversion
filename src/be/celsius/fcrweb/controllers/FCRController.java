package be.celsius.fcrweb.controllers;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.objects.FCRPageData;
import be.celsius.fcrweb.objects.RequestSummary;
import be.celsius.fcrweb.utils.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;

public class FCRController implements Controller {

	private MySqlAccessor sqlAccessor;
	private FCRPageData requests;
	private LoginBean loginBean;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String action = request.getParameter("action");
		String uid = loginBean.getLogin(request);
		String user_profiles = loginBean.getProfiles(request, sqlAccessor);
		
		if (action == null)
		{
			requests = sqlAccessor.getRequests(uid);
			String bonita_url = sqlAccessor.getApplicationParameter("bonita_url");
			ModelAndView mav = new ModelAndView("jsp/FCR.jsp");
			request.setAttribute("requests", requests);
			request.setAttribute("uid", uid);
			request.setAttribute("user_profile", user_profiles);
			request.setAttribute("bonita_url", bonita_url);
			request.setAttribute("page", "fcr");
			return mav;
		}
		else if (action.equals("getPreviousRequests"))
		{
			ArrayList<RequestSummary> list = sqlAccessor.getPreviousRequests(uid);
			String json = JSONUtils.requestListToJSON(list);
			response.getWriter().print(json);
		}
		else if (action.equals("getSearchResult"))
		{
			String searchValue = request.getParameter("searchValue");
			ArrayList<RequestSummary> list = sqlAccessor.getSearchResultRequests(searchValue, uid, user_profiles);
			String json = JSONUtils.requestListToJSON(list);
			response.getWriter().print(json);
		}
		return null;
	}

	public void setSqlAccessor(MySqlAccessor sqlAccessor)
	{
		this.sqlAccessor = sqlAccessor;
    }

	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
}
