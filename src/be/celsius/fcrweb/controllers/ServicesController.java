package be.celsius.fcrweb.controllers;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.objects.Service;
import be.celsius.fcrweb.objects.ServiceGroupSummary;
import be.celsius.fcrweb.objects.ServicesPageData;
import be.celsius.fcrweb.objects.ServicesSearchData;
import be.celsius.fcrweb.utils.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;

public class ServicesController implements Controller {
	
	protected final Log logger = LogFactory.getLog(getClass());
	private MySqlAccessor sqlAccessor;
	private LoginBean loginBean;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String action = request.getParameter("action");		
		String uid = loginBean.getLogin(request);
		String user_profiles = loginBean.getProfiles(request, sqlAccessor);
		String ts = loginBean.getTechnicalServices(sqlAccessor);
		
		if (action == null)
		{		
			String id = request.getParameter("id");
			String type = request.getParameter("type");
			if (id == null)
			{
				ModelAndView mav = new ModelAndView("jsp/Services.jsp");
				ServicesPageData services= sqlAccessor.getServices(ts);
				
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("services", services);
				request.setAttribute("page", "services");
				return mav;
			}
			else
			{
				ModelAndView mav = new ModelAndView("jsp/service_view.jsp");
				Service service = sqlAccessor.getService(id, type);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("service", service);
				request.setAttribute("page", "services");
				return mav;
			}
		}
		else if (action.equals("getSearchResult"))
		{			
			String searchValue = request.getParameter("searchValue");
			ServicesSearchData list = sqlAccessor.getSearchResultServices(searchValue, ts, user_profiles);
			String json = JSONUtils.serviceSearchDataToJSON(list);
			response.getWriter().print(json);
		}
		else if (action.equals("setPredefined"))
		{
			String service_id = request.getParameter("id");
			String service_type = request.getParameter("type");
			Boolean ok = sqlAccessor.setServiceAsPredefined(service_id, service_type);
			if (ok)
			{
				response.getWriter().print("ok");
			}
			else
			{
				response.getWriter().print("ko");
			}
		}
		return null;
	}
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor) 
	{
		this.sqlAccessor = sqlAccessor;
    }
	
	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
		
}
