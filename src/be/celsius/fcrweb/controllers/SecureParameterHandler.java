package be.celsius.fcrweb.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import be.celsius.util.Config;
import be.celsius.util.Profiler;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;


/**
 * 
 * @author jhuilian
 *
 * Common behaviors of a controller of type SecureParameterHandler
 */
public abstract class SecureParameterHandler implements Controller
{
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected String action;
	protected static Map<String,Profiler> profilers;
	protected String controllerName;
	
	public String cn()
	{
		return controllerName;
	}
	
	public SecureParameterHandler(String cn)
	{
		controllerName = cn;
		if (profilers==null)
			profilers = new HashMap<String,Profiler>();
		addProfiler(controllerName);
	}
	
	protected void addProfiler(String ctrlId)
	{
		profilers.put(ctrlId, new Profiler(ctrlId));
	}
	
	protected Profiler getProf()
	{
		if (profilers!=null && !profilers.containsKey(getControllerName()))
			addProfiler(getControllerName());
		return profilers.get(getControllerName());
	}
	
	public synchronized ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	{
		this.request 	= request;
		this.response 	= response;
		action 			= getParam("action");
		
		UtilLog.out(controllerName + ".handleRequest:" + action);
		try
		{
			return performRequest(request, response);
		}
		catch (Exception e)
		{
			getProf().reportln(UtilLog.getStackTrace(e));
		}
		return null;
	}
	
	public abstract ModelAndView performRequest(HttpServletRequest request, HttpServletResponse response);
	
	/**
	 * 
	 * @param paramName the name of the parameter
	 * @return the parameter from http request which name is {@param paramName} if the param is found
	 * 			else null
	 */
	protected String getParam(String paramName)
	{
		try 
		{
			String param = request.getParameter(paramName).trim();
			param = UtilStr.undoFilter(param);
			UtilLog.out("param " + paramName + " gotted: "+param);
			if(UtilStr.isEmpty(param))param = "";
			return param;
		}
		catch (Exception e)
		{
			UtilLog.out("Error inexistant Request parameter name: " + paramName);
			return "";
		}
	}
		
	/**
	 * 
	 * @param paramName the name of the parameter
	 * @return the parameter from session which name is {@param paramName} if the param is found
	 * 			else null
	 */
	protected Object getSessionParam(String paramName)
	{
		try 
		{
			Object param = request.getSession().getAttribute(paramName);
			if (param==null)
				UtilLog.out("param " + paramName + " gotted from session is null");
			isNullParam("session", param, paramName);
			return param;
		}
		catch (Exception e)
		{
			UtilLog.out("Error inexistant Session parameter name: " + paramName);
			return null;
		}
	}
	
	protected void setSessionParam(String paramName, Object param)
	{
		UtilLog.out("set the param: " + paramName + " into the session parameters list.");
		request.getSession().setAttribute(paramName, param);
	}
	
	protected Object getContextParam(String paramName)
	{
		try 
		{
			Object param = request.getSession().getServletContext().getAttribute(paramName);
			isNullParam("session context", param, paramName);
			return param;
		}
		catch (Exception e) 
		{
			UtilLog.out("Error inexistant Context parameter name: " + paramName);
			return null;
		}
	}
	
	protected String getHeader(String header)
	{
		try 
		{
			String param = request.getHeader(header).trim();
			param = UtilStr.undoFilter(param);
			UtilLog.out("header " + header + " gotted: "+param);
			if(UtilStr.isEmpty(param))param = "";
			return param;
		}
		catch (Exception e)
		{
			UtilLog.out("Error inexistant Request header name: " + header);
			return "";
		}
	}
	
	/**
	 * @category logging
	 * @param context the context where the param is searched
	 * @param param the parameter to test 
	 * @param paramName the name of the parameter
	 * @post test whether the parameter @{param param} is null,
	 * if true then a message is printed to standart output
	 * to notify that param is null.
	 */
	private void isNullParam(String context, Object param, String paramName)
	{
		if (param==null)
			UtilLog.out("param " + paramName + " gotted from " + context + " is null");
	}
	
	protected void write(String text)
	{
		try 
		{
			if (text.startsWith("<html") && text.endsWith("</html>"))
				response.setContentType(Config.HTML);
			else if (text.startsWith("<") && text.endsWith(">"))
				response.setContentType(Config.responseContentType);
			else
				response.setContentType(Config.TEXT);
			UtilLog.out1(text);			
			response.getWriter().print(text);
			response.getWriter().close();
		} 
		catch (Exception e) 
		{
			UtilLog.out1("Error occurs while trying to send response: " + text);
		}
	}
	
	protected String msgResponse = "";
	protected void doReply(){doReply(true);}
	protected void doReply(boolean showResponse)
	{
		try 
		{
if (showResponse)
	UtilLog.out1(msgResponse);
			if (msgResponse.startsWith("<") & msgResponse.endsWith(">"))
				response.setContentType(Config.responseContentType);
			else 
				response.setContentType(Config.TEXT);	
			
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().print(msgResponse);
			response.getWriter().close();
		} 
		catch (Exception e) 
		{
UtilLog.out1("Error occurs while trying to send response");
		}
	}

	public String getControllerName() {
		return controllerName;
	}

	public void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}

	public static Map<String, Profiler> getProfilers() {
		return profilers;
	}
	
	protected String uri(){return request.getRequestURI();}
}
