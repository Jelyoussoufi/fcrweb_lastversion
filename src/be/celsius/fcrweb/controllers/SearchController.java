package be.celsius.fcrweb.controllers;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.objects.RequestSummary;
import be.celsius.fcrweb.objects.RuleSummary;
import be.celsius.fcrweb.utils.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;

public class SearchController implements Controller {
	
	private MySqlAccessor sqlAccessor;
	private LoginBean loginBean;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String uid = loginBean.getLogin(request);
		String user_profiles = loginBean.getProfiles(request, sqlAccessor);
		String ts = loginBean.getTechnicalServices(sqlAccessor);
		
		String action = request.getParameter("action");
		String type = request.getParameter("type");
		if (action == null)
		{
			ModelAndView mav = new ModelAndView("jsp/Search.jsp");
			request.setAttribute("user_profile", user_profiles);
			if (type == null) type = "FCR";
			request.setAttribute("type", type);
			request.setAttribute("page", "search");
			return mav;
		}
		else if (action.equals("getFCRResult"))
		{
			String query = "";
			String and = "";
			if (request.getParameter("fcrnumber") != null)
			{
				query += and +" request.id=" + request.getParameter("fcrnumber");
				and = " and ";
			}
			if (request.getParameter("requestor") != null)
			{
				query += and + " request.requestor_uid='" + request.getParameter("requestor") + "'";
				and = " and ";
			}
			/*if (request.getParameter("technical") != null)
			{
				query += and + " " + request.getParameter("technical");
				and = " and ";
			}*/
			if (request.getParameter("context") != null)
			{
				query += and + " request.type='" + request.getParameter("context")+"'";
				and = " and ";
			}
			if (request.getParameter("from") != null)
			{
				query += and + " STR_TO_DATE('"+ request.getParameter("from") +"', '%d-%m-%Y') <= task.ts_assigned" ;
				and = " and ";
			}
			if (request.getParameter("to") != null)
			{
				query += and + " STR_TO_DATE('"+ request.getParameter("to") +"', '%d-%m-%Y') >= task.ts_assigned" ;
				and = " and ";
			}
			if (request.getParameter("task") != null)
			{
				query += and + " request.status='" + request.getParameter("task")+"'";
			}
			if (request.getParameter("source") != null)
			{
				query += and + " f_request_contains_ip_or_name_in_source_objects(request.id, '"+request.getParameter("source")+"') = 1 ";
				and = " and ";
			}
			if (request.getParameter("destination") != null)
			{
				query += and + " f_request_contains_ip_or_name_in_destination_objects(request.id, '"+request.getParameter("destination")+"') = 1 ";
				and = " and ";
			}
			ArrayList<RequestSummary> list = sqlAccessor.getAdvancedSearchResultRequests(query, uid, user_profiles);
			String json = JSONUtils.requestListToJSON(list);
			response.getWriter().print(json);
		}
		else if (action.equals("getRuleResult"))
		{
			String query = "";
			String and = "";
			if (request.getParameter("name") != null)
			{
				query += and +" rule.ref like '%" + request.getParameter("name")+"%'";
				and = " and ";
			}
			/*if (request.getParameter("from") != null)
			{
				query += and + " STR_TO_DATE('"+ request.getParameter("from") +"', '%d-%m-%Y') <= rule." ;
				and = " and ";
			}
			if (request.getParameter("to") != null)
			{
				query += and + " STR_TO_DATE('"+ request.getParameter("to") +"', '%d-%m-%Y') >= task.ts_completed" ;
				and = " and ";
			}*/
			if (request.getParameter("source") != null)
			{
				query += and + " f_rule_contains_ip_or_name_in_source_objects(rule.id, '"+request.getParameter("source")+"') = 1";
				and = " and ";
			}
			if (request.getParameter("destination") != null)
			{
				query += and + " f_rule_contains_ip_or_name_in_destination_objects(rule.id, '"+request.getParameter("destination")+"') = 1";
				and = " and ";
			}
			if (request.getParameter("service") != null)
			{
				query += and + " f_rule_contains_port_or_name_in_services(rule.id, '"+request.getParameter("service")+"') = 1";
				and = " and ";
			}
			
			ArrayList<RuleSummary> list = sqlAccessor.getAdvancedSearchResultRules(query, ts, user_profiles);
			
			String json = JSONUtils.ruleListToJSON(list);
			response.getWriter().print(json);
		}
		return null;
	}
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor)
	{
		this.sqlAccessor = sqlAccessor;
    }
	
	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
		
}
