package be.celsius.fcrweb.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import be.celsius.fcrweb.beans.FormIdContainerBean;
import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.utils.AuthenticationUtils;
import be.celsius.util.BonitaJ;
import be.celsius.util.Config;
import be.celsius.util.TransientLogger;
import be.celsius.util.Util;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilProc;
import be.celsius.util.UtilStr;

/**
 * 
 * @author jhuilian
 *
 *
 * This class calls BonitaRestServer and allow the user 
 * to start a workflow process using REST API.
 * 
 * The following info should be provided:
 * - the bonita server host
 * - the bonita server port
 * - the bonita authentication
 * - the bonitaRestAuth and bonitaRestPwd are supposed to be: 'restuser:restbpm'
 * - the data is gotted from database.appconf and can be managed in admin.jsp page
 * 
 * Most of process requires these commons attributes:
 * - the user id
 * - the user team id
 * which can be found in userInfo bean.
 * These info allows user authentication and check about constraints integrity.
 */
public class BonitaRestController extends SecureParameterHandler {

	private BonitaJ bonita;
	private TransientLogger bonitaLogs;
	private String VERSION = "1.0";
	private MySqlAccessor sqlAccessor;
	private FormIdContainerBean formIdBeans;
	private String workflowType;
	private LoginBean loginBean;
	
	public BonitaRestController()
	{
		super("BonitaRestController");
		bonita 				= new BonitaJ();
		//bonitaLogs			= (TransientLogger)TransientLogger.getLoggerByApplicationContext("bonitaLogs");
	}
	public ModelAndView performRequest(HttpServletRequest request, HttpServletResponse response)
	{
		ModelAndView mav = null;

		String version		= VERSION;
		
		String form_mode 	= getParam("pr_form_mode");
		
		String[] parameter;
		String[] mandatory;
		String[] optional;
		String processName = "";
		if (form_mode.equals("new") || form_mode.equals("dup"))
		{
			processName = "FCR_Web_New_FCR";
			workflowType = "new fcr";
			parameter = new String[2];
			mandatory = new String[1];
			optional = new String[1];
			parameter[0] 	= "pr_form_mode";
			parameter[1] 	= "pr_dup_form_id";
			mandatory[0] 	= "pr_form_mode";
			optional[0] 	= "pr_dup_form_id";
		}
		else if (form_mode.equals("rule"))
		{
			processName = "FCR_Web_New_FCR";
			workflowType = "new fcr";
			parameter = new String[3];
			mandatory = new String[3];
			optional = new String[0];
			parameter[0] 	= "pr_form_mode";
			parameter[1] 	= "pr_maintain_ids";
			parameter[2] 	= "pr_ts_name";
			mandatory[0] 	= "pr_form_mode";		
			mandatory[1] 	= "pr_ts_name";
			mandatory[2] 	= "pr_maintain_ids";
		}
		else
		{
			processName = "FCR_Web_New_FCR";
			workflowType = "maintain fcr";
			parameter = new String[3];
			mandatory = new String[1];
			optional = new String[2];
			parameter[0] 	= "pr_form_mode";
			parameter[1] 	= "pr_maintain_ids";
			parameter[2] 	= "pr_ts_name";
			mandatory[0] 	= "pr_form_mode";		
			optional[0] 	= "pr_ts_name";
			optional[1] 	= "pr_maintain_ids";
		}
		
		mav = startProcess(processName, version, parameter, mandatory, optional, null, null);	

		return mav;
	}	
		
	/**
	 * @param request
	 * @param response
	 * @param action
	 * @param version
	 * @param parameter
	 * @param mandatory
	 * @param optionalParameter
	 * @return a ModelAndView
	 * @see {@code ApplicationTableMonitor.performAutomaticTasks()}
	 * @see {@code BonitaLogController.handleChainActions(SimpleDomParser parser)}
	 * @post start bonita process with parameters
	 * @
	 */
	public ModelAndView startProcess
	(
		String action,
		String version,
		String[] parameter,
		String[] mandatory,
		String[] optionalParameter,
		Map<String,String> mandatoryValues,
		Map<String,String> optionalValues
	)
	{
		try
		{
			String uid = loginBean.getLogin(request);
			formIdBeans.removeFid(uid);
			Map<String,String> params = new HashMap<String,String>();
			fillParameters(parameter, params);
			fillParameters(optionalParameter, params);
			
			String missings = getMissingMandatory(mandatory, params);
			missings += getMissingMandatory(mandatoryValues);
			String statusDescription 	= "";
			String statusCode 			= "";
			String infoError			= "";
			// set connection parameters
			bonita.clearVariables();
			bonita.setBonitaServer(sqlAccessor.getApplicationParameter("bonitaRestServer"));
			bonita.setBonitaPort(Util.getInteger(sqlAccessor.getApplicationParameter("bonitaRestPort")));
			bonita.setBonitaAuthUser(sqlAccessor.getApplicationParameter("bonitaRestUser"));
			bonita.setBonitaAuthPassword(sqlAccessor.getApplicationParameter("bonitaRestPWD"));
			bonita.setBonitaUPB64(sqlAccessor.getApplicationParameter("bonitaRestUPB64"));

			bonita.setLogin(uid);

			bonita.addVariable("a_from_console", "false");
			bonita.addVariable("a_at_celsius", ""+Config.atCelsius);
			bonita.addVariable("pr_requestor", uid);

			if (UtilStr.isEmpty(missings))
			{

				for (String param : mandatory)
				{
					String name = param;
					String ref	= param;
					if (UtilStr.hasX(param, "#"))
					{
						String[] paramRef = param.split("#");
						name 	= paramRef[0];
						ref 	= paramRef[1];
					}
					bonita.addVariable(ref, params.get(name));
				}
				if(optionalParameter!=null)
				{
					for (String param : optionalParameter)
					{
						String name = param;
						String ref	= param;
						if (UtilStr.hasX(param, "#"))
						{
							String[] paramRef = param.split("#");
							name 	= paramRef[0];
							ref 	= paramRef[1];
						}
						bonita.addVariable(ref, params.get(name));
					}
				}

				infoError = bonita.instantiateProcessWithVariables(action,version);

				if (UtilStr.hasX(infoError, "ERROR"))
				{
					statusCode 			= "2";
					statusDescription 	= "process: " + action + " raised an error."; // + statusDescription;
				}
				else if (UtilStr.hasX(infoError, "EXCEPTION"))
				{
					statusCode 			= "2";
					statusDescription 	= "process: " + action + " resulted in an exception.";
				}
				else
				{
					statusCode = "1";
					statusDescription =  "process: " + action +" started";
					// OK
					// let BonitaLogController to notify user about request progression.
				}
			}
			else
			{
				statusCode 			= "2";
				statusDescription 	= "could not start process " + action + " since it's missing the following mandatory parameters:" + missings;
			}
			
			if (statusCode.equals("2"))
			{
				request.setAttribute("error", statusDescription + "<br><br><br>" + infoError);
				ModelAndView mav = new ModelAndView("jsp/Error.jsp");
				return mav;
			}
			//writeJson(new Json().put("statusCode",statusCode).put("statusDescription", statusDescription).toString());
			
			int trial = 100;
			while(formIdBeans.getFid(uid) == null & trial>0)
			{
				UtilProc.sleep(1000);
				trial--;
			}
			String fid = formIdBeans.getFid(uid);
			
			if (formIdBeans.getFid(uid) == null)
			{
				request.setAttribute("error", "No Response received from Bonita fid: "+fid+" uid "+uid);
				ModelAndView mav = new ModelAndView("jsp/Error.jsp");
				return mav;
			}
			else
			{
				if (workflowType.equals("new fcr"))
				{
					response.sendRedirect("/fcrweb/jsp/fcr-form.jsp?fid="+fid);
				}
				else
				{
					response.sendRedirect("/fcrweb/jsp/fcr-maintain-form.jsp?fid="+fid);
				}
			}
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
			request.setAttribute("error", "Exception while Bonita process launch");
			ModelAndView mav = new ModelAndView("jsp/Error.jsp");
			
			return mav;
		}
		return null;
	}
	
	private void fillParameters(String[] parameters, Map<String,String> params)
	{
		if (parameters!=null)
		for (int i=0 ; i<parameters.length ; i++)
		{
			params.put(parameters[i],getParam(parameters[i]));
		}
	}
	
	private String getMissingMandatory(String[] mandatory, Map<String,String> params)
	{
		String missings = "";
		
		for (int i=0 ; i<mandatory.length ; i++)
		{
			String name = mandatory[i];
			String ref 	= mandatory[i];
			
			if (UtilStr.hasX(mandatory[i], "#"))
			{
				String[] paramRef = mandatory[i].split("#");
				name 	= paramRef[0];
				ref		= paramRef[1];
			}
			
			if (!params.containsKey(name) || UtilStr.isEmpty(params.get(name)))
				missings += name + ";";
		}
		
		return missings;				
	}
	
	private String getMissingMandatory(Map<String,String> mandatory)
	{
		String missings = "";
		if (mandatory!=null)
		{
			for (String key : mandatory.keySet())
				if (UtilStr.isEmpty(mandatory.get(key)))
					missings += key + ";";
		}
		return missings;
	}
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor)
	{
		this.sqlAccessor = sqlAccessor;
    }
	
	public void setFormIdBeans(FormIdContainerBean fb)
	{
		this.formIdBeans = fb;
    }
	
	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }

}
