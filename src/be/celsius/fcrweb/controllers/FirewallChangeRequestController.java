package be.celsius.fcrweb.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import be.celsius.fcrweb.sql.FWRulesAccess;
import be.celsius.fcrweb.sql.FcrWebMtnAccess;
import be.celsius.util.Config;
import be.celsius.util.JsonArray;
import be.celsius.util.JsonObject;
import be.celsius.util.Ldap;
import be.celsius.util.Util;
import be.celsius.util.UtilApp;
import be.celsius.util.UtilDT;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilNet;
import be.celsius.util.UtilStr;
import be.celsius.util.bean.BasketBean;
import be.celsius.util.bean.BoxBean;
import be.celsius.util.bean.LeafBean;
import be.celsius.util.datastructure.Ulist;
import be.celsius.util.datastructure.Umap;
import be.celsius.util.datastructure.Umap.KVP;
import be.celsius.util.datastructure.Uset;

public class FirewallChangeRequestController extends SecureParameterHandler 
{
	private FcrWebMtnAccess store;
	private FWRulesAccess fwrules;
	private Fcrweb_Webservices webservices;
	private Ldap ldap;
	
	
	private Umap interaction;
	private boolean s_b_i;	//SessionBeansInitialized;
	
	private String celsiusUser 			= UtilApp.default_celsius_user;
	private String localmemberofgroups 	= "";//starting as simple user maybe tso (cfr fcrweb database)
	private boolean isADM				= false;
	private boolean isADV				= false;
	private boolean isFWE				= false;
	private boolean isSO				= false;
	private boolean isFWO				= false;
										
	public FirewallChangeRequestController()
	{
		super("FirewallChangeRequestController");
		
		store 		= new FcrWebMtnAccess();
		fwrules		= new FWRulesAccess();
		webservices = new Fcrweb_Webservices();
		ldap		= new Ldap();
		s_b_i 		= false;
	}
	
	private void initializeSessionBeans()
	{
		if (!s_b_i)
		{
			if (getSessionParam("interaction")==null)
			{
				interaction = new Umap();
				setSessionParam("interaction", interaction);		
			}
			s_b_i = true;
		}
	}
	
	public ModelAndView performRequest(HttpServletRequest request, HttpServletResponse response)
	{
		//String uri = request.getRequestURI();
		String uri 	= action;
		msgResponse = "";
		response.setStatus(HttpServletResponse.SC_OK);
		
		initializeSessionBeans();
		UtilLog.out("GET:" + uri + " - noCache:" + getParam("noCache"));

		if (!UtilStr.isEmpty(getParam("local")))
			celsiusUser = getParam("local");
		
		if (UtilStr.hasX(uri, "maintain/") | UtilStr.hasX(uri, "action/"))
		{
			JsonObject form = new JsonObject();
			if (UtilStr.hasX(uri, "load-form"))
			{
				if (UtilStr.hasX(uri, "action/"))
					form = load_form();
				else
					form = load_maintain_form();
			}
			else if (UtilStr.hasX(uri, "submit-form") | UtilStr.hasX(uri, "drop-form")| UtilStr.hasX(uri, "save-form"))
				form = submit_drop_save_form();
			msgResponse = form.toString();
			doReply();
		}
		else if (UtilStr.hasX(uri, "fwo/"))
		{
			JsonObject fwo = new JsonObject();
			if (UtilStr.hasX(uri, "update-rule-refs"))
			{
				String form_id 	= getParam("form-id");
				int nb_flow 	= Util.getInteger(getParam("f-nb-flow"));
				if (UtilStr.isEmpty(form_id))
					msgResponse = addFormMetadata(fwo, UtilApp.SC_MISS_VAL, "Error: missing fcr id").toString();
				else if (nb_flow<=0)
					msgResponse = addFormMetadata(fwo, UtilApp.SC_MISS_VAL, "Error: unexpected value for number of flows:" + nb_flow).toString();
				else
				{
					Umap result = store.update_rule_refs(form_id, extractGrpsInfo("f-", "nb-flow", "flows"));
					msgResponse = addFormMetadata(fwo, Util.getInteger(result.get("statusCode")), result.get("statusDescription")).toString();
				}
			}
			else
				msgResponse = addFormMetadata(fwo, UtilApp.SC_NONE, "Unknown action:" + uri).toString();
			doReply();
		}	
		else if (UtilStr.hasX(uri, "access/"))
		{
			JsonObject access_response 	= new JsonObject();
			boolean showResponse 		= true;
			UtilLog.out("START:" + uri);
			if (UtilStr.hasX(uri, "autocomplete"))
				access_response = autocomplete();
			else if (UtilStr.hasX(uri, "list-help-msg"))
				access_response = new JsonObject().put("configs", store.getApplicationHelpMsg());
			else if (UtilStr.hasX(uri, "check-private-range-constraint"))
				access_response = check_private_range_constraint();
			else if (UtilStr.hasX(uri, "list-application-service-elements"))
				access_response = list_application_service_elements();			
			else if (UtilStr.hasX(uri, "list-organization-elements"))
			{
				access_response = list_org_elements();
				showResponse 	= false;
			}
			else if (UtilStr.hasX(uri, "list-members"))
				access_response = list_members();
			else if (UtilStr.hasX(uri, "list-user"))			
				access_response = getLdapUsers(getParam("template")).setName("items").toJson();
			else if (UtilStr.hasX(uri, "list-application-service"))
				access_response = list_application_service();
			else if (UtilStr.hasX(uri, "list-context-detail"))
				access_response = list_context_detail();
			else if (UtilStr.hasX(uri, "fw-next-rule-id"))
			{
				String fw 				= getParam("fw");
				int fw_next_id			= 0;
				int fwrules_next_id 	= fwrules.getFwLastRuleId(fw);
				int fcrweb_next_id		= store.getFwLastRuleId(fw);
				fw_next_id  			= Math.max((fwrules_next_id<=0) ? 0 : fwrules_next_id, (fcrweb_next_id<=0) ? 0 : fcrweb_next_id) + 1;
				store.updateNextFwId(fw,fw_next_id + "");
				access_response = new JsonObject().put("fw", fw).put("next_rule_id", fw_next_id + "");
			}
			else if (UtilStr.hasX(uri, "clearSessionBean"))
			{				
				access_response = new JsonObject().put("access", "clearSessionBean");
				String beanId = getParam("beanId");
				if (UtilStr.isEmpty(beanId))
				{
					Umap bean = (Umap)getSessionParam(getParam("beanId"));
					if (bean!=null)
					{
						bean.clear();
						access_response.put("status", beanId + " cleared.");
					}
					else
						access_response.put("status", beanId + " bean not found.");
				}
				else
					access_response.put("status", "missing beanId.");
			}
			msgResponse = addFormMetadata(access_response).toString();
			doReply(showResponse);
			UtilLog.out("END:" + uri);			
		}
		else if (UtilStr.hasX(action,"rules-revalidation"))
		{
			JsonObject test				= new JsonObject();
			int statusCode				= UtilApp.SC_NONE;
			String statusDescription 	= "";
			String user					= get_SM_USER();
			String user_group			= getLdapGroup(user, false);
			
			if (UtilStr.hasX(action, "check-data"))
			{
				JsonObject rules_rev 	= new JsonObject();
				rules_rev.put("edition", "false");
				rules_rev.put("fwo", "false");
				try
				{
					String ts_id = getParam("ts_id");
					if (!UtilStr.isEmpty(ts_id))
					{
						BoxBean ts = store.getTechnicalServices("where id=" + store.dbS(ts_id)).getBasket().get(0).toBox();
						rules_rev	
						.put("sm_user", user)
						.put("sm_user_group", user_group)
						.put("ts_name", ts.get("name").toSeed().getSeed());
						
						String task = ts.get("reval_task_uid").toSeed().getSeed();
						if (UtilStr.isX(ts.get("owner").toSeed().getSeed(), user) & UtilStr.hasX(task,"checkdecision"))	
							rules_rev.put("edition", "true");
						if (UtilStr.hasX(task,"droprules") & UtilStr.hasX(user_group, "fwo"))
							rules_rev.put("fwo", "true");
						
						statusCode 			= UtilApp.SC_OK;
						statusDescription 	= "";
					}
					else
					{
						statusCode = UtilApp.SC_MISS_VAL;
						statusDescription = "Error, missing parameter technical service id";
					}
				}catch(Exception e)
				{
					statusCode = UtilApp.SC_FAULT;
					statusDescription = "Exception, unexpected error occurs while processing request.";
				}
				msgResponse = addFormMetadata(rules_rev, statusCode, statusDescription).toString();
				doReply();
			}
			else if (UtilStr.hasX(uri, "update-decision"))
			{
				String ts_id = getParam("ts_id");
				
				if (UtilStr.isEmpty(ts_id)){
					statusCode = UtilApp.SC_MISS_VAL;
					statusDescription = "Error, missing technical service id.";
				}else{
					try
					{
						BoxBean ts = store.getTechnicalServices("where id=" + store.dbS(ts_id)).getBasket().get(0).toBox();
						
						if (!UtilStr.isX(ts.get("owner").toSeed().getSeed(), user))
						{
							statusCode 			= UtilApp.SC_NO_RIGHT;
							statusDescription 	= "Error, Unauthorized action: Rules revalidation";
						}
						else if (!UtilStr.isX(ts.get("reval_status").toSeed().getSeed(), "revalidation"))
						{
							statusCode 			= UtilApp.SC_NO_RIGHT;
							statusDescription 	= "Error, Rules revalidation is not planned.";
						}
						else
						{
							Umap decision 	= new Umap().setKVP(KVP.KVP3).mapsterize(getParam("decision"));
							Umap rule_id_map= new Umap().setKVP(KVP.KVP3).mapsterize(getParam("rule_id_map"));
							String ups 		= "";	
							String rule_ids	= "";
							boolean hasDrop = false;
							String proc_uid	= ts.get("reval_proc_uid").toSeed().getSeed();
							String task_uid	= ts.get("reval_task_uid").toSeed().getSeed();
							
							for (String rule : decision.keySet())
							{			
								String link_rule_ts_q 	= " update link_rule_technical_service set "
														+ " decision=" + store.dbS(decision.get(rule)) 
														+ " ,mdata=" + store.dbS(new Umap().setKVP(KVP.KVP3).push("rule-id", rule_id_map.get(rule)).parameterize())
														+ " where rule_name=" + store.dbS(rule);
								
								if (store.update(link_rule_ts_q)<=0)
								{
									ups += UtilStr.isEmpty(ups) ? rule : ", " + rule;									
								}
								
								if (UtilStr.isX(decision.get(rule), "drop"))
								{
									rule_ids += UtilStr.isEmpty(rule_ids) ? rule_id_map.get(rule) : "," + rule_id_map.get(rule);
									hasDrop = true;									
								}
							}
							
							if (!UtilStr.isEmpty(ups)){
								statusCode 			= UtilApp.SC_KO;
								statusDescription 	= "Error, following rule(s) has (have) not been updated correctly:\n" 
													+ ups + "\n";
							}else if (UtilStr.isEmpty(proc_uid) | UtilStr.isEmpty(task_uid)){
								statusCode 			= UtilApp.SC_MISS_VAL;
								statusDescription 	= "Error, missing bonita references.";
							}else{
								Umap arguments 	= new Umap().setKVP(KVP.KVP1)
												.push("d_processUUID", proc_uid)
												.push("d_taskUUID", task_uid)
												.push("pr_requestor", user)
												.push("pr_form_mode", "ts-rules-revalidation")
												.push("pr_maintain_ids", rule_ids)
												.push("pr_ts_id", ts_id)												
												.push("pr_has_drop", hasDrop + "")
												.push("a_at_celsius", Config.atCelsius + "")
												.push("a_from_console", "false");							
								
								String logs 	= store.startBonitaProcess(user, "FCR_Web_Commit_Event" , new Umap().push("pr_arguments", arguments.parameterize()));
								
								if (UtilStr.hasX(logs, "error")){
									statusCode 			= UtilApp.SC_KO;
									statusDescription 	= "Error, occurs while commit decision to Bonita";
								}else{
									store.update("update cat_technical_service set reval_status='revalidated', reval_last_update=" + store.dbS(UtilDT.now()) + " where id=" + store.dbS(ts_id));
									statusCode 			= UtilApp.SC_OK;
									statusDescription 	= "Decision has been submitted.";
								}
							}					
						}
					}catch (Exception e){
	UtilLog.printException(e);
						statusCode 			= UtilApp.SC_FAULT;
						statusDescription 	= "Exception occurs while processing your request";			
					}
				}			
				msgResponse = addFormMetadata(test, statusCode, statusDescription).toString();
				doReply();
			}
			else if (UtilStr.hasX(uri, "update-flags"))
			{
				try
				{
					if (!UtilStr.hasX(user_group, "fwo"))
					{
						statusCode 			= UtilApp.SC_NO_RIGHT;
						statusDescription 	= "Error, Unauthorized action: update flag(s).";
					}else{
						Umap flag = new Umap().setKVP(KVP.KVP3).mapsterize(getParam("flags"));						
						for (String rule : flag.keySet())						
							store.update("update link_rule_technical_service set flag=" + store.dbS(flag.get(rule)) + " where rule_name=" + store.dbS(rule));
						
						statusCode 			= UtilApp.SC_OK;
						statusDescription 	= "Flags have been updated.";
					}
				}catch (Exception e){
UtilLog.printException(e);
					statusCode 			= UtilApp.SC_FAULT;
					statusDescription 	= "Exception occurs while processing your request";			
				}						
				msgResponse = addFormMetadata(test, statusCode, statusDescription).toString();
				doReply();
			}
			else if (UtilStr.hasX(uri, "check-saved-rules-decision-flag"))
			{
				JsonArray rules = new JsonArray();
				try{
				for (LeafBean leaf : store.getLinkRuleTS("where technical_service_id=" + store.dbS(getParam("ts_id"))).getBasket())
				{
					BoxBean lrts = leaf.toBox();
					rules.put
					(
						new Umap().push("rule", lrts.get("rule_name").toSeed().getSeed())
						.push("decision", lrts.get("decision").toSeed().getSeed())
						.push("flag", lrts.get("flag").toSeed().getSeed())
					);			
				}
				msgResponse = addFormMetadata(test.put("rules", rules)).toString();
				doReply();
				}catch(Exception e){UtilLog.printException(e);}
			}
			else if (UtilStr.hasX(uri, "getRevalidationRules"))
			{
				msgResponse = getRulesSample();
				doReply();
			}			
		}
		else if (Config.atCelsius && UtilStr.hasX(uri, "local/"))//security sensitive actions
		{
			JsonObject debug = new JsonObject();
			if (UtilStr.hasX(uri, "setCelsiusUser"))
			{
				celsiusUser = !UtilStr.isEmpty(getParam("celsiusUser")) ? getParam("celsiusUser") : UtilApp.default_celsius_user;				
				debug.put("celsiusUser", celsiusUser);
				debug.put("ldapGroup", getLdapGroup(celsiusUser, false));
				debug.put("isAdmin", isAdmin() + "");
				debug.put("user-type", getUserType());
			}
			else if (UtilStr.hasX(uri, "toggleLdapGroup"))
			{
				String group = getParam("group");
				if (UtilStr.isX(group, "adm"))			isADM 	= !isADM;
				else if (UtilStr.isX(group, "adv")) 	isADV 	= !isADV;
				else if (UtilStr.isX(group, "fwe")) 	isFWE	= !isFWE;
				else if (UtilStr.isX(group, "so")) 		isSO 	= !isSO;
				else if (UtilStr.isX(group, "fwo")) 	isFWO 	= !isFWO;
				
				localmemberofgroups = "";
				if (isADM)
					localmemberofgroups += !UtilStr.isEmpty(localmemberofgroups) ? "^" + UtilApp.ADM : UtilApp.ADM;
				if (isADV)
					localmemberofgroups += !UtilStr.isEmpty(localmemberofgroups) ? "^" + UtilApp.ADV : UtilApp.ADV;
				if (isFWE)
					localmemberofgroups += !UtilStr.isEmpty(localmemberofgroups) ? "^" + UtilApp.FWE : UtilApp.FWE;
				if (isSO)
					localmemberofgroups += !UtilStr.isEmpty(localmemberofgroups) ? "^" + UtilApp.SO : UtilApp.SO;
				if (isFWO)
					localmemberofgroups += !UtilStr.isEmpty(localmemberofgroups) ? "^" + UtilApp.FWO : UtilApp.FWO ;
			}
			else if (UtilStr.hasX(uri, "duplicate-form"))
			{
				String dup_form_id = getParam("dup-form-id");
				String form_id = getParam("form-id");
				if (!UtilStr.isEmpty(dup_form_id) & !UtilStr.isEmpty(dup_form_id))
					debug = duplicate_form(dup_form_id, form_id).put("duplicate-operation", UtilApp.SC_OK + "");
				else
					debug.put("duplicate-operation", UtilApp.SC_OK + "");
			}
			else if (UtilStr.hasX(uri, "init-firewall-name"))
			{
				if (Config.atCelsius && UtilStr.isX(getParam("pwd"), "debug-mode-on-init-firewall-name"))
				{	
					init_firewall_names();					
					debug.put("init-firewall-name", UtilApp.SC_OK + "");
				}
				else
					debug.put("init-firewall-name", UtilApp.SC_KO + "Undefined action.");
			}
			else if (UtilStr.hasX(uri, "update-catalog"))
			{
				store.updateCatalog(getParam("form-id"));
				debug.put("update-catalog", UtilApp.SC_OK + "");
			}		
			else if (UtilStr.hasX(uri, "init-reval-month"))
			{
				store.init_TS_reval_month();
				debug.put("update-catalog", UtilApp.SC_OK + "");
			}
			msgResponse = addFormMetadata(debug).toString();
			doReply();
		}
		else if (UtilStr.hasX(uri, "test/"))
		{
			JsonObject debug = new JsonObject();
			if (UtilStr.hasX(uri, "ldap-action"))
			{
				debug.put("ldap", debug_ldap(getParam("values")));
			}
			else if (UtilStr.hasX(uri, "init-ts-revalidation"))
			{
				store.request_TS_revalidation();
			}
			else if (UtilStr.hasX(uri, "ws-action"))
			{
				Map<String, Object> map;
				String ws = getParam("ws");
				if (UtilStr.isX(ws,"mrt"))
				{
					map = webservices.get_MRT_projects();
					for (String key : map.keySet())
					{
						msgResponse += UtilLog.getLineOf(15, "-") + ">  " + key + "  <"+ UtilLog.getLineOf(15, "-") + "\n";
						if (UtilStr.isX(key, "list"))
							msgResponse += ((Ulist) map.get(key)).toJson().toString();
						else
							msgResponse += map.get(key).toString();
						msgResponse += "\n";
					}
				}
				else if (UtilStr.isX(ws,"raa"))
				{
					map = webservices.get_RAA_Supplier_list();
					for (String key : map.keySet())
					{
						msgResponse += UtilLog.getLineOf(15, "-") + ">  " + key + "  <"+ UtilLog.getLineOf(15, "-") + "\n";
						if (UtilStr.isX(key, "list"))
							msgResponse += ((Ulist) map.get(key)).toJson().toString();
						else
							msgResponse += map.get(key).toString();							
						msgResponse += "\n"; 
					}							
				}
				else
					msgResponse = "Unknwon webservice";
					
				doReply();
				return null;
			}
			else if (UtilStr.hasX(uri, "qoso"))
			{
				String query = getParam("query");
				if (UtilStr.hasX(query, "alter") | 	UtilStr.hasX(query, "drop") | UtilStr.hasX(query, "delete") | UtilStr.hasX(query, "insert") | UtilStr.hasX(query, "create"))
				{
					debug.put("result", "forbidden operation (alter,drop,insert,create,delete) detected.");
					msgResponse = addFormMetadata(debug).toString();
					doReply();
					return null;
				}
					
				if (UtilStr.isX(getParam("db"),"fcrweb"))
					msgResponse = store.execute(query, getParam("attrs")).toJson().toString();
				else
					msgResponse = fwrules.execute(query, getParam("attrs")).toJson().toString();
				doReply();
				return null;
			}
			msgResponse = addFormMetadata(debug).toString();
			doReply();
		}		
		else if (UtilStr.hasX(uri, "/rules.htm"))
		{
			msgResponse = new JsonObject().put("response", "Wrong destination url:" + uri).toString();
			doReply();
		}
		else 	
		{
			msgResponse = new JsonObject().put("response", "Invalid url:" + uri).toString();
			doReply();
		}
		
		return null;
	}
	
	private void init_firewall_names()
	{
		BasketBean hosts = store.getHosts("");
		
		for (LeafBean leaf : hosts.getBasket())
		{
			BoxBean host = leaf.toBox();
			String fw_name = UtilApp.getShortOrg(host.get("organization").toSeed().getSeed());
			fw_name += "-host";
			fw_name += "-" + host.get("name").toSeed().getSeed();
			fw_name += "-" + host.get("environment").toSeed().getSeed();
			fw_name += "-" + host.get("ip").toSeed().getSeed();
			store.update("update cat_host set firewall_name=" + store.dbS(fw_name) + " where id=" + store.dbS(host.get("id").toSeed().getSeed()));
		}
		BasketBean subnets = store.getSubnets("");
		for (LeafBean leaf : subnets.getBasket())
		{
			BoxBean subnet = leaf.toBox();
			String fw_name = UtilApp.getShortOrg(subnet.get("organization").toSeed().getSeed());
			fw_name += "-net";
			fw_name += "-" + subnet.get("ip").toSeed().getSeed();
			fw_name += "-" + subnet.get("mask").toSeed().getSeed();
			store.update("update cat_subnet set firewall_name=" + store.dbS(fw_name) + " where id=" + store.dbS(subnet.get("id").toSeed().getSeed()));
		}
		BasketBean goos = store.getObjectGroups("", "", "", "");
		for (LeafBean leaf : goos.getBasket())
		{
			BoxBean goo = leaf.toBox();
			String fw_name = UtilApp.getShortOrg(goo.get("organization").toSeed().getSeed());
			fw_name += "-" + goo.get("name").toSeed().getSeed();
			fw_name += "-" + goo.get("environment").toSeed().getSeed();
			fw_name += "-grp";
			store.update("update cat_object_group set firewall_name=" + store.dbS(fw_name) + " where id=" + store.dbS(goo.get("id").toSeed().getSeed()));
		}	
		BasketBean srvs = store.getServices("", "", "");
		for (LeafBean leaf : srvs.getBasket())
		{
			BoxBean srv = leaf.toBox();
			String fw_name 	= srv.get("protocol").toSeed().getSeed(); 
			fw_name += "-" + srv.get("name").toSeed().getSeed();
			if (!UtilStr.isX(srv.get("protocol").toSeed().getSeed(), "icmp"))
				fw_name += "-" + srv.get("port").toSeed().getSeed();
			
			
			store.update("update cat_service set firewall_name=" + store.dbS(fw_name) + " where id=" + store.dbS(srv.get("id").toSeed().getSeed()));
		}
		BasketBean gsrvs = store.getServiceGroups("", "", "");
		for (LeafBean leaf : gsrvs.getBasket())
		{
			BoxBean gsrv = leaf.toBox();
			String fw_name = gsrv.get("name").toSeed().getSeed() + "-grp";
			store.update("update cat_service_group set firewall_name=" + store.dbS(fw_name) + " where id=" + store.dbS(gsrv.get("id").toSeed().getSeed()));
		}
	}

	private Uset getLdapUsers(String pattern)
	{
		String dn 		= "ou=staff,o=mobistar.be";
		String attr		= "uid";
		String filter 	= !UtilStr.isEmpty(pattern) ? attr + "=*" + pattern + "*" : attr + "=*";
		Uset users 		= getLdapLogins(dn, filter, attr);
		if (!Config.atCelsius) // add partners
		{
			dn = "ou=Partners,o=mobistar.be";
			users.addSet(getLdapLogins(dn, filter, attr));
		}
		return users;
	}
	
	private JsonObject debug_ldap(String values)
	{
		JsonObject json	= new JsonObject();
		Umap configs 	= new Umap().setKVP(KVP.KVP3).mapsterize(values);
		ldap.resetConnectionConfiguration();			
		ldap.setConnectionConfiguration
		(
			configs.get("h"), 
			Util.getInteger(configs.get("p")), 
			configs.get("u"), 
			configs.get("m")
		);
		String dn 		= configs.get("d");
		String filter 	= configs.get("f");
		String attr 	= configs.get("a");
		String flat		= configs.get("flat");
		Uset users 		= new Uset();
		try
		{
			String[] attrs 			= {attr};			
			BasketBean ldapResults 	= ldap.getLdapData(dn, Ldap.SUB, filter, attrs).toBasket();
json.put("raw", ldapResults.toJson());
			for (LeafBean ldapE : ldapResults.getBasket())
			{
				BoxBean entry = ldapE.toBox();
				if (!entry.get(attr).isNullSeed())
				{
					try {
						for (LeafBean leaf : entry.get(attr).toBasket().getBasket())
						{
							String login = leaf.toSeed().getSeed();
							if (!UtilStr.isEmpty(login))
							users.push(login);
						}
					}catch(Exception e){};
				}
			}
		}
		catch(Exception e){UtilLog.printException(e);users = new Uset();}
json.put("results", users.toJson());

		if (UtilStr.isX(flat,"true"))
		{
			Uset logins = new Uset();
			for	(String member : users)
				logins.push(new Umap().setKVP(KVP.KVP_LDAP).mapsterize(member).get("uid"));
json.put("flatten", logins.toJson());
		}
		return json;
	}
	
	private Uset getLdapLogins(String dn, String filter, String attr)
	{
		Umap configs = store.getApplicationConfigs();
		ldap.resetConnectionConfiguration();			
		ldap.setConnectionConfiguration
		(
			configs.get("users-ldap-host"), 
			Util.getInteger(configs.get("users-ldap-port")), 
			configs.get("users-ldap-user"), 
			configs.get("users-ldap-pwd")
		);
		
		Uset users = new Uset();
		try
		{
			String[] attrs 			= {attr};			
			BasketBean ldapResults 	= ldap.getLdapData(dn, Ldap.SUB, filter, attrs).toBasket();

			for (LeafBean ldapE : ldapResults.getBasket())
			{
				BoxBean entry = ldapE.toBox();
				if (!entry.get(attr).isNullSeed())
				{
					try {
						for (LeafBean leaf : entry.get(attr).toBasket().getBasket())
						{
							String login = leaf.toSeed().getSeed();
							if (!UtilStr.isEmpty(login))
							users.push(login);
						}
					}catch(Exception e){};
				}
			}
		}
		catch(Exception e){UtilLog.printException(e);users = new Uset();}

		return users;
	}
	
	private Uset getLdapGroupUsers(String group_filter)
	{
		Uset members = getLdapLogins
		(
			Config.atCelsius ? "ou=groups,ou=staff,o=mobistar.be" : "ou=Applications Groups,ou=groups,ou=staff,o=mobistar.be", 
			"cn=SecTools - FCR Web - " + group_filter,
			"uniqueMember"
		);
		
		Uset logins = new Uset();
		
		for 
		(
			String member :	members
		)
		{
			logins.push(new Umap().setKVP(KVP.KVP_LDAP).mapsterize(member).get("uid"));
		}
		UtilLog.out("flatten members:" + logins.toJson().toString());
		return logins;
	}
	
	
	private JsonObject list_application_service()
	{
		Ulist apps_srvs = new Ulist().setName("items");
		String attr 	= "name";
		String filter 	= UtilStr.isX(getParam("owner"), "true") ? "where owner in" + Util.array2string(Util.wrapAll(memberofgroups().split("\\^"), "'"), ",", "", "(", ")") : "";		
		for (LeafBean row : store.execute("select " + attr + " from cat_technical_service " + filter + " order by " + attr + " asc", attr).getBasket())
			apps_srvs.add(row.toBox().get(attr).toSeed().getSeed());
		return apps_srvs.toJson();
	}
	
	private JsonObject list_context_detail()
	{
		Ulist list_context_detail = new Ulist().setName("items");
		String rc = getParam("request-context"); 
		if (UtilStr.isX(rc, "Project"))
		{
			//list_context_detail.push("Firefox").push("Internet Explorer").push("Chrome").push("Safari").push("Opera");
			list_context_detail = (Ulist) webservices.get_MRT_projects().get("list");
		} else if (UtilStr.isX(rc, "Remote Access")) {
			//list_context_detail.push("Facebook").push("Twitter").push("Instagram").push("Skype").push("MSN").push("LinkedIn").push("debug-mode-on");
			list_context_detail = (Ulist) webservices.get_RAA_Supplier_list().get("list");
		}
		return list_context_detail.setName("items").toJson();
	}
	
	private JsonObject list_application_service_elements()
	{		
		JsonObject list_application_service_elements 	= new JsonObject();		
		JsonArray profile 								= new JsonArray();
		JsonArray service								= new JsonArray();
		JsonArray service_port							= new JsonArray();
		JsonArray gsrv	 								= new JsonArray();
		JsonArray global_gsrv							= new JsonArray();	//predefined==1		
		JsonArray goo_encryption						= new JsonArray();
		JsonArray goo_authentication					= new JsonArray();
		JsonObject goo_encryption_map					= new JsonObject();
		JsonObject goo_authentication_map				= new JsonObject();
		String ts_name = getParam("application-service");
		for (LeafBean row : store.getProfiles("", ts_name).getBasket())
			profile.put(row.toBox().get("name").toSeed().getSeed());
		for (LeafBean row : store.getServices("", ts_name, "").getBasket())
		{
			service.put(row.toBox().get("name").toSeed().getSeed());
			service_port.put(row.toBox().get("port").toSeed().getSeed());
		}
		for (LeafBean row : store.getServiceGroups("", ts_name, "").getBasket())
		{
			String gsrv_name = row.toBox().get("name").toSeed().getSeed();
			gsrv.put(gsrv_name);
			if (UtilStr.isX(row.toBox().get("global").toSeed().getSeed(), "1"))
				global_gsrv.put(gsrv_name);
		}
		for (LeafBean row : store.getObjectGroups("", getParam("application-service"), "", "(vpn_encryption='1' or user_authentication='1')").getBasket())
		{
			String name 	= row.toBox().get("name").toSeed().getSeed();
			String fw_name 	= row.toBox().get("firewall_name").toSeed().getSeed();			
			if (UtilStr.isX(row.toBox().get("vpn").toSeed().getSeed(),"1"))
			{
				goo_encryption.put(name);
				goo_encryption_map.put(name, fw_name);
			}
			if (UtilStr.isX(row.toBox().get("user").toSeed().getSeed(),"1"))
			{
				goo_authentication.put(name);
				goo_authentication_map.put(name, fw_name);
			}
		}
		list_application_service_elements.put("profile", profile);
		list_application_service_elements.put("srv", service);
		list_application_service_elements.put("srv_port", service_port);
		list_application_service_elements.put("gsrv", gsrv);
		list_application_service_elements.put("global_gsrv", global_gsrv);
		list_application_service_elements.put("goo_encryption", goo_encryption);
		list_application_service_elements.put("goo_encryption_map", goo_encryption_map);
		list_application_service_elements.put("goo_authentication", goo_authentication);
		list_application_service_elements.put("goo_authentication_map", goo_authentication_map);
		
		return list_application_service_elements;
	}
	
	private JsonObject list_org_elements()
	{
		JsonObject list_org_elements 	= new JsonObject();		
		JsonArray host_name_ip 			= new JsonArray();
		JsonArray host_name 			= new JsonArray();
		JsonArray host_ip 				= new JsonArray();		
		JsonArray subnet_mask 			= new JsonArray();
		JsonArray area		 			= new JsonArray();
		JsonArray goo 					= new JsonArray();
		JsonArray global_goo			= new JsonArray();
		BasketBean hosts = store.getHosts("where organization=" + store.dbS(getParam("organization")) + " order by name asc");
		for (LeafBean row : hosts.getBasket())
		{
			host_name_ip.put(new JsonObject().put("name",row.toBox().get("name").toSeed().getSeed()).put("ip",row.toBox().get("ip").toSeed().getSeed()));
			host_name.put(row.toBox().get("name").toSeed().getSeed());
			host_ip.put(row.toBox().get("ip").toSeed().getSeed());
		}
		
		for (LeafBean row : store.getSubnets("where organization=" + store.dbS(getParam("organization")) + " order by ip asc, mask asc").getBasket())
			subnet_mask.put(row.toBox().get("ip").toSeed().getSeed() + "/" + row.toBox().get("mask").toSeed().getSeed());
		
		for (LeafBean row : store.getAreas("where organization=" + store.dbS(getParam("organization")) + " order by name asc").getBasket())
			area.put(row.toBox().get("name").toSeed().getSeed());
		try{
		boolean filter_global 		= UtilStr.isX(getParam("filter-global"),"true");
		boolean do_filter			= filter_global && !isAdmin();
UtilLog.out("filter_global:" + filter_global + " - do_filter:" + do_filter);
		for (LeafBean row : store.getObjectGroups("", getParam("application-service"), getParam("organization"), "").getBasket())
		{
			String goo_name 	= row.toBox().get("name").toSeed().getSeed();
			boolean predefined 	= UtilStr.isX(row.toBox().get("global").toSeed().getSeed(), "1");
			if (!(do_filter & predefined))
				goo.put(goo_name);

			if (predefined)
				global_goo.put(goo_name);				
		}
		}catch(Exception e){UtilLog.printException(e);}
		list_org_elements.put("host_name", host_name);
		list_org_elements.put("host_ip", host_ip);
		list_org_elements.put("subnet_mask", subnet_mask);
		list_org_elements.put("area", area);
		list_org_elements.put("goo", goo);
		list_org_elements.put("global_goo", global_goo);
		return list_org_elements;
	}
	
	private JsonObject list_members()
	{
		JsonObject list 	= new JsonObject();		
		BasketBean rows 	= new BasketBean();
		if (UtilStr.isX(getParam("type"), "goo"))
		{
			//Umap area_id_2_name = store.initAreaNameMap();
			rows 				= store.getObjectGroupMembers(getParam("group"), getParam("ts_name"), getParam("org"));
			JsonArray hosts 	= new JsonArray();
			JsonArray subnets 	= new JsonArray();
			JsonArray goos		= new JsonArray();
			JsonArray hosts_fw 	= new JsonArray();
			JsonArray subnets_fw= new JsonArray();
			JsonArray goos_fw	= new JsonArray();
			for (LeafBean row : rows.getBasket())
			{		
				BoxBean box 	= row.toBox();
				String type 	= box.get("type").toSeed().getSeed();
				String fw_name 	= box.get("fw_name").toSeed().getSeed();
				Umap object		= new Umap().setKVP(KVP.KVP3);
				if (UtilStr.isX(type, "host"))
				{
					String name = box.get("arg1").toSeed().getSeed();
					hosts.put(name);
					object.push("output", fw_name).push("cat", type).push("uc", "member")
						.push("host", name)
						.push("ip", box.get("arg2").toSeed().getSeed())
						.push("ip-tsl", box.get("arg3").toSeed().getSeed())
						;
					hosts_fw.put(new JsonObject().put("id", fw_name).put("value", object.parameterize()));
				}
				else if (UtilStr.isX(type, "subnet"))
				{
					String ip_mask 	= box.get("arg1").toSeed().getSeed();
					String area_id	= box.get("arg2").toSeed().getSeed();
					subnets.put(ip_mask);
					object.push("output", fw_name).push("cat", type).push("uc", "member")
						.push("subnet", ip_mask)
						//.push("area-id", area_id)
						//.push("area", area_id_2_name.get(area_id))
						;
					subnets_fw.put(new JsonObject().put("id", fw_name).put("value", object.parameterize()));
				}
				else if (UtilStr.isX(type, "goo"))
				{
					String name	= box.get("arg1").toSeed().getSeed();
					goos.put(name);
					object.push("output", fw_name).push("cat", type).push("uc", "member")
						.push("goo", name);
					goos_fw.put(new JsonObject().put("id", fw_name).put("value", object.parameterize()));
				}
			}
			list.put("hosts", hosts).put("subnets", subnets).put("goos", goos)
				.put("hosts_fw", hosts_fw).put("subnets_fw", subnets_fw).put("goos_fw", goos_fw);
		}
		else if (UtilStr.isX(getParam("type"), "gsrv"))
		{	
			rows = store.getServiceGroupMembers(getParam("group"), getParam("ts_name"));
			JsonArray members 		= new JsonArray();
			JsonArray members_fw 	= new JsonArray();
			for (LeafBean row : rows.getBasket())
			{
				BoxBean box 	= row.toBox();
				String name 	= box.get("name").toSeed().getSeed();
				String fw_name	= box.get("fw_name").toSeed().getSeed();
				members.put(name);
				
				Umap srv = new Umap().setKVP(KVP.KVP3);
				srv.push("output", fw_name).push("uc", "member")
					.push("srv-name", name)
					.push("srv-proto", box.get("proto").toSeed().getSeed())
					.push("srv-port", box.get("port").toSeed().getSeed());
				members_fw.put(new JsonObject().put("id", fw_name).put("value", srv.parameterize()));;
			}
			list.put("srvs", members).put("srvs_fw", members_fw);
		}
		return list;
	}
	
	private JsonObject autocomplete()
	{
		JsonObject values 	= new JsonObject();
		String element 		= getParam("element");
		try
		{
			if (UtilStr.isX(element, "host"))
			{
				String ips = "";
				BasketBean rows = store.getHosts("where name="+store.dbS(getParam("name")));
				for (int i=0 ; i<rows.getBasket().size() ; i++)
				{					
					BoxBean box = rows.getBasket().get(i).toBox();
					String ip	= box.get("ip").toSeed().getSeed();
					if (i==0)
					{
						values.put("name", box.get("name").toSeed().getSeed())
						.put("ip", ip)
						.put("tsl_ip", box.get("tsl_ip").toSeed().getSeed())
						.put("environment", box.get("environment").toSeed().getSeed())
						.put("area", UtilStr.isX(box.get("organization").toSeed().getSeed(), "mobistar") & !UtilStr.isEmpty(ip) ? store.getHostArea(ip) : "")
						;
					}
					else
					{
						ips += UtilStr.isEmpty(ips) ? ip : ", " + ip;
					}
				}
				values.put("ips", ips);
			} else if (UtilStr.isX(element, "ip")) {
				BoxBean box = store.getHosts("where ip="+store.dbS(getParam("ip"))).getBasket().get(0).toBox();
				values.put("name", box.get("name").toSeed().getSeed())
						.put("ip", box.get("ip").toSeed().getSeed())
						.put("tsl_ip", box.get("tsl_ip").toSeed().getSeed());
			} else if (UtilStr.isX(element, "subnet")) {
				String[] ip_mask = getParam("subnet").split("/");
				BoxBean box = store.getSubnets("where ip=" + store.dbS(ip_mask[0]) + " and mask=" + store.dbS(ip_mask[1]) + " and organization=" + store.dbS(getParam("org"))).getBasket().get(0).toBox();
				values.put("subnet", getParam("subnet"))
						.put("area", box.get("area").toSeed().getSeed());				
			} else if (UtilStr.isX(element, "goo")) {
				BoxBean box = store.getObjectGroups(getParam("name"), getParam("ts_name"), getParam("org"), "").getBasket().get(0).toBox();
				values.put("name", box.get("name").toSeed().getSeed())
				.put("organization", box.get("organization").toSeed().getSeed())
				.put("environment", box.get("environment").toSeed().getSeed());
			} else if (UtilStr.isX(element, "service")) {
				BoxBean box = store.getServices(getParam("name"), getParam("application-service"), "").getBasket().get(0).toBox();
				values.put("name", box.get("name").toSeed().getSeed())
						.put("protocol", box.get("protocol").toSeed().getSeed())
						.put("type", box.get("type").toSeed().getSeed())
						.put("port", box.get("port").toSeed().getSeed());
			} else if (UtilStr.isX(element, "srv-port")) {
				BoxBean box = store.getServices("", getParam("application-service"), "port=" + store.dbS(getParam("port"))).getBasket().get(0).toBox();
				values.put("name", box.get("name").toSeed().getSeed())
						.put("protocol", box.get("protocol").toSeed().getSeed())
						.put("type", box.get("type").toSeed().getSeed())
						.put("port", box.get("port").toSeed().getSeed());
				/*
				String u_port 	= getParam("port");
				int i_port 		= Util.getInteger(u_port);
				JsonArray srvs	= new JsonArray();
				//values.put("name", "").put("protocol", "").put("port", "");
				for (LeafBean leaf : store.getServices("", getParam("application-service"), "port!='-'").getBasket())
				{
					BoxBean box 	= leaf.toBox();
					String port		= box.get("port").toSeed().getSeed();
					String[] ports 	= port.split("\\:");
					try
					{
						if 
						(
							(ports.length==1 && UtilStr.isX(ports[0], u_port)) 
							|
							(ports.length==2 && (Util.getInteger(ports[0]) <= i_port & i_port <= Util.getInteger(ports[1])))
						)
						{
							srvs.put(box.get("name").toSeed().getSeed());
						}
					}
					catch (Exception e)
					{
						UtilLog.printException(e);
					}
					values.put("srvs", srvs);
				}
				*/
			}else if (UtilStr.isX(element, "gsrv")) {
				//currently no autocomplete
			} else if (UtilStr.isX(element, "profile")) {
				BoxBean box = store.getProfiles(getParam("name"), getParam("application-service")).getBasket().get(0).toBox();
				values.put("name", box.get("name").toSeed().getSeed())
						.put("owner", box.get("owner").toSeed().getSeed())
						.put("description", box.get("description").toSeed().getSeed());
			}
		} catch (Exception e){}
		return values;
	}
	
	private JsonObject check_private_range_constraint()
	{
		JsonObject result 	= new JsonObject();
		String elem 		= getParam("element");
		if (UtilStr.isEmpty(elem))
			return result.put("statusCode", UtilApp.SC_FAULT + "").put("statusDescription", "missing element type to check.");
		
		String val			= getParam(elem);		
		if (UtilStr.isEmpty(val))
			return result.put("statusCode", UtilApp.SC_MISS_VAL + "").put("statusDescription", "Missing " + elem + ".");
		else if (UtilStr.isX(elem, "ip") | UtilStr.isX(elem, "subnet"))
		{
UtilLog.out("check.");
			Ulist[] ranges = store.getPrivateRanges();
			UtilLog.out("ranges:"+ranges.length);
UtilLog.out(ranges[0].toJson().toString());
UtilLog.out(ranges[0].toJson().toString());
			if (UtilStr.isX(elem, "ip"))
			{
UtilLog.out("isInRange:" + val);
				/** 
				 * @note if the ip is in a private ranges then it means that ip is not external
				 * @return KO if ip is not external
				 **/
				if (UtilNet.isInRange(val, ranges[0], ranges[1]))
					return result.put("statusCode", UtilApp.SC_KO + "").put("statusDescription", "invalid ip address");
				else 
					return result.put("statusCode", UtilApp.SC_OK + "").put("statusDescription", "valid ip address");
			}
			else if (UtilStr.isX(elem, "subnet"))
			{
				/** 
				 * @note if the subnet is in or overlap a private ranges then it means that subnet is not external
				 * @return KO if subnet is not external
				 **/
				if (UtilNet.isSubnetInOrOverlapRange(val, ranges[0], ranges[1]))
					return result.put("statusCode", UtilApp.SC_KO + "").put("statusDescription", "invalid subnet");
				else 
					return result.put("statusCode", UtilApp.SC_OK + "").put("statusDescription", "valid subnet");
			}
		}
		return result.put("statusCode", UtilApp.SC_FAULT + "").put("statusDescription", "unexpected error occurs while check " + elem + " address constraint.");
	}
	
	private JsonObject load_maintain_form()
	{
		String fid 		= getParam("form-id");
		JsonObject form = new JsonObject();		
		
		if (!UtilStr.isEmpty(fid))
		{
			Map<String,Umap> form_elements 	= store.retrieveMtnForm(fid);
			Umap ai 						= form_elements.get("adminInfo");
			String fcr_mode = ai.get("fcr-mode");
			UtilLog.out("fcr_mode:" + fcr_mode);
			if (!UtilStr.hasX(fcr_mode, "initialized"))
			{
UtilLog.out("initializeMtnGrp:" + ai.get("maintain-ids"));
				store.initializeMtnGrp(fid, ai);	
				if (!UtilStr.isEmpty(ai.get("maintain-ids")))
					form_elements = store.retrieveMtnForm(fid);
				store.update("update request set form_mode=" + store.dbS(fcr_mode + "-initialized") + " where id='" + fid + "'");
			}
			Umap req_ed_tsk					= form_elements.get("requestEditionTask");
			form.put(UtilApp.HD_AI, ai.toJson().getJsonObject(UtilApp.HD_AI));
			form.put(UtilApp.HD_MTN, form_elements.get("mtns").toJson().getJsonObject(UtilApp.HD_MTN));
			form.put(UtilApp.HD_HD, getHiddenInfo(fid, ai, req_ed_tsk).toJson().getJsonObject(UtilApp.HD_HD));
			addFormMetadata(form, UtilApp.SC_OK, "OK.");
		}
		else
			addFormMetadata(form, UtilApp.SC_MISS_VAL, "Missing form-id.");
		UtilLog.out("form:" + form.toString());
		return form;
	}
	
	private JsonObject load_form()
	{
		String fid 		= getParam("form-id");
		JsonObject form = new JsonObject();
		if(UtilStr.isEmpty(fid) && UtilStr.isX(interaction.get("action"), "document-submit"))
		{
			fid = interaction.get("form-id");
			UtilLog.out("document-submit:" + fid);
		}		
		if (!UtilStr.isEmpty(fid))
		{
			Map<String,Umap> form_elements 		= store.retrieveForm(fid);
			Umap ai 							= form_elements.get("adminInfo");
			Umap req_ed_tsk						= form_elements.get("requestEditionTask");
			String fcr_mode 					= ai.get("fcr-mode");
UtilLog.out(ai.toString());
			if (UtilStr.isX(fcr_mode, "new"))
			{
				store.update("update request set form_mode='created' where id='" + fid + "'");
			}
			else if (UtilStr.isX(fcr_mode, "dup"))
			{
				String dup_form_id = ai.get("fcr-dup-id");
				if (!UtilStr.isEmpty(dup_form_id))
				{				
					store.update("update request set form_mode='duplicated' where id='" + fid + "'");
					duplicate_form(dup_form_id, fid);
					form_elements 	= store.retrieveForm(fid);
					ai 				= form_elements.get("adminInfo");
					req_ed_tsk		= form_elements.get("requestEditionTask");
				}
			}
			else if (UtilApp.isChangeRule(fcr_mode) & !UtilStr.hasX(fcr_mode, "initialized"))
			{
				UtilLog.out("LOADING DATA FROM FWRULES");
				/**
				 * init fcr using data in fwrules database.
				 * CAUTION: maintain_ids represents in this case rule ids. 
				 **/
				form_elements.get("techInfo").put("form-selector", "advanced");
				form_elements.put("aefs", fwrules.init_fcr_with_rules(fid, ai, store));
				store.updateFormInDatabase(fid, form_elements, false);
				store.update("update request set form_mode='" + fcr_mode + "-initialized' where id='" + fid + "'");
				form_elements 	= store.retrieveForm(fid);
				ai 				= form_elements.get("adminInfo");
				req_ed_tsk		= form_elements.get("requestEditionTask");
			}
			
			form.put(UtilApp.HD_AI, ai.toJson().getJsonObject(UtilApp.HD_AI));
			form.put(UtilApp.HD_TI, form_elements.get("techInfo").toJson().getJsonObject(UtilApp.HD_TI));
			form.put(UtilApp.HD_SEF, form_elements.get("sef").toJson().getJsonObject(UtilApp.HD_SEF));
			form.put(UtilApp.HD_AEF, form_elements.get("aefs").toJson().getJsonObject(UtilApp.HD_AEF));
			
			Umap hidden = getHiddenInfo(fid, ai, req_ed_tsk);

			addFormMetadata(form, UtilApp.SC_OK, "OK.").put(UtilApp.HD_HD, hidden.toJson().getJsonObject(UtilApp.HD_HD));		
		}
		else
			addFormMetadata(form, UtilApp.SC_MISS_VAL, "Missing form-id.");
		
		return form;		
	}
	
	private Umap getHiddenInfo(String fid, Umap adminInfo, Umap reqEdTask)
	{
		UtilLog.out("getHiddenInfo:" + fid);
		
		Umap hidden 			= UtilApp.getHiddenInfoList();
		String user 			= get_SM_USER();
		String user_group		= getLdapGroup(user, UtilStr.isX(user, adminInfo.get("requestor")));
		String task 			= reqEdTask.get("task");
		String expected_group 	= reqEdTask.get("actor");
		String expected_user	= reqEdTask.get("editor");
		String ro 				= "true";
		String req 				= "";	
		String fwe 				= "";	
		String fwo 				= "";
		
		hidden.push("form-id", fid);
		hidden.push("user", user);
		hidden.push("user-group", user_group);
		hidden.push("task", task);
		hidden.push("expected-user", expected_user);
		hidden.push("expected-group", expected_group);
		hidden.push("change-rule", UtilApp.isChangeRule(adminInfo.get("fcr-mode")) + "");
		hidden.push("tsrr", UtilApp.isTSRR(adminInfo.get("fcr-mode")) + "");
		
		if (UtilStr.isX(reqEdTask.get("statusCode"), UtilApp.SC_OK + ""))
		{
			if (UtilStr.hasX(task, "req"))
			{
				if (UtilStr.isX(user, expected_user))
				{
					/** editable only iff edition task **/
					ro 	= !UtilStr.hasX(task, "edition") + "";
					req = "true";
					hidden.push("formViewStatusDescription", "OK");
				}
				else
					hidden.push("formViewStatusDescription", "Error: current user and expected user mismatch.");
			}
			else if (UtilStr.hasX(task, "fw_expert"))
			{
				if (!UtilStr.hasX(user_group, expected_group))
					hidden.push("formViewStatusDescription", "Error: user is not allowed to process task.");
				else if (UtilStr.hasX(task, "edition"))
				{
					if (UtilStr.isX(user, expected_user))
					{
						ro 	= "false";
						fwe = "true";
						hidden.push("formViewStatusDescription", "OK.");
					}
					else
						hidden.push("formViewStatusDescription", "Error: current user and expected user mismatch.");
				}
				else
					hidden.push("formViewStatusDescription", "OK.");
			}
			else if (UtilStr.hasX(task, "fw_operations"))
			{
				if (!UtilStr.hasX(user_group, expected_group))
					hidden.push("formViewStatusDescription", "Error: user is not allowed to process task.");
				else
				{
					fwo = "true";
					hidden.push("formViewStatusDescription", "OK.");
				}
			}
			else if (UtilStr.hasX(user_group, expected_group))
				hidden.push("formViewStatusDescription", "OK.");
			else
				hidden.push("formViewStatusDescription", "Error: user is not allowed to process task.");
			
		}
		else
			hidden.push("formViewStatusDescription", reqEdTask.get("statusDescription"));
		
		hidden.push("fwe", fwe);
		hidden.push("fwo", fwo);
		hidden.push("req", req);
		hidden.push("readonly", ro);
		
		/** 
		 * in order to determine which form (simple or advanced) to display first
		 * I define two vars new_fcr:boolean, advanced:boolean
		 * new_fcr is true when the fcr is loaded for the very first time
		 * advanced is true when user belongs to the ldap group 'SecTools - FCR Web - Advanced users'.
		 * if new_fcr and advanced then the form initially loaded in advanced form, else simple.
		 * **/	
		hidden.push("form-sel", adminInfo.get("form-sel"));
		hidden.push("user-type", getUserType());
		hidden.push("adm", isAdmin() + "");

		return hidden;
	}
	
	/**
	 * 
	 * @param dup_form_id id of the form to be duplicated
	 * @param form_id the id of the form in which duplication must be done
	 * @return result of the duplication operation
	 */
	private JsonObject duplicate_form(String dup_form_id, String form_id)
	{
		JsonObject form = new JsonObject();
		if (!UtilStr.isEmpty(dup_form_id))
		{
			store.updateFormInDatabase(form_id, store.retrieveForm(dup_form_id), true);
			store.duplicateFlowDocumentation(form_id, dup_form_id);
		}

		return form;		
	}
	
	private JsonObject submit_drop_save_form()
	{
		JsonObject form = new JsonObject();
		try
		{			
			String form_id 		= getParam("form-id");
			if (UtilStr.isEmpty(form_id))
			{
				addFormMetadata(form, UtilApp.SC_MISS_VAL, "form-id is Missing");
				return form;
			}
			Umap ai 			= store.retrieveAdminAndTechInfo(form_id);
			String requestor 	= ai.get("requestor");
			String behalf		= ai.get("on-behalf-of");// utility ?
			String user 		= get_SM_USER();									//editor-id
			String ldap_group	= getLdapGroup(user, UtilStr.isX(user, requestor));	//editor-group-id
			Umap taskInfo 		= store.getEditionTaskNameAndProcess(form_id, user, ldap_group);
			Umap arguments 		= new Umap().setKVP(KVP.KVP1);			
			boolean is_mtn_fcr	= UtilApp.isMtn(ai.get("fcr-mode"));
			
UtilLog.out("ai:" + ai.parameterize());
UtilLog.out("taskInfo:" + taskInfo.parameterize());
UtilLog.out("is_mtn_fcr:" + is_mtn_fcr);
			if (UtilStr.hasX(action, "save-form"))
			{
				saveForm(form_id, is_mtn_fcr);				
				addFormMetadata(form, UtilApp.SC_OK, "Form saved."); 
				/*
				//requestor only can save form.
				UtilLog.out("expected requestor to save form:" + requestor);
				if (UtilStr.isX(user, requestor))
				{
					//TODO notify save Error;
					
				}
				else
					addFormMetadata(form, UtilApp.SC_NO_RIGHT, "Error: Unauthorized action."); 
				*/
				
				return form;
			}
			else if (Util.getInteger(taskInfo.get("code"))>=0)
			{
				arguments.push("d_processUUID", taskInfo.get("process")).push("d_taskUUID", taskInfo.get("task"));
				
				String req_tso_action = is_mtn_fcr ? "pr_tso_action" : "pr_req_action";
				if (UtilStr.hasX(action, "submit-form"))
				{
					//TODO notify save Error;
					saveForm(form_id, is_mtn_fcr);					
					
					if (UtilStr.isX(ldap_group,"req"))
						arguments.push(req_tso_action, "Submit");
					//fwe has unique transition edge at edition task
						
					String logs = store.startBonitaProcess(user, "FCR_Web_Commit_Event" , new Umap().push("pr_arguments", arguments.parameterize()));
					
					if (UtilStr.hasX(logs, "error"))
						addFormMetadata(form, UtilApp.SC_KO, "Error(s) occurs while commit user action to Bonita");
					else
						addFormMetadata(form, UtilApp.SC_OK, "Form has been submitted.");
				}
				else if (UtilStr.hasX(action, "drop-form"))
				{
					//requestor only can drop form.
					if (UtilStr.isX(user, requestor))
					{
						arguments.push(req_tso_action, "Drop");
						String logs = store.startBonitaProcess(user, "FCR_Web_Commit_Event", new Umap().push("pr_arguments", arguments.parameterize()));
						
						if (UtilStr.hasX(logs, "error"))
							addFormMetadata(form, UtilApp.SC_KO, "Error(s) occurs while commit user action to Bonita");
						else
							addFormMetadata(form, UtilApp.SC_OK, "Form has been dropped."); 
							// this is a false error but once form has been dropped 
							// further modification is not admitted 
					}
					else
						addFormMetadata(form, UtilApp.SC_NO_RIGHT, "Error: Unauthorized action."); 
				}				
			}
			else
				addFormMetadata(form, UtilApp.SC_MISS_VAL, "Error(s) Unable to commit user action: missing bonita references");
				
		} catch (Exception e) {
			UtilLog.printException(e);
			addFormMetadata(form, UtilApp.SC_FAULT, "Error(s) occurs while attempt to commit user action");
		}
		return form;		
	}	

	private JsonObject addFormMetadata(JsonObject form)
	{
		return form
			.put("noCache", getParam("noCache"))
			.put("action", action);
	}
	private JsonObject addFormMetadata(JsonObject form, int statusCode, String statusDescription)
	{
		return addFormMetadata
		(
			form
				.put("statusCode", statusCode + "")
				.put("statusDescription", statusDescription)
		);
	}
	
	private void saveForm(String form_id, boolean isMtn) 
	{	
		if (!isMtn)
			store.updateFormInDatabase(form_id, extractForm(isMtn), false);
		else
			store.updateMtnFormInDatabase(form_id, extractForm(isMtn));
	}
	
	private Map<String,Umap> extractForm(boolean isMtn)
	{
UtilLog.out("extractForm:" + isMtn);
		Map<String,Umap> collections = new HashMap<String,Umap>();
		collections.put("adminInfo", extractSimpleListParam(UtilApp.getAdminInfoList().keyList()));
		collections.put("techInfo", extractSimpleListParam(UtilApp.getTechInfoList().keyList()));
		collections.put("docInfo", new Umap());
		
		if (!isMtn){
			collections.put("sef", extractSimpleListParam(UtilApp.getSefInfoList().keyList()));
			collections.put("aefs", extractGrpsInfo("f-", "nb-flow", UtilApp.HD_AEF));
		}
		else
		{
			collections.put("mtns", extractGrpsInfo("g-", "nb-group", UtilApp.HD_MTN));
		}		
		return collections;
	}
	
	private Umap extractSimpleListParam(List<String> params) {
		Umap map = new Umap();
		for (String param : params)
			map.put(param, getParam(param));
		return map;
	}	
	
	private Umap extractGrpsInfo(String prefix, String count_id, String group_name) 
	{
		String count 	= getParam(prefix + count_id);
		Umap grps 		= new Umap().setName(group_name);
		grps.put(prefix + count_id, count);
		if (!UtilStr.isEmpty(count) && Util.getInteger(count)>0)
		{
			Map<String,Object> parameters = request.getParameterMap();
			for (Object k : request.getParameterMap().keySet())
			{
				String key = (String)k;
				if (key.startsWith(prefix))
					grps.put(key, getParam(key));
			}
		}
UtilLog.out(grps.getName() + ":" + grps.toJson());
		return grps;
	}

	private String get_SM_USER(){
	
		if (Config.atCelsius)
			return celsiusUser;
		else 
			return getHeader("sm_user");		
	}
	
	
	private String memberofgroups()
	{
		if (Config.atCelsius)
		{
			String extra	= "cn=SecTools - FCR Web- TS Owners Grp1,ou=groups,ou=staff,o=mobistar.be";
			if (!UtilStr.isEmpty(localmemberofgroups) & !UtilStr.isEmpty(extra))
				return localmemberofgroups + "^" + extra;
			else 
				return localmemberofgroups + extra;
		}
		else return request.getHeader("memberofgroups");
	}
	
	
	private boolean isAdmin(){return UtilStr.hasX(memberofgroups(), "SecTools - FCR Web - Administrators");}
	
	private int is_tso 		= 0;
	private String ts_list 	= "";
	
	private void init_tso_values()
	{
		if (is_tso==0)
		{
			is_tso = 2;
			String groups = memberofgroups();
			for (String tso_ldap_group : store.getTS_Owners())
			{
				if (UtilStr.hasX(groups, tso_ldap_group))
				{
					is_tso = 1;
					ts_list += UtilStr.isEmpty(ts_list) ? tso_ldap_group : "," + tso_ldap_group; 
				}
			}
		}
	}
	
	private boolean isTSO()
	{
		init_tso_values();
		return is_tso == 1;
	}
	
	private String getTS_list()
	{
		init_tso_values();
		return ts_list;
	}
	
	private String getLdapGroup(String login, boolean isReqOrBehalf)
	{
		String user_group = "";
		if (isReqOrBehalf)	
			user_group = "req";
		if (UtilStr.hasX(memberofgroups(), "SecTools - FCR Web - FW Experts"))	
			user_group += UtilStr.isEmpty(user_group) ? "fwe": ",fwe";
		if (UtilStr.hasX(memberofgroups(), "SecTools - FCR Web - Security Officers"))	
			user_group += UtilStr.isEmpty(user_group) ? "so": ",so";			
		if (isTSO())
			user_group += UtilStr.isEmpty(user_group) ? "tso": ",tso";
		if (UtilStr.hasX(memberofgroups(), "SecTools - FCR Web - FW Operations"))	
			user_group += UtilStr.isEmpty(user_group) ? "fwo": ",fwo";
		return user_group;
	}

	private String getUserType()
	{
		return UtilStr.hasX(memberofgroups(), "SecTools - FCR Web - Advanced users") ? UtilApp.user_type_advanced : UtilApp.user_type_simple;
	}
	
	private String getRulesSample()
	{
		return "{\"ruleSummary\": [{\"id\": \"11378020\",\"name\": \"KYKA 5\",\"source\": [{\"id\": \"39992618\",\"name\": \"Any\",\"type\": \"any\",\"predefined\": \"1\",\"profile\": \"\"}],\"destination\": [{\"id\": \"39992618\",\"name\": \"Any\",\"type\": \"any\",\"predefined\": \"1\",\"profile\": \"null\"}],\"service\": [{\"id\": \"12098800\",\"name\": \"Any\",\"type\": \"any\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"action\": \"accept\",\"nr\": \"10.0\",\"technicalService\": \"TS: VPN Securemote NetCracker\",\"active\": \"true\",\"last_used\": \"null\"},{\"id\": \"11379180\",\"name\": \"BUSO 0001\",\"source\": [{\"id\": \"39992887\",\"name\": \"Any\",\"type\": \"any\",\"predefined\": \"1\",\"profile\": \"\"}],\"destination\": [{\"id\": \"40002948\",\"name\": \"Cluster-ParCore2\",\"type\": \"gateway_cluster\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"service\": [{\"id\": \"12099868\",\"name\": \"IPSEC\",\"type\": \"group\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"12101417\",\"name\": \"IKE\",\"type\": \"udp\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"12101937\",\"name\": \"ntp-udp\",\"type\": \"udp\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"action\": \"accept\",\"nr\": \"1.0\",\"technicalService\": \"TS: VPN Securemote NetCracker\",\"active\": \"true\",\"last_used\": \"null\"},{\"id\": \"11379207\",\"name\": \"BUSO 0125\",\"source\": [{\"id\": \"39993137\",\"name\": \"extA7670-A08BRU-192.168.76.15\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"\"},{\"id\": \"39993333\",\"name\": \"extA7670-A08BRU-192.168.70.5\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"\"},{\"id\": \"39993428\",\"name\": \"extMsRCP-PCFS-192.168.9.98\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"\"},{\"id\": \"39993429\",\"name\": \"extMsSSP-PCFS-192.168.9.97\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"\"},{\"id\": \"39993778\",\"name\": \"allGSU2A108Grp\",\"type\": \"group\",\"predefined\": \"unknown\",\"profile\": \"\"}],\"destination\": [{\"id\": \"39993576\",\"name\": \"allNSS-OMC-CSGrp\",\"type\": \"group\",\"predefined\": \"0\",\"profile\": \"null\"}],\"service\": [{\"id\": \"12101488\",\"name\": \"snmpTrap-162\",\"type\": \"udp\",\"predefined\": \"0\",\"profile\": \"null\"}],\"action\": \"accept\",\"nr\": \"20.0\",\"technicalService\": \"TS: VPN Securemote NetCracker\",\"active\": \"true\",\"last_used\": \"null\"},{\"id\": \"11382318\",\"name\": \"MOTA 1000\",\"source\": [{\"id\": \"39992887\",\"name\": \"Any\",\"type\": \"any\",\"predefined\": \"1\",\"profile\": \"L-bss2GipAdmin\"}],\"destination\": [{\"id\": \"40002623\",\"name\": \"appBscTcuPcuGrp\",\"type\": \"group\",\"predefined\": \"0\",\"profile\": \"null\"}],\"service\": [{\"id\": \"12099341\",\"name\": \"echo-request\",\"type\": \"icmp\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"12099453\",\"name\": \"OSIFTAM\",\"type\": \"tcp\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"12101970\",\"name\": \"telnet\",\"type\": \"tcp\",\"predefined\": \"1\",\"profile\": \"null\"},{\"id\": \"12101979\",\"name\": \"ftp_basic\",\"type\": \"tcp\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"12101984\",\"name\": \"ssh-22\",\"type\": \"tcp\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"action\": \"Client Auth\",\"nr\": \"1336.0\",\"technicalService\": \"TS: VPN Securemote NetCracker\",\"active\": \"true\",\"last_used\": \"null\"},{\"id\": \"11379181\",\"name\": \"BUSO 0002\",\"source\": [{\"id\": \"39992887\",\"name\": \"Any\",\"type\": \"any\",\"predefined\": \"1\",\"profile\": \"\"}],\"destination\": [{\"id\": \"39993911\",\"name\": \"fwlVRRP-Bulq-Sora\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"service\": [{\"id\": \"12101928\",\"name\": \"ntpGrp\",\"type\": \"group\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"action\": \"accept\",\"nr\": \"2.0\",\"technicalService\": \"TS: XML gateway access\",\"active\": \"true\",\"last_used\": \"null\"},{\"id\": \"11381356\",\"name\": \"MOTA 2222\",\"source\": [{\"id\": \"40003364\",\"name\": \"appCitrixForEricssonGrp\",\"type\": \"group\",\"predefined\": \"unknown\",\"profile\": \"\"}],\"destination\": [{\"id\": \"40003453\",\"name\": \"grpOmc2gGrp\",\"type\": \"group\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"service\": [{\"id\": \"12101970\",\"name\": \"telnet\",\"type\": \"tcp\",\"predefined\": \"1\",\"profile\": \"null\"}],\"action\": \"accept\",\"nr\": \"567.0\",\"technicalService\": \"TS: XML gateway access\",\"active\": \"true\",\"last_used\": \"null\"},{\"id\": \"11382057\",\"name\": \"MOTA 0533\",\"source\": [{\"id\": \"39993716\",\"name\": \"extEntraServerGrp\",\"type\": \"group\",\"predefined\": \"unknown\",\"profile\": \"\"}],\"destination\": [{\"id\": \"40003749\",\"name\": \"appDomainControllersStaffGrp\",\"type\": \"group\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"service\": [{\"id\": \"12100095\",\"name\": \"minisql\",\"type\": \"tcp\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"12100096\",\"name\": \"adobesrv\",\"type\": \"tcp\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"12101432\",\"name\": \"smtp\",\"type\": \"tcp\",\"predefined\": \"1\",\"profile\": \"null\"},{\"id\": \"12101438\",\"name\": \"name\",\"type\": \"udp\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"action\": \"accept\",\"nr\": \"1091.0\",\"technicalService\": \"TS: XML gateway access\",\"active\": \"true\",\"last_used\": \"null\"},{\"id\": \"11383535\",\"name\": \"FIKI 0531\",\"source\": [{\"id\": \"39993763\",\"name\": \"uxgaladriel-192.168.156.39\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"\"},{\"id\": \"39994202\",\"name\": \"uxArneb-192.168.156.17\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"\"},{\"id\": \"39994203\",\"name\": \"uxArcturus-192.168.156.18\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"\"},{\"id\": \"39998230\",\"name\": \"uxThuban-192.168.156.145\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"\"}],\"destination\": [{\"id\": \"39997667\",\"name\": \"uxMmsTraffic2-175.175.32.31\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"service\": [{\"id\": \"12101913\",\"name\": \"http-8080\",\"type\": \"tcp\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"12102709\",\"name\": \"http\",\"type\": \"tcp\",\"predefined\": \"1\",\"profile\": \"null\"}],\"action\": \"accept\",\"nr\": \"612.0\",\"technicalService\": \"TS: XML gateway access\",\"active\": \"true\",\"last_used\": \"null\"},{\"id\": \"11383682\",\"name\": \"FIKI 0097\",\"source\": [{\"id\": \"39996114\",\"name\": \"appBSCS-Cls_TempoIII_Citrix_Grp\",\"type\": \"group\",\"predefined\": \"unknown\",\"profile\": \"\"}],\"destination\": [{\"id\": \"39994829\",\"name\": \"ux-172.31.10.43\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"39994837\",\"name\": \"ux-172.31.10.191\",\"type\": \"host\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"service\": [{\"id\": \"12099560\",\"name\": \"tcp-33000-33100\",\"type\": \"tcp\",\"predefined\": \"unknown\",\"profile\": \"null\"},{\"id\": \"12099596\",\"name\": \"sqlnet-1520-1600\",\"type\": \"tcp\",\"predefined\": \"unknown\",\"profile\": \"null\"}],\"action\": \"accept\",\"nr\": \"702.0\",\"technicalService\": \"TS: XML gateway access\",\"active\": \"false\",\"last_used\": \"null\"}] }";
	}
}
