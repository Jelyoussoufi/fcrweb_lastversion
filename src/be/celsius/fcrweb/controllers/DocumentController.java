package be.celsius.fcrweb.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.web.servlet.ModelAndView;

import be.celsius.fcrweb.sql.FcrWebAccess;
import be.celsius.util.JsonObject;
import be.celsius.util.UtilApp;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilStr;
import be.celsius.util.datastructure.Umap;

public class DocumentController extends SecureParameterHandler{
	
	private FcrWebAccess store;
	public DocumentController()
	{
		super("DocumentController");
		store = new FcrWebAccess();
	}
	
	private Umap file_parameter = new Umap();
	public ModelAndView performRequest(HttpServletRequest request, HttpServletResponse response)
	{			
		UtilLog.out("Document performRequest:" + request.getRequestURI() + " - noCache:" + getParam("noCache"));	
		try 
		{
			UtilLog.out("ServletFileUpload detect multipartContent:" + ServletFileUpload.isMultipartContent(request));
			UtilLog.out("enc-type:" + getParam("enc-type"));
			if (ServletFileUpload.isMultipartContent(request))
			{
UtilLog.out("detected multipart content:");
				ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
				List fileItemsList;
				fileItemsList = servletFileUpload.parseRequest(request);

				String optionalFileName = "";
				FileItem fileItem = null;

				Iterator it = fileItemsList.iterator();
				
				while (it.hasNext())
				{
					FileItem fileItemTemp = (FileItem)it.next();
					if (fileItemTemp.isFormField())
					{
						String name = fileItemTemp.getFieldName();
						String value = fileItemTemp.getString();

						if (fileItemTemp.getFieldName().equals("filename"))
							optionalFileName = fileItemTemp.getString();
					}
					else
						fileItem = fileItemTemp;
				}
				file_parameter.clear();
				//Umap file_parameter = new Umap();
				file_parameter.push("field", fileItem.getFieldName());
				file_parameter.push("name", FilenameUtils.getName(fileItem.getName()));
				file_parameter.push("content-type", fileItem.getContentType());
				file_parameter.push("size", fileItem.getSize() + "");
				file_parameter.push("char-encode", request.getCharacterEncoding());
UtilLog.out("file:" + file_parameter);
				if (fileItem!=null && fileItem.getSize() > 0)
				{
					InputStream uploadedStream = fileItem.getInputStream();
/*
					String content = convertStreamToString(uploadedStream);
UtilLog.out("content:" + content);
					file_parameter.push("content", UtilStr.string2ByteString(content));
*/
					/*
					if (interaction!=null && !UtilStr.isEmpty(interaction.get("form-id")))
					{
UtilLog.out("Save flow-documentation.");
						store.saveFile(
								interaction.get("form-id"), 
								 fileItem.getName(), 
								 fileItem.getSize(),
								 fileItem.getContentType(), 
								 uploadedStream);
						//store.setFlowDocumentation(interaction.get("form-id"), file_parameter);
					}		
					*/
/*
byte[] buffer = new byte[8 * 1024];

try {
  OutputStream output = new FileOutputStream("C:\\Users\\jhuilian\\Desktop\\copy_" + fileItem.getName());
  try {
    int bytesRead;
    while ((bytesRead = uploadedStream.read(buffer)) != -1) {
      output.write(buffer, 0, bytesRead);
    }
  } finally {
    output.close();
  }
} finally {
	uploadedStream.close();
}
*/
					Umap interaction = (Umap)getSessionParam("interaction");
					if (interaction==null)
					{
						interaction = new Umap().setName("interaction");
						setSessionParam("interaction", interaction);
					}
					
					storeFile(interaction.get("form-id"), FilenameUtils.getName(fileItem.getName()), fileItem.getSize(), uploadedStream).toJson().toString();
					//write(storeFile(interaction.get("form-id"), FilenameUtils.getName(fileItem.getName()), fileItem.getSize(), uploadedStream).toJson().toString());
					String redirect = interaction.get("form-url");
					UtilLog.out("interaction:" + interaction.parameterize());
					write
					(
							"<html>" +
							"<head>" +
							"<SCRIPT LANGUAGE='JavaScript' >" +
							"function redirect(){document.location.href = '" + redirect + "';}" +
							"</SCRIPT>" + 
							"</head>" +
							"<body onload='redirect()' ></body>" +
							"</html>"
					);
					return null;
				}
			}
			else if (UtilStr.isX(action, "document-submit"))
			{
				Umap interaction = (Umap)getSessionParam("interaction");
				if (interaction==null)
				{
					interaction = new Umap().setName("interaction");
					setSessionParam("interaction", interaction);
				}
				interaction.put("action", "document-submit");
				interaction.put("form-id", getParam("form-id"));
				interaction.put("form-url", getParam("form-url"));
				write(interaction.toJson().put("noCache", getParam("noCache")).toString());
				return null;
			}
			else if (UtilStr.isX(action, "get-flow-documentation"))
			{				
				String formId = getParam("form-id");
				JsonObject access_response = new JsonObject();
				if(!UtilStr.isEmpty(formId))
				{
					access_response.put("form-id", formId);
					Umap doc = store.getFlowDocumentation(formId);
UtilLog.out("getFlowDocumentation(" + formId + "):" + doc.toJson().toString());
					if (UtilStr.isX(doc.get("file"), "OK"))
					{	
				    	try 
						{
				    		String name 			= doc.get("name");
				    		ServletOutputStream out = response.getOutputStream();
				    		response.setContentType(doc.get("content-type")); 
					    	response.setHeader("Content-Disposition","attachment; filename=" + name + ";");
							String file_dir ="documentation\\fcr_" + formId + "\\" + name;
														
							try 
							{
								IOUtils.copy(new FileInputStream(file_dir), out);
								/* *
								if (UtilStr.endsWith(name, ".pdf"))
						    	{
									PDDocument pdf = PDDocument.load(new File(file_dir));
									pdf.save(out);
						    	}
						    	else if (UtilStr.endsWith(name, ".docx"))
						    	{
						    		XWPFDocument document = new XWPFDocument(new FileInputStream(new File(file_dir)));
									document.write(out);
						    	}
						    	else if (UtilStr.endsWith(name, ".doc"))
						    	{
						    		HWPFDocument document = new HWPFDocument(new FileInputStream(new File(file_dir)));
									document.write(out);
						    	}
						    	else if (UtilStr.endsWith(name, ".xlsx"))
						    	{
						    		XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(new File(file_dir)));
						    		wb.write(out);
						    	}
						    	else if (UtilStr.endsWith(name, ".xls"))
						    	{
						    		HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(new File(file_dir)));
						    		wb.write(out);
						    	}
						    	else if (UtilStr.endsWith(name, ".vsd"))
						    	{
						    		POIFSFileSystem fs 		= new POIFSFileSystem(new FileInputStream(new File(file_dir)));
									HDGFDiagram hdgf 		= new HDGFDiagram(fs.getRoot(), fs);
									hdgf.write(out);
						    	}
								/* */
							}
							catch (Exception e) 
							{
								
							}
					        out.flush();
					        out.close();
							return null;
						} 
						catch (Exception e) 
						{
							write(addFormMetadata(access_response, UtilApp.SC_FAULT, "Exception occurs while accessing flow documentation.").toString());
						}
					}
					else 
					{
						write(addFormMetadata(access_response, UtilApp.SC_KO, "Error: flow documentation not found.").toString());
					}
				}
				else
				{
					write(addFormMetadata(access_response, UtilApp.SC_MISS_VAL, "Error: Missing value form-id.").toString());
				}					
			}
		} catch (FileUploadException e){
			UtilLog.out("FileUploadException:");
			e.printStackTrace();
		} catch (Exception e){
			UtilLog.out("Exception:");
			e.printStackTrace();
		}	
		
		String redirect = ""; 
		
		
		return null;
		//return new ModelAndView("fcr-form");
	}
		
	private Umap storeFile(String formId, String fileName, long size, InputStream in)
	{
		Umap result = new Umap();
		result.push("formId", formId).push("fileName", fileName);
		
		
		
		if (UtilStr.isEmpty(formId) | UtilStr.isEmpty(fileName) | in==null)
		{
			if (UtilStr.isEmpty(formId))
				result.push("statusCode", UtilApp.SC_MISS_VAL + "").push("statusDescription", "Error occurs while saving documentation: missing form id.");
			else if (UtilStr.isEmpty(fileName))		
				result.push("statusCode", UtilApp.SC_MISS_VAL + "").push("statusDescription", "Error occurs while saving documentation: missing document name.");		
			else		
				result.push("statusCode", UtilApp.SC_MISS_VAL + "").push("statusDescription", "Error occurs while saving documentation: document content could not be caught.");
			return result;
		}
		
		String query = "update request set ";
		query += "filename=" + store.dbS(fileName) + ", size=" + store.dbS(size + "") + ", mediatype=" + store.dbS(UtilApp.getContentType(fileName));
		query += " where id="+ store.dbS(formId);
		store.update(query);
		
		String doc_path 	= "documentation";
	    File doc_directory 	= new File(doc_path);
		if (!doc_directory.exists()) doc_directory.mkdir();		
		String fcr_path 	= doc_path + "\\fcr_" + formId;
		File fcr_directory 	= new File(fcr_path);
		if (!fcr_directory.exists()) fcr_directory.mkdir();
		String filepath_on_disk = fcr_path + "\\" + fileName;
		
		//String filepath_on_disk = fileName;
		
		try 
		{
			File f = new File(".");
			result.push("current_dir", f.getAbsolutePath());
			FileOutputStream stream = new FileOutputStream(filepath_on_disk);			
			try
			{
				IOUtils.copy(in, stream);
				/*
				if (fileName.endsWith(".pdf"))
				{
					UtilLog.out("attempt to save PDF " + fileName);	
					Document document 	= new Document();
					PDDocument doc 		= PDDocument.load(in);
					doc.save(stream);
					doc.close();
					UtilLog.out("PDF saved.  " + fileName);
					result.push("statusCode", UtilApp.SC_OK + "").push("statusDescription", "File saved to " + filepath_on_disk);
				}
				else if (fileName.endsWith(".vsd"))
				{
					UtilLog.out("attempt to save VISIO " + fileName);
					//HDGFDiagram hdgf = new HDGFDiagram(new POIFSFileSystem(in));
					
					IOUtils.copy(in,stream);
					UtilLog.out("VISIO saved.  " + fileName);
					result.push("statusCode", UtilApp.SC_OK + "").push("statusDescription", "File saved to " + filepath_on_disk);
				}
				else if (fileName.endsWith(".docx"))
				{
					UtilLog.out("attempt to save WORD 2000 " + fileName);
					//XWPFDocument document	= new XWPFDocument(in);
					//document.write(stream);
					IOUtils.copy(in,stream);
					UtilLog.out("WORD 2000 saved.  " + fileName);
					result.push("statusCode", UtilApp.SC_OK + "").push("statusDescription", "File saved to " + filepath_on_disk);
				}
				else if (fileName.endsWith(".doc"))
				{
					UtilLog.out("attempt to save WORD " + fileName);
					HWPFDocument document	= new HWPFDocument(in);
					HWPFFileSystem fs	 	= new HWPFFileSystem();
					document.write(stream);
					UtilLog.out("WORD saved.  " + fileName);
					result.push("statusCode", UtilApp.SC_OK + "").push("statusDescription", "File saved to " + filepath_on_disk);
				}
				else if (fileName.endsWith(".xlsx"))
				{				
					UtilLog.out("attempt to save EXCEL 2000 " + fileName);
					XSSFWorkbook wb 		= new XSSFWorkbook(in);
					wb.write(stream);
					UtilLog.out("EXCEL 2000 saved.  " + fileName);
					result.push("statusCode", UtilApp.SC_OK + "").push("statusDescription", "File saved to " + filepath_on_disk);
				}
				else if (fileName.endsWith(".xls"))
				{
					UtilLog.out("attempt to save EXCEL " + fileName);
					//HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(new File("test.xls")));
					HSSFWorkbook wb 			= new HSSFWorkbook(in);
					POIFSFileSystem filesystem 	= new POIFSFileSystem();
				    filesystem.createDocument(new ByteArrayInputStream(wb.getBytes()), "Workbook");
				    filesystem.writeFilesystem(stream);			    
				    UtilLog.out("EXCEL saved.  " + fileName);
				    result.push("statusCode", UtilApp.SC_OK + "").push("statusDescription", "File saved to " + filepath_on_disk);
				}
				else
				{
					UtilLog.out("Unsupported file " + fileName);
					result.push("statusCode", UtilApp.SC_KO + "").push("statusDescription", "Error, unsupported file.");
				}
				/* */
					
			}
			catch (Exception e)
			{
				result.push("statusCode", UtilApp.SC_FAULT + "").push("statusDescription", "Exception occurs while saving document (1).");
			}
			
			stream.close();
		}
		catch (Exception e)
		{
			UtilLog.printException(e);
			result.push("statusCode", UtilApp.SC_FAULT + "").push("statusDescription", "Exception occurs while saving document (2).");
		}
		return result;
	}
	
	private String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	private JsonObject addFormMetadata(JsonObject form, int statusCode, String statusDescription)
	{
		return form
				.put("statusCode", statusCode + "")
				.put("statusDescription", statusDescription)
				.put("noCache", getParam("noCache"))
				.put("action", request.getRequestURI());
	}
	
	
	
	public static void main(String[] args) {

		String s = "C:\\oracle\\Middleware\\user_projects\\domains\\my_domain_test_1\\documentation\\fcr_9398";
		String[] elems = s.split("\\");
		System.out.println("s1:"+elems[elems.length-1]);
		String s2 = "C:/oracle/Middleware/user_projects/domains/my_domain_test_1/documentation/fcr_9398";
		String[] elems2 = s.split("/");
		System.out.println("s2:"+elems2[elems2.length-1]);
		
	}
}