package be.celsius.fcrweb.controllers;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.objects.RuleConflictObjects;
import be.celsius.fcrweb.utils.AuthenticationUtils;
import be.celsius.fcrweb.utils.JSONUtils;
import be.celsius.fcrweb.utils.WebServiceUtils;

import java.io.IOException;
import java.util.ArrayList;

public class AdminController implements Controller {
	
	protected final Log logger = LogFactory.getLog(getClass());
	private MySqlAccessor sqlAccessor;
	private WebServiceUtils webServiceUtils;
	private LoginBean loginBean;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{		
		String uid = loginBean.getLogin(request);
		String user_profiles = loginBean.getProfiles(request, sqlAccessor);
		
		if (!(AuthenticationUtils.hasAdministratorProfile(user_profiles)))
		{
			ModelAndView mav = new ModelAndView("jsp/Error.jsp");
			request.setAttribute("error", "You are not allowed to access this page");
			return mav;
		}
		String id = request.getParameter("id");

		if (id == null)
		{
			ModelAndView mav = new ModelAndView("jsp/Admin.jsp");
			request.setAttribute("user_profile", user_profiles);
			request.setAttribute("page", "admin");
			request.setAttribute("uid", uid);
			return mav;
		}
		else
		{
			if (id.equals("1"))
			{
				String technical_service = request.getParameter("service");
				if (technical_service == null)
				{
					String autocomplete_rule = request.getParameter("autocomplete_rule");
					String autocomplete_ts = request.getParameter("autocomplete_ts");
					if (autocomplete_rule == null && autocomplete_ts == null) //DEFAULT PAGE
					{
						ModelAndView mav = new ModelAndView("jsp/link_rule_technical_service.jsp");
						//String rulenames = sqlAccessor.getAllRuleNames();
						//String ts = sqlAccessor.getAllTechnicalServiceNames();
						//request.setAttribute("rulenames", rulenames);
						//request.setAttribute("technical_services", ts);
						request.setAttribute("user_profile", user_profiles);
						request.setAttribute("page", "admin");
						request.setAttribute("uid", uid);
						return mav;
					}
					else if (autocomplete_rule == null) //AUTOCOMPLETE TECHNICAL_SERVICE
					{
						String tsMatchDB = sqlAccessor.getAutocompleteTechnicalServices(autocomplete_ts);
						/*String url = "";
						String auth_user = "";
						String auth_pwd = "";
						String tsMatchWS = webServiceUtils.getAutocompleteTechnicalServices(autocomplete, url, auth_user, auth_pwd);*/
						String responseList = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><listdata>";
						responseList += tsMatchDB;
						/*if (tsMatchDB.equals(""))
						{
							responseList += tsMatchWS;
						}
						else
						{
							responseList += "|"+tsMatchWS;
						}*/
						responseList += "</listdata>";
						response.getWriter().print(responseList);
					}
					else // AUTOCOMPLETE RULE
					{
						String ruleMatchDB = sqlAccessor.getAutocompleteRules(autocomplete_rule);
						
						String responseList = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><listdata>";
						responseList += ruleMatchDB;						
						responseList += "</listdata>";
						response.getWriter().print(responseList);
					}
				}
				else
				{
					technical_service = technical_service.replace("%20", " ");
					String ruleList = request.getParameter("list");
					String rule = request.getParameter("rule");
					String force = request.getParameter("force");
					if (ruleList == null)
					{
						if (rule == null) // FILL MANAGEMENTPANEL WITH RULES LINKED TO THE SELECTED TECHNICAL SERVICE
						{
							String json = JSONUtils.AssignedRulesToJson(sqlAccessor.getSpecificTechnicalServiceRules(technical_service), sqlAccessor.getAllRuleNames());
							response.getWriter().print(json);
						}
						else // ASSIGN SINGLE RULE TO TECHNICAL SERVICE
						{
							if (sqlAccessor.ruleExists(rule) && sqlAccessor.technicalServiceExists(technical_service))
							{
								Boolean force_assign= false;
								if (force.equals("true"))
								{
									force_assign = true;
								}
								ArrayList<RuleConflictObjects> conflicts = sqlAccessor.assignTechnicalServiceToRule(technical_service, rule, force_assign);
								if (conflicts.size() == 0)
								{
									response.getWriter().print("ok");
								}
								else
								{
									String json = JSONUtils.RuleConflictObjectsListToJSON(conflicts);
									response.getWriter().print(json);
								}
							}
							else
							{
								response.getWriter().print("Not Existing");
							}
						}
					}
					else // ASSIGN RULE LIST TO TECHNICAL SERVICE
					{
						Boolean force_assign= false;
						if (force.equals("true"))
						{
							force_assign = true;
						}
						ruleList = ruleList.replace("%20", " ");
						String[] rules = ruleList.split(",");						
						ArrayList<RuleConflictObjects> conflicts = sqlAccessor.updateTechnicalServiceRules(technical_service, rules, force_assign);
						if (conflicts.size() == 0)
						{
							response.getWriter().print("ok");
						}
						else
						{
							String json = JSONUtils.RuleConflictObjectsListToJSON(conflicts);
							response.getWriter().print(json);
						}
					}
				}
			}
			if (id.equals("2"))
			{
				String param_number = request.getParameter("paramnumber");
				
				if (param_number == null)
				{
					ModelAndView mav = new ModelAndView("jsp/manage_application_parameters.jsp");				
					ArrayList<String[]> params = sqlAccessor.getApplicationParameters();
					request.setAttribute("params", params);
					request.setAttribute("user_profile", user_profiles);
					request.setAttribute("page", "admin");
					request.setAttribute("uid", uid);
					return mav;
				}
				else
				{
					int paramN = Integer.parseInt(param_number);
					
					for (int i = 0; i < paramN; i++)
					{
						String paramName = request.getParameter("param"+i+"name").replace("%20", " ");
						String paramValue = request.getParameter("param"+i+"value").replace("%20", " ");
						sqlAccessor.updateApplicationParameter(paramName, paramValue);
					}
				}
			}
			
			if (id.equals("3"))
			{
				String newRanges = request.getParameter("ranges");
				
				if (newRanges == null)
				{
					ModelAndView mav = new ModelAndView("jsp/manage_private_ranges.jsp");
					ArrayList<String[]> ranges = sqlAccessor.getPrivateRanges();
					request.setAttribute("ranges", ranges);
					request.setAttribute("user_profile", user_profiles);
					request.setAttribute("page", "admin");
					request.setAttribute("uid", uid);
					return mav;
				}
				else
				{					
					String[] newRangesSplit = newRanges.split(";");
					sqlAccessor.resetPrivateRanges();
					for (int i = 0; i < newRangesSplit.length; i++)
					{
						String[] singleRange = newRangesSplit[i].split(",");
						sqlAccessor.addPrivateRange(singleRange[0], singleRange[1]);
					}
				}
			}
		}
		
		return null;
	}
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor) 
	{
		this.sqlAccessor = sqlAccessor;
    }
	
	public void setWebServiceUtils(WebServiceUtils webServiceUtils) 
	{
		this.webServiceUtils = webServiceUtils;
    }
	
	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
}
