package be.celsius.fcrweb.controllers;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.objects.FWObjectNamesNotCompliant;
import be.celsius.fcrweb.objects.HostDefinitionProblemList;
import be.celsius.fcrweb.objects.ObjectInconsistencies;
import be.celsius.fcrweb.objects.RequestSummary;
import be.celsius.fcrweb.objects.ServiceInconsistencies;
import be.celsius.fcrweb.objects.TechnicalServiceRevalidationData;
import be.celsius.fcrweb.utils.AuthenticationUtils;
import be.celsius.fcrweb.utils.WebServiceUtils;

import java.io.IOException;
import java.util.ArrayList;

public class ReportsController implements Controller {
	
	protected final Log logger = LogFactory.getLog(getClass());
	private MySqlAccessor sqlAccessor;
	private WebServiceUtils webServiceUtils;
	private LoginBean loginBean;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String uid = loginBean.getLogin(request);
		String user_profiles = loginBean.getProfiles(request, sqlAccessor);
		
		if (!(AuthenticationUtils.hasAdministratorProfile(user_profiles)||AuthenticationUtils.hasSecurityOfficerProfile(user_profiles)))
		{
			ModelAndView mav = new ModelAndView("jsp/Error.jsp");
			request.setAttribute("error", "You are not allowed to access this page");			
			return mav;
		}
		
		String id = request.getParameter("id");
		
		if (id == null)
		{
			ModelAndView mav = new ModelAndView("jsp/Reports.jsp");
			request.setAttribute("user_profile", user_profiles);
			request.setAttribute("page", "reports");
			return mav;
		}
		else
		{
			if (id.equals("1"))
			{
				ModelAndView mav = new ModelAndView("jsp/Orphan_rules.jsp");
				String orphanRules = sqlAccessor.getOrphanRules();
				request.setAttribute("orphan_rules", orphanRules);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "reports");
				return mav;
			}
			else if (id.equals("2"))
			{
				ModelAndView mav = new ModelAndView("jsp/Rules_without_owner.jsp");
				String rulesWithoutOwner = sqlAccessor.getRulesWithoutOwner();
				request.setAttribute("rules_without_owner", rulesWithoutOwner);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "reports");
				return mav;
			}
			else if (id.equals("3"))
			{
				ModelAndView mav = new ModelAndView("jsp/object_names_not_compliant.jsp");
				FWObjectNamesNotCompliant objects_not_compliant = sqlAccessor.getObjectNamesNotCompliant();
				request.setAttribute("objects_not_compliant", objects_not_compliant);
				int objects_total = sqlAccessor.getObjectsNumber();
				int services_total = sqlAccessor.getServicesNumber();
				int profiles_total = sqlAccessor.getProfilesNumber();
				request.setAttribute("objects_total", objects_total);
				request.setAttribute("services_total", services_total);
				request.setAttribute("profiles_total", profiles_total);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "reports");
				return mav;
			}
			else if (id.equals("4"))
			{
				ModelAndView mav = new ModelAndView("jsp/object_definition.jsp");				
				HostDefinitionProblemList host_definition = sqlAccessor.getHostDefinitionProblems();
				request.setAttribute("host_definition", host_definition);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "reports");
				return mav;
			}
			else if (id.equals("5"))
			{
				ModelAndView mav = new ModelAndView("jsp/object_consistency.jsp");				
				ArrayList<ObjectInconsistencies> object_consistency = sqlAccessor.getFWObjectInconsistencies();
				ArrayList<ServiceInconsistencies> service_consistency = sqlAccessor.getFWServiceInconsistencies();
				request.setAttribute("object_consistency", object_consistency);
				request.setAttribute("service_consistency", service_consistency);
				int objects_total = sqlAccessor.getObjectsNumber();
				int services_total = sqlAccessor.getServicesNumber();
				request.setAttribute("objects_total", objects_total);
				request.setAttribute("services_total", services_total);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "reports");
				return mav;
			}
			else if (id.equals("6"))
			{
				ModelAndView mav = new ModelAndView("jsp/fcr_rejected.jsp");				
				ArrayList<RequestSummary> fcr_rejected = sqlAccessor.getRejectedFCR();
				request.setAttribute("fcr_rejected", fcr_rejected);				
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "reports");
				return mav;
			}
			else if (id.equals("7"))
			{
				ModelAndView mav = new ModelAndView("jsp/fcr_per_team.jsp");
				String[][] users = sqlAccessor.getFCRUsers();
				String ws_url = sqlAccessor.getApplicationParameter("ws_bonita");
				ArrayList<String[]> fcr_per_team = webServiceUtils.getFCRPerTeam(users, ws_url);
				request.setAttribute("fcr_per_team", fcr_per_team);				
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "reports");
				return mav;
			}
			else if (id.equals("8"))
			{
				ModelAndView mav = new ModelAndView("jsp/actions_delay.jsp");				
				String actions_delay = sqlAccessor.getActionsDelay();
				request.setAttribute("actions_delay", actions_delay);				
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "reports");
				return mav;
			}
			else if (id.equals("9"))
			{
				ModelAndView mav = new ModelAndView("jsp/technical_service_revalidation.jsp");				
				ArrayList<TechnicalServiceRevalidationData> technical_service_revalidation = sqlAccessor.getTechnicalServiceRevalidationData();
				request.setAttribute("technical_services", technical_service_revalidation);				
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "reports");
				return mav;
			}
		}
		
		return null;
	}
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor) 
	{
		this.sqlAccessor = sqlAccessor;
    }
	
	public void setWebServiceUtils(WebServiceUtils webServiceUtils) 
	{
		this.webServiceUtils = webServiceUtils;
    }

	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
}
