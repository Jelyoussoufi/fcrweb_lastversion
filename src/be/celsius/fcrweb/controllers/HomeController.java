package be.celsius.fcrweb.controllers;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;

import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class HomeController implements Controller {	

	private LoginBean loginBean;
	private MySqlAccessor sqlAccessor;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{		
		String uid = loginBean.getLogin(request);
		String user_profiles = loginBean.getProfiles(request, sqlAccessor);
		ModelAndView mav = new ModelAndView("jsp/home.jsp");
		request.setAttribute("user_profile", user_profiles);
		request.setAttribute("page", "home");
		request.setAttribute("uid", uid);
		return mav;
	}

	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor) 
	{		
		this.sqlAccessor = sqlAccessor;
    }
}
