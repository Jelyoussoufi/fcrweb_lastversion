package be.celsius.fcrweb.controllers;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.objects.Profile;
import be.celsius.fcrweb.objects.ProfileSummary;
import be.celsius.fcrweb.objects.ProfilesPageData;
import be.celsius.fcrweb.utils.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;

public class ProfilesController implements Controller {
	
	protected final Log logger = LogFactory.getLog(getClass());
	private MySqlAccessor sqlAccessor;
	private LoginBean loginBean;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String action = request.getParameter("action");
		String uid = loginBean.getLogin(request);
		String user_profiles = loginBean.getProfiles(request, sqlAccessor);
		String ts = loginBean.getTechnicalServices(sqlAccessor);
		
		
		if (action == null)
		{
			String id = request.getParameter("id");
			if (id == null)
			{
				ModelAndView mav = new ModelAndView("jsp/Profiles.jsp");
				ProfilesPageData profiles = sqlAccessor.getProfiles(ts);
				
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("profiles", profiles);
				request.setAttribute("page", "profiles");
				request.setAttribute("uid", uid);
				return mav;
			}
			else
			{
				ModelAndView mav = new ModelAndView("jsp/profile_view.jsp");
				Profile profile = sqlAccessor.getProfile(id);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("profile", profile);
				request.setAttribute("page", "profiles");
				request.setAttribute("uid", uid);
				return mav;
			}
		}
		else if (action.equals("getSearchResult"))
		{
			String searchValue = request.getParameter("searchValue");
			ArrayList<ProfileSummary> list = sqlAccessor.getSearchResultProfiles(searchValue, ts, user_profiles);
			String json = JSONUtils.profileListToJSON(list);
			response.getWriter().print(json);
		}
		else if (action.equals("setPredefined"))
		{
			String profile_id = request.getParameter("id");
			sqlAccessor.setProfileAsPredefined(profile_id);
		}
		return null;
	}
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor) 
	{
		this.sqlAccessor = sqlAccessor;
    }		

	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
}
