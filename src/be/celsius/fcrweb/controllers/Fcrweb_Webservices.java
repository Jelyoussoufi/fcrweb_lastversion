package be.celsius.fcrweb.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jgroups.GetStateEvent;
import org.w3c.dom.Element;

import be.celsius.fcrweb.sql.FcrWebCatalogAccess;
import be.celsius.util.Config;
import be.celsius.util.SimpleDomParser;
import be.celsius.util.UtilLog;
import be.celsius.util.UtilSoap;
import be.celsius.util.UtilStr;
import be.celsius.util.datastructure.Ulist;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
//VERY IMPORTANT.  SOME OF THESE EXIST IN MORE THAN ONE PACKAGE!
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

public class Fcrweb_Webservices 
{
	private FcrWebCatalogAccess configs;
	private UtilSoap soap;
	/** use map for debug purpose **/
	private Map<String,Object> map;	
	
	public Fcrweb_Webservices()
	{
		configs = new FcrWebCatalogAccess();
		soap 	= new UtilSoap();
		soap.setUseLogs(true);
		map		= new HashMap<String,Object>();
	}
	
	private String addCertificatesInTrustStore()
	{
		String message 	= "";
		String NL 		= " \n";
		
		String cers_path = configs.getApplicationConfigs().get("mobi_ws_certificates");
		
		try
		{
			message += "Adding certificate in truststore ..." + NL;
			message += "current dir:" + new File(".").getAbsolutePath() + NL;
			KeyStore trustStore  = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null);//Make an empty store
			InputStream fis	= null;
			try{
				fis = new FileInputStream(new File(cers_path + "/Mobistar_server_ca.cer"));				
			} catch(Exception e) {
				message += UtilLog.getStackTrace(e) + NL;
			}
			BufferedInputStream bis = new BufferedInputStream(fis);
			CertificateFactory cf 	= CertificateFactory.getInstance("X.509");
			while (bis.available() > 0) {
			    Certificate cert = cf.generateCertificate(bis);
			    trustStore.setCertificateEntry("Mobistar_server_ca", cert);
			    message += "add a server certificate;" + NL;
			}
			try{
				fis = new FileInputStream(new File(cers_path + "/Mobistar_root_CA.cer"));				
			} catch(Exception e) {
				message += "current dir:" + new File(".").getAbsolutePath() + NL + UtilLog.getStackTrace(e) + NL;
			}
			bis = new BufferedInputStream(fis);
			while (bis.available() > 0) {
			    Certificate cert = cf.generateCertificate(bis);
			    trustStore.setCertificateEntry("Mobistar_root_CA", cert);
			    message += "add a root certificate;" + NL;
			}
			try{
				fis = new FileInputStream(new File(cers_path + "/chain.p7b"));				
			} catch(Exception e) {
				message += "current dir:" + new File(".").getAbsolutePath() + NL + UtilLog.getStackTrace(e) + NL;
			}
			bis = new BufferedInputStream(fis);
			while (bis.available() > 0) {
			    Certificate cert = cf.generateCertificate(bis);
			    trustStore.setCertificateEntry("chain", cert);
			    message += "add a p7b certificate;" + NL;
			}
			message += "certificate successfully added in truststore" + NL;
		}
		catch (Exception e)
		{
			message += UtilLog.getStackTrace(e) + NL;
		}
		return message;
	}
	
	public Map get_MRT_projects()
	{	
		map.clear();
		soap.clearLogs();
		Ulist projects 	= new Ulist();
		String url		= configs.getApplicationConfigs().get("ws_mrt"); 
		String request 	= "<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:mrt='https://opmweb.acc.mobistar.be/webservices/MRT'>"
						+ "<soapenv:Header/>"
						+ "<soapenv:Body>"
						+ "<mrt:getLightProjects soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'/>"
						+ "</soapenv:Body>"
						+ "</soapenv:Envelope>"
						;
		
		//map.put("truststore_mngmnt", addCertificatesInTrustStore());
		String response = soap.extractResponse(soap.submitRequest(url, request, true, false));
		String error 	= getErrorString(response);
		if (UtilStr.isEmpty(error))
		{
			try
			{
				SimpleDomParser dom = new SimpleDomParser(response);
				Element res			= (Element)dom.getNode(null, "projectList");
				List<Element> items = dom.getNodesByE(res, "item");
				for (Element item : items)
					projects.push(Config.atCelsius ? "MRT_" + dom.getValue(item, "name") : dom.getValue(item, "name"));		
			}catch (Exception e){
				map.put("MRT_parse_exception", UtilLog.getStackTrace(e));
			}
		}
		else
		{
			projects.push("MRT_ERROR - " + error);
		}
		
for (String key : soap.getLogs().keySet())map.put("MRT_" + key, soap.getLogs().get(key));
soap.clearLogs();
		
		map.put("list", projects);
		
		return map;
	} 
	
	
	public Map<String,Object> get_RAA_Supplier_list()
	{
		map.clear();
		soap.clearLogs();
		
		Ulist list 				= new Ulist();
		String url 				= configs.getApplicationConfigs().get("ws_raa");
		String request 			= "<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:raa='https://websec.ux.mobistar.be/sectools/webservices/RAA_WS'>"
								+ "<soapenv:Header/>"
								+ "<soapenv:Body>"
								+ "<raa:getActiveRAAList soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'/>"
								+ "</soapenv:Body>"
								+ "</soapenv:Envelope>"
								;
		
		//map.put("truststore_mngmnt", addCertificatesInTrustStore());
		String response = soap.extractResponse(soap.submitRequest(url, request, true, false));
		String error 	= getErrorString(response);
		if (UtilStr.isEmpty(error))
		{
			try
			{
				SimpleDomParser dom = new SimpleDomParser(response);
				Element res			= (Element)dom.getNode(null, "return");
				List<Element> items = dom.getNodesByE(res, "item");
				for (Element item : items)
					list.push(Config.atCelsius ? "ACT_" + dom.getValue(item, "supplier_name") : dom.getValue(item, "supplier_name"));		
			}catch (Exception e){
				map.put("ACT_parse_exception", UtilLog.getStackTrace(e));
			}
		}
		else
		{
			list.push("ACT_ERROR - " + error);
		}
		
for (String key : soap.getLogs().keySet())map.put("ACT_" + key, soap.getLogs().get(key));
soap.clearLogs();

		request		= "<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:raa='https://websec.ux.mobistar.be/sectools/webservices/RAA_WS'>"
					+ "<soapenv:Header/>"
					+ "<soapenv:Body>"
					+ "<raa:getSupplierList soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'/>"
					+ "</soapenv:Body>"
					+ "</soapenv:Envelope>"
					;
		response 	= soap.extractResponse(soap.submitRequest(url, request, true, true));
		error 		= getErrorString(response);
		if (UtilStr.isEmpty(error))
		{
			try
			{
				SimpleDomParser dom = new SimpleDomParser(response);
				Element res			= (Element)dom.getNode(null, "return");
				List<Element> items = dom.getNodesByE(res, "item");
				for (Element item : items)
					list.push(Config.atCelsius ? "SP_" + dom.getValue(item, "name") : dom.getValue(item, "name"));		
			}catch (Exception e){
				map.put("SP_parse_exception", UtilLog.getStackTrace(e));
			}
		}
		else
		{
			list.push("SP_ERROR - " + error);
		}
		
for (String key : soap.getLogs().keySet())map.put("SP_" + key, soap.getLogs().get(key));
soap.clearLogs();

		map.put("list", list);
		
		return map;
	}
	
	
	
	public String create_user_group_in_myriam_ad_ldap(String group, String owner,String description)
	{
		String url 		= configs.getApplicationConfigs().get("ws_user_group_myriam_ad_ldap");		
		String request 	= ""
						+ "<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:new='https://websec.ux.mobistar.be/sectools/webservices/NEW_FW_AUTH'>"
						+ "<soapenv:Header/>"
						+ "<soapenv:Body>"
						+ "<new:newFWAuthResource soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
						+ "<fw_auth_group_name xsi:type='xsd:string'>" + group + "</fw_auth_group_name>"
						+ "<owner xsi:type='xsd:string'>" + owner + "</owner>"
						+ "</new:newFWAuthResource>"
						+ "</soapenv:Body>"
						+ "</soapenv:Envelope>"
						/*
						+ "<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:cyb='http://eland.ux.mobistar.be/sectools/webservices/FCRWEB_WS'>"
						+ "<soapenv:Header/>"
						+ "<soapenv:Body>"
						+ "<fcr:newFWGroup  soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>"
						+ "<group_name xsi:type='xsd:string'>" + group + "</safe_name>"
						+ "<description xsi:type='xsd:string'>" + description + "</description>"
						+ "<owner xsi:type='xsd:string'>uid=" + owner + ",ou=staff,o=mobistar.be </owner>"
						+ "<access_group_DN xsi:type='xsd:string'>cn=GF_FW_" + group.replace(" ", "_") + ",ou=FW Groups,ou=Groups,ou=staff,o=mobistar.be</access_group_DN>"
						+ "</fcr:newFWGroup>"
						+ "</soapenv:Body>"
						+ "</soapenv:Envelope>"
						/* */
						;

		String response = soap.extractResponse(soap.submitRequest(url, request, true, true));

		if (UtilStr.isEmpty(getErrorString(response)))
		{
			try
			{
				SimpleDomParser dom = new SimpleDomParser(response);
				Element res			= (Element)dom.getNode(null, "return");
				return dom.getValue(res, "value");
			}catch (Exception e){}
		}
		return "-9";
	}
	
	public String getErrorString(String msg)
	{
		try
		{
			if (UtilStr.hasX(msg, "DOCTYPE") | UtilStr.hasX(msg, "<html>"))
				return msg.split("<title>")[1].split("</title>")[0];
			else
				return "";
		}
		catch (Exception e){}
		return "";
	}
}
