package be.celsius.fcrweb.controllers;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.objects.ChangeLogRuleSummary;
import be.celsius.fcrweb.objects.Rule;
import be.celsius.fcrweb.objects.RuleObject;
import be.celsius.fcrweb.objects.RuleService;
import be.celsius.fcrweb.objects.RuleProfile;
import be.celsius.fcrweb.objects.RuleSummary;
import be.celsius.fcrweb.objects.RulesPageData;
import be.celsius.fcrweb.utils.JSONUtils;
import be.celsius.util.UtilStr;

import java.io.IOException;
import java.util.ArrayList;

public class RulesController implements Controller {
	
	protected final Log logger = LogFactory.getLog(getClass());
	private MySqlAccessor sqlAccessor;
	private RulesPageData rules;
	private LoginBean loginBean;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String action = request.getParameter("action");

		if (action == null)
		{
			String uid = loginBean.getLogin(request);
			String user_profiles = loginBean.getProfiles(request, sqlAccessor);
			String ts = loginBean.getTechnicalServices(sqlAccessor);
			
			String object_id = request.getParameter("object_id");
			String service_id = request.getParameter("service_id");
			String profile_name = request.getParameter("profile_name");
			String id = request.getParameter("id");
			if (object_id == null && service_id == null && id == null && profile_name == null) //Display rules.jsp page
			{
				rules = sqlAccessor.getRules(ts);
				ModelAndView mav = new ModelAndView("jsp/Rules.jsp");			
				request.setAttribute("rules", rules);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("page", "rules");
				return mav;
			}
			else if (service_id == null && id == null && profile_name == null) //OBJECT ID FOUND
			{				
				ModelAndView mav = new ModelAndView("jsp/rule_object_view.jsp");
				RuleObject object = sqlAccessor.getRuleObject(object_id);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("object", object);
				request.setAttribute("page", "rules");
				return mav;
			}
			else if (object_id == null && id == null && profile_name == null) //SERVICE ID FOUND
			{
				ModelAndView mav = new ModelAndView("jsp/rule_service_view.jsp");
				RuleService service = sqlAccessor.getRuleService(service_id);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("service", service);
				request.setAttribute("page", "rules");
				return mav;
			}			
			else if (id == null) //PROFILE NAME FOUND
			{
				ModelAndView mav = new ModelAndView("jsp/rule_profile_view.jsp");
				RuleProfile profile = sqlAccessor.getRuleProfile(profile_name);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("profile", profile);
				request.setAttribute("page", "rules");
				return mav;
			}
			else if (object_id == null && service_id == null) //RULE ID FOUND
			{
				ModelAndView mav = new ModelAndView("jsp/rule_view.jsp");
				Rule rule = sqlAccessor.getRule(id, ts);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("rule", rule);
				request.setAttribute("page", "rules");
				return mav;
			}
		}
		else if (action.equals("getRevalidationRules"))
		{
			String login = request.getParameter("login");
			String technical_service = UtilStr.undoFilter(request.getParameter("ts"));
			ArrayList<RuleSummary> list = sqlAccessor.getUserRules(login, technical_service);
			String json = JSONUtils.ruleListToJSON(list);
			response.getWriter().print(json);
		}
		else if (action.equals("getSearchResult"))
		{			
			String uid = loginBean.getLogin(request);
			String searchValue = request.getParameter("searchValue");
			String ts = loginBean.getTechnicalServices(sqlAccessor);
			ArrayList<RuleSummary> list = sqlAccessor.getSearchResultRules(searchValue, ts);			
			String json = JSONUtils.ruleListToJSON(list);
			response.getWriter().print(json);
		}
		else if (action.equals("getChangeLog"))
		{
			String uid = loginBean.getLogin(request);
			String ruleName = request.getParameter("ruleName").replace("-S-", " ");
			ArrayList<ChangeLogRuleSummary> list = sqlAccessor.getRuleChangeLog(ruleName);			
			String json = JSONUtils.ruleChangeLogListToJSON(list);
			response.getWriter().print(json);
		}
		
		return null;
	}
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor) 
	{
		this.sqlAccessor = sqlAccessor;
    }

	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
}
