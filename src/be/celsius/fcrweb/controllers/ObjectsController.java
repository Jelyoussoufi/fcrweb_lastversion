package be.celsius.fcrweb.controllers;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.objects.ObjectSummary;
import be.celsius.fcrweb.objects.ObjectsPageData;
import be.celsius.fcrweb.utils.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;

public class ObjectsController implements Controller {
	
	protected final Log logger = LogFactory.getLog(getClass());
	private MySqlAccessor sqlAccessor;
	private LoginBean loginBean;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String action = request.getParameter("action");
		String uid = loginBean.getLogin(request);
		String user_profiles = loginBean.getProfiles(request, sqlAccessor);
		String ts = loginBean.getTechnicalServices(sqlAccessor);
		
		
		if (action == null)
		{
			String id = request.getParameter("id");
			String type = request.getParameter("type");
			if (id == null)
			{
				ModelAndView mav = new ModelAndView("jsp/Objects.jsp");
				ObjectsPageData objects = sqlAccessor.getObjects(ts);
				
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("objects", objects);
				request.setAttribute("page", "objects");
				request.setAttribute("uid", uid);
				return mav;
			}
			else
			{
				ModelAndView mav = new ModelAndView("jsp/object_view.jsp");
				be.celsius.fcrweb.objects.Object object = sqlAccessor.getObject(id, type);
				request.setAttribute("user_profile", user_profiles);
				request.setAttribute("object", object);
				request.setAttribute("page", "objects");
				request.setAttribute("uid", uid);
				return mav;
			}
		}
		else if (action.equals("getSearchResult"))
		{
			String searchValue = request.getParameter("searchValue");
			ArrayList<ObjectSummary> list = sqlAccessor.getSearchResultObjects(searchValue, ts, user_profiles);
			String json = JSONUtils.objectListToJSON(list);
			response.getWriter().print(json);
		}
		else if (action.equals("setPredefined"))
		{
			String object_id = request.getParameter("id");
			Boolean ok = sqlAccessor.setObjectGroupAsPredefined(object_id);
			if (ok)
			{
				response.getWriter().print("ok");
			}
			else
			{
				response.getWriter().print("ko");
			}
		}
		return null;
	}
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor) 
	{
		this.sqlAccessor = sqlAccessor;
    }

	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
}
