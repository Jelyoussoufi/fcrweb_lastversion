package be.celsius.fcrweb.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import be.celsius.fcrweb.beans.LoginBean;
import be.celsius.fcrweb.dataAccess.MySqlAccessor;
import be.celsius.fcrweb.objects.ObjectSummary;
import be.celsius.fcrweb.utils.WebServiceUtils;
import be.celsius.util.UtilStr;

public class PreapprovedController implements Controller {
	
	protected final Log logger = LogFactory.getLog(getClass());
	private MySqlAccessor sqlAccessor;
	private WebServiceUtils webServiceUtils;
	private LoginBean loginBean;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String uid = loginBean.getLogin(request);
		String user_profiles = loginBean.getProfiles(request, sqlAccessor);
		
		int[] allowed_ids = sqlAccessor.getPreapprovedAllowedIds(user_profiles);
		
		String id = request.getParameter("id");
		if (id == null)
		{
			ModelAndView mav = new ModelAndView("jsp/preapproved.jsp");
			ArrayList<ObjectSummary> preapproved = sqlAccessor.getPreapprovedObjects(allowed_ids);
			request.setAttribute("preapproved", preapproved);
			request.setAttribute("user_profile", user_profiles);
			request.setAttribute("page", "preapproved");
			return mav;
		}
		else
		{
			Boolean modification_allowed = false;
			for (int i = 0; i < allowed_ids.length; i++)
			{
				if (allowed_ids[i] == Integer.parseInt(id))
				{
					modification_allowed = true;
				}
			}
			if (modification_allowed == false)
			{
				request.setAttribute("error", "You are not allowed to modify this preapproved object");
				ModelAndView mav = new ModelAndView("jsp/Error.jsp");
				return mav;
			}
			
			String obj_ids = sqlAccessor.getPreapprovedObjectIds(id);			
			if ( obj_ids == null || obj_ids.equals("none"))
			{
				request.setAttribute("error", "No Objects linked to the group selected");
				ModelAndView mav = new ModelAndView("jsp/Error.jsp");
				return mav;
			}
			String technical_service = sqlAccessor.getPreapprovedObjectechnicalService(obj_ids.split(",")[0]);
			
			response.sendRedirect("forms.htm?pr_form_mode=maintain-group-objects-preapproved&pr_maintain_ids="+obj_ids+"&pr_ts_name="+UtilStr.doFilter(technical_service));
			
		}
		
		return null;
	}
	
	public void setSqlAccessor(MySqlAccessor sqlAccessor) 
	{		
		this.sqlAccessor = sqlAccessor;
    }
	
	public void setWebServiceUtils(WebServiceUtils webServiceUtils) 
	{
		this.webServiceUtils = webServiceUtils;
    }

	public void setLoginBean(LoginBean lb) 
	{
		this.loginBean = lb;
    }
}
