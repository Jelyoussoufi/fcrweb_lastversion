package be.celsius.fcrweb.application;

public class ApplicationLoadAction
{
	private boolean isMonitoring;
	
	public ApplicationLoadAction()
	{
		isMonitoring = false;
	}
	
	public void startMonitoring()
	{
		if (!isMonitoring)
		{
			isMonitoring = true;
		}
	}
}
