package be.celsius.fcrweb.application;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

/**
 * @author jhuilian
 * @category Util
 */
public class ApplicationContextLoader implements ApplicationContextAware
{
	/** @category Property */
	private static ApplicationContext applicationContext = null;
	
	/** @category Accessor */
	public static ApplicationContext getApplicationContext()
	{
        return applicationContext;
    }

	/** @category Accessor */
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException 
	{
		this.applicationContext = applicationContext;
	}
}
