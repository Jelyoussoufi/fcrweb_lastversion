package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class Service 
{
	double id;
	String name;
	String type;
	String usedInGroups;
	String usedInRules;
	Boolean predefined;
	ArrayList<String[]> otherParameters;
	
	public Service()
	{
		otherParameters = new ArrayList<String[]>();
		predefined = false;
	}

	public double getId() {
		return id;
	}

	public void setId(double id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}		

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUsedInGroups() {
		return usedInGroups;
	}

	public void setUsedInGroups(String usedInGroups) {
		this.usedInGroups = usedInGroups;
	}
	
	public String getUsedInRules() {
		return usedInRules;
	}

	public void setUsedInRules(String usedInRules) {
		this.usedInRules = usedInRules;
	}
	

	public ArrayList<String[]> getOtherParameters() {
		return otherParameters;
	}

	public void setOtherParameters(ArrayList<String[]> otherParameters) {
		this.otherParameters = otherParameters;
	}
	
	public Boolean isPredefined()
	{
		return this.predefined;
	}
	
	public void setPredefined(Boolean predef)
	{
		this.predefined = predef;
	}

}
