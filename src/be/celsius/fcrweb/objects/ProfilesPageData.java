package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class ProfilesPageData 
{
	ArrayList<ProfileSummary> myProfiles;
	ArrayList<ProfileSummary> PredefinedProfiles;
	
	public ProfilesPageData()
	{
		myProfiles = new ArrayList<ProfileSummary>();
		PredefinedProfiles = new ArrayList<ProfileSummary>();
	}

	public ArrayList<ProfileSummary> getMyProfiles() {
		return myProfiles;
	}

	public void setMyProfiles(ArrayList<ProfileSummary> myProfiles) {
		this.myProfiles = myProfiles;
	}

	public ArrayList<ProfileSummary> getPredefinedProfiles() {
		return PredefinedProfiles;
	}

	public void setPredefinedProfiles(ArrayList<ProfileSummary> predefinedProfiles) {
		PredefinedProfiles = predefinedProfiles;
	}

		
}
