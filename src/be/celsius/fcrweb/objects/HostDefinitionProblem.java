package be.celsius.fcrweb.objects;

public class HostDefinitionProblem 
{
	Long id;
	String name;
	String ip;
	
	public HostDefinitionProblem()
	{
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}		

}
