package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class HostDefinitionProblemList 
{
	private ArrayList<HostDefinitionProblem> notExistingCMDB;
	private ArrayList<HostDefinitionProblem> notExistingFW;
	
	public HostDefinitionProblemList()
	{
		notExistingCMDB = new ArrayList<HostDefinitionProblem>();
		notExistingFW = new ArrayList<HostDefinitionProblem>();
	}

	public ArrayList<HostDefinitionProblem> getNotExistingCMDB() {
		return notExistingCMDB;
	}

	public void setNotExistingCMDB(ArrayList<HostDefinitionProblem> notExistingCMDB) {
		this.notExistingCMDB = notExistingCMDB;
	}

	public ArrayList<HostDefinitionProblem> getNotExistingFW() {
		return notExistingFW;
	}

	public void setNotExistingFW(ArrayList<HostDefinitionProblem> notExistingFW) {
		this.notExistingFW = notExistingFW;
	}

}
