package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class ServicesPageData 
{
	ArrayList<ServiceSummary> myServices;
	ArrayList<ServiceSummary> PredefinedServices;
	ArrayList<ServiceGroupSummary> myServiceGroups;
	ArrayList<ServiceGroupSummary> PredefinedServiceGroups;
	
	public ServicesPageData()
	{
		
	}

	public ArrayList<ServiceGroupSummary> getMyServiceGroups() {
		return myServiceGroups;
	}

	public void setMyServiceGroups(ArrayList<ServiceGroupSummary> myServiceGroups) {
		this.myServiceGroups = myServiceGroups;
	}

	public ArrayList<ServiceGroupSummary> getPredefinedServiceGroups() {
		return PredefinedServiceGroups;
	}

	public void setPredefinedServiceGroups(
			ArrayList<ServiceGroupSummary> predefinedServiceGroups) {
		PredefinedServiceGroups = predefinedServiceGroups;
	}

	public ArrayList<ServiceSummary> getMyServices() {
		return myServices;
	}

	public void setMyServices(ArrayList<ServiceSummary> myServices) {
		this.myServices = myServices;
	}

	public ArrayList<ServiceSummary> getPredefinedServices() {
		return PredefinedServices;
	}

	public void setPredefinedServices(ArrayList<ServiceSummary> genericServices) {
		PredefinedServices = genericServices;
	}

}
