package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class ObjectInconsistencies 
{
	String objectName;
	ArrayList<ObjectInconsistency> inconsistencies;
	
	public ObjectInconsistencies()
	{
		inconsistencies = new ArrayList<ObjectInconsistency>();
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public ArrayList<ObjectInconsistency> getInconsistencies() {
		return inconsistencies;
	}

	public void setInconsistencies(ArrayList<ObjectInconsistency> inconsistencies) {
		this.inconsistencies = inconsistencies;
	}

}
