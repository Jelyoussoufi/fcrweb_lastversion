package be.celsius.fcrweb.objects;

public class RuleProfile 
{
	String name;
	String usedInRules;
	
	public RuleProfile()
	{
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}		

	public String getUsedInRules() {
		return usedInRules;
	}

	public void setUsedInRules(String usedInRules) {
		this.usedInRules = usedInRules;
	}
	
}
