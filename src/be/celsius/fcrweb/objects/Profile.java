package be.celsius.fcrweb.objects;

public class Profile 
{
	double id;
	String name;
	String description;
	String responsibleUid;
	String usedInRules;
	Boolean predefined;
	
	public Profile()
	{
		predefined = false;
	}

	public double getId() {
		return id;
	}

	public void setId(double id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResponsibleUid() {
		return responsibleUid;
	}

	public void setResponsibleUid(String responsibleUid) {
		this.responsibleUid = responsibleUid;
	}

	public void setName(String name) {
		this.name = name;
	}		

	public String getUsedInRules() {
		return usedInRules;
	}

	public void setUsedInRules(String usedInRules) {
		this.usedInRules = usedInRules;
	}
	
	public Boolean isPredefined()
	{
		return this.predefined;
	}
	
	public void setPredefined(Boolean predef)
	{
		this.predefined = predef;
	}	
}
