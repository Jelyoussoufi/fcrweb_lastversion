package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class TechnicalServiceRevalidationData 
{
	private String technicalService;
	private String lastRevalidation;	
	private String status;
	private String revalidationMonth;
	private String owner;
	
	public TechnicalServiceRevalidationData()
	{
		
	}

	public String getTechnicalService() {
		return technicalService;
	}

	public void setTechnicalService(String technicalService) {
		this.technicalService = technicalService;
	}

	public String getLastRevalidation() {
		return lastRevalidation;
	}

	public void setLastRevalidation(String lastRevalidation) {
		this.lastRevalidation = lastRevalidation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRevalidationMonth() {
		return revalidationMonth;
	}

	public void setRevalidationMonth(String revalidationMonth) {
		this.revalidationMonth = revalidationMonth;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	
}
