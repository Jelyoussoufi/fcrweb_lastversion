package be.celsius.fcrweb.objects;

public class RuleObjectSummary 
{	
	int id;	
	String name;
	String type;
	String predefined;
	String profile;
	
	public RuleObjectSummary()
	{}	

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	public String getPredefined() {
		return predefined;
	}

	public void setPredefined(String predefined) {
		this.predefined = predefined;
	}

	public String toJson()
	{		
		String json = "{";
		json += "\"id\": \"" +id + "\"";
		json += ",\"name\": \"" +name + "\"";
		json += ",\"type\": \"" +type + "\"";
		json += ",\"predefined\": \"" +predefined + "\"";
		json += ",\"profile\": \"" +profile + "\"";
		json += "}";
		return json;
	}
	
	
}
