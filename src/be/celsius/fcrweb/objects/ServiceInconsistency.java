package be.celsius.fcrweb.objects;


public class ServiceInconsistency 
{
	int id;
	String type;
	String ports;
	String firewalls;
	
	public ServiceInconsistency()
	{
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPorts() {
		return ports;
	}

	public void setPorts(String ports) {
		this.ports = ports;
	}

	public String getFirewalls() {
		return firewalls;
	}

	public void setFirewalls(String firewalls) {
		this.firewalls = firewalls;
	}

}
