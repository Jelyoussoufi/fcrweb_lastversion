package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class RuleSummary 
{	
	int id;
	String name;
	ArrayList<RuleObjectSummary> source;
	ArrayList<RuleObjectSummary> destination;
	ArrayList<RuleObjectSummary> service;
	String action;
	String nr;
	String technicalService;
	Boolean active;
	String last_used;
	
	public RuleSummary()
	{
		source = new ArrayList<RuleObjectSummary>();
		destination = new ArrayList<RuleObjectSummary>();
		service = new ArrayList<RuleObjectSummary>();
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<RuleObjectSummary> getSource()
	{
		return source;
	}
	
	public ArrayList<RuleObjectSummary> getDestination()
	{
		return destination;
	}
	
	public ArrayList<RuleObjectSummary> getService()
	{
		return service;
	}
	
	public void addSource(RuleObjectSummary obj)
	{
		source.add(obj);
	}
	
	public void addDestination(RuleObjectSummary obj)
	{
		destination.add(obj);
	}
	
	public void addService(RuleObjectSummary obj)
	{
		service.add(obj);
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public String getNr() {
		return nr;
	}

	public void setNr(String nr) {
		this.nr = nr;
	}	
	
	public String getTechnicalService() {
		return technicalService;
	}

	public void setTechnicalService(String technicalService) {
		this.technicalService = technicalService;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getLast_used() {
		return last_used;
	}

	public void setLast_used(String last_used) {
		this.last_used = last_used;
	}

	public String toJson()
	{		
		String json = "{";
		json += "\"id\": \"" +id + "\"";
		json += ",\"name\": \"" +name + "\"";		
		String srcList = "[";
		for (int i = 0; i < source.size(); i ++)
		{
			if (i > 0)
			{
				srcList += ",";
			}
			srcList += source.get(i).toJson();
		}
		srcList += "]";		
		json += ",\"source\": " +srcList;
		
		String dstList = "[";
		for (int i = 0; i < destination.size(); i ++)
		{
			if (i > 0)
			{
				dstList += ",";
			}
			dstList += destination.get(i).toJson();
		}
		dstList += "]";		
		json += ",\"destination\": " +dstList;
		
		String svcList = "[";
		for (int i = 0; i < service.size(); i ++)
		{
			if (i > 0)
			{
				svcList += ",";
			}
			svcList += service.get(i).toJson();
		}
		svcList += "]";		
		json += ",\"service\": " +svcList;		
		json += ",\"action\": \"" +action + "\"";	
		json += ",\"nr\": \"" +nr + "\"";
		json += ",\"technicalService\": \"" +technicalService + "\"";
		json += ",\"active\": \"" +active + "\"";
		json += ",\"last_used\": \"" +last_used + "\"";
		json += "}";
		return json;
	}
	
	
}
