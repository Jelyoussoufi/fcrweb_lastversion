package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class RulesPageData 
{
	ArrayList<RuleSummary> myRules;
	ArrayList<RuleSummary> GenericRules;
	
	public RulesPageData()
	{
		
	}

	public ArrayList<RuleSummary> getMyRules() 
	{
		return myRules;
	}

	public void setMyRules(ArrayList<RuleSummary> myRules) 
	{
		this.myRules = myRules;
	}

	public ArrayList<RuleSummary> getGenericRules() 
	{
		return GenericRules;
	}

	public void setGenericRules(ArrayList<RuleSummary> genericRules) 
	{
		GenericRules = genericRules;
	}
	
		
}
