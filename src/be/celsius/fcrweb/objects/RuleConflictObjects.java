package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class RuleConflictObjects 
{	
	String ruleName;
	// The list conflictObjects is constructed like following:
	// conflictObjects[0] = object id
	// conflictObjects[1] = object name
	// conflictObjects[2] = object type
	// conflictObjects[3] = technical service to which object is assigned
	ArrayList<String[]> conflictObjects;
	
	public RuleConflictObjects()
	{
		conflictObjects = new ArrayList<String[]>();
	}

	public String getRuleName() 
	{
		return ruleName;
	}

	public void setRuleName(String ruleName) 
	{
		this.ruleName = ruleName;
	}

	public ArrayList<String[]> getConflictObjects() 
	{
		return conflictObjects;
	}

	public void setConflictObjects(ArrayList<String[]> conflictObjects) 
	{
		this.conflictObjects = conflictObjects;
	}
	
	public void addConflictObject(String[] conflictObject) 
	{
		conflictObjects.add(conflictObject);
	}
	
	public String toJson()
	{		
		String json = "{";
		json += "\"ruleName\": \"" +ruleName + "\"";
		String conflictObjectsList = "[";
		Boolean first = true;
		for (int i = 0; i < conflictObjects.size(); i ++)
		{
			String[] temp = conflictObjects.get(i);
			String conflictObj = "{\"id\": \""+temp[0]+"\", \"name\": \""+temp[1]+"\", \"type\": \""+temp[2]+"\", \"technicalService\": \""+temp[3]+"\"}";
			
			if (first)
			{				
				conflictObjectsList += conflictObj;
				first = false;
			}
			else
			{
				conflictObjectsList += "," + conflictObj;
			}
		}
		conflictObjectsList += "]";
		json += ",\"conflictObjects\": " +conflictObjectsList;
		json += "}";
		return json;
	}
}
