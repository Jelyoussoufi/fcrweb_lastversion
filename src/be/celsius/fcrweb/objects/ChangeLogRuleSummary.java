package be.celsius.fcrweb.objects;

public class ChangeLogRuleSummary
{		
	int id;
	String date;
	String change;
	String src_existing;
	String src_new;
	String src_removed;
	String dst_existing;
	String dst_new;
	String dst_removed;
	String svc_existing;
	String svc_new;
	String svc_removed;
	String expanded_src_existing;
	String expanded_src_new;
	String expanded_src_removed;
	String expanded_dst_existing;
	String expanded_dst_new;
	String expanded_dst_removed;
	String expanded_svc_existing;
	String expanded_svc_new;
	String expanded_svc_removed;
	String action;
	String nr;
	
	public ChangeLogRuleSummary()
	{}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNr() {
		return nr;
	}

	public void setNr(String nr) {
		this.nr = nr;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getChange() {
		return change;
	}

	public void setChange(String change) {
		this.change = change;
	}
	
	public String getSrc_existing() {
		return src_existing;
	}

	public void setSrc_existing(String src_existing) {
		this.src_existing = src_existing;
	}

	public String getSrc_new() {
		return src_new;
	}

	public void setSrc_new(String src_new) {
		this.src_new = src_new;
	}

	public String getSrc_removed() {
		return src_removed;
	}

	public void setSrc_removed(String src_removed) {
		this.src_removed = src_removed;
	}

	public String getDst_existing() {
		return dst_existing;
	}

	public void setDst_existing(String dst_existing) {
		this.dst_existing = dst_existing;
	}

	public String getDst_new() {
		return dst_new;
	}

	public void setDst_new(String dst_new) {
		this.dst_new = dst_new;
	}

	public String getDst_removed() {
		return dst_removed;
	}

	public void setDst_removed(String dst_removed) {
		this.dst_removed = dst_removed;
	}

	public String getSvc_existing() {
		return svc_existing;
	}

	public void setSvc_existing(String svc_existing) {
		this.svc_existing = svc_existing;
	}

	public String getSvc_new() {
		return svc_new;
	}

	public void setSvc_new(String svc_new) {
		this.svc_new = svc_new;
	}

	public String getSvc_removed() {
		return svc_removed;
	}

	public void setSvc_removed(String svc_removed) {
		this.svc_removed = svc_removed;
	}

	public String getExpanded_src_existing() {
		return expanded_src_existing;
	}

	public void setExpanded_src_existing(String expanded_src_existing) {
		this.expanded_src_existing = expanded_src_existing;
	}

	public String getExpanded_src_new() {
		return expanded_src_new;
	}

	public void setExpanded_src_new(String expanded_src_new) {
		this.expanded_src_new = expanded_src_new;
	}

	public String getExpanded_src_removed() {
		return expanded_src_removed;
	}

	public void setExpanded_src_removed(String expanded_src_removed) {
		this.expanded_src_removed = expanded_src_removed;
	}

	public String getExpanded_dst_existing() {
		return expanded_dst_existing;
	}

	public void setExpanded_dst_existing(String expanded_dst_existing) {
		this.expanded_dst_existing = expanded_dst_existing;
	}

	public String getExpanded_dst_new() {
		return expanded_dst_new;
	}

	public void setExpanded_dst_new(String expanded_dst_new) {
		this.expanded_dst_new = expanded_dst_new;
	}

	public String getExpanded_dst_removed() {
		return expanded_dst_removed;
	}

	public void setExpanded_dst_removed(String expanded_dst_removed) {
		this.expanded_dst_removed = expanded_dst_removed;
	}

	public String getExpanded_svc_existing() {
		return expanded_svc_existing;
	}

	public void setExpanded_svc_existing(String expanded_svc_existing) {
		this.expanded_svc_existing = expanded_svc_existing;
	}

	public String getExpanded_svc_new() {
		return expanded_svc_new;
	}

	public void setExpanded_svc_new(String expanded_svc_new) {
		this.expanded_svc_new = expanded_svc_new;
	}

	public String getExpanded_svc_removed() {
		return expanded_svc_removed;
	}

	public void setExpanded_svc_removed(String expanded_svc_removed) {
		this.expanded_svc_removed = expanded_svc_removed;
	}

	public String toJson()
	{
		String json = "{";
		json += "\"date\": \"" +date + "\"";
		json += ",\"change\": \"" +change + "\"";
		json += ",\"action\": \"" +action + "\"";
		json += ",\"nr\": \"" +nr + "\"";
		json += ",\"src_existing\": \"" +src_existing + "\"";
		json += ",\"src_new\": \"" +src_new + "\"";
		json += ",\"src_removed\": \"" +src_removed + "\"";
		json += ",\"dst_existing\": \"" +dst_existing + "\"";
		json += ",\"dst_new\": \"" +dst_new + "\"";
		json += ",\"dst_removed\": \"" +dst_removed + "\"";
		json += ",\"svc_existing\": \"" +svc_existing + "\"";
		json += ",\"svc_new\": \"" +svc_new + "\"";
		json += ",\"svc_removed\": \"" +svc_removed + "\"";
		json += ",\"expanded_src_existing\": \"" +expanded_src_existing + "\"";
		json += ",\"expanded_src_new\": \"" +expanded_src_new + "\"";
		json += ",\"expanded_src_removed\": \"" +expanded_src_removed + "\"";
		json += ",\"expanded_dst_existing\": \"" +expanded_dst_existing + "\"";
		json += ",\"expanded_dst_new\": \"" +expanded_dst_new + "\"";
		json += ",\"expanded_dst_removed\": \"" +expanded_dst_removed + "\"";
		json += ",\"expanded_svc_existing\": \"" +expanded_svc_existing + "\"";
		json += ",\"expanded_svc_new\": \"" +expanded_svc_new + "\"";
		json += ",\"expanded_svc_removed\": \"" +expanded_svc_removed + "\"";	
		json += "}";
		return json;
	}
}
