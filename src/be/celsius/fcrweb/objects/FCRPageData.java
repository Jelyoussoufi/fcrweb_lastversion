package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class FCRPageData 
{
	ArrayList<RequestTaskSummary> myTasks;
	ArrayList<RequestSummary> myOngoing;
	ArrayList<RequestSummary> myPrevious;
	
	public FCRPageData()
	{
		
	}

	public ArrayList<RequestTaskSummary> getMyTasks() {
		return myTasks;
	}

	public void setMyTasks(ArrayList<RequestTaskSummary> myTasks) {
		this.myTasks = myTasks;
	}

	public ArrayList<RequestSummary> getMyOngoing() {
		return myOngoing;
	}

	public void setMyOngoing(ArrayList<RequestSummary> myOngoing) {
		this.myOngoing = myOngoing;
	}

	public ArrayList<RequestSummary> getMyPrevious() {
		return myPrevious;
	}

	public void setMyPrevious(ArrayList<RequestSummary> myPrevious) {
		this.myPrevious = myPrevious;
	}
	
		
}
