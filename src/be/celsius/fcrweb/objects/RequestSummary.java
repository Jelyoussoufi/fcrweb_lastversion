package be.celsius.fcrweb.objects;

import java.util.ArrayList;


public class RequestSummary 
{
	int id;
	String detail;
	String requestor;
	String date;
	String status;
	String formMode;
	ArrayList<Task> tasks;
	
	public RequestSummary()
	{}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getRequestor() {
		return requestor;
	}
	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getStatus() {
		return status;
	}
	public String getLabelStatus() {
		String temp_status = status;
		if (temp_status.equals("Validation"))
		{
			temp_status = "FWE Validation";
		}
		else if (temp_status.equals("Implement"))
		{
			temp_status = "FWO Implement";
		}
		return temp_status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<Task> getTasks() {
		return tasks;
	}
	public void addTask(Task task) {
		this.tasks.add(task);
	}
	public void setTasks(ArrayList<Task> tasks) {
		this.tasks = tasks;
	}
	
	public String getFormMode() {
		return formMode;
	}

	public void setFormMode(String form_mode) {
		this.formMode = form_mode;
	}

	public String toJson()
	{		
		String json = "{";
		json += "\"id\": \"" +id + "\"";
		json += ",\"detail\": \"" +detail.replace('"', '\'').replace("\\", "/").replace("\t", " ") + "\"";
		json += ",\"requestor\": \"" +requestor + "\"";
		json += ",\"date\": \"" +date + "\"";
		json += ",\"status\": \"" +status + "\"";
		json += ",\"label_status\": \"" +getLabelStatus() + "\"";
		json += ",\"form_mode\": \"" +formMode + "\"";
		String taskList = "[";
		for (int i = 0; i < tasks.size(); i ++)
		{
			Task temp = tasks.get(i);
			if (i == 0)
			{
				taskList += "{\"task\" : \"" + temp.getTask() + "\",";
			}
			else
			{
				taskList += ",{\"task\" : \"" + temp.getTask() + "\",";
			}			
			taskList += "\"label_task\" : \"" + temp.getLabelTask() + "\",";
			taskList += "\"user\" : \"" + temp.getUser() + "\",";
			taskList += "\"date\" : \"" + temp.getDate() + "\",";
			String commen = ""+temp.getComment();
			taskList += "\"comment\" : \"" + commen.replace('"', '\'').replace("\\", "/").replace("\t", " ")+ "\"}";
		}
		taskList += "]";
		json += ",\"tasks\": " +taskList;
		json += "}";
		return json;
	}
	
}
