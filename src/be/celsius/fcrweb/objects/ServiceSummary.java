package be.celsius.fcrweb.objects;

public class ServiceSummary 
{	
	int id;
	String name;
	String protocol;
	String port;
	String technicalService;
	
	public ServiceSummary()
	{}	

	public String getTechnicalService() {
		return technicalService;
	}

	public void setTechnicalService(String technicalService) {
		this.technicalService = technicalService;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String toJson()
	{		
		String json = "{";
		json += "\"id\": \"" +id + "\"";
		json += ",\"name\": \"" +name + "\"";
		json += ",\"protocol\": \"" +protocol + "\"";
		json += ",\"port\": \"" +port + "\"";
		json += ",\"technical_service\": \"" +technicalService + "\"";
		json += "}";
		return json;
	}
	
	
}
