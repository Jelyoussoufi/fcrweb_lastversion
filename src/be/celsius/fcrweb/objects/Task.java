package be.celsius.fcrweb.objects;

public class Task 
{
	String task;
	String user;
	String date;
	String comment;
	
	public Task()
	{}

	public String getTask() {
		return task;
	}
	
	public String getLabelTask() 
	{
		String temp_status = task;
		if (temp_status.equals("Validation"))
		{
			temp_status = "FWE Validation";
		}
		else if (temp_status.equals("Implement"))
		{
			temp_status = "FWO Implement";
		}
		return temp_status;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
}
