package be.celsius.fcrweb.objects;


public class ObjectInconsistency 
{
	int id;
	String type;
	String ip;
	String netmask;
	String firewalls;
	
	public ObjectInconsistency()
	{
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getNetmask() {
		return netmask;
	}

	public void setNetmask(String netmask) {
		this.netmask = netmask;
	}

	public String getFirewalls() {
		return firewalls;
	}

	public void setFirewalls(String firewalls) {
		this.firewalls = firewalls;
	}

}
