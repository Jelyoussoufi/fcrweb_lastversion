package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class ServiceInconsistencies 
{
	String serviceName;
	ArrayList<ServiceInconsistency> inconsistencies;
	
	public ServiceInconsistencies()
	{
		inconsistencies = new ArrayList<ServiceInconsistency>();
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String objectName) {
		this.serviceName = objectName;
	}

	public ArrayList<ServiceInconsistency> getInconsistencies() {
		return inconsistencies;
	}

	public void setInconsistencies(ArrayList<ServiceInconsistency> inconsistencies) {
		this.inconsistencies = inconsistencies;
	}

}
