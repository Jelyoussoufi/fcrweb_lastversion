package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class ObjectsPageData 
{
	ArrayList<ObjectSummary> myObjects;
	ArrayList<ObjectSummary> PredefinedObjects;
	
	public ObjectsPageData()
	{
		
	}

	public ArrayList<ObjectSummary> getMyObjects() {
		return myObjects;
	}

	public void setMyObjects(ArrayList<ObjectSummary> myObjects) {
		this.myObjects = myObjects;
	}

	public ArrayList<ObjectSummary> getPredefinedObjects() {
		return PredefinedObjects;
	}

	public void setPredefinedObjects(ArrayList<ObjectSummary> genericObjects) {
		PredefinedObjects = genericObjects;
	}
		
}
