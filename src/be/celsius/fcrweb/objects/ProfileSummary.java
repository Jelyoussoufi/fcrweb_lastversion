package be.celsius.fcrweb.objects;

public class ProfileSummary 
{	
	int id;
	String name;
	String description;
	String technicalService;
	
	public ProfileSummary()
	{}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	

	public String getTechnicalService() {
		return technicalService;
	}

	public void setTechnicalService(String technicalService) {
		this.technicalService = technicalService;
	}

	public String toJson()
	{		
		String json = "{";
		json += "\"id\": \"" +id + "\"";
		json += ",\"name\": \"" +name + "\"";
		json += ",\"description\": \"" +description + "\"";
		json += ",\"technical_service\": \"" +technicalService + "\"";
		json += "}";
		return json;
	}
	
	
}
