package be.celsius.fcrweb.objects;

public class Rule 
{	
	RuleSummary summary;
	RuleSummary expandedSummary;
	String name;
	String created;
	String comment;
	String fcr;
	String endDate;
	String lastUsed;
	
	public Rule()
	{}
		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public RuleSummary getSummary() {
		return summary;
	}

	public void setSummary(RuleSummary summary) {
		this.summary = summary;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getFcr() {
		return fcr;
	}

	public void setFcr(String fcr) {
		this.fcr = fcr;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getLastUsed() {
		return lastUsed;
	}

	public void setLastUsed(String lastUsed) {
		this.lastUsed = lastUsed;
	}

	public RuleSummary getExpandedSummary() {
		return expandedSummary;
	}

	public void setExpandedSummary(RuleSummary expandedSummary) {
		this.expandedSummary = expandedSummary;
	}

	public String toJson()
	{		
		String json = "{";
		json += "\"name\": \"" +name + "\"";
		json += "\"created\": \"" +created + "\"";
		json += ",\"comment\": \"" +comment + "\"";
		json += ",\"fcr\": \"" +fcr + "\"";
		json += ",\"endDate\": \"" +endDate + "\"";
		json += ",\"lastUsed\": \"" +lastUsed + "\"";
		json += ",\"ruleSummary\": " + summary.toJson();
		json += "}";
		return json;
	}
	
	
}
