package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class ServicesSearchData 
{
	ArrayList<ServiceSummary> services;
	ArrayList<ServiceGroupSummary> serviceGroups;
	
	public ServicesSearchData()
	{
		services = new ArrayList<ServiceSummary>();
		serviceGroups = new ArrayList<ServiceGroupSummary>();
	}

	public ArrayList<ServiceSummary> getServices() {
		return services;
	}
	
	public void addService(ServiceSummary s)
	{
		this.services.add(s);
	}

	public void setServices(ArrayList<ServiceSummary> services) {
		this.services = services;
	}

	public ArrayList<ServiceGroupSummary> getServiceGroups() {
		return serviceGroups;
	}
	
	public void addServiceGroup(ServiceGroupSummary s)
	{
		this.serviceGroups.add(s);
	}

	public void setServiceGroups(ArrayList<ServiceGroupSummary> serviceGroups) {
		this.serviceGroups = serviceGroups;
	}

}
