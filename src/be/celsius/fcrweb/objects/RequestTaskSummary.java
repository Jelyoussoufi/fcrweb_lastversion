package be.celsius.fcrweb.objects;

import java.util.ArrayList;


public class RequestTaskSummary 
{
	int id;
	String detail;
	String requestor;
	String date;
	String status;
	String formMode;
	String task;
	ArrayList<Task> tasks;
	
	public RequestTaskSummary()
	{}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getRequestor() {
		return requestor;
	}
	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getStatus() {
		return status;
	}
	public String getLabelStatus() 
	{
		String temp_status = status;
		if (temp_status.equals("Validation"))
		{
			temp_status = "FWE Validation";
		}
		else if (temp_status.equals("Implement"))
		{
			temp_status = "FWO Implement";
		}
		return temp_status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<Task> getTasks() {
		return tasks;
	}
	public void addTask(Task task) {
		this.tasks.add(task);
	}
	public void setTasks(ArrayList<Task> tasks) {
		this.tasks = tasks;
	}
	
	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getFormMode() {
		return formMode;
	}

	public void setFormMode(String formMode) {
		this.formMode = formMode;
	}
	
}
