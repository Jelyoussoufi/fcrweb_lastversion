package be.celsius.fcrweb.objects;

import java.util.ArrayList;

public class FWObjectNamesNotCompliant 
{
	ArrayList<String[]> networkObjectGroups;
	ArrayList<String[]> subnets;
	ArrayList<String[]> hosts;
	ArrayList<String[]> serviceGroups;
	ArrayList<String[]> services;
	ArrayList<String> userGroups;
	
	public FWObjectNamesNotCompliant()
	{
		networkObjectGroups = new ArrayList<String[]>();
		subnets = new ArrayList<String[]>();
		hosts = new ArrayList<String[]>();
		serviceGroups = new ArrayList<String[]>();
		services = new ArrayList<String[]>();
		userGroups = new ArrayList<String>();
	}

	public ArrayList<String[]> getNetworkObjectGroups() {
		return networkObjectGroups;
	}

	public void setNetworkObjectGroups(ArrayList<String[]> grps) {
		this.networkObjectGroups = grps;
	}

	public ArrayList<String[]> getServiceGroups() {
		return serviceGroups;
	}

	public void setServiceGroups(ArrayList<String[]> serviceGroups) {
		this.serviceGroups = serviceGroups;
	}

	public ArrayList<String[]> getServices() {
		return services;
	}

	public void setServices(ArrayList<String[]> services) {
		this.services = services;
	}

	public ArrayList<String> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(ArrayList<String> userGroups) {
		this.userGroups = userGroups;
	}

	public ArrayList<String[]> getSubnets() {
		return subnets;
	}

	public void setSubnets(ArrayList<String[]> subnets) {
		this.subnets = subnets;
	}

	public ArrayList<String[]> getHosts() {
		return hosts;
	}

	public void setHosts(ArrayList<String[]> hosts) {
		this.hosts = hosts;
	}	
	
}
